//
//  BGChangeMoneyPopView.swift
//  gameplay
//
//  Created by block on 2019/2/25.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

//MARK: 代理协议
protocol BGChangeMoneyPopViewDelegate:NSObjectProtocol {
    
    /** 点击立即进入游戏 */
    func clickNowPlayGame(playIndexPath:IndexPath,gameCode:String,forwardUrl:String?);
    
}
class BGChangeMoneyPopView: UIView,ChessAndCardPayContentViewDelegate {
    /**游戏代码*/
    var gameCode:String = ""
    
    var lastController:BaseController?
    
    /** 金额数组  Key玩法   value金额 */
    var moneyAry:[Dictionary<String, Any>]?
    
    /** 点击的当前游戏的玩法下标 */
    var playIndexPath:IndexPath?
    
    var meminfo:Meminfo?
    
    var lotterData:LotteryData?
    
    
    weak var delegate:BGChangeMoneyPopViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        autoLayoutSubView()
        self.backgroundColor = UIColor.colorWithRGB(r: 0, g: 0, b: 0, alpha: 0.3)
    }
    var code:String{
        get{
            return self.gameCode
        }
        set{
            self.gameCode = newValue

        }
    }
    
    var gameName:String = ""{
        didSet{
            //游戏赋值
            self.NBpayView.walletBalanceTitle.text = String(format: "%@余额：",gameName)
        }
    }
    var assController:BaseController{
        get{
            return self.lastController!
        }
        set{
            self.lastController = newValue
            accountWeb(type: 1)
        }
    }
    
    //#MARK: 业务事件
    /** 获取总余额 */
    func accountWeb(type:Int) -> Void {
        //帐户相关信息
        
//        print(self.gameCode)
        
        self.lastController?.request(frontDialog: false, url:MEMINFO_URL,
                                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                                        if !resultStatus {
                                            return
                                        }
                                        if let result = MemInfoWraper.deserialize(from: resultJson){
                                            if result.success{
                                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                                if let memInfo = result.content{
                                                    self.meminfo = memInfo
                                                    self.walletBalance.text = String(format: "%@元", memInfo.balance)
                                                    self.balanceGetWith(gameCode: self.gameCode, type: type)

                                                }
                                            }
                                        }
        })
    }
    
    /** 获取单项余额 */
    func balanceGetWith(gameCode:String, type:Int){
//        var changeCode = gameCode
//        if gameCode == "dgzr"{
//            changeCode="dg"
//        }
        self.lastController?.request(frontDialog: false,method: .post,url: REAL_GAME_BALANCE_URL,params:["type":gameCode],
                                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                                        if isEmptyString(str: resultJson){
                                            return
                                        }
                                        if !resultStatus {
                                            if resultJson.isEmpty {
                                                showToast(view: self, txt: convertString(string: "同步余额失败"))
                                            }else{
                                                showToast(view: self, txt: resultJson)
                                            }
                                            return
                                        }
                                        let data = resultJson.data(using: String.Encoding.utf8, allowLossyConversion: true)
                                        //把Data对象转换回JSON对象
                                        let json = try? JSONSerialization.jsonObject(with: data!,options:.allowFragments) as! [String: Any]
                                        if !JSONSerialization.isValidJSONObject(json){
                                            return
                                        }
                                        let dic = [gameCode:json!["money"]] //gameCode
                                        self.moneyAry?.append(dic)
                                        if (json!["money"] != nil){
                                                self.NBpayView.walletBalance.text = String(format: "%@元", json!["money"] as! CVarArg)
                                        }
                                        self.NBpayView.moneyTF.text = ""
        })
    }
    
    /** 额度转换代码 */
    func actionConvert(from:String,to:String,money:String,changeType:Int) -> Void {

        
        let params = ["changeFrom":from,"changeTo":to,"money":money] as Dictionary<String, AnyObject> //from,to
        print(params)
        self.lastController?.request(frontDialog: true,method: .post, loadTextStr:"转换中...", url:FEE_CONVERT_URL,
                                     params: params,
                                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                                        if !resultStatus {
                                            if resultJson.isEmpty {
                                                showToast(view: self, txt: convertString(string: "额度转换失败"))
                                            }else{
                                                showToast(view: self, txt: resultJson)
                                            }
                                            return
                                        }
                                        if isEmptyString(str: resultJson){
                                            return
                                        }
                                        let data = resultJson.data(using: String.Encoding.utf8, allowLossyConversion: true)
                                        //把Data对象转换回JSON对象
                                        let json = try? JSONSerialization.jsonObject(with: data!,options:.allowFragments) as! [String: Any]
                                        if (json?.keys.contains("success"))!{
                                            let ok = json!["success"] as! Bool
                                            if ok{
                                                showToast(view: self, txt: "额度转换成功")
                                                self.accountWeb(type: 1)
                                                self.balanceGetWith(gameCode:self.gameCode , type: 1)

                                                
                                            }else{
                                                if (json?.keys.contains("msg"))!{
                                                    showToast(view: self, txt: json!["msg"] as! String)
                                                }else{
                                                    showToast(view: self, txt: "额度转换失败")
                                                }
                                            }
                                        }
        })
    }
    
    //#MARK: 点击事件
    /**f
     点击按钮调用方法
     1. 点击取消按钮  2. 立即充值  3. 一键刷新  4. 进入游戏
     */
    @objc func clickButton(send:UIButton){
        if send.tag == 1{
            self.removeFromSuperview()
        }else if send.tag == 3{
            showToast(view: self, txt: convertString(string: "正在获取"))
            accountWeb(type: 2)
        }else if send.tag == 2 {
            self.removeFromSuperview()
            if getCharge_page_mode_switch() == "v1" {
                let recordPage = getChareController() as! NewChargeMoneyV2Controller
                recordPage.meminfo = self.meminfo
                self.lastController?.navigationController?.pushViewController(recordPage, animated: true)
            }else{
                let recordPage = getChareController() as! NewChargeMoneyController
                recordPage.meminfo = self.meminfo
                self.lastController?.navigationController?.pushViewController(recordPage, animated: true)
            }
        }else if send.tag == 4{
            self.removeFromSuperview()
            self.delegate?.clickNowPlayGame(playIndexPath: self.playIndexPath!, gameCode: self.gameCode, forwardUrl: lotterData?.forwardUrl)
        }
    }
    
    //#MARK: 代理方法
    /**
     输入框内容改变调用代理方法
     */
    func textFieldVaueChange(changeText: String, selfTag: Int) {
        
    }
    /**
     点击了转入转出按钮调用代理方法
     10 转入 11 转出
     10 nb 11 ky
     */
    func contentViewOperationWiht(type: Int, selfTag: Int) {
//        if self.gameCode == "dgzr"{
//             self.actionConvert(from: type == 10 ? "sys" : "dg" , to: type == 10 ? "dg" : "sys", money: self.NBpayView.moneyTF.text!, changeType: selfTag)
//        }else if self.gameCode == "bgzr"{
//         self.actionConvert(from: type == 10 ? "sys" : "bg" , to: type == 10 ? "bg" : "sys", money: self.NBpayView.moneyTF.text!, changeType: selfTag)
//        }else if self.gameCode == "rgzr"{
//            self.actionConvert(from: type == 10 ? "sys" : "rg" , to: type == 10 ? "rg" : "sys", money: self.NBpayView.moneyTF.text!, changeType: selfTag)
//        }
        
        self.actionConvert(from: type == 10 ? "sys":gameCode, to: type == 10 ? gameCode : "sys", money: self.NBpayView.moneyTF.text!, changeType: selfTag)

    }
    //#MARK: 约束
    func autoLayoutSubView (){
        self.bgView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.centerY.equalTo(self.snp.centerY)
            make.height.equalTo(330)
        }
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(0)
            make.height.equalTo(40)
        }
        self.cancelBtn.snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.right.equalTo(-5)
            make.width.height.equalTo(30)
        }
        self.headerView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.top.equalTo(55)
            make.height.equalTo(45)
        }
        self.walletBalanceTitle.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.centerY.equalTo(self.headerView.snp.centerY)
        }
        self.walletBalance.snp.makeConstraints { (make) in
            make.left.equalTo(self.walletBalanceTitle.snp.right).offset(0)
            make.centerY.equalTo(self.headerView.snp.centerY)
        }
        self.nowRecharge.snp.makeConstraints { (make) in
            make.right.equalTo(self.reload.snp.left).offset(-10)
            make.centerY.equalTo(self.headerView.snp.centerY)
            make.height.equalTo(25)
            make.width.equalTo(55)
        }
        self.reload.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.centerY.equalTo(self.headerView.snp.centerY)
            make.height.equalTo(25)
            make.width.equalTo(55)
        }
        self.contentView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.top.equalTo(self.headerView.snp.bottom).offset(15)
            make.height.equalTo(140)
        }
        self.NBpayView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.top.equalTo(self.headerView.snp.bottom).offset(15)
            make.height.equalTo(120)
        }

        self.startGame.snp.makeConstraints { (make) in
            make.width.equalTo(150)
            make.bottom.equalTo(-20)
            make.height.equalTo(40)
            make.centerX.equalTo(self.bgView.snp.centerX).offset(0)
        }
    }
    
    //#MARK: 懒加载
    /** 背景视图 */
    lazy var bgView : UIView = {
        let view = UIView.init()
        view.backgroundColor = UIColor.colorWithRGB(r: 255, g: 246, b: 225, alpha: 1)
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true
        self.addSubview(view)
        return view
    }()
    /** 标题 */
    lazy var titleLabel : UILabel = {
        let label = UILabel.initWith(text: "快速转账", textColor: UIColor.colorWithRGB(r: 254, g: 197, b: 64, alpha: 1), fontSize: 16, fontName: 2)
        label.textAlignment = .center
        label.backgroundColor = UIColor.colorWithRGB(r: 79, g: 55, b: 6, alpha: 1)
        self.bgView.addSubview(label)
        return label
    }()
    /** 取消按钮 */
    lazy var cancelBtn : UIButton = {
        let btn = UIButton.initWith(title: "X", titleColot: nil, fontSize: 16, self, action: #selector(clickButton(send:)))
        btn.tag = 1
        btn.titleLabel?.textColor = UIColor.colorWithRGB(r: 254, g: 197, b: 64, alpha: 1)
        self.bgView.addSubview(btn)
        return btn
    }()
    
    /** 头部视图 */
    lazy var headerView : UIView = {
        let view = UIView.init()
        view.backgroundColor = UIColor.colorWithRGB(r: 254, g: 223, b: 158, alpha: 1)
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true
        let borderLayer = CAShapeLayer.init(layer: view.layer)
        borderLayer.frame = CGRect(x: 0, y: 0, width: kScreenWidth-40, height: 45)
        borderLayer.path = UIBezierPath.init(roundedRect: borderLayer.bounds, cornerRadius: 4).cgPath
        borderLayer.lineWidth = 1
        borderLayer.lineDashPattern = [3,3]
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.black.cgColor
        view.layer.addSublayer(borderLayer)
        self.bgView.addSubview(view)
        return view
    }()
    
    /** 钱包余额标题 */
    lazy var walletBalanceTitle : UILabel = {
        let label = UILabel.initWith(text: "钱包余额：", textColor: UIColor.black, fontSize: 14, fontName: 0)
        self.headerView.addSubview(label)
        return label
    }()
    /** 钱包余额 */
    lazy var walletBalance : UILabel = {
        let label = UILabel.initWith(text: "0.00元", textColor: UIColor.red, fontSize: 14, fontName: 0)
        self.headerView.addSubview(label)
        return label
    }()
    
    /** 立即充值 */
    lazy var nowRecharge : UIButton = {
        let btn = UIButton.initWith(title: "立即充值", titleColot: UIColor.colorWithRGB(r: 254, g: 197, b: 64, alpha: 1), fontSize: 12, self, action: #selector(clickButton(send:)))
        btn.tag = 2
        btn.backgroundColor = UIColor.colorWithRGB(r: 79, g: 55, b: 6, alpha: 1)
        self.headerView.addSubview(btn)
        return btn
    }()
    
    /** 一键刷新 */
    lazy var reload : UIButton = {
        let btn = UIButton.initWith(title: "一键刷新", titleColot: UIColor.white, fontSize: 12, self, action: #selector(clickButton(send:)))
        btn.tag = 3
        btn.backgroundColor = UIColor.colorWithRGB(r: 21, g: 153, b: 37, alpha: 1)
        self.headerView.addSubview(btn)
        return btn
    }()
    
    /** 内容视图 */
    lazy var contentView : UIView = {
        let view = UIView.init()
        view.backgroundColor = UIColor.colorWithRGB(r: 254, g: 223, b: 158, alpha: 1)
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true
        let borderLayer = CAShapeLayer.init(layer: view.layer)
        borderLayer.frame = CGRect(x: 0, y: 0, width: kScreenWidth-40, height: 140)
        borderLayer.path = UIBezierPath.init(roundedRect: borderLayer.bounds, cornerRadius: 4).cgPath
        borderLayer.lineWidth = 1
        borderLayer.lineDashPattern = [3,3]
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.black.cgColor
        view.layer.addSublayer(borderLayer)
        self.bgView.addSubview(view)
        return view
    }()
    /**充值输入视图 */
    lazy var NBpayView : ChessAndCardPayContentView = {
        let view = ChessAndCardPayContentView.init(frame: .zero)
        view.tag = 10
        view.delegate = self
        view.walletBalanceTitle.text = "BG捕鱼余额："
        self.bgView.addSubview(view)
        return view
    }()

    
    /** 开始游戏 */
    lazy var startGame : UIButton = {
        let btn = UIButton.initWith(title: "进入游戏", titleColot: UIColor.white, fontSize: 14, self, action: #selector(clickButton(send:)))
        btn.tag = 4
        btn.backgroundColor = UIColor.colorWithRGB(r: 148, g: 103, b: 21, alpha: 1)
        btn.layer.cornerRadius = 4
        btn.layer.masksToBounds = true
        self.bgView.addSubview(btn)
        return btn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
