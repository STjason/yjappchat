//
//  ChessAndCardPayContentView.swift
//  gameplay
//
//  Created by block on 2018/12/31.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
//MARK: 代理协议
protocol ChessAndCardPayContentViewDelegate:NSObjectProtocol {
    
    func textFieldVaueChange(changeText:String, selfTag:Int)
    /**
     点击了转入转出按钮调用代理方法
     10 转入 11 转出
     */
    func contentViewOperationWiht(type:Int,selfTag:Int)
}
class ChessAndCardPayContentView: UIView,UITextFieldDelegate {

    weak var delegate:ChessAndCardPayContentViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        autoLayoutSubView()
        self.moneyTF.addTarget(self, action:#selector(moneyTextFiledValueChange) , for: .editingChanged)
    }
    
    //#MARK: 业务事件
    @objc func moneyTextFiledValueChange() {
        delegate?.textFieldVaueChange(changeText: self.moneyTF.text!, selfTag: self.tag)
    }
    
    /** 输入框代理方法 限制输入内容 */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location > 6{
            showToast(view: self, txt: "当前允许输入最大转换额度长度为7位")
            return false
        }
        return true
    }
    //#MARK: 点击事件
    /**
     点击操作按钮
     10 转入 11 转出
     */
    @objc func clickOperationBtn(send:UIButton){
        if self.moneyTF.text == ""{
            showToast(view: self, txt: "请输入金额")
        }
        delegate?.contentViewOperationWiht(type: send.tag, selfTag: self.tag)
    }
    
    /** 点击快捷投注按钮 */
    @objc func clickShortcutBtn(send:UIButton){
        switch send.tag {
        case 1:
            self.moneyTF.text = "10"
        case 2:
            self.moneyTF.text = "50"
        case 3:
            self.moneyTF.text = "100"
        case 4:
            self.moneyTF.text = "200"
        case 5:
            self.moneyTF.text = "500"
        case 6:
            self.moneyTF.text = "1000"
        case 7:
            delegate?.textFieldVaueChange(changeText: "全部", selfTag: self.tag)
        default:
            delegate?.textFieldVaueChange(changeText: self.moneyTF.text!, selfTag: self.tag)
        }
    }
    
    //#MARK: 代理方法
    
    
    //#MARK: 约束
    func autoLayoutSubView (){
        
        self.walletBalanceTitle.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalTo(10)
        }
        self.walletBalance.snp.makeConstraints { (make) in
            make.left.equalTo(self.walletBalanceTitle.snp.right).offset(0)
            make.centerY.equalTo(self.walletBalanceTitle.snp.centerY)
        }
        
        self.moneyTF.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(self.walletBalanceTitle.snp.bottom).offset(3)
            make.height.equalTo(40)
        }
        
        self.rollOut.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.top.equalTo(self.moneyTF.snp.bottom).offset(15)
            make.height.equalTo(25)
            make.width.equalTo(40)
        }
        self.rollIn.snp.makeConstraints { (make) in
            make.right.equalTo(self.rollOut.snp.left).offset(-10)
            make.top.equalTo(self.moneyTF.snp.bottom).offset(15)
            make.height.equalTo(25)
            make.width.equalTo(40)
        }
        
        var lastBtn = UIButton.init()
        for i in 0...6{
            let btn = UIButton.initWith(title: ["10","50","100","200","500","1000","全部"][i], titleColot: UIColor.white, fontSize: 10, self, action: #selector(clickShortcutBtn(send:)))
            btn.tag = i+1;
            btn.layer.cornerRadius = 3;
            btn.layer.masksToBounds = true
            btn.backgroundColor = UIColor.colorWithRGB(r: 252, g: 62, b: 66, alpha: 1)
            self.addSubview(btn)
            if i==0{
                btn.snp.makeConstraints { (make) in
                    make.left.equalTo(8)
                    make.top.equalTo(self.moneyTF.snp.bottom).offset(15)
                    make.height.equalTo(23)
                    make.width.equalTo(20)
                }
            }else{
                btn.snp.makeConstraints { (make) in
                    make.left.equalTo(lastBtn.snp.right).offset(8)
                    make.top.equalTo(self.moneyTF.snp.bottom).offset(15)
                    make.height.equalTo(23)
                    make.width.equalTo([20,20,23,23,25,25,37,25][i])
                }
            }
            lastBtn = btn;
        }
        
        
    }
    
    //#MARK: 懒加载
    /** 余额标题 */
    lazy var walletBalanceTitle : UILabel = {
        let label = UILabel.initWith(text: "", textColor: UIColor.black, fontSize: 14, fontName: 0)
        self.addSubview(label)
        return label
    }()
    /** 余额 */
    lazy var walletBalance : UILabel = {
        let label = UILabel.initWith(text: "0元", textColor: UIColor.red, fontSize: 14, fontName: 0)
        self.addSubview(label)
        return label
    }()
    /** 金额输入框 */
    lazy var moneyTF : UITextField = {
        let textField = CustomFeildText.init(frame: .zero)
        textField.placeholder = " 0元"
        textField.layer.cornerRadius = 4;
        textField.layer.masksToBounds = true
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.black.cgColor
        textField.keyboardType = .numberPad
        textField.delegate = self
        self.addSubview(textField)
        return textField
    }()
    
    /** 转入按钮 */
    lazy var rollIn : UIButton = {
        let btn = UIButton.initWith(title: "转入", titleColot: UIColor.black, fontSize: 13, self, action: #selector(clickOperationBtn(send:)))
        btn.tag = 10;
        btn.layer.cornerRadius = 3;
        btn.layer.masksToBounds = true
        btn.backgroundColor = UIColor.colorWithRGB(r: 253, g: 208, b: 47, alpha: 1)
        self.addSubview(btn)
        return btn
    }()
    /** 转出按钮 */
    lazy var rollOut : UIButton = {
        let btn = UIButton.initWith(title: "转出", titleColot: UIColor.white, fontSize: 13, self, action: #selector(clickOperationBtn(send:)))
        btn.backgroundColor = UIColor.colorWithRGB(r: 149, g: 103, b: 21, alpha: 1)
        btn.tag = 11;
        btn.layer.cornerRadius = 3;
        btn.layer.masksToBounds = true
        self.addSubview(btn)
        return btn
    }()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


}
