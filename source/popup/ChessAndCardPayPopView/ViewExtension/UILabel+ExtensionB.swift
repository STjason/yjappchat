//
//  UILabel+Extension.swift
//  YiboGameIos
//
//  Created by block on 2018/11/21.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

extension UILabel {

    /// 为label设置富文本
    /// - Parameters:
    ///   - text: 要设置的文本
    ///   - ranges: 要设置的富文本的range
    ///   - colors: 富文本字体颜色
    func setAttributeString(text:String,ranges:[NSRange],colors:[UIColor],fonts:[CGFloat]?) {
        let attributeString = NSMutableAttributedString(string: text)
        for (index,range) in ranges.enumerated() {
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: colors[index], range: range)
            
            if let optionalFonts = fonts {
                attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: optionalFonts[index]), range: range)
            }
        }
        
        self.attributedText = attributeString
    }
    
    class func initWith(text:String, textColor:UIColor?, fontSize:CGFloat ,fontName:Int) -> UILabel{
        let label = UILabel.init(frame:.zero)
        label.text = text
        label.textColor = (textColor != nil) ? textColor : UIColor.black
        /*
        UIFontWeightUltraLight  - 超细字体
        UIFontWeightThin  - 纤细字体
        UIFontWeightLight  - 亮字体
        UIFontWeightRegular  - 常规字体
        UIFontWeightMedium  - 介于Regular和Semibold之间
        UIFontWeightSemibold  - 半粗字体
        UIFontWeightBold  - 加粗字体
        UIFontWeightHeavy  - 介于Bold和Black之间
        UIFontWeightBlack  - 最粗字体(理解)
         */
        let weight:UIFont
        switch fontName {
        case 0://常规字体
            weight = UIFont.systemFont(ofSize: fontSize, weight: .regular)
        case 1://一般加粗
            weight = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        case 2://加粗
            weight = UIFont.systemFont(ofSize: fontSize, weight: .black)
        default:
            weight = UIFont.systemFont(ofSize: fontSize, weight: .regular)
        }
        
        label.font = weight
        
        return label
    }
    
    
}
