//
//  UIButton+Extension.swift
//  YiboGameIos
//
//  Created by block on 2018/11/21.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

extension UIButton {
    class func initWith(title:String, titleColot:UIColor?,fontSize:CGFloat, _ target:Any?, action:Selector?) -> UIButton {
        let btn = UIButton.init(type: .custom)
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(titleColot, for: .normal)
        if (action != nil) {
            btn.addTarget(target, action: action!, for: .touchUpInside)
        }
        btn.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        return btn
    }
    
}
