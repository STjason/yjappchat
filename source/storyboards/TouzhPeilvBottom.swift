//
//  TouzhPeilvBottom.swift
//  gameplay
//
//  Created by admin on 2018/10/10.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class TouzhPeilvBottom: UIView {
    
    var fastBtnActionHandler:((UIButton) -> Void)?
    var betBtnClickHandler:(() -> Void)?
    var clearBtnClickHandler:(() -> Void)?
    var minusClickHandler:((UITapGestureRecognizer) -> Void)?
    var plusClickHandler:((UITapGestureRecognizer) -> Void)?
    var sliderChangeHandler:((CustomSlider) -> Void)?
    var creditSliderChangedHandler:((CustomSlider, UIEvent) -> Void)?
    var creditMoneyTextChangeHandler:((CustomFeildText) -> Void)?
    var creditFastMoneyChooseHandler:((String) -> Void)?
    
    @IBOutlet weak var chipsBgView: UIView!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var creditbottomTopField: CustomFeildText!
    @IBOutlet weak var noteCountLabel: UILabel!
    @IBOutlet weak var accountBalanceLaebl: UILabel!
    @IBOutlet weak var creditbottomTopSlider: CustomSlider!
    @IBOutlet weak var bet_kick_add: UIImageView!
    @IBOutlet weak var bet_kick_minus: UIImageView!
    @IBOutlet weak var creditbottomSlideLeftLabel: UILabel!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var betBtn: UIButton!
    @IBOutlet weak var fastBtn: UIButton!
    
    lazy var xibView:UIView = {
        return Bundle.main.loadNibNamed("TouzhPeilvBottom", owner: self, options: nil)?.first as! UIView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibView.frame = bounds
        addSubview(xibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(self.xibView)
        xibView.frame = self.bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        creditbottomSlideLeftLabel.textColor = UIColor.colorWithHexString("a2a2a2")
        creditbottomSlideLeftLabel.textColor = UIColor.white
        setupTheme()
        setupBtn(btn: betBtn)
        setupBtn(btn: clearBtn)
        setupBtn(btn: fastBtn)
        setupMinus(image: bet_kick_minus, ismMinus: true)
        setupMinus(image: bet_kick_add, ismMinus: false)
        setupSlider(slider: creditbottomTopSlider)
        setPeilvInput(field: creditbottomTopField)
        
        setUpChipsView()
    }
    
    //MARK: - 私有方法
    //MARK: 主题色
    private func setupTheme() {
        setViewBackgroundColorTransparent(view: sliderView, alpha: 0.8,color: UIColor.black)
        setViewBackgroundColorTransparent(view: bottomView, alpha: 0.8,color: UIColor.black)
        setViewBackgroundColorTransparent(view: chipsBgView, alpha: 0.8, color: UIColor.black)
    }
    
    //MARK: 设置金额输入框
    private func setPeilvInput(field:CustomFeildText) {
        field.addTarget(self, action: #selector(creditMoneyTextChange(textField:)), for: UIControl.Event.editingChanged)
        
        field.layer.cornerRadius = 3.0
        field.layer.masksToBounds = true
    }
    
    //MARK: 减少、返水图片
    private func setupMinus(image:UIImageView,ismMinus:Bool) {
        if ismMinus
        {
            image.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(minusClick(ui:))))
        }else
        {
            image.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(plusClick(ui:))))
        }
    }
    
    //MARK: 设置按钮
    private func setupBtn(btn: UIButton) {
        btn.layer.cornerRadius = 3.0
        btn.layer.masksToBounds = true
    }
    
    //MARK: 设置slider
    private func setupSlider(slider:CustomSlider) {
        slider.addTarget(self, action: #selector(sliderChange), for: UIControl.Event.valueChanged)
        
        slider.addTarget(self, action: #selector(creditSliderChanged(slider:event:)), for: .valueChanged)

    }
    
    //MARK: - 事件方法
    @objc func creditMoneyTextChange(textField:CustomFeildText) {
        creditMoneyTextChangeHandler?(textField)
    }
    
    
    @objc func creditSliderChanged(slider: CustomSlider, event: UIEvent) {
        creditSliderChangedHandler?(slider,event)
    }
    
    @objc func sliderChange(slider:CustomSlider) -> Void{
        sliderChangeHandler?(slider)
    }
    
    @objc private func minusClick(ui:UITapGestureRecognizer) {
        self.minusClickHandler?(ui)
    }
    
    @objc private func plusClick(ui:UITapGestureRecognizer) {
        self.plusClickHandler?(ui)
    }
    
    @IBAction func betBtnClick(_ sender: Any) {
        betBtnClickHandler?()
    }
    
    @IBAction func clearBtnClick(_ sender: Any) {
        clearBtnClickHandler?()
    }
    
    @IBAction func fastBtnAction(_ sender: UIButton) {
        fastBtnActionHandler?(sender)
    }
    
    
    
    //筹码视图
    //MARK: 获取快速金额
    func getFastMoneySetting() -> [String] {
        var str = ""
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                str = config.content.fast_money_setting
            }
        }
        if isEmptyString(str: str){
            str = "10,20,50,100,200,500,1000,2000,5000"
        }
        
        str = str.trimmingCharacters(in: .whitespaces)
        let moneys = str.components(separatedBy: ",")
        return moneys
    }
    
    private func setUpChipsView() {
        for view in chipsBgView.subviews {
            view.removeFromSuperview()
        }
        
        let moneyItems = getFastMoneySetting()
        
        let scrollView = UIScrollView()
        let containerView = UIView()
        chipsBgView.addSubview(scrollView)
        scrollView.addSubview(containerView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalTo(scrollView)
            make.height.equalTo(scrollView)
        }
        
        let moneyBtnWH:CGFloat = 45
        let btnMargin:CGFloat = 5 //筹码左右间距
        
        for (index,money) in moneyItems.enumerated() {
            let moneyBtn = UIButton()
            configBtnFontAndBgImg(money: money, button: moneyBtn,index: index)
            containerView.addSubview(moneyBtn)
            
            moneyBtn.snp.makeConstraints { (make) in
                make.centerY.equalTo(25)
                make.left.equalTo( CGFloat(index) * moneyBtnWH + CGFloat(index + 1) * btnMargin )
                make.width.height.equalTo(moneyBtnWH)
                
                if index + 1 == moneyItems.count {
                    make.right.equalTo(containerView.snp.right)
                }
            }
        }
    }
    
    func configBtnFontAndBgImg(money:String,button:UIButton,index:Int) {
        guard let num = Int64(money) else {return}
        
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle(money, for: .normal)
        
        button.tag = 100*100 + index
        button.addTarget(self, action: #selector(clickChipAction), for: .touchUpInside)
        
        //设置背景图
        var imgName = ""
        if (num == 1) {
            imgName = "chip_1"
        } else if (num == 2) {
            imgName = "chip_2"
        } else if (num > 2 && num <= 5) {
            imgName = "chip_3"
        } else if (num > 5 && num <= 10) {
            imgName = "chip_4"
        } else if (num > 10 && num <= 20) {
            imgName = "chip_5"
        } else if (num > 20 && num <= 25) {
            imgName = "chip_6"
        } else if (num > 5 && num <= 50) {
            imgName = "chip_7"
        } else if (num > 50 && num <= 100) {
            imgName = "chip_8"
        } else if (num > 100 && num <= 200) {
            imgName = "chip_9"
        } else if (num > 200 && num <= 500) {
            imgName = "chip_10"
        } else if (num > 500 && num <= 1000) {
            imgName = "chip_11"
        } else if (num > 1000 && num <= 5000) {
            imgName = "chip_12"
        } else {
            imgName = "chip_13"
        }
        
        button.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
        
        //设置字号
        var fontSize:CGFloat = 16
        if money.length == 2 {
            fontSize = 15
        }else if money.length == 3 {
            fontSize = 13
        }else if money.length >= 4 {
            fontSize = 11
        }
        
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    @objc func clickChipAction(sender:UIButton) {
        let index = sender.tag - 100*100

        let moneyItems = getFastMoneySetting()
        if index < moneyItems.count {
            let moneyValue = moneyItems[index]
//            selectMoneyFromDialog(money: moneyValue)
            creditFastMoneyChooseHandler?(moneyValue)
        }
    }

}
