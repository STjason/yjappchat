//
//  PagePicturePop.swift
//  gameplay
//
//  Created by admin on 2019/6/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class PagePicturePop: UIView {
    
    private var popViewWidth:CGFloat = screenWidth - 80
    private var popViewHeight:CGFloat = screenHeight - 130
    
    private var urls = [String]()
    private let CELLIDENTIFIER = "pictureCollectionCell"
    
    private var indicator = UIPageControl()
    private var bgView = UIView() //弹窗视图父视图
    private var coverButton = UIButton() //点击隐藏背景按钮
    
    lazy var collection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize.init(width: popViewWidth, height: popViewHeight)
        
        let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clear
        collectionView.isPagingEnabled = true
        collectionView.contentSize = CGSize.init(width: popViewWidth * 2, height: popViewHeight)
        
        collectionView.register(PictureCollectionCell.self, forCellWithReuseIdentifier: CELLIDENTIFIER)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        return collectionView
    }()
    
    convenience init(frame: CGRect,urls:[String]) {
        self.init(frame: frame)
        self.urls = urls
        self.setupUI()
    }

    //MARK: - UI
    private func setupUI() {
        
        self.indicator.currentPage = 0
        self.indicator.numberOfPages = self.urls.count
        self.indicator.isHidden = self.urls.count <= 1
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.45)
        
        self.coverButton.backgroundColor = UIColor.clear
        self.coverButton.addTarget(self, action: #selector(closebgView), for: .touchUpInside)
        
        bgView.backgroundColor = UIColor.groupTableViewBackground
        
        indicator.pageIndicatorTintColor = UIColor.red
        indicator.contentMode = .center
        indicator.currentPageIndicatorTintColor = UIColor.lightGray
        
        let header = UIView()
        setThemeViewThemeColor(view: header)
        
        let titleLabel = UILabel()
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 17.0)
        titleLabel.text = "存款指南"
        
        let closeButton = UIButton.init(type: .custom)
        closeButton.setImage(UIImage.init(named: "closeButtonImg"), for: .normal)
        closeButton.addTarget(self, action: #selector(closebgView), for: .touchUpInside)
        
        if let window = UIApplication.shared.keyWindow {
            
            window.addSubview(self)
            self.isHidden = true
        }
        
        addSubview(self.coverButton)
        addSubview(bgView)
        bgView.addSubview(header)
        header.addSubview(titleLabel)
        header.addSubview(closeButton)
        bgView.addSubview(self.collection)
        bgView.addSubview(self.indicator)
        
        bgView.bringSubviewToFront( self.indicator)
        
        self.snp.updateConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        self.coverButton.snp.updateConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        bgView.snp.updateConstraints { (maker) in
            maker.centerY.equalTo(self.snp.centerY)
            maker.centerX.equalTo(self.snp.centerX)
            maker.width.equalTo(popViewWidth)
            maker.height.equalTo(popViewHeight)
        }
        
        self.indicator.snp.updateConstraints { (maker) in
            maker.bottom.equalTo(-40)
            maker.leading.trailing.equalTo(0)
            maker.height.equalTo(30)
        }
        
        header.snp.updateConstraints { (maker) in
            maker.top.leading.trailing.equalTo(0)
            maker.height.equalTo(40)
        }
        
        titleLabel.snp.updateConstraints { (maker) in
            maker.centerY.equalTo(header.snp.centerY)
            maker.centerX.equalTo(header.snp.centerX)
        }
        
        closeButton.snp.updateConstraints { (maker) in
            maker.centerY.equalTo(header.snp.centerY)
            maker.trailing.equalTo(-8)
            maker.width.height.equalTo(35)
        }
        
        self.collection.snp.updateConstraints { (maker) in
            maker.top.equalTo(header.snp.bottom).offset(0)
            maker.leading.trailing.bottom.equalTo(0)
        }
    }
    
    //MARK: - Events
    @objc private func closebgView() {
        self.hide()
    }
    
    //MARK: - Public
    func hide() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    func show() {
        if let window = UIApplication.shared.keyWindow {
            
            window.bringSubviewToFront( self)
            self.isHidden = false
        }
    }
    
}


//MARK: - UICollectionViewDelegate,UICollectionViewDataSource
extension PagePicturePop:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELLIDENTIFIER, for: indexPath) as? PictureCollectionCell else {
            fatalError("dequeueReusableCell \(CELLIDENTIFIER) failure")
        }
        
         cell.configWebview(with: urls[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urls.count
    }
}

class PictureCollectionCell: UICollectionViewCell {
    var updateHeightHandler:((_ height:CGFloat) -> Void)?
    
    private var popViewWidth:CGFloat = screenWidth - 80
    private var popViewHeight:CGFloat = screenHeight - 130
    
    private var webView:WKWebView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configWebview(with url:String?) {
        if let urlString = url, let urlP = URL.init(string: urlString) {
            self.webView?.load(URLRequest.init(url: urlP))
        }else {
            self.webView?.reload()
        }
    }
    
    private func setupUI() {
        
        let webConfiguration = WKWebViewConfiguration()
        self.webView = WKWebView(frame: .zero, configuration: webConfiguration)
        self.webView?.sizeThatFits(CGSize.zero)
        self.webView?.scrollView.contentInset =  UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        addSubview(self.webView!)
        
        self.webView!.snp.makeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
    }
    
    ///传入image最终的 width，返回原本image比例的图片
    private func scaleImage(finalWidth:CGFloat,imageP:UIImage) -> UIImage {
        let scaleW = finalWidth / imageP.size.width
        let finalHeight = scaleW * imageP.size.height
        let finalImage = imageP.reSizeImage(reSize: CGSize.init(width: finalWidth, height: finalHeight))
        return finalImage
    }
    
    ///获取image尺寸适配后的高度
    private func getScaleImgaeHeight(finalWidth:CGFloat,imageP:UIImage) -> CGFloat {
        let scaleW = finalWidth / imageP.size.width
        let finalHeight = scaleW * imageP.size.height
        if finalHeight >= popViewHeight {
            return popViewHeight
        }else {
            if finalHeight <= popViewHeight * 0.5 {
                return popViewHeight * 0.5
            }else if finalHeight <= popViewHeight * 0.75{
                return popViewHeight * 0.75
            }else {
                return finalHeight
            }
        }
    }
}

extension PagePicturePop: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        // 随着滑动改变pageControl的状态
        self.indicator.currentPage = Int(offset.x / self.popViewWidth)
    }
}

