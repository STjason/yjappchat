//
//  NewUserListCell.swift
//  gameplay
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class NewUserListCell: UITableViewCell {
    
    var teamOVerView:((Int) -> Void)?
    var accountChange:((Int) -> Void)?
    var rebateSetup:((Int) -> Void)?
    var userDetail:((Int) -> Void)?
    var memberTransformProxy:((Int) -> Void)?
    
    private var isMember = false
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var fourthButton: UIButton!
    @IBOutlet weak var rateBackConsH: NSLayoutConstraint!
    
    @IBOutlet weak var itemWidthConstraint: NSLayoutConstraint!
    var index = 0
    @IBOutlet weak var typeAndName: UILabel!
    @IBOutlet weak var balabceTV: UILabel!
    @IBOutlet weak var rebateTV: UILabel!
    @IBOutlet weak var statusTV: UILabel!
    
    @IBAction func teamOVerViewAction() {
        if isMember && getSwitch_member_to_proxy() {
            accountChange?(index)
        }else {
            teamOVerView?(index)
        }
    }
    
    @IBAction func accountChangeAction() {
        if isMember && getSwitch_member_to_proxy() {
            rebateSetup?(index)
        }else {
            accountChange?(index)
        }
    }
    
    @IBAction func rebateSetupAction() {
        if isMember && getSwitch_member_to_proxy() {
            userDetail?(index)
        }else {
            rebateSetup?(index)
        }
    }
    
    @IBAction func userDetailAction() {
        if isMember && getSwitch_member_to_proxy() {
            memberTransformProxy?(index)
        }else {
            userDetail?(index)
        }
    }
    
    func setModel(model:UserListSmallBean?){
        if let bean = model {
            typeAndName.text = String.init(format:"%@•%@",getUserType(t: bean.type),bean.username)
            balabceTV.text = String.init(format: "%.2f元", bean.money)
            rebateTV.text = String.init(format: "%.2f%@", bean.kickback,"‰")
            statusTV.text = bean.status == 1 ? "关闭" : "开启"
            
            if getUserType(t: bean.type) == "会员"
            {
                isMember = true
                
                /* 会员情况下，没有 ‘会员转成代理情况’ */
                if getSwitch_member_to_proxy() {
                    itemWidthConstraint.constant = (screenWidth - 20) * 0.25
                    setupBottomTitle()
                }else {
                  itemWidthConstraint.constant = (screenWidth - 20) * (1 / 3)
                  setupBottomNormalTitle()
                }
            }else if getUserType(t: bean.type) == "代理"
            {
                isMember = false
                itemWidthConstraint.constant = (screenWidth - 20) * 0.25
                setupBottomNormalTitle()
            }
        }
    }
    
    private func setupBottomTitle() {
        firstButton.setTitle("账变记录", for: .normal)
        secondButton.setTitle("返点设定", for: .normal)
        thirdButton.setTitle("用户详情", for: .normal)
        fourthButton.setTitle("会员转成代理", for: .normal)
    }
    
    private func setupBottomNormalTitle() {
        firstButton.setTitle("团队总览", for: .normal)
        secondButton.setTitle("账变记录", for: .normal)
        thirdButton.setTitle("返点设定", for: .normal)
        fourthButton.setTitle("用户详情", for: .normal)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
