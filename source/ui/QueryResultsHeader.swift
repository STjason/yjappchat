//
//  QueryResultsHeader.swift
//  gameplay
//
//  Created by admin on 2019/3/30.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class QueryResultsHeader: UIView {

    @IBOutlet weak var betMoneyLabel: UILabel!
    
    @IBOutlet weak var winMoneyLabel: UILabel!
    
    @IBOutlet weak var profitLabel: UILabel!
    
    func configWith(betMoney:String,winMoney:String,profit:String) {
        betMoneyLabel.text = betMoney
        winMoneyLabel.text = winMoney
        profitLabel.text = profit
    }
}
