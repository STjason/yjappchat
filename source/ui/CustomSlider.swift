//
//  CustomSlider.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/10.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

protocol SeekbarChangeEvent {
    func onSeekbarEvent(currentRate:Float,currentWin:Float,progressRate:Float,reload:Bool)
}

class CustomSlider: UISlider {
    
    var currentOdds:Float = 0;//当前奖金或賠率
    var currentRateback:Float = 0;//当前返水比例
    var maxRakeback:Float = 0;//最大返水
    var maxOdds:Float = 0;//最大奖金或賠率
    var minOdds:Float = 0;//最小獎金或賠率
    var isOfficialVersion = true;//是否奖金版本
    var delegate:SeekbarChangeEvent?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
    }
    
    func setupLogic(odd:PeilvWebResult?,resetValue:Bool = true,resetSlider:Bool = false){
        if odd == nil{
            return
        }
        self.maxRakeback = (odd?.rakeback)!
        if maxRakeback <= 0 {
            self.isHidden = true
        }else{
            self.isHidden = false
        }
        
        if !switchShowBetSliderBar() {
            self.isHidden = true
        }
        
        self.maxOdds = (odd?.maxOdds)!
        self.minOdds = (odd?.minOdds)!
        self.currentOdds = maxOdds
        
        if let callback = delegate{
            let progressRateP:Float = resetSlider ? 0 : (1 - self.value)
            //
            self.currentOdds = maxOdds - progressRateP * abs(maxOdds - minOdds)
            self.currentRateback = abs(progressRateP * maxRakeback)
            callback.onSeekbarEvent(currentRate: currentRateback, currentWin: currentOdds,
                                    progressRate: progressRateP,reload: false)
        }
        
    }
    
    func setupLogic(maxRakeback:Float,maxOdds:Float,minOdds:Float,resetSlider:Bool = false){
        if maxRakeback <= 0 {
            self.isHidden = true
        }else{
            self.isHidden = false
        }
        self.maxRakeback = maxRakeback
        self.maxOdds = maxOdds
        self.minOdds = minOdds
        self.currentOdds = maxOdds

        if let callback = delegate{
            let progressRateP:Float = resetSlider ? 0 : (1 - self.value)
            self.currentRateback = abs(progressRateP * maxRakeback)
            callback.onSeekbarEvent(currentRate: currentRateback, currentWin: currentOdds, progressRate: progressRateP,reload: false)
        }
        
    }
    
    func changeSeekbar(currentProgress:Float,reload:Bool=false){
        let progressRate = 1 - currentProgress
        self.currentRateback = abs(progressRate * maxRakeback)
        self.currentOdds = maxOdds - progressRate * abs(maxOdds - minOdds)
        if self.currentOdds < 0 {
            self.currentOdds = 0
        }

        if let callback = delegate{
            callback.onSeekbarEvent(currentRate: currentRateback, currentWin: currentOdds, progressRate: progressRate,reload: reload)
        }
    }
    
    
    /**
     fixRate:固定返水步长
     currentProgress:当前进度
     add:是否加进度
     reload:是否刷新界面
     **/
    func changeSeekbarStepFixRate(fixRate:Float,currentProgress:Float,add:Bool,reload:Bool=false){
        
        if fixRate > maxRakeback{
            changeSeekbar(currentProgress: currentProgress)
            return
        }
        if fixRate <= 0{
            changeSeekbar(currentProgress: currentProgress)
            return
        }
        if maxRakeback == 0{
            return
        }
        let stepProgress:Float = (fixRate)/maxRakeback
        if add {
            self.value = self.value + stepProgress
            if self.value > 1{
                self.value = 1
            }
        }else{
            self.value = self.value - stepProgress
            if self.value < 0{
                self.value = 0
            }
        }
        changeSeekbar(currentProgress: self.value,reload: reload)
    }
    
    

}
