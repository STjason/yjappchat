//
//  FeeConvertRecordCell.swift
//  gameplay
//
//  Created by admin on 2019/1/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class FeeConvertRecordCell: UITableViewCell {

    @IBOutlet weak var gameTypeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var detailButton: UIButton!
    
    @IBOutlet weak var numConsLabel: UILabel!
    @IBOutlet weak var transformTypeLabel: UILabel!
    @IBOutlet weak var transformTimeLabel: UILabel!
    
    let status = ["全部状态","转账失败","转账成功","处理中"]
    let transformMethod = ["全部转入方式","从系统转入三方","从三方转入系统"]
    let thirdPartyType = ["三方类型","AG","BBIN","MG","QT","ALLBET","PT","OG","DS","NB","SKYWIND","KY"]
    
    func configWith(model:FeeConvertRecordRowModel) {
        
        let cellModel = formatModel(from: model)
        gameTypeLabel.text = model.username
        amountLabel.text = cellModel.amount
        statusLabel.text = cellModel.status

        numConsLabel.text = cellModel.orderId
        transformTypeLabel.text = cellModel.transformType
        transformTimeLabel.text = cellModel.transformTime
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
    private func formatModel(from model:FeeConvertRecordRowModel) -> FeeConvertCellRecord {
        let cellModel = FeeConvertCellRecord()
        cellModel.status = getStatustitle(with: model.status)
        cellModel.gameType = getThirdPartyType(with: model.platform)
        cellModel.transformType = getTransformMethodTitle(With: model.type)
        cellModel.amount = model.money
        
        if model.createDatetime != 0 {
            cellModel.transformTime = timeStampToString(timeStamp: model.createDatetime)
        }
        
        cellModel.orderId = model.orderId
        
        return cellModel
    }
    
    //MARK:根据code获得title
    func getTransformMethodTitle(With code:String) -> String {
        if code == "0" {
            return transformMethod[0]
        }else if code == "1" {
            return transformMethod[2]
        }else if code == "2" {
            return transformMethod[1]
        }else {
            return ""
        }
    }
    
    func getStatustitle(with code:String) -> String {
        if let codeNum = Int(code) {
            return status[codeNum]
        }
        return ""
    }
    //平台简称
    func getThirdPartyType(with code:String) -> String {
        if let codeNum = Int(code) {
            if codeNum == 20{
                return "BG"
            }else if codeNum == 21{
                return "DG"
            }else if codeNum == 22{
                return "RG"
            }else if codeNum == 24 {
                return "CQ9"
            }else if codeNum == 25 {
                return "Ebet"
            }else if codeNum == 26 {
                return "Leyou"
            }else if codeNum == 27 {
                return "YG王者"
            }else if codeNum == 34{
                return "YBQP"
            }else{
                if codeNum < thirdPartyType.count{
                    return thirdPartyType[codeNum]
                }
                
            }
        }
        return ""
    }
}

class FeeConvertCellRecord {
    var gameType = ""
    var amount = ""
    var status = ""
    var orderId = ""
    var transformType = ""
    var transformTime = ""
}
