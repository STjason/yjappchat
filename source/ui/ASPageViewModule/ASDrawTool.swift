//
//  ASDrawTool.swift
//  gameplay
//
//  Created by admin on 2018/11/5.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class ASDrawTool {
    //MARK: 画 N条横线 ,间距为 40,第一条线的原点是 originalPoint
    static func drawHoriLines(count: Int,gap:CGFloat,originalPoints:(CGPoint,CGPoint),view:UIView,lineColor:UIColor)
    {
        for index in 0..<count
        {
            let pointStart = CGPoint.init(x: originalPoints.0.x , y: originalPoints.0.y + CGFloat(index) * gap)
            let pointEnd = CGPoint.init(x: originalPoints.1.x , y: originalPoints.1.y + CGFloat(index) * gap)
            drawTable(points: (pointStart,pointEnd),view:view,lineColor:lineColor)
        }
    }
    
    //MARK: 画 N条 竖线
    static func drawVerticalLines(count: Int,gap:CGFloat,originalPoints:(CGPoint,CGPoint),view:UIView,lineColor:UIColor)
    {
        for index in 0..<count
        {
            let pointStart = CGPoint.init(x: originalPoints.0.x + CGFloat(index) * gap, y: originalPoints.0.y)
            let pointEnd = CGPoint.init(x: originalPoints.1.x + CGFloat(index) * gap, y: originalPoints.1.y)
            drawTable(points: (pointStart,pointEnd),view:view,lineColor:lineColor)
        }
    }
    
    //绘制 line
    private static func drawTable(points:(CGPoint,CGPoint),view:UIView,lineColor:UIColor)
    {
        
        let lineWidth: CGFloat = 0.5
        
        let path = UIBezierPath()
        let pathLayer = CAShapeLayer()
        
        path.move(to: points.0)
        path.addLine(to: points.1)
        path.close()
        
        pathLayer.path = path.cgPath
        pathLayer.strokeColor = lineColor.cgColor
        pathLayer.lineWidth = lineWidth
        pathLayer.fillColor = UIColor.clear.cgColor
        view.layer.addSublayer(pathLayer)
    }
}
