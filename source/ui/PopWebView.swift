//
//  PopWebView.swift
//  gameplay
//
//  Created by admin on 2019/2/20.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import WebKit

class PopWebView: UIView {
    var popView = UIView()
    var webView = WKWebView()
    
    var webViewH:CGFloat = 0 {
        didSet {
            var popFrame = popView.frame
            var webFrame = webView.frame
            
            if popFrame.height + webViewH > screenHeight - 160 {
                webViewH = screenHeight - 160 - popFrame.height
            }
            
            popFrame = CGRect.init(x: popFrame.origin.x, y: popFrame.origin.y, width: popFrame.width, height: (popFrame.height + webViewH))
            webFrame = CGRect.init(x: webFrame.origin.x, y: webFrame.origin.y, width: webFrame.width, height: webViewH)
            
            popView.frame = popFrame
            webView.frame = webFrame
            
            popView.centerY = screenHeight * 0.5
        }
    }
    
    convenience init(frame:CGRect,contents:String,isURL:Bool,title:String) {
        self.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight))
        self.configUI(frame: frame, isURL:isURL,contents:contents, title: title)
    }
    
    private func configUI(frame:CGRect,isURL:Bool,contents:String, title:String = "活动详情") {
        
        if isEmptyString(str: contents) {
            return
        }
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(closeButtonClick))
        self.addGestureRecognizer(tapGesture)
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        popView.layer.cornerRadius = 3.0
        popView.layer.masksToBounds = true
        self.addSubview(popView)
        popView.addSubview(webView)
        
        webView.navigationDelegate = self
        webView.sizeThatFits(CGSize.zero)
        if isURL {
            if let url = URL.init(string: contents) {
                webView.load(URLRequest.init(url: url))
            }
        }else {
            var webContents = ""
            if contents.contains("<img src") {
                webContents = contents
            }else {
                webContents = contents + "<meta name=" + "\"viewport\"" + " " + "content=\"width=device-width," + " " + "initial-scale=1.0," + " " + "shrink-to-fit=no\">"
            }
            
            webView.loadHTMLString(webContents, baseURL: URL.init(string: BASE_URL))
        }
        
        let header = UIView()
        popView.addSubview(header)
        header.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: 50)
        header.theme_backgroundColor = "Global.themeColor"
        
        let closeButton = UIButton()
        header.addSubview(closeButton)
        let closeBtnW:CGFloat = 25
        closeButton.frame = CGRect.init(x: frame.width - closeBtnW - 8, y: (header.height - closeBtnW)*0.5, width: closeBtnW, height: closeBtnW)
        closeButton.setImage(UIImage(named: "closeButtonImg"), for: .normal)
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.addTarget(self, action: #selector(closeButtonClick), for: .touchUpInside)
        
        let titleLabel = UILabel()
        titleLabel.backgroundColor = UIColor.clear
        header.addSubview(titleLabel)
        titleLabel.frame = CGRect.init(x: closeBtnW + 8, y: 0, width: header.width - 2*(closeBtnW + 8), height: 50)
        titleLabel.text = title
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 17.0)
        
        popView.frame = CGRect.init(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: header.height)
        webView.frame = CGRect.init(x: 0, y: header.y + header.height, width: frame.width, height: 0)
    }
    
    @objc private func closeButtonClick() {
        dismiss()
    }
    
    private func dismiss() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.alpha = 0
        }, completion: { (finish) -> Void in
            if finish {
                self.removeFromSuperview()
            }
        })
    }
    
    func show(superView:UIView?) {
        self.alpha = 0
        
        if let view = superView {
            view.addSubview(self)
        }else {
            let wind = UIApplication.shared.keyWindow
            wind?.addSubview(self)
        }
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.alpha = 1
        })
    }
}

extension PopWebView:WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                webView.evaluateJavaScript("document.documentElement.offsetHeight", completionHandler: { (height, error) in
                    if let webViewHeight = height as? CGFloat{
                        if self.webViewH == 0 {
                            self.webViewH = webViewHeight
                        }
                    }
                })
            }
            
        })
    }
}
