//
//  MainHeaderView.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/3/24.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
//主界面固定视图，轮播，功能区，公告

protocol MainViewDelegate {
    func clickFunc(tag:Int)
}
class MainHeaderView: UIView ,MarqueeDelegate{

    @IBOutlet weak var firstConsMW: NSLayoutConstraint!
    @IBOutlet weak var secondConsMW: NSLayoutConstraint!
    @IBOutlet weak var thirdConsMW: NSLayoutConstraint!
    @IBOutlet weak var fourthConsMW: NSLayoutConstraint!
    @IBOutlet weak var fifthConsMW: NSLayoutConstraint!
    
    @IBOutlet weak var noticeUIHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var speakerIcon: UIImageView!
    @IBOutlet weak var notiUIBgView: UIView!
    @IBOutlet weak var activeTxt:UILabel!
    
    @IBOutlet weak var fifthTitleLabel: UILabel!
    @IBOutlet weak var fourthTitleLabel: UILabel!
    @IBOutlet weak var secondTitleLabel: UILabel!
    @IBOutlet weak var firstTitleLabel: UILabel!
    @IBOutlet weak var onlineSeviceImage: UIImageView!
    @IBOutlet weak var activeImg: UIImageView!
    @IBOutlet weak var betRecordImage: UIImageView!
    @IBOutlet weak var inOutImage: UIImageView!
    @IBOutlet weak var fifthImage: UIImageView!
    
    @IBOutlet weak var cycleScrollView: WRCycleScrollView!
    //存取款，投注记录，优惠活动，在线客服等按钮
    @IBOutlet weak var helpUI:UIView!
    @IBOutlet weak var withdrawBtn: UIView!
    @IBOutlet weak var recordsBtn: UIView!
    @IBOutlet weak var activeBtn: UIView!
    @IBOutlet weak var activeBadge:UIButton!
    @IBOutlet weak var serviceBtn: UIView!
    @IBOutlet weak var noticeUI:MarqueeView!
    @IBOutlet weak var noticeMoreBtn:UIButton!
    @IBOutlet weak var fifthView: UIView!
    
    var lunboImgs:[String] = [];//轮播图片数据集
    var lunboItems:[LunboResult] = []
    
    var mainDelegate:MainViewDelegate?
    var controller:BaseController!
    var notices:[NoticeBean] = []
    

    override func awakeFromNib() {
        
        setupNoPictureAlphaBgView(view: withdrawBtn)
        setupNoPictureAlphaBgView(view: recordsBtn)
        setupNoPictureAlphaBgView(view: activeBtn)
        setupNoPictureAlphaBgView(view: serviceBtn)
        setupNoPictureAlphaBgView(view: notiUIBgView)
        setupNoPictureAlphaBgView(view: fifthView)
        
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: firstTitleLabel)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: secondTitleLabel)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: activeTxt)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: fourthTitleLabel)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: fifthTitleLabel)
        
        //给界面上各按钮添加手势事件
        withdrawBtn.isUserInteractionEnabled = true
        recordsBtn.isUserInteractionEnabled = true
        activeBtn.isUserInteractionEnabled = true
        serviceBtn.isUserInteractionEnabled = true
        fifthView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(_:)))
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(_:)))
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(_:)))
        let tap4 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(_:)))
        let tap5 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(_:)))
        withdrawBtn.addGestureRecognizer(tap)
        recordsBtn.addGestureRecognizer(tap2)
        activeBtn.addGestureRecognizer(tap3)
        serviceBtn.addGestureRecognizer(tap4)
        fifthView.addGestureRecognizer(tap5)
        noticeMoreBtn.addTarget(self, action: #selector(onMoreNoticeClick), for: .touchUpInside)
        noticeUI.delegate = self
        
        chooseMainPageVersion()
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if shouldShowNoticeWIndow() {}
        }
    }
    
    func updateFunctionButtonsForVersion1(){
        if !getSwitch_lottery() {
            let offset:CGFloat = -screenWidth * 0.25
            firstConsMW.constant = offset + screenWidth * 0.3333
            secondConsMW.constant = offset
            thirdConsMW.constant = offset + screenWidth * 0.3333
            fourthConsMW.constant = offset + screenWidth * 0.3333
        }
    }
    
    func updateFunctionButtonsForVersion9(){
        
        let offset:CGFloat = -screenWidth * 0.25
        if !getSwitch_lottery() {
            if switchMoney_income() {
                firstConsMW.constant = offset + screenWidth * 0.25
                secondConsMW.constant = offset + screenWidth * 0.25
                thirdConsMW.constant = offset + screenWidth * 0.25
                fourthConsMW.constant = offset
                fifthConsMW.constant = offset + screenWidth * 0.25
            }else {
                firstConsMW.constant = offset + screenWidth * 0.333
                secondConsMW.constant = offset
                thirdConsMW.constant = offset + screenWidth * 0.333
                fourthConsMW.constant = offset
                fifthConsMW.constant = offset + screenWidth * 0.333
            }
            
        }else {
            if switchMoney_income() {
                firstConsMW.constant = offset + screenWidth * 0.2
                secondConsMW.constant = offset + screenWidth * 0.2
                thirdConsMW.constant = offset + screenWidth * 0.2
                fourthConsMW.constant = offset + screenWidth * 0.2
                fifthConsMW.constant = offset + screenWidth * 0.2
            }else {
                firstConsMW.constant = offset + screenWidth * 0.25
                secondConsMW.constant = offset
                thirdConsMW.constant = offset + screenWidth * 0.25
                fourthConsMW.constant = offset + screenWidth * 0.25
                fifthConsMW.constant = offset + screenWidth * 0.25
            }
        }
    }
    
    private func chooseMainPageVersion() {
       let str = switchMainPageVersion()
        if str == "V1" {
            version1()
        }else if str == "V2" {
            version2()
        }else if str == "V3" {
            version3()
        }else if str == "V4"
        {
            version4()
        }else if str == "V5"
        {
            version5()
        }else if str == "V6"
        {
            version6()
        }else if str == "V7" {
            version7()
        }else if str == "V8" {
            version8()
        }else if str == "V9" {
            version9()
        }else if str == "V10"{
            version10()
        }else if str == "V11"{
            version2()
        }else if str == "V12"{
            version1()
        }
    }
    
    private func version1() {
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        betRecordImage.theme_image = "HomePage.betRecord"
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        secondTitleLabel.text = "投注记录"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"
        
        updateFunctionButtonsForVersion1()
    }
    
    
    private func version2() {
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        betRecordImage.theme_image = "HomePage.withdrawal"
        inOutImage.theme_image = "HomePage.homeDeposit"
        activeImg.theme_image = "TabBar.discount"
        
        firstTitleLabel.text = "存款"
        secondTitleLabel.text = "取款"
        activeTxt.text = "优惠活动"
        fourthTitleLabel.text = "在线客服"
    }
    
    private func version3() {
        
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"
        
        if let sys = getSystemConfigFromJson() {
            if sys.content != nil{
                let lottery_dynamic_odds = sys.content.lottery_dynamic_odds
                if isEmptyString(str: lottery_dynamic_odds) || lottery_dynamic_odds == "on"{
                    betRecordImage.theme_image = "HomePage.tuiguanglianjie"
                    secondTitleLabel.text = "推广链接"
                    return
                }
            }
        }
        
        betRecordImage.theme_image = "HomePage.wodetuijian"
        secondTitleLabel.text = "我的推荐"
    }
    
    private func version4() {
        
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        betRecordImage.theme_image = "HomePage.withdrawal"
        inOutImage.theme_image = "HomePage.homeDeposit"
        activeImg.theme_image = "HomePage.exchangeMoney"
        
        firstTitleLabel.text = "存款"
        secondTitleLabel.text = "取款"
        activeTxt.text = "额度转换"
        fourthTitleLabel.text = "在线客服"
    }
    
    private func version5() {
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"
        
        
        betRecordImage.theme_image = ThemeImagePicker.init(keyPath:"HomePage.Balanceinterest")
        secondTitleLabel.text = "余额生金"
    }
    
    private func version6() {
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"

        betRecordImage.theme_image = ThemeImagePicker.init(keyPath:"HomePage.betRecord")
        secondTitleLabel.text = "聊天室"
    }
    
    private func version7() {
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        betRecordImage.theme_image = "HomePage.betRecord"  // 免费试玩
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        secondTitleLabel.text = "免费试玩"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"
    }
    
    private func version8() {
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        betRecordImage.theme_image = "PopView.popup_开奖网"  // 开奖网
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        secondTitleLabel.text = "开奖网"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"
        
        if !getSwitch_lottery() {
            let offset:CGFloat = -screenWidth * 0.25
            firstConsMW.constant = offset + screenWidth * 0.3333
            secondConsMW.constant = offset
            thirdConsMW.constant = offset + screenWidth * 0.3333
            fourthConsMW.constant = offset + screenWidth * 0.3333
        }
    }
    
    private func version9() {
        
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        betRecordImage.theme_image = "HomePage.Balanceinterest"
        activeImg.theme_image = "HomePage.download"
        onlineSeviceImage.theme_image = "PopView.popup_开奖网" //图标替换开奖网图标
        fifthImage.theme_image = "HomePage.onelineService"
        
        firstTitleLabel.text = "存取款"
        secondTitleLabel.text = "余额生金"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "开奖网"
        fifthTitleLabel.text = "在线客服"
        
        updateFunctionButtonsForVersion9()
    }
    
    private func version10(){
        onlineSeviceImage.theme_image = "HomePage.onelineService"
        betRecordImage.theme_image = "HomePage.activeHall"  // 开奖网
        inOutImage.theme_image = "HomePage.homeDepositAndWithDraw"
        activeImg.theme_image = "HomePage.download"
        
        firstTitleLabel.text = "存取款"
        secondTitleLabel.text = "活动大厅"
        activeTxt.text = "APP下载"
        fourthTitleLabel.text = "在线客服"
        
        if !getSwitch_lottery() {
            let offset:CGFloat = -screenWidth * 0.25
            firstConsMW.constant = offset + screenWidth * 0.3333
            secondConsMW.constant = offset
            thirdConsMW.constant = offset + screenWidth * 0.3333
            fourthConsMW.constant = offset + screenWidth * 0.3333
        }
    }
    
    @objc func onMoreNoticeClick() -> Void {
        print("click notice")
        if self.notices.isEmpty{
            return
        }
        if let delegate = self.mainDelegate{
            openNoticePage(controller: delegate as! BaseController, notices: self.notices)
        }
    }
    
    func syncHeaderFromWeb(controller:BaseController) -> Void {
        self.controller = controller
        self.syncSomeWebData(controller: controller)
    }

    func updateFuncBtn() -> Void {
//        if isActiveShowFunc(){
//            activeTxt.text = "优惠活动"
//            activeImg.image = UIImage.init(named: "app_active_icon")
//        }else{
            activeTxt.text = "APP下载"
//            activeImg.image = UIImage.init(named: "app_download_icon")
        activeImg.theme_image = "HomePage.download"
//        }
        //获取一些主页需要的web数据
    }
    
    @objc func tapEvent(_ recongnizer: UIPanGestureRecognizer) {
        let tag = recongnizer.view!.tag
        if let delegate = self.mainDelegate{
            delegate.clickFunc(tag: tag)
        }
    }
    
    func syncSomeWebData(controller:BaseController) ->  Void {
        //最新公告
        // 类型 1:关于我们,2:取款帮助,3:存款帮助,4:合作伙伴->联盟方案,5:合作伙伴->联盟协议,
        //6:联系我们,7:常见问题 ,8:玩法介绍,9:彩票公告,10:视讯公告,11:体育公告,12:电子公告,13:最新公告
        controller.request(frontDialog: false, url:NOTICE_URL,params:["code":13],
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            if let result = NoticeWraper.deserialize(from: resultJson){
                                if result.success{
                                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                                    self.notices = result.content
                                    if !result.content.isEmpty{
                                        self.updateNotice(str: result.content)
                                    }else {
                                        self.setupNoticeUIWithNotices(notices: "")
                                    }
                                }
                            }
        })
        //获取轮播
        controller.request(frontDialog: false, url:LUNBO_URL,params:["code":5],
                           callback: {[weak self](resultJson:String,resultStatus:Bool)->Void in
                            guard let weakSelf = self else{return}
                            if !resultStatus {
                                return
                            }
                            if let result = LunboWraper.deserialize(from: resultJson){
                                if result.success{
                                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                                    if let lunbo = result.content{
                                        //更新轮播图
                                        weakSelf.updateLunbo(lunbo:lunbo);
                                    }
                                }
                            }
        })
        
        showAnnounce(controller:controller)
        checkVersion(controller: controller, showDialog: false, showText: "正在检测新版本...",showError: false, isUpdate: false, isSuperSign: false)
    }
    
    private func showAnnounce(controller:BaseController) {
        if YiboPreference.isShouldAlert_isAll() == "" {
            YiboPreference.setAlert_isAll(value: "on" as AnyObject)
        }
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if !shouldShowNoticeWIndow() {
                return
            }
        }
        //获取公告弹窗内容
        controller.request(frontDialog: false, url:ACQURIE_NOTICE_POP_URL,params:["code":19],
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            
                            if let result = NoticeResultWraper.deserialize(from: resultJson){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                PopupAlertListView(resultJson: resultJson,controller:UIViewController(),not:"MainHeaderView_PopupAlertView",anObject:self,from: 0)
                            }
        })
    }
    
    // 遍历数组,取出公告赋值
    private func forArrToAlert(notices: Array<NoticeResult>) {
        
        var noticesP = notices
        noticesP = noticesP.sorted { (noticesP1, noticesP2) -> Bool in
            return noticesP1.sortNum < noticesP2.sortNum
        }
        

        var models = [NoticeResult]()
        for index in 0..<noticesP.count {
            let model = noticesP[index]
            if model.isIndex {
                models.append(model)
            }
        }
        
        if models.count > 0 {
            let weblistView = WebviewList.init(noticeResuls: models)
            weblistView.show()
        }
    }
    
    func updateLunbo(lunbo:[LunboResult]) -> Void {
        if lunbo.isEmpty{
            return
        }
        self.lunboImgs.removeAll()// remove all banner images first
        self.lunboItems.removeAll()
        for item in lunbo{
            if !isEmptyString(str: item.titleImg) && (item.titleImg.contains("http://")||item.titleImg.contains("https://")){
                
                var logoImg = item.titleImg
                if logoImg.contains("\t"){
                    let strs = logoImg.components(separatedBy: "\t")
                    if strs.count >= 2{
                        logoImg = strs[1]
                    }
                }
                
                logoImg = logoImg.trimmingCharacters(in: .whitespaces)
                if let status = item.status {
                    if status == 2 {
                        self.lunboImgs.append(logoImg)
                        self.lunboItems.append(item)
                    }
                }
                print("lundo ===",logoImg)
            }
        }
        
        let frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 180)
        
        if lunboImgs.count <= 1{
            //改变 isAutoScroll 为false，就crash，封装方法中调用先后顺序有问题，很不好改
            cycleScrollView = WRCycleScrollView.init(frame: frame, type: .SERVER, imgs: lunboImgs, descs: nil, defaultDotImage: nil, currentDotImage: nil,shouldScroll:false)
        }else{
            cycleScrollView = WRCycleScrollView.init(frame: frame, type: .SERVER, imgs: lunboImgs, descs: nil, defaultDotImage: nil, currentDotImage: nil)
            cycleScrollView.autoScrollInterval = 3.0
        }
    
        cycleScrollView.delegate = self
        cycleScrollView.backgroundColor = UIColor.lightGray
        cycleScrollView.clipsToBounds = true
        cycleScrollView.descLabelHeight = 40
        cycleScrollView.descLabelFont = UIFont.systemFont(ofSize: 13)
        cycleScrollView.pageControlAliment = .CenterBottom
        cycleScrollView.bottomViewBackgroundColor = UIColor.lightGray
        cycleScrollView.removeFromSuperview()
        self.addSubview(cycleScrollView)
        
    }
    
    func updateNotice(str:[NoticeBean]) -> Void {
        var notices = ""
        for item in str{
            if let content = item.content{
                notices.append(content)
            }
        }
        
        noticeUI.setupView(title: notices,htmlTxt: true)
        self.setupNoticeUIWithNotices(notices: notices)
    }
    
    var didGetNotices:((Bool) -> Void)?
    
    // 布局有些杂乱，这里就不改变布局，其他控件直接隐藏了
    private func setupNoticeUIWithNotices(notices: String) {
        let hasNotices = !isEmptyString(str: notices)
        
        self.noticeUIHeightConstraint.constant = hasNotices ? 30 : 0
        self.speakerIcon.isHidden = !hasNotices
        self.notiUIBgView.isHidden = !hasNotices
        self.noticeMoreBtn.isHidden = !hasNotices
        
        didGetNotices?(hasNotices)
    }
    
    func onDelegate() {
        onMoreNoticeClick()
    }

}

extension MainHeaderView: WRCycleScrollViewDelegate{
    /// 点击图片事件
    func cycleScrollViewDidSelect(at index:Int, cycleScrollView:WRCycleScrollView){
        if let url = URL.init(string: lunboItems[index].titleUrl) {
            UIApplication.shared.openURL(url)
        }
    }
}

