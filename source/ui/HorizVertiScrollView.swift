//
//  HorizVertiScrollView.swift
//  gameplay
//
//  Created by admin on 2018/10/19.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class HorizVertiScrollView: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    var deleteItemHandler:((Int) -> Void)?
    var openURLHandler:(() -> Void)?
    var longGestureHandler:((UIImage?) -> Void)?
    
    private var isHorizontal = true
    private var datas = [WechatQRCodeModel]()
    
    lazy var xibView:UIView = {
        return Bundle.main.loadNibNamed("HorizVertiScrollView", owner: self, options: nil)?.first as! UIView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibView.frame = bounds
        addSubview(xibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(self.xibView)
        xibView.frame = self.bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pageControl.isHidden = true
    }
    
    func configWithModels(qrcodeImgs: [WechatQRCodeModel]?,isHorizontal:Bool) {
        self.isHorizontal = isHorizontal
        
        if let datasP = qrcodeImgs
        {
            if datasP.count > 0
            {
                datas = datasP
                datas = datas.sorted{(subDataP1,subDataP2) -> Bool in
                    return subDataP1.imgSort < subDataP2.imgSort
                }
                
                if isHorizontal
                {
                    datas.insert(datasP.last!, at: 0)
                    datas.append(datasP[0])
                    
                    pageControl.numberOfPages = datas.count - 2
                    pageControl.currentPage = 0
                }
                
                setupCollectionUI(isHorizontal: isHorizontal)
            }
        }
    }
    
    private func setupCollectionUI(isHorizontal:Bool) {
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UINib.init(nibName: "HorizVertiScrollViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "horizVertiScrollViewCell")
        
        setupCollectionLayoutWithType(isHorizontal: isHorizontal)
    }
    
    /** collectionView 是水平滚动，还是竖直滚动 */
    private func setupCollectionLayoutWithType(isHorizontal:Bool) {
        let layout = UICollectionViewFlowLayout.init()
        if isHorizontal
        {
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize.init(width: screenWidth - 15 * 2, height: 200)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            collectionView.collectionViewLayout = layout
            collectionView.isScrollEnabled = true
            collectionView.isPagingEnabled = true
            pageControl.isHidden = datas.count < 2 ? true: false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                self.collectionView.contentOffset = CGPoint.init(x: self.width, y: 0)
                if self.datas.count > 1
                {
                  self.collectionView.scrollToItem(at: IndexPath.init(row: 1, section: 0), at: .left, animated: false)
                }
            }
        }else
        {
            layout.scrollDirection = .vertical
            layout.itemSize = CGSize.init(width: screenWidth - 15 * 2, height: 200)
            layout.minimumLineSpacing = 20
            layout.minimumInteritemSpacing = 0
            collectionView.collectionViewLayout = layout
            collectionView.isScrollEnabled = false
            
            pageControl.isHidden = true
        }
    }
    
    //MARK: 删除逻辑
    private func deleteItemLogic(indexPath: IndexPath) {
        
        let index = indexPath.row
        
        if isHorizontal
        {
            datas.remove(at: index)
            datas.removeFirst()
            datas.removeLast()
            pageControl.numberOfPages = datas.count
            
            if datas.count > 0
            {
                datas.insert(datas.last!, at: 0)
                datas.append(datas[1])
            }
            
        }else
        {
            datas.remove(at: index)
        }
        
        deleteItemHandler?(datas.count)
        collectionView.reloadData()
    }
    
    private func tapCellImage(indexPath: IndexPath) {
        let model = datas[indexPath.row]
        
        if model.linkType == "3"
        {
            deleteItemLogic(indexPath: indexPath)
        }else
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.openURLHandler?()
            }
            
            if let URL = URL.init(string: model.linkUrl)
            {
                UIApplication.shared.openURL(URL)
            }
        }
    }
}

extension HorizVertiScrollView: UICollectionViewDelegate {
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        tapCellImage(indexPath: indexPath)
//    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if isHorizontal
        {
            var currentIndex = Int(collectionView.contentOffset.x / collectionView.width)
            if currentIndex == 0
            {
                currentIndex = datas.count - 1
                
                // currentIndex == 0，跳到实际图片 最后 1张
                let indexPath = IndexPath.init(row: datas.count - 2, section: 0)
                collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
            }else if currentIndex == datas.count - 1
            {
                currentIndex = 0
                
                // currentIndex == last,跳到实际图片 第 0张
                let indexPath = IndexPath.init(row: 1, section: 0)
                collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
            }else
            {
                
                currentIndex -= 1
            }
            
            pageControl.currentPage = Int(currentIndex)
        }
        
    }
}

extension HorizVertiScrollView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "horizVertiScrollViewCell", for: indexPath) as! HorizVertiScrollViewCell
        
        let data = datas[indexPath.row]
        cell.configWithModel(model: data,indexPath: indexPath)
        
        cell.longGestureHandler = {(img) -> Void in
            self.longGestureHandler?(img)
        }
        
        cell.closeActionHandler = {(idnexPath) -> Void in
            self.tapCellImage(indexPath: indexPath)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datas.count
    }
}

