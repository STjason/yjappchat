//
//  RatebackSetupCell.swift
//  gameplay
//
//  Created by admin on 2018/9/23.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class RatebackSetupCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var titleContents: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self)
    }
    
    func configWithContents(title: String,contents: String) {
        titleLabel.text = title
        titleContents.text = contents
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
