//
//  FeeConvertRecordFilter.swift
//  gameplay
//
//  Created by admin on 2019/1/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class FeeConvertRecordFilter: UIView {
    
    var isShow = false
    
    var startTimehandler:(() -> Void)?
    var endTimeHandler:(() -> Void)?
    var allStatusHandler:(() -> Void)?
    var transformHandler:(() -> Void)?
    var thirdTypeHandler:(() -> Void)?
    var cancelHandler:(() -> Void)?
    var confirmHandler:((_ startTime:String,_ endTime:String,_ status:String,_ transformType:String, _ thirdType:String) -> Void)?
    
    @IBOutlet weak var startTimeField: CustomFeildText!
    @IBOutlet weak var endTimeField: CustomFeildText!
    @IBOutlet weak var allStatusField: CustomFeildText!
    @IBOutlet weak var transformField: CustomFeildText!
    @IBOutlet weak var thirdPartyField: CustomFeildText!
    
    @IBAction func confirmAction(_ sender: UIButton) {
        let startTime = startTimeField.text ?? ""
        let endTime = endTimeField.text ?? ""
        let allStatus = allStatusField.text ?? ""
        let transform = transformField.text ?? ""
        let thirdParty = thirdPartyField.text ?? ""
        confirmHandler?(startTime, endTime, allStatus, transform, thirdParty)
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        cancelHandler?()
    }
    
    //MARK: -UI
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        startTimeField.delegate = self
        endTimeField.delegate = self
        allStatusField.delegate = self
        transformField.delegate = self
        thirdPartyField.delegate = self
        
        let tapStartTimeGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapStartTime))
        let tapEndTimeGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapEndTime))
        let tapAllStatusGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapAllStatus))
        let tapTransformGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapTransform))
        let tapThirdPartyGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapThirdParty))
        startTimeField.addGestureRecognizer(tapStartTimeGesture)
        endTimeField.addGestureRecognizer(tapEndTimeGesture)
        allStatusField.addGestureRecognizer(tapAllStatusGesture)
        transformField.addGestureRecognizer(tapTransformGesture)
        thirdPartyField.addGestureRecognizer(tapThirdPartyGesture)
    }
    
    //MARK: -Events
    @objc private func tapStartTime() {
        startTimehandler?()
    }
    
    @objc private func tapEndTime() {
        endTimeHandler?()
    }
    
    @objc private func tapAllStatus() {
        allStatusHandler?()
    }
    
    @objc private func tapTransform() {
        transformHandler?()
    }
    
    @objc private func tapThirdParty() {
        thirdTypeHandler?()
    }
}


//MARK: -<UITextFieldDelegate>
extension FeeConvertRecordFilter:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}










