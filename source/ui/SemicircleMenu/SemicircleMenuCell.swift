//
//  SemicircleMenuCellCollectionViewCell.swift
//  AWCollectionViewDialLayoutDemo
//
//  Created by Moayad on 5/29/16.
//  Copyright © 2016 Moayad. All rights reserved.
//

import UIKit

class SemicircleMenuCell: UICollectionViewCell {
    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var label:UILabel!
    
    @IBOutlet weak var rightIcon: UIImageView!
    
    @IBOutlet weak var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.label.textColor = UIColor.groupTableViewBackground
        self.rightLabel.textColor = UIColor.groupTableViewBackground
    }
    
    func configWithItem(titleAndImage:(String,String)) {
        self.label.text = titleAndImage.0
        self.rightLabel.text = titleAndImage.0
        
        let image = "SemiCircleMenu.\(titleAndImage.1)"
        self.icon.image = UIImage(named: titleAndImage.1)
        self.rightIcon.image = UIImage(named: titleAndImage.1)
        
//        self.icon.theme_image = ThemeImagePicker.init(keyPath: image)
        
//        self.rightIcon.theme_image = ThemeImagePicker.init(keyPath: image)
    }
    
}


