//
//  SemicircleChatMenuView.swift
//  gameplay
//
//  Created by admin on 2019/12/10.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class SemicircleChatMenuView: UIView,UICollectionViewDataSource, UICollectionViewDelegate {
    
    // 1:左边，2右边
    let CellTypeLeft = 1
    let CellTypeRight = 2
    var cellType = 0
    
    var hideSemicircleHandler:(() -> Void)? // 隐藏环形按钮触发
    
    var thumbnailCache = [String: UIImage]()
    var dialLayout:SemicircleMenuLayout!
    var cell_height:CGFloat!
    var collectionView:UICollectionView!
    var items: [(String,String)] = []
    var visualView: UIVisualEffectView!
    weak var holdVC:CRChatController!
    var allDatas = [LotteryData]()
    var realPerson:[LotteryData] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    
    private func setupView() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(clickSpaceView))
        tapGesture.cancelsTouchesInView = false
        self.addGestureRecognizer(tapGesture)
        
        setupBlurView()
        setupCollectionView()
    }
    
    @objc func clickSpaceView(sender: UITapGestureRecognizer){
        
        if let indexPath = self.collectionView?.indexPathForItem(at: sender.location(in: self.collectionView)) {
            //            let cell = self.collectionView?.cellForItem(at: indexPath)
            
            let name = self.items[indexPath.row].0
            openVCwithName(name: name)
            
        }
        
        self.isHidden = true
        
        hideSemicircleHandler?()
    }
    
    private func setupBlurView() {
        let blurEffectView = UIBlurEffect.init(style: .dark)
        visualView = UIVisualEffectView.init(effect: blurEffectView)
        let frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        visualView.frame = frame
        self.addSubview(visualView)
    }
    
    func setupCollectionView(){
        
        dialLayout = SemicircleMenuLayout()
        dialLayout.cellSize = CGSize(width: 180, height: 50)
        dialLayout.shouldFlip = true
        dialLayout.dialRadius = 130
        dialLayout.angularSpacing = 23
        dialLayout.xOffset = 130
        dialLayout.itemHeight = 120
        
        let collectoinFrame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        collectionView = UICollectionView.init(frame: collectoinFrame, collectionViewLayout: dialLayout)
        collectionView.backgroundColor = UIColor.white.withAlphaComponent(0)
        self.addSubview(collectionView)
        
        let cellNib = UINib.init(nibName: "SemicircleMenuCell", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "semicircleMenuCell")
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.hideItemTitle()
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SemicircleMenuCell!
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "semicircleMenuCell", for: indexPath) as! SemicircleMenuCell
        
        cell.rightIcon.isHidden = cellType == CellTypeLeft
        cell.rightLabel.isHidden = cellType == CellTypeLeft
        cell.icon.isHidden = cellType == CellTypeRight
        cell.label.isHidden = cellType == CellTypeRight
        
        let item = self.items[indexPath.item]
        cell.configWithItem(titleAndImage: item)
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hideItemTitle()
    }
    
    func hideItemTitle() {
        let cells = self.collectionView.visibleCells
        for (_, cell) in cells.enumerated()
        {
            guard let cellP = cell as? SemicircleMenuCell else
            {
                fatalError("fatalError log 'dequeueReusableCell as? CircularMenuCell failure'")
            }
            
            cellP.label.isHidden = (cellP.frame.origin.x < -(cellP.width * 0.15) || cellType == CellTypeRight)
            cellP.rightLabel.isHidden = (cellP.frame.origin.x > (screenWidth - cellP.width * 0.85) || cellType == CellTypeLeft)
        }
    }
    //浮窗工具
    private func openVCwithName(name: String) {
        
        guard let vc = UIApplication.topViewController(controller: self.holdVC)
            else {
                showToast(view: self, txt: "当前页面不支持跳转")
                return
        }
        let isLogined = YiboPreference.getLoginStatus()
        let notFilterArray = getSwitch_lottery() ? ["中奖榜单"] : ["中奖榜单"]
        
        if !notFilterArray.contains(name) && !isLogined
        {
            showToast(view: self, txt: "请登录，以使用该功能")
            return
        }
        
       if name == "中奖榜单"{
            let vcP = InsideMessageController()
            vc.navigationController?.pushViewController(vcP, animated: true)
        }
    }
    
    //MARK: - 跳转数据处理回调
    // 充值
    var getChargeInfo:((Meminfo) -> Void)?
    // 提款
    var getWithdrawInfo:((Meminfo) -> Void)?
    // 投注,Int为1：官方，2信用
    var gotoBet:((String,Int) -> Void)?
}

//MARK: 跳转数据处理 && 点击事件
extension SemicircleChatMenuView {
    
   
  
}




