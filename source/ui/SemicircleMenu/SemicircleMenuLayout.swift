//
//  SemicircleMenuLayout.swift
//  SemicircleMenuLayoutDemo
//
//  Created by Moayad on 5/29/16.
//  Copyright © 2016 Moayad. All rights reserved.
//

import UIKit


enum WheelAlignmentType{
    case left, center
}

class SemicircleMenuLayout: UICollectionViewFlowLayout {
    var cellCount:Int!
    var center:CGPoint!
    var offset:CGFloat!
    var itemHeight:CGFloat = 0
    var xOffset:CGFloat = 0
    var cellSize:CGSize!
    var angularSpacing:CGFloat = 0
    var dialRadius:CGFloat = 0
    var currentIndexPath:IndexPath!
    
    var shouldFlip = true
    
    var lastVelocity:CGPoint!
    
    override init() {
        super.init()

        self.itemSize = CGSize.zero

        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
        
        self.sectionInset = UIEdgeInsets.zero
        self.scrollDirection = .vertical

        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(){
        self.offset = 0.0;
    }
    
    override func prepare(){
        super.prepare()
        if self.collectionView!.numberOfSections > 0{
            self.cellCount = self.collectionView?.numberOfItems(inSection: 0)
        }else{
            self.cellCount = 0
        }
        self.offset = -self.collectionView!.contentOffset.y / self.itemHeight
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    
    func getRectForItem(_ itemIndex: Int) -> CGRect{
        let newIndex =  CGFloat(itemIndex) + self.offset
        let scaleFactor = fmax(0.5, 1 - fabs( newIndex * 0.25))
//        let scaleFactor:CGFloat = 0.1
        let deltaX = self.cellSize.width/2
        
        let temp = Float(self.angularSpacing)
        let dds = Float(self.dialRadius + (deltaX*scaleFactor))
        
        var rX = cosf(temp * Float(newIndex) * Float(Double.pi/180)) * dds
        
        let rY = sinf(temp * Float(newIndex) * Float(Double.pi/180)) * dds
        var oX = -self.dialRadius + self.xOffset - (0.5 * self.cellSize.width);
        let oY = self.collectionView!.bounds.size.height/2 + self.collectionView!.contentOffset.y - (0.5 * self.cellSize.height)
        
        
        if(shouldFlip){
            oX = self.collectionView!.frame.size.width + self.dialRadius - self.xOffset - (0.5 * self.cellSize.width)
            rX *= -1
        }
        
        let itemFrame = CGRect(x: oX + CGFloat(rX), y: oY + CGFloat(rY), width: self.cellSize.width, height: self.cellSize.height)
        
        return itemFrame
    }
    
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var theLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        let maxVisiblesHalf:Int = 180 / Int(self.angularSpacing)
        //var lastIndex = -1
        
        for i in 0 ..< self.cellCount{
            let itemFrame = self.getRectForItem(i)
            
            if(rect.intersects(itemFrame) && i > (-1 * Int(self.offset) - maxVisiblesHalf) && i < (-1 * Int(self.offset) + maxVisiblesHalf)){
                
                let indexPath = IndexPath(item: i, section: 0)
                let theAttributes = self.layoutAttributesForItem(at: indexPath)
                theLayoutAttributes.append(theAttributes!)
                //lastIndex = i;
            }
        }
        
        return theLayoutAttributes;
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        let index = Int(floor(proposedContentOffset.y / self.itemHeight))
        let off = (Int(proposedContentOffset.y) % Int(self.itemHeight))
        
        let height = Int(self.itemHeight)
        
        var targetY = index * height
        if( off > Int((self.itemHeight * 0.5)) && index <= self.cellCount ){
            targetY = (index+1) * height
        }
        
        return CGPoint(x: proposedContentOffset.x, y: CGFloat(targetY))
    }
    
    
    override func targetIndexPath(forInteractivelyMovingItem previousIndexPath: IndexPath, withPosition position: CGPoint) -> IndexPath {
        return IndexPath(item: 0, section: 0)
    }
    
    override var collectionViewContentSize : CGSize {
        return CGSize(width: self.collectionView!.bounds.size.width, height: CGFloat(self.cellCount-1) * self.itemHeight + self.collectionView!.bounds.size.height)
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        let theAttributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        theAttributes.size = self.cellSize
        
        let newFrame = self.getRectForItem(indexPath.item)
        theAttributes.frame = CGRect(x: newFrame.origin.x , y: newFrame.origin.y, width: newFrame.size.width, height: newFrame.size.height)
        
        return theAttributes
    }
}
