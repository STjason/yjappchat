//
//  OnlinePayInputWindow.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/7/1.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

protocol StopSaleDelegate {
    func onStopSaleClick()
}

class StopSaleWindow: UIView{
    
    var delegate:StopSaleDelegate?
    var controller:UIViewController!
    
    var _shareViewBackground :UIView!
    var _window             :UIWindow!
    var windowDelegate:SportWindowDelegate?
    var keyBoardNeedLayout: Bool = true
    @IBOutlet weak var cancelBtn:UIButton!
    
    

    override func awakeFromNib() {
        cancelBtn.addTarget(self, action: #selector(hidden), for: UIControl.Event.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func cancelAction(){
        hidden()
    }
    
    func show() {
        if _shareViewBackground == nil{
            _shareViewBackground = UIView.init(frame: UIScreen.main.bounds)
            _shareViewBackground.backgroundColor = UIColor.clear
            _shareViewBackground.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(cancelAction)))
        }
        _window = UIWindow.init(frame: UIScreen.main.bounds)
        _window.windowLevel = UIWindow.Level.alert+1
        _window.backgroundColor = UIColor.clear
        _window.isHidden = true
        _window.isUserInteractionEnabled = true
        _window.addSubview(_shareViewBackground)
        _window.addSubview(self)
        _window.isHidden = false
        
//        var windowHeight = CGFloat(245)
//        windowHeight = windowHeight > kScreenHeight ? kScreenHeight : windowHeight
//        let y = kScreenHeight - windowHeight
        
        self.frame = CGRect.init(x:0, y:0, width:UIScreen.main.bounds.width, height:kScreenHeight)
        UIView.animate(withDuration: 0.4, animations: {
            self._shareViewBackground.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
        })
        
    }
    
    @objc func hidden() {
        
//        var windowHeight = CGFloat(245)
//        windowHeight = windowHeight > kScreenHeight ? kScreenHeight : windowHeight
//        let y = kScreenHeight - windowHeight
        
        self._shareViewBackground.backgroundColor = UIColor.init(white: 0.0, alpha: 0.0)
        self.frame = CGRect.init(x:0, y:0, width:kScreenWidth, height:kScreenHeight)
        self._window = nil
        
//        UIView.animate(withDuration: 0.4, animations: {
//            self._shareViewBackground.backgroundColor = UIColor.init(white: 0.0, alpha: 0.0)
//            self.frame = CGRect.init(x:0, y:y, width:kScreenWidth, height:kScreenHeight)
//        }) { (finished) in
//            self._window = nil
//        }
    }
    
}
