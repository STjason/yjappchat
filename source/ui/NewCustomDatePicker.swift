//
//  NewCustomDatePicker.swift
//  gameplay
//
//  Created by admin on 2018/9/11.
//  Copyright © 2018 yibo. All rights reserved.
//

import UIKit

class NewCustomDatePicker: UIView {

    
    var isShowing = false
    var isStartDate = true
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var cancelHandler:(() -> Void)?
    var confirmHandler:((_ time:String) -> Void)?
    
    @IBAction func clickBgViewAction(_ sender: Any) {
        hideDatePickerView()
        self.cancelHandler?()
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        self.cancelHandler?()
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        let time = getTimeWithDate(date: self.datePicker.date)
        self.confirmHandler?(time)
    }
    
    private func hideDatePickerView() {
        // 动画
//        self.removeFromSuperview()
        self.isHidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isHidden = true
    }

}
