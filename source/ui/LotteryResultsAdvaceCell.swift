//
//  LotteryResultsAdvaceCell.swift
//  gameplay
//
//  Created by admin on 2018/10/30.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LotteryResultsAdvaceCell: UICollectionViewCell {

    @IBOutlet weak var themeBGView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var circleLabel: UILabel!
    @IBOutlet weak var ballsView: BallsView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        circleLabel.layer.cornerRadius = 12.0
        circleLabel.layer.masksToBounds = true
    }
    
    func configTheme(indexPath:IndexPath)
    {
        if indexPath.section % 2 == 0
        {
            themeBGView.theme_backgroundColor = "FrostedGlass.subColorImageCoverColor"
            themeBGView.theme_alpha = "FrostedGlass.ColorImageCoverAlpha"
        }else
        {
           themeBGView.theme_alpha = "FrostedGlass.ColorImageCoverAlphHigerForGlass"
           themeBGView.theme_backgroundColor = "FrostedGlass.subColorImageCoverColorNormalGray"
        }
    }
    
    /** 期号，和 */
    func configWithTitle(title:String,indexPath:IndexPath)
    {
        hideSubViews()
        titleLabel.isHidden = false
        titleLabel.text = title
    }
    
    func configCircleWithTitle(title:String,indexPath:IndexPath)
    {
        hideSubViews()
        circleLabel.isHidden = false
        circleLabel.text = title
        circleLabel.textColor = UIColor.white
//        circleLabel.textColor = title == "-" ? UIColor.black : UIColor.white
        
        circleLabel.backgroundColor = getColorWithTitle(title: title)
    }
    
    private func getColorWithTitle(title:String) -> UIColor?
    {
        if title == "小" || title == "单"
        {
            return UIColor.init(hexString: "72bb00")
        }else if title == "大" || title == "双"
        {
            return UIColor.init(hexString: "ffd200")
        }else if title == "和"
        {
            return UIColor.init(hexString: "0096ff")
        }else if title == "豹"
        {
            return UIColor.init(hexString: "ff4d4d")
        }else if title == "-"
        {
            return UIColor.clear
        }else
        {
            return nil
        }
    }
    /**
     year:代表这期开奖结果所在的农历年
     **/
    func configBalls(balls:[String],indexPath:IndexPath,width:CGFloat,cpVersion:String,year:String="")
    {
        hideSubViews()
        ballsView.isHidden = false
        
        var ballWidth:CGFloat = 30
        var small = false
        if isSaiche(lotType: cpVersion){
            ballWidth = 18
        }else if isFFSSCai(lotType: cpVersion){
            ballWidth = 30
            small = false
        }else if isXYNC(lotType: cpVersion){
            ballWidth = 18
        }
        
        // 同之前逻辑，缺少同时存在官方信用玩法，暂时同之前处理
        let version = cpVersion == "9" || cpVersion == "6" ? VERSION_2 : VERSION_1
        var y = 0;
        if !isEmptyString(str: year){
            y = Int(year)!
        }
        ballsView.basicSetupBalls(nums: balls, offset: 0, lotTypeCode:cpVersion , cpVersion: version,ballWidth: ballWidth,small: small,gravity_bottom:false,ballsViewWidth: width,isBetTopView:true,lotteryResults:true,time:0,year:y)
    }

    private func hideSubViews()
    {
        titleLabel.isHidden = true
        circleLabel.isHidden = true
        ballsView.isHidden = true
    }
    
}
