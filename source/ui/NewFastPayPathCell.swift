//
//  NewFastPayPathCell.swift
//  gameplay
//
//  Created by admin on 2018/9/27.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class NewFastPayPathCell: UITableViewCell {
    
    
    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var minChargeLabel: UILabel!
    @IBOutlet weak var accountName: UILabel!
    @IBOutlet weak var selectionStatusBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconImage.contentMode = .scaleAspectFit
        selectionStatusBtn.isUserInteractionEnabled = false
        setupNoPictureAlphaBgView(view: self)
    }
    
    func configIcon(icon: String,imgURL:String) {
        
        var placeImg = ""
        let themeBgName = YiboPreference.getCurrentThmeByName()
        if themeBgName.contains("Frosted") && themeBgName != "FrostedPlain" {
            placeImg = "payPlaceHolderFrosted"
        }else {
            placeImg = "payPlaceHolderNormal"
        }
        
        if !isEmptyString(str: imgURL) {
            if let url = URL.init(string: imgURL) {
                iconImage.kf.setImage(with: ImageResource.init(downloadURL: url), placeholder: UIImage.init(named: placeImg), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }else {
            iconImage.image = UIImage.init(named: placeImg)
        }
        
//        else if icon.contains("MemberPage.Charge.")
//        {
//            iconImage.theme_image = ThemeImagePicker.init(keyPath: icon)
//        }else
//        {
//            iconImage.image = UIImage.init(named: icon)
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
