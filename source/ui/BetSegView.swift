//
//  BetSegView.swift
//  gameplay
//
//  Created by admin on 2019/5/10.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class BetSegView: UIView {
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftTipImage: UIImageView!
    
    var clickButtonHandler:((_ selectedIndex:Int) -> Void)?
    
    var selectedIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clear
        
        leftButton.isSelected = true
        rightButton.isSelected = false
        
        leftTipImage.contentMode = .scaleAspectFit
        setupThemeButton(button: leftButton)
        setupThemeButton(button: rightButton)
        
        leftButton.addTarget(self, action: #selector(clickLeftAction), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(clickRightAction), for: .touchUpInside)
        
        setupBtnAfterSelected(index: selectedIndex)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupCorner(type: 0, view: leftButton)
        setupCorner(type: 1, view: rightButton)
    }
    
    /// 为view添加圆角
    ///
    /// - Parameters:
    ///   - type: 0.左边上下圆角，1.右边上下圆角
    ///   - view: 被添加圆角的view
    private func setupCorner(type:Int,view:UIView) {
        
        let corner:UIRectCorner = type == 0 ? [.topLeft,.bottomLeft] : [.topRight,.bottomRight]
        
        let maskPath = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corner, cornerRadii: CGSize(width: 3, height: 3))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = view.bounds
        maskLayer.path = maskPath.cgPath
        view.layer.mask = maskLayer
    }
    
    private func setupThemeButton(button:UIButton) {
        button.layer.theme_borderColor = "SegmentColor.border"
        button.layer.borderWidth = 1.5
        button.theme_setTitleColor("SegmentColor.normalFont", forState: .normal)
        button.theme_setTitleColor("SegmentColor.selectedFont", forState: .selected)
        button.theme_setTitleColor("SegmentColor.selectedFont", forState: .highlighted)
        
        updateButtonBgColor(button: button)
    }
    
    private func updateButtonBgColor(button:UIButton) {
        if button.isSelected {
            button.theme_backgroundColor = "SegmentColor.selectedBg"
        }else {
            button.theme_backgroundColor = "SegmentColor.normalBg"
        }
    }
    
    func setupBtnAfterSelected(index:Int) {
        rightButton.isSelected = index == 1 ? true : false
        leftButton.isSelected =  index == 0 ? true : false
        
        updateButtonBgColor(button: leftButton)
        updateButtonBgColor(button: rightButton)
    }
    
    @objc func clickLeftAction() {
        selectedIndex = 0
        setupBtnAfterSelected(index: selectedIndex)
        clickButtonHandler?(selectedIndex)
    }
    
    @objc func clickRightAction() {
        selectedIndex = 1
        setupBtnAfterSelected(index: selectedIndex)
        clickButtonHandler?(selectedIndex)
    }
    
    
}
