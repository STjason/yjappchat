//
//  BallsView.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/6.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

class BallsView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        self.backgroundColor = UIColor.orange
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func FastThreeLeopard(values:[String],index:Int,str:String) -> String {
        
        if let sys = getSystemConfigFromJson()
        {
            if sys.content.k3_baozi_daXiaoDanShuang == "on" {
                return str
            }
        }
        
        var numsT = [String]()
        if values.count < 3 {
            return str
        }
        numsT.append(values[0])
        numsT.append(values[1])
        numsT.append(values[2])
        if isLeopard(values: numsT) {
            if CGFloat.init(index) == 5 || CGFloat.init(index) == 6 {
               return "豹子"
            }
        }
        return str
    }
    
    func isLeopard(values:[String]) -> Bool {
        var allValueEqual = false
        for (index,value) in values.enumerated()
        {
            if index > 0
            {
                allValueEqual = value == values[index - 1]
                if !allValueEqual
                {
                    break
                }
            }
        }
        return allValueEqual
    }
    
    /**
     Nums:号码数组
     Offset:整个号码球视图与左边的间距
     lotTypeCode: 彩种类型编号
     cpVersion: 彩票版本
     ballWidth:每个球的宽度
     small:球上的小字号是否显示小字号
     grivity_bottom:球是否居底部显示
     ballsViewWidth:所有球所占的视图宽度
     forBetPageBelow:是否只针对投注页中的最近开奖号码
     time:开奖时间，单位毫秒
     year:当前开奖结果对应的农历年
     fromChatRoom:是否在聊天室页面中
     **/
    func basicSetupBalls(nums:[String],offset:Int,lotTypeCode:String,cpVersion:String,ballWidth:CGFloat=35,
                         small:Bool=false,gravity_bottom:Bool=true,ballsViewWidth: CGFloat,forBetPageBelow:Bool=false,
                         isBetTopView:Bool = false,lotteryResults:Bool = false,time:Int64=0,year:Int=0,fromChatRoom:Bool=false,isChatBottom:Bool = false,focusNum:String = "") -> Void {
        
        for view in self.subviews{
            view.removeFromSuperview()
        }
        var ballW = ballWidth
        //        let ballViewWidth = kScreenWidth - 100 - 8 - 5 // 期号label宽度为100，self与前后控件间距为 8，5
        //        let offset:Float = Float(self.bounds.width/2) - Float(nums.count)*Float(ballW)/2
        var offset:Float = 0 //整个号码球视图与左边的间距
        let offsetBetweenBalls:Float = 1.5//两个号码球之前的间距
        
        var ballsViewWidth2 = ballsViewWidth
        if offsetBetweenBalls > 0 && !forBetPageBelow{
            ballsViewWidth2 = ballsViewWidth - CGFloat(offsetBetweenBalls*Float(nums.count+1))
        }
        
        //若时投注页开奖结果列表里的号码view，不需要将号码view居中
        if !forBetPageBelow{
            offset = isSixMarkWithTypeCode(type: lotTypeCode) && isBetTopView ? 5 : Float(ballsViewWidth2/2) - Float(nums.count)*Float(ballW)/2
        }
        
        let ballsMaxIndex = isSixMarkWithTypeCode(type: lotTypeCode)  && isBetTopView  ? nums.count : nums.count - 1
        for index in 0...ballsMaxIndex {
            
            let ball = UIButton.init()
            let label = UILabel.init()
            var numString = ""
            
            if isSixMarkWithTypeCode(type: lotTypeCode) && isBetTopView
            {
                label.font = UIFont.systemFont(ofSize: 12.0)
                label.textColor = isChatBottom ? UIColor.white : UIColor.black
//                label.textColor = UIColor.black
                label.textAlignment = .center
                label.backgroundColor = UIColor.clear
                
                
                label.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index))), y: 3 + ballW, width: ballW, height: (15))
                
                if index < ballsMaxIndex - 1
                {
                    numString = nums[index]
                    ball.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index))), y: 0, width: ballW, height: ballW)
                    
                    label.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index))), y: 3 + ballW, width: ballW, height: (15))
                }else if index == ballsMaxIndex - 1{
                    numString = "+"
                    ball.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index - 2))), y: 0, width: ballW, height: ballW)
                    
                    label.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index - 2))), y: 3 + ballW, width: ballW, height: (15))
                }else if index == ballsMaxIndex{
                    numString = nums[ballsMaxIndex - 1]
                    
                    ball.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index - 3))), y: 0, width: ballW, height: ballW)
                    
                    label.frame = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index - 3))), y: 3 + ballW, width: ballW, height: (15))
                }
            }else{
                
                if lotTypeCode == "3" {
                    ballW = 20
                }else {
                    ballW = 25
                }
                if lotTypeCode == "9" {
                    offset = isSixMarkWithTypeCode(type: lotTypeCode) && isBetTopView ? 5 : Float(ballsViewWidth2/2) - Float(nums.count)*Float(ballW)/2
                    if offset < 0{
                        offset = 0
                    }
                }
                if fromChatRoom{
                    ball.frame  = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index))), y: !gravity_bottom ? ((60 - ballW)/2): (self.bounds.height - ballW), width: ballW, height: ballW)
                }else{
                    ball.frame  = CGRect.init(x: CGFloat(Float(ballW*CGFloat(index)) + offset + Float(offsetBetweenBalls*Float(index))), y: !gravity_bottom ? ((self.bounds.height - ballW)/2) : (self.bounds.height - ballW), width: ballW, height: ballW)
                }

                numString = nums[index]
                
                if lotTypeCode == "4" { //ks豹子计算
                     numString = FastThreeLeopard(values: nums, index: index, str: numString)
                }
            }
            
            ball.contentMode = .scaleAspectFill
            ball.isUserInteractionEnabled = false
            
            let (image,showTitle) = figureImgeByCpCode(cpCode: lotTypeCode, cpVersion: cpVersion, num:numString ,forBetPageBelow:forBetPageBelow,index: index)
            
            if lotTypeCode == "3" && numString == focusNum {
                let line = UIView()
                var ballFrame = ball.frame
                ballFrame.origin.y = ball.frame.origin.y - 8
                ball.frame = ballFrame
                line.frame = CGRect.init(x: ball.x, y: ball.y+ball.height+5, width: ball.width, height: 2)
                line.backgroundColor = UIColor.red
                self.addSubview(line)
            }
            
            if showTitle{
                ball.setTitle(numString, for: .normal)
                if isSixMarkWithTypeCode(type: lotTypeCode) && isBetTopView
                {
                    //                    label.text = getZodiacWithNumString(value_String: numString)
                    if isPurnInt(string: numString) && !isEmptyString(str: numString){
                        
                        if time > 0{
                            let date = Date.init(timeIntervalSince1970: TimeInterval(time/1000));
                            let sx = getShenxiaoFromNumberAndDate(number: numString, date: date)
                            label.text = sx
                        }else if year > 0{
                            let title = getShenxiaoWithNumberAndYear(value_Int: Int(numString)!, year: String(year))
                            label.text = title
                        }
                        
                    }else{
                        label.text = ""
                    }
                }
                
                ball.titleLabel?.font = UIFont.boldSystemFont(ofSize: small ? 10 : 14)
                
                //设置号码球 字体颜色
                if (isPlainBlue() || YiboPreference.getIsChatBet() == "on")
                {
                    if isSixMarkWithTypeCode(type: lotTypeCode)
                    {
                        ball.setTitleColor(switch_LHC_option_icons() ? UIColor.white : UIColor.black, for: .normal)
                    }else
                    {
                        ball.setTitleColor(lotteryResults ? UIColor.white :UIColor.black, for: .normal)
                    }
                    
                    if numString == "+"
                    {
                        ball.setTitleColor(UIColor.black, for: .normal)
                    }
                }else
                {
                    if isSixMarkWithTypeCode(type: lotTypeCode)
                    {
                        ball.setTitleColor(switch_LHC_option_icons() ? UIColor.white : UIColor.black, for: .normal)
                    }else
                    {
                        
                        if isKuai3(lotType: lotTypeCode){
                            ball.setTitleColor(UIColor.black, for: .normal)
                            label.textColor = UIColor.black
                        }else{
                            ball.setTitleColor(UIColor.white, for: .normal)
                        }
                        
                    }
                    
                    if numString == "+"{
                        ball.setTitleColor(UIColor.black, for: .normal)
                    }
                }
            }
            
            //设置号码球 背景图片
            // 不需要主题
            if cpVersion == VERSION_2 && lotTypeCode == "9" {
                ball.setBackgroundImage(UIImage(named: image), for: .normal)
            }else if isSixMarkWithTypeCode(type: lotTypeCode) {
                
                ball.setBackgroundImage(UIImage(named: image), for: .normal)
                
                if isSixMarkWithTypeCode(type: lotTypeCode)  && isBetTopView
                {
                    if index == ballsMaxIndex - 1
                    {
                        ball.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
                        ball.setBackgroundImage(UIImage(named: ""), for: .normal)
                    }
                }
            }else if lotTypeCode == "4" || lotTypeCode == "3"{
                if !isEmptyString(str: image){
                    ball.setBackgroundImage(UIImage(named: image), for: .normal)
                }
            }else {
                
                if YiboPreference.getIsChatBet() == "on" {
                    if !lotteryResults
                    {
                        ball.setBackgroundImage(UIImage.init(named: "ballDark_plain_blue"), for: .normal)
                    }else
                    {
                        ball.theme_setBackgroundImage("TouzhOffical.recentResult", forState: .normal)
                    }
                }else {
                    if !lotteryResults
                    {
                        ball.theme_setBackgroundImage(ThemeImagePicker.init(keyPath: image), forState: .normal)
                    }else
                    {
                        ball.theme_setBackgroundImage("TouzhOffical.recentResult", forState: .normal)
                    }
                }
            }
            self.addSubview(ball)
            self.addSubview(label)
        }
        
    }
    
    func getKuai3NumImage(num:String,index:Int) -> (image:String,showTitle:Bool) {
        if index > 2 {
            return ("",true)
        }
        switch num {
        case "1":
            return ("kuai3_bg_one",false)
        case "2":
            return ("kuai3_bg_two",false)
        case "3":
            return ("kuai3_bg_three",false)
        case "4":
            return ("kuai3_bg_four",false)
        case "5":
            return ("kuai3_bg_five",false)
        case "6":
            return ("kuai3_bg_six",false)
        default:
            break
        }
        return ("",true)
    }
    
    func figurePceggImage(num:String) -> String {
//        let images = // ["ffc_yellow_bg","ffc_red_bg","ffc_green_bg","ffc_blue_bg","ffc_light_blue_bg"]
//        let i = Int(arc4random() % UInt32(images.count))
//        return images[i]
        return "open_result_ball"
    }
    
    func figureLhcImages(num:String) -> String {
        var numP = num
        if numP.length == 1 {
            numP = "0\(numP)"
        }
        let redBO = ["01","02","07","08","12","13","18","19","23","24","29","30","34","35","40","45","46"]
        let blueBO = ["03","04","09","10","14","15","20","25","26","31","36","37","41","42","47","48"]
        let greenBO = ["05","06","11","16","17","21","22","27","28","32","33","38","39","43","44","49"]
        if redBO.contains(numP){
            return switch_LHC_option_icons() ? "lhc_red_bg": "lhc_red_bg_3D"
        }else if blueBO.contains(numP){
            return switch_LHC_option_icons() ? "lhc_blue_bg" :  "lhc_blue_bg_3D"
        }else if greenBO.contains(numP){
            return switch_LHC_option_icons() ? "lhc_green_bg"  : "lhc_green_bg_3D"
        }
        return  switch_LHC_option_icons() ? "lhc_red_bg" :  "lhc_red_bg_3D"
    }
    
    func figureSaiCheImage(num:String) -> (image:String,showTitle:Bool) {
        if num == "?"{
            return ("open_result_ball",true)
        }
        return (String.init(format: "sc_%@_title",num ),false)
    }
    
    func figureXYNCImage
        (num:String) -> (image:String,showTitle:Bool) {
        if num == "?"{
            return ("open_result_ball",true)
        }
        return (String.init(format: "xync_%@",num ),false)
    }
    
    func figureImgeByCpCode(cpCode:String,cpVersion:String,num:String,forBetPageBelow:Bool=false,index:Int = -1) -> (image:String,showTitle:Bool) {
        
        var image = "TouzhOffical.recentResult"
        var showTitle = false
        
        switch cpVersion {
        case VERSION_2:
            switch cpCode{
            case "4"://快三
                let (images,showTitles) = getKuai3NumImage(num: num,index: index)
                image = images
                showTitle = showTitles
            case "9"://重庆幸运农场,湖南快乐十分,广东快乐十分
                let (images,showTitles) = figureXYNCImage(num: num)
                image = images
                showTitle = showTitles
            case "3"://极速赛车，北京赛车，幸运飞艇
                let (images,showTitles) = figureSaiCheImage(num: num)
                image = images
                showTitle = showTitles
            case "5","8","7","1","2"://11选5,福彩3D，排列三,pc蛋蛋，加拿大28
                showTitle = true
                if forBetPageBelow {
                    image = "TouzhOffical.recentResult"
                }else{
                    image = "TouzhOffical.ballDark"
                }
            case "6","66"://六合彩，十分六合彩
                image = figureLhcImages(num: num)
                showTitle = true
            default:
                break
            }
        case VERSION_1:
            switch cpCode{
                case "4"://快三
                    let (images,showTitles) = getKuai3NumImage(num: num,index: index)
                    image = images
                    showTitle = showTitles
                case "3"://极速赛车，北京赛车，幸运飞艇
                    let (images,showTitles) = figureSaiCheImage(num: num)
                    image = images
                    showTitle = showTitles
                case "5","8","7","1","2"://11选5,福彩3D，排列三,pc蛋蛋，加拿大28
                    showTitle = true
                    if forBetPageBelow{
                        image = "TouzhOffical.recentResult"
                    }else{
                        image = "TouzhOffical.ballDark"
                    }
                case "6","66"://六合彩，十分六合彩
                    image = figureLhcImages(num: num)
                    showTitle = true
                default:
                    break
                }
        default:
            break
        }
        return (image:image,showTitle:showTitle)
    }
}
