//
//  UserlistFilterView.swift
//  gameplay
//
//  Created by admin on 2018/8/17.
//  Copyright © 2018 yibo. All rights reserved.
//

import UIKit

class UserlistFilterView: UIView {
//    var showAllLevelBtn = UIButton()
    
    
    var didClickShowAllLevelBtn: ((Bool) -> Void)?
    
    
    @IBOutlet weak var userNameInput: CustomFeildText!
    @IBOutlet weak var minimumBalance: CustomFeildText!
    @IBOutlet weak var maxBalanceInput: CustomFeildText!
    @IBOutlet weak var loginStartTimeButton: UIButton!
    @IBOutlet weak var loginEndTimeButton: UIButton!
    @IBOutlet weak var allLevelButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    var isShowing = false
    
    var loginStartHandler:(() -> Void)?
    var loginEndHandler:(() -> Void)?
    var allLevelHandler:(() -> Void)?
    var confirmHandler:((_ userName:String,_ minimumBalace: String,_ maxBalance:String,_ startTime: String,_ endTime: String,_ showAllLevel:Bool) -> Void)?
    var cancelHandler:(() -> Void)?
    
    @IBAction func loginStartTimeAction(_ sender: Any) {
        self.loginStartHandler?()
    }
    
    @IBAction func loginEndTimeAction(_ sender: Any) {
        self.loginEndHandler?()
    }
    
    
    @IBAction func allLevelAction(_ sender: Any) {
        self.allLevelButton.setTitle(self.allLevelButton.titleLabel?.text == "否" ? "是" : "否", for: .normal)
        self.allLevelHandler?()
    }
    
    @IBAction func confirmAction(_ sender: Any) {
       handleData()
        
    }
    
    func clearData() {
        self.userNameInput.text = ""
        self.minimumBalance.text = ""
        self.maxBalanceInput.text = ""
        self.loginStartTimeButton.setTitle("登录开始时间", for: .normal)
        self.loginEndTimeButton.setTitle("登录结束时间", for: .normal)
        self.allLevelButton.titleLabel?.text = "否"
    }
    
    private func handleData() {
        let showAllLevel = self.allLevelButton.titleLabel?.text == "否" ? false : true
        
        var userName = ""
        if let userNameP =  userNameInput.text {
            userName = userNameP
        }
        
        var minimumBalanceContents = ""
        if let minimumBalanceP = minimumBalance.text {
            minimumBalanceContents = minimumBalanceP
        }
        
        var maxBalanceContents = ""
        if let maxBalanceContentsP = maxBalanceInput.text {
            maxBalanceContents = maxBalanceContentsP
        }
        
        var loginStartTime = ""
        if let loginStartTimeLabelP = loginStartTimeButton.titleLabel {
            if let loginStartTimeP = loginStartTimeLabelP.text {
                if loginStartTimeP != "登录开始时间" {
                    loginStartTime = loginStartTimeP
                }
            }
        }
        
        var loginEndTime = ""
        if let loginEndTimeLabelP = loginEndTimeButton.titleLabel {
            if let loginEndTimeP = loginEndTimeLabelP.text {
                if loginEndTimeP != "登录结束时间" {
                    loginEndTime = loginEndTimeP
                }
            }
        }
        
        self.confirmHandler?(userName, minimumBalanceContents, maxBalanceContents, loginStartTime, loginEndTime, showAllLevel)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.cancelHandler?()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViewsBorder(views: [userNameInput,minimumBalance,maxBalanceInput,loginStartTimeButton,loginEndTimeButton,allLevelButton,cancelButton,confirmButton])
        
        self.setupTextFieldLeftView(textFields: [userNameInput,minimumBalance,maxBalanceInput])
    }
    
    private func setupTextFieldLeftView(textFields: [CustomFeildText]) {
        for textField in textFields {
            textField.leftViewMode = .always
            
            let leftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 5, height: 1))
            leftView.backgroundColor = UIColor.clear
            textField.leftView = leftView
        }
    }
    
    private func setupViewsBorder(views: [UIView])  {
        for view in views {
            view.layer.cornerRadius = 3.0
            view.layer.borderColor = UIColor.gray.cgColor
            view.layer.borderWidth = 0.5
        }
    }

}
