//
//  FloatTableView.swift
//  gameplay
//
//  Created by admin on 2018/12/28.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class FloatTableView: UIView {
    
    var datas: [String] = [String]()
    var tableView:UITableView!
    var selectIndexHandler:((_ index:Int,_ title:String) -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect,datas:[String]) {
        self.init(frame: frame)
        tableView = UITableView(frame: self.bounds, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.datas = datas
        addSubview(tableView)
        tableView.reloadData()
    }
    
}

extension FloatTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = datas[indexPath.row]
        selectIndexHandler?(indexPath.row, title)
    }
}

extension FloatTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        cell.backgroundColor = UIColor.clear
        let title = datas[indexPath.row]
        cell.textLabel?.text = title
        
        return cell
    }
}
