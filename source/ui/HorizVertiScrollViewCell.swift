//
//  HorizVertiScrollViewCell.swift
//  gameplay
//
//  Created by admin on 2018/10/19.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class HorizVertiScrollViewCell: UICollectionViewCell {
    
    var indexPath:IndexPath = IndexPath.init(row: 0, section: 0)
    var closeActionHandler:((IndexPath) -> Void)?
    var longGestureHandler:((UIImage?) -> Void)?
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!

    @IBAction func closeAction() {
        closeActionHandler?(indexPath)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        closeBtn.imageView?.contentMode = .scaleToFill
        closeBtn.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        closeBtn.isHidden = true
    }
    
    func configWithModel(model:WechatQRCodeModel,indexPath:IndexPath) {
        self.indexPath = indexPath
        
//        closeBtn.isHidden = model.linkType == "1" || model.linkType == "2" ? true : false
        
        let placeholderImage = UIImage.init(named: "")
        if let url = URL.init(string: model.imgUrl)
        {
            img.kf.setImage(with: ImageResource.init(downloadURL: url), placeholder: placeholderImage, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                if image == nil && error == nil
                {
                    self.img.image = image
                }
            }
        }else
        {
            img.image = placeholderImage
        }
        
        let longPressGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(logPressPicAction))
        self.img.addGestureRecognizer(longPressGesture)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapImageAction))
        self.img.addGestureRecognizer(tapGesture)
    }
    
    @objc private func tapImageAction(gesture:UIGestureRecognizer) {
        closeActionHandler?(indexPath)
    }
    
    @objc private func logPressPicAction(gesture:UIGestureRecognizer) {
        if let image = self.img.image
        {
            longGestureHandler?(image)
        }else
        {
            longGestureHandler?(nil)
        }
    }
    
    
}



