//
//  SubPayMethodCell.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class SubPayMethodCell: UICollectionViewCell {
    
    //    var indictorLabel : UILabel?//指示label
    var moneyBtn:UIButton! //禁用
    var payTV:UILabel!
    var moneyBgBtn = UIImageView() //UIButton()
    var moneyMAGE = UIImageView()
    var moneyV = UIImageView()
//    var moneyIS:Bool?
    //宽度要减去与两边的我间距20,再减去单元项间距0.5*6
    let screen_width = UIScreen.main.bounds.size.width - 0.5*6//获取屏幕宽
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setViewBackgroundColorTransparent(view: self)
        
        //初始化各种控件
        
        moneyBgBtn = UIImageView.init(frame: CGRect.init(x: 7, y: self.bounds.height/2-10.5, width: self.bounds.width - 14, height: 32)) //bg
        
        moneyBtn = UIButton.init(frame: CGRect.init(x: 10, y: self.bounds.height/2-12.5, width: self.bounds.width - 20, height: 30)) //icon ???
        
        payTV = UILabel.init(frame: CGRect.init(x: 10, y: self.bounds.height/2-12.5, width: self.bounds.width - 20, height: 30))
        
        
        moneyBtn.layer.cornerRadius = 3
        //        moneyBgBtn.layer.cornerRadius = 3
        setupNoPictureAlphaBgView(view: self.moneyBtn,alpha: 0.2,allThemeAlphaSame: true)
        //        setupNoPictureAlphaBgView(view: self.moneyBgBtn,alpha: 0.2,allThemeAlphaSame: true)
        
        moneyBtn.isUserInteractionEnabled = false
        moneyBgBtn.isUserInteractionEnabled = false
        payTV.font = UIFont.systemFont(ofSize: 14)
        payTV.textColor = UIColor.black
        payTV.textAlignment = .center
        payTV.isHidden = true
        moneyBtn.addSubview(moneyBgBtn)
        self.addSubview(moneyBgBtn)
        self.addSubview(moneyBtn!)
        self.addSubview(payTV)
        
        
        moneyBtn.snp.updateConstraints { (make) in
            make.left.top.equalTo(5)
            make.bottom.equalTo(-5)
            make.right.equalTo(-22)
        }
        
        moneyBtn.contentMode = .scaleAspectFit
        
        
        //        版本1
        payTV.isHidden = true
        moneyBtn.isHidden = true
        
        
        moneyBgBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(0)
        }
        
        
        
        self.addSubview(moneyMAGE)
        moneyMAGE.snp.makeConstraints { (make) in
            make.left.top.bottom.right.equalTo(0)
            //                    make.bottom.equalTo(-5)
            //                    make.right.equalTo(-5)
        }
        
        moneyBgBtn.isHidden = true

        
        // 如果子视图的范围超出了父视图的边界，那么超出的部分就会被裁剪掉。
        moneyMAGE.clipsToBounds = true
        // 圆角的半径
        moneyMAGE.layer.cornerRadius = 5
        // 边框线宽
        moneyMAGE.layer.borderWidth = 2
        // 边框线颜色
        //        moneyMAGE.layer.borderColor = UIColor.red.cgColor
        
        moneyV = UIImageView.init()
        moneyMAGE.addSubview(moneyV)
        moneyV.snp.makeConstraints { (make) in
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(15)
            make.width.equalTo(25)
        }
        moneyV.image = UIImage.init(named:"icon_triangle_select")
        

//        if moneyIS! {
//            moneyBgBtn.addSubview(moneyMAGE)
//            //            self.addSubview(moneyMAGE)
//            moneyMAGE.snp.makeConstraints { (make) in
//                make.left.top.equalTo(0)
//                make.bottom.equalTo(-5)
//                make.right.equalTo(-5)
//            }
//        }else{
//            //                moneyBgBtn.addSubview(moneyMAGE)
//            self.addSubview(moneyMAGE)
//            moneyMAGE.snp.makeConstraints { (make) in
//                make.left.top.bottom.right.equalTo(0)
//                //                    make.bottom.equalTo(-5)
//                //                    make.right.equalTo(-5)
//            }
//
//            moneyBgBtn.isHidden = true
//
//
//            //                moneyMAGE.layer.masksToBounds = true
//            //                moneyMAGE.layer.shadowColor = UIColor.red.cgColor
//            //                moneyMAGE.layer.borderWidth = 1
//
//            // 如果子视图的范围超出了父视图的边界，那么超出的部分就会被裁剪掉。
//            moneyMAGE.clipsToBounds = true
//            // 圆角的半径
//            moneyMAGE.layer.cornerRadius = 5
//            // 边框线宽
//            moneyMAGE.layer.borderWidth = 2
//            // 边框线颜色
//            //        moneyMAGE.layer.borderColor = UIColor.red.cgColor
//
//            moneyV = UIImageView.init()
//            self.addSubview(moneyV)
//            moneyV.snp.makeConstraints { (make) in
//                make.right.equalTo(0)
//                make.bottom.equalTo(0)
//                make.height.equalTo(15)
//                make.width.equalTo(25)
//            }
//            moneyV.image = UIImage.init(named:"icon_triangle_select")
//        }
    }
    
 
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /// 设置支付cell
    ///
    /// - Parameter payName: funcFirst: false 先支付通道；true 先支付方式
    func setupBtn(payName:String,funcFirst:Bool = false,isBank:Bool = false){
        if isEmptyString(str: payName){
            moneyBtn.setImage(nil, for: .normal)
            //            moneyBgBtn.setImage(nil, for: .normal)
            moneyBgBtn.image = nil
            return
        }
        
        
        var compontentsURL = funcFirst ? URL_ICON_ONLINE_PAY_METHOD : "/native/resources/images/"
        if isBank{
            ///mobile/images/bankimg/' + item + '.png'
            compontentsURL = funcFirst ? URL_ICON_ONLINE_PAY_METHOD : "/mobile/images/bankimg/"
//            moneyBgBtn.isHidden = true
        }
        let imageURL = URL(string: BASE_URL + PORT + compontentsURL + payName + ".png")
        
        if let url = imageURL{
            moneyMAGE.kf.setImage(with: ImageResource(downloadURL: url), placeholder:nil, options: nil, progressBlock: nil) { (image, errer, cacheType, url) in
                if image != nil && errer == nil
                {
                    self.moneyMAGE.image = image
                }else
                {
                    self.moneyMAGE.isHidden = false
                    self.payTV.isHidden = false
                    self.moneyBgBtn.isHidden = false
                    self.payTV.text = payName
                    
                }
            }
            
            //            payTV.isHidden = true
            //            moneyBtn.isHidden = false
            //            moneyBgBtn.isHidden = false
            //
            ////            moneyBtn.kf.setImage(with: ImageResource(downloadURL: url), for: .normal, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            //
            //            moneyBtn.kf.setImage(with: ImageResource(downloadURL: url), for: .normal, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
            //                if image != nil && error == nil
            //                {
            //                    self.moneyBtn.setImage(image, for: .normal)
            //                }else
            //                {
            //                    self.payTV.isHidden = false
            //                    self.moneyBtn.isHidden = false
            //                    self.moneyBgBtn.isHidden = false
            //                    self.payTV.text = payName
            //                }
            //            }
            
        } else{
            payTV.isHidden = false
            moneyBtn.isHidden = true
            moneyBgBtn.isHidden = true
            moneyMAGE.isHidden = true
            payTV.text = payName
        }
    }
    
}
