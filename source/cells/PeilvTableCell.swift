//
//  PeilvTableCell.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/15.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
/*
 赔率版每一行中的table cell
 */
class PeilvTableCell: UITableViewCell {
    
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var numberTV:UILabel!
    @IBOutlet weak var peilvTV:UILabel!
    
    @IBOutlet weak var firstPeilvConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var secondPeilvTV: UILabel!
    @IBOutlet weak var ballviews:BallsView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isPlainBlue()
        {
            setupNoPictureAlphaBgView(view: self)
        }else
        {
            setViewBackgroundColorTransparent(view: self)
        }
    }
    
    private func setupPeilvData(data:PeilvWebResult) {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 3
        formatter.minimumFractionDigits = 1
        if !(["sze","ezt"].contains(data.playCode) || ["sze","ezt"].contains(data.code)){
            if (data.currentOdds) > Float(0){
                let string = String.init(format:"%f",data.currentOdds)
                let double: Double = (Double(string)! * 10000).rounded() / 10000
//                peilvTV.text = String.init(format: "%.3lf", (double))
                peilvTV.text = formatter.string(from: NSNumber(value: double))
            }else{
//                peilvTV.text = String.init(format: "%.3f", (data.maxOdds))
                peilvTV.text = formatter.string(from: NSNumber(value: data.maxOdds))
            }
            
            if showTwopeilv() {
                let string = String.init(format:"%f",data.minOdds)
                let double: Double = (Double(string)! * 10000).rounded() / 10000
//                secondPeilvTV.text = String.init(format: "%.3lf", (double))
                secondPeilvTV.text = formatter.string(from: NSNumber(value: double))
            }else {
                if let top = firstPeilvConstraintTop {
                    top.constant = 0
                }
                
                secondPeilvTV.text = ""
                secondPeilvTV.isHidden = true
            }
        }else {
            secondPeilvTV.isHidden = false
            if let top = firstPeilvConstraintTop {
                top.constant = -8
            }
            if (data.currentOdds) > Float(0){
//                peilvTV.text = String.init(format: "%.3f", (data.currentOdds))
                peilvTV.text = formatter.string(from: NSNumber(value: data.currentOdds))
            }else{
//                peilvTV.text = String.init(format: "%.3f", (data.maxOdds))
                peilvTV.text = formatter.string(from: NSNumber(value: data.maxOdds))
            }
            
            if (data.currentSecondOdds) > Float(0) {
//                secondPeilvTV.text = String.init(format: "%.3f", (data.currentSecondOdds))
                secondPeilvTV.text = formatter.string(from: NSNumber(value: data.currentSecondOdds))
            }else {
//                secondPeilvTV.text = String.init(format: "%.3f", (data.secondMaxOdds))
                secondPeilvTV.text = formatter.string(from: NSNumber(value: data.secondMaxOdds))
            }
            
        }
        
    }
    
    func setupData(data:PeilvWebResult?,cpCode:String,cpVersion:String,num:String){
        if data == nil{
            return
        }
        numberTV.text = data?.numName
        
        if let dataP = data {
            setupPeilvData(data: dataP)
        }
        
        if (data?.isSelected)! {
            self.bgView.theme_backgroundColor = "TouzhOffical.betSelectedCellColor"
        }else{
            self.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
        if !num.isEmpty{
            let array = num.components(separatedBy: ",")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.ballviews.basicSetupBalls(nums: array, offset: 0, lotTypeCode: cpCode, cpVersion: cpVersion,small:false,gravity_bottom:false,ballsViewWidth: self.ballviews.width,isBetTopView:false)
            }
            
        }
    }
}
