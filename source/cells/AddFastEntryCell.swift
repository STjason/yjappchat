//
//  AddFastEntryCell.swift
//  gameplay
//
//  Created by admin on 2018/9/16.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class AddFastEntryCell: UITableViewCell {

    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configWithItem(item: (String,String)) {
        if item.1.hasPrefix("/") {
            
            self.icon.kf.setImage(with: URL(string: BASE_URL + item.1))
        }else{
            self.icon.image = UIImage.init(named: item.1)
        }
        
        self.titleLabel.text = item.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
