//
//  PayInfoMoneyCell.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class PayInfoMoneyCell: UICollectionViewCell {
    
    //    var indictorLabel : UILabel?//指示label
    var moneyBtn:UIButton!
    var moneyV = UIImageView()
    //宽度要减去与两边的我间距20,再减去单元项间距0.5*6
    let screen_width = UIScreen.main.bounds.size.width - 0.5*6//获取屏幕宽
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //初始化各种控件
        setViewBackgroundColorTransparent(view: self)
        moneyBtn = UIButton.init(frame: CGRect.init(x: 10, y: self.bounds.height/2-12.5, width: self.bounds.width - 20, height: 30))

//        moneyBtn.theme_setTitleColor("Global.themeColor", forState: .normal)
//        moneyBtn.layer.theme_borderColor = "Global.themeColor"
        
        moneyBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        
        
//        setupNoPictureAlphaBgView(view: self.moneyBtn,alpha: 0.1)
        
        moneyBtn.layer.borderWidth = 1
        moneyBtn.layer.cornerRadius = 3
        moneyBtn.layer.masksToBounds = true
        moneyBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        
        moneyBtn.clipsToBounds = true
        moneyBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        moneyBtn.setTitle("0元", for: .normal)
        moneyBtn.isUserInteractionEnabled = false
        self.addSubview(moneyBtn!)
        
        
        moneyV = UIImageView.init()
        moneyBtn.addSubview(moneyV)
        moneyV.snp.makeConstraints { (make) in
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(15)
            make.width.equalTo(25)
        }
        moneyV.image = UIImage.init(named:"icon_triangle_select")
        
        
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupBtn(money:String){
        moneyBtn.setTitle(String.init(format: "%@元", money), for: .normal)
    }

}
