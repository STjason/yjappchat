//
//  AddBankTableCell.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/20.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class AddBankTableCell: UITableViewCell{
    
    var transferHandler:(() -> Void)?
    
    @IBOutlet weak var alipayTransferBtn: UIButton!
    @IBOutlet weak var alipayTransferV1Btn: UIButton!
    
    @IBOutlet weak var textTV:UILabel!
    @IBOutlet weak var inputTV:CustomFeildText!
    @IBOutlet weak var valueTV:UILabel!
    @IBOutlet weak var copyBtn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self,alpha: 0.2)
    }
    
    @objc private func alipayAction() {
        if textTV.text == "支付宝转卡" {
            transferHandler?()
        }
    }
    
    func configModel(model:FakeBankBean) {
        if alipayTransferBtn != nil {
            setupAliPayBtn(btn: alipayTransferBtn, model: model)
        }else if alipayTransferV1Btn != nil {
            setupAliPayBtn(btn: alipayTransferV1Btn, model: model)
        }
        
        textTV.text = model.text
    }
    
    private func setupAliPayBtn(btn:UIButton,model:FakeBankBean) {
        if model.text == "支付宝转卡" {
            btn.setTitle(" 点击支付宝转卡 ", for: .normal)
            btn.layer.cornerRadius = 3
            btn.backgroundColor = UIColor.init(hexString: "3faff7")
            btn.addTarget(self, action: #selector(alipayAction), for: .touchUpInside)
            btn.isHidden = false
        }else {
            btn.isHidden = true
        }
    }

    func setModel(model:FakeBankBean,row:Int,isOtherBank:Bool = false){
        var placeholderdata = [String]()
        var rows = row
        if isOtherBank {
            placeholderdata = ["请输入银行名称",
                               "请输入开户预留姓名",
                               "请输入开户银行地址",
                               "银行卡号必须是16至19个数子组成",
                               "请手动输入银行卡"]
        }else {
            placeholderdata = ["请输入开户预留姓名",
                               "请输入开户银行地址",
                               "银行卡号必须是16至19个数子组成",
                               "请手动输入银行卡"]
        }
        
        if rows == 0{
            accessoryType = .disclosureIndicator
            inputTV.isHidden = true
            valueTV.isHidden = false
            valueTV.text = model.value
            textTV.text = model.text
        }else{
            inputTV.isHidden = false
            valueTV.isHidden = true
            accessoryType = .none
            textTV.text = model.text
            inputTV.text = model.value
            inputTV.placeholder = String.init(format:placeholderdata[rows-1]) //model.text
        }
    }
    

}
