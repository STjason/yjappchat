//
//  ConfirmTouzhCell.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/3/22.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

class ConfirmTouzhCell: UITableViewCell {
    
    @IBOutlet weak var playUI:UILabel!
    @IBOutlet weak var numUI:UILabel!
    @IBOutlet weak var beishuUI:UILabel!
    @IBOutlet weak var moneyUI:UILabel!
    @IBOutlet weak var deleteUI:UIButton!
    
    var fromHistory = false //是否是历史注单页跳转来的

    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self)
        
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: playUI)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: numUI)
        setThemeLabelTextColorGlassWhiteOtherBlack(label: moneyUI)
        setThemeLabelTextColorGlassWhiteOtherBlack(label: beishuUI)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /**
     * 某些赔率编码时不显示赔率名称（官方，信用注单页中使用）
     * @param oddCode
     * @return
     */
    func dontShowOddNameWhenOrder(oddCode:String) -> Bool{
        let codes = ["hsymbdwd", "hsembdwd", "qsymbdwd", "qsembdwd","hsanhez","qsanhez","hehzz",
                     "qehzz","zshez","hehzz","rezuxhz","rszuxhz","hszh","qszh","zshz"];
        if codes.contains(oddCode){
            return true
        }
        return false
    }
    
    func setData(data:OrderDataInfo) -> Void {
        if !dontShowOddNameWhenOrder(oddCode: data.oddsCode){
            if fromHistory {
                playUI.text = String.init(format: "%@",data.oddsName)
            }else {
                playUI.text = String.init(format: "%@-%@", data.subPlayName,data.oddsName)
            }
        }else{
            playUI.text = String.init(format: "%@", data.subPlayName)
        }
        numUI.text = !isEmptyString(str: data.numbers) ? data.numbers : "暂无号码"
        beishuUI.text = String.init(format: "%d倍%@", data.beishu,convert_mode(mode: data.mode))
//        moneyUI.text = String.init(format: "%d注%.2f元", data.zhushu,convert_moneyOfMode(mode: data.mode, money: data.money))
        moneyUI.text = String.init(format: "%d注%.2f元", data.zhushu,data.money)
    }
    
    func convert_moneyOfMode(mode: Int, money: Double) -> Double{
        return money / Double(mode)
    }
    
    func convert_mode(mode:Int) -> String {
        if mode == 1{
            return "元模式"
        }else if mode == 10{
            return "角模式"
        }else if mode == 100{
            return "分模式"
        }
        return ""
    }

}
