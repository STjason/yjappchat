//
//  PayListCell.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class PayListCell: UITableViewCell {
    
    @IBOutlet weak var payImg:UIImageView!
    @IBOutlet weak var payText:UILabel!
    @IBOutlet weak var payContents: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self)
//        payContents.lineBreakMode = .byTruncatingTail
        setThemeLabelTextColorGlassWhiteOtherRed(label: payContents)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func setModel(model:Dictionary<String,String>,theme:Bool=true){
        if let imageName = model["img"] {
            if theme{
                payImg.theme_image = ThemeImagePicker.init(keyPath: imageName)
            }else{
                payImg.image = UIImage.init(named: imageName)
            }
            print(imageName)
        }
        payText.text = model["text"]
        configTips(title: model["text"])
    }
    
    func didHtml(strhtml:String)->String{
        let str = strhtml.replacingOccurrences(of:"</span>", with:"")
        var strhtml1="";
        var text:NSString?
        let scanner = Scanner(string: str)
        while scanner.isAtEnd == false {
            scanner.scanUpTo("<", into: nil)
            scanner.scanUpTo(">", into: &text)
            strhtml1 = strhtml.replacingOccurrences(of:"\(text == nil ? "" : text!)>", with: "")
        }
        let str1 = strhtml1.replacingOccurrences(of:"</span>", with:"")
        let str2 = str1.replacingOccurrences(of:" ", with:"")
        return str2
    }
    
    func configTips(title: String?) {
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                let fastTips = config.content.pay_tips_deposit_fast.trimmingCharacters(in: .whitespacesAndNewlines)
                
                switch title
                {
                case "在线支付":
                    payContents.text = didHtml(strhtml: config.content.pay_tips_deposit_third).trimmingCharacters(in: .whitespacesAndNewlines)
                case "微信支付","微信":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_weixin) ? config.content.pay_tips_deposit_weixin : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "支付宝支付","支付宝":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_alipay) ? config.content.pay_tips_deposit_alipay : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "QQ支付","QQ":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_qq) ? config.content.pay_tips_deposit_qq : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "美团":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_meituan) ? config.content.pay_tips_deposit_meituan : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "银行卡转账支付":
                    payContents.text = didHtml(strhtml: config.content.pay_tips_deposit_general).trimmingCharacters(in: .whitespacesAndNewlines)
                case "云闪付":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_yunshanfu) ? config.content.pay_tips_deposit_yunshanfu : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "微信|支付宝":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_weixin_alipay) ? config.content.pay_tips_deposit_weixin_alipay : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "USDT":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_usdt) ? config.content.pay_tips_deposit_usdt : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                case "PAYPAL":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_paypal) ? config.content.pay_tips_deposit_paypal : fastTips).trimmingCharacters(in: .whitespacesAndNewlines)
                    default:
                        print("未识别的类型")
                }
            }
        }
    }
    
    
    
    
    
    
    
    

}
