//
//  AppSettingCell.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/4.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

class AppSettingCell: UITableViewCell {
    
    /** 手势锁开关需刷新列表 回调 */
    var gestureLockBlockStatus:((_ status: Bool) -> Void)?
    @IBOutlet weak var txt:UILabel!
    @IBOutlet weak var exitlogin:UILabel!
    @IBOutlet weak var moreImg:UIImageView!
    @IBOutlet weak var toggle:UISwitch!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txt.theme_textColor = "FrostedGlass.normalDarkTextColor"
        setupNoPictureAlphaBgView(view: self)
    }
    
    @objc func onSwitchAction(view:UISwitch) -> Void {
        print("on switch action ,tag = \(view.tag)")
        print("view is on === \(view.isOn)")
        if view.tag == 0{
            YiboPreference.saveAutoLoginStatus(value: view.isOn)
        }else if view.tag == 1{
            YiboPreference.saveShakeTouzhuStatus(value: view.isOn as AnyObject)
        }else if view.tag == 2{
            YiboPreference.setPlayTouzhuVolume(value: view.isOn as AnyObject)
        }else if view.tag == 3 {
            showAllAnnounceAlert(hideAll: view.isOn ? "off" : "on")
        }else if view.tag == 4 {
            YiboPreference.setShouldOpenFloatTool(value: view.isOn ? "on" as AnyObject : "off" as AnyObject)
            showToast(view: self, txt: "切换成功，请重启应用")
        }else if view.tag == 5{
            UserDefaults.standard.set(view.isOn ? "open" : "close", forKey: "FLOAT_OPEN_PUSH")
        }else if view.tag == 6{
            
            YiboPreference.setFloatingWindowONdata(value: view.isOn ? true : false)
            if YiboPreference.getFloatingWindowONdata() == false {
                popViewRemoveFromSuperview()
            }
        }else if view.tag == 7 {
            YiboPreference.setGESTURE_LOCK(isOpen: view.isOn ? "true" : "false")
            gestureLockBlockStatus!(view.isOn)
        }else if view.tag == 8{
            YiboPreference.setInsideStation(value: view.isOn ? true : false)
        }
    }
    
    private func showAllAnnounceAlert(hideAll: String) {
//        YiboPreference.setAlert_isAll(value: hideAll as AnyObject)
        YiboPreference.setAlert_isAll(value: hideAll as AnyObject)
        YiboPreference.setNotipDate(date: hideAll == "off" ? Date() : nil)
    }
    
}
