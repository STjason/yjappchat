//
//  ShenxiaoCell.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/16.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class RecentResultCell: UITableViewCell {
    
    @IBOutlet weak var qihaoTV:UILabel!
    @IBOutlet weak var numsTV:BallsView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func convert_to_array(nums:String) -> [String]{
        if isEmptyString(str: nums){
            return []
        }
        return nums.components(separatedBy: ",")
    }
    
    func setupData(qihao:String,nums:String,cpCode:String,cpVersion:String,date:Int64 = 0){
        
        let ballWidth:CGFloat = 20
        let small = true        
        var shownums = convert_to_array(nums: nums)
        if !shownums.isEmpty && !shownums.contains("?"){
            //时时彩，快三，低频彩，PC蛋蛋
            if isFFSSCai(lotType: cpCode) || isKuai3(lotType: cpCode) ||
                isDPC(lotType: cpCode) || isPCEgg(lotType: cpCode){
                
                var total = 0//总和
                var bigSmall = ""//大小
                var singleDouble = ""//单双
                for value in shownums {
                    if let v = Int(value){
                        total += v
                    }
                }
                
                let maxTotal = 9*shownums.count//时时彩，快三，低频彩，PC蛋蛋最大号码为9
                if isKuai3(lotType: cpCode) {
                    bigSmall = total <= 10 ? "小" : "大"
                }else if isPCEgg(lotType: cpCode) {
                    bigSmall = total >= 14 ? "大" : "小"
                }else {
                    bigSmall = total > maxTotal/2 ? "大" : "小"
                }
                singleDouble =  total % 2 == 0 ? "双" : "单"
                
                shownums.append("=")
                shownums.append(String(total))
                shownums.append(bigSmall)
                shownums.append(singleDouble)
            }
        }
        qihaoTV.text = String.init(format: "第%@期:", trimQihao(currentQihao: qihao))
        if cpCode == "6" || cpCode == "66" {
            numsTV.basicSetupBalls(nums: shownums, offset: 0, lotTypeCode: cpCode, cpVersion: cpVersion,ballWidth: 30,small: small,gravity_bottom:true,ballsViewWidth: kScreenWidth - 100 - 5 - 5,isBetTopView:true,time:date)
        }else {
            numsTV.basicSetupBalls(nums: shownums, offset: 0, lotTypeCode: cpCode, cpVersion: cpVersion,ballWidth: ballWidth, small:small,gravity_bottom:false,ballsViewWidth: kScreenWidth - 100 - 5 - 5,forBetPageBelow: true)
        }
        
        
    }
    
    
    static func isLeopard(values:[String]) -> Bool { // trun = 豹子
        var allValueEqual = false
        for (index,value) in values.enumerated()
        {
            if index > 0
            {
                allValueEqual = value == values[index - 1]
                if !allValueEqual
                {
                    break
                }
            }
        }
        return allValueEqual
    }
    
    
}
