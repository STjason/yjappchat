//
//  WinLostWraper.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/3/14.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class LhcServerTimeWraper: HandyJSON {
    
    var success:Bool = false;
    var msg:String = "";
    var code:Int = 0;
    var accessToken:String = "";
    var content:Int64 = 0;
    
    required init(){}
    
}
