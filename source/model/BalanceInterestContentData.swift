//
//  BalanceInterestContentData.swift
//  gameplay
//
//  Created by ken on 2019/3/26.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class BalanceInterestContentData: HandyJSON {
    required init() {
        
    }
    var scaleStr = ""
    var money = ""
    var incomeEnd = ""
    var scaleEnd = ""
    var incomeMon = ""
    var incomeStr = ""
    var incomeYear = ""
    var startTime = ""
    var endTime = ""
    var totalRateMoney = ""
    var sevendayRate = ""
    var yesterdayEarnMoney = ""
    
//    private String scaleStr;
//    private String money;//总金额
//    private String incomeEnd;
//    private String scaleEnd;//万份收益
//    private String incomeMon;//预计月收益
//    private String startTime;
//    private String endTime;
//    private String incomeStr;
//    private String totalRateMoney;//累计收益
//    private String sevendayRate;//七日年化利率
//    private String yesterdayEarnMoney;//昨日收益
//    private String incomeYear;//预计年收益
}
