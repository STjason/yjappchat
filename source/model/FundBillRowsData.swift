//
//  FundBillRowsData.swift
//  gameplay
//
//  Created by ken on 2019/3/28.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class FundBillRowsData: HandyJSON {
    required init() {
        
    }
    var accountId:NSInteger?
    var balance:Double?
    var betNum:NSInteger?
    var id:NSInteger?
    var income = ""
    var scale:Float?
    var statDate=""
    var username=""
    var stationId:NSInteger?
    var status:NSInteger?
}

//"accountId":192,
//"balance":1026012.6193,
//"betNum":100000,
//"id":21,
//"income":114.9134,
//"scale":1.12,
//"statDate":"2019-03-27",
//"stationId":2,
//"status":1,
//"username":"wnkjch001"
