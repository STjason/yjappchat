//
//  AllRateBack.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/28.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class AllRateBack: HandyJSON {

    required init() {}
    
    var shabaArray:[RebateBean] = []
    var lotteryArray:[RebateBean] = []
    var sportArray:[RebateBean] = []
    var realArray:[RebateBean] = []
    var egameArray:[RebateBean] = []
    var chessArray:[RebateBean] = []
    var esportArray:[RebateBean] = []
    var fishingArray:[RebateBean] = []
    var current_other_kickback:CurrentOtherKickback =  CurrentOtherKickback()
    var lot_kickback:String = ""
}

class CurrentOtherKickback: HandyJSON {
//    "accountId": 18569,
//    "chessScale": 0,
//    "egameScale": 2,
//    "esportScale": 2,
//    "fishingScale": 2,
//    "realScale": 2,
//    "shabaSportScale": 2,
//    "sportScale": 2,
//    "stationId": 5,
//    "thirdLotScale": 2
    
    required init() {}
    /** 用户ID*/
    var accountId: Int64 = 0
    /** 电子返点 */
    var egameScale: String = ""
    var chessScale: String = ""
    /** 真人返点 */
    var realScale: String = ""
    /** 沙巴体育返点返点 */
    var shabaSportScale: String = ""
    /** 体育返点返点 */
    var sportScale: String = ""
    var stationId:String = ""
    var esportScale = ""
    var fishingScale = ""
    var thirdLotScale = ""
}
