//
//  UnreadMsgWraper.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/10/10.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class UnreadMsgWraper: HandyJSON {

    required init() {
        
    }
    var content:UnreadMsg?
    var success:Bool!
    var msg:String?
    var code:Int = 0
    var accessToken:String?
    
}
