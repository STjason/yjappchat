//
//  FakeDataWraper.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/9/26.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class FakeDataWraper: HandyJSON {
    
    required init() {
        
    }

    var content:[NewFakeData]?
    var success:Bool!
    var msg:String?
    var code:Int = 0
    var accessToken:String?
    
}
