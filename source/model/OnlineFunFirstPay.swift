//
//  OnlineFunFirstPay.swift
//  gameplay
//
//  Created by admin on 2019/4/26.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class OnlineFunFirstPayWraper: HandyJSON {
    required init() {}
    
    var onlineList = [OnlineFunFirstPay]()
    var bankList:[String] = [String]()
}


/// 在线支付--先支付方法后通道版本，支付通道数据
class OnlineFunFirstPay: HandyJSON {
    required init() {}
    
//    "alterNative": "",
    var alterNative = ""
//    "bankList": "bankcodes|n|WEIXINH5,ALIPAYH5,JDPAYH5,QQPAYH5,UNIONH5|pcGateway,pcScan|h5Scan,h5h5",
    var bankList = ""
//    "fixedAmount": "100,200,300",
    var fixedAmount = ""
//    "icon": "/common/member/center/images/onlinepay/rbankdefault.png",
    var icon = ""
//    "id": 136,
    var id = 0
//    "isFixedAmount": 3,
    var isFixedAmount = ""
//    "max": 100000000,
    var max:Int = 0
//    "min": 0,
    var min:Int = 0
//    "payAlias": "",
    var payAlias = ""
//    "payName": "快支付",
    var payName = ""
//    "payPlatformCode": "EASYPAY",
    var payPlatformCode = ""
//    "payType": "WEIXINH5",
    var payType = ""
//    "url": ""
    var url = ""
//    提示语
    var appRemark = ""
}
