//
//  FeeConverPlatformItem.swift
//  gameplay
//
//  Created by Gallen on 9/3/2020.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class FeeConverPlatformItem: HandyJSON {
    
    var success:Bool = false
    var accessToken = ""
    var content:[FeeConverContentItem] = [FeeConverContentItem]()
    var msg = ""
    
    required init(){}

}

class FeeConverContentItem:HandyJSON{
    var name = ""
    var platform = ""
    required init(){}
}
