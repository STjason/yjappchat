//
//  FeeConvertRecordWrapper.swift
//  gameplay
//
//  Created by admin on 2019/1/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class FeeConvertRecordWrapper: HandyJSON {
    var success = false
    var msg = ""
    var accessToken = ""
    var code = ""
    var content:FeeConvertRecordContent?
    
    required init() {}
}

class FeeConvertRecordContent: HandyJSON {
    var currentPageNo = ""
    var hasNext = ""
    var hasPre = ""
    var nextPage = ""
    var pageSize = ""
    var prePage = ""
    
    var rows = [FeeConvertRecordRowModel]()
    
    required init() {}
}

class FeeConvertRecordRowModel: HandyJSON {
    var accountId = ""
    var createDatetime:Int64 = 0
    var id = ""
    var money = ""
    var orderId = ""
    var platform = ""
    var stationId = ""
    var status = ""
    var type = ""
    var username:String = ""
    
    required init() {}
}
