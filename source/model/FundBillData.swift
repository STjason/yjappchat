//
//  FundBillData.swift
//  gameplay
//
//  Created by ken on 2019/3/28.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class FundBillData: HandyJSON {
    required init() {
        
    }
    var aggsData:FundBillAggsData?
    var currentPageNo:NSInteger?
    var hasNext:Bool!
    var hasPre:Bool!
    var nextPage:NSInteger?
    var pageSize:NSInteger?
    var prePage:NSInteger?
    var rows:[FundBillRowsData]?
    var start:NSInteger?
    var total:NSInteger?
    var totalPageCount:NSInteger?
}
