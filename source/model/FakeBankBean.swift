//
//  FakeBankBean.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/21.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class FakeBankBean: NSObject {
    
    var text:String = ""
    var value:String = ""
    var placeholder = ""
    var code = ""
    var label = ""
    var aliQrcodeLink = ""
    var bankCode = ""
    var card = ""
    //以下为注册管理功能用到的参数
    var eleName = ""
    var eleType = 0
    var id = 0
    var name = ""
    var platform = 0
    var required = 0
    var show = 0
    var sortNo = 0
    var stationId = 0
    var uniqueness = 0
    var validate = 0
    var regex = ""
}
