//
//  VisitRecords.swift
//  gameplay
//
//  Created by William on 2018/7/24.
//  Copyright © 2018年 yibo. All rights reserved.
//

import HandyJSON

@objcMembers
@objc class VisitRecords :NSObject, HandyJSON{
    required override init() {}
    
    var userName: String?//彩票名字
    var cpName: String?//彩票名字
    var czCode: String?//彩票类型
    var ago: String? //时间差
    var cpBianHao: String?//彩票编号
    var lotType: String?//时间类型
    var lotVersion: String?//彩票版本
    var num: String? //这种彩票总点击次数
    var icon: String? //彩票icon名

}
