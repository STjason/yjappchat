//
//  PushNewAnnouncementModel.swift
//  gameplay
//
//  Created by block on 2018/11/21.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
/** 最新公告模型 */
class PushNewAnnouncementModel: HandyJSON {

    var bet:Bool?
    var code = ""
    var content = ""
    var deposit:Bool?
    var id:Int?
    var index:Bool?
    var overTime = ""
    var reg:Bool?
    var stationId = ""
    var status = ""
    var title = "";
    var updateTime = ""
    
    required init() {
        
    }
}
