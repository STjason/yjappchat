//
//  PushDiscountsModel.swift
//  gameplay
//
//  Created by block on 2018/11/21.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

/** 优惠活动推送 */
class PushDiscountsModel: HandyJSON {

    var content = ""
    var id:Int?
    var overTime = ""
    var stationId = ""
    var status = ""
    var title = ""
    var titleImgUrl = ""
    var updateTime = ""
    
    
    
    required init() {
        
    }
}
