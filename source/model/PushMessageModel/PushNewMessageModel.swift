//
//  PushNewMessageModel.swift
//  gameplay
//
//  Created by block on 2018/11/21.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
/** 新消息模型 */
class PushNewMessageModel: HandyJSON {
    var createTime = ""
    var id:Int?
    var message = ""
    var receiveMessageId = ""
    var receiveType = ""
    var sendAccount = ""
    var sendType = ""
    var stationId = ""
    var status = ""
    var title = ""

    required init() {
        
    }
}
