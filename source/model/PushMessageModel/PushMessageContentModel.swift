//
//  PushMessageContentModel.swift
//  gameplay
//
//  Created by block on 2018/11/21.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

/** 内容拆分模型 */
class PushMessageContentModel: HandyJSON {
    
    var code:Int = 0
    var data:Dictionary<String, Any>?
    var sort:String = ""
    
    required init() {
        
    }
}
