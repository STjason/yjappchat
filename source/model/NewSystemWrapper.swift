//
//  NewSystemWrapper.swift
//  gameplay
//
//  Created by admin on 2018/9/27.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class NewSystemWrapper: HandyJSON {
    
    var content:NewSystemConfig!
    var success:Bool!
    var msg:String?
    var accessToken:String?
    
    required init() {}
}
