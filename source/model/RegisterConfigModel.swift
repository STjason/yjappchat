//
//  RegisterConfigModel.swift
//  gameplay
//
//  Created by JK on 2020/6/22.
//  Copyright © 2020 yibo. All rights reserved.
//

import Foundation
import HandyJSON

class RegisterConfigModel : HandyJSON{
    
    required init() {}
    
    var success: Bool = false
    var accessToken: String = ""
    var code:Int = 0
    var content:[RegisterConfigContentModel]?
    var msg:String = ""
}

class RegisterConfigContentModel : HandyJSON{
    required init() {}
    
    var eleName = ""
    var eleType = 0
    var id = 0
    var name = ""
    var platform = 0
    var required = 0
    var show = 0
    var sortNo = 0
    var stationId = 0
    var uniqueness = 0
    var validate = 0
    var regex = ""
}
