//
//  OfflineChargeResult.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/9/17.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class OfflineChargeResult: HandyJSON {

    var orderid = ""
    var fycode = ""
    var remark = ""
    
    required init() {
        
    }
    
}
