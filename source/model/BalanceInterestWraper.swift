//
//  BalanceInterestData.swift
//  gameplay
//
//  Created by ken on 2019/3/26.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class BalanceInterestWraper: HandyJSON {
    required init() {
        
    }
    var content:BalanceInterestContentData?
    var success:Bool!
    var msg = "";
    var code = 0
    var accessToken = "";
}
