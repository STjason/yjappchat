//
//  BaseWrapper.swift
//  gameplay
//
//  Created by admin on 2018/10/4.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class BaseWrapper: HandyJSON {
    
    required init() {
        
    }
    
    var success: Bool = false
    var accessToken: String = ""
    var code:Int = 0
    var content = false
    var msg:String = ""
}
