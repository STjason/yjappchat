//
//  LHCSpecialData.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/7/10.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class LHCSpecialData: HandyJSON {

    required init() {
        
    }
    
    var code = ""
    var name = ""
//    var headrCoder = "" //标识当前小玩法，如连码中的四全中为 "sqz"
    var data:[LHCSpecialData]?
    
}
