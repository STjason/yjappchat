//
//  MemInfoWraper.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/10.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class MemInfoWraper: HandyJSON {

    required init(){}
    var success:Bool=false;
    var msg = "";
    var code = -1;
    var accessToken = "";
    var content:Meminfo?;
    
}

class VipLevelMoel: NSObject {
    
    /** 当前存款总额 */
    var accDepositTotal : String?
    /** 当前等级名称 */
    var curLevelName : String?
    /** 当前等级需存款金额 */
    var curLevelDepositMoney : String?
    /** 下一等级名称 */
    var newLevelName : String?
    /** 下一等级 需存款金额 */
    var newLevelDepositMoney : String?
    
    func getCurrentJson(dic: NSDictionary) -> VipLevelMoel {
        let model = VipLevelMoel()
        
        model.accDepositTotal       = String(format: "%@", dic.kValue(forKey: "accDepositTotal") as CVarArg)
        model.curLevelName          = String(format: "%@", dic.kValue(forKey: "curLevelName") as CVarArg)
        model.curLevelDepositMoney  = String(format: "%@", dic.kValue(forKey: "curLevelDepositMoney") as CVarArg)
        model.newLevelName          = String(format: "%@", dic.kValue(forKey: "newLevelName") as CVarArg)
        model.newLevelDepositMoney  = String(format: "%@", dic.kValue(forKey: "newLevelDepositMoney") as CVarArg)
        
        return model
    }
    
}
