//
//  SystemConfig.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2017/12/13.
//  Copyright © 2017年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class SystemConfig: HandyJSON {
    var on_off_register_sms_verify="off";//注册时开启手机号码短信验证
    var single_honest_play_force_office_text = "off" //单信用玩法时将信用玩法文字强制为官方玩法
    var recaptcha_verify_type = ""//行为验证码类型
    var on_off_recaptcha_verify_hide_code = "off" //开启行为验证后隐藏验证码
    var on_off_recaptcha_verify = "off" //开启站点行为验证
    
    var native_chat_room = "on" //是否显示原生聊天室
    var notice_list_dialog_expand_first_notice = "on"//首页公告弹窗列表是否默认展示第一条公告内容
    var on_off_money_income = "off";//余额生金开关
    
    var onoff_mobile_guest_register:String = "";//注册时是否显示访客试玩入口开关
    //    String onoff_lottery_record;//彩票投注记录开关
    //    String onoff_six_record;//六合投注记录开关
    //    String onoff_real_record;//真人投注记录开关
    //    String onoff_electronic_record;//电子投注记录开关
    //    String onoff_sport_record;//体育投注记录开关
//    var onoff_sign_in:String = "";//签到开关
    var app_qr_code_link_ios:String = "";//IOS版本app二维码地址
    var app_qr_code_link_android:String = "";//Android版本app二维码地址
    var lottery_order_cancle_switch:String = "";//彩票撤单开关
    var lottery_order_chase_switch:String = "";//彩票追号开关
    var onoff_change_money:String = "";//帐变记录开关
    var onoff_avia_game: String = "";//电竞注单开关
    var onoff_mobile_app_reg:String = "";//注册app单独开关
    var onoff_lottery_game:String = "";//彩票游戏开关
    var onoff_liu_he_cai:String = "";//六合彩开关
    var onoff_sports_game:String = "";//皇冠体育开关
    var onoff_zhen_ren_yu_le:String = "";//真人娱乐开关
    var onoff_dian_zi_you_yi:String = "";//电子游艺开关
    var iosExamine:String = "";//手机页面ios审核是否关闭
    var mobileIndex:String = "";////手机主页设置。v1－突出彩票，v2－突出真人,v3-突出体育
    var yjf:String = "";//元角分模式
    
    var onoff_member_mobile_red_packet:String = "";//手机抢红包开关
    //手机页面中显示取款按钮
    var onoff_mobile_drawing:String = "";
    //手机页面中显示存款按钮
    var onoff_mobile_recharge:String = "";
    //版本号
    var version:String = "";
    var customerServiceUrlLink:String = ""; //在线客服？
    var online_service_open_switch = "v1" //在线客服打开方式 v1--浏览器打开 v2-应用内部打开
    var online_customer_showphone:String = "";
    
    var bankFlag:String =  "";
    var fastFlag:String = "";
    var onlineFlag:String = "";
    var exchange_score:String = "";//积分兑换开关
    var isActive:Bool!;//是否显示优惠活动
    var app_download_link_ios:String = "";
    var app_download_link_android:String = "";
    var lottery_page_logo_url:String = ""//网站Logo地址
    var station_name:String = ""//网站名称
    var login_page_logo_url: String = "" // 新的登录注册log地址
    var onoff_turnlate = "";//大转盘开关
    
    var native_style_code = "";//主页风格code
    /**1.分组模式、2.经典模式、3.二级分栏模式*/
   // var main_style_type = ""
    /**二级分栏模式 on图文模式*/
    var switch_optimize_mainpage_tabs = "on"
    var member_bg_value = "";//会员中心头部背景图片
    var member_page_logo_url = "";//个人中心头像的地址
    var member_page_bg_url = "";//个人中心头部背景
    var bet_page_style_type = "";//投注页主题风格色系
    
    var score_show="";//显示积分开关
    var onoff_sport_switch = "off";//体育开关
    var onoff_sb_switch = "off";//沙巴体育开关
    var lottery_version = "";//彩票版本选择
    var fee_convert_switch = "";//额度转换
    var fast_money_setting = "";//快选金额字符串，以逗号分隔
    var allow_muti_bankcards_switch = ""//添加多张银行卡开关
    var tip_for_multi_browser_pay = ""
    
    var mainPageVersion = ""//主页版本配置，默认“V1” v2 版本-存款，v3 主页推广链接，v4 主页额度转换，v5 主页余额生金 ， v6聊天室，V11纯电竞版，v12纯体育版本
    
    var onoff_register = "on";//注册开关
    var native_register_switch = "on"//注册开关
    var switch_back_computer = "on";//返回电脑端开关
    var switch_back_wap = "on";//返回wap开关
    var switch_sign_in = "on";//签到开关
    var switch_chatroom = "off"; //聊天室开关
    var switch_money_change = "on" //额度转换开关
    var switch_level_show = "on" //会员等级显示开关
//    var default_lot_code = "on" // 默认彩种编号
    var show_welcome_pages = "on" // 显示欢迎页面
    var lottery_dynamic_odds = "on"//彩票动态赔率开关
    var pc_sign_logo = "";//签到url
    var multi_broswer_switch = "off";//充值多浏览器开关
    var fixed_rate_model = "off"; // 固定模式所有层级返点一样默认"off"
    /** 主页模块排序(1-官方,2-信用,3-真人,4-电子,5-体育,6-棋牌，7红包 8 电竞 捕鱼) */
    var mainpage_module_indexs = "7,1,2,3,4,5,6,8,9 "
    
    // 用于崩溃收集，区别站点
    var station_id = ""
    var stationCode = "" //站点，如 "v023"
    var withdraw_time_one_day: Int?;
    var switch_xfwf = "off" // on选择信用 off选择官方
    var mobile_station_index_logo = ""
    var float_chat_bar_switch = "on" // 投注页聊天室浮动入口
    var mobile_station_app_logo = "on"; // APP下载页是否显示LOGO
    var modify_person_info_after_first_modify_switch = "off";//个人资料填写后不能再修改
    var waitfor_openbet_after_bet_deadline = "off";//原生封盘时允许开始进入下一期投注
    var native_float_funcbar_switch = "on" // 悬浮案辅助功能按钮开关
    var show_rateback_when_zero = "on" //返点比例为0时是否需要显示
    var nb_chess_showin_mainpage: String = "off" //是否独立显示 NB棋牌
    var gesture_pwd_switch = "on";//手势密码开关
    var switch_nb_game:String = "off" // 棋牌开关
    /**红包开关*/
    var native_mainpage_rp_switch = ""
    var show_lottrack_menu = "off";//个人中心是否要关闭追号查询，"on"--关闭，"off"-显示
    /** 首页是否显示 官信体真 标志 */
    var game_item_circle_little_badge_switch = "on"
    /** 快速入款充值金额是否需要补随机小数 */
//    var fast_deposit_add_random = "off"
    /**新快速入款充值金额是否需要补随机整数或小数*/
    //0是关闭 1是随机增加1-5元 2是随机增加1-5的两位小数
    var fast_deposit_add_money_select = 0
    /** 手机、邮箱、微信、QQ启用开关。默认开启 */
    var switch_communication = "on"
    /** 快三豹子算大小单双 */
    var k3_baozi_daXiaoDanShuang = "off"
    /** 赛车冠亚和11为小 */
    var bjsc_guanyahe_11 = "off"
    /** pc蛋蛋大小单双[13,14]算和 */
    var pcdd_daXiaoDanShuang_1314 = "off"
    /** 11选5总和大小单双[29,30]算不中奖 */
    var syxw_zongHe_dxds_29_30 = "off"
    /**  若开关打开则投注记录列表显示撤单 */
    var lottery_revoke_order_switch = "off"
    /** 是否显示倍数调整框*/
    var show_beishu_simple_betpage = "off"
    /** 是否显示元角分调整框 */
    var show_yjfmode_simple_betpage = "off"
    /** 绑定银行卡，是否需要验证手机绑定 */
    var add_bank_perfect_contact = "on"
    /** 简约版投注页返水条是否隐藏开关  */
    var hide_kickbackbar_simple_betpage = "on"
    var charge_icon_wap_switch = "on"; //是否使用网页版充值图标
    var pay_whether_switch_remarks = "off"//是否开启快速入款备注
    /** 是否使用扁平风格六合彩图标 */
    var use_lhc_optimise_icons = "off"
    var allnumber_switch_when_register="off"//是否允许全数字帐号注册
    /** 选择弹窗不再弹出，是否次日再次弹出 */
    var show_noticewindow_when_click_notip = "off"
    /** 信用版投注注单页底部快捷金额是否显示 */
    var honest_betorder_page_fastmoney_switch = "on"
    var unread_point_showin_membertab = "on" //tab上会员中心，未读消息数默认显示
    var show_title_time_in_active_item = "on";//优惠活动默认打开显示
    var switch_lottery = "off" //关闭彩票开关
    var switch_member_to_proxy = "off" //默认关
    var show_min_odd_when_bet = "off" //是否显示最小赔率
    var reset_yjfmode_after_bet = "on" //投注成功后是否需要重置元角分模式
    var active_open_in_dialog = "off" // on公告弹框形式展现 off跳转页面
    var switch_continue_bet_in_detail_betorder = "off"//注单详情中是否显示继续下注按钮
    var switch_webpay_guide = "on" //开启网站入款指南配置
    
    //MARK: - 支付说明
    /////////////////////////////////////
    /** 快速入款支付说明 */
    var pay_tips_deposit_fast = ""
    /** 一般入款支付说明 */
    var pay_tips_deposit_general = ""
    /** 一般入款提交页支付说明*/
    var pay_tips_deposit_general_detail = ""
    /** 一般入款温馨提示 */
    var pay_tips_deposit_general_tt = ""
    /** 快速入款支付说明 */
    var pay_tips_deposit_fast_tt = ""
    /** 第三方支付说明 */
    var pay_tips_deposit_third = ""
    /** 支付宝入款支付说明 */
    var pay_tips_deposit_alipay = ""
    /** 微信入款支付说明 */
    var pay_tips_deposit_weixin = ""
    /** QQ入款支付说明 */
    var pay_tips_deposit_qq = ""
    /** 云闪付入款支付说明 */
    var pay_tips_deposit_yunshanfu = ""
    /** 美团入款支付说明 */
    var pay_tips_deposit_meituan = ""
    /** 微信支付宝入款支付说明 */
    var pay_tips_deposit_weixin_alipay = ""
    /** PAYPAL入款支付说明 */
    var pay_tips_deposit_paypal = ""
    /** USDT入款支付说明 */
    var pay_tips_deposit_usdt = ""
    //MARK: - 版本控制
    /////////////////////////////////////
    // V1原生版本支付页面， V2 wap版本支付页面
    var native_charge_version = "V1"
    /** V1先支付方法，V2先支付通道 */
    var mobilepay_version = "V1"
    
    var rateback_step_offset = "0.1"; //默认0.1
    
    var play_introduce_show_style = "V1";
    /** 开奖结果版本 */
    var switch_openresult_version = "V1"
    
    var go_downpage_when_update_version = "off";//是否去下载页更新下载app
    /** 多久获取一次本地推送消息   分钟单位 */
    var switch_push_interval = "2";
    var switch_chess = "on";//chess toggle
    var use_new_lottery_groud_icons = "on";//是否使用彩票分组新图标
    var withdraw_page_introduce = ""
    /** 会员中心页面风格：1：分组、2：经典 */
    var member_center_style_type = "1"
    /** 会员中心分组排序 */
    var membercenter_group_sorts = "1,2,3,4,5,6,7"
    //v1---app模式，v2-pc模式
    var promp_link_mode_switch = "v1"
    //"v1": "分组模式", "v2": "经典模式"
    var charge_page_mode_switch = "v1"
    var qq_custom_server_url = "" // QQ在线客服
    var front_show_member_to_agent_switch = "on" //前端是否显示会员转代理
    var switch_pt_egame = "off";//PT电子游戏开关
    var mobile_bet_qrcode = "off";//投注页面手机扫码下注二维码
    var native_usual_game_switch = "on" //常玩游戏开关
    var welcome_page_startapp_text = ""
    var native_default_color_theme = "red" //默认主题色
    
    var switch_mainpage_version = "V1" //主页版本，v2 版本-存款，v3 主页推广链接，v4 主页额度转换，v5 主页余额生金 未用？ v6 聊天室
    
    var onoff_payment_show_info = "on"  ////是否显示支付二维码；

    var rob_redpacket_version = "v2"  // ("抢红包页面风格。v1－经典风格，v2－优化风格"),默认"v2"
    
    var multi_list_dialog_switch = "off" //平台公告版本开关

    var foreign_chat_room_url = "" //外部聊天室开关
    
    var open_bet_website_enter = "" // 主页+号里开奖网入口 url
    var redpacket_float_inmainpage = "on" // 主页是否显示红包入口图标 - OK
    var default_lot_code = "CQSSC" //开奖默认彩种  彩种编号
    var switch_nb_game_redpacket = "off" //NB棋牌红包
    
    /// 19-09-14 add JK 中秋活动开关
    var switch_mid_autumn = "off"
    
    
    /// 强制更新
    var force_install_when_new_version = "off"
    /// 每日加奖
    var one_bonus_onoff = "off"
    /// 周周转运
    var week_deficit_onoff = "off"
    /// 是否需要临时活动开关
    var show_temp_activity_switch = "off";
    /// 第三方免额度转换开关
    var third_auto_exchange = "off"
    /// 首页显示在线人数开关
    var switch_mainpage_online_count = "off"
    ///真人电子跳转外部浏览器打开
    var zrdz_jump_broswer = "on"
    
    /** 追加在线人数 */
    var online_count_fake = "0"
    /**充值卡开关*/
    var recharge_card_onoff = "on"
    /**代金券开关*/
    var coupons_onoff = "on"
    
    /** 主页电子竞技 */
    var switch_esport = "off"
    /** 主页捕鱼游戏 */
    var switch_fishing = "off"
    /**  优惠活动大厅 */
    var onoff_application_active = "off"
    var on_off_mobile_verify_code = "off" //登录是否需要验证码
    //首页彩票图文显示，默认打开
    var lottery_show_mode_switch = "on"
    // USDT汇率
    var pay_tips_deposit_usdt_rate = ""
    // USDT教程链接
    var pay_tips_deposit_usdt_url = ""
    //首页标题显示，默认打开
    var switch_native_show_station_name = "on"
    //是否开启USDT提款 默认“on”
    var onoff_usdt_withdraw = "on"
    required init() {}
    
}
