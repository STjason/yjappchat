//
//  UnreadMsg.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/10/10.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class UnreadMsg: HandyJSON {
    
    required init() {
        
    }
    var count = 0
    var push_code = 0

}
