//
//  FeeManagement.swift
//  gameplay
//
//  Created by ken on 2019/3/17.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class FeeManagement: HandyJSON {
    required init() {}
    var drawNum = 0
    var feeType = 0   //手续费类型 1:固定手续费 2:浮动手续费
    var feeValue = 0  //手续费值
    var id = 0
    var lowerLimit = 0
    var remark = ""
    var stationId = 0
    var status = 0
    var upperLimit = 0
}

/* 后端数据
 "drawNum":2,
 "feeType":2,
 "feeValue":10,
 "id":12,
 "lowerLimit":5,
 "remark":"测试",
 "stationId":2,
 "status":2,
 "upperLimit":1000
 test
 */

