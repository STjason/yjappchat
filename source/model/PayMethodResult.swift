//
//  PayMethodResult.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/29.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class PayMethodResult: HandyJSON {

    required init(){}
    
    var online:[OnlinePay] = [];
    var fast:[FastPay] = [];//微信支付方式，可能有多个
    var fast2:[FastPay] = [];//支付宝支付方式，可能有多个
    var fast_qq:[FastPay] = []//腾讯QQ支付，可能有多个
    var fast_ysf:[FastPay] = []//云闪付
    var fast_meituan:[FastPay] = []//美团闪付
    var weixin_alipay:[FastPay] = []//微信|支付宝
    var bank:[BankPay] = [];//银行卡支付方式，可能有多个
    var USDT:[FastPay] = [];//USDT
    var PAYPAL:[FastPay] = [];//PAYPAL
    var minMoney = "";
    var serverStartTime = "";
    var serverEndTime = "";
}

class PayMethodV2Result: HandyJSON {
    
    required init(){}
    
    var online:[OnlinePay] = [];
    var fast:[FastPayInfo] = [] //快速支付方式，可能有多个
    var bank:[BankPay] = [] //银行卡支付方式，可能有多个
    var minMoney = "";
    var serverStartTime = "";
    var serverEndTime = "";
}

class FastPayInfo: HandyJSON {
    required init(){}
    
    var payChildren:[FastPay] = []
    var payGroup:PayGroup = PayGroup()
}

class PayGroup: HandyJSON {
    required init() {}
    var sortNo = ""
    var code = ""
    var payName = ""
}
















