//
//  UnreadMsgWrapper.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2017/12/3.
//  Copyright © 2017年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class QrcodeLoginWraper: HandyJSON {

    var content:QrcodeLoginBean!
    var success:Bool!
    var msg:String = ""
    var accessToken:String?
    
    required init() {}
    
}
