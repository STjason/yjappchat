//
//  SportDataWraper.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/20.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class NewSportResult: HandyJSON {
    
    var totalPage = 0;
    var counts:SportGameCount?
    var games:[[Dictionary<String,String>]]?
    required init(){}
    
}
