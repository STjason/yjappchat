//
//  WechatQRCodeWraper.swift
//  gameplay
//
//  Created by admin on 2018/10/20.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class WechatQRCodeWraper: HandyJSON {
    var msg = ""
    var success = false
    var content:[WechatContent]?
    var accessToken = ""
    
    required init() {
        
    }
}

class WechatContent: HandyJSON {
    
    var id = ""
    var imgType = ""
    var showPage = ""
    var showPosition = ""
    var afsList: [WechatQRCodeModel]?
    required init() {
        
    }
}

class WechatQRCodeModel: HandyJSON {
    var imgUrl = ""
    var linkUrl = ""
    var imgSort = ""
    var linkType = ""
    
    required init() {
    }
}
