//
//  SportGameCount.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/20.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class SportGameCount: HandyJSON {
    var tD_BK = 0;//今日赛事-篮球
    var fT_BK = 0;//早盘-篮球
    var rB_BK = 0;//滚球-篮球
    var tD_FT = 0;//今日寒事-足球
    var fT_FT = 0;//早盘-足球
    var rB_FT = 0;//滚球-足球
    
    required init(){}
}
