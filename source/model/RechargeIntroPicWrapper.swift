//
//  RechargeIntroPicWrapper.swift
//  gameplay
//
//  Created by admin on 2019/6/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class RechargeIntroPicWrapper: HandyJSON {
    
    required init() {
        
    }
    
    var success: Bool = false
    var accessToken: String = ""
    var code:Int = 0
    var content = [String]()
    var msg:String = ""
}
