//
//  UserListSmallBean.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class QrcodeLoginBean: HandyJSON {

    required init() {
        
    }
    var accountId:Int64 = 0
    var lotCode:String = ""
    var lotVersion:String = ""
    var lotGroup:String = ""
    var lotType:Int = 0
    var accountType:Int = 0
}
