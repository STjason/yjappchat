//
//  NewFakeData.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/9/26.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class NewFakeData: HandyJSON {
    
    required init() {
        
    }
    
    var id:Int64 = 0
    var username:String = ""
    var winMoney:Float = 0
    var win_time:Int64 = 0
    var type:Int = 0
    var stationId:Int64 = 0
    var itemName:String = ""
    var account:String = ""
    var money:Float = 0
    

}
