//
//  GameItemResult.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/24.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class GameItemResult: HandyJSON {

    var ButtonImagePath = ""
    var DisplayName = ""
    var typeid = ""
    var LapisId = ""
    var single = 0
    
    var buttonImagePath = ""
    var displayEnName = ""
    var img:String = ""
    var lapisId:String = ""
    var name:String = ""
    var url:String = ""
    /**直接跳转游戏*/
    var forwardUrl:String = ""
    
    required init(){}
}
