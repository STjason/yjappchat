//
//  LotteryPathLogic.swift
//  gameplay
//
//  Created by admin on 2018/11/6.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LotteryPathLogic: NSObject {
    
    private var dataSourceItems = [FormatPathDataItem]()
    private var maxRowX = 0
    private var minRowX = 0
    private var tdHasValueNotes = [String]()
    private var pageIndex = 0
    private var lotType = ""

    func getFormatedItems(datas:[BcLotteryData],lotType:String,superTitle:String = "",subTitle:String)
    {
        if lotType == "1" || lotType == "2" || lotType == "5"
        {
            if superTitle == "第一球"
            {
                
            }
        }
    }

    
    //MARK: 单双
    private func reloadBallParity(datas:[BcLotteryData],index:Int,lotType:String)
    {
        let lotteryAllParities = getParityWithNums(datas: datas, index: index)
        dataSourceItems = formatTilesWithModel(nums: lotteryAllParities)
    }
    
    //MARK: 获得单双数据
    private func getParityWithNums(datas:[BcLotteryData],index:Int) -> [String]
    {
        let lotteryAllNums = getNumsWithIndex(datas: datas,index: pageIndex)
        var nums = [String]()
        for (_,value) in lotteryAllNums.enumerated()
        {
            if let num = Int(value)
            {
                nums.append(num % 2 == 0 ? "双" : "单")
            }
        }
        
        return nums
    }
    
    /// 获取开奖结果中的，所有 index上的号码 的数组
    ///
    /// - Parameter index: 第index 个开奖号码的数组
    private func getNumsWithIndex(datas:[BcLotteryData],index:Int) -> [String]
    {
        var lotteryAllNums = [String]()
        for (_,value) in datas.enumerated()
        {
            let numStr = value.haoMa
            
            let Nums = numStr.components(separatedBy: ",")
            lotteryAllNums.append(Nums[index])
        }
        
        return lotteryAllNums
    }
    
    //路子显示的模型逻辑
    private func formatTilesWithModel(nums:[String]) -> [FormatPathDataItem]
    {
        var models = [FormatPathDataItem]()
        for (index,value) in nums.enumerated()
        {
            let model = FormatPathDataItem()
            model.title = value
            
            if index == 0
            {
                model.tdArray = [0,0]
                models.append(model)
                
            }else
            {
                let lastModel = models[index - 1]
                let td = lastModel.tdArray
                
                if td[1] == 0
                {
                    minRowX =  td[0]
                }
                
                if value == lastModel.title
                {
                    if td[1] == 5
                    {
                        model.tdArray = [td[0] + 1,5]
                    }else
                    {
                        if tdHasValueNotes.contains("\(td[0])\(td[1] + 1)")
                        {
                            model.tdArray = [td[0] + 1,td[1]]
                        }else
                        {
                            model.tdArray = [td[0],td[1] + 1]
                        }
                    }
                }else
                {
                    model.tdArray = [minRowX + 1,0]
                }
                
                maxRowX = model.tdArray[0] > maxRowX ? model.tdArray[0] : maxRowX
                models.append(model)
                tdHasValueNotes.append("\(model.tdArray[0])\(model.tdArray[1])")
            }
        }
        
        return models
    }
}
