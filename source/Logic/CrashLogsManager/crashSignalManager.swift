//
//  crashSignalManager.swift
//  gameplay
//
//  Created by admin on 2018/11/6.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

    func SignalExceptionHandler(signal:Int32) -> Void
    {
        
        var mstr = String()
        mstr += "Stack:\n"
        
        mstr = mstr.appendingFormat("slideAdress:0x%0x\r\n", calculate())
        
        for symbol in Thread.callStackSymbols {
            mstr = mstr.appendingFormat("%@\r\n", symbol)
        }

        saveCrash(exceptionInfo: mstr)
        exit(signal)
    }

    func registerSignalHandler()
    {
        signal(SIGABRT, SignalExceptionHandler)
        signal(SIGSEGV, SignalExceptionHandler)
        signal(SIGBUS, SignalExceptionHandler)
        signal(SIGTRAP, SignalExceptionHandler)
        signal(SIGILL, SignalExceptionHandler)
        signal(SIGHUP, SignalExceptionHandler)
        signal(SIGINT, SignalExceptionHandler)
        signal(SIGQUIT, SignalExceptionHandler)
        signal(SIGFPE, SignalExceptionHandler)
        signal(SIGPIPE, SignalExceptionHandler)
}
