//
//  SlideAdressTool.m
//  gameplay
//
//  Created by admin on 2018/11/6.
//  Copyright © 2018年 yibo. All rights reserved.
//

#import "SlideAdressTool.h"
#import <mach-o/dyld.h>

@implementation SlideAdressTool

long  calculate(void){
    long slide = 0;
    for (uint32_t i = 0; i < _dyld_image_count(); i++) {
        if (_dyld_get_image_header(i)->filetype == MH_EXECUTE) {
            slide = _dyld_get_image_vmaddr_slide(i);
            break;
        }
    }
    return slide;
}


@end
