//
//  CrashManager.swift
//  gameplay
//
//  Created by admin on 2018/11/6.
//  Copyright © 2018年 yibo. All rights reserved.
//

import Foundation

let filePath = getDocumentsDirectory().appendingPathComponent("CRASHFILE.log")

func setupCrashParamter() ->  Dictionary<String, Any>?
{
    let newTodo = YiboPreference.getCrashLogParamter()
    return newTodo
}

//MARK: - CRASH PARAMTERS SETUP

func timestamp() -> String
{
    let date = Date.init()
    let timestamp = Int64(date.timeIntervalSince1970) * 1000
    return "\(timestamp)"
}

func crashHandle(crashContentAction:@escaping ([URL])->Void){    
    registerSignalHandler()
    registerUncaughtExceptionHandler()
}

func saveCrash(exceptionInfo:String)
{
    setupCrashLogParamter()
    var paramterStr = ""
    if let dic = setupCrashParamter() {
        for (key,value) in dic {
            paramterStr += "\(key):\(value)\n"
        }
    }
    
    let finalCrashReporter = "\(paramterStr)\n\(exceptionInfo)"
    try? finalCrashReporter.write(to: filePath, atomically: true, encoding: .utf8)
}

func setupCrashLogParamter() {
    let newTodo: [String: String] =
        ["crashTime": timestamp(), //
            "platform" : "1", //
            "appVersion": getVerionName(),
            "osVersion" :  "iOS/\(UIDevice.current.systemVersion)",
            "domainUrl": getDomainUrl(),
            "buildModel" : iphoneType(),
            "username" : YiboPreference.getUserName(),
            "stationId" : getBundleIdLastParmter() ?? ""]
    
    YiboPreference.setCrashLogParamter(value: newTodo)
}

func deleteCrashFile()
{
    try? FileManager.default.removeItem(at: filePath)
    YiboPreference.setCrashLogParamter(value: nil)
}

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}

func getBundleIdLastParmter() -> String? {
    let infoDictionary = Bundle.main.infoDictionary
    let bundleIdValue : AnyObject? = infoDictionary! ["CFBundleIdentifier"] as AnyObject
    let bundleId = bundleIdValue as! String
    let array = bundleId.components(separatedBy: ".")
    return array.last
}

func uploadFiles(_ urlPath: [URL]){
    
    var dic:Dictionary<String,Any>!
    if let dicP = setupCrashParamter() {
        dic = dicP
    }else {
        dic =
            ["crashTime": timestamp(), //
                "platform" : "1", //
                "appVersion": getVerionName(),
                "osVersion" :  "iOS/\(UIDevice.current.systemVersion)",
                "domainUrl": getDomainUrl(),
                "buildModel" : iphoneType(),
                "username" : YiboPreference.getUserName(),
                "stationId" : getBundleIdLastParmter() ?? ""]
    }
    
    if let url = URL(string: "https://api.yunlog8.com/upload/upload_crash_log"){
//    if let url = URL(string: "http://192.168.0.133:8080/cloud/upload/upload_crash_log"){
        var request = URLRequest(url: url)
        let boundary:String = "Boundary-\(UUID().uuidString)"
        
        request.httpMethod = "POST"
        request.timeoutInterval = 10
        request.allHTTPHeaderFields = ["Content-Type": "multipart/form-data; boundary=----\(boundary)"]
        
        
        for path in urlPath{
            do{
                var data2: Data = Data()
                var data: Data = Data()
                
                data2 = try Data(contentsOf: path)
                
                for (key,value) in dic
                {
                    data.append("------\(boundary)\r\n")
                    data.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                    data.append("\(value)\r\n")
                }
                
                data.append("------\(boundary)\r\n")
                //Here you have to change the Content-Type
                data.append("Content-Disposition: form-data; name=\"file\"; filename=\"crashDump.txt\"\r\n")
                data.append("Content-Type: application/YourType\r\n\r\n")
                data.append(data2)
                data.append("\r\n")
                data.append("------\(boundary)--")
                
                request.httpBody = data
                
            }catch let e{
                print("error =  \(e.localizedDescription)")
            }
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).sync {
                let session = URLSession.shared
                _ = session.dataTask(with: request, completionHandler: { (dataS, aResponse, error) in
                    if let erros = error{
                        print("error = \(erros)")
                    }else{
                        do{
                            let responseObj = try JSONSerialization.jsonObject(with: dataS!, options: JSONSerialization.ReadingOptions(rawValue:0)) as! [String:Any]
                            
                            if let success = responseObj["success"] as? Int
                            {
                                if success == 1
                                {
                                    deleteCrashFile()
                                }
                            }
                            
                        }catch let e{
                            print("error =  \(e.localizedDescription)")
                        }
                    }
                }).resume()
            }
        }
    }
}

func readAndUpdateCrashFiles() {
    do {
        if (try? String(contentsOf: filePath, encoding: String.Encoding.utf8)) != nil
        {
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                uploadFiles([filePath])
            }
        }
    }catch {
        
    }
}

func crashRegister() {
    crashHandle { (urls) in}
    
    readAndUpdateCrashFiles()
}
