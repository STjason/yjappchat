//
//  BetLogic.swift
//  gameplay
//
//  Created by admin on 2019/12/31.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

//MARK: 赛车 冠亚和值号码背景色
/// 赛车 冠亚和值号码背景色
func getRacingFirstSecondBGColor(with number:String) -> String {
    let colors = ["#1DBF0D","#E5CC2D","#1081FF","#3F3F3F","#F86811","#0FB1B3","#4814FA","#B0B0B0","#FD050C","#6F0002"]
    guard let intValue = Int(number) else {
        return ""
    }
    
    var index = intValue >= 10 ? intValue - 10 : intValue
    if intValue == 20 { //冠亚都是10
        index = 0
    }
    
    return colors[index]
}

/** 根据条件将peilvListDatas装载到集合里,用以保留已选中数据,以达到多玩法投注 */
func handlePeilvListDatasSuperSet(peilvListDatas:[BcLotteryPlay],peilvListDatasSuperSet:[[BcLotteryPlay]]) {
    if peilvListDatas.count == 0 {
        return
    }
    
    var peilvListDatasSuperSet = peilvListDatasSuperSet
    
    if peilvListDatasSuperSet.count == 0 {
        peilvListDatasSuperSet = [peilvListDatas]
    }else {
        //判断peilvListDatas SuperSet 是否存在当前选择的 peilvListDatas
        
        for (index,peilvs) in peilvListDatasSuperSet.enumerated() {
            //表示已经选择过该左侧边栏玩法
            
            if isHeaderPlay(parentCode: peilvs[0].parentCode) {
                
                if peilvs[0].parentCode == peilvListDatas[0].parentCode {
                    if peilvs[0].code == peilvListDatas[0].code {
                        peilvListDatasSuperSet[index] = peilvListDatas
                        return
                    }
                }
            }else if peilvs[0].fakeParentCode == peilvListDatas[0].fakeParentCode && !isEmptyString(str: peilvs[0].fakeParentCode) {
                //从服务器返回的玩法数据，分离出来的数据处理

                peilvListDatasSuperSet[index] = peilvListDatas
                return
            }else if peilvs[0].parentCode == peilvListDatas[0].parentCode && !isHeaderPlay(parentCode: peilvs[0].parentCode)  {
//                && isEmptyString(str: peilvListDatas[0].fakeParentCode)

                peilvListDatasSuperSet[index] = peilvListDatas
                return
            }
        }
    }
    
}

//MARK: 对有header的玩法的判断
func isHeaderPlay(parentCode:String) -> Bool {
    return ["zxbz","zm16","ztm","lm","hx","lx",weishulian].contains(parentCode)
}

//MARK: 过滤重复数据 [已去重的数据 array,被过滤掉的数据 array]
func filterRepeatData(datas:[OrderDataInfo]) -> ([OrderDataInfo],[OrderDataInfo]) {
    var newDatas = [OrderDataInfo]() //已去重的数据
    var filterdDatas = [OrderDataInfo]() //被过滤掉的数据
    var identifierArray = [String]()
    
    for (_, model) in datas.enumerated() {
        let idendifier = "\(model.subPlayCode),\(model.numbers),\(model.cpCode),\(model.oddsCode)"
        if !identifierArray.contains(idendifier) {
            newDatas.append(model)
        }else {
            filterdDatas.append(model)
        }
        
        identifierArray.append(idendifier)
    }
    
    return (newDatas,filterdDatas)
}

//MARK: - 格式化出同wap端相同的玩法数据
func handlePlayRules(czCode:String, rulesP:[BcLotteryPlay]) -> [BcLotteryPlay]? {
    if rulesP.count == 0 {
        return [BcLotteryPlay]()
    }else if isSixMarkWithTypeCode(type: czCode) {
        return handlePlayRulesWhenLHC(rulesP:rulesP)
    }else if isSaiche(lotType: czCode) {
        return handlePlayRulesWhenRacingCar(rulesP:rulesP)
    }else if isFFSSCai(lotType: czCode) {
        return handlePlayRulesWhenFFC_SSC(rulesP: rulesP)
    }else {
        return rulesP
    }
}

///格式化分分彩数据
func handlePlayRulesWhenFFC_SSC(rulesP:[BcLotteryPlay]) -> [BcLotteryPlay]? {
    if rulesP.count > 0 {
        if rulesP[0].lotVersion == 1 {
            return rulesP
        }
    }else {
        return [BcLotteryPlay]()
    }
    
    var rules = rulesP
    
    var twoSidesRules = [BcLotteryPlay]() //两面
    var firstToFifthRules = [BcLotteryPlay]() //1-5球
    var frontMiddleBackRules = [BcLotteryPlay]() //前中后
    var firstRules = [BcLotteryPlay]() // 第一球
    var secondRules = [BcLotteryPlay]()
    var thirdRules = [BcLotteryPlay]()
    var fourthRules = [BcLotteryPlay]()
    var fifthRules = [BcLotteryPlay]() // 第五球
    
    var twoSidesModel = BcLotteryPlay()
    var firstToFifthModel = BcLotteryPlay()
    var frontMiddleBackModel = BcLotteryPlay()
    var firstModel = BcLotteryPlay()
    var secondModel = BcLotteryPlay()
    var thirdModel = BcLotteryPlay()
    var fourthModel = BcLotteryPlay()
    var fifthModel = BcLotteryPlay()
    
    for (index,model) in rulesP.enumerated() {
        if model.code == "zh" && model.children.count > 0 { //如果是 整合玩法
            twoSidesModel = model.copy()
            firstToFifthModel = model.copy()
            frontMiddleBackModel = model.copy()
            firstModel = model.copy()
            secondModel = model.copy()
            thirdModel = model.copy()
            fourthModel = model.copy()
            fifthModel = model.copy()
            
            let twoSidesRulesCodes = ["sum","w","q","b","s","g"]
            let firstToFifthRulesCodes = ["w","q","b","s","g"]
            let frontMiddleBackRulesCodes = ["qs","zs","hs"]
            let firstRulesCodes = ["w"]
            let secondRulesCodes = ["q"]
            let thirdRulesCodes = ["b"]
            let fourthRulesCodes = ["s"]
            let fifthRulesCodes = ["g"]
            
            for (_,innerModel) in model.children.enumerated() {
                
                if twoSidesRulesCodes.contains(innerModel.code) {
                    twoSidesRules = twoSidesRules + [innerModel.copy()]
                }
                
                if firstToFifthRulesCodes.contains(innerModel.code) {
                    firstToFifthRules += [innerModel.copy()]
                }
                
                if frontMiddleBackRulesCodes.contains(innerModel.code) {
                    frontMiddleBackRules = frontMiddleBackRules + [innerModel.copy()]
                }else if firstRulesCodes.contains(innerModel.code) {
                    firstRules = firstRules + [innerModel.copy()]
                }else if secondRulesCodes.contains(innerModel.code) {
                    secondRules = secondRules + [innerModel.copy()]
                }else if thirdRulesCodes.contains(innerModel.code) {
                    thirdRules = thirdRules + [innerModel.copy()]
                }else if fourthRulesCodes.contains(innerModel.code) {
                    fourthRules = fourthRules + [innerModel.copy()]
                }else if fifthRulesCodes.contains(innerModel.code) {
                    fifthRules = fifthRules + [innerModel.copy()]
                }
            }
            
            rules.remove(at: index)
            break
        }
    }
    
    var fakeParentCode = ""
    var fakeParentName = ""
    
    if twoSidesRules.count > 0 {
        fakeParentName = "两面"
        fakeParentCode = "ffc_ssc_lm"
        twoSidesModel.fakeParentCode = fakeParentCode
        twoSidesModel.fakeParentName = fakeParentName
        
        for (_, model) in twoSidesRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        if let lastModel = twoSidesRules.last {
            if lastModel.code == "sum" {
                let model = lastModel.copy()
                twoSidesRules.removeLast()
                twoSidesRules.insert(model, at: 0)
            }
        }
        
        twoSidesModel.children = twoSidesRules
        
        rules.insert(twoSidesModel, at: 0)
    }
    
    if firstToFifthRules.count > 0 {
        fakeParentName = "1-5球"
        fakeParentCode = "ffc_ssc_1To5"
        firstToFifthModel.fakeParentCode = fakeParentCode
        firstToFifthModel.fakeParentName = fakeParentName
        
        for (_, model) in firstToFifthRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        firstToFifthModel.children = firstToFifthRules
        
        rules.append(firstToFifthModel)
    }
    
    if frontMiddleBackRules.count > 0 {
        fakeParentName = "前中后"
        fakeParentCode = "ffc_ssc_qzh"
        frontMiddleBackModel.fakeParentCode = fakeParentCode
        frontMiddleBackModel.fakeParentName = fakeParentName
        
        for (_, model) in frontMiddleBackRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        frontMiddleBackModel.children = frontMiddleBackRules
        
        rules.append(frontMiddleBackModel)
    }
    
    if firstRules.count > 0 {
        fakeParentName = "第一球"
        fakeParentCode = "ffc_ssc_dyiq"
        firstModel.fakeParentCode = fakeParentCode
        firstModel.fakeParentName = fakeParentName
        
        for (_, model) in firstRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        firstModel.children = firstRules
        
        rules.append(firstModel)
    }
    
    
    if secondRules.count > 0 {
        fakeParentName = "第二球"
        fakeParentCode = "ffc_ssc_derq"
        secondModel.fakeParentCode = fakeParentCode
        secondModel.fakeParentName = fakeParentName
        
        for (_, model) in secondRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        secondModel.children = secondRules
        
        rules.append(secondModel)
    }
    
    if thirdRules.count > 0 {
        fakeParentName = "第三球"
        fakeParentCode = "ffc_ssc_dsanq"
        thirdModel.fakeParentCode = fakeParentCode
        thirdModel.fakeParentName = fakeParentName
        
        for (_, model) in thirdRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        thirdModel.children = thirdRules
        
        rules.append(thirdModel)
    }
    
    if fourthRules.count > 0 {
        fakeParentName = "第四球"
        fakeParentCode = "ffc_ssc_dsiq"
        fourthModel.fakeParentCode = fakeParentCode
        fourthModel.fakeParentName = fakeParentName
        
        for (_, model) in fourthRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        fourthModel.children = fourthRules
        
        rules.append(fourthModel)
    }
    
    if fifthRules.count > 0 {
        fakeParentName = "第五球"
        fakeParentCode = "ffc_ssc_dwuq"
        fifthModel.fakeParentCode = fakeParentCode
        fifthModel.fakeParentName = fakeParentName
        
        for (_, model) in fifthRules.enumerated() {
            model.fakeParentCode = fakeParentCode
            model.fakeParentName = fakeParentName
        }
        
        fifthModel.children = fifthRules
        
        rules.append(fifthModel)
    }
    
    return rules
}

///格式化赛车WAP端数据
func handlePlayRulesWhenRacingCar(rulesP:[BcLotteryPlay]) -> [BcLotteryPlay]? {
    
    if rulesP.count > 0 {
        if rulesP[0].lotVersion == 1 {
            return rulesP
        }
    }else {
        return [BcLotteryPlay]()
    }
    
    var rules = rulesP + [BcLotteryPlay]()
    var oneToFiveRules = [BcLotteryPlay]() //1-5
    var sixToTenRules = [BcLotteryPlay]() //6-10
    var oneToFiveModel = BcLotteryPlay()
    var sixToTenModel = BcLotteryPlay()
    
    var followModel:BcLotteryPlay? //跟单
    
    var dnmModelDeleted = false
    for (index,model) in rulesP.enumerated() {
        if model.code == "dnm" {
            oneToFiveModel = model.copy()
            sixToTenModel = model.copy()
            
            let oneToFiveCodes = ["dgj","dyj","ddsm","ddsim","ddwm"]
            let sixToTenCodes = ["ddlm","ddqm","ddbm","ddjm","ddshm"]
            
            for (_,innerModel) in model.children.enumerated() {
                if oneToFiveCodes.contains(innerModel.code) {
                    oneToFiveRules = oneToFiveRules + [innerModel]
                }else if sixToTenCodes.contains(innerModel.code) {
                    sixToTenRules = sixToTenRules + [innerModel]
                }
            }
            
            rules.remove(at: index)
            dnmModelDeleted = true
        }else if model.code == "gdjh" {
            let followIndex = dnmModelDeleted ? (index - 1) : index
            followModel = rulesP[index]
            rules.remove(at: followIndex)
        }
    }
    
    if oneToFiveRules.count > 0 {
        let fakeParentCode = "sc1_5"
        let fakeParentName = "第1-5名"
        oneToFiveModel.fakeParentCode = fakeParentCode
        oneToFiveModel.fakeParentName = fakeParentName
        
        for (_, model) in oneToFiveRules.enumerated() {
            model.fakeParentCode = fakeParentCode // 六合彩两面
            model.fakeParentName = fakeParentName
        }
        
        oneToFiveModel.children = oneToFiveRules
        rules.append(oneToFiveModel)
    }
    
    if sixToTenRules.count > 0 {
        let fakeParentCode = "sc6_10"
        let fakeParentName = "第6-10名"
        sixToTenModel.fakeParentCode = fakeParentCode
        sixToTenModel.fakeParentName = fakeParentName
        
        for (_, model) in sixToTenRules.enumerated() {
            model.fakeParentCode = fakeParentCode // 六合彩两面
            model.fakeParentName = fakeParentName
        }
        
        sixToTenModel.children = sixToTenRules
        rules.append(sixToTenModel)
    }
    
    if let wrapFollow = followModel {
        rules.append(wrapFollow)
    }
    
    return rules
}

///格式化六合彩WAP端数据
func handlePlayRulesWhenLHC(rulesP:[BcLotteryPlay]) -> [BcLotteryPlay]? {
    var rules = rulesP
    
    var seboRules = [BcLotteryPlay]() //色波
    
    for (index,model) in rulesP.enumerated() {
        if model.code == "tma" && model.children.count > 0 {
            var temaRules = [BcLotteryPlay]()//特码
            var twoSidesModel = BcLotteryPlay()
            var twoSides = [BcLotteryPlay]() //两面
            
            let temaCodes = ["tm"] //特码codes
            let seboCodes = ["tsb"]//特色波codes
            //其他的则为 ‘两面’codes
            
            //特码数据格式化 为 特码，特色波，两面
            for (_,innerModel) in model.children.enumerated() {
                if temaCodes.contains(innerModel.code) {
                    temaRules = temaRules + [innerModel]
                }else if seboCodes.contains(innerModel.code) {
                    seboRules = seboRules + [innerModel]
                    innerModel.parentCode = "sb"
                    innerModel.parentName = "色波"
                }else {
                    twoSides = twoSides + [innerModel]
                }
            }
            
            if twoSides.count > 0 {
                twoSidesModel = model.copy()
                let fakeParentName = "两面"
                let fakeParentCode = "lhlm"
                
                twoSidesModel.fakeParentName = fakeParentName
                twoSidesModel.fakeParentCode = fakeParentCode
                
                for (_, model) in twoSides.enumerated() {
                    model.fakeParentCode = fakeParentCode // 六合彩两面
                    model.fakeParentName = fakeParentName
                }
                
                twoSidesModel.children = twoSides
            }
            
            if temaRules.count > 0 {
                model.children = temaRules
                rules.insert(twoSidesModel, at: index + 1)
            }else {
                rules.remove(at: index)
                rules.insert(twoSidesModel, at: index)
            }
            
        }else if model.code == "sb" && model.children.count > 0 {
            model.children = seboRules + model.children
        }
    }
    return rules
}

