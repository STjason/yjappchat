//
//  SwitchCommon.swift
//  gameplay
//
//  Created by admin on 2018/11/1.
//  Copyright © 2018年 yibo. All rights reserved.
//  开关判断公共方法

import UIKit

//首页 Nav是否显示网站名称
func switchShowStationName() -> Bool {
    if let sysConfig = getSystemConfigFromJson()
    {
        if sysConfig.content != nil
        {
            return sysConfig.content.switch_native_show_station_name == "on"
        }
    }
    
    return false
}

///隐藏反水条
func switchShowBetSliderBar() -> Bool {
    if let sysConfig = getSystemConfigFromJson()
    {
        if sysConfig.content != nil
        {
            return sysConfig.content.hide_kickbackbar_simple_betpage == "on"
        }
    }
    
    return false
}

//注册时，是否需要手机验证码
func switchVerifiedCodeOn() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
         return value.content.on_off_register_sms_verify == "on"
    }
    
    return false
}

///只开启了‘信用’，并且需要将信用转换为‘官方’
func switchOnlyPeillvSingleForceOffical() -> Bool {
    
    let system = getSystemConfigFromJson()
    if let value = system {
        let singleForce = value.content.single_honest_play_force_office_text == "on"
        let onlyPeilv = value.content.lottery_version == VERSION_V2
        return singleForce && onlyPeilv
    }

    return false
}

///只开启了信用时，信用玩法文字显示，转化为 ‘官方’
func switchSingleForceOfficeText() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
         return value.content.single_honest_play_force_office_text == "on"
    }
    
    return false
}

//MARK: - 验证
///登录需要验证码
func switchLoginverifyNeed() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
         return value.content.on_off_mobile_verify_code == "on"
    }
    
    return false
}

///行为验证码类型
func switchRecaptcha_verify_type() -> String {
    var switchType = "163huadong"
    
    let system = getSystemConfigFromJson()
    if let value = system {
        switchType = value.content.recaptcha_verify_type
    }
    
    return switchType
}

///开启行为验证后隐藏验证码 true 隐藏，false 不隐藏
func switchRecaptcha_verify_hide_code() -> Bool {
    var hideCodeIfVerifyAlready = false
    
    let system = getSystemConfigFromJson()
    if let value = system {
        hideCodeIfVerifyAlready = value.content.on_off_recaptcha_verify_hide_code == "on"
    }
    
    return hideCodeIfVerifyAlready
}

///开启站点行为验证 true 开启验证，false 不需要验证
func switchRecaptcha_verify() -> Bool {
    var needVerify = false
    let system = getSystemConfigFromJson()
    if let value = system {
        needVerify = value.content.on_off_recaptcha_verify == "on"
    }
    
    return needVerify
}

///禁言时，是否同时删除消息
func deleteMsgsWhenProhibit() -> Bool {
    var switch_ban_speak_del_msg = false
    if let config = getChatRoomSystemConfigFromJson(),let source = config.source {
        switch_ban_speak_del_msg = (source.switch_ban_speak_del_msg == "1")
    }
   
    return switch_ban_speak_del_msg
}

///是否开启聊天室自定义头像
func switchLocalAvar() -> Bool {
    var switch_local_ava = false
    if let config = getChatRoomSystemConfigFromJson(),let source = config.source {
        switch_local_ava = (source.switch_local_ava == "1")
    }
    
    return switch_local_ava
}

///是否在线管理员才可以查看在线用户,默认只有管理员可以查看在线用户
func switchOnlyManagerCanWatchOnlineUser() -> Bool {
    if let config = getChatRoomSystemConfigFromJson(), config.source?.switch_room_people_admin_show == "1" {
        return true
    }
    
    return false
}

///获得环形菜单按钮
func getSemicycleButton() -> SemiCircleFloatDragButton? {
    if let windonw = UIApplication.shared.keyWindow {
        for view in windonw.subviews {
            if view.isKind(of: SemiCircleFloatDragButton.self) {
                if let view = view as? SemiCircleFloatDragButton {
                    return view
                }else {
                    return nil
                }
            }
        }
    }
    
    return nil
}

///是否显示环形菜单入口按钮
func showSemicycleButton() -> Bool {
    if let config = getSystemConfigFromJson()
    {
        
        if config.content != nil
        {
            if config.content.native_float_funcbar_switch != "on" || YiboPreference.isShouldOpenFloatTool() != "on"
            {
                return false
            }
        }
    }

    return true
}

///首页公告弹窗列表是否默认展示第一条公告内容 "平台公告"
func switchNoticeListExpandFirst() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
        return value.content.notice_list_dialog_expand_first_notice != "off"
    }

    return true
}

///余额生金
func switchMoney_income() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
        return value.content.on_off_money_income == "on"
    }
    
    return false
}

///是否显示免费注册,默认 true 打开
func switchOnoff_mobile_guest_register() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
        return value.content.onoff_mobile_guest_register != "off"
    }
    
    return true
}

/** 是否显示彩票 开启：off，关闭：on */
func getSwitch_lottery() -> Bool {
    var show = true
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            show = config.content.switch_lottery != "on"
        }
    }
    return show
}

///是否开启了开奖网
func switchOpen_bet_website_enter() -> Bool {
    let system = getSystemConfigFromJson()
    if let value = system {
        return value.content.open_bet_website_enter.length > 0
    }
    
    return false
}


//主页版本配置，默认“V1” V2 版本-存款，V3 主页推广链接，V4 主页额度转换，V5 主页余额生金 ， V6聊天室,V7免费试玩，V8开奖网，V9所，V11纯电竞版，v12纯体育版本
func switchMainPageVersion() -> String {
    if let config = getSystemConfigFromJson(){
        if config.content != nil{
            var version = config.content.mainPageVersion

            if version == "V7" {
                //免费试玩被关闭
                if !switchOnoff_mobile_guest_register() {
                    version = "V1"
                }
            }else if version == "V8" {
                if !switchOpen_bet_website_enter() {
                    version = "V1"
                }
            }

            return version
        }
    }
    return "V1"
}

///开启网站入款指南配置,默认 ‘on’ 开
func switchWebpayGuide() -> Bool {
    if let sysconfig = getSystemConfigFromJson() {
        let version = sysconfig.content.switch_webpay_guide
        return version != "off"
    }
    
    return true
}

//MARK: - 获得下载页更新url
/// 获得下载页更新url
func getDownloadPageURL() -> String {
    var updateUrl = ""
    if switchGoDownpageWhenUpdateVersion() {
        updateUrl = CHECK_UPDATE_URL
    }else {
        updateUrl = CHECK_UPDATE_URL_V2
    }
    
    return updateUrl
}

/// 是否去下载页更新App，默认 off
func switchGoDownpageWhenUpdateVersion() -> Bool {
    if let sysconfig = getSystemConfigFromJson() {
        let version = sysconfig.content.go_downpage_when_update_version
        return version == "on"
    }
    
    return false
}


//MARK: - （购彩记录）撤单 && 是否显示 继续投注

/// 注单页下方按钮版本
///
/// - Returns: v1 撤单，继续下注 都显示; v2 只显示 撤单; v3只显示 继续下注;v4 撤单， 继续下注 都不存在; 返回""的情况不存在
func switchDetailBetoOrderVersion(status:String) -> String {
    if switchContinueBetInDetailBetOrder() && judgeStatusRevokeOrderToShow(status: status) {
        return "v1"
    }else if !switchContinueBetInDetailBetOrder() && judgeStatusRevokeOrderToShow(status: status) {
        return "v2"
    }else if switchContinueBetInDetailBetOrder() && !judgeStatusRevokeOrderToShow(status: status) {
        return "v3"
    }else if !switchContinueBetInDetailBetOrder() && !judgeStatusRevokeOrderToShow(status: status) {
        return "v4"
    }else {
        return ""
    }
}

/// 是否在注单页显示继续下注
func switchContinueBetInDetailBetOrder() -> Bool {
    if let sysconfig = getSystemConfigFromJson() {
        let version = sysconfig.content.switch_continue_bet_in_detail_betorder
        return version == "on"
    }
    
    return false
}

/// 未开奖的时候才可以撤单
///
/// - Parameter status: "1"表示未开奖
/// - Returns: true可撤单
func judgeStatusRevokeOrderToShow(status:String) -> Bool {
    if status == "1" && switchRevoke_order() {
        return true
    }
    
    return false
}

/** 若开关打开，则显示撤单按钮,默认关闭 */
func switchRevoke_order() -> Bool {
    if let sysconfig = getSystemConfigFromJson() {
        let version = sysconfig.content.lottery_revoke_order_switch
        return version == "on"
    }
    
    return false
}

/** 是否可以多玩法投注:需要开关打开,并且是赔率玩法时 */
func switchCanMutiRulesBet(isPeilv:Bool) -> Bool {
    return isPeilv && switchAllowMutiRulesBet()
}

/** 开关是否允许多玩法投注 */
func switchAllowMutiRulesBet() -> Bool {
    return true
}

/** 是否可以投注 true:封盘，false：可投 */
func isBetNotAble() -> Bool {
    return YiboPreference.getAbleBet() == "off" && !switch_waitforOpenbetAfterBetDeadline()
}

/** 是否可以投注 true:封盘，false：可投 */
func isBetNotAbleShare() -> Bool {
    return YiboPreference.getAbleBetShare() == "off" && !switch_waitforOpenbetAfterBetDeadline()
}

/** 官方信用版本, 判断是否只有其中一个*/
func onlyHasVersionOfficalOrPeilv() -> Bool {
    if let sysconfig = getSystemConfigFromJson() {
        let version = sysconfig.content.lottery_version
        return [VERSION_V1,VERSION_V2].contains(version)
    }
    
    return false
}

///MARK: - 聊天室
/** 开关： 聊天室开关 */
func switch_chatRoomOn() -> Bool
{
    var chatRoomOn = false
    if let sysconfig = getSystemConfigFromJson() {
        let float_chat_bar_switch = sysconfig.content.float_chat_bar_switch
        let switch_chatroom = sysconfig.content.switch_chatroom
        chatRoomOn = float_chat_bar_switch == "on" && switch_chatroom == "on"
    }

    return chatRoomOn
}

/** 开关： 手势密码开关 */
func switch_GesturePwdOn() -> Bool
{
    var gestureSwitch = false
    if let sysconfig = getSystemConfigFromJson() {
        let gesture_pwd_switch = sysconfig.content.gesture_pwd_switch
        gestureSwitch = gesture_pwd_switch == "on"
    }
    return gestureSwitch
}

/// 原生聊天室，wap聊天室切换开关
///
/// - Returns: true原生，false为WAP版本
func switch_nativeOrWapChat() -> Bool {
    
    if  let config = getSystemConfigFromJson(){
        if config.content != nil {
            return config.content.native_chat_room != "off"
        }
    }
    
    return true
}

/** 原生封盘时允许开始进入下一期投注 ,默认on:允许*/
func switch_waitforOpenbetAfterBetDeadline() -> Bool {
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
           return config.content.waitfor_openbet_after_bet_deadline == "on"
        }
    }

    return false
}

/// 选择哪个支付页面
///
/// - Returns: 先通道版【原生v1;wap v2】;先支付方法版【原生v3；wap v4】
func switch_payVersion() -> String {
    var mobilepay_version = "V1"
    var native_charge_version = "V1"
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            native_charge_version = config.content.native_charge_version
            mobilepay_version = config.content.mobilepay_version
        }
    }

    if mobilepay_version == "V1" {
        return native_charge_version == "V2" ? "V4" : "V3"
    } else {
        return native_charge_version == "V2" ? "V2" : "V1"
    }
}

/** 支付列表页面："v1": "分组模式", "v2": "经典模式" */
func getCharge_page_mode_switch() -> String {
    var version = "v1"
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            version = config.content.charge_page_mode_switch
        }
    }

    return version
}

// V1原生版本支付页面， V2 wap版本支付页面
func getNative_charge_version() -> String {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.native_charge_version
        }
    }
    
    return "V1"
}

/** 根据充值模式，获得对应控制器 */
func chooseGoChargeController(meminfo:Meminfo?,controller:UIViewController,isPush:Bool = false) {
    if getCharge_page_mode_switch() == "v1" {
        let recordPage = getChareController() as! NewChargeMoneyV2Controller
        recordPage.meminfo = meminfo
        if isPush {
            controller.navigationController?.pushViewController(recordPage, animated: true)
        }else {
            let nav = UINavigationController.init(rootViewController: recordPage)
            controller.present(nav, animated: true, completion: nil)
        }
    }else {
        let recordPage = getChareController() as! NewChargeMoneyController
        recordPage.meminfo = meminfo
        if isPush {
            controller.navigationController?.pushViewController(recordPage, animated: true)
        }else {
            let nav = UINavigationController.init(rootViewController: recordPage)
            controller.present(nav, animated: true, completion: nil)
        }
    }
}

func switch_native_default_color_theme(handler: (String) -> Void) {
    var color = "red"
    if let sysConfig = getSystemConfigFromJson() {
        if sysConfig.content != nil {
            color = sysConfig.content.native_default_color_theme
        }
    }
    
    let finalTheme = getCurrentThemeListNameWithName(name: color)
    handler(finalTheme)
}

/** 常玩游戏开关 */
func switch_nativeUsualGame() -> Bool {
    var usual_game = true
    if let sysConfig = getSystemConfigFromJson()
    {
        if sysConfig.content != nil
        {
            usual_game = sysConfig.content.native_usual_game_switch != "off"
        }
    }
    
    return usual_game && getSwitch_lottery()
}

/** 转账，是否不全两位小数 */
func switch_shouldRandom() -> Bool {
    var shouldRandom = false
    if let sysConfig = getSystemConfigFromJson()
    {
        if sysConfig.content != nil
        {
            shouldRandom = sysConfig.content.fast_deposit_add_money_select == 0
        }
    }
    
    return shouldRandom
}

/** on 公告弹框形式展现/off 跳转页面 */
func switch_active_open_in_dialog() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.active_open_in_dialog == "on"
        }
    }
    return false
}

/** 显示2个赔率 */
func showTwopeilv() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.show_min_odd_when_bet == "on"
        }
    }
    return false
}

/** 投注成功之后，是否需要重置投注 ‘圆角分’模式 */
func getResetBetYJFModel() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.reset_yjfmode_after_bet != "off"
        }
    }
    return true
}

/** 是否开启了 会员转代理开关 */
/** 是否开启了 前端是否显示会员转代理开关 */
func getSwitch_member_to_proxy() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.switch_member_to_proxy != "off" && config.content.front_show_member_to_agent_switch == "on"
        }
    }
    
    return false
}

/** 沙巴体育 */
func getOnoff_sb_switch() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.onoff_sb_switch == "on"
        }
    }
    
    return false
}

//MARK: - 主页 tab显示项
/** 是否显示真人 */
func getOnoff_zhen_ren_yu_le() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.onoff_zhen_ren_yu_le == "on"
        }
    }
    
    return false
}

/** 是否显示电子 */
func getOnoff_dian_zi_you_yi() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.onoff_dian_zi_you_yi == "on"
        }
    }
    
    return false
}

/** 是否显示体育 */
func getOnoff_sport_switch() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.onoff_sport_switch == "on"
        }
    }
    
    return false
}

/** 是否显示棋牌 */
func getNb_chess_showin_mainpage() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.nb_chess_showin_mainpage == "on"
        }
    }
    
    return false
}
/** 是否显示电竞游戏 */
func getGaming_showin_mainpage() -> Bool{
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.switch_esport == "on"
        }
    }
    
    return false
}
/** 是否显示捕鱼游戏 */
func getGameFishing_showin_mainpage() -> Bool{
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.switch_fishing == "on"
        }
    }
    
    return false
}


/** 额度转换 */
func getSwitch_money_change() -> Bool {
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.switch_money_change == "on"
        }
    }
    return false
}

/** 棋牌注单显示 */
func getOnoff_chess() -> Bool {    
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            return config.content.switch_chess == "on"
        }
    }
    return false
}

/** 优惠活动，显示title和date */
func getShow_title_time_in_active_item() -> Bool {
    var show = true
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            show = config.content.show_title_time_in_active_item != "off"
        }
    }

    return show
}

/** tab上，会员中心 未读消息数 */
func getUnread_point_showin_membertab() -> Bool {
    var show = true
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            show = config.content.unread_point_showin_membertab != "off"
        }
    }
    
    return show
}

func getChareController() -> UIViewController {
    if getCharge_page_mode_switch() == "v1" {
        let vc = UIStoryboard(name: "new_charge_money_page", bundle: nil).instantiateViewController(withIdentifier: "new_charge_v2")
        return vc
    }else {
        let vc = UIStoryboard(name: "new_charge_money_page", bundle: nil).instantiateViewController(withIdentifier: "new_charge")
        return vc
    }
}

/** v1---app模式，v2-pc模式 */
func promp_link_mode_switch() -> String{
    var urlVersion = "v1"
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            urlVersion = config.content.promp_link_mode_switch
        }
    }
    return urlVersion
}

func honest_betorder_page_fastmoney_switch() -> Bool {
    var switchValue = "on"
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            switchValue = config.content.honest_betorder_page_fastmoney_switch
        }
    }
    
    return switchValue == "on"
}


/** 会员中心版本 控制器 */
func pickMemberPageController() -> UIViewController {
    if memberPageVersion() == "1" {
        let vc = UIStoryboard(name: "MemberPageTwoController", bundle: nil).instantiateViewController(withIdentifier: "memberPageTwoController")
        return vc
    }else {
        let moduleStyle = YiboPreference.getMallStyle()
        let (xibname,_) = selectMainStyleByModuleID(styleID: moduleStyle)
        let vc = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: "member_page")
        return vc
    }
}


/** 会员中心版本号 */
func memberPageVersion() -> String {
    var version = "1"
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            version = config.content.member_center_style_type
        }
    }
    
    return version
}


/** 逻辑：是否允许打开弹窗公告（开关打开 && 不是同一天）,只在 用户已经关闭弹窗时判断即可，不影响打开状态 */
func shouldShowNoticeWIndow() -> Bool {
    let should = switch_show_noticewindow_when_click_notip() && !isSameDateWithNoTipsDate()
    if should {
        YiboPreference.setNotipDate(date: nil)
        YiboPreference.setAlert_isAll(value: "on" as AnyObject)
    }
    
    return should
}

/** 开关：是否第二天，允许弹窗公告 */
func switch_show_noticewindow_when_click_notip() -> Bool {
    var shouldShow = "off"
    if let config = getSystemConfigFromJson() {
        if config.content != nil {
            shouldShow = config.content.show_noticewindow_when_click_notip
        }
    }
    
    return shouldShow == "on"
}

/**  开关： 手否允许全数字注册账号 */
func switch_allnumber_switch_when_register() -> Bool
{
    var allnumber_switch = "off"
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            allnumber_switch = config.content.allnumber_switch_when_register
        }
    }
    
    return allnumber_switch == "on"
}

/** 开关： 六合彩图片扁平 */
func switch_LHC_option_icons() -> Bool
{
    var switchOption = "off"
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            switchOption = config.content.use_lhc_optimise_icons
        }
    }

//    if YiboPreference.getIsChatBet() == "on" {
//        switchOption = YiboPreference.getIsChatBet()
//    }
    
    return switchOption == "on"
}

func switch_scanBetOn() -> Bool
{
    var scanBetOn = false
    if let sysconfig = getSystemConfigFromJson() {
        let mobile_bet_qrcode = sysconfig.content.mobile_bet_qrcode
        scanBetOn = mobile_bet_qrcode == "on"
    }
    
    return scanBetOn
}

func stationID() -> String
{
    var stationId = ""
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            stationId = config.content.station_id
        }
    }
    
    return stationId
}

func stationCode() -> String {
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            return config.content.stationCode
        }
    }
    
    return ""
}

//MARK: - 判断账号类型
/// 判断是否是，手机端注册试玩账号
///
/// - Returns: true:是手机端试玩账号,false: 可能不是试玩账号，也可能是后台注册试玩账号
func judgeIsMobileGuest() -> Bool {
    if YiboPreference.getAccountMode() == GUEST_TYPE{
        let username = YiboPreference.getUserName()
        return username.starts(with: "guest") || isEmptyString(str: username)
    }
    return false
}

/// 判断是否是，后台注册试玩账号
///
/// - Returns: true:是后台注册试玩账号,false:可能不是试玩账号，可能是手机端注册试玩账号
func judgeIsWebGuest() -> Bool {
    if YiboPreference.getAccountMode() == GUEST_TYPE{
        let username = YiboPreference.getUserName()
        return !(username.starts(with: "guest") || isEmptyString(str: username))
    }
    
    return false
}

/// 判断是否是，试玩账号
///
/// - Returns: true:试玩账号，false:可能是后台注册试玩账号,也可能是手机端注册试玩账号
func judgeIsGuest() -> Bool {
    return YiboPreference.getAccountMode() == GUEST_TYPE
}


//MARK: - 聊天室
///返回 0 都关闭，1 都开启，2 只有 中奖榜单开始，3 只有 盈亏榜单开启, -1 该情况不存在
func switcnWinnerAndProfitListStatus() -> Int {
    if let config = getChatRoomSystemConfigFromJson()?.source{
        return config.switch_winning_list_show == "1" ? 2 : 0
    }
    
    return 0
    
//    if let config = getChatRoomSystemConfigFromJson()?.source{
//        //中奖榜单
//        var winListOn = false
//        var profitListOn = false
//
//        winListOn = config.switch_winning_list_show == "1"
//        profitListOn = config.switch_today_earnings_list_info == "1"
//        if !winListOn && !profitListOn {
//            return 0
//        }else if winListOn && profitListOn {
//            return 1
//        }else if winListOn && !profitListOn {
//            return 2
//        }else if !winListOn && profitListOn {
//            return 3
//        }
//    }
//
//    return -1
}






