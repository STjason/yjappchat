//
//  YiboPreference.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2017/12/21.
//  Copyright © 2017年 com.lvwenhan. All rights reserved.
//

import UIKit

class YiboPreference {
    
    static let PLAY_TOUZHU_VOLUME:String = "play_touzhu_volume"
    static let CP_VERSION:String = "cpVersion"
    static let TOKEN:String = "token"
    static let CONFIG:String = "config"
    static let CONFIG_NEW: String = "newConfig"
    static let YJF_MODE = "yjf_mode"
    static let SAVE_PWD_STATUS = "save_pwd_status"
    static let NOT_TIP_LATER = "not_tip_later"
    static let SAVE_LOGIN_STATUS = "save_login_status"
    static let NOT_TOAST_WHEN_ENDBET = "not_toast_when_endbet"
    static let SAVE_PWD = "save_pwd"
    static let SAVE_SIGN_DATE = "sign_date"
    static let SAVE_TOUZHU_ORDER_JSON = "order_json"
    static let SAVE_LOTTERYS = "save_lotterys"
    static let ACCOUNT_TYPE = "account_type"
    static let USERNAME = "username"
    static let AUTO_LOGIN_STATUS = "auto_login_status"
    static let MAIN_STYLE_ID = "main_style_id"
    static let SHAKE_TOUZHU = "shake_touzhu"
    static let AUTO_LOGIN = "auto_login"
    static let ASK_BEFORE_ZUIHAO = "ask_before_zuihao"
    static let THEME_CURRENT = "currentTheme"
    static let THEME_CURRENT_BYNAME = "currentTheme_name"
    static let SOUNDEFFECT_CURRENT = "currentSoundEffect"
    static let SHOWALERT_ALLOR_NONE = "showAlert_all"
    static let FLOAT_TOOL_SHOW = "float_tool_show"
    static let DEFAULT_BROWER_TYPE = "default_brower_type"
    static let SHOW_SCANCODE_FUNCTIONS_TIPSPAG = "showScanCode_newfunction"
    static let SHOWNEW_FUNCTIONS_TIPSPAG_BET = "showNewFunctionTipsPage_bet"
    static let RECENT_DOMAIN_URLS = "recentDomainURLs"
    static let SHOW_RECENT_DOMAIN_URLS = "showRecentDomainURLs"
    static let OPEN_CONFIGURLSPAGE = "off"
    static let SEMICIRCLE_MENUITEMS = "semicircelMenuItems"
    static let CRASHLOGS = "crashLogs"
    static let NOTIPSDATE = "noTipsDate"
    static let ChATVIEWON = "ChatViewON"
    static let LAUNCHER_IMGURL:String = "launcherUrl"
    static let FLOATINGWINDOWSON = "FloatingWindowON"
    static let ABLE_BET = "ableBet"
    static let ABLE_BET_SHARE = "ableBet_share"
    static let CACHEAVATAR = "CACHEAVATAR"
    /**选择更新版本 0是企业签名 1是超级签名*/
    static let UPDATEVERSIONPAYMENT = "updateVersonPayment"
    static let IS_CHAT_BET = "isChatBet"
    // 手势锁开关
    static let GESTURE_LOCK = "Gesture lock"
    // 手势锁锁屏时间
    static let GESTURE_LOCK_TIME = "Gesture lock time"
    // 最后一次进入离开前台进入后台时间
    static let LAST_TIME = "lastTime"
    // 手势锁网络开关
    static let Gesture_lock_netSwitch = "GestureLockNetSwitch"
    //站内信
    static let INSIDESTATION = "insideStation"
    //主页的二级分栏模式
    static let MALLIMAGETEXTSTYLE = "mallImageTextStyle"
    
    static func getGestureLockNetSwitch() -> String {
        return UserDefaults.standard.string(forKey: Gesture_lock_netSwitch) ?? "on"
    }
    
    static func setGestureLockNetSwitch(isOpen: String) {
        UserDefaults.standard.set(isOpen, forKey: Gesture_lock_netSwitch)
    }
    
    static func getLAST_TIME() -> Int {
        return UserDefaults.standard.integer(forKey: LAST_TIME)
    }
    
    static func setLAST_TIME(time: Int) {
        UserDefaults.standard.set(time, forKey: LAST_TIME)
    }
    
    static func getGESTURE_LOCK_TIME() -> Int {
        let interval = UserDefaults.standard.integer(forKey: GESTURE_LOCK_TIME)
        // 默认进入后台 超过10秒 需要解锁
        return interval == 0 ? 10 : interval
    }
    
    static func setGESTURE_LOCK_TIME(time: Int) {
        UserDefaults.standard.set(time, forKey: GESTURE_LOCK_TIME)
    }
    
    static func getGESTURE_LOCK() -> String {
        return UserDefaults.standard.string(forKey: GESTURE_LOCK) ?? "false"
    }
    
    static func setGESTURE_LOCK(isOpen: String) {
        UserDefaults.standard.set(isOpen, forKey: GESTURE_LOCK)
    }
    
    static func getCACHEAVATARdata() -> Data {
        let data = Data.init()
        return  UserDefaults.standard.data(forKey: CACHEAVATAR) ?? data
    }

    static func setCACHEAVATARdata(value:Data) {
        UserDefaults.standard.set(value, forKey: CACHEAVATAR)
    }
    //聊天室自己的ID
    static let SELFUSERID = "selfUserId"
    
    /// 是否可以选择号码进行下注
    ///
    /// - Returns: 默认on:可以选择，off不能选择
    static func setAbleBet(value:String) {
        UserDefaults.standard.set(value, forKey: ABLE_BET)
    }
    
    /// 是否可以选择号码进行下注
    ///
    /// - Returns: 默认on:可以选择，off不能选择
    static func getAbleBet() -> String {
        return UserDefaults.standard.string(forKey: ABLE_BET) ?? "on"
    }
    
    /// 是否可以选择号码进行下注_分享注单
    ///
    /// - Returns: 默认on:可以选择，off不能选择
    static func setAbleBetShare(value:String) {
        UserDefaults.standard.set(value, forKey: ABLE_BET_SHARE)
    }
    
    /// 是否可以选择号码进行下注_分享注单
    ///
    /// - Returns: 默认on:可以选择，off不能选择
    static func getAbleBetShare() -> String {
        return UserDefaults.standard.string(forKey: ABLE_BET_SHARE) ?? "on"
    }
    
    static func setFloatingWindowONdata(value:Bool) {
        UserDefaults.standard.set(value, forKey: FLOATINGWINDOWSON)
    }
    
    static func getFloatingWindowONdata() -> Bool
    {
       let value =  UserDefaults.standard.value(forKey: FLOATINGWINDOWSON)
//        let value = UserDefaults.standard(forKey: FLOATINGWINDOWSON)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    
    static func setChATVIEWONdata(value:Bool) {
        UserDefaults.standard.set(value, forKey: ChATVIEWON)
    }
    
    static func getChATVIEWONdata() -> Bool
    {
        let value = UserDefaults.standard.bool(forKey: ChATVIEWON)
        return value
    }

    static func setCrashLogParamter(value:Dictionary<String, Any>?) {
        UserDefaults.standard.set(value, forKey: CRASHLOGS)
    }
    
    static func getCrashLogParamter() -> Dictionary<String, Any>? {
        if let value = UserDefaults.standard.dictionary(forKey: CRASHLOGS)
        {
            return value
        }else
        {
            return nil
        }
    }
    
    
    static func setLauncherUrl(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: LAUNCHER_IMGURL)
    }
    
    static func getLauncherUrl() -> String {
        let value = UserDefaults.standard.string(forKey: LAUNCHER_IMGURL)
        if let v = value{
            return v
        }
        return ""
    }
    
    static func setRecentDomainURLs(value: Array<String>) -> Void {
        UserDefaults.standard.set(value, forKey: RECENT_DOMAIN_URLS)
    }
    
    static func getRecentDomainURLs() -> Array<String> {
        if let value = UserDefaults.standard.array(forKey: RECENT_DOMAIN_URLS) as? Array<String> {
            return value
        }else {
            return ["https://t1.yunji22.com"]
        }
    }
    
    // 有时间改成数据库的方式
    //  环形菜单，没有使用 数据库，数据管理很混乱，这一块有时间必须要改
    // 删除快捷方式
    static func deleteMenuItems(value: Array<String> ) -> Void {
        var currentMenuItems = getSemicircelMenuItems()
        var resultMenuItems = currentMenuItems
        for index in 0..<currentMenuItems.count {
            let item = currentMenuItems[index]
            
            for inIndex in 0..<value.count {
                let innerItem = value[inIndex]
                
                if innerItem == item
                {
                    resultMenuItems.remove(at: index)
                }
            }
        }
        
        setSemicircleMenuItems(value: resultMenuItems)
    }
    
    // 添加快捷方式
    static func addMenuItems(value: Array<String> ) -> Void {
        var currentMenuItems = getSemicircelMenuItems()
        var handleValue = value
        
        for index in 0..<currentMenuItems.count {
            let item = currentMenuItems[index]
            
            for inIndex in 0..<value.count {
                let innerItem = value[inIndex]
                
                if innerItem == item
                {
                    handleValue.remove(at: inIndex)
                }
            }
        }
        
        var resultsMenuItems = currentMenuItems
        resultsMenuItems.insert(contentsOf: handleValue, at: resultsMenuItems.count - 1)
        setSemicircleMenuItems(value: resultsMenuItems)
    }
    
    static func setSemicircleMenuItems(value: Array<String> ) -> Void {
        UserDefaults.standard.setValue(value, forKey: SEMICIRCLE_MENUITEMS)
    }
    
    static func getSemicircelMenuItems() -> (Array<String>) {
        if let value = UserDefaults.standard.array(forKey: SEMICIRCLE_MENUITEMS) as? Array<String>
        {
            var array = value
            if !switch_chatRoomOn()
            {
                for (index,value) in SemicircleMenuItems.enumerated()
                {
                    if value.0 == "聊天室"
                    {
                        array.remove(at: index)
                        break
                    }
                }
            }else if !value.contains("聊天室")
            {
                for (index,value) in SemicircleMenuItems.enumerated()
                {
                    if value.0 == "投注"
                    {
                        array.insert("聊天室", at: index + 1)
                        break
                    }
                }
            }
            
            if !switch_scanBetOn(){
                for (index,value) in SemicircleMenuItems.enumerated()
                {
                    if value.0 == "扫码下注"
                    {
                        array.remove(at: index)
                    }
                }
            }else if !value.contains("扫码下注"){
                array.insert("扫码下注", at: 0)
            }
            
            return array
        }else {
            
            var semicircleItems = [String]()
            for index in 0..<SemicircleMenuItems.count {
                semicircleItems.append(SemicircleMenuItems[index].0)
            }
            
            setSemicircleMenuItems(value: semicircleItems)
            return semicircleItems
        }
    }
    
    static func getSemiCircleTuplesWithTitles(value: Array<String>) -> [(String,String)] {
        let allDatas = SemicircleMenuItems + addFastEntryMenuItems
        var tuplesP = [(String,String)]()
        
        for index in 0..<value.count
        {
            let title = value[index]
            for item in allDatas
            {
                if item.0 == title
                {
                    tuplesP.append(item)
                }
            }
        }
        
        return tuplesP
    }
    
    static func setOpenConfigURLPage(value:String) {
        UserDefaults.standard.setValue(value, forKey: OPEN_CONFIGURLSPAGE)
    }
    
    static func getOPenConfigURLPage() -> String
    {
        if let value = UserDefaults.standard.string(forKey: OPEN_CONFIGURLSPAGE)
        {
            return value
        }else
        {
            return "off"
        }
    }
    
    static func setShowRecentDomainURLs(value: String) -> Void {
        UserDefaults.standard.set(value, forKey: SHOW_RECENT_DOMAIN_URLS)
    }
    
    static func getShowRecentDomainURLs() -> String {
        if let value = UserDefaults.standard.string(forKey: SHOW_RECENT_DOMAIN_URLS) {
            return value
        }else {
            return "off"
        }
    }

    //MARK: - 浮层新功能操作，教学提示
    static func setShowBetNewfunctionTipsPage(value: String) -> Void {
        UserDefaults.standard.set(value, forKey: SHOWNEW_FUNCTIONS_TIPSPAG_BET)
    }
    
    static func getShowBetNewfunctionTipsPage() -> String {
        if let value = UserDefaults.standard.string(forKey: SHOWNEW_FUNCTIONS_TIPSPAG_BET) {
            return value
        }else {
            return ""
        }
    }

    static func setScanCodeTipsPage(value: String) -> Void {
        UserDefaults.standard.set(value, forKey: SHOW_SCANCODE_FUNCTIONS_TIPSPAG)
    }
    
    static func getShowScanCodeTipsPage() -> String {
        if let value = UserDefaults.standard.string(forKey: SHOW_SCANCODE_FUNCTIONS_TIPSPAG) {
            return value
        }else {
            return ""
        }
    }
    
    /** -1：没有有默认浏览器，不需要弹窗；其他 >= 0 的值，表示已有默认浏览器,不需要弹窗 */
    static func getDefault_brower_type() -> Int {
        if let value = UserDefaults.standard.string(forKey: DEFAULT_BROWER_TYPE) {
            return Int(value)!
        }else {
            return -1
        }
    }
    
    static func setNotipDate(date: Date?) {
        if let dateP = date {
            UserDefaults.standard.setValue(dateP, forKey: NOTIPSDATE)
            } else {
            UserDefaults.standard.removeObject(forKey: NOTIPSDATE)
        }
        
    }
    
    static func getNotipsDate() -> Date? {
        if let value = UserDefaults.standard.object(forKey: NOTIPSDATE) {
            return value as? Date
        }else {
            return nil
        }
    }
    
    static func setDefault_brower_type(value: String) -> Void {
        UserDefaults.standard.set(value, forKey: DEFAULT_BROWER_TYPE)
    }
    
    static func isShouldAlert_isAll() -> String {
        if let value = UserDefaults.standard.string(forKey: SHOWALERT_ALLOR_NONE) {
            return value
        }else {
            return ""
        }
    }

    static func setAlert_isAll(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SHOWALERT_ALLOR_NONE)
    }
    
    static func isShouldOpenFloatTool() -> String {
        if let value = UserDefaults.standard.string(forKey: FLOAT_TOOL_SHOW) {
            return value
        }else {
            return ""
        }
    }
    //本地保存站内信弹窗
    static func setInsideStation(value:Bool) -> Void{
        UserDefaults.standard.set(value, forKey: INSIDESTATION)
    }
    /**获取站内信的开关*/
    static func getInsideStation() -> Bool{
        let  value = UserDefaults.standard.value(forKey: INSIDESTATION)
        if let values = value {
            return (values as? Bool)!
        }
        return true
    }
   
    
    static func setShouldOpenFloatTool(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: FLOAT_TOOL_SHOW)
    }

    static func setCurrentTheme(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: THEME_CURRENT)
    }
    
    static func getCurrentThme() -> Int {
        let value = UserDefaults.standard.integer(forKey: THEME_CURRENT)
        return value
    }
    
    static func setCurrentThemeByName(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: THEME_CURRENT_BYNAME)
    }
    
    static func getCurrentThmeByName() -> String {
        if let value = UserDefaults.standard.string(forKey: THEME_CURRENT_BYNAME) {
            return value
        }
        return ""
    }
    
    static func setIsChatBet(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: IS_CHAT_BET)
    }
    
    static func getIsChatBet() -> String {
        if let value = UserDefaults.standard.string(forKey: IS_CHAT_BET) {
            return value
        }
        return ""
    }
    
    static func setCurrentSoundEffect(value: AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SOUNDEFFECT_CURRENT)
    }
    
    static func setPlayTouzhuVolume(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: PLAY_TOUZHU_VOLUME)
    }
    
    static func isPlayTouzhuVolume() -> Bool {
        let value = UserDefaults.standard.bool(forKey: PLAY_TOUZHU_VOLUME)
        return value
    }
    
    static func setToken(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: TOKEN)
    }
    
    static func getToken() -> String {
        let value = UserDefaults.standard.string(forKey: TOKEN)
        if let v = value{
            return v
        }
        return ""
    }
    
    static func setAccountMode(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: ACCOUNT_TYPE)
    }
    
    
    
    static func getCurrentSoundEffect() -> Int {
        let value = UserDefaults.standard.integer(forKey: SOUNDEFFECT_CURRENT)
        return value
    }
    
    
    static func getAccountMode() -> Int {
        let value = UserDefaults.standard.integer(forKey: ACCOUNT_TYPE)
        return value
    }
    
    static func saveUserName(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: USERNAME)
    }
    
    static func getUserName() -> String {
        let value = UserDefaults.standard.string(forKey: USERNAME)
        if let nameValue = value{
            return nameValue
        }
        return ""
    }
    
    static func saveLoginStatus(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SAVE_LOGIN_STATUS)
    }
    
    static func getLoginStatus() -> Bool {
        let value = UserDefaults.standard.bool(forKey: SAVE_LOGIN_STATUS)
        return value
    }
    
    static func setNotToastWhenTouzhuEnd(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: NOT_TOAST_WHEN_ENDBET)
    }
    
    static func getNotToastWhenTouzhuEnd() -> Bool {
        let value = UserDefaults.standard.bool(forKey: NOT_TOAST_WHEN_ENDBET)
        return value
    }
    
    static func saveAutoLoginStatus(value:Bool) -> Void {
        UserDefaults.standard.set(value, forKey: AUTO_LOGIN_STATUS)
    }
    
    static func getAutoLoginStatus() -> Bool {
        let value = UserDefaults.standard.value(forKey: AUTO_LOGIN_STATUS)
        if  let values = value as? Bool{
            return values
        }
        //默认自动登录
        return true
    }

    
    static func saveMallStyle(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: MAIN_STYLE_ID)
    }
    
    static func getMallStyle() -> String {
        let value = UserDefaults.standard.string(forKey: MAIN_STYLE_ID)
        return value == "0" ? String.init(format: "%d", OLD_CLASSIC_STYLE) : value!
    }
    
    static func setMallImageTextTabStyle(value:AnyObject) -> Void{
        UserDefaults.standard.setValue(value, forKey: MALLIMAGETEXTSTYLE)
    }
    static func getMallImageTextTabSytle() -> String{
        let value = UserDefaults.standard.string(forKey: MALLIMAGETEXTSTYLE)
        return value ?? "on"
    }
    
    
    static func saveShakeTouzhuStatus(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SHAKE_TOUZHU)
    }
    
    static func getShakeTouzhuStatus() -> Bool {
        let value = UserDefaults.standard.bool(forKey: SHAKE_TOUZHU)
        return value
    }
    
    static func savePwdState(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SAVE_PWD_STATUS)
    }
    
    static func getPwdState() -> Bool {
        let value = UserDefaults.standard.bool(forKey: SAVE_PWD_STATUS)
        return value
    }
    
    static func setAskBeforeZuihao(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: ASK_BEFORE_ZUIHAO)
    }
    
    static func getAskBeforeZuihao() -> Bool {
        let value = UserDefaults.standard.bool(forKey: ASK_BEFORE_ZUIHAO)
        return value
    }
    
    static func savePwd(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SAVE_PWD)
    }
    
    static func getPwd() -> String {
        let value = UserDefaults.standard.string(forKey: SAVE_PWD)
        if let v = value{
            return v
        }
        return ""
    }
    
    static func save_signDate(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SAVE_SIGN_DATE)
    }
    
    static func getSignDate() -> String {
        let value = UserDefaults.standard.string(forKey: SAVE_SIGN_DATE)
        if let v = value{
            return v
        }
        return ""
    }
    
    static func saveTouzhuOrderJson(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SAVE_TOUZHU_ORDER_JSON)
    }
    
    static func getTouzhuOrderJson() -> String {
        let value = UserDefaults.standard.string(forKey: SAVE_TOUZHU_ORDER_JSON)
        if let v = value{
            return v
        }
        return ""
    }
    
    static func saveLotterys(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: SAVE_LOTTERYS)
    }
    
    static func getLotterys() -> String {
        let value = UserDefaults.standard.string(forKey: SAVE_LOTTERYS)
        if let v = value{
            return v
        }
        return ""
    }
    static func setYJFMode(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: YJF_MODE)
    }
    
    static func getYJFMode() -> String {
        var value = UserDefaults.standard.string(forKey: YJF_MODE)
        if isEmptyString(str: value!){
            value = YUAN_MODE
        }
        return value!
    }
    
    static func saveConfig(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: CONFIG)
    }

    static func getConfig() -> String {
        let value = UserDefaults.standard.string(forKey: CONFIG)
        if let v = value{
            return v
        }
        return ""
    }
    
    static func saveConfigNew(value: AnyObject) -> Void {
        UserDefaults.standard.setValue(value, forKey: CONFIG_NEW)
    }
    
    static func getConfigNew() -> String {
        let value = UserDefaults.standard.string(forKey: CONFIG_NEW)
        if let v = value
        {
            return v
        }
        
        return ""
    }
    static func saveUpdatePayment(value:AnyObject) -> Void{
        UserDefaults.standard.set(value, forKey: UPDATEVERSIONPAYMENT)
    }
    static func getUpdatePayment() -> String{
        let value = UserDefaults.standard.string(forKey: UPDATEVERSIONPAYMENT)
        if let update = value{
            return update
        }
        return "0"
    }
//    static func saveSelfUserId(value:AnyObject?) ->Void{
//        UserDefaults.standard.set(value,forKey:SELFUSERID)
//    }
//    static func getSelfUserId() ->String{
//        let value = UserDefaults.standard.string(forKey:SELFUSERID)
//        if let selfUserId = value {
//            return selfUserId
//        }
//        return ""
//    }
}



