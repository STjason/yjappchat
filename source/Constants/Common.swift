 //
//  Common.swift
//  SwiftSideslipLikeQQ
//
//  Created by JohnLui on 15/4/11.
//  Copyright (c) 2015年 com.lvwenhan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Kingfisher
import Reachability
import HandyJSON
import AVFoundation
import AudioToolbox
import UserNotifications



var audioPlayer: AVAudioPlayer!
 
 func separateStringToArray(string:String) -> [String] {
    let letters = string.map { String($0) }
    return letters
 }
 
 ///根据后台配置，对输入金额进行处理
 func getPayInfoMoney(postMoney:String,shouldRandom:Int) -> String{
     var postMoney = postMoney
     if  shouldRandom > 0 {
         if shouldRandom == 1{
             // 有小数去掉小数
             if postMoney.contains("."){
                 //
                 let postMoneyArray = postMoney.components(separatedBy: ".")
                 //取整数
                 let randomMoney = postMoneyArray[0];
                 //取1〜5的随机整数
                 let money =  Int(arc4random_uniform(5))+1
                 
                 postMoney = String.init(format:"%d",(Int(randomMoney)! + money))
             }else{
                 //1~5随机整数
                 let money =  Int(arc4random_uniform(5))+1
                 postMoney = String.init(format:"%d",(money + Int(postMoney)!))
             }
         }else if (shouldRandom == 2){
//             // 不包含小数
//             if !postMoney.contains("."){
//
//             }else{
//                 let randomNum = Float(arc4random_uniform(5/100))
//                 postMoney = String(format:"%.2f",(Float(postMoney)! + randomNum))
//             }
//
            let randomNum = Float(arc4random_uniform(49) + 1) / 100.0
            postMoney = String(format:"%.2f",(Float(postMoney)! + randomNum))
         }
     }
     return postMoney
 }
 
 //今天还有多少秒结束
 func todayRemainSeconds() -> Int {
     let date = Date()
     let calendar = Calendar.current

     let hour = calendar.component(.hour, from: date)
     let minutes = calendar.component(.minute, from: date)
     let seconds = calendar.component(.second, from: date)
     
     let secondsValue = hour * 60 * 60 + minutes * 60 + seconds
     let remainSeconds = 24 * 60 * 60 - secondsValue
     return remainSeconds
 }
 
 ///从字符串的首位截取掉 0为首的字符串
 func delZeroFrom(value:String) -> String {
    var array = separateStringToArray(string: value)
    if array.count > 1 && array[0] == "0" {
        array.remove(at: 0)
        return array.joined()
    }
    
    return value
 }
 
 public extension UIView {
 
     ///----------------------
     /// MARK: viewControllers
     ///----------------------

     /**
     Returns the UIViewController object that manages the receiver.
     */
     public func viewController()->UIViewController? {
         
         var nextResponder: UIResponder? = self
         
         repeat {
             nextResponder = nextResponder?.next
             
             if let viewController = nextResponder as? UIViewController {
                 return viewController
             }
             
         } while nextResponder != nil
         
         return nil
     }
 }
 
 func isSimulator() -> String {
    var isSimulator = false
    #if targetEnvironment(simulator)
      isSimulator = true
    #endif

    return "\(isSimulator)"
 }
 
 //MARK: - 获取IP
 public func GetIPAddresses() -> String? {
    var addresses = [String]()
    
    var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while (ptr != nil) {
            let flags = Int32(ptr!.pointee.ifa_flags)
            var addr = ptr!.pointee.ifa_addr.pointee
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        if let address = String(validatingUTF8:hostname) {
                            addresses.append(address)
                        }
                    }
                }
            }
            ptr = ptr!.pointee.ifa_next
        }
        freeifaddrs(ifaddr)
    }
    return addresses.first
 }

 
 ///view:渐变色的view
 func createGradientLayer(view:UIView,frame:CGRect,hexColors:[String]) {
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = frame
    
    var colors = [CGColor]()
    for hex in hexColors {
        let color = UIColor.colorWithHexString(hex).cgColor
        colors.append(color)
    }
    
    gradientLayer.colors = colors
    view.layer.addSublayer(gradientLayer)
    gradientLayer.startPoint = CGPoint.init(x: 0.0, y: 0.0)
    gradientLayer.endPoint = CGPoint.init(x: 1.0, y: 1.0)
 }

/// 如果小数点后面都是零，则返回其整数 String
 func formatPureIntIfIsInt(value:String) -> String {
    
    var originalValue = value
    var intValue = 0
    var floatValue:Float = 0.00
    
    if originalValue.hasSuffix(".") {
        originalValue += "0"
    }
    
    if let floatValueP = Float(originalValue) {
        floatValue = floatValueP
    }
  
    intValue = Int(floatValue)
    
    if Float(intValue) == floatValue {
        return "\(intValue)"
    }else {
        return "\(floatValue)"
    }
}
 
 ///view:渐变色的view
 func createGradientLayer(view:UIView,frame:CGRect,hexColors:[String],cornerRadius:CGFloat = 0) {
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = frame
    gradientLayer.cornerRadius = 3
    
    var colors = [CGColor]()
    for hex in hexColors {
        let color = UIColor.colorWithHexString(hex).cgColor
        colors.append(color)
    }
    
    gradientLayer.colors = colors
    view.layer.insertSublayer(gradientLayer, below: view.layer)
    gradientLayer.startPoint = CGPoint.init(x: 0.0, y: 0.0)
    gradientLayer.endPoint = CGPoint.init(x: 1.0, y: 1.0)
 }

//MARK: - 接口


/// 存款指南
///
/// - Parameters:
///   - controller: 调用的控制器
///   - bannerType: 在线入款 8，快速入款 6，银行入款 7
func requestDepositeGuidePictures(controller:BaseController, bannerType:String, success:@escaping ( (_ pictures:[String]) -> () ) ,failure:@escaping ( (_ msg:String)->() ) ) {
    
    if !switchWebpayGuide() {
        success([String]())
        return
    }
    
    let params = ["bannerType":bannerType] as [String: Any]
    controller.request(frontDialog: false, url: URL_CHARGE_INTRO,params:params) { [weak controller] (resultJson, resultStatus) in
        
        if let weakSelf = controller  {

            if !resultStatus {
                failure("getRechargeIntroPic.do获取失败")
                return
            }

            if let result = RechargeIntroPicWrapper.deserialize(from: resultJson) {
                if result.success{
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    if result.content.count > 0 {
                        success(result.content)
                    }
                }else {
                    if !isEmptyString(str: result.msg){
                        weakSelf.print_error_msg(msg: result.msg)
                    }

                    if (result.code == 0 || result.code == -1) {
                        loginWhenSessionInvalid(controller: weakSelf)
                    }
                    
                    failure("getRechargeIntroPic.do获取失败")
                }
            }
        
//            success(["https://yj6.me/img/wtIx/iijy4W0WR.jpg","https://yj6.me/img/wtIx/iBuGtqHP6.png"])
        }
    }
}

func playAlertSound(path:String, isShock:Bool=false) {//播放音频或震动
    let path = Bundle.main.path(forResource: path, ofType: "mp3")
    let pathURL = NSURL(fileURLWithPath: path!)
    
    if isShock {//true 音频带震动
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }

    do {
        audioPlayer = try AVAudioPlayer(contentsOf: pathURL as URL)
        audioPlayer?.play()
//        audioPlayer?.volume = 80 //调节音量
    } catch {
        audioPlayer = nil
    }
}

let lunarInfo = [ 0x04bd8, 0x04ae0, 0x0a570,
                  0x054d5, 0x0d260, 0x0d950, 0x16554, 0x056a0, 0x09ad0, 0x055d2,
                  0x04ae0, 0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 0x0d6a0,
                  0x0ada2, 0x095b0, 0x14977, 0x04970, 0x0a4b0, 0x0b4b5, 0x06a50,
                  0x06d40, 0x1ab54, 0x02b60, 0x09570, 0x052f2, 0x04970, 0x06566,
                  0x0d4a0, 0x0ea50, 0x06e95, 0x05ad0, 0x02b60, 0x186e3, 0x092e0,
                  0x1c8d7, 0x0c950, 0x0d4a0, 0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4,
                  0x025d0, 0x092d0, 0x0d2b2, 0x0a950, 0x0b557, 0x06ca0, 0x0b550,
                  0x15355, 0x04da0, 0x0a5d0, 0x14573, 0x052d0, 0x0a9a8, 0x0e950,
                  0x06aa0, 0x0aea6, 0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260,
                  0x0f263, 0x0d950, 0x05b57, 0x056a0, 0x096d0, 0x04dd5, 0x04ad0,
                  0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b5a0, 0x195a6,
                  0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40,
                  0x0af46, 0x0ab60, 0x09570, 0x04af5, 0x04970, 0x064b0, 0x074a3,
                  0x0ea50, 0x06b58, 0x055c0, 0x0ab60, 0x096d5, 0x092e0, 0x0c960,
                  0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 0x0abb7, 0x025d0,
                  0x092d0, 0x0cab5, 0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9,
                  0x04ba0, 0x0a5b0, 0x15176, 0x052b0, 0x0a930, 0x07954, 0x06aa0,
                  0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260, 0x0ea65,
                  0x0d530, 0x05aa0, 0x076a3, 0x096d0, 0x04bd7, 0x04ad0, 0x0a4d0,
                  0x1d0b6, 0x0d250, 0x0d520, 0x0dd45, 0x0b5a0, 0x056d0, 0x055b2,
                  0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0 ];

var showDragBtnHandler:((_ show:Bool,_ holdVC:BaseMainController) -> Void)? //聊天室

struct Common {
    // Swift 中， static let 才是真正可靠好用的单例模式
    static let screenWidth = UIScreen.main.applicationFrame.maxX
    static let screenHeight = UIScreen.main.applicationFrame.maxY
}


func DownloadhandleImageURL(urlString: String) -> String{

    var logoImg = urlString
    if logoImg.contains("\t"){
        let strs = logoImg.components(separatedBy: "\t")
        if strs.count >= 2{
            logoImg = strs[1]
        }
    }
    logoImg = logoImg.trimmingCharacters(in: .whitespaces)
    if !logoImg.hasPrefix("https://") && !logoImg.hasPrefix("http://"){
        logoImg = String.init(format: "%@/%@", BASE_URL,logoImg)
    }
    
    return logoImg
}

// MARK: 字符串转字典
func stringValueDic(_ str: String) -> [String : Any]?{
    let data = str.data(using: String.Encoding.utf8)
    if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
        return dict
    }
    return nil
}

//下载用户头像
func DownloadUserAvatar(Controller:BaseController){
    if YiboPreference.getCACHEAVATARdata().count <= 0 {
        DownloadAvatar(Controller: Controller)
    }
}

func DownloadAvatarPageLogoUrl(Controller:BaseController){
    //取后台统一配置的头像，针对新用户
    guard let sys = getSystemConfigFromJson() else{return}
    var logoImg = sys.content.member_page_logo_url
    if isEmptyString(str: logoImg) == false {
        logoImg = DownloadhandleImageURL(urlString: logoImg)
        let logoImgurl = URL.init(string: logoImg)
        getDataFromUrl(url: logoImgurl!) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                YiboPreference.setCACHEAVATARdata(value: data)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue:"DownloadAvatar"), object: Controller, userInfo:nil)
            }
        }
    }
}

//获取头像 首页异步获取,登陆再次获取
func DownloadAvatar(Controller:BaseController){
    //登陆限制
    let key = YiboPreference.getPwd() + YiboPreference.getUserName() + "project-yunji"+getAPPID()
    let dic = ["headerKey" : MD5(key)]
    Controller.request(frontDialog: false, method:.post, loadTextStr:"", url:URL_HEADER_DER, params: dic) { (resultJson:String, resultStatus:Bool) in
        if resultStatus {
            let dic = stringValueDic(resultJson)
            if let dict = dic{
                let content = dict["content"] as! String
                if content.count >= 1 {
                    let url = URL.init(string: content)!
                    getDataFromUrl(url: url) { (data, response, error)  in
                        guard let data = data, error == nil else { return }
                        DispatchQueue.main.async() { () -> Void in
                            YiboPreference.setCACHEAVATARdata(value: data)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"DownloadAvatar"), object: Controller, userInfo:nil)
                        }
                    }
                }else{
                    DownloadAvatarPageLogoUrl(Controller: Controller)
                }
            }
           
        }else{
            DownloadAvatarPageLogoUrl(Controller: Controller)
        }
    }
}
 
 //上传头像
 func UploadAvatarLocal(url:String,dic:[String: Any],imageData:Data,callback:@escaping (_ resultJson:[String:Any],_ returnStatus:Bool)->()) -> Void {

     if let url = URL(string: url) {
         var request = URLRequest(url: url)
         let boundary:String = "Boundary-\(UUID().uuidString)"
         request.httpMethod = "POST"
         request.timeoutInterval = 60
         request.allHTTPHeaderFields = ["User-Agent":String.init(format: "ios/%@", getVerionName()),"Content-Type": "multipart/form-data; boundary=----\(boundary)"]
         //参数
         var data: Data = Data()
         for (key,value) in dic
         {
             data.append("------\(boundary)\r\n")
             data.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
             data.append("\(value)\r\n")
         }
         data.append("------\(boundary)\r\n")
         data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\("fileString")\"\r\n")
         data.append("Content-Type: application/YourType\r\n\r\n")
         data.append(imageData)
         data.append("\r\n")
         data.append("------\(boundary)--")
         request.httpBody = data
         //post
             DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).sync {
                 let session = URLSession.shared
                 _ = session.dataTask(with: request, completionHandler: { (dataS, aResponse, error) in
                     if let erros = error{
                         print("error = \(erros)")
                     }else{
                         do{
                             let responseObj = try JSONSerialization.jsonObject(with: dataS!, options: JSONSerialization.ReadingOptions(rawValue:0)) as! [String:Any]
                             
                             if let success = responseObj["success"] as? Int
                             {
                                 if success == 1
                                 {
                                     callback(responseObj,true)
                                 }else{
                                     callback(responseObj,false)
                                 }
                             }
                             
                         }catch let e{
                             let errer = ["errer":e.localizedDescription] as [String:Any]
                             callback(errer,false)
                         }
                     }
                 }).resume()
             }
         }
 }

//上传头像
func UploadAvatar(imageData:Data,callback:@escaping (_ resultJson:[String:Any],_ returnStatus:Bool)->()) -> Void {
    let num = Int(arc4random_uniform(89999) + 10000)
    let filename = String.init(format:"%i.png",num)
    var dic:Dictionary<String,Any>!
    dic =
        [   "userName" : YiboPreference.getUserName(),
            "stationId": YiboPreference.getPwd(),
            "pid": getAPPID(),
            "projectId" : 2 ]
    
    if let url = URL(string: URL_HEADER_SAVE){
        var request = URLRequest(url: url)
        let boundary:String = "Boundary-\(UUID().uuidString)"
        request.httpMethod = "POST"
        request.timeoutInterval = 60
        request.allHTTPHeaderFields = ["User-Agent":String.init(format: "ios/%@", getVerionName()),"Content-Type": "multipart/form-data; boundary=----\(boundary)"]
        //参数
        var data: Data = Data()
        for (key,value) in dic
        {
            data.append("------\(boundary)\r\n")
            data.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            data.append("\(value)\r\n")
        }
        data.append("------\(boundary)\r\n")
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        data.append("Content-Type: application/YourType\r\n\r\n")
        data.append(imageData)
        data.append("\r\n")
        data.append("------\(boundary)--")
        request.httpBody = data
        //post
            DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).sync {
                let session = URLSession.shared
                _ = session.dataTask(with: request, completionHandler: { (dataS, aResponse, error) in
                    if let erros = error{
                        print("error = \(erros)")
                    }else{
                        do{
                            let responseObj = try JSONSerialization.jsonObject(with: dataS!, options: JSONSerialization.ReadingOptions(rawValue:0)) as! [String:Any]
                            
//                           ["success": 1, "content": https://yj8.me/header/iXMGfdDxF/iXMGfdDx8.png, "msg": 上传成功]
                            
                            if let success = responseObj["success"] as? Int
                            {
                                if success == 1
                                {
                                    callback(responseObj,true)
//                                    deleteCrashFile()
                                }else{
                                    callback(responseObj,false)
                                }
                            }
                            
                        }catch let e{
                            let errer = ["errer":e.localizedDescription] as [String:Any]
                            callback(errer,false)
                        }
                    }
                }).resume()
            }
        }
}

/** 去除url中的空格 */
func formatUrl(url:String) -> String {
    var tempUrl = url
    if tempUrl.contains("\t"){
        let s = tempUrl.replacingOccurrences(of: "\t", with: "")
        if s.contains("\t") {
            _ = formatUrl(url: s)
        }
    }
    
    tempUrl = tempUrl.trimmingCharacters(in: .whitespaces)
    return tempUrl
}
//默认开奖号码,统一走这里
func DefaultLotteryNumber()->String{
    var code = "CQSSC"
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            code = config.content.default_lot_code
        }
    }
    return code
}

func DefaultLotteryLotTypeNumber(type:String)->String{
    if LennyModel.allLotteryTypesModel != nil {
        if LennyModel.allLotteryTypesModel!.obtainAllLotteryWithIndex().count >= 1 {
            return LennyModel.allLotteryTypesModel!.obtainAllLotteryWithIndex()[0].lotType ?? ""
        }
    }
    return type
}

func openPopViewinitializationClick(requestSelf:BaseController,controller:UIViewController){
    //   1=网站首页，2=个人中心，3=登录页面，4=注册页面，5=彩票投注页面
    if YiboPreference.getFloatingWindowONdata() == false {
        return
    }

    let showPage = VCshowPageManagement(controller: controller)
    if showPage == 0 {
        return
    }

    requestSelf.request(frontDialog: false, method: .get, loadTextStr: "", url: QRCODE_WECHAT_IMAGES, params: ["showPage":showPage,"levelId":1]) { (resultJson:String, resultStatus:Bool) in
        if !resultStatus {
            if resultJson.isEmpty {
                showToast(view: controller.view, txt: convertString(string: "提交失败"))
            }else{
                showToast(view: controller.view, txt: resultJson)
            }
            return
        }
        print(resultJson)
        if let result = WechatQRCodeWraper.deserialize(from: resultJson)
        {
            if result.success {
                YiboPreference.setToken(value: result.accessToken as AnyObject)
                if let datas = result.content{

                    if datas.count == 0 {return}
                    var imgType = false
                    if datas[0].imgType == "1" {
                        imgType = true
                    }
                    let showPosition = datas[0].showPosition
                    if let afsList = datas[0].afsList{
                        if afsList.count > 0{
                            
                            popViewinitialization(positionStr:showPosition, view:controller.view, array: afsList as NSArray, type:imgType,Controller: controller)
                        }

                    }
                }
            }else{
                if !isEmptyString(str: result.msg){
                    showToast(view: controller.view, txt: result.msg)
                }else{
                    showToast(view: controller.view, txt: convertString(string: "获取客服二维码失败"))
                }
            }
        }
    }
    //exit
}

/** 平台公告提示框2个版本
 from:0 首页，1 投注页面 2，支付页面 3，注册页面 4,个人中心 站内信
 */
func PopupAlertListView(resultJson:String,controller:UIViewController,not:String,anObject:Any?,from:Int = -1){
    var Switch  = true
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil{
            if config.content.multi_list_dialog_switch == "on"
            {
                Switch = false
            }
        }
    }

    if Switch {
        if not.isEmpty{
            if let result  = NoticeResultWraper.deserialize(from:resultJson){
                let popView = PopupAlertView.init(frame: controller.view.bounds)
                
                if let notices = result.content {
                    let models = handleNoticeResult(notices: notices, from: from)
                    if models.count > 0 {
                        popView.assignmentDataArray = models
                        controller.view.addSubview(popView)
                    }
                }
            }
        }else{ //controller为nil，弄通知去显示
            NotificationCenter.default.post(name: NSNotification.Name(not), object:anObject, userInfo:["resultJson":resultJson])
        }
    }else{
        
        if let result  = NoticeResultWraper.deserialize(from:resultJson){
            if let notices = result.content{
                //显示公告内容
                if notices.isEmpty{
                    return
                }else {
                    let models = handleNoticeResult(notices: notices, from: from)
                    if models.count > 0 {
                        let weblistView = WebviewList.init(noticeResuls: models)
                        weblistView.show()
                    }
                    
                }
            }
        }
    }
    
}


/// 处理手机弹窗数据，排序和过滤
///
/// - Parameters:
///   - notices: 所有弹窗数据
///   - from: 要显示的页面
/// - Returns: 排序和过滤后的弹窗数据
private func handleNoticeResult(notices:[NoticeResult],from:Int) -> [NoticeResult] {
    var noticesP = notices
    noticesP = noticesP.sorted { (noticesP1, noticesP2) -> Bool in
        return noticesP1.sortNum < noticesP2.sortNum
    }
    
    var models = [NoticeResult]()
    for index in 0..<noticesP.count {
        let model = noticesP[index]
        switch from {
        case 0:
            if model.isIndex {
                models.append(model)
            }
            break
        case 1:
            if model.isBet {
                models.append(model)
            }
        case 2:
            if model.isDeposit {
                models.append(model)
            }
        case 3:
            if model.isReg {
                models.append(model)
            }
        default:
            print("")
        }
    }
    
    return models
}



// 使用浏览器打开url
func openUrlWithBrowser(url:URL,view:UIView) {
    UIApplication.shared.openURL(url)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        if UIApplication.shared.applicationState == UIApplication.State.active {
            showToast(view:view , txt: "地址不正确，请联系客服!")
        }
    }
}

// 是否为同一天
func isSameDateWithNoTipsDate() -> Bool {
    let currentDate = Date()
    if let date = YiboPreference.getNotipsDate() {
        return Calendar.current.isDate(currentDate, inSameDayAs: date)
    }else {
        return false
    }
}

//显示文字提示框
func showToast(view:UIView,txt:String,afterDelay:TimeInterval = 1) {
    //全局修改ActivityIndicator为白色
    UIActivityIndicatorView.appearance(whenContainedInInstancesOf: [MBProgressHUD.self]).color = .white
    var dialog:MBProgressHUD!
    if let window = view.window{
        dialog = MBProgressHUD.showAdded(to:  window, animated: true)
    }else{
        dialog = MBProgressHUD.showAdded(to: view, animated: true)
    }
    //    dialog.label.text = txt
    dialog.detailsLabel.text = txt
    dialog.isSquare = false
    dialog.mode = .text
    dialog.label.textColor = UIColor.white
    dialog.detailsLabel.textColor = UIColor.white
    dialog.bezelView.color = UIColor.black
    dialog.hide(animated: true, afterDelay: afterDelay)
    dialog.bezelView.blurEffectStyle = UIBlurEffect.Style.light
}

//显示加载框
func showLoadingDialog(view:UIView,loadingTxt txt:String) -> MBProgressHUD {
    //全局修改ActivityIndicator为白色
    UIActivityIndicatorView.appearance(whenContainedInInstancesOf: [MBProgressHUD.self]).color = .white
    let hud = MBProgressHUD.showAdded(to: view, animated: true)
    hud.label.text = txt
    hud.label.textColor = UIColor.white
    hud.bezelView.color = UIColor.black
    hud.label.layoutMargins = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
    hud.backgroundView.style = .solidColor
    hud.bezelView.blurEffectStyle = UIBlurEffect.Style.light
//    hud.activityIndicatorColor = UIColor.white
    hud.hide(animated: true, afterDelay: 2)
    return hud
}
//隐藏加载框
func hideLoadingDialog(hud:MBProgressHUD)  {
//    Thread.sleep(forTimeInterval: 1000000)
    hud.hide(animated: true)
}

func convertString(string: String) -> String {
    let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
    return String(bytes: data!, encoding: String.Encoding.utf8)!
}

//clear picture cache
func clearKingFisher() -> Void {
    let cache = KingfisherManager.shared.cache
    // 清理硬盘缓存，这是一个异步的操作
    cache.clearDiskCache()
    // 清理过期或大小超过磁盘限制缓存。这是一个异步的操作
    cache.cleanExpiredDiskCache()
}

func isEmptyString(str:String) -> Bool {
    if str.isEmpty{
        return true
    }
    return false
}

func isPurnInt(string: String) -> Bool {
    let scan: Scanner = Scanner(string: string)
    var val:Int = 0
    return scan.scanInt(&val) && scan.isAtEnd
}

func convertPostMode(mode:String) -> Int {
    if (mode == YUAN_MODE) {
        return 1;
    } else if (mode == JIAO_MODE) {
        return 10;
    } else if (mode == FEN_MODE) {
        return 100;
    }else {
        return 1;
    }
    
}

func str_from_mode(mode:String) -> String{
    if mode == YUAN_MODE{
        return "元"
    }else if mode == JIAO_MODE{
        return "角"
    }else if mode == FEN_MODE{
        return "分"
    }else {
        return "元"
    }
    
}
func getShengXiaoFromYear() -> String{
    let calendar:Calendar = Calendar(identifier: .chinese);
    var comps:DateComponents = DateComponents()
    comps = calendar.dateComponents([.year,.month,.second], from: Date())
    print("当前年份的农历时第几年 === \(comps.year!)")
    let shenxiao = getChineseYearWithDate(year: comps.year! - 1)
    return shenxiao
}

//date -- 指定的日期
func getShengXiaoFromYearBaseDate(date:Date) -> String{
    let calendar:Calendar = Calendar(identifier: .chinese);
    var comps:DateComponents = DateComponents()
    comps = calendar.dateComponents([.year,.month,.second], from: date)
    print("当前年份的农历时第几年 === \(comps.year!)")
    let shenxiao = getChineseYearWithDate(year: comps.year! - 1)
    return shenxiao
}
//year -- 农历第几年
func getShengXiaoFromYear(year:Int) -> String{
//    let str = String(format: "%.2f", Float(year)/60)//六十nian一甲子
//    let y = ceilf(Float(str)!);
//    let benming_sx = getChineseYearWithDate(year: Int(y))
//    return benming_sx
    let sx = shenxiao_str[(year - 1900 + 36)%12]
    print("sx === ",sx)
    return sx
}

//year -- 农历第几年
func getChineseYearWithDate(year:Int) -> String {
    
    let chineseYears = ["甲子", "乙丑", "丙寅", "丁卯",  "戊辰",  "己巳",  "庚午",  "辛未",  "壬申",  "癸酉",
                        "甲戌",   "乙亥",  "丙子",  "丁丑", "戊寅",   "己卯",  "庚辰",  "辛巳",  "壬午",  "癸未",
                        "甲申",   "乙酉",  "丙戌",  "丁亥",  "戊子",  "己丑",  "庚寅",  "辛卯",  "壬辰",  "癸巳",
                        "甲午",   "乙未",  "丙申",  "丁酉",  "戊戌",  "己亥",  "庚子",  "辛丑",  "壬寅",  "癸卯",
                        "甲辰",   "乙巳",  "丙午",  "丁未",  "戊申", "己酉",  "庚戌",  "辛亥",  "壬子",  "癸丑",
                        "甲寅",   "乙卯",  "丙辰",  "丁巳",  "戊午",  "己未",  "庚申",  "辛酉",  "壬戌",  "癸亥"]
    
//    let calendar:Calendar = Calendar(identifier: .chinese);
//    var comps:DateComponents = DateComponents()
//    comps = calendar.dateComponents([.year,.month,.second], from: date)
    
//    let y_str = chineseYears[comps.year! - 1]
    let y_str = chineseYears[year]
    var shenxiao = ""
    if y_str.hasSuffix("子"){
        shenxiao = "鼠"
    }else if y_str.hasSuffix("丑"){
        shenxiao = "牛"
    }else if y_str.hasSuffix("寅"){
        shenxiao = "虎"
    }else if y_str.hasSuffix("卯"){
        shenxiao = "兔"
    }else if y_str.hasSuffix("辰"){
        shenxiao = "龙"
    }else if y_str.hasSuffix("巳"){
        shenxiao = "蛇"
    }else if y_str.hasSuffix("午"){
        shenxiao = "马"
    }else if y_str.hasSuffix("未"){
        shenxiao = "羊"
    }else if y_str.hasSuffix("申"){
        shenxiao = "猴"
    }else if y_str.hasSuffix("酉"){
        shenxiao = "鸡"
    }else if y_str.hasSuffix("戌"){
        shenxiao = "狗"
    }else if y_str.hasSuffix("亥"){
        shenxiao = "猪"
    }
    return shenxiao
}

func getShenxiaoNumbers(startIndex:Int) -> String{
    let maxValue = 49
    var tmp = startIndex
    var sb = ""
    while tmp <= maxValue {
        sb = sb + String.init(format: "%02d,", tmp)
        tmp = tmp + 12
    }
    if sb.count > 0{
        let index = sb.index(sb.endIndex, offsetBy:-1)
        sb = sb.substring(to: index)
    }
    return sb
}

func getNumbersFromShenXiaoName(sx:String) -> [String]{
    let numbers = getNumbersFromShengXiao()
    if !numbers.isEmpty{
        for s in numbers{
            if s.contains(sx){
                let split = s.components(separatedBy: "|")
                if split.count == 2{
                    let fs = split[1].components(separatedBy: ",")
                    return fs
                }
            }
        }
    }
    return []
}
func getNumberStrFromShenXiaoName(sx:String) -> String{
    let numbers = getNumbersFromShengXiao()
    if !numbers.isEmpty{
        for s in numbers{
            if s.contains(sx){
                let split = s.components(separatedBy: "|")
                if split.count == 2{
                    return split[1]
                }
            }
        }
    }
    return ""
}

func getNumberStrFromShenXiaoName(sx:String,serverTime:Int64) -> String{
    var numbers:[String] = []
    if serverTime > 0{
        numbers = getNumbersFromShengXiao(serverBetTime: serverTime)
    }else{
        numbers = getNumbersFromShengXiao()
    }
    if !numbers.isEmpty{
        for s in numbers{
            if s.contains(sx){
                let split = s.components(separatedBy: "|")
                if split.count == 2{
                    return split[1]
                }
            }
        }
    }
    return ""
}

func getWeishuArrays() -> [String]{
    var list = [String]()
    list.append("1|1,11,21,31,41")
    list.append("2|2,12,22,32,42")
    list.append("3|3,13,23,33,43")
    list.append("4|4,14,24,34,44")
    list.append("5|5,15,25,35,45")
    list.append("6|6,16,26,36,46")
    list.append("7|7,17,27,37,47")
    list.append("8|8,18,28,38,48")
    list.append("9|9,19,29,39,49")
    list.append("0|10,20,30,40")
    return list;
}

func getWeishuFromArrays(index:String) ->String{
    let array = getWeishuArrays()
    for item in array {
        if  index.contains(item.subString(start: 0, length: 1)){
            return item
        }
    }
//    if array.count > 0{
//        return array[index]
//    }
    return ""
}

func getHeadEndFrom(index:String) -> String{
    
    let array = getWeishuArrays()
    for item in array {
        if  index.contains(item.subString(start: 0, length: 1)){
            return item
        }
    }
    
    return ""
}


func getNumbersFromShengXiao() -> [String]{
    let shenxiao = getShengXiaoFromYear();
    var results = Array<String>(repeating:"",count:shenxiao_str.count)
    var bmnIndex = 0;
    for i in 0...shenxiao_str.count-1{
        let item = shenxiao_str[i]
        if item == shenxiao{
            bmnIndex = i
            break
        }
    }
    for i in 0...shenxiao_str.count-1{
        var startNum = 0
        if i <= bmnIndex{
            startNum = bmnIndex - i + 1
        }else{
            startNum = i - bmnIndex - 1
            startNum = 12 - startNum
        }
        let numResult = String.init(format: "%@%@%@", shenxiao_str[i],"|",getShenxiaoNumbers(startIndex:startNum))
        results[i] = numResult
    }
    return results;
}

func getNumbersFromShengXiao(serverBetTime:Int64) -> [String]{
    var shenxiao = ""
    if serverBetTime > 0{
        let date = Date.init(timeIntervalSince1970: TimeInterval(serverBetTime/1000));
        shenxiao = getShengXiaoFromYearBaseDate(date: date);
    }else{
        shenxiao = getShengXiaoFromYear()
    }
    var results = Array<String>(repeating:"",count:shenxiao_str.count)
    var bmnIndex = 0;
    for i in 0...shenxiao_str.count-1{
        let item = shenxiao_str[i]
        if item == shenxiao{
            bmnIndex = i
            break
        }
    }
    for i in 0...shenxiao_str.count-1{
        var startNum = 0
        if i <= bmnIndex{
            startNum = bmnIndex - i + 1
        }else{
            startNum = i - bmnIndex - 1
            startNum = 12 - startNum
        }
        let numResult = String.init(format: "%@%@%@", shenxiao_str[i],"|",getShenxiaoNumbers(startIndex:startNum))
        results[i] = numResult
    }
    return results;
}

//根据号码和指定的农历年分，计算出号码对应的生肖
func getShenxiaoFromNumberAndBenmingYear(number:String,year:Int) -> String{
    var shenxiao = ""
    if year == 0{
        shenxiao = getShengXiaoFromYear();
    }else{
        shenxiao = getShengXiaoFromYear(year:year);//指定的农历年分对应的生肖
    }
    var bmnIndex = 0;
    for i in 0...shenxiao_str.count-1{
        let item = shenxiao_str[i]
        if item == shenxiao{
            bmnIndex = i
            break
        }
    }
    for i in 0...shenxiao_str.count-1{
        var startNum = 0
        if i <= bmnIndex{
            startNum = bmnIndex - i + 1
        }else{
            startNum = i - bmnIndex - 1
            startNum = 12 - startNum
        }
        let numbers = getShenxiaoNumbers(startIndex:startNum)
        for num in numbers.components(separatedBy: ","){
            if num == number || num == "0\(number)"{
                return shenxiao_str[i]
            }
        }
    }
    return "";
}

//根据号码和指定的date，计算出号码对应的生肖
func getShenxiaoFromNumberAndDate(number:String,date:Date?) -> String{
    var shenxiao = ""
    if date == nil{
        shenxiao = getShengXiaoFromYear();//指定的农历年分对应的生肖
    }else{
        shenxiao = getShengXiaoFromYearBaseDate(date: date!)//指定的农历年分对应的生肖
    }
    var bmnIndex = 0;
    for i in 0...shenxiao_str.count-1{
        let item = shenxiao_str[i]
        if item == shenxiao{
            bmnIndex = i
            break
        }
    }
    for i in 0...shenxiao_str.count-1{
        var startNum = 0
        if i <= bmnIndex{
            startNum = bmnIndex - i + 1
        }else{
            startNum = i - bmnIndex - 1
            startNum = 12 - startNum
        }
        let numbers = getShenxiaoNumbers(startIndex:startNum)
        for num in numbers.components(separatedBy: ","){
            if num == number || num == "0\(number)"{
                return shenxiao_str[i]
            }
        }
    }
    return "";
}

func loginWhenSessionInvalid(controller:UIViewController,canBack:Bool=true) -> Void {

    if !(controller.isViewLoaded) {
        return
    }
    let loginVC = UIStoryboard(name: "login", bundle: nil).instantiateViewController(withIdentifier: "login_page")
    let loginPage = loginVC as! LoginController
    loginPage.openFromOtherPage = true
    loginPage.canBack = canBack
    
    controller.navigationController?.pushViewController(loginPage, animated: true)
}

func showWhenWebsiteMintancce(controller:UIViewController,cause:String="") -> Void {
    
    if !(controller.isViewLoaded) {
        return
    }
    let pvc = UIStoryboard(name: "cause", bundle: nil).instantiateViewController(withIdentifier: "cause")
    let page = pvc as! CauseController
    page.causeStr = cause
    if controller.navigationController == nil{
        let nav = UINavigationController.init(rootViewController: page)
        controller.present(nav, animated: true, completion: nil)
    }else{
        controller.navigationController?.pushViewController(page, animated: true)
    }
}

/// 跳转临时活动页面
func openGeneralActivityController(controller:UIViewController,url:String,name:String) -> Void {
    
    //    if !YiboPreference.getLoginStatus(){
    //        loginWhenSessionInvalid(controller: controller)
    //        return
    openActiveDetail(controller: controller, title: name, content: "",foreighUrl: BASE_URL+PORT+url)
}

func openRegisterPage(controller:UIViewController) -> Void {
    
    if !(controller.isViewLoaded && (controller.view.window != nil)){
        return
    }
    let loginVC = UIStoryboard(name: "register_page", bundle: nil).instantiateViewController(withIdentifier: "newRegisterController")
    let regPage = loginVC as! NewRegisterController
    controller.navigationController?.pushViewController(regPage, animated: true)
}

func openLoginPage(controller:UIViewController) -> Void {
    if !(controller.isViewLoaded && (controller.view.window != nil)){
        return
    }
    let loginVC = UIStoryboard(name: "login", bundle: nil).instantiateViewController(withIdentifier: "login_page")
    let loginPage = loginVC as! LoginController
    loginPage.openFromOtherPage = true
    controller.navigationController?.pushViewController(loginPage, animated: true)
}

func openFeedback(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "device_feedback", bundle: nil).instantiateViewController(withIdentifier: "feedback")
    let settingPage = loginVC as! DeviceFeedBackController
    controller.navigationController?.pushViewController(settingPage, animated: true)
}

//func openWebPay(controller:UIViewController,url:String,params:Dictionary<String,Any>) -> Void {
//    let loginVC = UIStoryboard(name: "pay_web", bundle: nil).instantiateViewController(withIdentifier: "payWeb")
//    let page = loginVC as! PayWebController
//    page.url = url
//    page.params = params
//    controller.navigationController?.pushViewController(page, animated: true)
//}

func openSetting(controller:UIViewController,from:Int=0) -> Void {
    let loginVC = UIStoryboard(name: "app_setting", bundle: nil).instantiateViewController(withIdentifier: "appSetting")
    let settingPage = loginVC as! AppSettingController
    settingPage.from = from
    controller.navigationController?.pushViewController(settingPage, animated: true)
}

func openRainController(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "rain_packet", bundle: nil).instantiateViewController(withIdentifier: "rain_page")
    let page = loginVC as! RainPackageController
    controller.navigationController?.pushViewController(page, animated: true)
}


//进入奖金版主单页
func openBetOrderPage(controller:UIViewController,
                      data:[OrderDataInfo],
                      lotCode:String,
                      lotName:String,
                      subPlayCode:String,
                      subPlayName:String,
                      cpTypeCode:String,
                      cpVersion:String,
                      officail_odds:[PeilvWebResult],
                      icon:String="",
                      meminfo:Meminfo?,
                      fromHistory:Bool = false,
                      officalBonus:Double?) -> Void {
    let loginVC = UIStoryboard(name: "touzhu_order_page", bundle: nil).instantiateViewController(withIdentifier: "confirm_order")
    let page = loginVC as! TouzhuOrderController
    page.order = data
    page.cpBianHao = lotCode
    page.cpName = lotName
    page.subPlayCode = subPlayCode
    page.subPlayName = subPlayName
    page.cpTypeCode = cpTypeCode
    page.cpVersion = cpVersion
    page.officail_odds = officail_odds
    page.lotteryIcon = icon
    page.meminfo = meminfo
    page.fromHistory = fromHistory
    page.officalBonus = officalBonus
    controller.navigationController?.pushViewController(page, animated: true)
}

func openPeilvBetOrderPage(controller:UIViewController,order:[OrderDataInfo],
                           peilvs:[BcLotteryPlay],lhcLogic:LHCLogic2,
                           subPlayName:String,subPlayCode:String,
                           cpTypeCode:String,cpBianHao:String,
                           current_rate:Float,cpName:String,cpversion:String="",icon:String="",fromHistory:Bool = false) -> Void {
    let loginVC = UIStoryboard(name: "peilv_order_page", bundle: nil).instantiateViewController(withIdentifier: "peilv_order")
    
    let page = loginVC as! PeilvOrderController
    page.order = order
    page.peilvs = peilvs
    page.lhcLogic = lhcLogic
    
    page.subPlayName = subPlayName
    page.subPlayCode = subPlayCode
    page.cpTypeCode = cpTypeCode
    page.cpBianHao = cpBianHao
    page.current_rate = current_rate
    page.cpName = cpName
    page.cpVersion = cpversion
    page.lotteryicon = icon
    page.fromHistory = fromHistory
    
    controller.navigationController?.pushViewController(page, animated: true)
}

//func openContactUs(controller:UIViewController) -> Void {
//    
//    var url:String = ""
//    let system = getSystemConfigFromJson()
//    if let value = system{
//        let datas = value.content
//        if let datasValue = datas{
//            url = datasValue.customerServiceUrlLink
//        }
//    }
//    if isEmptyString(str: url){
//        showToast(view: controller.view, txt: "没有在线客服链接，请检查是否配置")
//        return
//    }
//    let url2 = URL(string: url)
//    if #available(iOS 10.0, *) {
//        UIApplication.shared.open(url2!, options: ["":""]) { (yes) in
//            if !yes {
//                showToast(view: controller.view, txt: "跳转链接错误")
//            }
//        }
//    } else {
//       let yes =  UIApplication.shared.canOpenURL(url2!)
//        if !yes {
//            showToast(view: controller.view, txt: "跳转链接错误")
//        }
//    }
//}
 
 func openContactUs(controller:UIViewController) -> Void {
    
    var version = getSystemConfigFromJson()?.content.online_service_open_switch
    version = "v2"
    if version == "v1"{
        var url:String = ""
        let system = getSystemConfigFromJson()
        if let value = system{
            let datas = value.content
            if let datasValue = datas{
                url = datasValue.customerServiceUrlLink
            }
        }
        if isEmptyString(str: url){
            showToast(view: controller.view, txt: "没有在线客服链接，请检查是否配置")
            return
        }
        url = url.trimmingCharacters(in: .whitespaces)
        if let urlstring = URL.init(string: url){
            UIApplication.shared.openURL(urlstring)
        }else{
            showToast(view: controller.view, txt: "链接不正确")
        }
        
    }else if version == "v2"{
        var url:String = ""
        let system = getSystemConfigFromJson()
        if let value = system{
            let datas = value.content
            if let datasValue = datas{
                url = datasValue.customerServiceUrlLink
            }
        }
        if isEmptyString(str: url){
            showToast(view: controller.view, txt: "没有在线客服链接，请检查是否配置")
            return
        }
        let vc = InteriorOpenServiceController()
        vc.webURL = url
        if controller.navigationController != nil{
            controller.navigationController?.pushViewController(vc, animated: true)
        }else{
            controller.navigationController?.pushViewController(vc, animated: true)
            let nav = UINavigationController.init(rootViewController: vc)
            controller.present(nav, animated: true, completion: nil)
        }
    }
 }

func getSystemConfigFromJson() -> SystemWrapper?{
    let configJson = YiboPreference.getConfig()
    if !isEmptyString(str: configJson){
        if let lotWraper = JSONDeserializer<SystemWrapper>.deserializeFrom(json: configJson) {
            if lotWraper.success{
                return lotWraper
            }
        }
    }
    return nil
}

func getChatRoomSystemConfigFromJson() ->CRSystemConfigurationItem?{
    let configJson = CRPreference.getChatRoomConfig()
    if !isEmptyString(str: configJson){
        if let chatRoom = JSONDeserializer<CRSystemConfigurationItem>.deserializeFrom(json: configJson){
            if chatRoom.success{
                return chatRoom
            }
        }
    }
    return nil
}

/** 获得 index:0官方、index:1信用的index */
func getOfficalAndPeilvIndex(tabs:[String]) -> (Int,Int){
    let array = tabs
    var officalIndex = 0
    var peilvIndex = 0
    
    for index in 0..<array.count{
        let value = array[index]
        if value == "彩票"{
            officalIndex = index
            peilvIndex = index
        }else{
            if value == "官方"{
                officalIndex = index
            }else if value == "信用"{
                peilvIndex = index
            }
        }
    }
    
    return (officalIndex,peilvIndex)
}

/** 会员中心: 从首页排序字符串得到格式化的数组 */
func getMemberTabsArrayWithString(tabsStr:String) -> [String] {
    let array_0 = [String](tabsStr.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: ","))
    var array_1 = [String]()
    for value in array_0
    {
        if !array_1.contains(value)
        {
            array_1.append(value)
        }
    }
    let finalArray = Array(array_1.prefix(7))
    return finalArray
}

/** 获得会员中心排序数组 */
func getArraysOfMemberpage() -> [String] {
    guard let config = getSystemConfigFromJson() else {return []}
    
    if config.content != nil{
        
        //取得排序数组 字符串
        let mainpage_module_indexsStr = config.content.membercenter_group_sorts.trimmingCharacters(in: .whitespacesAndNewlines)
        //去空格，得到去重 数组
        let mainpage_module_indexs = getMemberTabsArrayWithString(tabsStr: mainpage_module_indexsStr)
        //和所有存在数组比较，得到左后排序号的数组
        let sortedArray = handleTabTitlesWithSortArrayMembers(sortArray: mainpage_module_indexs)
        return sortedArray
    }
    
    return []
    
}

private func getFigure_tabs_member() -> [String] {
    return ["7","1","2","3","4","5","6","8","9"]
}

private func handleTabTitlesWithSortArrayMembers(sortArray:[String]) -> [String]{
    let allTabsWithoutSort = getFigure_tabs_member()
    var sortedArray = sortArray
    
    for tabItem in allTabsWithoutSort
    {
        if !sortedArray.contains(tabItem)
        {
            sortedArray.append(tabItem)
        }
    }
    
    return sortedArray
}

/** 从首页排序字符串得到格式化的数组 */
func getTabsArrayWithString(tabsStr:String) -> [String] {
    let array_0 = [String](tabsStr.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: ","))
    var array_1 = [String]()
    for value in array_0
    {
        if !array_1.contains(value)
        {
            array_1.append(value)
        }
    }
    let finalArray = Array(array_1.prefix(array_0.count))
    return finalArray
}

/** 获得首页segment排序数组 */
func figure_tabs_from_config() -> [String] {
    guard let config = getSystemConfigFromJson() else {return []}
    
    if config.content != nil{
        var datas:[String] = []
        let version = config.content.lottery_version
        //主页的二级分栏开关
        //on 是开启
        let switch_optimize_mainpage_tabs = YiboPreference.getMallImageTextTabSytle()
        var mainpage_module_indexsStr = config.content.mainpage_module_indexs.trimmingCharacters(in: .whitespacesAndNewlines)
        if isEmptyString(str: mainpage_module_indexsStr){
            //后台若不小心把排序删掉  取默认 "7,1,2,3,4,5,6,8,9"
            mainpage_module_indexsStr = "7,1,2,3,4,5,6,8,9"
        }
        let mainpage_module_indexs = getTabsArrayWithString(tabsStr: mainpage_module_indexsStr)
        
        var isExist = true
        for value in mainpage_module_indexs{
            
            if switch_optimize_mainpage_tabs == "on"{
                //主页的二级分栏
                //1.有官方或信用的任意彩种 显示官方 、
                //2.官方或信用都有 就要显示二级Tab
                var lottoyTypes:[String] = [String]()
                if (value == "1" || value == "2") && isExist{
                    if getSwitch_lottery() {
                        datas.append("彩票")
                        isExist = false
                    }
                }
                if value == "1"{
                    //官方彩票
                    if getSwitch_lottery() {
                        if version == VERSION_V1{
                            lottoyTypes.append("官方")
                        }else if version == VERSION_V1V2{
                            lottoyTypes.append("官方")
                        }
                    }
                }else if value == "2"{
                    if getSwitch_lottery() {
                        if version == VERSION_V2{
                            lottoyTypes.append("信用")
                        }else if version == VERSION_V1V2{
                            lottoyTypes.append("信用")
                        }
                    }
                }
            }else{
                if value == "1"{
                    if getSwitch_lottery() {
                        if version == VERSION_V1{
                            datas.append("官方")
                        }else if version == VERSION_V1V2{
                            datas.append("官方")
                        }
                    }
                }else if value == "2"{
                    if getSwitch_lottery() {
                        if version == VERSION_V2{
                            datas.append("信用")
                        }else if version == VERSION_V1V2{
                            datas.append("信用")
                        }
                    }
                }
            }
            if value == "3"{
                if !isEmptyString(str: config.content.onoff_zhen_ren_yu_le)&&config.content.onoff_zhen_ren_yu_le=="on"{
                    datas.append("真人")
                }
            }else if value == "4"{
                if !isEmptyString(str: config.content.onoff_dian_zi_you_yi)&&config.content.onoff_dian_zi_you_yi=="on"{
                    datas.append("电子")
                }
            }else if value == "5"{
                if !isEmptyString(str: config.content.onoff_sport_switch)&&config.content.onoff_sport_switch=="on"{
                    datas.append("体育")
                }
            }else if value == "6"{
                if config.content.nb_chess_showin_mainpage == "on" {
                    datas.append(HomeTabNameChess)
                }
            }else if value == "7"{
                //红包游戏
                if !isEmptyString(str: config.content.native_mainpage_rp_switch)&&config.content.native_mainpage_rp_switch == "on"{
                    datas.append(HomeRedPacketGame)
                }
            }else if value == "8"{
                /** 电竟 */
                if !isEmptyString(str: config.content.switch_esport) && config.content.switch_esport == "on"{
                    datas.append(HomeGaming)
                }
                
            }else if value == "9"{
                /** 捕鱼 */
                if !isEmptyString(str: config.content.switch_fishing) && config.content.switch_fishing == "on"{
                    datas.append(HomeGameFishing)
                }
                
            }
        }

        //容错和，排序数组和实际配置数组不匹配问题
        
        let sortedArray = handleTabTitlesWithSortArray(sortArray: datas)
        return sortedArray
    }
    return []
}

private func handleTabTitlesWithSortArray(sortArray:[String]) -> [String]{
    //所有的游戏种类
    let allTabsWithoutSort = getFigure_tabs()
    var sortedArray = sortArray
    
    for tabItem in allTabsWithoutSort
    {
        if !sortedArray.contains(tabItem)
        {
            sortedArray.append(tabItem)
        }
    }
    
    return sortedArray
}
 //MARK:二级分栏开关开启获取的官方和信用
func getLotterGovernmentAndCredit() -> [String]{
    
    let switch_optimize_mainpage_tabs = YiboPreference.getMallImageTextTabSytle()
    if switch_optimize_mainpage_tabs == "off"{
        return []
    }
    var array:[String] = [String]()
    guard let config = getSystemConfigFromJson() else {return[]}
    let mainpage_module_indexsStr = config.content.mainpage_module_indexs.trimmingCharacters(in: .whitespacesAndNewlines)
    let mainpage_module_indexs = getTabsArrayWithString(tabsStr: mainpage_module_indexsStr)
    
    var datas:[String] = [String]()
    for index in mainpage_module_indexs{
        datas.append(index)
    }
//     getFigure_tabs_member()
//    let sortedArray = handleTabTitlesWithSortArray(sortArray: datas)
    let version = config.content.lottery_version
    for value in datas{
        if isEmptyString(str: value){continue}
        if value == "1.1" || value == "1"{
            if getSwitch_lottery() {
                if version == VERSION_V1{
                    array.append("官方")
                }else if version == VERSION_V1V2{
                   array.append("官方")
                }
            }
            
        }else if value == "1.2" || value == "2"{
            if getSwitch_lottery() {
                if version == VERSION_V2{
                    array.append("信用")
                }else if version == VERSION_V1V2{
                    array.append("信用")
                }
            }
        }
    }
    
    if array.count == 2 {
        guard let config = getSystemConfigFromJson() else {return []}
        let switch_xfwf = config.content.switch_xfwf
       
            var countArray:[String] = [String]()
            for item in  array{
            
                //off 默认官方
                if switch_xfwf == "off"{
                    if item == "官方"{
                        countArray.insert(item, at: 0)
                    }else{
                        countArray.append(item)
                    }
                }else{
                    //默认信用
                    if item == "信用"{
                        countArray.insert(item, at: 0)
                    }else{
                        countArray.append(item)
                    }
                }
            }
           return countArray
        }
    
    return array
 }

private func getFigure_tabs() -> [String] {
    guard let config = getSystemConfigFromJson() else {return []}
    if config.content != nil{
        var datas:[String] = []
        let version = config.content.lottery_version
        //二级分栏是否开启
        let switch_optimize_mainpage_tabs = YiboPreference.getMallImageTextTabSytle()
        var isExist = true
        if getSwitch_lottery() {
            if switch_optimize_mainpage_tabs == "on"{
                if (version == VERSION_V1 || version == VERSION_V2 || version == VERSION_V1V2) && isExist{
                    datas.append("彩票")
                    isExist = false
                }
            }else{
                if version == VERSION_V1{
                    datas.append("官方")
                }else if version == VERSION_V2{
                    //                datas.append("彩票")
                    datas.append("信用")
                }else if version == VERSION_V1V2{
                    datas.append("官方")
                    datas.append("信用")
                }
            }
           
        }
        
        if !isEmptyString(str: config.content.onoff_zhen_ren_yu_le)&&config.content.onoff_zhen_ren_yu_le=="on"{
            datas.append("真人")
        }
        
        if !isEmptyString(str: config.content.onoff_dian_zi_you_yi)&&config.content.onoff_dian_zi_you_yi=="on"{
            datas.append("电子")
        }
        
        if !isEmptyString(str: config.content.onoff_sport_switch)&&config.content.onoff_sport_switch=="on"{
            datas.append("体育")
        }
        
        if config.content.nb_chess_showin_mainpage == "on" {
            datas.append(HomeTabNameChess)
        }
        if !isEmptyString(str: config.content.native_mainpage_rp_switch) && config.content.native_mainpage_rp_switch == "on"{
            //红包
            datas.append(HomeRedPacketGame)
        }
        
        if !isEmptyString(str: config.content.switch_esport) && config.content.switch_esport == "on"{
            //电竞
            datas.append(HomeGaming)
        }
        if !isEmptyString(str: config.content.switch_fishing) && config.content.switch_fishing == "on"{
            //电竞
            datas.append(HomeGameFishing)
        }
        return datas
    }
    return []
}

func getSysConfigNewFromJson() -> NewSystemWrapper? {
    let configJson = YiboPreference.getConfigNew()
    if !isEmptyString(str: configJson){
        if let wraper = JSONDeserializer<NewSystemWrapper>.deserializeFrom(json: configJson)
        {
            return wraper
        }
    }
    
    return nil
}

func openTouzhuRecord(controller:UIViewController,title:String,code:String,recordType:Int=MenuType.CAIPIAO_RECORD) -> Void {
    let loginVC = UIStoryboard(name: "touzhu_record", bundle: nil).instantiateViewController(withIdentifier: "touzhuRecord")
    let recordPage = loginVC as! TouzhuRecordController
    recordPage.titleStr = title
    recordPage.cpBianma = code
    recordPage.recordType = recordType
    recordPage.hidesBottomBarWhenPushed = true
    if let nav = controller.navigationController{
        nav.pushViewController(recordPage, animated: true)
    }
}

func openAccountMingxi(controller:UIViewController,whichPage:Int) -> Void {
    let loginVC = UIStoryboard(name: "account_mingxi_page", bundle: nil).instantiateViewController(withIdentifier: "mingxi")
    let recordPage = loginVC as! AccountMingxiController
    recordPage.pageIndex = whichPage
    if let nav = controller.navigationController{
        nav.pushViewController(recordPage, animated: true)
    }
}

func openTouzhuRecordInPresent(controller:UIViewController,title:String,lotCode:String,recordType:Int){
    let loginVC = UIStoryboard(name: "touzhu_record", bundle: nil).instantiateViewController(withIdentifier: "touzhuRecord")
    let recordPage = loginVC as! TouzhuRecordController
    recordPage.titleStr = title
    recordPage.cpBianma = lotCode
    recordPage.recordType = recordType
    
    let nav = UINavigationController.init(rootViewController: recordPage)
    controller.present(nav, animated: true, completion: nil)
}

func openConvertMoneyPage(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "fee_convert", bundle: nil).instantiateViewController(withIdentifier: "feeConvert")
    let recordPage = loginVC as! FeeConvertController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openConvertMoneyReocrdPage(controller:UIViewController) -> Void {
    
}

func openBigPanPage(controller:UIViewController) -> Void {
//    let loginVC = UIStoryboard(name: "bigpan", bundle: nil).instantiateViewController(withIdentifier: "big_pan")
//    let recordPage = loginVC as! BigPanController
//    controller.navigationController?.pushViewController(recordPage, animated: true)
    
    let loginVC = UIStoryboard(name: "NewBigPanController", bundle: nil).instantiateViewController(withIdentifier: "newBigPanController")
    let recordPage = loginVC as! NewBigPanController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openQRCodePage(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "qrcode_page", bundle: nil).instantiateViewController(withIdentifier: "qrcodeController")
    let recordPage = loginVC as! QrcodeViewController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openNoticePage(controller:BaseController,notices:[NoticeBean]) -> Void {
    let loginVC = UIStoryboard(name: "notice_page", bundle: nil).instantiateViewController(withIdentifier: "notice_page")
    let recordPage = loginVC as! NoticesPageController
    recordPage.notices = notices
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openPlayIntroduceController(controller:UIViewController,payRule:String,
                                 touzhu:String,winDemo:String) -> Void {
    let loginVC = UIStoryboard(name: "play_rule_introduction", bundle: nil).instantiateViewController(withIdentifier: "playRule")
    let recordPage = loginVC as! PlayRuleController
    recordPage.playRule = payRule
    recordPage.touzhuTxt = touzhu
    recordPage.winDemoTxt = winDemo
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

//根据后台用户选择的模块风格选择风格
func selectMainStyleByModuleID(styleID:String) -> (xibname:String,identifier:String){
    var id = styleID
    if isEmptyString(str: styleID){
        id = "1"
    }
    return (String.init(format: "custom%@", id),String.init(format: "main_controller_%@", id))
}

//func setupNavigationBar() {
//    
//}

func goBetPage(controller:UIViewController,lotData:LotteryData?) -> Void{
    
    let lotMenuController = UIStoryboard(name: "lot_menu_page",bundle:nil).instantiateViewController(withIdentifier: "lot_menu") as! LotMenuController
    
    
    let touzhuPage = getTouzhuPage(lotData: lotData)
    
    let slideContainer = SlideContainerController(centerViewController:touzhuPage , leftViewController: lotMenuController, rightViewController: nil)
    
    
    slideContainer.scaleEnable = false
    touzhuPage.menuDelegate = slideContainer//将实现了菜单按钮事件delegate的侧滑容器传入主tab界面
    controller.navigationController?.pushViewController(slideContainer, animated: true)
    
}

/** 获得投注控制器 */
private func getTouzhuPage(lotData:LotteryData?) -> BetPageController {
    let chatVC = ChatViewController()
    let isNativeChat = switch_nativeOrWapChat()
    
    chatVC.chatViewType = .ChatFromBetPage
    
    if YiboPreference.getCurrentThmeByName() ==  "FrostedPlain"
    {        
        let touzhuController = UIStoryboard(name: "touzh_page",bundle:nil).instantiateViewController(withIdentifier: "touzhu_v3")
        let touzhuPage = touzhuController as! TouzhPlainController
        touzhuPage.lotData = lotData
        touzhuPage.cpVersion = "\(String(describing: lotData?.lotVersion))"
        
        if isNativeChat {
            let pageController = BetPageController.init(subControllers: [touzhuPage], latestIndex: 0, scrollEnabled: false)
            return pageController
        }else {
            let pageController = BetPageController.init(subControllers:switch_chatRoomOn() ? [touzhuPage,chatVC] : [touzhuPage], latestIndex: 0, scrollEnabled: false)
            
            return pageController
        }
        
    }else
    {
        let touzhuController = UIStoryboard(name: "touzh_page",bundle:nil).instantiateViewController(withIdentifier: "touzhu_v2")
        let touzhuPage = touzhuController as! TouzhNewController
        touzhuPage.lotData = lotData
        touzhuPage.groupCode = lotData?.groupCode ?? ""
        
        if isNativeChat {
            let pageController = BetPageController.init(subControllers:[touzhuPage], latestIndex: 0, scrollEnabled: false)
            
            return pageController
        }else {
            let pageController = BetPageController.init(subControllers:switch_chatRoomOn() ? [touzhuPage,chatVC] : [touzhuPage], latestIndex: 0, scrollEnabled: false)
            
            return pageController
        }
    }
}

// 是否特殊的玩法球背景，长方形背景，如快三的某些玩法
func isSpeicalOfficalBall(playCode:String,lotType:String) -> Bool{
    if playCode == "qwxdds" && lotType == "5"{
        return true
    }
    return false
}


func goMainScreen(controller:UIViewController) -> Void {
    let moduleStyle = YiboPreference.getMallStyle()
    print("module style === ",moduleStyle)
    let (xibname,identifier) = selectMainStyleByModuleID(styleID: moduleStyle)
    
    let mainViewController = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: identifier) as? RootTabBarViewController
    guard let mainController = mainViewController else{ return }
    
    let menuController = UIStoryboard(name: "menu_page",bundle:nil).instantiateViewController(withIdentifier: "menu_page") as! MenuController
    menuController.useForSlideMenu = true
    let navP = MainNavController.init(rootViewController: menuController)
    
    let slideContainer = SlideContainerController(centerViewController: mainController, leftViewController: nil, rightViewController: navP)
    slideContainer.sideMovable = true
    
    mainController.menuDelegate = slideContainer//将实现了菜单按钮事件delegate的侧滑容器传入主tab界面
//    controller.present(slideContainer, animated: true, completion: nil)
    UIApplication.shared.keyWindow?.rootViewController = slideContainer
    
}


//普通用户时，是否要过滤代理用户专属的指定项
func needDailiFilterItem(txtName:String) -> Bool{
//    let dailiFilter:[String] = ["团队报表","团队总览","用户列表","注册管理","代理管理","推广管理"]
    let dailiFilter:[String] = ["团队报表","团队总览","代理管理","推广链接"]
    if YiboPreference.getAccountMode() == MEMBER_TYPE || YiboPreference.getAccountMode() == GUEST_TYPE{
        if dailiFilter.contains(txtName){
            return true
        }
    }
    return false
}

//开关控制
func needFilterSystemItem(txtName:String) -> Bool{
    if let sys = getSystemConfigFromJson() {
        if sys.content != nil{
            
            if txtName == "额度转换" {
                let switch_money_change = sys.content.switch_money_change
                if !isEmptyString(str: switch_money_change) && switch_money_change == "on"{
                    return false
                }
            }
        }
    }
    return true
}


func hideTailChar(str:String,showCount:Int) -> String{
    if isEmptyString(str: str){
        return ""
    }
    if showCount >= str.count{
        return str
    }
    var aaa = ""
    for _ in 0...(str.count - showCount - 1){
        aaa = aaa + "*"
    }
    let end = str.index(str.startIndex, offsetBy: (showCount))
    let result = str.substring(to: end)
    return String.init(format: "%@%@", result,aaa)
}

/**
 字典转换为JSONString
 
 - parameter dictionary: 字典参数
 
 - returns: JSONString
 */
func getJSONStringFromDictionary(dictionary:NSDictionary) -> String {
    if (!JSONSerialization.isValidJSONObject(dictionary)) {
        print("无法解析出JSONString")
        return ""
    }
    let data : NSData! = try! JSONSerialization.data(withJSONObject: dictionary, options: []) as NSData?
    let JSONString = NSString(data:data as Data,encoding: String.Encoding.utf8.rawValue)
    return JSONString! as String
    
}

func getFieldWithJsonString(jsonString: String) -> String? {
    
    if let jsonData = jsonString.data(using: .utf8)
    {
        if let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject] {
            let str = json["content"] as? String
            if let strTemp = str
            {
                return strTemp
            }
        }
    }
    
    return ""
    
}

func getUserType(t:Int) -> String{
    if t == AGENT_TYPE{
        return "代理"
    }else if t == MEMBER_TYPE{
        return "会员"
    }else if t == TOP_AGENT_TYPE{
        return "总代理"
    }else if t == GUEST_TYPE{
        return "试玩账号"
    }
    return ""
}

//根据主单列表计算注单
func fromBetOrder(official_orders:[OfficialOrder],subPlayName:String,subPlayCode:String,selectedBeishu:Int,
                  cpTypeCode:String,cpBianHao:String,current_rate:Float,selectMode:String) -> [OrderDataInfo] {
    var orders:[OrderDataInfo] = []
    
//    let mode = official_orders.count > 0 ? official_orders[0].m : 1
    
    for order in official_orders{
        let orderInfo = OrderDataInfo()
        orderInfo.user = YiboPreference.getUserName()
        orderInfo.subPlayName = subPlayName
        orderInfo.subPlayCode = subPlayCode
        orderInfo.beishu = selectedBeishu
        orderInfo.zhushu = order.n
        orderInfo.numbers = order.c
        orderInfo.cpCode = cpTypeCode
        orderInfo.lotcode = cpBianHao
        orderInfo.lottype = cpTypeCode
        orderInfo.rate = Double(current_rate)
        orderInfo.oddsCode = order.i
        orderInfo.oddsName = order.oddName
        orderInfo.mode = convertPostMode(mode: selectMode)
        let total_money = (Float(order.n) * Float(selectedBeishu) * Float(2)) / Float(orderInfo.mode)
        print("total money when enter order page = ",total_money)
        orderInfo.money = Double(total_money)
//        orderInfo.money = Double(order.a)
        orders.append(orderInfo)
    }
    return orders
}

func fromPeilvBetOrder(peilv_orders:[PeilvOrder],subPlayName:String,subPlayCode:String,
                       cpTypeCode:String,cpBianHao:String,current_rate:Float) -> [OrderDataInfo] {
    var orders:[OrderDataInfo] = []
    for order in peilv_orders{
        let orderInfo = OrderDataInfo()
        orderInfo.user = YiboPreference.getUserName()
        orderInfo.subPlayName = subPlayName
        orderInfo.subPlayCode = subPlayCode
        orderInfo.money = Double(order.a)
        orderInfo.numbers = order.c
        orderInfo.cpCode = cpTypeCode
        orderInfo.lotcode = cpBianHao
        orderInfo.lottype = cpTypeCode
        orderInfo.rate = Double(current_rate)
        orderInfo.oddsCode = order.i
        orderInfo.cpCode = order.d
        orderInfo.oddsName = order.oddName
        orders.append(orderInfo)
    }
    return orders
}

/**
 * random generate lottery ballon numbers
 * @param allowRepeat 是否允许重复号码
 * @param numCount 选择几个号码
 * @param format 号码字符串分隔格式
 * @param numStr 被随机选择的字符串
 * @return
 */
func randomNumbers(numStr:String,allowRepeat:Bool,numCount:Int,format:String) -> [String]{
    var results = numStr.components(separatedBy: format)
    if results.isEmpty{
        return []
    }
    let maxNum = results.count
    var i = 0
    var count = 0
    var numbers = [String]()
    while count < numCount {
        i = Int(arc4random() % UInt32(maxNum))
        if !allowRepeat{
            if numbers.contains(results[i]){
                continue
            }
        }
        numbers.append(results[i])
        count += 1
    }
    results.removeAll()
    return numbers

}

//MARK: 地址可能是相对地址的处理
func handleImageURL(urlString: String) -> String{
    
    if isEmptyString(str: urlString) {
        return ""
    }
    
    var logoImg = urlString
    if logoImg.contains("\t"){
        let strs = logoImg.components(separatedBy: "\t")
        if strs.count >= 2{
            logoImg = strs[1]
        }
    }
    logoImg = logoImg.trimmingCharacters(in: .whitespaces)
    if !logoImg.hasPrefix("https://") && !logoImg.hasPrefix("http://"){
        logoImg = String.init(format: "%@/%@", BASE_URL,logoImg)
    }
    
    return logoImg
}

//更新头像
func updateAppLogo(icon:UIImageView) -> Void {
    guard let sys = getSystemConfigFromJson() else{return}
    let logoImg = sys.content.member_page_logo_url
    if !isEmptyString(str: logoImg){
        let urlString = handleImageURL(urlString: logoImg)
        let themeName = YiboPreference.getCurrentThmeByName()
        var bgImageName = ""
        if themeName == "Red" {
            bgImageName = "placeHeader_red"
        }else if themeName == "Blue" || themeName == "FrostedPlain"{
            bgImageName = "placeHeader_blue"
        }else if themeName == "Green" {
            bgImageName = "placeHeader_green"
        }else if themeName == "FrostedOrange" {
            bgImageName = "placeHeader_glassOrange"
        }else {
            // 可下载主题时，需要分类配置数据数组，现在可以这样处理
            bgImageName = "placeHeader_glassOrange"
        }
        if let url = URL.init(string: urlString) {
            if YiboPreference.getCACHEAVATARdata().count >= 1 {
                icon.image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
            }else{
                icon.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: bgImageName), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }else {
            icon.theme_image = "General.personalHeaderBg"
        }
    }else{
        icon.theme_image = "General.personalHeaderBg"
    }
}

//更新类 会员中心，头下的
func updateHeaderBgLogo(imageView:UIImageView) -> Void {
    guard let sys = getSystemConfigFromJson() else{return}
    let logoImg = sys.content.member_page_bg_url
    if !isEmptyString(str: logoImg){
        
        let urlString = handleImageURL(urlString: logoImg)
        
        let themeName = YiboPreference.getCurrentThmeByName()
        var bgImageName = ""
        if themeName == "Red" {
            bgImageName = "personalHeaderBg_red"
        }else if themeName == "Blue" || themeName == "FrostedPlain"{
            bgImageName = "personalHeaderBg_blue"
        }else if themeName == "Green" {
            bgImageName = "personalHeaderBg_green"
        }else if themeName == "FrostedOrange" {
            bgImageName = "personalHeaderBg_glassOrange"
        }else {
            bgImageName = "personalHeaderBg_glassOrange"
        }
        
        if let url = URL.init(string: urlString) {
            imageView.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: bgImageName), options: nil, progressBlock: nil, completionHandler: nil)
        }else {
            imageView.theme_image = "General.personalHeaderBg"
        }
        
    }else{
        imageView.theme_image = "General.personalHeaderBg"
    }
}

//MARK: 日期转字符串
/** 日期转字符串 */
func getTimeWithDate(date: Date, dateFormat:String = "yyyy-MM-dd HH:mm:ss") -> String {
//    let timeZone = TimeZone.init(identifier: "UTC")
    let timeZone = TimeZone.init(identifier: "Asia/Shanghai")
    let formatter = DateFormatter()
    formatter.timeZone = timeZone
    formatter.locale = Locale.init(identifier: "zh_CN")
    formatter.dateFormat = dateFormat
    let date = formatter.string(from: date)
    return date
//    return date.components(separatedBy: " ").first!
}
 
 

//MARK: 字符串转日期
/** 字符串转日期 */
func stringConvertToDate(string:String, dateFormat:String="yyyy-MM-dd HH:mm:ss") -> Date {
    let dateFormatter = DateFormatter.init()
    dateFormatter.dateFormat = dateFormat
    let date = dateFormatter.date(from: string)
    return date!
}

func getTodayZeroTime() -> String{
    let date = Date()
    let calendar = NSCalendar.init(identifier: .chinese)
    calendar?.date(bySettingHour: 0, minute: 0, second: 0, of: date, options: .wrapComponents)
    let components = calendar?.components([.year,.month,.day], from: date)
    let dformatter = DateFormatter()
    dformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dformatter.string(from: (calendar?.date(from: components!))!)
}

/** beforeDays:beforeDays天之前的时间 */
func getTomorrowNowTime(beforeDays:Int = 0) -> String{
    
    let date = Date()
    let calendar = NSCalendar.init(identifier: .chinese)
    var tomorrow = Date.init(timeInterval: 24*60*60, since: date)
    if beforeDays != 0 {
        tomorrow = Date.init(timeInterval: TimeInterval(-24*60*60*beforeDays), since: date)
    }
    let dformatter = DateFormatter()
    dformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let components2 = calendar?.components([.year,.month,.day,.hour,.minute,.second], from: tomorrow)
    return dformatter.string(from: (calendar?.date(from: components2!))!)
}

func getNowTime() -> String{
    
    let date = Date()
    let calendar = NSCalendar.init(identifier: .chinese)
    let tomorrow = Date.init(timeInterval: 0, since: date)
    let dformatter = DateFormatter()
    dformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let components2 = calendar?.components([.year,.month,.day,.hour,.minute,.second], from: tomorrow)
    return dformatter.string(from: (calendar?.date(from: components2!))!)
}


func getLotVersionConfig() ->String{
    guard let config:SystemWrapper = getSystemConfigFromJson() else {return VERSION_V1}
    if let sys = config.content{
        return sys.lottery_version
    }
    return VERSION_V1
}

func trimQihao(currentQihao:String,length:Int = 6) -> String{
    if isEmptyString(str: currentQihao){
        return currentQihao
    }
    if currentQihao.count > length{
        let trim = (currentQihao as NSString).substring(from: currentQihao.count - length)
        return trim
    }
    return currentQihao
}

func hideChar(str:String,showBackCharCount:Int) -> String{
    if isEmptyString(str: str){
        return ""
    }
    if str.count < showBackCharCount + 1{
        return str
    }
    var aaa = ""
    for _ in 0...(str.count - showBackCharCount - 1){
        aaa = aaa + "*"
    }
    let start = str.index(str.startIndex, offsetBy: (str.count - showBackCharCount))
    let result = str.substring(from: start)
    return String.init(format: "%@%@", aaa,result)
}

func numberOfChars(_ str: String) -> Int {
    var number = 0
    guard str.count > 0 else {return 0}
    for i in 0...str.count - 1 {
        let c: unichar = (str as NSString).character(at: i)
        
        if (c >= 0x4E00) {
            number += 2
        }else {
            number += 1
        }
    }
    return number
}

//只能为中文
func onlyInputChineseCharacters(_ string: String) -> Bool {
    let inputString = "[\u{4e00}-\u{9fa5}]+"
    let predicate = NSPredicate(format: "SELF MATCHES %@", inputString)
    let Chinese = predicate.evaluate(with: string)
    return Chinese
}

//只能为数字
func onlyInputTheNumber(_ string: String) -> Bool {
    let numString = "[0-9]*"
    let predicate = NSPredicate(format: "SELF MATCHES %@", numString)
    let number = predicate.evaluate(with: string)
    return number
}

func createFAB(controller:UIViewController) -> ActionButton {
    
    var items:[(title:String,imgname:String,action: (ActionButtonItem) -> Void)] = []
    let redpack = (title:"抢红包",imgname:"icon_real",action: {(item:ActionButtonItem)->Void in
        openRainController(controller: controller)
    })
    
    var bigpan:(title:String,imgname:String,action: (ActionButtonItem) -> Void)?
    if let sys = getSystemConfigFromJson()  {
        if !isEmptyString(str: sys.content.onoff_turnlate) && sys.content.onoff_turnlate == "on"{
            bigpan = (title:"大转盘",imgname:"icon_game",action:{(item:ActionButtonItem)->Void in
                openBigPanPage(controller: controller)
            })
        }
    }
    let scorechange = (title:"积分兑换",imgname:"app_service_icon",action:{(item:ActionButtonItem)->Void in
        openScoreExchangePage(controller: controller)
    })
    let active = (title:"优惠活动",imgname:"app_download_icon",action:{(item:ActionButtonItem)->Void in
        openActiveController(controller: controller)
        
//        controller.isAttachInTabBar = false
        //        let vc = UIStoryboard(name: "active_page",bundle:nil).instantiateViewController(withIdentifier: "activePage")
        //        let nav = MainNavController.init(rootViewController: vc)
        //        controller.present(nav, animated: true, completion: nil)
    })
    let message = (title:"站内信",imgname:"app_record_icon",action:{(item:ActionButtonItem)->Void in
        //        openMessageCenter(controller: controller)
        let nav = UINavigationController.init(rootViewController: InsideMessageController())
        controller.present(nav, animated: true, completion: nil)
    })
    let backcomputer = (title:"返回电脑版",imgname:"app_money_icon",action:{(item:ActionButtonItem)->Void in
        openBrower(urlString: BASE_URL)
    })
    
    items.append(backcomputer)
    items.append(message)
    items.append(active)
    items.append(scorechange)
    if bigpan != nil{
        items.append(bigpan!)
    }
    items.append(redpack)
    
    return showFAB(attachView: controller.view, items: items)
}

func openActiveController(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "active_page", bundle: nil).instantiateViewController(withIdentifier: "activePage")
    let recordPage = loginVC as! ActiveController
    recordPage.isAttachInTabBar = false
    recordPage.showDragBtnHandlerIS=false
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openAPPDownloadController(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "app_download_page", bundle: nil).instantiateViewController(withIdentifier: "appDownload")
    let recordPage = loginVC as! AppDownloadViewController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openLotteryResultsController(controller:UIViewController) -> Void {
    let moduleStyle = YiboPreference.getMallStyle()
    let (xibname,_) = selectMainStyleByModuleID(styleID: moduleStyle)
    let vc2 = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: "notice") as! LotteryResultsController
    vc2.code = DefaultLotteryNumber()
//    vc2.code = "CQSSC"
    vc2.title = "开奖结果"
    controller.navigationController?.pushViewController(vc2, animated: true)
}

func openOpenResultController(controller:UIViewController,cpName:String,
                              cpBianMa:String,cpTypeCode:String) -> Void {
    let loginVC = UIStoryboard(name: "lottery_open_result", bundle: nil).instantiateViewController(withIdentifier: "openResultID")
    let recordPage = loginVC as! LotteryOpenResultController
    recordPage.cpName = cpName
    recordPage.cpBianMa = cpBianMa
    recordPage.cpTypeCode = cpTypeCode
    controller.navigationController?.pushViewController(recordPage, animated: true)
}


func openModifyPwd(controller:UIViewController,loginPwd:Bool) -> Void {
    let loginVC = UIStoryboard(name: "pwd_modify", bundle: nil).instantiateViewController(withIdentifier: "modifyPwd")
    let recordPage = loginVC as! ModifyPwdController
    recordPage.isLoginPwdModify = loginPwd
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openChargeMoney(controller:UIViewController,meminfo:Meminfo?) -> Void {
//    let loginVC = UIStoryboard(name: "new_charge_money_page", bundle: nil).instantiateViewController(withIdentifier: "new_charge")
//    let recordPage = loginVC as! NewChargeMoneyController
//    recordPage.meminfo = meminfo
//    controller.navigationController?.pushViewController(recordPage, animated: true)
    chooseGoChargeController(meminfo: meminfo, controller: controller,isPush: true)
}

func openPickMoney(controller:UIViewController,meminfo:Meminfo?) -> Void {
    let loginVC = UIStoryboard(name: "withdraw_page", bundle: nil).instantiateViewController(withIdentifier: "withDraw")
    let recordPage = loginVC as! WithdrawController
    recordPage.meminfo = meminfo
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openBalance(controller:UIViewController,meminfo:Meminfo?) -> Void {
    let root = fundViewController()
    controller.present(root, animated: true, completion: nil)
//    recordPage.meminfo = meminfo
}

func hitPlayRuleUrl(code:String,isCredit:Bool) -> String{
    var url = ""
    if isCredit{
        url = String.init(format: "%@%@/%@?code=%@", BASE_URL,"credit","viewRule",code)
    }else{
        url = String.init(format: "%@%@/%@?code=%@", BASE_URL,"official","viewRule",code)
    }
    return url
}

func openActiveDetail(controller:UIViewController,title:String,content:String,
                      foreighUrl:String="",outsideOpen:Bool=false, isChat: Bool = false) -> Void {
    let loginVC = UIStoryboard(name: "active_detail_page", bundle: nil).instantiateViewController(withIdentifier: "activeDetail")
    let recordPage = loginVC as! ActiveDetailController
    recordPage.titleStr = title
    recordPage.htmlContent = content
    recordPage.foreignUrl = foreighUrl
    recordPage.outsideOpen = outsideOpen
    recordPage.isChat = isChat
    if controller.navigationController == nil{
        let nav = UINavigationController.init(rootViewController: recordPage)
        controller.present(nav, animated: true, completion: nil)
    }else{
        controller.navigationController?.pushViewController(recordPage, animated: true)
    }
//    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openUserCenter(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "user_center", bundle: nil).instantiateViewController(withIdentifier: "userCenter")
    let recordPage = loginVC as! UserCenterController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openBankPwdSetting(controller:UIViewController,delegate:BankDelegate){
    let loginVC = UIStoryboard(name: "set_bank_pwd", bundle: nil).instantiateViewController(withIdentifier: "setBankPwd")
    let recordPage = loginVC as! SetBankPwdController
    recordPage.delegate = delegate
    if controller.navigationController == nil{
        let nav = UINavigationController.init(rootViewController: recordPage)
        controller.present(nav, animated: true, completion: nil)
    }else{
        controller.navigationController?.pushViewController(recordPage, animated: true)
    }
}

func openBankSetting(controller:UIViewController,delegate:BankDelegate,json:String){
    let loginVC = UIStoryboard(name: "bank_setting", bundle: nil).instantiateViewController(withIdentifier: "bankSetting")
    let recordPage = loginVC as! BankSettingController
    recordPage.delegate = delegate
    recordPage.dataJson = json
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openConfirmPay(controller:UIViewController,orderNo:String,accountName:String,chargeMoney:String,payMethodName:String, receiveName:String,receiveAccount:String,dipositor:String,dipositorAccount:String,qrcodeUrl:String,payType:Int,payJson:String,remark:String="",fycode:String=""){
    
    let loginVC = UIStoryboard(name: "confirm_pay", bundle: nil).instantiateViewController(withIdentifier: "confirmPay")
    let recordPage = loginVC as! ConfirmPayController
    recordPage.orderno = orderNo
    recordPage.account = accountName
    recordPage.money = chargeMoney
    recordPage.payMethodName = payMethodName
    recordPage.receiveName = receiveName
    recordPage.reeiveAccount = receiveAccount
    recordPage.dipositor = dipositor
    recordPage.dipositorAccount = dipositorAccount
    recordPage.qrcodeUrl = qrcodeUrl
    recordPage.payType = payType
    recordPage.payJson = payJson
    recordPage.remark = remark
    recordPage.fycode = fycode
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openActive(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "active_page", bundle: nil).instantiateViewController(withIdentifier: "activePage")
    let recordPage = loginVC as! ActiveController
    recordPage.isAttachInTabBar = false
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openScoreExchangePage(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "score_change_page", bundle: nil).instantiateViewController(withIdentifier: "scoreExchange")
    let recordPage = loginVC as! ScoreExchangeController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

 func openBraveZuiHaoPage(controller:UIViewController,order:[OrderDataInfo],lotCode:String,lotName:String="",cpVersion:String,cptype:String="",icon:String="",officalBonus:Double?) -> Void {
    let loginVC = UIStoryboard(name: "brave_zuihao_page", bundle: nil).instantiateViewController(withIdentifier: "braveZuihao")
    let page = loginVC as! BraveZuihaoController
    page.orderInfo = order
    page.cpBianma = lotCode
    page.cpLotName = lotName
    page.cpVersion = cpVersion
    page.cptype = cptype
    page.lotteryicon = icon
    page.officalBonus = officalBonus
    controller.navigationController?.pushViewController(page, animated: true)
}

func openScoreChange(controller:UIViewController) -> Void {
    //    let loginVC = UIStoryboard(name: "lottery_open_result", bundle: nil).instantiateViewController(withIdentifier: "openResultID")
    //    let recordPage = loginVC as! LotteryOpenResultController
    //    recordPage.cpName = cpName
    //    recordPage.cpBianMa = cpBianMa
    //    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func openAboutus(controller:UIViewController) -> Void {
    let loginVC = UIStoryboard(name: "aboutus", bundle: nil).instantiateViewController(withIdentifier: "aboutus")
    let recordPage = loginVC as! AboutUsController
    controller.navigationController?.pushViewController(recordPage, animated: true)
}
/**
 
 *  获取当前Year
 
 */

func getYear() ->Int {
    let calendar = Calendar.current
    //这里注意 swift要用[,]这样方式写
    let year = calendar.component(.year, from: Date())
    print("current year = \(year)")
    return year
}

func isMatchRegex(text:String,regex:String) -> Bool{
    let words = NSPredicate.init(format: "SELF MATCHES %@", regex)
    let isMatch = words.evaluate(with: text)
    return isMatch
}

func stringToTimeStamp(stringTime:String)->String {
    
    let dfmatter = DateFormatter()
    dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
    let date = dfmatter.date(from: stringTime)
    let dateStamp:TimeInterval = date!.timeIntervalSince1970
    let dateSt:Int = Int(dateStamp)
    return String(dateSt)
    
}


//将指定格式时间字符串转为用户指定的时间格式
func timeStringToFormatString(timeStamp:String,format:String)->String {
    let str = stringToTimeStamp(stringTime: timeStamp)
    var timeLong = Int64(str)!
    timeLong = timeLong/1000
    let timeStr:TimeInterval = TimeInterval(timeLong)
    let date = Date(timeIntervalSince1970: timeStr)
    let dfmatter = DateFormatter()
    dfmatter.dateFormat=format
    return dfmatter.string(from: date)
}

//timeStamp 单位精确到秒
func timeStampToString(timeStamp:Int64)->String {
    var timeLong = timeStamp
    timeLong = timeLong/1000
    let timeStr:TimeInterval = TimeInterval(timeLong)
    let date = Date(timeIntervalSince1970: timeStr)
    let dfmatter = DateFormatter()
    dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
    let str = dfmatter.string(from: date)
    return str
}

func timeStampToString(timeStamp:Int64,format:String)->String {
    var timeLong = timeStamp
    timeLong = timeLong/1000
    let timeStr:TimeInterval = TimeInterval(timeLong)
    let date = Date(timeIntervalSince1970: timeStr)
    let dfmatter = DateFormatter()
    dfmatter.dateFormat=format
    let str = dfmatter.string(from: date)
    return str
}


/// 秒转换成000000格式, 注意没有分隔符
///
/// - Parameter secounds: 秒
/// - Returns:
func getFormatTime(secounds:TimeInterval)->String{
    if secounds.isNaN{
        return "00:00:00"
    }
    var Min = Int(secounds / 60)
    let Sec = Int(secounds.truncatingRemainder(dividingBy: 60))
    var Hour = 0
    if Min>=60 {
        Hour = Int(Min / 60)
        Min = Min - Hour*60
        return String(format: "%02d:%02d:%02d", Hour, Min, Sec)
    }
    return String(format: "00:%02d:%02d", Min, Sec)
}

func getFormatTimeWithSpace(secounds:TimeInterval)->String{
    if secounds.isNaN{
        return "00:00:00"
    }
    var Min = Int(secounds / 60)
    let Sec = Int(secounds.truncatingRemainder(dividingBy: 60))
    var Hour = 0
    if Min>=60 {
        Hour = Int(Min / 60)
        Min = Min - Hour*60
        return String(format: "%02d:%02d:%02d", Hour, Min, Sec)
    }
    return String(format: "00:%02d:%02d", Min, Sec)
}

func getVerionName() -> String{
    let infoDictionary = Bundle.main.infoDictionary
    let majorVersion : AnyObject? = infoDictionary! ["CFBundleShortVersionString"] as AnyObject
    let appversion = majorVersion as! String
    return appversion
}

func getDomainUrl() -> String{
    
    
    #if DEBUG
        let debugEditURLMode = YiboPreference.getShowRecentDomainURLs()
        if debugEditURLMode == "on"
        {
            if let url = YiboPreference.getRecentDomainURLs().first
            {
                return url
            }
        }

        let infoDictionary = Bundle.main.infoDictionary
        let domainUrlValue : AnyObject? = infoDictionary! ["domain_url"] as AnyObject
        let domainUrl = domainUrlValue as! String
        return domainUrl
    
//    return "http://192.168.9.164:8022/game-play"
    
    #else
        let infoDictionary = Bundle.main.infoDictionary
        let domainUrlValue : AnyObject? = infoDictionary! ["domain_url"] as AnyObject
        let domainUrl = domainUrlValue as! String
        return domainUrl
    #endif
    
}

func isInternalTest() -> Bool{
    let infoDictionary = Bundle.main.infoDictionary
    let internalTest : AnyObject? = infoDictionary! ["internal_test"] as AnyObject
    let result = internalTest as! Bool
    return result
}

func getAppName()->String{
    let infoDictionary = Bundle.main.infoDictionary
    let bundleName: AnyObject? = infoDictionary!["CFBundleName"] as AnyObject as AnyObject
    if let bn = bundleName{
        return bn as! String
    }
    return ""
}

func getAPPID()->String{
    let bid = Bundle.main.bundleIdentifier!
    return bid
}

func checkVersion(controller:BaseController, showDialog:Bool,showText:String,showError:Bool=false,isUpdate:Bool,isSuperSign:Bool){
    
    let updateUrl = getDownloadPageURL()
    
    controller.request(frontDialog: showDialog, method: .post, loadTextStr: showText, url:updateUrl,params:["curVersion":getVerionName(),"appID":getAPPID(),"platform":"ios","superSign":isSuperSign ? "1":"0"],
                       callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if showError{
                                if resultJson.isEmpty {
                                    showToast(view: controller.view, txt: convertString(string: "检测升级失败"))
                                }else{
                                    showToast(view: controller.view, txt: resultJson)
                                }
                            }
                            return
                        }
                        if let result = CheckUpdateWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                if let content = result.content{
                                    if isEmptyString(str: content.url){
                                        return
                                    }
                                    showUpdateDialog(controller: controller, version: content.version, content: content.content, url: content.url)
                                }
                            }else{
                                if showError{
                                    if !isEmptyString(str: result.msg){
                                        showToast(view: controller.view, txt: result.msg)
                                    }else{
                                        showToast(view: controller.view, txt: convertString(string: "检测升级失败"))
                                    }
                                }
                            }
                        }
    })
}


//免费试玩注册
func freeRegister(controller:BaseController,showDialog:Bool,showText:String,delegate:LoginAndRegisterDelegate?){
    controller.request(frontDialog: showDialog, method: .post, loadTextStr: showText, url:REG_GUEST_URL,
                       callback: { (resultJson:String,resultStatus:Bool)->Void in
                        
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: controller.view, txt: convertString(string: "注册失败"))
                            }else{
                                showToast(view: controller.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = RegisterResultWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                YiboPreference.saveLoginStatus(value: true as AnyObject)
                                
                                isSixMarkRequest(controller:controller)
                                //获取注册帐户相关信息
                                if let infos = result.content{
                                    YiboPreference.setAccountMode(value: infos.accountType as AnyObject)
                                    //自动登录的情况下，要记住帐号密码
//                                    if infos.accountType != GUEST_TYPE{
                                        if YiboPreference.getAutoLoginStatus(){
                                            YiboPreference.saveUserName(value: infos.account as AnyObject)
                                        }
//                                    }
                                }
                                controller.onBackClick()
                                //                                controller.navigationController?.popViewController(animated: true)
                                if let callback = delegate{
                                    callback.fromRegToLogin()
                                }
                            }else{
                                if let errorMsg = result.msg{
                                    showToast(view: controller.view, txt: errorMsg)
                                }else{
                                    showToast(view: controller.view, txt: convertString(string: "注册失败"))
                                }
                            }
                        }
    })
    
    //是否是六合彩接口判断,返回数组
    func isSixMarkRequest(controller:BaseController) {
        controller.request(frontDialog: false, method: .get, url: IS_SIXMARK, callback: { (resultJson:String,resultStatus:Bool) -> Void in
            if !resultStatus {
                return
            }
            
            class SixMarkModel:HandyJSON {
                required init() {}
                var success = false
                var accessToken = ""
                var content = [String]()
            }
            
            if let result = SixMarkModel.deserialize(from: resultJson){
                if result.success{
                    sixMarkArray = result.content
                    if !isEmptyString(str: result.accessToken){
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                    }
                    
                }else {}
            }
        })
    }
}

func openBrower(urlString:String)->Void{

    let url = URL.init(string: urlString)
    
    if let urlValue = url{
        //根据iOS系统版本，分别处理
        if #available(iOS 10, *) {
            UIApplication.shared.open(urlValue, options: [:],
                                      completionHandler: {
                                        (success) in
                                        print("open success")
            })
        } else {
            UIApplication.shared.openURL(urlValue)
        }
    }
}

 func shouldShowBrowsersChooseView() -> Bool {
    if let config = getSystemConfigFromJson(){
        if config.content != nil{
            let addCardSwitch = config.content.multi_broswer_switch
            return addCardSwitch == "off" ? false : true
        }
    }

    return false
}


func openInBrowser(url:String,browerType: Int,view: UIView){
    
    if browerType == BROWER_TYPE_UC {
        if UIApplication.shared.canOpenURL(URL.init(string: "ucbrowser://")!){
            let openUrl = String.init(format: "%@%@", "ucbrowser://",url)
            openBrower(urlString: openUrl)
        }else {
            showToast(view: view, txt: "未安装“UC”浏览器")
        }
    }else if browerType == BROWER_TYPE_QQ {
        if UIApplication.shared.canOpenURL(URL.init(string: "mttbrowser://")!){
            let openUrl = String.init(format: "%@url=%@", "mttbrowser://",url)
            openBrower(urlString: openUrl)
        }else {
            showToast(view: view, txt: "未安装“QQ”浏览器")
        }
    }else if browerType == BROWER_TYPE_GOOGLE {
        
        if UIApplication.shared.canOpenURL(URL.init(string: "googlechromes://")!){
            if url.starts(with: "https") {
                let urlString = url.replacingOccurrences(of: "https", with: "googlechromes")
                openBrower(urlString: urlString)
            }else if url.starts(with: "http"){
                let urlFinalString = url.replacingOccurrences(of: "http", with: "googlechrome")
                openBrower(urlString: urlFinalString)
            }else {
                showToast(view: view, txt: "未识别的链接")
            }
        }else {
            showToast(view: view, txt: "未安装“谷歌”浏览器")
        }
    }else if browerType == BROWER_TYPE_SAFARI {
        openBrower(urlString: url)
    }else if browerType == BROWER_TYPE_FIREFOX {
        if UIApplication.shared.canOpenURL(URL.init(string: "firefox://")!){
            let urlP = encodeByAddingPercentEscapes(url)
            openBrower(urlString: "firefox://open-url?url=\(urlP)")
            
//            let urlString = "firefox://open-url=\(url)"
//            openBrower(urlString: urlString)
            
//            let dataStrPosition = url.positionOf(sub: "data=")
//            let dataStr = url.subString(start: (dataStrPosition + 5))
//            let subDataStr = url.subString(start: 0, length: (dataStrPosition + 5))
//            ///////////////////////////
//            let escaped = encodeByAddingPercentEscapes(subDataStr)
//            let stitchingStr = escaped + dataStr
//            let finalUrl = "firefox://open-url?url=\(stitchingStr)"
//            openBrower(urlString: finalUrl)
            
//            let pasteboard = UIPasteboard.general
//            pasteboard.string = url
//            openBrower(urlString: "firefox://")
            
        }else {
            showToast(view: view, txt: "未安装“火狐”浏览器")
        }
    }else {
        
        openBrower(urlString: url)
    }
}

fileprivate func encodeByAddingPercentEscapes(_ input: String) -> String {
    return NSString(string: input).addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]"))!
}

func showUpdateDialog(controller:BaseController,version:String,content:String,url:String) ->Void{
    
    let Dict:[String:String] = ["version":version,"content":content,"url":url]
    let popView = PopupAlertView.init(frame:controller.view.bounds)
    popView.assignmentData = Dict
    popView.updateCallClose = { (url) in
        let alertController = UIAlertController(title: "若您使用的是超级签名APP请点击超级签名进行版本更新，以免出现安装失败的情况", message: "", preferredStyle:.alert)
        let enterpriseAction = UIAlertAction(title: "企业签名", style: .default) { (action) in
            
            YiboPreference.saveUpdatePayment(value: "0" as AnyObject)
            checkVersion(controller: controller, showDialog: true, showText: "正在检查更新...", isUpdate: true, isSuperSign: false)
             openBrower(urlString: url)
        }
        let superSignAction = UIAlertAction(title: "超级签名", style: .default) { (action) in
            checkVersion(controller: controller, showDialog: true, showText: "正在检查更新...", isUpdate: true, isSuperSign: true)        
            YiboPreference.saveUpdatePayment(value: "1" as AnyObject)
             openBrower(urlString: url)
        }
        alertController.addAction(enterpriseAction)
        alertController.addAction(superSignAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: {
            alertController.tapAlert();
            popView.removeFromSuperview()
        })
    }
    controller.view.addSubview(popView)
    
//    let alertController = UIAlertController(title: "发现新版本",
//                                            message: content, preferredStyle: .alert)
//    let okAction = UIAlertAction(title: "去更新", style: .default, handler: {
//        action in
//        if let config = getSystemConfigFromJson(){
//            if config.content != nil{
//                let go_broswer = config.content.goto_downpage_when_update_version == "on"
//                if go_broswer{
//                    openBrower(urlString: url)
//                }else{
//                    if let ulll = URL.init(string: url){
//                        UIApplication.shared.openURL(ulll)
//                    }
//                }
//            }
//        }
//    })
//    let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
//    alertController.addAction(okAction)
//    alertController.addAction(cancelAction)
//    do{
//        var msg = getAppName() + " " + version + "\n\n"
//        if !isEmptyString(str: content){
//            let split = content.components(separatedBy: ";")
//            if !split.isEmpty{
//                for item in split{
//                    msg = msg + item + "\n"
//                }
//            }else{
//                msg = msg + content
//            }
//        }
//        msg = msg + "\n"
//        let attrStr = try NSAttributedString.init(data: msg.description.data(using: String.Encoding.unicode)!, options: [.documentType:NSAttributedString.DocumentType.html], documentAttributes: nil)
//        alertController.setValue(attrStr, forKey: "attributedMessage")
//    }catch{
//        print(error)
//    }
//    controller.present(alertController, animated: true, completion: nil)
}

class NetwordUtil {
    static func isNetworkValid() -> Bool {
        var reachability: Reachability!
        reachability = try? Reachability.init(hostname: "baidu.com")
        
        // 检测网络连接状态
        if reachability.isReachable {
            print("网络连接：可用")
        } else {
            print("网络连接：不可用")
        }
        
        // 检测网络类型
        if reachability.isReachableViaWiFi {
            print("网络类型：Wifi")
        } else if reachability.isReachableViaWWAN {
            print("网络类型：移动网络")
        } else {
            print("网络类型：无网络连接")
        }
        return reachability.isReachable
    }
}

func dontShowPeilvWhenTouzhu(playCode:String) -> Bool{
    if playCode == lianxiao || playCode == hexiao || playCode == weishulian{
        return true
    }
    return false
}

func isQuanBuZhong(playCode:String)->Bool{
    if playCode == quanbuzhong{
        return true
    }
    return false
}

func isMulSelectMode(subCode:String) ->Bool {
    if subCode == "sze" || subCode == "sqz" ||
        subCode == "eqz" || subCode == "ezt" ||
        subCode == "tc" || subCode == "hxy" ||
        subCode == "hxe" || subCode == "hxs" ||
        subCode == "hxsi" || subCode == "hxw" ||
        subCode == "hxl" || subCode == "hxq" ||
        subCode == "hxb" || subCode == "hxj" ||
        subCode == "hxsh" || subCode == "hxsy" ||
        subCode == "wbz" || subCode == "lbz" ||
        subCode == "qbz" || subCode == "bbz" ||
        subCode == "jbz" || subCode == "sbz" ||
        subCode == "sybz" || subCode == "sebz" ||
        subCode == "wbz" || subCode == "lxex" ||
        subCode == "lxsx" || subCode == "lxsix" ||
        subCode == "lxwx" {
        return true
    }
    return false
}

func onSortPeilvNumber(s1:PeilvWebResult, s2:PeilvWebResult) -> Bool{
    return s1.selectedPosSortWhenClick < s2.selectedPosSortWhenClick
}

//是否六合彩彩种
func isSixMark(lotCode:String) -> Bool{
    if isEmptyString(str: lotCode) {
        return false;
    }
    if sixMarkArray.count > 0 {
        return sixMarkArray.contains(lotCode)
    }else {
        return lotCode.contains("LHC")
    }
//    return lotCode == "LHC" || lotCode == "SFLHC" || lotCode == "WFLHC" || lotCode == "FFLHC" || lotCode == "SLHC" || lotCode == "TTLHC" || lotCode == "AMLHC" || lotCode == "QMLHC";
}

func isXGLHC(lotCode:String) -> Bool{
    if isEmptyString(str: lotCode) {
        return false;
    }
    return lotCode == "LHC"
}

func isSixMarkWithTypeCode(type:String) -> Bool {
    
    return ["6","66"].contains(type)
//    return type == "6" || type == "66"
}

func isSaiche(lotType:String) -> Bool{
    return lotType == "3"
}

func isKuaiLeShiFeng(lotType: String) -> Bool {
    return lotType == "9"
}

func isKuai3(lotType:String) -> Bool{
    return lotType == "4"
}

func isFFSSCai(lotType:String) -> Bool{
    return lotType == "1" || lotType == "2"
}

func isXYNC(lotType:String) -> Bool{
    return lotType == "9"
}

func isPCEgg(lotType:String) -> Bool{
    return lotType == "7"
}

func isDPC(lotType:String) -> Bool{
    return lotType == "8"
}

/**
 * 判断是否是选中的号码
 * @param data
 * @return
 */
func isSelectedNumber(data:PeilvPlayData) ->Bool {
    if data.isSelected{
        return true
    }
    return false
}

func getPeilvPostNumbers(data:PeilvPlayData) -> String{
    var postNumber = ""
    if data.appendTag{
        postNumber = data.itemName + "--" + data.number
    }else{
        postNumber = data.number
    }
    return postNumber
}

func limitAccount(account:String) -> Bool{
    
    if account.count < 5 || account.count > 11{
        return false
    }
    
    let pwdRegx = ".*[a-zA-Z].*[0-9]|.*[0-9].*[a-zA-Z]";
    
    if !isMatchRegex(text: account, regex: pwdRegx) && !switch_allnumber_switch_when_register() {
        return false
    }
    
    if switch_allnumber_switch_when_register() {
        if isPurnInt(string: account)
        {
            return true
        }
        
        if !isMatchRegex(text: account, regex: pwdRegx)
        {
            return false
        }
    }
    
    return true
}

func limitPwd(account:String) -> Bool{
    //    let pwdRegx = ".*[a-zA-Z].*[0-9]|.*[0-9].*[a-zA-Z]";
    //    if !isMatchRegex(text: account, regex: pwdRegx){
    //        return false
    //    }
    if account.count < 6 || account.count > 16{
        return false
    }
    return true
}

func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
    var urlRequest = URLRequest(url: url)
    urlRequest.addValue(String.init(format: "SESSION=%@", YiboPreference.getToken()), forHTTPHeaderField: "Cookie")
    URLSession.shared.dataTask(with: urlRequest) {
        (data, response, error) in
        completion(data, response, error)
        }.resume()
}

//异步下载图片
func downloadImage(url: URL,imageUI:UIImageView) {
    print("Download Started")
    getDataFromUrl(url: url) { (data, response, error)  in
        guard let data = data, error == nil else { return }
        print(response?.suggestedFilename ?? url.lastPathComponent)
        print("Download Finished")
        DispatchQueue.main.async() { () -> Void in
            imageUI.image = UIImage(data: data)
        }
    }
}

func actionOpenGameList(controller:BaseController,gameCode:String,title:String,otherPlay:OtherPlay?) -> Void{
    let vc = UIStoryboard(name: "innner_game_list", bundle:nil).instantiateViewController(withIdentifier: "gameList")
    let other = vc as! GameListController
    other.gameCode = gameCode
    other.myTitle = title
    controller.navigationController?.pushViewController(other, animated: true)
}

//获取真人转发数据
func forwardReal(controller:BaseController,requestCode:Int,playCode:String) -> Void{
    //获取列表数据
    var url = ""
    var params:Dictionary<String,AnyObject> = [:]
    if playCode == "ag"{//AG真人娱乐
        url = url + REAL_AG_URL
    }else if playCode == "mg"{//MG真人娱乐
        url = url + REAL_MG_URL
        params["gameType"] = 1 as AnyObject
        params["mobile"] = 1 as AnyObject
    }else if playCode == "bbin"{//BBIN真人娱乐
        url = url + REAL_BBIN_URL
        
        params["gameType"] = 62 as AnyObject
        params["gameId"] = 5000 as AnyObject
        params["mobile"] = 1 as AnyObject
        
    }else if playCode == "ab"{//ab真人娱乐
        url = url + REAL_AB_URL
    }else if playCode == "ogzr" || playCode == "og"{//OG真人娱乐
        url = url + REAL_OG_URL
        params["gameType"] = "mobile" as AnyObject
    }else if playCode == "ds"{//ds真人娱乐
        url = url + REAL_DS_URL
    }else if playCode.contains("bg"){//BG娱乐
        url = BG_GET_URL
        var gameType = ""
        if playCode == "bgdz"{
            gameType = "2"
        }else if playCode == "bgby"{
            gameType = "3"
        }else{
            gameType = "1"
        }
        params.updateValue(gameType as AnyObject, forKey: "gameType")
        params.updateValue("1" as AnyObject, forKey: "mobile")
    }else if playCode.contains("dgzr"){//dg娱乐
        url = REAL_DG_URL
        params.updateValue("1" as AnyObject, forKey: "gameType")
        params.updateValue("1" as AnyObject, forKey: "mobile")
    }else if playCode.contains("rgzr"){//rg娱乐
        url = REAL_RG_URL
        params.updateValue("1" as AnyObject, forKey: "gameType")
        params.updateValue("1" as AnyObject, forKey: "mobile")
    }else if playCode.contains("ntzr"){//nt娱乐
        url = REAL_NT_URL
        params.updateValue("1" as AnyObject, forKey: "gameType")
        params.updateValue("1" as AnyObject, forKey: "mobile")
    }else if playCode == "ebet" {
        url = REAL_EBET_URL
        params.updateValue("1" as AnyObject, forKey: "gameType")
        params.updateValue("1" as AnyObject, forKey: "mobile")
    }
    
    print("url === ",url)
    controller.request(frontDialog: true,method: .post, loadTextStr:"正在跳转...", url:url,
                       params: params,
                       callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: controller.view, txt: convertString(string: "获取跳转链接失败"))
                            }else{
                                showToast(view: controller.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = RealPlayWraper.deserialize(from: resultJson){
                            if result.success{
                                if !isEmptyString(str: result.url){
                                    openBrower(urlString: result.url)
                                }else {
                                    let str = result.html
                                    if !str.isEmpty {
                                        openBBINDetail(controller: controller, content: str)
                                    }else {
                                        if !isEmptyString(str: result.msg){
                                            showToast(view:controller.view, txt: result.msg)
                                        }else{
                                            showToast(view:controller.view, txt: "获取跳转链接失败")
                                        }
                                    }
                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    showToast(view: controller.view, txt: result.msg)
                                    if result.msg.contains("超时") || result.msg.contains("登录"){
                                        loginWhenSessionInvalid(controller: controller)
                                    }
                                }else{
                                    showToast(view: controller.view, txt: convertString(string: "获取跳转链接失败"))
                                }
                            }
                        }
    })
}

func forwardRealWeb(controller:BaseController,forwardUrl:String,titleName:String) -> Void{
    controller.request(frontDialog: true,method: .post, loadTextStr:"正在跳转...", url:forwardUrl,
                       params: [:],
                       callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: controller.view, txt: convertString(string: "获取跳转链接失败"))
                            }else{
                                showToast(view: controller.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if resultJson.contains("<!DOCTYPE"){
                            let loginVC = UIStoryboard(name: "bbin_page", bundle: nil).instantiateViewController(withIdentifier: "bbin")
                            let recordPage = loginVC as! BBinWebContrllerViewController
                            recordPage.htmlContent = resultJson
                            recordPage.title = titleName
//                            recordPage.isHideNavBar = true
                            controller.navigationController?.pushViewController(recordPage, animated: true)
                            
                        }else{
                            if let result = RealPlayWraper.deserialize(from: resultJson){
                                if result.success{
                                    if !isEmptyString(str: result.url){
                                        //真人电子跳转外部浏览器打开
                                        if getSystemConfigFromJson()?.content.zrdz_jump_broswer == "on"{
                                            openBrower(urlString: result.url)
                                        }else{
                                            let actionCtrl = UIStoryboard(name: "active_detail_page", bundle: nil).instantiateViewController(withIdentifier: "activeDetail")
                                            let gameCenterCtrl = actionCtrl as! ActiveDetailController
                                            gameCenterCtrl.foreignUrl = result.url
                                            gameCenterCtrl.titleStr = titleName
                                            
                                            controller.navigationController?.pushViewController(gameCenterCtrl, animated: true)
                                        }
                                        
                                    }else {
                                        let str = result.html
                                        if !str.isEmpty {
                                            openBBINDetail(controller: controller, content: str)
                                        }else {
                                            if !isEmptyString(str: result.msg){
                                                showToast(view:controller.view, txt: result.msg)
                                            }else{
                                                showToast(view:controller.view, txt: "获取跳转链接失败")
                                            }
                                        }
                                    }
                                }else{
                                    if !isEmptyString(str: result.msg){
                                        showToast(view: controller.view, txt: result.msg)
                                        if result.msg.contains("超时") || result.msg.contains("登录"){
                                            loginWhenSessionInvalid(controller: controller)
                                        }
                                    }else{
                                        showToast(view: controller.view, txt: convertString(string: "获取跳转链接失败"))
                                    }
                                }
                            }
                        }
                
    })
}

func openBBINDetail(controller:UIViewController,content:String,playCode:String = "") -> Void {
    let loginVC = UIStoryboard(name: "bbin_page", bundle: nil).instantiateViewController(withIdentifier: "bbin")
    let recordPage = loginVC as! BBinWebContrllerViewController
    recordPage.playCode = playCode
    recordPage.htmlContent = content
    controller.navigationController?.pushViewController(recordPage, animated: true)
}

func getUnreadMsg(controller:BaseController,notify:Bool=false) -> Void{
    
    controller.request(frontDialog: false, method: .get, loadTextStr: "", url: UNREAD_MSG_COUNT, params: ["curVersion":getVerionName(),"appID":getAPPID(),"platform":"ios"]) { (resultJson:String, resultStatus:Bool) in
        //                        print("请求数据-------->>>>>>>\(resultJson)");
        if !resultStatus {
            return
        }
        
        if let result = UnreadMsgWraper.deserialize(from: resultJson){
            if result.success{
                let user:UserDefaults = UserDefaults.standard//持久储存对象
                
                /** 未读消息 */
                let messageNumber:PushMessageContentModel = result.content![0]
                let newMessagePush = PushPushTransform.deserialize(from: messageNumber.data)
                
                
                if newMessagePush?.push != nil && (newMessagePush?.push?.values.count)! > 0  && (newMessagePush?.count)! > 0{
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unredMessageChangeNoti"), object: "\((newMessagePush?.count)!)")
                    
                    if YiboPreference.getLoginStatus() {//是否登录
                        //推送详细信息
                        let messagePush = PushNewMessageModel.deserialize(from: newMessagePush?.push)
                        if (user.object(forKey: "newMessagePush") != nil) {
                            var ary:Array<Int> = user.object(forKey: "newMessagePush") as! Array<Int>
                            if !ary.contains((messagePush?.id)!) {
                                pushNotification(title: messagePush!.sendAccount, content: messagePush!.message,dic: ["code":10 as AnyObject], identifier: "unread_msg",badge: Int((newMessagePush?.count)!))
                                ary.append((messagePush?.id)!)
                                user.set(ary, forKey: "newMessagePush")
                            }
                        }else{//历史记录直接推送
                            pushNotification(title: messagePush!.sendAccount, content: messagePush!.message,dic: ["code":10 as AnyObject], identifier: "unread_msg",badge: Int((newMessagePush?.count)!))
                            let ary:Array<Int> = [(messagePush?.id)!]
                            user.set(ary, forKey: "newMessagePush")
                        }
                    }
                }
                
                /** 最新公告 */
                let announcement:PushMessageContentModel = result.content![1]
                let announcementPush = PushPushTransform.deserialize(from: announcement.data)
                if announcementPush?.push != nil && (announcementPush?.push?.values.count)! > 0 {
                    //最新公告push
                    let announcementModel = PushNewAnnouncementModel.deserialize(from: announcementPush?.push)
                    
                    /// 规避h5转换富文本时 内容过大导致线程阻塞
                    DispatchQueue.global().async {
                        let attStr = try? NSMutableAttributedString.init(data: (announcementModel?.content)!.data(using: String.Encoding.utf16)!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                        let contentStr:String = (attStr?.string)!
                        
                        if (user.object(forKey: "newAnnouncementPush") != nil) {
                            var ary:Array<Int> = user.object(forKey: "newAnnouncementPush") as! Array<Int>
                            if !ary.contains((announcementModel?.id)!) {
                                pushNotification(title: "有新的公告", content:contentStr ,dic: ["code":11 as AnyObject], identifier: "unread_announcement",badge: 0)
                                ary.append((announcementModel?.id)!)
                                user.set(ary, forKey: "newAnnouncementPush")
                            }
                        }else{
                            pushNotification(title: "有新的公告", content:contentStr ,dic: ["code":11 as AnyObject], identifier: "unread_announcement",badge: 0)
                            let ary:Array<Int> = [(announcementModel?.id)!]
                            user.set(ary, forKey: "newAnnouncementPush")
                        }
                    }
                }
                
                /** 优惠活动 */
                let discounts:PushMessageContentModel = result.content![2]
                let discountsPush = PushPushTransform.deserialize(from: discounts.data)
                if discountsPush?.push != nil && (discountsPush?.push?.values.count)! > 0 {
                    //优惠活动push
                    let discountsModel = PushDiscountsModel.deserialize(from: discountsPush?.push)
                    /// 规避h5转换富文本时 内容过大导致线程阻塞
                    DispatchQueue.global().async {
                        let attStr = try? NSMutableAttributedString.init(data: (discountsModel?.content)!.data(using: String.Encoding.utf16)!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                        let contentStr:String = (attStr?.string) ?? ""
                        if (user.object(forKey: "newDiscountsPush") != nil) {
                            var ary:Array<Int> = user.object(forKey: "newDiscountsPush") as! Array<Int>
                            if !ary.contains((discountsModel?.id)!) {
                                pushNotification(title: "有新的优惠活动", content: contentStr,dic: ["code":12 as AnyObject], identifier: "unread_discounts",badge: 0)
                                ary.append((discountsModel?.id)!)
                                user.set(ary, forKey: "newDiscountsPush")
                            }
                        }else{
                            pushNotification(title: "有新的优惠活动", content: contentStr,dic: ["code":12 as AnyObject], identifier: "unread_discounts",badge: 0)
                            let ary:Array<Int> = [(discountsModel?.id)!]
                            user.set(ary, forKey: "newDiscountsPush")
                        }
                    }
                }
                
                /** 新版本 */
                if result.content!.count > 3{
                    let newVersion:PushMessageContentModel = result.content![3]
                    let versionPush = PushPushTransform.deserialize(from: newVersion.data)
                    if versionPush?.push != nil && (versionPush?.push?.values.count)! > 0 {
                        let newVersionModel = CheckUpdateWraper.deserialize(from: versionPush?.push)
                        if newVersionModel?.success == true{
                            let contentStr = String(format: "%@%@", (newVersionModel?.content?.version)!,(newVersionModel?.content?.content)!)
                            pushNotification(title: "有新的版本可以更新", content: contentStr,dic: ["code":13 as AnyObject, "url":newVersionModel?.content?.url as AnyObject], identifier: "unread_newVersion",badge: 0)
                        }
                    }
                }
            }
        }
    }
}

func convertSportBallon(ballType:Int) -> String{
    if ballType == ALL_BALL_CONSTANT{
        return "全部"
    }else if ballType == FOOTBALL_CONSTANT{
        return "足球"
    }else if ballType == BASCKETBALL_CONSTANT{
        return "篮球"
    }
    return "全部"
}

func convertSportTimeType(timeType:Int) -> String{
    if timeType == 1{
        return "滚球"
    }else if timeType == 2{
        return "今日"
    }else if timeType == 3{
        return "早盘"
    }
    return ""
}

func isSpeicalLHCPlayCode(code:String) -> Bool{
    
    let codes = ["lm","yxa","wsa","hx","lx",weishulian]
    if codes.contains(code){
        return true
    }
    return false
}

func convertSportRecordStatus(status:Int,betResult:Float) -> String{
    if status == BALANCE_UNDO{
        return "等待开奖"
    }
    if status == BALANCE_CUT_GAME{
        return "赛事取消"
    }
    if status == BALANCE_DONE || status == BALANCE_AGENT_HAND_DONE ||
        status == BALANCE_BFW_DONE{
        if betResult > 0{
            return String.init(format: "派彩:%.2f元", betResult)
        }
        return "未中奖"
    }
    return "等待开奖"
}


func getSportRecordStatus(row: SportOrder) -> String {
    
    let balance = row.balance
    
    if balance == 1
    {
        return "等待开奖"
    }else if balance == 2
    {
        
        let resultStatus = row.resultStatus
        if resultStatus == 0{
            return "<font color='blue'>已确认</font>"
        }else if resultStatus == 1{
            return "<font color='red'>全输</font>";
        }else if resultStatus == 2
        {
            return "<font color='blueviolet'>输一半(派彩:"+row.winMoney+")</font>";
        }else if resultStatus == 3
        {
            return "<font color='chocolate'>平(退回本金:"+row.winMoney+")</font>";
        }else if resultStatus == 4
        {
            return "<font color='coral'>赢一半(派彩:"+row.winMoney+")</font>";
        }else if resultStatus == 5
        {
            return "<font color='green'>全赢(派彩:"+row.winMoney+")</font>";
        }else if resultStatus == 6
        {
            return "<font color='cadetblue'>撤单(退回本金:"+row.winMoney+")</font>";
        }
    }else if balance == 3
    {
        return "<font color='blue'>订单取消</font>";
    }else if balance == 4
    {
//        return "<font color='blue'>赛事腰斩</font>";
        return "<font color='blue'>订单取消</font>";
    }
    
    return ""
}



//是否优惠活动
//func isActiveShowFunc() -> Bool {
//    //获取存储在本地preference中的config
//    let system = getSystemConfigFromJson()
//    if let value = system{
//        let datas = value.content
//        if let active = datas?.isActive{
//            return active
//        }
//    }
//    return false
//}

func convertRealMainPlatformValue(platform:String) -> Int {
    switch platform {
    case "AG":
        return AG_INT
    case "MG":
        return MG_INT
    case "BBIN":
        return BBIN_INT
    case "ALLBET":
        return ALLBET_INT
    case "OG":
        return OG_INT
    case "DS":
        return DS_INT
    default:
        return 0
    }
}

func convertRatebackStatus(model:AllLotteryQueryModelRows) -> String{
    let agentType:Bool = (YiboPreference.getAccountMode() == AGENT_TYPE || YiboPreference.getAccountMode() == TOP_AGENT_TYPE)
    if !agentType{
        if model.rollBackStatus == "1"{
            return "未返水"
        }else if model.rollBackStatus == "2"{
            return "已返水"
        }
    }else{
        if model.proxyRollback == "1"{
            return "未返水"
        }else if model.proxyRollback == "2"{
            return "已返水"
        }else if model.proxyRollback == "3"{
            return "返水已回滚"
        }
    }
    return "未返水"
}

func convertMemberRatebackStatus(status:Int) -> String{
    if status == 1{
        return "未返水"
    }else if status == 2{
        return "已返水"
    }
    return "未返水"
}

func showFAB(attachView:UIView,items:[(title:String,imgname:String,action: (ActionButtonItem) -> Void)]) -> ActionButton {
    var datas:[ActionButtonItem] = []
    for (title,imgName,action) in items{
        print("title and imgname = ",title,imgName)
        let image = UIImage(named: imgName)!
        let item = ActionButtonItem(title: title, image: image)
        
        item.action = action
        datas.append(item)
    }
    let actionButton = ActionButton(attachedToView: attachView, items: datas)
    actionButton.action = { button in button.toggleMenu() }
    actionButton.setTitle("+", forState: UIControl.State())
    actionButton.backgroundColor = UIColor.red
    return actionButton
}

func convertEgameValue(platform:String) -> Int {
    switch platform {
    case "PT":
        return PT_INT
    case "QT":
        return QT_INT
    default:
        return 0
    }
}

/**
 content:通知的正文内容
 dic:通知中附加消息
 identifier:通知标识
 badge:未读消息数
 **/
func pushNotification(title:String,content:String,dic:[String:AnyObject],identifier:String,badge:Int=0){
    
        if #available(iOS 10.0, *) {
            let wrapper = UNMutableNotificationContent()
            wrapper.userInfo = dic
            wrapper.title = title
            wrapper.subtitle = ""
            wrapper.body = content
            wrapper.badge = NSNumber.init(value: badge)
            wrapper.sound = UNNotificationSound.default
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)
            let request = UNNotificationRequest(identifier: identifier, content: wrapper, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {error in
                print("成功建立通知...")
            })
        }else{
            //发送本地推送
            let notification = UILocalNotification()
            var fireDate = Date()
            fireDate = fireDate.addingTimeInterval(TimeInterval.init(4.0))
            notification.fireDate = fireDate
            
            notification.alertTitle = title
            
            notification.alertBody = content
            notification.userInfo = dic
            notification.timeZone = NSTimeZone.system
            
            notification.soundName = UILocalNotificationDefaultSoundName;//设置声音
            UIApplication.shared.scheduleLocalNotification(notification)
            UIApplication.shared.applicationIconBadgeNumber = badge;
    }
}

func getLiuheZodiaces(liuheNum:[String]) -> [String] {
    var zodiaces = [String]()
    for item in liuheNum
    {
        if let value_int = Int(item)
        {
            zodiaces.append(getZodiacWithNum(value_Int: value_int))
        }else
        {
            zodiaces.append("")
        }
    }
    
    return zodiaces
}

func getZodiacWithNumString(value_String: String) -> String {
    if let value_int = Int(value_String)
    {
        return getZodiacWithNum(value_Int:value_int)
    }else
    {
        return ""
    }
    
}

func getZodiacWithNum(value_Int: Int) -> String {
//    let current_sx = getShengXiaoFromYear()
    let shenxiaoArray = shenxiao_str
    
    for (_, title) in shenxiaoArray.enumerated() {
        let numbers = getNumbersFromShenXiaoName(sx: title)
        if numbers.contains("\(value_Int)") || numbers.contains("0\(value_Int)"){
            return title
        }
    }
    return ""
}

//
func getShenxiaoWithNumberAndYear(value_Int: Int,year:String="") -> String {
    var y = 0
    if !isEmptyString(str: year){
        y = Int(year)!
    }
    let sx = getShenxiaoFromNumberAndBenmingYear(number: String(value_Int), year: y)
    return sx;
}

// func getZodiacWithNum(value_Int: Int) -> String {
//    if value_Int == 22 || value_Int == 46 || value_Int == 34 || value_Int == 10 { return "牛"}else
//    if value_Int == 11 || value_Int == 23 || value_Int == 47 || value_Int == 35 { return "鼠"}else
//    if value_Int == 45 || value_Int == 33 || value_Int == 9 || value_Int == 21 { return "虎"}else
//    if value_Int == 3 || value_Int == 39 || value_Int == 15 || value_Int == 27 { return "猴"}else
//    if value_Int == 6 || value_Int == 42 || value_Int == 18 || value_Int == 30 { return "蛇"}else
//    if value_Int == 1 || value_Int == 13 || value_Int == 37 || value_Int == 25 || value_Int == 49{ return "狗"}else
//    if value_Int == 12 || value_Int == 36 || value_Int == 24 || value_Int == 48 { return "猪"}else
//    if value_Int == 32 || value_Int == 44 || value_Int == 8 || value_Int == 20 { return "兔"}else
//    if value_Int == 28 || value_Int == 16 || value_Int == 4 || value_Int == 40 { return "羊"}else
//    if value_Int == 7 || value_Int == 43 || value_Int == 31 || value_Int == 19 { return "龙"}else
//    if value_Int == 29 || value_Int == 41 || value_Int == 5 || value_Int == 17 { return "马"}else
//    if value_Int == 26 || value_Int == 14 || value_Int == 38 || value_Int == 2{ return "鸡"}
//    else {return ""}
//}

    func debugConfigURLs(controller:UIViewController)
    {
        #if DEBUG
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if YiboPreference.getOPenConfigURLPage() == "on"
                {
                    controller.navigationController?.pushViewController(ConfigDomainURLViewController(), animated: true)
                    YiboPreference.setOpenConfigURLPage(value: "off")
                }
            }
        #else
        #endif
    }


/** 是否有权限打开 ‘额度转换’ */
func shouldOpenExchangMoney(controller:UIViewController) -> Bool{
    
    if !YiboPreference.getLoginStatus(){
        showToast(view: controller.view, txt: "请先登录")
        return false
    }
    
    if YiboPreference.getAccountMode() == GUEST_TYPE{
        showToast(view: controller.view, txt: tip_shiwan)
        return false
    }
    
    if let config = getSystemConfigFromJson()
    {
        if config.content != nil
        {
            let switch_money_change = config.content.switch_money_change
            if isEmptyString(str: switch_money_change) || switch_money_change == "off"{
                showToast(view: controller.view, txt: "开关未开启,请联系客服")
                return false
            }
        }
    }
    
    return true
}



func iphoneType() ->String {
    
    var systemInfo = utsname()
    uname(&systemInfo)
    
    let platform = withUnsafePointer(to: &systemInfo.machine.0) { ptr in
        return String(cString: ptr)
    }
    
    if platform == "iPhone1,1" { return "iPhone 2G"}
    else if platform == "iPhone1,2" { return "iPhone 3G"}
    else if platform == "iPhone2,1" { return "iPhone 3GS"}
    else if platform == "iPhone3,1" { return "iPhone 4"}
    else if platform == "iPhone3,2" { return "iPhone 4"}
    else if platform == "iPhone3,3" { return "iPhone 4"}
    else if platform == "iPhone4,1" { return "iPhone 4S"}
    else if platform == "iPhone5,1" { return "iPhone 5"}
    else if platform == "iPhone5,2" { return "iPhone 5"}
    else if platform == "iPhone5,3" { return "iPhone 5C"}
    else if platform == "iPhone5,4" { return "iPhone 5C"}
    else if platform == "iPhone6,1" { return "iPhone 5S"}
    else if platform == "iPhone6,2" { return "iPhone 5S"}
    else if platform == "iPhone7,1" { return "iPhone 6 Plus"}
    else if platform == "iPhone7,2" { return "iPhone 6"}
    else if platform == "iPhone8,1" { return "iPhone 6S"}
    else if platform == "iPhone8,2" { return "iPhone 6S Plus"}
    else if platform == "iPhone8,4" { return "iPhone SE"}
    else if platform == "iPhone9,1" { return "iPhone 7"}
    else if platform == "iPhone9,2" { return "iPhone 7 Plus"}
    else if platform == "iPhone10,1" { return "iPhone 8"}
    else if platform == "iPhone10,2" { return "iPhone 8 Plus"}
    else if platform == "iPhone10,3" { return "iPhoneX"}
    else if platform == "iPhone10,4" { return "iPhone 8"}
    else if platform == "iPhone10,5" { return "iPhone 8 Plus"}
    else if platform == "iPhone10,6" { return "iPhone X"}
    else if platform == "iPhone9,4" { return "iPhone 7 Plus"}
    
    else if platform == "iPhone10,6" { return "iPhone X"}
    else if platform == "iPhone11,2" {return "iPhone XS"}
    else if platform == "iPhone11,4" || platform == "iPhone11,6"  {return "iPhone XS Max"}
    else if platform == "iPhone11,8" {return "iPhone XR"}
    else if platform == "iPhone12,1" {return "iPhone iPhone 11"}
    else if platform == "iPhone12,3" {return "iPhone 11 Pro"}
    else if platform == "iPhone12,5" {return "iPhone 11 Pro Max"}
    else if platform == "iPod1,1" { return "iPod Touch 1G"}
    else if platform == "iPod2,1" { return "iPod Touch 2G"}
    else if platform == "iPod3,1" { return "iPod Touch 3G"}
    else if platform == "iPod4,1" { return "iPod Touch 4G"}
    else if platform == "iPod5,1" { return "iPod Touch 5G"}
    
    else if platform == "iPad1,1" { return "iPad 1"}
    else if platform == "iPad2,1" { return "iPad 2"}
    else if platform == "iPad2,2" { return "iPad 2"}
    else if platform == "iPad2,3" { return "iPad 2"}
    else if platform == "iPad2,4" { return "iPad 2"}
    else if platform == "iPad2,5" { return "iPad Mini 1"}
    else if platform == "iPad2,6" { return "iPad Mini 1"}
    else if platform == "iPad2,7" { return "iPad Mini 1"}
    else if platform == "iPad3,1" { return "iPad 3"}
    else if platform == "iPad3,2" { return "iPad 3"}
    else if platform == "iPad3,3" { return "iPad 3"}
    else if platform == "iPad3,4" { return "iPad 4"}
    else if platform == "iPad3,5" { return "iPad 4"}
    else if platform == "iPad3,6" { return "iPad 4"}
    else if platform == "iPad4,1" { return "iPad Air"}
    else if platform == "iPad4,2" { return "iPad Air"}
    else if platform == "iPad4,3" { return "iPad Air"}
    else if platform == "iPad4,4" { return "iPad Mini 2"}
    else if platform == "iPad4,5" { return "iPad Mini 2"}
    else if platform == "iPad4,6" { return "iPad Mini 2"}
    else if platform == "iPad4,7" { return "iPad Mini 3"}
    else if platform == "iPad4,8" { return "iPad Mini 3"}
    else if platform == "iPad4,9" { return "iPad Mini 3"}
    else if platform == "iPad5,1" { return "iPad Mini 4"}
    else if platform == "iPad5,2" { return "iPad Mini 4"}
    else if platform == "iPad5,3" { return "iPad Air 2"}
    else if platform == "iPad5,4" { return "iPad Air 2"}
    else if platform == "iPad6,3" { return "iPad Pro 9.7"}
    else if platform == "iPad6,4" { return "iPad Pro 9.7"}
    else if platform == "iPad6,7" { return "iPad Pro 12.9"}
    else if platform == "iPad6,8" { return "iPad Pro 12.9"}
    
    else if platform == "i386"   { return "iPhone Simulator"}
    else if platform == "x86_64" { return "iPhone Simulator"}
    else {return "未识别机型: \(platform)"}
}

func openScanPage(controller:UIViewController) -> Void {
    let vc = UIStoryboard(name: "scan_page", bundle: nil).instantiateViewController(withIdentifier: "scanPage")
    let page = vc as! ScanCodeViewController
    page.idTxt = ""
    page.keyTxt = ""
    page.domainUrl = ""
    controller.navigationController?.pushViewController(page, animated: true)
}

/// 校验聊天室登录状态
func kCheckLoginStatus(vc: UIViewController) {
//    vc.navigationController?.pushViewController(CRChatController(), animated: true)
    //帐户相关信息
    let loadingHud = showLoadingHUD(loadingStr: "验证聊天室登录...")
    let url = VALID_LOGIN_URL
    kRequest(isContent: false, frontDialog: false, url: url) { (content) in
        loadingHud.hide(animated: true)
        if let result = ValidLoginWraper.deserialize(from: content as? String ?? ""){
            if result.success{
                YiboPreference.saveAutoLoginStatus(value: true)
                let chatVC = CRChatController()
                vc.navigationController?.pushViewController(chatVC, animated: true)
            }else{
                if !isEmptyString(str: result.msg){
                    showToast(view: vc.view, txt: result.msg)
                }else{
                    showToast(view: vc.view, txt: convertString(string: "验证失败"))
                }
                //被踢或未登录情况下，断开socket连接
                socket?.disconnect();
                isSocketConnect = false
                //本地登录状态置为false
                YiboPreference.saveAutoLoginStatus(value: false)
                //跳转到登录页
                if result.code == 0 || result.code == -1{
                    loginWhenSessionInvalid(controller: vc)
                }
            }
        }
    }
}
/// 退出聊天室时 需要校验
func kCheckLoginStauts() {
    kRequest(isContent: false, frontDialog: false, url: VALID_LOGIN_URL) { (content) in
        if let result = ValidLoginWraper.deserialize(from: content as? String ?? ""){
            if result.success{
                YiboPreference.saveAutoLoginStatus(value: true)
            }else{
                YiboPreference.saveAutoLoginStatus(value: false)
            }
        }
    }
}
 
 //MARK: 活动大厅
 ///活动大厅
 func openEventHall(controller:UIViewController) {
    let vc = EventHallController()
    vc.title = "活动大厅"
    controller.navigationController?.pushViewController(vc, animated: true)
 }
