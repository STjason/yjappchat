//
//  ThemeCommon.swift
//  gameplay
//
//  Created by admin on 2018/9/7.
//  Copyright © 2018 yibo. All rights reserved.
//

import UIKit

let THEME_PLAIN_NAME = "简约主题(推荐)"
let THEME_RED_NAME = "红色主题风格"
let THEME_BLUE_NAME = "蓝色主题风格"
let THEME_GREEN_NAME = "绿色主题风格"
let THEME_GLASS_ORANGE_NAME = "毛玻璃主题风格(橙色)"
let THEME_GLASS_BLUE_NAME = "毛玻璃主题风格(蓝色)"
let THEME_GLASS_PURE_NAME = "毛玻璃主题风格(紫色)"
let THEME_GLASS_REAL_NAME = "真人海报主题风格"

func setupTheme(immediately: Bool = false ) {
    if immediately {
        setupTheme(themePlistName: "Red", defaulThemePlistName: "Red")
    }else {
        let themePlistName = YiboPreference.getCurrentThmeByName()
        switch_native_default_color_theme(handler: {(defaulThemePlistName) -> Void in
            print("")
            setupTheme(themePlistName: themePlistName, defaulThemePlistName: defaulThemePlistName)
        })
    }
}

func setupTheme(themePlistName:String,defaulThemePlistName:String) {
    let themeStr = isEmptyString(str: themePlistName) ? defaulThemePlistName : themePlistName
    
    let themeIndex = getCurrentThemeIndexWithName(name:  themeStr)
    YiboPreference.setCurrentTheme(value: themeIndex as AnyObject)
    
    ThemeManager.setTheme(plistName: themeStr, path: .mainBundle)
    YiboPreference.setCurrentThemeByName(value: themeStr as AnyObject)
    YiboPreference.setCurrentTheme(value: themeIndex as AnyObject)
    
    UIApplication.shared.theme_setStatusBarStyle("Global.UIStatusBarStyle", animated: true)
    let navigationBarApperance = UINavigationBar.appearance()
    navigationBarApperance.theme_tintColor = "Global.barTextColor" // navBar的文字颜色
    navigationBarApperance.theme_barTintColor = "Global.barTintColor" // bar的背景色
    
    navigationBarApperance.theme_titleTextAttributes = ThemeStringAttributesPicker(keyPath: "Global.barTextColor") {
        value -> [NSAttributedString.Key : AnyObject]? in
        guard let rgba = value as? String else {
            return nil
        }
        
        let color = UIColor(rgba: rgba)
        let titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
            ]
        
        return titleTextAttributes
    }
        
    let tabbar = UITabBar.appearance()
    tabbar.theme_tintColor = "Global.themeColor"
    tabbar.theme_barTintColor = "Global.themeColor"
}

/** 简约主题 */
func isPlainBlue() -> Bool{
    return YiboPreference.getCurrentThmeByName() ==  "FrostedPlain"
}

/** 根据主题，选择投注控制器 */
func chooseControllerWithController(controller: UIViewController,lottery: LotteryData) {
    if !YiboPreference.getLoginStatus() {
        loginWhenSessionInvalid(controller: controller)
        return
    }
    
    let chatVC = ChatViewController()
    chatVC.chatViewType = .ChatFromBetPage
    
    //开关 是否是原生聊天室
    let isNativeChat = switch_nativeOrWapChat()
    
    if YiboPreference.getCurrentThmeByName() ==  "FrostedPlain"
    {
        if !YiboPreference.getLoginStatus() {
            loginWhenSessionInvalid(controller: controller)
            return
        }
        
        let touzhuController = UIStoryboard(name: "touzh_page",bundle:nil).instantiateViewController(withIdentifier: "touzhu_v3")
        let touzhuPage = touzhuController as! TouzhPlainController
        touzhuPage.lotData = lottery
        touzhuPage.cpVersion = "\(lottery.lotVersion)"
        
        let pageController = BetPageController.init(subControllers:switch_chatRoomOn() ? [touzhuController,chatVC] : [touzhuController], latestIndex: 0, scrollEnabled: false)
        
        controller.navigationController?.pushViewController(pageController, animated: true)
    }else{
        let touzhuController = UIStoryboard(name: "touzh_page",bundle:nil).instantiateViewController(withIdentifier: "touzhu_v2")
        let touzhuPage = touzhuController as! TouzhNewController
        touzhuPage.lotData = lottery
        touzhuPage.groupName = lottery.groupName
        touzhuPage.groupCode = lottery.groupCode
        touzhuPage.cpVersion = "\(lottery.lotVersion)"
        let pageController = BetPageController.init(subControllers:switch_chatRoomOn() ? [touzhuPage,chatVC] : [touzhuPage], latestIndex: 0, scrollEnabled: false)
        pageController.groupCode = lottery.groupCode
        
        controller.navigationController?.pushViewController(pageController, animated: true)
    }
}

/** 根据主题显示名称获得主题plist文件名 */
func getCurrentThemeListNameWithName(name:String) -> String{
    
    var themeStr = ""
    
    switch name {
    case THEME_RED_NAME,"red":
        themeStr = "Red"
    case THEME_BLUE_NAME,"blue":
        themeStr = "Blue"
    case THEME_GREEN_NAME,"green":
        themeStr = "Green"
    case THEME_GLASS_ORANGE_NAME,"blur_orange":
        themeStr = "FrostedOrange"
    case THEME_GLASS_BLUE_NAME,"blur_blue":
        themeStr = "FrostedBlue"
    case THEME_GLASS_PURE_NAME,"blur_purple":
        themeStr = "FrostedPurple"
    case THEME_GLASS_REAL_NAME,"realman":
        themeStr = "FrostedBeautyOne"
    case THEME_PLAIN_NAME,"simple":
        themeStr = "FrostedPlain"
    default:
        themeStr = "Red"
    }
    
    return themeStr
}

func getCurrentThemeIndexWithName(name:String) -> Int {
    
    var themeStr = 0
    
    switch name {
    case THEME_RED_NAME,"Red","red":
        themeStr = 1
    case THEME_BLUE_NAME,"Blue","blue":
        themeStr = 2
    case THEME_GREEN_NAME,"Green","green":
        themeStr = 3
    case THEME_GLASS_ORANGE_NAME,"FrostedOrange","blur_orange":
        themeStr = 4
    case THEME_GLASS_BLUE_NAME,"FrostedBlue","blur_blue":
        themeStr = 5
    case THEME_GLASS_PURE_NAME,"FrostedPurple","blur_purple":
        themeStr = 6
    case THEME_GLASS_REAL_NAME,"FrostedBeautyOne","realman":
        themeStr = 7
    case THEME_PLAIN_NAME,"FrostedPlain","simple":
        themeStr = 0
    default:
        themeStr = 0
    }
    
    return themeStr
}

//MARK: - TextField
func setThemeTextFieldTextColorGlassBlackOtherDarkGray(textField: UITextField) {
    textField.theme_textColor = "FrostedGlass.normalDarkTextColor"
}

//MARK: - TextView
func setThemeTextViewTextColorGlassWhiteOtherRed(textView: UITextView) {
    textView.theme_textColor = "FrostedGlass.textGlassWhiteOtherRed"
}

func setThemeTextViewTextColorGlassBlackOtherDarkGray(textView: UITextView) {
    textView.theme_textColor = "FrostedGlass.normalDarkTextColor"
}

//MARK: - Label
func setThemeLabelTextColorWithNavTextColor(label: UILabel) {
    label.theme_textColor = "Global.barTextColor"
}

func setThemeLabelTextColorGlassWhiteOtherRed(label: UILabel) {
    label.theme_textColor = "FrostedGlass.textGlassWhiteOtherRed"
}

func setThemeLabelTextColorGlassBlackOtherDarkGray(label: UILabel) {
    label.theme_textColor = "FrostedGlass.normalDarkTextColor"
}

func setThemeLabelTextColorGlassWhiteOtherBlack(label: UILabel) {
    label.theme_textColor = "FrostedGlass.glassTextWiteOtherBlack"
}

func setThemeLabelTextColorGlassWhiteOtherGray(label: UILabel) {
    label.theme_textColor = "FrostedGlass.textGlassWhiteOtherGray"
}

//MARK: - Button
func setThemeButtonTitleColorGlassBlackOtherDarkGray(button: UIButton) {
    button.theme_setTitleColor("FrostedGlass.normalDarkTextColor", forState: .normal)
}

func setThemeButtonTitleColorGlassWhiteOtherBlack(button: UIButton) {
    button.theme_setTitleColor("FrostedGlass.glassTextWiteOtherBlack", forState: .normal)
}

func setThemeButtonTitleColorGlassWhiteOtherGray(button: UIButton) {
    button.theme_setTitleColor("FrostedGlass.textGlassWhiteOtherGray", forState: .normal)
}

//MARK: - View
func setThemeViewThemeColor(view: UIView) {
    view.theme_backgroundColor = "Global.themeColor"
}

func setThemeViewColorGlassWhiteOtherGray(view: UIView) {
    view.theme_backgroundColor = "FrostedGlass.viewGrayGlassOtherGray"
}

func setThemeViewColorGlassBlackOtherRed(view: UIView) {
    view.theme_backgroundColor = "FrostedGlass.viewGlassBlackOtherRed"
}

func setThemeViewColorGlassAndOther(view: UIView, color:String) {
    view.theme_backgroundColor = ThemeColorPicker.init(keyPath: color)
}


/// 设置页面主题背景
///
/// - Parameters:
///   - view: 在view上添加主题图片，在view上，主题图片层级之上再添加透明或不透明view
///   - alpha: 主题图片之上的view的透明度
func setupthemeBgView(view: UIView?,alpha: CGFloat = 0.2) {
    if let viewP = view {
        
        setViewBackgroundColorTransparent(view: viewP)
        
        let bgImageView = UIImageView()
        viewP.addSubview(bgImageView)
        bgImageView.whc_Top(0).whc_Left(0).whc_Bottom(0).whc_Right(0)
        bgImageView.contentMode = .scaleAspectFill
        bgImageView.theme_image = "FrostedGlass.ColorImage"
        viewP.insertSubview(bgImageView, at: 0)
        
        var bgView = UIView()
        viewP.addSubview(bgView)
        bgView = bgView.whc_FrameEqual(bgImageView)
        bgView.theme_backgroundColor = "FrostedGlass.ColorImageCoverColor"
        
        if alpha == 0 {
            bgView.theme_alpha = "FrostedGlass.ColorImageCoverAlphZeroForGlass"
        }else {
            bgView.theme_alpha = "FrostedGlass.ColorImageCoverAlpha"
        }
        
        viewP.insertSubview(bgView, at: 1)
    }
}

/// 设置页面主题背景
/// 与方法 setupNoPictureAlphaBgView()的却别在于 不设置透明度
/// - Parameters:
///   - view: 在view上添加主题图片，在view上
func setupthemeBgViewForPlainTheme(view: UIView?) {
    if let viewP = view {
        
        setViewBackgroundColorTransparent(view: viewP)
        
        let bgImageView = UIImageView()
        viewP.addSubview(bgImageView)
        bgImageView.whc_Top(0).whc_Left(0).whc_Bottom(0).whc_Right(0)
        bgImageView.contentMode = .scaleAspectFill
        bgImageView.theme_image = "FrostedGlass.ColorImage"
        viewP.insertSubview(bgImageView, at: 0)
    }
}

func setupNoPictureAlphaBgView(view: UIView?,alpha: CGFloat = 0.2,bgViewColor: String = "FrostedGlass.subColorImageCoverColor",allThemeAlphaSame: Bool = false) {
    
    if let viewP = view {
        viewP.layer.masksToBounds = true
        viewP.clipsToBounds = true
        setViewBackgroundColorTransparent(view: viewP)
        var bgView = UIView()
        viewP.addSubview(bgView)
        bgView.isUserInteractionEnabled = false
        bgView = bgView.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
        bgView.theme_backgroundColor = ThemeColorPicker.init(keyPath: bgViewColor)
        
        if !allThemeAlphaSame {
            if alpha > 0.2 {
                bgView.theme_alpha = "FrostedGlass.ColorImageCoverAlphHigerForGlass"
            }else {
                bgView.theme_alpha = "FrostedGlass.ColorImageCoverAlpha"
            }
        }else {
            bgView.theme_alpha = "FrostedGlass.ColorImageCoverAlphPoint2"
        }
        
        viewP.insertSubview(bgView, at: 0)
    }
}

func setThemeViewNoTransparentDefaultGlassBlackOtherGray(view: UIView,bgViewColor: String = "FrostedGlass.viewColorGlassBlackOtherGray") {
    
    setViewBackgroundColorTransparent(view: view)
    var bgView = UIView()
    view.addSubview(bgView)
    bgView.isUserInteractionEnabled = false
    bgView = bgView.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
    bgView.theme_backgroundColor = ThemeColorPicker.init(keyPath: bgViewColor)
    
    view.insertSubview(bgView, at: 0)
}

func setViewAlphaWithTheme(view: UIView) {
    view.theme_alpha = "FrostedGlass.ColorImageCoverAlpha"
}

func setViewBackgroundColorTransparent(view: UIView,alpha:CGFloat = 0.0,color:UIColor = UIColor.white) {
    view.backgroundColor = color.withAlphaComponent(alpha)
}
