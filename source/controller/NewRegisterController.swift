//
//  NewRegisterController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/17.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit
import HandyJSON
import SnapKit

class NewRegisterController: BaseController,UITableViewDelegate,UITableViewDataSource{

    
    @IBOutlet weak var tableConstraintH: NSLayoutConstraint!
    @IBOutlet weak var logoUI: UIImageView!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var regButton:UIButton!
    @IBOutlet weak var customServer:UILabel!
    @IBOutlet weak var directLoginUI:UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var freeTrialButton: UIButton!
    @IBOutlet weak var backBtnTopConstraint: NSLayoutConstraint!
//    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var argeeButton: UIButton!
    @IBOutlet weak var imageViewBG: UIImageView!
    @IBOutlet weak var qrcodeConstraintH: NSLayoutConstraint!
    
    @IBOutlet weak var horizVertiScrollView: HorizVertiScrollView!
    var phoneNum = "" //手机号码
    var phoneOn = false //开启了手机号码项
    var verifiedCodeTimer:Timer? //验证码倒计时timer
    var verifiedCodeBtn:UIButton?
    var countdownTime = 60 //验证码倒计时
    let countdownConst = 60 //验证码倒计时
    var selectedPoint = CGPoint.zero // 输入框的相对屏幕的点
    var wechatQRCodes = [WechatQRCodeModel]()
    
    var loginAndRegDelegate:LoginAndRegisterDelegate?
    
//    var registers = [["name":"用户帐号:","hintText":"请输入用户名","neccessary":true],["name":"登录密码:","hintText":"请输入密码","neccessary":true],["name":"确认密码:","hintText":"请再次输入密码","neccessary":true]]
    var basicRegisters:[RegConfig] = []
    
    let ACCOUNT_ROW_INDEX = 0
    let PWD_ROW_INDEX = 1
    let AGAIN_PWD_INDEX = 2
    
    var params:Dictionary<String,String> = [:] //注册验证码参数
    var webView:WKWebView?
    
    lazy var bgView:UIButton =  {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.frame = UIScreen.main.bounds
        view.addTarget(self, action: #selector(clickBgView), for: .touchUpInside)
        return view
    }()
    
    lazy var loading:UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView()
        loading.hidesWhenStopped = true
        loading.style = .gray
        return loading
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showAnnounce(controller: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if /*glt_iphoneX*/UIScreen.main.bounds.height >= 812.0  {
            backBtnTopConstraint.constant = 54
        }else {
            backBtnTopConstraint.constant = 30
        }

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
//        self.tableView.register(RegisterTableCell.self, forCellReuseIdentifier: "registerCell")
        
        self.navigationItem.title = "注册"
        
        imageViewBG.theme_image = "Global.Login_Register.background"
        
        logoUI.contentMode = .scaleAspectFit
        updateAppLogo(logo: logoUI)
        
        directLoginUI.textColor = UIColor.white
        let str = NSMutableAttributedString.init(string: "已有账号,点击登录")
        str.addAttribute(.underlineStyle, value: 1, range: NSRange.init(location: 0, length: str.length))
        directLoginUI.attributedText = str
        
        customServer.textColor = UIColor.white
        let str2 = NSMutableAttributedString.init(string: "在线客服")
        str2.addAttribute(.underlineStyle, value: 1, range: NSRange.init(location: 0, length: str2.length))
        customServer.attributedText = str2
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.layer.cornerRadius = 5
        tableView.clipsToBounds = true
        tableView.separatorStyle = .none
        
        argeeButton.setTitle("同意并愿意遵守本公司用户注册协议", for: .normal)
        argeeButton.setTitleColor(UIColor.white, for: .normal)
        argeeButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        argeeButton.imageView?.contentMode = .scaleAspectFit
        argeeButton.theme_setImage("Global.Login_Register.rememberCheckboxNormal", forState: .normal)
        argeeButton.theme_setImage("Global.Login_Register.rememberCheckboxSelected", forState: .selected)
        
        argeeButton.isSelected = true //默认勾选
        argeeButton.tag = 123
        argeeButton.addTarget(self, action: #selector(button_TongyizhuceHandle(button:)), for: .touchUpInside)
        
        self.regButton.addTarget(self, action: #selector(onRegClick), for: UIControl.Event.touchUpInside)
        regButton.backgroundColor = UIColor.black
        regButton.layer.cornerRadius = 3.0
        regButton.clipsToBounds = true
        regButton.setTitle("确认注册", for: .normal)
        regButton.setTitleColor(UIColor.white, for: .normal)

        //准备帐号，密码，再次密码注册项
        let accountConfig = RegConfig()
        accountConfig.required = 2
        accountConfig.name = "用户帐号"
        accountConfig.show = 2
        accountConfig.uniqueness = 2
        accountConfig.validate = 2
        accountConfig.key = "reg_Account"
        
        let pwdConfig = RegConfig()
        pwdConfig.required = 2
        pwdConfig.name = "登录密码"
        pwdConfig.show = 2
        pwdConfig.uniqueness = 2
        pwdConfig.validate = 2
        pwdConfig.key = "reg_pwd"
        
        let pwdAgainConfig = RegConfig()
        pwdAgainConfig.required = 2
        pwdAgainConfig.name = "确认密码"
        pwdAgainConfig.show = 2
        pwdAgainConfig.uniqueness = 2
        pwdAgainConfig.validate = 2
        pwdAgainConfig.key = "reg_again_pwd"
        
        basicRegisters.append(accountConfig)
        basicRegisters.append(pwdConfig)
        basicRegisters.append(pwdAgainConfig)
        tableConstraintH.constant = CGFloat(basicRegisters.count * 44)
        
        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(recongnizer:)))
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(recongnizer:)))
        customServer.addGestureRecognizer(tap1)
        directLoginUI.addGestureRecognizer(tap2)
        
        let system = getSystemConfigFromJson()
        if let value = system{
            let datas = value.content
            if !isEmptyString(str: (datas?.onoff_mobile_guest_register)!) && datas?.onoff_mobile_guest_register == "on"{
                let rightBtn = UIBarButtonItem.init(title: "免费试玩", style: UIBarButtonItem.Style.plain, target: self, action: #selector(freeRegisterAction))
                self.navigationItem.rightBarButtonItem = rightBtn
            }
        }
        
        loadDatas()
//        requestWechatQRCode()
        openPopViewinitializationClick(requestSelf: self, controller: self)
        configDynamicUI()
    }
    
    //Mark: - 事件
    @objc private func tapImageAction() {
        
    }
    
    private func longPressPicAction(qrcodeImg:UIImage){
        let alert = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction.init(title: "保存到相册", style: .default, handler: {(action:UIAlertAction) in
            
            UIImageWriteToSavedPhotosAlbum(qrcodeImg, self, #selector(self.save_image(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(cancel)
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect.init(x: kScreenWidth/4, y: kScreenHeight, width: kScreenWidth/2, height: 300)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    //保存二维码
    @objc func save_image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            showToast(view: self.view, txt: "保存图片成功")
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    //键盘弹起响应
    @objc override func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            
            if selectedPoint.y > frame.origin.y
            {
                let deltaY = abs((frame.origin.y - (selectedPoint.y)))
                
                if keyBoardNeedLayout {
                    UIView.animate(withDuration: duration, delay: 0.0,
                                   options: UIView.AnimationOptions(rawValue: curve),
                                   animations: {
                                    self.view.frame = CGRect.init(x:0,y:-deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                                    self.keyBoardNeedLayout = false
                                    self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
    }
    
    //键盘隐藏响应
    @objc override func keyboardWillHide(notification: NSNotification) {

        if let userInfo = notification.userInfo,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            UIView.animate(withDuration: duration, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            self.view.frame = CGRect.init(x:0,y:0,width:self.view.bounds.width,height:self.view.bounds.height)
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func updateAppLogo(logo:UIImageView) -> Void {
        guard let sys = getSystemConfigFromJson() else{return}
        
        var newLogoImg = sys.content.login_page_logo_url
        
        if !isEmptyString(str: newLogoImg) {
            
            newLogoImg = handleImageURL(logoImgP: newLogoImg)
            
            let imageURL = URL(string: newLogoImg)
            logo.kf.setImage(with: ImageResource(downloadURL: imageURL!), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
        }else {
            var logoImg = sys.content.lottery_page_logo_url
            if !isEmptyString(str: logoImg){
                logoImg = handleImageURL(logoImgP: logoImg)
                
                let imageURL = URL(string: logoImg)
                logo.kf.setImage(with: ImageResource(downloadURL: imageURL!), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                logo.image = nil
            }
            
        }
    }
    
    //MARK: 地址可能是相对地址的处理
    private func handleImageURL(logoImgP: String) -> String{
        var logoImg = logoImgP
        if logoImg.contains("\t"){
            let strs = logoImg.components(separatedBy: "\t")
            if strs.count >= 2{
                logoImg = strs[1]
            }
        }
        logoImg = logoImg.trimmingCharacters(in: .whitespaces)
        if !logoImg.hasPrefix("https://") && !logoImg.hasPrefix("http://"){
            logoImg = String.init(format: "%@/%@", BASE_URL,logoImg)
        }
        
        return logoImg
    }
    
    @objc private func button_TongyizhuceHandle(button: UIButton) {
        button.isSelected = !button.isSelected
    }
    
    @objc func tapEvent(recongnizer: UIPanGestureRecognizer) {
        let label = recongnizer.view as! UILabel
        switch label.tag {
        case 10:
            openContactUs(controller: self)
        case 11:
            let vc = UIStoryboard(name: "login",bundle:nil).instantiateViewController(withIdentifier: "login_page") as! LoginController
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    @objc func freeRegisterAction() -> Void {
        freeRegister(controller: self, showDialog: true, showText: "试玩注册中...",delegate: self.loginAndRegDelegate)
    }
    
    @objc func onRegClick() -> Void {
        
        if self.basicRegisters.isEmpty{
            showToast(view: self.view, txt: "没有注册数据")
            return
        }
        
        self.view.endEditing(true)
        
        if !argeeButton.isSelected {
            showToast(view: self.view, txt: "请先同意注册协议")
            return
        }
        
        
        let accountModle = self.basicRegisters[0]
        let account  = accountModle.inputContent
        
        if isEmptyString(str: account){
            showToast(view: self.view, txt: "请输入帐号")
            return
        }
//        if !limitAccount(account: account){
//            showToast(view: self.view, txt: "账号由5-11个数字和字母组成,不能包含中文")
//            return
//        }
        
//        let pwdModle = modelDic["1"] as CellContentsModel?
        let pwdModle = self.basicRegisters[1]
        let pwd = pwdModle.inputContent
        if isEmptyString(str: pwd){
            showToast(view: self.view, txt: "请设置密码")
            return
        }
        if !limitPwd(account: pwd){
            showToast(view: self.view, txt: "密码请输入6-16个英文字母和数字组合")
            return
        }
        
//        let apwdModel = modelDic["2"] as CellContentsModel?
        let apwdModel = self.basicRegisters[2]
        let apwd = apwdModel.inputContent
        if isEmptyString(str: apwd){
            showToast(view: self.view, txt: "请再次确认密码")
            return
        }
        if pwd != apwd{
            showToast(view: self.view, txt: "两次密码设置不一致")
            return
        }
            
        var params:Dictionary<String,String> = [:]
        params["account"] = account
        params["password"] = pwd
        params["rpassword"] = apwd
        
        let webRows = self.basicRegisters.count
        if webRows >= 4{
            for index in 3...webRows-1{
                //将所有列表行数加上前面三项固定项:帐号，密码，确认密码
                //用于校验从后台获取的注册项
                let cellModel = self.basicRegisters[index]
                let cellInput = cellModel.inputContent
                
                //检查必输配置
                if cellModel.show == 2{
                    if (isEmptyString(str: cellInput) && cellModel.required == 2) {
                        showToast(view: self.view, txt: String.init(format: "请输入%@", cellModel.name))
                        return
                    }
                    //校验输入
                    if !isEmptyString(str: cellModel.regex){
                        if (cellModel.validate == 2 && !isMatchRegex(text: cellInput, regex: cellModel.regex)
                            && !isEmptyString(str: cellInput)) {
                            showToast(view: self.view, txt: String.init(format: "请输入正确格式的%@",   cellModel.name))
                            return;
                        }
                    }
                }
                params[cellModel.key] = cellInput
            }
        }
        let vcCodeModel = self.basicRegisters.last
        if let vc = vcCodeModel{
            let vcCodeText = vc.inputContent
            if isEmptyString(str: vcCodeText){
                showToast(view: self.view, txt: "请输入验证码")
                return
            }
            params["verifyCode"] = vcCodeText
        }
        
        self.params = params
        
        if switchRecaptcha_verify() {
            //行为验证码
            self.setupWebviewVerity(loginOrRegister: 1)
        }else {
            actionRegister(params: self.params)
        }
    }
    
    //MARK: 注册接口调用
    func actionRegister(params:Dictionary<String,String>) ->  Void{
        
        request(frontDialog: true,method: .post, loadTextStr: "正在注册中...", url:REGISTER_URL,params:params,
                callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "注册失败"))
                        }else{
                            showToast(view: weakSelf.view, txt: resultJson)
                        }
                        weakSelf.tableView.reloadData()
                        return
                    }
                    if let result = RegisterResultWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            showToast(view: weakSelf.view, txt: "注册成功")
                            YiboPreference.saveLoginStatus(value: true as AnyObject)
                            
                            weakSelf.isSixMarkRequest()
                            //获取注册帐户相关信息
                            if let infos = result.content{
                                YiboPreference.setAccountMode(value: infos.accountType as AnyObject)
                                //自动登录的情况下，要记住帐号密码
                                if YiboPreference.getAutoLoginStatus(){
                                    if !isEmptyString(str: infos.account){
                                        YiboPreference.saveUserName(value: infos.account as AnyObject)
                                    }
                                    if weakSelf.basicRegisters.count > 1{
                                        let name = weakSelf.basicRegisters[0].inputContent
                                        let pwd = weakSelf.basicRegisters[1].inputContent
                                        YiboPreference.savePwd(value: pwd as AnyObject)
                                        YiboPreference.saveUserName(value: name as AnyObject)
                                    }
                                }
                            }
//                            goMainScreenInPushMethod(controller: self)
//                            self.navigationController?.popViewController(animated: true)
                            weakSelf.onBackClick()
                            if let delegate = weakSelf.loginAndRegDelegate{
                                delegate.fromRegToLogin()
                            }
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: weakSelf.view, txt: errorMsg)
                            }else{
                                showToast(view: weakSelf.view, txt: convertString(string: "注册失败"))
                            }
                            weakSelf.tableView.reloadData()
                        }
                    }
        })
    }
    
    //是否是六合彩接口判断,返回数组
    func isSixMarkRequest() {
        request(frontDialog: false, method: .get, url: IS_SIXMARK, callback: { (resultJson:String,resultStatus:Bool) -> Void in
            if !resultStatus {
                return
            }
            
            class SixMarkModel:HandyJSON {
                required init() {}
                var success = false
                var accessToken = ""
                var content = [String]()
            }
            
            if let result = SixMarkModel.deserialize(from: resultJson){
                if result.success{
                    sixMarkArray = result.content
                    if !isEmptyString(str: result.accessToken){
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                    }
                    
                }else {}
            }
        })
    }

    
    func loadDatas() -> Void {
        request(frontDialog: true, loadTextStr: "获取注册配置中...", url:REG_CONFIG_URL,
                callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: weakSelf.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = RegisterConfigWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let configValues = result.content{
                                if !configValues.isEmpty{
//                                    self.webRegisters.removeAll()
                                    for config in configValues{
                                        if config.show == 2{
//                                            self.webRegisters.append(config)
                                            config.key = config.eleName
                                            weakSelf.basicRegisters.append(config)
                                            
                                            if config.eleName == "phone" && switchVerifiedCodeOn() {
                                                weakSelf.phoneOn = true
                                                config.required = 2 //如果需要输入手机验证码，则手机号码为必须输入项目
                                                
                                                let vcCodeConfig = RegConfig()
                                                vcCodeConfig.required = 2
                                                vcCodeConfig.name = "手机验证码"
                                                vcCodeConfig.show = 2
                                                vcCodeConfig.uniqueness = 2
                                                vcCodeConfig.required = 2
                                                vcCodeConfig.validate = 1
                                                vcCodeConfig.key = "smsVerifyCode"
                                                weakSelf.basicRegisters.append(vcCodeConfig)
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            //显示验证码区
                            let vcCodeConfig = RegConfig()
                            vcCodeConfig.required = 2
                            vcCodeConfig.name = "验证码"
                            vcCodeConfig.show = 2
                            vcCodeConfig.uniqueness = 2
                            vcCodeConfig.required = 2
                            vcCodeConfig.validate = 1
                            vcCodeConfig.key = "verifyCode"
//                            self.webRegisters.append(vcCodeConfig)
                            weakSelf.basicRegisters.append(vcCodeConfig)
                            
                            weakSelf.tableConstraintH.constant = CGFloat(weakSelf.basicRegisters.count * 44)
                            
                            weakSelf.tableView.reloadData()
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: weakSelf.view, txt: errorMsg)
                            }else{
                                showToast(view: weakSelf.view, txt: convertString(string: "获取失败"))
                            }
                        }
                    }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return basicRegisters.count
    }
    
    @objc func inputUIContentChange(textField:UITextField)->Void{
        let index = (textField.tag - 1000)
        if !basicRegisters.isEmpty{
            basicRegisters[index].inputContent = textField.text!
            
            //获取手机号码
            let model = basicRegisters[index]
            if model.eleName == "phone" {
                phoneNum = textField.text!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "registerCell", for: indexPath) as? RegisterTableCell else {
            fatalError("The dequeued cell is not an instance of RegisterTableCell.")
        }
        
        cell.inputUI.tag = indexPath.row + 1000
        cell.inputUI.addTarget(self, action: #selector(inputUIContentChange(textField:)), for: UIControl.Event.editingDidEnd)
        cell.inputUI.addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        
        
        let item:RegConfig = basicRegisters[indexPath.row]
        cell.isRequire = item.required //  是否必须
        
        let account = item.inputContent
        cell.inputUI.text = account
        
        if item.key == "reg_pwd" || item.key == "reg_again_pwd"{
            cell.inputUI.isSecureTextEntry = true
        }else{
            cell.inputUI.isSecureTextEntry = false
        }
        cell.fillNameUI(regName: item.name,index: indexPath.row, placeHolder: item.tips)
        if item.key == "verifyCode"{
            cell.vcCodeUI.isHidden = false
            //开始异步获取验证码图片
            cell.fillImage()
        }else{
            cell.vcCodeUI.isHidden = true
        }
        
        if item.eleName == "phone" && switchVerifiedCodeOn() {
            setupVerifiedCodeBtn(view: cell.contentView)
        }

        return cell
    }
    
    func setupVerifiedCodeBtn(view:UIView) {
        let btn = UIButton()
        verifiedCodeBtn = btn
        btn.backgroundColor = UIColor.groupTableViewBackground
        btn.layer.cornerRadius = 3
        btn.setTitleColor(UIColor.darkText, for: .normal)
        btn.setTitleColor(UIColor.darkText, for: .highlighted)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.addTarget(self, action: #selector(getVerifiedCode), for: .touchUpInside)
        view.addSubview(btn)
        
        verifiedCodeBtn!.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.rightMargin.equalToSuperview().offset(-5)
            make.height.equalTo(40)
            make.width.equalTo(100)
        }
        
        if countdownTime != countdownConst {
            btn.setTitle("\(countdownTime) 秒", for: .normal)
        }else {
            btn.setTitle("获取验证码", for: .normal)
        }
    }
    
    //MARK: 获取手机验证码
    @objc func getVerifiedCode() {
        
        if countdownTime == countdownConst && (verifiedCodeBtn?.titleLabel?.text ?? "" == "获取验证码")  {
            view.endEditing(true)
            
            if phoneNum.isEmpty {
                showToast(view: view, txt: "请输入手机号码")
                return
            }
            
            requestPhoneCode(phone:phoneNum)
            
            verifiedCodeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
            RunLoop.main.add(verifiedCodeTimer!, forMode: .common)
            
            verifiedCodeBtn?.setTitle("\(countdownTime) 秒", for: .normal)
        }
    }
    
    //MARK: 请求验证码接口
    func requestPhoneCode(phone:String) {
        
        request(frontDialog: true, url: VERITY_PHONE_CODE,params: ["phone":phone]) {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: weakSelf.view, txt: convertString(string: "获取失败"))
                }else{
                    showToast(view: weakSelf.view, txt: resultJson)
                }
                return
            }
            
            if let result = CRBaseModel.deserialize(from: resultJson){
                if result.success && result.msg.isEmpty {
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    showToast(view: weakSelf.view, txt: "验证码已发送,请耐心等待")
                }else {
                    let msg = result.msg.isEmpty ? "发送验证码错误" : result.msg
                    showToast(view: weakSelf.view, txt: msg)
                }
            }
        }
    }
    
    @objc func countdown() {
        countdownTime -= 1
        
        if countdownTime <= 0 {
            verifiedCodeBtn?.setTitle("获取验证码", for: .normal)
            countdownTime = countdownConst
            verifiedCodeTimer?.invalidate()
        }else {
            verifiedCodeBtn?.setTitle("\(countdownTime) 秒", for: .normal)
        }
    }
    
    @objc private func editingDidBegin(ui:CustomFeildText) {
        let point = ui.convert(CGPoint.init(x: 0, y: (0 + 44)), to: self.view.window)
        selectedPoint = point
    }
  
    //MARK: - 点击事件
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func freeTrialAction() {
        self.navigationController?.popViewController(animated: true)
        freeRegister(controller: self, showDialog: true, showText: "试玩注册中...", delegate: nil)
    }
    
    private func configDynamicUI() {
        let system = getSystemConfigFromJson()
        if let value = system{
            let datas = value.content
            if !isEmptyString(str: (datas?.onoff_mobile_guest_register)!) && datas?.onoff_mobile_guest_register == "on"{
                freeTrialButton.isHidden = false
            }else{
                freeTrialButton.isHidden = true
            }
            
            if !isEmptyString(str: (datas?.online_customer_showphone)!) && datas?.online_customer_showphone == "on"{
                customServer.isHidden = false
            }else{
                customServer.isHidden = false
            }
        }
    }
    
    // 遍历数组,取出公告赋值
    private func forArrToAlert(notices: Array<NoticeResult>) {
        
        var noticesP = notices
        noticesP = noticesP.sorted { (noticesP1, noticesP2) -> Bool in
            return noticesP1.sortNum < noticesP2.sortNum
        }
        
        var models = [NoticeResult]()
        for index in 0..<noticesP.count {
            let model = noticesP[index]
            if model.isReg {
                models.append(model)
            }
        }
        
        if models.count > 0 {
            let weblistView = WebviewList.init(noticeResuls: models)
            weblistView.show()
        }
        
    }
    
    private func showAnnounce(controller:BaseController) {
        if YiboPreference.isShouldAlert_isAll() == "" {
            YiboPreference.setAlert_isAll(value: "on" as AnyObject)
        }
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if !shouldShowNoticeWIndow() {
                return
            }
        }
        //获取公告弹窗内容
        controller.request(frontDialog: false, url:ACQURIE_NOTICE_POP_URL,params:["code":19],
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            
                            if let result = NoticeResultWraper.deserialize(from: resultJson){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                PopupAlertListView(resultJson: resultJson,controller:self,not:"",anObject:nil,from: 3)
                            }
        })
    }
    
    //MARK: - 水平、竖直滚动视图
    private func requestWechatQRCode() {
        request(frontDialog: true, method: .get, loadTextStr: "", url: QRCODE_WECHAT_IMAGES, params: ["showPage":4]) { (resultJson:String, resultStatus:Bool) in
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            if let result = WechatQRCodeWraper.deserialize(from: resultJson)
            {
                
                if result.success {
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                
                    if let datas = result.content
                    {
                        
                        if datas.count == 0 {return}
                        if let afsList = datas[0].afsList
                        {
                            if afsList.count > 0
                            {
                                let isHori = datas[0].imgType == "2"
                                self.qrcodeConstraintH.constant = CGFloat(isHori ? 200 : 200 * afsList.count + 20 * (afsList.count - 1))
                                self.horizVertiScrollView.configWithModels(qrcodeImgs: afsList, isHorizontal: isHori)
                                self.horizVertiScrollView.openURLHandler = {() -> Void in
                                    if UIApplication.shared.applicationState == UIApplication.State.active
                                    {
                                        showToast(view: self.view, txt: "跳转地址不正确")
                                    }
                                }
                                
                                self.horizVertiScrollView.longGestureHandler = {(img) -> Void in
                                    if let imgP = img
                                    {
                                        self.longPressPicAction(qrcodeImg: imgP)
                                    }else
                                    {
                                        showToast(view: self.view, txt: "图片为空")
                                    }
                                }
                                
                                self.horizVertiScrollView.deleteItemHandler = {(count) -> Void in
                                    if !isHori
                                    {
                                        self.qrcodeConstraintH.constant = CGFloat(200 * count + 20 * (count - 1))
                                    }else
                                    {
                                        if count == 0 {self.qrcodeConstraintH.constant = 0}
                                    }
                                }
                                
                            }else
                            {
                                self.qrcodeConstraintH.constant = 0
                            }
                        }
                    }
                }else{
                    if !isEmptyString(str: result.msg){
                        showToast(view: self.view, txt: result.msg)
                    }else{
                        showToast(view: self.view, txt: convertString(string: "获取客服二维码失败"))
                    }
                }
            }
        }
    }
}



extension NewRegisterController:WKScriptMessageHandler, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loading.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let trust = challenge.protectionSpace.serverTrust {
                let urlCrentCard = URLCredential.init(trust: trust)
                completionHandler(.useCredential,urlCrentCard)
            }
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        clickBgView()
        showToast(view: view, txt: "验证成功")
        
        actionRegister(params: self.params)
    }
    
    //MARK: webview行为验证码
    ///loginOrRegister 0 login，1 register
    func setupWebviewVerity(loginOrRegister:Int) {
        var url = ""
        var configName = ""
        
        if loginOrRegister == 0 {
            url = BASE_URL + ACTIVITY_LOGIN_CODE_WEB
            configName = "nativeVertifyLogin"
        }else if loginOrRegister == 1 {
            url = BASE_URL + ACTIVITY_REGISTER_CODE_WEB
            configName = "nativeVertifyRegister"
        }else {
            showToast(view: self.view, txt: "验证码类型不存在")
            return
        }
        
        let webConfiguration = WKWebViewConfiguration()
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        webConfiguration.preferences = preferences
        webConfiguration.userContentController = WKUserContentController()
        webConfiguration.userContentController.add(self, name: configName)
        
        webView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        webView?.scrollView.bouncesZoom = false
        webView?.scrollView.showsVerticalScrollIndicator = false
        webView?.scrollView.showsHorizontalScrollIndicator = false
        webView?.scrollView.isScrollEnabled = false
        webView?.sizeToFit()
        webView?.navigationDelegate = self
        guard let myURL = URL(string: url) else {
            return
        }
        
        let myRequest = URLRequest(url: myURL)
        
        guard let web = webView else {return}
        web.load(myRequest)
        
        view.addSubview(bgView)
        view.addSubview(web)
        view.addSubview(loading)
        
        loading.startAnimating()
        
        web.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(kScreenWidth * 0.8)
            make.height.equalTo(kScreenWidth * 0.8 * 2 / 3)
        }
        
        loading.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(30)
        }
    }
    
    @objc func clickBgView() {
        loading.stopAnimating()
        webView?.removeFromSuperview()
        bgView.removeFromSuperview()
        loading.removeFromSuperview()
        webView = nil
    }
}










