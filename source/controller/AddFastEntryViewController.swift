//
//  AddFastEntryViewController.swift
//  gameplay
//
//  Created by admin on 2018/9/16.
//  Copyright © 2018年 yibo. All rights reserved.
// //  环形菜单，没有使用 数据库，数据管理很混乱，这一块有时间必须要改

import UIKit

class AddFastEntryViewController: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    var realPersons:[LotteryData] = []
    
    var isFirstIn = true
    /** 快捷方式的 tuples */
    var datas = [(String,String)]()
    var hadSelectedDatas = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "保存", style: UIBarButtonItem.Style.plain, target: self, action: #selector(btnClick))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", style:.plain, target: self, action: #selector(backLastCtrl))
        navigationItem.title = "添加快捷方式"
        setupView()
        setupData()
    }
    

    private func setupView() {
        self.mainTableView.allowsMultipleSelection = true
    }
    
    private func setupData() {
        datas = addFastEntryMenuItems
        var currentDatas = datas
        
        if let config = getSystemConfigFromJson()
        {
            let exchange_score = config.content.exchange_score
            if exchange_score != "on"
            {
                YiboPreference.deleteMenuItems(value: ["积分兑换"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["积分兑换"], datas: currentDatas)
            }
            
            let onoff_turnlate = config.content.onoff_turnlate
            if onoff_turnlate != "on"
            {
                YiboPreference.deleteMenuItems(value: ["大转盘"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["大转盘"], datas: currentDatas)
            }
            
            let onoff_member_mobile_red_packet = config.content.onoff_member_mobile_red_packet
            if onoff_member_mobile_red_packet != "on"
            {
                YiboPreference.deleteMenuItems(value: ["抢红包"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["抢红包"], datas: currentDatas)
            }
            
            let onoff_dian_zi_you_yi = config.content.onoff_dian_zi_you_yi
            if onoff_dian_zi_you_yi != "on"
            {
                YiboPreference.deleteMenuItems(value: ["YG棋牌(NB)"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["YG棋牌(NB)"], datas: currentDatas)
            }
            
            let onoff_sport_switch = config.content.onoff_sport_switch
            if onoff_sport_switch != "on"
            {
                YiboPreference.deleteMenuItems(value: ["皇冠体育"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["皇冠体育"], datas: currentDatas)
            }
            let onoff_zheren_game_switch = config.content.onoff_zhen_ren_yu_le
            //真人游戏
            if onoff_zheren_game_switch != "on"{
                //真人娱乐
                for realPersonItem in realPersons{
                    YiboPreference.deleteMenuItems(value: ["AG真人"])
                    currentDatas = deleteItemWithTitleFromTuple(titles: ["AG真人"], datas: currentDatas)
                    YiboPreference.deleteMenuItems(value: [realPersonItem.name])
                    currentDatas = deleteItemWithTitleFromTuple(titles: [realPersonItem.name], datas: currentDatas)
                }
            }
        }
        
        datas = currentDatas
        hadSelectedDatas = YiboPreference.getSemicircelMenuItems()
    }
    
    private func deleteItemWithTitleFromTuple(titles: Array<String>,datas: Array<(String,String)>) ->  [(String,String)] {
        var resultsDatas = datas
        for index in 0..<datas.count
        {
            let item = datas[index]
            
            for inIndex in 0..<titles.count
            {
                if titles[inIndex] == item.0
                {
                    resultsDatas.remove(at: index)
                }
            }
        }
        
        return resultsDatas
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
  
}
//MARK：UITableViewDelegate代理
extension AddFastEntryViewController: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isFirstIn = false
        let cell = self.mainTableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        cell?.selectionStyle = .none
    }
    
    func tableView(_ tableView: UITableView,didDeselectRowAt indexPath: IndexPath) {
        self.isFirstIn = false
        let cell = self.mainTableView?.cellForRow(at: indexPath)
        cell?.accessoryType = .none
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "addFastEntryCell") as? AddFastEntryCell else {
            fatalError("dequeueReusableCell AddFastEntryCell failure")
        }
        
        let item = self.datas[indexPath.row]
        cell.configWithItem(item:item)

        if isFirstIn
        {
            if hadSelectedDatas.contains(item.0)
            {
                tableView.selectRow(at: indexPath, animated: true, scrollPosition:UITableView.ScrollPosition.none)
            }
        }
        
        if let selectedItems = self.mainTableView.indexPathsForSelectedRows {
            if selectedItems.contains(indexPath){
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
}
//MARK:事件响应
extension AddFastEntryViewController{
    @objc func btnClick() {
        
        showToast(view: self.view, txt: "保存成功")
        
        var array = [String]()
        for (_,value) in SemicircleMenuItems.enumerated(){
            array.append(value.0)
        }
        
        var titles = [String]()
        if let selectedItems = mainTableView.indexPathsForSelectedRows {
            for indexPath in selectedItems{
                titles.append(datas[indexPath.row].0)
            }
            YiboPreference.setSemicircleMenuItems(value: array)
            YiboPreference.addMenuItems(value: titles)
        }else{
            YiboPreference.setSemicircleMenuItems(value: array)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    @objc func backLastCtrl(){
        navigationController?.popViewController(animated: true)
    }
}

