//
//  ConfigDomainURLViewController.swift
//  gameplay
//
//  Created by admin on 2018/9/8.
//  Copyright © 2018 yibo. All rights reserved.
//

import UIKit

class ConfigDomainURLViewController: UIViewController {
    
    var offenURLs = ["https://t1.yunji22.com"]
    var textField = CustomFeildText()
    var tableView = UITableView()
    var confirmButton = UIButton()
    var switchButton = UISwitch()
    var urlDatas = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        loadData()
        setupURLView()
    }
    
    @objc private func switchButtonChange() {
        YiboPreference.setShowRecentDomainURLs(value: switchButton.isOn ? "on" : "off")
    }
    
    private func setupURLView() {
        self.view.addSubview(textField)
        self.view.addSubview(switchButton)
        self.view.addSubview(tableView)
        self.view.addSubview(confirmButton)
        
        textField.whc_Top(64).whc_Left(20).whc_Height(40).whc_Right(10,toView:switchButton)
        switchButton.whc_CenterY(0,toView:textField).whc_Right(5).whc_Width(80).whc_Height(30)
        tableView.whc_Top(20,toView:textField).whc_Left(0).whc_Bottom(44).whc_Right(0)
        confirmButton.whc_Left(0).whc_Bottom(0).whc_Right(0).whc_Height(44)
        
        textField.backgroundColor = UIColor.groupTableViewBackground
        
        let showRecentURLs = YiboPreference.getShowRecentDomainURLs()
        switchButton.isOn = showRecentURLs == "on"
        switchButton.addTarget(self, action: #selector(switchButtonChange), for: .touchUpInside)
        
        textField.clearButtonMode = .always
        textField.text = Bundle.main.infoDictionary!["domain_url"] as? String
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "domainCell")
        
        
        confirmButton.theme_backgroundColor = "Global.themeColor"
        confirmButton.setTitleColor(UIColor.white, for: .normal)
        confirmButton.setTitle("确定", for: .normal)
        confirmButton.addTarget(self, action: #selector(clickConfigAction), for: .touchUpInside)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func loadData() {
        urlDatas = YiboPreference.getRecentDomainURLs()
    }
    
    @objc func clickConfigAction() {
        
        if let text = textField.text
        {
            if !isEmptyString(str: text)
            {
                var array = YiboPreference.getRecentDomainURLs()
                
                if array.contains(text)
                {
                    for (index,url) in array.enumerated()
                    {
                        if url == text
                        {
                            array.remove(at: index)
                        }
                    }
                }
                
                array.insert(text, at: 0)
                YiboPreference.setRecentDomainURLs(value: array)
                loadData()
                tableView.reloadData()
                
                showToast(view: self.view, txt: "切换成功，请重启 APP")
            }
        }
    }
     
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ConfigDomainURLViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urlDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "domainCell", for: indexPath)
        
        let url = urlDatas[indexPath.row]
        cell.textLabel?.text = url
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = urlDatas[indexPath.row]
        textField.text = url
    }
}









