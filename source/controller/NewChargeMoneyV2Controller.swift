//
//  NewChargeMoneyV2Controller.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

//新充值页
class NewChargeMoneyV2Controller: BaseController {
    
    
    @IBOutlet weak var payFunctionHeader: UIView!
    @IBOutlet weak var headerBgImage: UIImageView!
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var headerImg:UIImageView!
    @IBOutlet weak var accountTV:UILabel!
    @IBOutlet weak var balanceTV:UILabel!
    @IBOutlet weak var tableView:UITableView!
    var meminfo:Meminfo?
    var payMethods:PayMethodResult!
    var selectPayType = PAY_METHOD_ONLINE;//已选择的支付方式
    
    var use_wap_icons = true // 是否使用网页版充值图标
    var propmtString = ""
    
    var datas:[Dictionary<String,String>] = [["img":"MemberPage.Charge.topup_zaixianzhifu","text":"在线支付"]]

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showAnnounce(controller: self)
    }
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view,alpha: 0)
        
        setupNoPictureAlphaBgView(view: payFunctionHeader,alpha: 0.4)
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        navigationItem.title = "充值"
//        headerImg.theme_image = "General.placeHeader"
        headerBgImage.contentMode = .scaleAspectFill
        headerBgImage.clipsToBounds = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView.init()
//        tableView.reloadData()
        if self.meminfo == nil{
            syncAccount()
        }else{
            //赋值帐户名及余额
            self.updateAccount(memInfo: self.meminfo!)
            self.syncNewPayMethod()
//            self.syncPayMethod()
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        headerView.isUserInteractionEnabled = true
        addGestureRecognizer(headerView: headerView)
        
        headerImg.layer.cornerRadius = 30.0
        headerImg.layer.masksToBounds = true
        headerImg.contentMode = UIView.ContentMode.scaleAspectFill
        
        //更新头像
        if let sysconfig = getSystemConfigFromJson(){
            if sysconfig.content != nil{
                let logoImg = sysconfig.content.member_page_logo_url
                use_wap_icons = sysconfig.content.charge_icon_wap_switch == "on"
                if isEmptyString(str: logoImg){
                    if YiboPreference.getCACHEAVATARdata().count >= 1 {
                        headerImg.image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
                    }else{
                        headerImg.theme_image = "General.placeHeader"
                    }
                    headerBgImage.theme_image = "General.personalHeaderBg"
                }else{
                    updateAppLogo(icon: self.headerImg)
                }
            }
        }
        updateHeaderBgLogo(imageView: headerBgImage)
        print("use wap icon flag === ",use_wap_icons)
    }
    
    private func addGestureRecognizer(headerView:UIView) {
        let longPress = UITapGestureRecognizer(target: self, action: #selector(self.pressClick))
        headerView.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(longPress)
    }
    
    @objc func pressClick(){
        self.navigationController?.pushViewController(UserInfoController(), animated: true)
    }
    
    func updateAccount(memInfo:Meminfo) -> Void {
        var accountNameStr = ""
        var leftMoneyName = ""
        if !isEmptyString(str: memInfo.account){
            accountNameStr = memInfo.account
        }else{
            accountNameStr = "暂无名称"
        }
        accountTV.text = accountNameStr
        if !isEmptyString(str: memInfo.balance){
            leftMoneyName = "余额:\(memInfo.balance)"
        }else{
            leftMoneyName = "0"
        }
        balanceTV.text = String.init(format: "%@元",  leftMoneyName)
    }
    
    // 遍历数组,取出公告赋值
    private func forArrToAlert(notices: Array<NoticeResult>) {
        
        var noticesP = notices
        noticesP = noticesP.sorted { (noticesP1, noticesP2) -> Bool in
            return noticesP1.sortNum < noticesP2.sortNum
        }
        
        var models = [NoticeResult]()
        for index in 0..<noticesP.count {
            let model = noticesP[index]
            if model.isDeposit {
                models.append(model)
            }
        }
        
        if models.count > 0 {
            let weblistView = WebviewList.init(noticeResuls: models)
            weblistView.show()
        }
    }
    
    
    
    private func showAnnounce(controller:BaseController) {
        if YiboPreference.isShouldAlert_isAll() == "" {
            YiboPreference.setAlert_isAll(value: "on" as AnyObject)
        }
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if !shouldShowNoticeWIndow() {
                return
            }
        }
        //获取公告弹窗内容
        controller.request(frontDialog: false, url:ACQURIE_NOTICE_POP_URL,params:["code":19],
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            
                            if let result = NoticeResultWraper.deserialize(from: resultJson){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                PopupAlertListView(resultJson: resultJson,controller:self,not:"",anObject:nil,from: 2)
                            }
        })
    }
}

extension NewChargeMoneyV2Controller{
    
    func syncAccount() -> Void {
        request(frontDialog: true,method: .get,loadTextStr: "同步帐号信息...",url:MEMINFO_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        return
                    }
                    if let result = MemInfoWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let memInfo = result.content{
                                //赋值帐户名及余额
                                self.updateAccount(memInfo: memInfo)
                                self.syncNewPayMethod()
//                                self.syncPayMethod()
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "同步帐户信息失败"))
                            }
                            if result.code == 0  || result.code == -1{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    func updatePayListAfterSyncMethods(fast:[FastPayInfo]){
        if self.payMethods == nil{
            return
        }
        self.datas.removeAll()
        if !self.payMethods.online.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.topup_zaixianzhifu","text":"在线支付","code":""])
            }else{
                self.datas.append(["img":"topup_wap_online","text":"在线支付","code":""])
            }
        }
        if !self.payMethods.bank.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.topup_yinhangkazhifu","text":"银行卡转账支付","code":""])
            }else{
                self.datas.append(["img":"topup_wap_bankcard","text":"银行卡转账支付","code":""])
            }
        }
        
        let fastPayInfos = fast
        for (_, model) in fastPayInfos.enumerated() {
            //"微信|支付宝"
            if model.payGroup.code == "WECHATANDALIPAY" {
                if !self.payMethods.weixin_alipay.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.topup_wecharAirpay","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)","imgURL":""])
                    }else{
                        self.datas.append(["img":"topup_wap_weixin_alipay","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //美团
            }else if model.payGroup.code == "MEITUANPAY" {
                if !self.payMethods.fast_meituan.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.meituan","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_wap_meituan","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //qq
            }else if model.payGroup.code == "TXQQPAY" {
                if !self.payMethods.fast_qq.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.qqfu","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_wap_qq","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //云闪付
            }else if model.payGroup.code == "YUNPAY" {
                if !self.payMethods.fast_ysf.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.yunshanfu","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_wap_ysf","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //支付宝
            }else if model.payGroup.code == "ALIPAY" {
                if !self.payMethods.fast2.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.topup_zhifubaozhifu","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_wap_zfb","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //微信
            }else if model.payGroup.code == "WECHAT" {
                if !self.payMethods.fast.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.topup_weixinzhifu","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_wap_weixin","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //USDT
            }else if model.payGroup.code == "USDT" {
                if !self.payMethods.USDT.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.topup_USDTzhifu","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_USDT","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
                //PAYPAL
            }else if model.payGroup.code == "PAYPAL" {
                if !self.payMethods.PAYPAL.isEmpty{
                    if !use_wap_icons{
                        self.datas.append(["img":"MemberPage.Charge.topup_PAYPALzhifu","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }else{
                        self.datas.append(["img":"topup_PAYPAL","text":"\(model.payGroup.payName)","code":"\(model.payGroup.code)"])
                    }
                }
            }
        }
        
        self.tableView.reloadData()
    }
    
    //MARK: 格式化 PayMethodV2ResultV2 -> PayMethodResult
    private func formatPayMethodResult(result:PayMethodV2Result) {
        self.payMethods = PayMethodResult()
        self.payMethods.serverEndTime = result.serverEndTime
        self.payMethods.serverStartTime = result.serverStartTime
        self.payMethods.minMoney = result.minMoney
        self.payMethods.bank = result.bank
        self.payMethods.online = result.online
        
        var fastPayInfos = result.fast
        fastPayInfos = fastPayInfos.sorted {(subDatasP1,subDatasP2) -> Bool in
            return subDatasP1.payGroup.sortNo < subDatasP2.payGroup.sortNo
        }
        
        for (_, model) in fastPayInfos.enumerated() {
            //"微信|支付宝"
            if model.payGroup.code == "WECHATANDALIPAY" {
                self.payMethods.weixin_alipay = model.payChildren
                //美团
            }else if model.payGroup.code == "MEITUANPAY" {
                self.payMethods.fast_meituan = model.payChildren
                //qq
            }else if model.payGroup.code == "TXQQPAY" {
                self.payMethods.fast_qq = model.payChildren
                //云闪付
            }else if model.payGroup.code == "YUNPAY" {
                self.payMethods.fast_ysf = model.payChildren
                //支付宝
            }else if model.payGroup.code == "ALIPAY" {
                self.payMethods.fast2 = model.payChildren
                //微信
            }else if model.payGroup.code == "WECHAT" {
                self.payMethods.fast = model.payChildren
                //USDT
            }else if model.payGroup.code == "USDT" {
                self.payMethods.USDT = model.payChildren
                //PAYPAL
            }else if model.payGroup.code == "PAYPAL" {
                self.payMethods.PAYPAL = model.payChildren
            }
        }
        
        self.updatePayListAfterSyncMethods(fast: fastPayInfos)
    }
    
    func syncNewPayMethod() -> Void {
        request(frontDialog: true,method: .get,loadTextStr: "获取充值方式中...",url:GET_PAY_V2_METHODS_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        return
                    }
                    if let result = PayMethodV2Wraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let payMethods = result.content{
                                self.formatPayMethodResult(result: payMethods)
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取充值方式失败"))
                            }
                            if result.code == 0 || result.code == -1{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
}

extension NewChargeMoneyV2Controller : UITableViewDelegate,UITableViewDataSource{
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    
//        return tableView.estimatedRowHeight
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "payCell") as? PayListCell  else {
            fatalError("The dequeued cell is not an instance of PayListCell.")
        }
        
        let model = self.datas[indexPath.row]
        cell.setModel(model: model,theme: !use_wap_icons)
        return cell
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.separatorInset = UIEdgeInsets.zero
//        cell.layoutMargins = UIEdgeInsets.zero
//    }
//    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let row = indexPath.row
        let data = self.datas[row]
        let name = data["text"]
        let code = data["code"]
        
        let version = switch_payVersion()
        if version == "V1" || version == "V3"{
            chooseChargeViewV1(name: name, code: code, version: version)
        }else if version == "V2" || version ==  "V4"{
            chooseChargeViewV2(name: name,payIcon: data["img"]!,code: code,version: version)
        }
    }
    
    private func chooseChargeViewV2(name: String?,payIcon: String,code: String?,version:String) {
        if let methods = self.payMethods{
            //wap版
            //先通道
            if name == "在线支付"{
                if version == "V2" {
                    goOnlinePayControllerV2(onlines: methods.online)
                }else if version == "V4"{
                    //先原生
                    goOnlinePayMethodV2Controller()
                }
                
            }else if code == "WECHAT"{
//                goWeixinPayController(weixins: methods.fast)
                goAlipayPayControllerV2(alipays: methods.fast,title: name!,payIcon: payIcon)
            }else if code == "ALIPAY"{
                goAlipayPayControllerV2(alipays: methods.fast2,title: name!,payIcon: payIcon)
            }else if name == "银行卡转账支付"{
                goBankPayControllerV2(banks: methods.bank)
            }else if code == "TXQQPAY"{
                goAlipayPayControllerV2(alipays: methods.fast_qq,title: name!,payIcon: payIcon)
            }else if code == "YUNPAY"{
                goAlipayPayControllerV2(alipays: methods.fast_ysf,title: name!,payIcon: payIcon)
            }else if code == "MEITUANPAY"{
                goAlipayPayControllerV2(alipays: methods.fast_meituan,title: name!,payIcon: payIcon)
            }else if code == "WECHATANDALIPAY"{
                goAlipayPayControllerV2(alipays: methods.weixin_alipay,title: name!,payIcon: payIcon)
            }else if code == "USDT"{
                goAlipayPayControllerV2(alipays: methods.USDT,title: name!,payIcon: payIcon)
            }else if code == "PAYPAL"{
                goAlipayPayControllerV2(alipays: methods.PAYPAL,title: name!,payIcon: payIcon)
            }
        }
    }
    
    private func chooseChargeViewV1(name: String?,code: String?,version:String) {
        if let methods = self.payMethods{
            if name == "在线支付"{
                //原生版
                //先通道版
                if version == "V1" {
                    goOnlinePayController(onlines: methods.online)
                }else if version == "V3" {
                    //先方法版
                    goOnlinePayMethodV1Controller()
                }
                
            }else if code == "WECHAT"{
                goWeixinPayController(weixins: methods.fast)
            }else if code == "ALIPAY"{
                goAlipayPayController(alipays: methods.fast2,title: name!)
            }else if name == "银行卡转账支付"{
                goBankPayController(banks: methods.bank)
            }else if code == "TXQQPAY"{
                goAlipayPayController(alipays: methods.fast_qq,title: name!)
            }else if code == "YUNPAY"{
                goAlipayPayController(alipays: methods.fast_ysf,title: name!)
            }else if code == "MEITUANPAY"{
                goAlipayPayController(alipays: methods.fast_meituan,title: name!)
            }else if code == "WECHATANDALIPAY"{
                goAlipayPayController(alipays: methods.weixin_alipay,title: name!)
            }else if code == "USDT"{
                goAlipayPayController(alipays: methods.USDT,title: name!)
            }else if code == "PAYPAL"{
                goAlipayPayController(alipays: methods.PAYPAL,title: name!)
            }
        }
    }
    
    private func goWeixinPayController(weixins:[FastPay]){
        let vc = UIStoryboard(name: "fast_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "fast_pay_info") as! FastPayInfoController
        vc.fasts = weixins
        vc.is_wx = true
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goAlipayPayController(alipays:[FastPay],title: String){
        let vc = UIStoryboard(name: "fast_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "fast_pay_info") as! FastPayInfoController
        vc.payFunction = title
        vc.fasts = alipays
        vc.is_wx = false
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goAlipayPayControllerV2(alipays:[FastPay],title: String,payIcon: String){
        let vc = UIStoryboard(name: "fast_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "fast_pay_info_v2") as! FastPayInfoControllerV2
        vc.payFunction = title
        vc.fasts = alipays
        vc.payIcon = payIcon
        vc.is_wx = title == "微信支付"
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }

    private func goBankPayController(banks:[BankPay]){
        let vc = UIStoryboard(name: "bank_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "bank_pay_info") as! BankPayInfoController
        let config = getSystemConfigFromJson()
        vc.banks = banks
        vc.meminfo = meminfo
        vc.propmtString = didHtml(strhtml: (config?.content.pay_tips_deposit_general)!).trimmingCharacters(in: .whitespacesAndNewlines)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goBankPayControllerV2(banks:[BankPay]){
        let vc = UIStoryboard(name: "bank_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "bank_pay_info_v2") as! BankPayInfoControllerV2
        vc.banks = banks
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: 先通道-原生版
    private func goOnlinePayController(onlines:[OnlinePay]){
        let vc = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay") as! OnlinePayInfoController
        vc.onlines = onlines
        vc.meminfo = meminfo
//        vc.propmtString = didHtml(strhtml: (config?.content.pay_tips_deposit_general)!).trimmingCharacters(in: .whitespacesAndNewlines)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 先通道-wap版
    private func goOnlinePayControllerV2(onlines:[OnlinePay]){
        let vcP = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v3") as! OnlineNewPayInfoControllerV2
        vcP.loadViewIfNeeded()
        vcP.onlines = onlines
        vcP.meminfo = meminfo
        self.navigationController?.pushViewController(vcP, animated: true)
    }
    
    //MARK: 先方法版-原生版
    private func goOnlinePayMethodV1Controller() {
        let vc = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v2") as! OnlineMethodPayV1Controller
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 先方法版-wap版
    private func goOnlinePayMethodV2Controller() {
        let vcP = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v4") as! OnlineMethodPayV2Controller
        self.navigationController?.pushViewController(vcP, animated: true)
    }
    
}

extension NewChargeMoneyV2Controller{
    
    func didHtml(strhtml:String)->String{
        let str = strhtml.replacingOccurrences(of:"</span>", with:"")
        var strhtml1="";
        var text:NSString?
        let scanner = Scanner(string: str)
        while scanner.isAtEnd == false {
            scanner.scanUpTo("<", into: nil)
            scanner.scanUpTo(">", into: &text)
            strhtml1 = strhtml.replacingOccurrences(of:"\(text == nil ? "" : text!)>", with: "")
        }
        let str1 = strhtml1.replacingOccurrences(of:"</span>", with:"")
        let str2 = str1.replacingOccurrences(of:" ", with:"")
        return str2
    }
}








