//
//  FeeConvertController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/2/1.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher
//额度转换
class FeeConvertController: BaseMainController {
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerBgImage: UIImageView!
    @IBOutlet weak var balanceUI:UILabel!
    @IBOutlet weak var tableView:UITableView!
    /**提示*/
    @IBOutlet weak var tipsLabel: UILabel!
    var isAttachTabBar = false
    var datas:[OtherPlay] = []
    var convertWindow:ConvertWindow!
    var _activityIndeView:UIActivityIndicatorView!
    
    let REAL_CODE = "shaba";

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        if #available(iOS 11.0, *) {} else { self.automaticallyAdjustsScrollViewInsets = false}
        self.title = "额度转换"
        
        if !isAttachTabBar && self.navigationController?.children.count != 1{
          self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        }
        
        if isAttachTabBar && self.navigationController?.children.count == 1 {
            self.adjustRightBtn()
        }
        
        _activityIndeView = activityIndeView()
        loadDatas()
        accountWeb()
        headerBgImage.theme_image = "General.personalHeaderBg"
        
        if YiboPreference.getCACHEAVATARdata().count >= 1 {
            headerImage.image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
        }else{
            headerImage.theme_image = "General.placeHeader"
        }
       
        headerImage.layer.cornerRadius = 40.0
        headerImage.layer.masksToBounds = true
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //额度转换开关
        if  getSystemConfigFromJson()?.content.third_auto_exchange == "off"{
            tipsLabel.isHidden = true
        }else{
            tipsLabel.isHidden = false
        }
    }
    
    override func adjustRightBtn() -> Void {
        if isAttachTabBar && self.navigationController?.children.count == 1 {
            super.adjustRightBtn()
            if YiboPreference.getLoginStatus(){
                self.navigationItem.rightBarButtonItems?.removeAll()
                let menuBtn = UIBarButtonItem.init(image: UIImage.init(named: "icon_menu"), style: .plain, target: self, action: #selector(BaseMainController.actionMenu))
                self.navigationItem.rightBarButtonItems = [menuBtn]
            }
        }
    }
    
    func loadDatas() -> Void {
        request(frontDialog: true,method: .get, loadTextStr:"正在获取...",url: REAL_CONVERT_DATA_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result =  OtherPlayWrapper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }
                            if let content = result.content{
                                self.datas.removeAll()
                                self.datas = self.datas + content
                                self.tableView.reloadData()
                            }
                            //开始异步获取各游戏的余额
                            self.getBalances(datas: self.datas);
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                            //所以此接口当code == 0时表示帐号被踢，或登录超时
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    //获取游戏余额
    func getBalance(gameType:String,url:String) -> Void {
        
        var url = ""
        if gameType == REAL_CODE{
            url = SBSPORT_BALANCE_URL
        }else{
            url = REAL_GAME_BALANCE_URL
        }
    
        request(frontDialog: false,method: .post,url: url,params:["type":gameType],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
            self._activityIndeView.stopAnimating()
            if isEmptyString(str: resultJson){
                return
            }
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "同步余额失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            let data = resultJson.data(using: String.Encoding.utf8, allowLossyConversion: true)
            //把Data对象转换回JSON对象
            let json = try? JSONSerialization.jsonObject(with: data!,options:.allowFragments) as! [String: Any]
            if !JSONSerialization.isValidJSONObject(json){
                return
            }
            if (json?.keys.contains("success"))!{
                let ok = json!["success"] as! Bool
                if ok{
                    if (json?.keys.contains("balance"))!{
                        for index in 0...self.datas.count-1{
                            let play = self.datas[index]
                            if play.gameType == gameType{
                                let m = json!["balance"] as! NSNumber
                                play.balance = m.floatValue
                                self.datas[index] = play
                                self.tableView.reloadData()
                                break
                            }
                        }
                    }else if (json?.keys.contains("money"))!{
                        for index in 0...self.datas.count-1{
                            let play = self.datas[index]
                            if play.gameType == gameType{
                                let m = json!["money"] as! NSNumber
                                play.balance = m.floatValue
                                self.datas[index] = play
                                self.tableView.reloadData()
                                break
                            }
                        }
                    }
                }else{
                    if (json?.keys.contains("msg"))!{
//                                showToast(view: self.view, txt: json!["msg"] as! String)
                    }else{
//                                showToast(view: self.view, txt: "额度转换失败")
                    }
                }
            }
        })
    }
    
    func activityIndeView() -> UIActivityIndicatorView {
        if _activityIndeView == nil{
            _activityIndeView = UIActivityIndicatorView.init(style: .white)
            _activityIndeView.hidesWhenStopped = true
        }
        return _activityIndeView
    }
    
    //获取每种游戏余额
    func getBalances(datas:[OtherPlay]) -> Void {
        if datas.isEmpty{
            return
        }
        for play in datas{
            self._activityIndeView.startAnimating()
            var gameType = ""
            if !play.gameType.isEmpty {
                gameType = play.gameType
            }else {
                gameType = play.playCode
                play.gameType = gameType
            }
            
            getBalance(gameType: gameType, url: play.feeConvertUrl)
        }
    }
    
    //点击进入游戏
    //MARK:进入游戏
    @objc func clickEnterGame(btn:UIButton) -> Void {
        let moneyItem = self.datas[btn.tag]
      
        let playCode = moneyItem.playCode
        let gameName = moneyItem.title
      
//        if getSystemConfigFromJson()?.content.third_auto_exchange == "off" {
////            let popView = ChessAndCardPayPopView.init(frame: UIScreen.main.bounds, dcode: playCode)
//           let popView = ChessAndCardPayPopView.init(frame: UIScreen.main.bounds, dcode: playCode, gameName: gameName)
//
//            popView.assController = self
//            popView.gameCode = moneyItem.playCode
//            popView.title = gameName
//            popView.delegate = self
//
//            UIApplication.shared.keyWindow?.addSubview(popView)
//        }else {
            if moneyItem.isListGame == 1 {
                //有子类游戏
                 actionOpenGameList(controller: self, gameCode: playCode, title: gameName, otherPlay: moneyItem)
            }else{
                forwardRealWeb(controller: self, forwardUrl: moneyItem.forwardUrl, titleName: gameName)
            }
           
//        }
    }
   
    
    func accountWeb() -> Void {
        //帐户相关信息
        request(frontDialog: false, url:MEMINFO_URL,
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            if let result = MemInfoWraper.deserialize(from: resultJson){
                                if result.success{
                                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                                    if let memInfo = result.content{
                                        self.updateAccount(memInfo:memInfo);
                                    }
                                }
                            }
        })
    }
    
    func updateAccount(memInfo:Meminfo) -> Void {
        var leftMoneyName = "0"
        if !isEmptyString(str: memInfo.balance){
            leftMoneyName = "\(memInfo.balance)"
        }else{
            leftMoneyName = "0"
        }
        balanceUI.text = String.init(format: "账户余额:%@元",  leftMoneyName)
    }
    
    func actionConvert(from:String,to:String,money:String,fromSys:Bool,url:String) -> Void {
        
        var limitUrl = url

        let arrayArr = limitUrl.components(separatedBy: "?")
        limitUrl = arrayArr.first!
        
        let params = ["changeFrom":from,"changeTo":to,"money":money] as Dictionary<String, AnyObject>
        
        request(frontDialog: true,method: .post, loadTextStr:"转换中...", url:limitUrl,
                params: params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "额度转换失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            if isEmptyString(str: resultJson){
                return
            }
            if resultJson.starts(with: "<html>"){
                showToast(view: self.view, txt: "额度转换失败,联系客服")
                return
            }
            //把Data对象转换回JSON对象
            let data = resultJson.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let json = try? JSONSerialization.jsonObject(with: data!,options:.allowFragments) as! [String: Any]
            if (json?.keys.contains("success"))!{
                let ok = json!["success"] as! Bool
                if ok{
                    showToast(view: self.view, txt: "额度转换成功")
                    self.getBalance(gameType: from == "sys" ? to : from, url: "")
                    self.accountWeb()
                    self.view.endEditing(true)
                   
                }else{
                    if (json?.keys.contains("msg"))!{
                        showToast(view: self.view, txt: json!["msg"] as! String)
                    }else{
                        showToast(view: self.view, txt: "额度转换失败")
                    }
                }
            }
        })
    }
}
//MARK:TableViewDelegate
extension FeeConvertController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //点击列表项弹也转换列表
        let data = self.datas[indexPath.row]
        let url = data.feeConvertUrl
        let gameType = !data.gameType.isEmpty ? data.gameType : data.playCode
        
        showSheetConvertDialog(code:gameType,title: data.title, url: url)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "convertCell") as? ConvertListCell  else {
            fatalError("The dequeued cell is not an instance of ConvertListCell.")
        }
        let data = self.datas[indexPath.row]
        cell.enterBtn.tag = indexPath.row
        if let imageURL = URL(string: BASE_URL + PORT + data.imgUrl) {
            cell.iconImage.kf.setImage(with: ImageResource(downloadURL: imageURL))
        }
        cell.gameNameUI.text = data.title
        cell.moneyUI.text = String.init(format: "%.2f元", data.balance)
        cell.enterBtn.addTarget(self, action: #selector(clickEnterGame(btn:)), for: UIControl.Event.touchUpInside)
        return cell
    }
}
extension FeeConvertController:ChessAndCardPayPopViewDelegate,ConvertDelegate{
   
    // 新增填充提示 代理回执
    func clickOpenGameList(gameCode: String, title: String) {
        actionOpenGameList(controller: self, gameCode: gameCode, title: title, otherPlay: nil)
    }
    
    func showConvertWindow(code:String,title:String,sin:Bool,url:String) -> Void {
        if convertWindow == nil{
            convertWindow = Bundle.main.loadNibNamed("convert_window", owner: nil, options: nil)?.first as! ConvertWindow
        }
        convertWindow.convertDelegate = self
        convertWindow.setData(code:code,title:title,convertIn: sin,url:url)
        convertWindow.show()
    }
    
    func convertInOut(sin:Bool,code:String,title:String,url:String) -> Void {
        showConvertWindow(code: code, title: title,sin:sin, url: url)
    }
    
    func showSheetConvertDialog(code:String,title:String,url:String) -> Void {
        let alert = UIAlertController.init(title: "额度转换", message: nil, preferredStyle: .actionSheet)
        let convertInAction = UIAlertAction.init(title: "转入", style: .default, handler: {(action:UIAlertAction) in
            self.convertInOut(sin:true,code:code,title: title,url:url)
        })
        let convertOutAction = UIAlertAction.init(title: "转出", style: .default, handler: {(action:UIAlertAction) in
            self.convertInOut(sin:false,code:code,title: title,url:url)
        })
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        
        alert.addAction(convertInAction)
        alert.addAction(convertOutAction)
        alert.addAction(cancelAction)
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect.init(x: kScreenWidth/4, y: kScreenHeight, width: kScreenWidth/2, height: 300)
        }
        self.present(alert,animated: true,completion: nil)
    }
    
    
    func onConvert(code: String, fromSys: Bool, money: String, url: String) {
        //开始发起转换
        if isEmptyString(str: money){
            showToast(view: self.view, txt: "请输入金额")
            return
        }
        if !isPurnInt(string: money){
            showToast(view: self.view, txt: "金额须为整数")
            return
        }
        if code == REAL_CODE{
            showToast(view: self.view, txt: "暂未开放，敬请期待")
            return
        }
        if (fromSys) {
            actionConvert(from: "sys", to: code, money: money,fromSys:true, url: url)
        }else{
            actionConvert(from: code, to: "sys", money: money,fromSys:false, url: url)
        }
    }

}
