//
//  NormalButtonCollectionCell.swift
//  gameplay
//
//  Created by admin on 2018/7/20.
//  Copyright © 2018 yibo. All rights reserved.
//

import UIKit

class NormalButtonCollectionCell: UICollectionViewCell {
//    @IBOutlet weak var normalButton: UIButton!
    
    @IBOutlet weak var normaltext: UILabel!
    var normalV = UIImageView()
//    var normalButtonIS = true
    override func awakeFromNib() {
        super.awakeFromNib()
//        setupNoPictureAlphaBgView(view: self,alpha: 0.1)
//        self.layer.cornerRadius = 3.0
//        self.layer.masksToBounds = true
//        self.normalButton.layer.cornerRadius = 3.0
//
//        normalButton.imageView?.contentMode = .scaleToFill
//        normalButton.imageView?.clipsToBounds = true

        normaltext.font = UIFont.systemFont(ofSize: 15)

        normaltext.clipsToBounds = true
        //        // 圆角的半径
        normaltext.layer.cornerRadius = 5
        //        // 边框线宽
        normaltext.layer.borderWidth = 2

        normaltext.addSubview(normalV)
        normalV.snp.makeConstraints { (make) in
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(15)
            make.width.equalTo(25)
        }
        normalV.image = UIImage.init(named:"icon_triangle_select")
        
      
        
    }
}
