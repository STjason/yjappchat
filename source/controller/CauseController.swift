//
//  CauseController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/10/12.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class CauseController: BaseController {

        
    @IBOutlet weak var cause:UILabel!
    var causeStr:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "网站维护"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
        cause.text = !isEmptyString(str: causeStr) ? causeStr : "网站正在维护中"
    }

}
