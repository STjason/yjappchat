//
//  RatebackViewController.swift
//  gameplay
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class RatebackViewController: BaseController {

    var isSelect = false
    var isProxy = false
    var accountId:Int64 = 0
    var gameDatas = [[FakeBankBean]]()
    var ratebackData:AllRateBack!
    var userName:String = ""
    
    var lotteryRebate = "";//选中的返点
    var sportRebate = "";//选中的体育返点
    var shabaRebate = "";//选中的沙巴体育返点
    var realRebate = "";//选中的真人返点
    var eqameRebate = "";//选中的电子返点
    var chessRebate = "";
    var gamingRebate = ""
    var gameFishingRebate = ""
    
    var notShowCodes = [String]()
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    
    @IBAction func saveAction() {
        saveRateback()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "返点设定"
        setupView()
        loadRatebackDatas(showDialog: true)
    }
    
    private func saveRateback() {
        
        var patrams = [String:Any]()
        for index in 0..<self.gameDatas[1].count
        {
            let modelP = self.gameDatas[1][index]
            if modelP.code == "tyfs" // 体育返点
            {
                if isEmptyString(str: sportRebate) {patrams["sportRebate"] = ("0")}else
                if sportRebate.contains("%") || sportRebate.contains("‰") ||  sportRebate.contains("--") {
                    patrams["sportRebate"] = getValueWithLabel(type: 3, label: sportRebate)
                }else {
                    patrams["sportRebate"] = (sportRebate)
                }
                    
            }else if modelP.code == "sbfs" // 沙巴体育返点
            {
                if isEmptyString(str: shabaRebate) {patrams["shabaRebate"] = ("0")}else
                if shabaRebate.contains("%") || shabaRebate.contains("‰") ||  shabaRebate.contains("--") {
                    patrams["shabaRebate"] = getValueWithLabel(type: 1, label: shabaRebate)
                }else {
                    patrams["shabaRebate"] = (shabaRebate)
                }
                    
            }else if modelP.code == "zrfs" // 真人返点
            {
                if isEmptyString(str: realRebate) {patrams["realRebate"] = ("0")}else
                if realRebate.contains("%") || realRebate.contains("‰") ||  realRebate.contains("--") {
                    patrams["realRebate"] = getValueWithLabel(type: 4, label: realRebate)
                }else {
                    patrams["realRebate"] = (realRebate)
                }
                    
            }else if modelP.code == "dzfs" // 电子返点
            {

                if isEmptyString(str: eqameRebate) {patrams["egameRebate"] = ("0")}else
                if eqameRebate.contains("%") || eqameRebate.contains("‰") ||  eqameRebate.contains("--") {
                    patrams["egameRebate"] = getValueWithLabel(type: 5, label: eqameRebate)
                }else {
                    patrams["egameRebate"] = (eqameRebate)
                }
                    
            }else if modelP.code == "qpfs" // 棋牌返点
            {
                
                if isEmptyString(str: chessRebate) {patrams["chessRebate"] = ("0")}else
                    if chessRebate.contains("%") || chessRebate.contains("‰") ||  chessRebate.contains("--") {
                        patrams["chessRebate"] = getValueWithLabel(type: 6, label: chessRebate)
                    }else {
                        patrams["chessRebate"] = (chessRebate)
                }
                
            }else if modelP.code == "cpfs" // 彩票返点
            {
                if isEmptyString(str: lotteryRebate) {patrams["kickback"] = ("0")}else
                if lotteryRebate.contains("%") || lotteryRebate.contains("‰") ||  lotteryRebate.contains("--") {
                    patrams["kickback"] = getValueWithLabel(type: 2, label: lotteryRebate)
                }else {
                    patrams["kickback"] = (lotteryRebate)
                }
            }else if modelP.code == "djfs" // 电竞返点
            {
                if isEmptyString(str: gamingRebate) {patrams["esportRebate"] = ("0")}else
                    if gamingRebate.contains("%") || gamingRebate.contains("‰") ||  gamingRebate.contains("--") {
                        patrams["esportRebate"] = getValueWithLabel(type: 7, label: gamingRebate)
                    }else {
                        patrams["esportRebate"] = (gamingRebate)
                }
            }else if modelP.code == "pyfs" // 捕鱼返点
            {
                if isEmptyString(str: gameFishingRebate) {patrams["fishingRebate"] = ("0")}else
                    if gameFishingRebate.contains("%") || gameFishingRebate.contains("‰") ||  gameFishingRebate.contains("--") {
                        patrams["fishingRebate"] = getValueWithLabel(type: 8, label: gameFishingRebate)
                    }else {
                        patrams["fishingRebate"] = (gameFishingRebate)
                }
            }
        }
        
        patrams["accountId"] = accountId
        
        self.request(frontDialog: true, method: .post,url: KICKBACKRESET,params:patrams,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = CancelOrderWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                if result.content{
                                    showToast(view: self.view, txt: "设置返点成功")
                                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                                        self.navigationController?.popViewController(animated: true)
                                    })
                                }else{
                                    showToast(view: self.view, txt: "设置返点失败")
                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    showToast(view: self.view, txt: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                                }
                                if result.code == 0{
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
        })
    }
    
    private func showRateBackListDialog(row:Int,code:String,sources:[RebateBean],label:String){
        if sources.isEmpty{
            showToast(view: self.view, txt: "没有返点数据，无法选择，请联系客服")
            return
        }
        var dataNames:[String] = []
        var selectedIndex = 0
        
        var titleP = ""
        for index in 0...sources.count-1{
            let item = sources[index]
            if code == "cpfs"{
                titleP = "请设置奖金"
//                if item.value == self.ratebackData.lot_kickback {
                if item.label == label {
                    selectedIndex = index
                }
            }else if code == "tyfs"{
                titleP = "请设置体育返点"
//                if item.value == self.ratebackData.current_other_kickback.sportScale{
                if item.label == label{
                    selectedIndex = index
                }
            }else if code == "sbfs"{
                titleP = "请设置第三方体育返点"
//                if item.value == self.ratebackData.current_other_kickback.shabaSportScale{
                if item.label == label{
                    selectedIndex = index
                }
            }else if code == "zrfs"{
                titleP = "请设置真人返点"
//                if item.value == self.ratebackData.current_other_kickback.realScale{
                if item.label == label {
                    selectedIndex = index
                }
            }else if code == "dzfs"{
                titleP = "请设置电子返点"
//                if item.value == self.ratebackData.current_other_kickback.egameScale{
                if item.label == label{
                    selectedIndex = index
                }
            }else if code == "qpfs"{
                titleP = "请设置棋牌返点"
                //                if item.value == self.ratebackData.current_other_kickback.egameScale{
                if item.label == label{
                    selectedIndex = index
                }
            }else if code == "djfs"{
                titleP = "请设置电竞返点"
                //                if item.value == self.ratebackData.current_other_kickback.egameScale{
                if item.label == label{
                    selectedIndex = index
                }
            }else if code == "pyfs"{
                titleP = "请设置捕鱼返点"
                //                if item.value == self.ratebackData.current_other_kickback.egameScale{
                if item.label == label{
                    selectedIndex = index
                }
            }
            
            dataNames.append(String.init(format: "%@", item.label))
        }
        
        let selectedView = LennySelectView(dataSource: dataNames, viewTitle: titleP)
        selectedView.selectedIndex = selectedIndex
        
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            
            selectedView.selectedIndex = index
            self?.gameDatas[1][row].value = selectedView.kLenny_InsideDataSource[index]
            self?.mainTableView.reloadData()
            if code == "cpfs"{
                self?.lotteryRebate = sources[index].value
            }else if code == "tyfs"{
                self?.sportRebate = sources[index].value
            }else if code == "sbfs"{
                self?.shabaRebate = sources[index].value
            }else if code == "zrfs"{
                self?.realRebate = sources[index].value
            }else if code == "dzfs"{
                self?.eqameRebate = sources[index].value
            }else if code == "qpfs"{
                self?.chessRebate = sources[index].value
            }else if code == "djfs"{
                self?.gamingRebate = sources[index].value
            }else if code == "pyfs"{
                self?.gameFishingRebate = sources[index].value
            }
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
        }
    }
    
    private func setupData(allData: AllRateBack) {
        
        var data1:[FakeBankBean] = []
        
        let item1 = FakeBankBean()
        item1.text = "账号类型: "
        item1.value = self.isProxy ? "代理" : "会员"
        
        
        let item2 = FakeBankBean()
        item2.text = "账户名: "
        item2.value = userName
        
        data1.append(item2)
        data1.append(item1)
        
        self.gameDatas.append(data1)
        
        var data2:[FakeBankBean] = []
        
        if getSwitch_lottery() {
            let item21 = FakeBankBean()
            item21.text = "设置奖金:"
            lotteryRebate = getLabelWithValue(type: 2, value: allData.lot_kickback)
            item21.value = getLabelWithValue(type: 2, value: allData.lot_kickback)
            item21.code = "cpfs"
            data2.append(item21)
        }
        
        let system = getSystemConfigFromJson()
        let sport = system?.content.onoff_sport_switch
        let sbsport = system?.content.onoff_sb_switch
        let zhenren = system?.content.onoff_zhen_ren_yu_le
        let game = system?.content.onoff_dian_zi_you_yi
        let chess = system?.content.switch_chess
        let gaming = system?.content.switch_esport
        let gameFishing = system?.content.switch_fishing
        
        if !isEmptyString(str: sport!) && sport == "on" && self.isProxy {
            let item22 = FakeBankBean()
            item22.text = "设置体育返点:"
            sportRebate = getLabelWithValue(type: 3, value: allData.current_other_kickback.sportScale)
            item22.value = getLabelWithValue(type: 3, value: allData.current_other_kickback.sportScale)
            item22.code = "tyfs"
            data2.append(item22)
        }
        
        if !isEmptyString(str: sbsport!) && sbsport == "on"  && self.isProxy{
            let item23 = FakeBankBean()
            item23.text = "设置第三方体育返点:"
            shabaRebate = getLabelWithValue(type: 1, value: allData.current_other_kickback.shabaSportScale)
            item23.value = getLabelWithValue(type: 1, value: allData.current_other_kickback.shabaSportScale)
            item23.code = "sbfs"
            data2.append(item23)
        }
        
        if !isEmptyString(str: zhenren!) && zhenren == "on"  && self.isProxy{
            let item24 = FakeBankBean()
            item24.text = "设置真人返点:"
            realRebate = getLabelWithValue(type: 4, value: allData.current_other_kickback.realScale)
            item24.value = getLabelWithValue(type: 4, value: allData.current_other_kickback.realScale)
            item24.code = "zrfs"
            data2.append(item24)
        }
        
        if !isEmptyString(str: game!) && game == "on"  && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置电子返点:"
            eqameRebate = getLabelWithValue(type: 5, value: allData.current_other_kickback.egameScale)
            item25.value = getLabelWithValue(type: 5, value: allData.current_other_kickback.egameScale)
            item25.code = "dzfs"
            data2.append(item25)
        }
        
        if !isEmptyString(str: chess!) && chess == "on"  && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置棋牌返点:"
            chessRebate = getLabelWithValue(type: 6, value: allData.current_other_kickback.chessScale)
            item25.value = getLabelWithValue(type: 6, value: allData.current_other_kickback.chessScale)
            item25.code = "qpfs"
            data2.append(item25)
        }
        if !isEmptyString(str: gaming!) && gaming == "on"  && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置电竞返点:"
            gamingRebate = getLabelWithValue(type: 7, value: allData.current_other_kickback.chessScale)
            item25.value = getLabelWithValue(type: 7, value: allData.current_other_kickback.chessScale)
            item25.code = "djfs"
            data2.append(item25)
        }
        if !isEmptyString(str: gameFishing!) && gameFishing == "on"  && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置捕鱼返点:"
            gameFishingRebate = getLabelWithValue(type: 8, value: allData.current_other_kickback.chessScale)
            item25.value = getLabelWithValue(type: 8, value: allData.current_other_kickback.chessScale)
            item25.code = "pyfs"
            data2.append(item25)
        }
        
        self.gameDatas.append(data2)
    }
    
    //MARK: 根据类型和 value ，获得 label
    /** type: 1 沙巴体育，2 彩票返点，3 体育，4 真人，5 电子,6 棋牌 */
    private func getLabelWithValue(type: Int,value: String) -> String{
        
        if value == "0" {return "0"}
        
        if type == 1 {
            for model in self.ratebackData.shabaArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 2{
            for model in self.ratebackData.lotteryArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 3 {
            for model in self.ratebackData.sportArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 4 {
            for model in self.ratebackData.realArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 5 {
            for model in self.ratebackData.egameArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 6 {
            for model in self.ratebackData.chessArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 7{
            for model in self.ratebackData.esportArray {
                if model.value == value {
                    return model.label
                }
            }
        }else if type == 8{
            for model in self.ratebackData.fishingArray {
                if model.value == value {
                    return model.label
                }
            }
        }
        
        return ""
    }
    
    //MARK: 根据类型和 value ，获得 label
    /** type: 1 沙巴体育，2 彩票返点，3 体育，4 真人，5 电子, 6 棋牌 */
    private func getValueWithLabel(type: Int,label: String) -> String{
        if type == 1 {
            for model in self.ratebackData.shabaArray {
                if model.label == label {
                    return model.value
                }
            }
        }else if type == 2{
            for model in self.ratebackData.lotteryArray {
                if model.label == label {
                    return model.value
                }
            }
        }else if type == 3 {
            for model in self.ratebackData.sportArray {
                if model.label == label {
                    return model.value
                }
            }
        }else if type == 4 {
            for model in self.ratebackData.realArray {
                if model.label == label {
                    return model.value
                }
            }
        }else if type == 5 {
            for model in self.ratebackData.egameArray {
                if model.label == label {
                    return model.value
                }
            }
        }else if type == 6 {
            for model in self.ratebackData.chessArray {
                if model.label == label {
                    return model.value
                }
            }
        }
        else if type == 7 {
            for model in self.ratebackData.esportArray {
                if model.label == label {
                    return model.value
                }
            }
        }else if type == 8 {
            for model in self.ratebackData.fishingArray {
                if model.label == label {
                    return model.value
                }
            }
        }
        
        return ""
    }
    
    private func setupView() {
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension RatebackViewController {
    
    func loadRatebackDatas(showDialog:Bool) -> Void {
        
        gameDatas = [[FakeBankBean]]()
        var params = [String:Any]()
        params["accountId"] = accountId
        
        request(frontDialog: showDialog, method:.get, loadTextStr:"获取数据中...", url:API_REGISTER_RATEBACK,params: params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = RatebackWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            self.ratebackData = result.content
                            self.setupData(allData: self.ratebackData)
                            self.mainTableView.reloadData()
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
}

extension RatebackViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameDatas[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.gameDatas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var showWhenZero = true
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                showWhenZero = config.content.show_rateback_when_zero == "on"
            }
            
        }
        
        if self.ratebackData == nil{
            return 0
        }
        let data = self.gameDatas[indexPath.section][indexPath.row]
        
        if data.code == "tyfs"{
            let array = self.ratebackData.sportArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero{
                notShowCodes.append("tyfs")
                return 0
            }
        }else if data.code == "sbfs"{
            let array = self.ratebackData.shabaArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero{
                notShowCodes.append("sbfs")
                return 0
            }
        }else if data.code == "zrfs"{
            let array = self.ratebackData.realArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero {
                notShowCodes.append("zrfs")
                return 0
            }
        }else if data.code == "dzfs"{
            let array = self.ratebackData.egameArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero {
                notShowCodes.append("dzfs")
                return 0
            }
        }else if data.code == "qpfs"{
            let array = self.ratebackData.chessArray
            if array.count == 0 && !showWhenZero {return 0} else
                if array.count == 1 && array[0].value == "0" && !showWhenZero {
                    notShowCodes.append("qpfs")
                    return 0
            }
        }else if data.code == "djfs"{
            let array = self.ratebackData.esportArray
            if array.count == 0 && !showWhenZero {return 0} else
                if array.count == 1 && array[0].value == "0" && !showWhenZero {
                    notShowCodes.append("qpfs")
                    return 0
            }
        }else if data.code == "pyfs"{
            let array = self.ratebackData.fishingArray
            if array.count == 0 && !showWhenZero {return 0} else
                if array.count == 1 && array[0].value == "0" && !showWhenZero {
                    notShowCodes.append("qpfs")
                    return 0
            }
        }
        
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ratebackSetupCell") as? RatebackSetupCell else {fatalError("dequeueReusableCell RatebackSetupCell failure")}
        
        cell.accessoryType = indexPath.section == 0 ? .none : .disclosureIndicator
        
        let modelP = self.gameDatas[indexPath.section][indexPath.row]
        if modelP.text.contains("设置奖金") {
            cell.configWithContents(title: modelP.text, contents: modelP.value == "0" ? "0%" : modelP.value)
        }else {
            cell.configWithContents(title: modelP.text, contents: modelP.value == "0" ? "0‰" : modelP.value)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{}else{
            
            if (isSelect == false) {
                isSelect = true
                
                if self.ratebackData == nil{
                    return
                }
                let data = self.gameDatas[indexPath.section][indexPath.row]
                let dataLabel = data.value
                
                if data.code == "cpfs"{
                    let array = self.ratebackData.lotteryArray
                    if array.count == 0 {
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row, code: data.code, sources: array,label:dataLabel)
                }else if data.code == "tyfs"{
                    let array = self.ratebackData.sportArray
                    if array.count == 0 {
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }else if data.code == "sbfs"{
                    let array = self.ratebackData.shabaArray
                    if array.count == 0 {
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }else if data.code == "zrfs"{
                    let array = self.ratebackData.realArray
                    if array.count == 0 {
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }else if data.code == "dzfs"{
                    let array = self.ratebackData.egameArray
                    if array.count == 0 {
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }else if data.code == "qpfs"{
                    let array = self.ratebackData.chessArray
                    if array.count == 0 {
                        showToast(view: self.view, txt: "没有棋牌反水数据")
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }else if data.code == "djfs"{
                    let array = self.ratebackData.esportArray
                    if array.count == 0 {
                        showToast(view: self.view, txt: "没有电竞反水数据")
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }else if data.code == "pyfs"{
                    let array = self.ratebackData.fishingArray
                    if array.count == 0 {
                        showToast(view: self.view, txt: "没有捕鱼反水数据")
                        self.isSelect = false
                        tableView.deselectRow(at: indexPath, animated: false)
                        return
                    }
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array,label:dataLabel)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.isSelect = false
                }
            }
            
        }
    }
    
}
