//
//  ScanCodeViewController.swift
//  QRCode
//
//  Created by gaofu on 16/9/9.
//  Copyright © 2016年 gaofu. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import MBProgressHUD

private let scanAnimationDuration = 3.0//扫描时长

class ScanCodeViewController: UIViewController
{
    
    //MARK: -
    //MARK: Global Variables
    
    @IBOutlet weak var scanPane: UIImageView!///扫描框
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var loadingDialog:MBProgressHUD?
    ///开光灯
    var lightOn = false
    ///允许执行 扫码接口调用
    var shouldExecute =  true
    
    //MARK: -
    //MARK: Lazy Components
    
    var idTxt = ""
    var keyTxt = ""
    var domainUrl = ""
    
    lazy var scanLine : UIImageView =
        {
            
            let scanLine = UIImageView()
            scanLine.frame = CGRect(x: 0, y: 0, width: self.scanPane.bounds.width, height: 3)
            scanLine.image = UIImage(named: "QRCode_ScanLine")
            
            return scanLine
            
    }()
    
    var scanSession :  AVCaptureSession?
    
    
    //MARK: -
    //MARK: Public Methods
    
    
    //MARK: -
    //MARK: Data Initialize
    
    
    //MARK: -
    //MARK: Life Cycle
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        self.title = "扫码下注"
        view.layoutIfNeeded()
        scanPane.addSubview(scanLine)
        setupScanSession()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        startScan()
        
    }
    
    
    //MARK: -
    //MARK: Interface Components
    
    func setupScanSession()
    {
        do
        {
            guard let device = AVCaptureDevice.default(for: .video) else{
                //,,,,,,
                return
            }
            //设置捕捉设备
            //            let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            //设置设备输入输出
            let input = try AVCaptureDeviceInput(device: device)
            let output = AVCaptureMetadataOutput()
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            //设置会话
            let  scanSession = AVCaptureSession()
            scanSession.canSetSessionPreset(AVCaptureSession.Preset.high)
            //                scanSession.canSetSessionPreset(AVCaptureSessionPresetHigh)
            
            if scanSession.canAddInput(input)
            {
                scanSession.addInput(input)
            }
            
            if scanSession.canAddOutput(output)
            {
                print("can add output -------")
                scanSession.addOutput(output)
            }
            
            //设置扫描类型(二维码和条形码)
            output.metadataObjectTypes = [
                AVMetadataObject.ObjectType.qr,
                AVMetadataObject.ObjectType.code39,
                AVMetadataObject.ObjectType.code128,
                AVMetadataObject.ObjectType.code39Mod43,
                AVMetadataObject.ObjectType.ean13,
                AVMetadataObject.ObjectType.ean8,
                AVMetadataObject.ObjectType.code93]
            
            //预览图层
            let scanPreviewLayer = AVCaptureVideoPreviewLayer(session:scanSession)
            scanPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            scanPreviewLayer.frame = view.layer.bounds
            
            view.layer.insertSublayer(scanPreviewLayer, at: 0)
            
            //设置扫描区域
            NotificationCenter.default.addObserver(forName: NSNotification.Name.AVCaptureInputPortFormatDescriptionDidChange, object: nil, queue: nil, using: { (noti) in
                output.rectOfInterest = (scanPreviewLayer.metadataOutputRectConverted(fromLayerRect: self.scanPane.frame))
            })
            //保存会话
            self.scanSession = scanSession
        }
        catch
        {
            //摄像头不可用
            Tool.confirm(title: "温馨提示", message: "摄像头不可用", controller: self)
            return
        }
        
    }
    
    //MARK: -
    //MARK: Target Action
    
    //闪光灯
    @IBAction func light(_ sender: UIButton)
    {
        
        lightOn = !lightOn
        sender.isSelected = lightOn
        turnTorchOn()
        
    }
    
    //相册
    @IBAction func photo()
    {
        
        Tool.shareTool.choosePicture(self, editor: true, options: .photoLibrary) {[weak self] (image) in
            
            self!.activityIndicatorView.startAnimating()
            
            DispatchQueue.global().async {
                let recognizeResult = image.recognizeQRCode()
                let result = (recognizeResult?.utf8CString.count)! > 0 ? recognizeResult : "无法识别"
                DispatchQueue.main.async {
                    self!.activityIndicatorView.stopAnimating()
                    Tool.confirm(title: "扫描结果", message: result, controller: self!)
                }
            }
        }
        
    }
    
    //MARK: -
    //MARK: Data Request
    
    
    //MARK: -
    //MARK: Private Methods
    
    //开始扫描
    fileprivate func startScan()
    {
        
        scanLine.layer.add(scanAnimation(), forKey: "scan")
        
        guard let scanSession = scanSession else { return }
        
        if !scanSession.isRunning
        {
            scanSession.startRunning()
        }
        
        
    }
    
    //扫描动画
    private func scanAnimation() -> CABasicAnimation
    {
        
        let startPoint = CGPoint(x: scanLine .center.x  , y: 1)
        let endPoint = CGPoint(x: scanLine.center.x, y: scanPane.bounds.size.height - 2)
        
        let translation = CABasicAnimation(keyPath: "position")
        translation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        translation.fromValue = NSValue(cgPoint: startPoint)
        translation.toValue = NSValue(cgPoint: endPoint)
        translation.duration = scanAnimationDuration
        translation.repeatCount = MAXFLOAT
        translation.autoreverses = true
        
        return translation
    }
    
    
    ///闪光灯
    private func turnTorchOn()
    {
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else
        {
            
            if lightOn
            {
                
                Tool.confirm(title: "温馨提示", message: "闪光灯不可用", controller: self)
                
            }
            return
        }
        
        if device.hasTorch
        {
            do
            {
                try device.lockForConfiguration()
                
                if lightOn && device.torchMode == .off
                {
                    device.torchMode = .on
                }
                
                if !lightOn && device.torchMode == .on
                {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            }
            catch{ }
            
        }
        
    }
    
    //MARK: -
    //MARK: Dealloc
    
    deinit
    {
        ///移除通知
        NotificationCenter.default.removeObserver(self)
        
    }
    
    
    
    func postAuthorize(result:String) -> Void {
        request(frontDialog: true,method: .post,loadTextStr: "正在处理...",url:QRCODE_BET_LOGIN,params: ["key":result],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if !isEmptyString(str: resultJson){
                            showToast(view: self.view, txt: resultJson)
                        }else{
                            showToast(view: self.view, txt: "获取失败")
                        }
                        return
                    }
                    if let result = QrcodeLoginWraper.deserialize(from: resultJson){
                        if result.success{
                            if let bean:QrcodeLoginBean = result.content{
                                YiboPreference.saveLoginStatus(value: true as AnyObject)
                                let lotData:LotteryData = LotteryData()
                                lotData.code = bean.lotCode
                                lotData.lotType = bean.lotType
                                lotData.lotVersion = Int(bean.lotVersion)!
                                YiboPreference.setAccountMode(value: bean.accountType as AnyObject)
//                                self.navigationController?.popViewController(animated: true)
                                chooseControllerWithController(controller: self, lottery: lotData);
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: "获取失败")
                            }
                        }
                    }else{
                        showToast(view: self.view, txt: "网络原因，获取失败")
                    }
        })
    }
    
    func showDialog(view:UIView,loadText:String) -> Void {
        //        if loadingDialog == nil {
        loadingDialog = showLoadingDialog(view: view, loadingTxt: loadText)
        //        }
    }
    
    func hideDialog() -> Void {
        if loadingDialog != nil {
            hideLoadingDialog(hud: loadingDialog!)
        }
    }
    
    func request(frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                 callback:@escaping (_ resultJson:String,_ returnStatus:Bool)->()) -> Void {
        
        if !NetwordUtil.isNetworkValid(){
            showToast(view: self.view, txt: "网络连接不可用，请检测")
            return
        }
        
        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog {
                self.showDialog(view: self.view, loadText: loadTextStr)
            }
        }
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in
            self.hideDialog()
            callback(resultJson, returnStatus)
        }
        let baseUrl = BASE_URL + PORT
        
        requestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
    }
    
    func postExitLogin(value:String) -> Void {
        if !YiboPreference.getLoginStatus() {
            self.postAuthorize(result:value)
            self.scanSession!.startRunning()
            return
        }
        
        request(frontDialog: true, loadTextStr:"退出登录中...",url:LOGIN_OUT_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "退出失败,请重试"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = LoginOutWrapper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }

                            CRDefaults.setLastRoom(value: "")
                            YiboPreference.saveLoginStatus(value: false as AnyObject)
                            //试玩站号推出时清除账号
//                            if YiboPreference.getAccountMode() == GUEST_TYPE{
                                YiboPreference.saveUserName(value: "" as AnyObject)
                                YiboPreference.savePwd(value: "" as AnyObject)
                                YiboPreference.setAccountMode(value: 0 as AnyObject)
//                            }
                            
                            
                            YiboPreference.setCACHEAVATARdata(value: Data.init())
        
                            self.postAuthorize(result:value)
                            self.scanSession!.startRunning()
                            
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "退出失败,请重试"))
                            }
                        }
                    }
        })
    }
    
}


//MARK: -
//MARK: AVCaptureMetadataOutputObjects Delegate

//扫描捕捉完成
extension ScanCodeViewController : AVCaptureMetadataOutputObjectsDelegate{
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //停止扫描
        self.scanLine.layer.removeAllAnimations()
        self.scanSession!.stopRunning()
        //播放声音
        Tool.playAlertSound(sound: "noticeMusic.caf")
        //扫完完成
        if metadataObjects.count > 0{
            guard let obj = metadataObjects.first as? AVMetadataMachineReadableCodeObject else{return}
            //继续扫描
            self.startScan()
            //提交授权请求
            guard let result = obj.stringValue else {return}
            
            if !isEmptyString(str: result) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.shouldExecute = true
                }
                
                if shouldExecute {
                    self.postExitLogin(value: result)
                }
                
                self.shouldExecute = false
            }
        }
    }
    
}







