//
//  RegexUntil.swift
//  gameplay
//
//  Created by William on 2018/9/14.
//  Copyright © 2018年 yibo. All rights reserved.
//

import Foundation

func matchPhone(message:String) -> Bool {//手机号
    let regex = "^1\\d{10}$"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
    let isValid = predicate.evaluate(with: message)
    return isValid
}

func matchEmail(message:String) -> Bool {//邮箱
    let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
    let isValid = predicate.evaluate(with: message)
    return isValid
}

func matchChineseName(message:String) -> Bool {//中文真实姓名
    let regex = "^[\\u4E00-\\u9FA5]+(·[\\u4E00-\\u9FA5]+)*$"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
    let isValid = predicate.evaluate(with: message)
    return isValid
}

func matchWeiChat(message:String) -> Bool {//微信
    let regex = "^[-_a-zA-Z0-9]{5,20}$"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
    let isValid = predicate.evaluate(with: message)
    return isValid
}

func matchQQ(message:String) -> Bool {//QQ
    let regex = "^[1-9]{1}[0-9]{5,15}$"
    let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
    let isValid = predicate.evaluate(with: message)
    return isValid
}
