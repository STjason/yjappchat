//
//  BaseMainController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/3/20.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import MBProgressHUD
import HandyJSON

let kScreenWidth = UIScreen.main.bounds.width
let kScreenHeight = UIScreen.main.bounds.height
let kScrollRect = CGRect(x: 0, y: 0, width: 600, height: kScreenHeight*0.3)
let kIphone4sScrollRect = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight*0.5)

class BaseMainController: BaseController {
    var chatFloatBtnHoldVC:UIViewController? //聊天室按钮显示室，所显示的控制器
    var titleOfOtherPages = ""
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if #available(iOS 11.0, *) {} else { self.automaticallyAdjustsScrollViewInsets = false}
    
        openPopViewinitializationClick(requestSelf: self, controller: self)
    }
    
    //MARK: - 聊天室
    func dragButtonClickAction(_ btn : FloatDragButton) {
        if switch_nativeOrWapChat() {
            
            guard let floatVC = self.chatFloatBtnHoldVC else {
                return
            }
            /// 规避侧边栏被展示
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showCenterNotiActionName"), object: nil)
            
            if !YiboPreference.getLoginStatus() {
                loginWhenSessionInvalid(controller: floatVC)
                return
            }
            
            if let nav = floatVC.navigationController {
                //隐藏多功能入口按钮
                if let view = getSemicycleButton() {
                    view.isHidden = true
                }
                
                if !YiboPreference.getLoginStatus(){
                    showToast(view: self.view, txt: "请先登录再使用")
                    loginWhenSessionInvalid(controller: self)
                    return
                }
                kCheckLoginStatus(vc: floatVC)
            }
            
        }else {
            let ChatView = ChatViewController()
            ChatView.ItemIS = true
            let navP = MainNavController.init(rootViewController: ChatView)
            self.present(navP, animated: true, completion:nil)
        }
    }
  
    override func viewWillAppear(_ animated: Bool) {
        adjustRightBtn()
    }
    
    func adjustRightBtn() -> Void {
        if !YiboPreference.getLoginStatus(){
            self.navigationItem.rightBarButtonItems?.removeAll()
            
            var rightBarItems = [UIBarButtonItem]()
            
            let system = getSystemConfigFromJson()
            if let appReg = system?.content.native_register_switch {
                if !isEmptyString(str:appReg) && appReg == "on"{
                    self.navigationItem.rightBarButtonItems?.removeAll()
                    let regBtn = UIBarButtonItem.init(title: "注册", style: UIBarButtonItem.Style.plain, target: self, action:#selector(actionReg))
                    rightBarItems.append(regBtn)
                }
            }
            
            let loginBtn = UIBarButtonItem.init(title: "登录", style: UIBarButtonItem.Style.plain, target: self, action: #selector(BaseMainController.actionLogin))
            rightBarItems.append(loginBtn)
            self.navigationItem.rightBarButtonItems = rightBarItems
            
            
//            if allowRegisterSwitch(){
//                self.navigationItem.rightBarButtonItems?.removeAll()
//                let regBtn = UIBarButtonItem.init(title: "注册", style: UIBarButtonItem.Style.plain, target: self, action:#selector(actionReg))
//                self.navigationItem.rightBarButtonItems = [loginBtn,regBtn]
//            }
        }
    }
    
    
    func allowRegisterSwitch() -> Bool{
        let system = getSystemConfigFromJson()
        if let value = system{
            let datas = value.content
            if let appReg = datas?.onoff_register{
                return !isEmptyString(str:appReg) && appReg == "on"
            }else{
                return false
            }
        }
        return false
    }
    
    @objc func actionQRCode() -> Void {
        let link = getSystemConfigFromJson()?.content.app_qr_code_link_ios
        let qrAlert = QRCodeView.init(link: link)
        qrAlert.show()
    }
    
    @objc func actionMenu(){
        if let delegate = menuDelegate{
            delegate.menuEvent(isRight: true)
        }
    }
    
    func actionSetting() -> Void {
        let firstView = self.navigationController?.viewControllers.first
        openSetting(controller: firstView!)
    }
    
    func actionLoginReg() -> Void {
        let firstView = self.navigationController?.viewControllers.first
        loginWhenSessionInvalid(controller: firstView!)
    }
    
    @objc func actionLogin() -> Void {
        loginAction()
    }
    
    func loginAction() {
        let firstView = self.navigationController?.viewControllers.first
        openLoginPage(controller: firstView!)
    }
    
    @objc func actionReg() -> Void {
        registerAction()
    }
    
    func registerAction() {
        let firstView = self.navigationController?.viewControllers.first
        openRegisterPage(controller: firstView!)
    }
    
}


extension BaseMainController: WRCycleScrollViewDelegate{
    /// 点击图片事件
    func cycleScrollViewDidSelect(at index:Int, cycleScrollView:WRCycleScrollView){
        
    }
    /// 图片滚动事件
    func cycleScrollViewDidScroll(to index:Int, cycleScrollView:WRCycleScrollView){
        
    }
}


