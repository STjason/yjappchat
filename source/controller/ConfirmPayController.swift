//
//  ConfirmPayController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/29.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON
import Kingfisher

/**
 * 再次确认充值订单提交后 订单及收款，付款帐号信息，及扫码二维码信息
 * //在线支付方式时，需要在此页面确认跳转收银台支付等操作
 */
class ConfirmPayController: BaseController,UITableViewDelegate,UITableViewDataSource{
    
    var gameDatas = [FakeBankBean]()
    @IBOutlet weak var tablview:UITableView!
    var qrcodeImgUI:UIImageView!
    
    var orderno = ""
    var account = ""
    var money = ""
    var payMethodName = ""
    var receiveName = ""
    var reeiveAccount = ""
    var dipositor = ""
    var dipositorAccount = ""
    var qrcodeUrl = ""
    var payType = PAY_METHOD_ONLINE
    var payJson = ""
    
    var remark = ""//备注
    var fycode = ""//附言吗
    
    var qrcodeIS = true
    var imageOR : UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        qrcodeIS=false
        
//        if let config = getSystemConfigFromJson(){
//            if config.content != nil{
//                if config.content.onoff_payment_show_info == "on" {
//                    qrcodeIS=false
//                }
//            }
//        }
        
        tablview.tableHeaderView = headerView()
//        if !isEmptyString(str: qrcodeUrl) {
////            tablview.tableFooterView = footerView()
//        }else {
//
//            tablview.tableFooterView = UIView()
//        }
        
        
        if isEmptyString(str: qrcodeUrl) {
            qrcodeIS = false
        }
        
        getFakeModels()
        
        tablview.delegate = self
        tablview.dataSource = self
        tablview.reloadData()
//        if !isEmptyString(str: qrcodeUrl){
//            fillImage(url: qrcodeUrl)
//        }
    }
    
    func headerView() -> UIView{
        let chargeOK:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 60))
        chargeOK.backgroundColor = UIColor.groupTableViewBackground
        chargeOK.text = "订单提交成功"
        chargeOK.font = UIFont.systemFont(ofSize: 20)
        chargeOK.textColor = UIColor.red
        chargeOK.textAlignment = NSTextAlignment.center
        return chargeOK;
    }
    
    func getFastPayTips(title: String?) -> String {
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                let fastTips = config.content.pay_tips_deposit_fast
                
                switch title
                {
                case "微信支付","微信":
                    return !isEmptyString(str: config.content.pay_tips_deposit_weixin) ? config.content.pay_tips_deposit_weixin : fastTips
                case "支付宝支付","支付宝":
                    return !isEmptyString(str: config.content.pay_tips_deposit_alipay) ? config.content.pay_tips_deposit_alipay : fastTips
                case "QQ支付","QQ":
                    return !isEmptyString(str: config.content.pay_tips_deposit_qq) ? config.content.pay_tips_deposit_qq : fastTips
                case "美团":
                    return !isEmptyString(str: config.content.pay_tips_deposit_meituan) ? config.content.pay_tips_deposit_meituan : fastTips
                case "云闪付":
                    return !isEmptyString(str: config.content.pay_tips_deposit_yunshanfu) ? config.content.pay_tips_deposit_yunshanfu : fastTips
                case "微信|支付宝":
                    return !isEmptyString(str: config.content.pay_tips_deposit_weixin_alipay) ? config.content.pay_tips_deposit_weixin_alipay : fastTips
                default:
                    return ""
                }
            }
        }
        
        return ""
    }
    
    func footerView() -> UIView{
        let footer = UIView.init(frame: CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 250))
        footer.backgroundColor = UIColor.clear
        qrcodeImgUI = UIImageView.init(frame: CGRect.init(x: (kScreenWidth-200)*0.5, y: 10, width: 200, height: 200))
        
        if let url = URL.init(string: qrcodeUrl)
        {
            qrcodeImgUI.kf.setImage(with: ImageResource.init(downloadURL: url))
        }
        
        qrcodeImgUI.contentMode = UIView.ContentMode.scaleAspectFit
        addLongPressGestureRecognizer(qrcode: qrcodeImgUI)
        let chargeOK:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 210, width: kScreenWidth, height: 30))
        chargeOK.backgroundColor = UIColor.groupTableViewBackground
        chargeOK.text = "若订单提交但还未扫码支付，请在此再次扫码付款！"
        chargeOK.font = UIFont.systemFont(ofSize: 14)
        chargeOK.textColor = UIColor.red
        chargeOK.textAlignment = NSTextAlignment.center
        footer.addSubview(qrcodeImgUI)
        footer.addSubview(chargeOK)
        
//        if let config = getSystemConfigFromJson(){
//            if config.content != nil{
//                if config.content.onoff_payment_show_info == "off" {
//                    qrcodeImgUI.isHidden=true
//                    footer.snp.updateConstraints { (make) in
//                        make.height.equalTo(250-200) //
//                    }
//                }
//            }
//        }
        
        
        qrcodeImgUI.isHidden=true
//        footer.snp.updateConstraints { (make) in
//            make.height.equalTo(250-200) //
//        }
//        chargeOK.isHidden = true
        
        
        return footer
    }
    
    func getFakeModels(){
        
        if payType != PAY_METHOD_ONLINE{
//            if !isEmptyString(str: receiveName){
                let item1 = FakeBankBean()
                item1.text = "订单号码:"
                item1.value = orderno
                gameDatas.append(item1)
//            }
        }
        
        let item2 = FakeBankBean()
        item2.text = "会员账号"
        item2.value = account
        gameDatas.append(item2)
        
        let item3 = FakeBankBean()
        item3.text = "充值金额:"
        var shouldRandom = 0
        if let sysConfig = getSystemConfigFromJson(){
            if sysConfig.content != nil{
                //0是关闭 1先砍掉 然后1到5随机加上  2.1到50的小数
                shouldRandom = sysConfig.content.fast_deposit_add_money_select
            }
        }
        if shouldRandom == 0 || shouldRandom == 1{
            //关掉
            if let moenyFloat = Float(money) {
                let moneyInt = Int(moenyFloat)
                money = String.init(format:"%d",moneyInt)
            }else {
                showToast(view: self.view, txt: "请输入数字")
                return
            }
        }else if (shouldRandom == 2){
            if let moneyFloat = Float(money) {
                money = String.init(format: "%.2f", moneyFloat)
            }
        }
      
        
        item3.value = String.init(format: "%@元", money)
        gameDatas.append(item3)
        
        let item4 = FakeBankBean()
        item4.text = "支付方式:"
        item4.value = payMethodName
        gameDatas.append(item4)
        
        if !isEmptyString(str: receiveName){
            let item5 = FakeBankBean()
            item5.text = "收款姓名:"
            item5.value = receiveName
            gameDatas.append(item5)
        }
        
        if !isEmptyString(str: reeiveAccount){
            let item5 = FakeBankBean()
            item5.text = "收款帐号:"
            item5.value = reeiveAccount
            gameDatas.append(item5)
        }
        
        if !isEmptyString(str: dipositor){
            let item5 = FakeBankBean()
            item5.text = "存款人名:"
            item5.value = dipositor
            gameDatas.append(item5)
        }
        
        if !isEmptyString(str: dipositorAccount){
            let item5 = FakeBankBean()
            item5.text = "存款帐号:"
            item5.value = dipositorAccount
            gameDatas.append(item5)
        }
        
        if !isEmptyString(str: remark){
            let item5 = FakeBankBean()
            item5.text = "备注:"
            item5.value = remark
            gameDatas.append(item5)
        }
        
        if !isEmptyString(str: fycode){
            let item5 = FakeBankBean()
            item5.text = "附言码:"
            item5.value = fycode
            gameDatas.append(item5)
        }
        
        if getNative_charge_version() == "V2" {
            if let sysConfig = getSystemConfigFromJson()
            {
                if sysConfig.content != nil
                {
                    let item6 = FakeBankBean()
                    item6.text = "订单提示:"
                    if payType == 1
                    {
                        let tips = getFastPayTips(title: payMethodName)
                        item6.value = tips
                    }else if payType == 2
                    {
                        item6.value = sysConfig.content.pay_tips_deposit_general_detail
                    }else if payType == 0
                    {
                        
                    }
                    
                    gameDatas.append(item6)
                }
            }
        }
        
        if qrcodeIS {
            let item6 = FakeBankBean()
            item6.text = "二维码:"
            item6.value = fycode
            gameDatas.append(item6)
        }
        
    }
    func updateQrcodeImage(url:String,image:UIImageView?){
        if image == nil{
            return
        }
        var qrcodeUrl = url
        if !isEmptyString(str: qrcodeUrl){
            //这里的logo地址有可能是相对地址
            if qrcodeUrl.contains("\t"){
                let strs = qrcodeUrl.components(separatedBy: "\t")
                if strs.count >= 2{
                    qrcodeUrl = strs[1]
                }
            }
            qrcodeUrl = qrcodeUrl.trimmingCharacters(in: .whitespaces)
            let imageURL = URL(string: qrcodeUrl)
            if let url = imageURL{
                image?.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage.init(named: "default_placeholder_picture"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }else{
//            image?.image = UIImage.init(named: "default_placeholder_picture")
              image?.isHidden = true
        }
    }
    
    private func addLongPressGestureRecognizer(qrcode:UIImageView) {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressClick))
        qrcodeImgUI.isUserInteractionEnabled = true
        qrcodeImgUI.addGestureRecognizer(longPress)
    }
    
    @objc func longPressClick(){
        let alert = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction.init(title: "保存到相册", style: .default, handler: {(action:UIAlertAction) in
            if self.qrcodeImgUI.image == nil{
                return
            }
            UIImageWriteToSavedPhotosAlbum(self.qrcodeImgUI.image!, self, #selector(self.save_image(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        let action2 = UIAlertAction.init(title: "识别二维码图片", style: .default, handler: {(action:UIAlertAction) in
            if self.qrcodeImgUI.image == nil{
                return
            }
            UIImageWriteToSavedPhotosAlbum(self.qrcodeImgUI.image!, self, #selector(self.readQRcode(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(action2)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func readQRcode(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer){
        
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy : CIDetectorAccuracyHigh])
        let imageCI = CIImage.init(image: self.qrcodeImgUI.image!)
        let features = detector?.features(in: imageCI!)
        guard (features?.count)! > 0 else { return }
        let feature = features?.first as? CIQRCodeFeature
        let qrMessage = feature?.messageString
        
        guard var code = qrMessage else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确")
            return
        }
        if !isEmptyString(str: code){
            var appname = ""
            code = code.lowercased()
            if code.contains("weixin"){
                appname = "微信"
            }else if code.contains("alipay"){
                appname = "支付宝"
            }else{
                showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            }
            if error == nil {
                let ac = UIAlertController.init(title: "保存成功",
                                                message: String.init(format: "您可以打开%@,从相册选取并识别此二维码", appname), preferredStyle: .alert)
                ac.addAction(UIAlertAction(title:"去扫码",style: .default,handler: {(action:UIAlertAction) in
                    // 跳转扫一扫
                    if appname == "微信"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "weixin://")!){
                            openBrower(urlString: "weixin://")
                        }else{
                            showToast(view: self.view, txt: "您未安装微信，无法打开扫描")
                        }
                    }else if appname == "支付宝"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "alipay://")!){
                            openBrower(urlString: "alipay://")
                        }else{
                            showToast(view: self.view, txt: "您未安装'支付宝'，无法打开扫描")
                        }
                    }
                }))
                self.present(ac, animated: true, completion: nil)
            } else {
                let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
        }else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            return
        }
    }
    
    //保存二维码
    @objc func save_image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            //            let ac = UIAlertController(title: "保存成功", message: "请打开微信识别二维码", preferredStyle: .alert)
            //            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            //            self.present(ac, animated: true, completion: nil)
            showToast(view: self.view, txt: "保存图片成功")
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    func fillImage(url:String) -> Void {
        let imageURL = URL(string: url)
        if let url = imageURL{
            downloadImage(url: url, imageUI: self.qrcodeImgUI)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.gameDatas[indexPath.row]
        let widht:CGFloat = screenWidth - 150 // 从 SB中查看所得
        if model.text.contains("订单提示") {
            let height = getHeightForTitle(title: model.value, width: widht)
            return height > 44 ? height + 20 : 44
        }else {
            if qrcodeIS {
                if indexPath.row == self.gameDatas.count-1{
                    return 250
                }
            }
            return 44.0
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameDatas.count
    }
    @objc private func saveImage(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: AnyObject) {
        if error == nil {
            showToast(view: self.view, txt: "保存图片成功")
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
    
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if qrcodeIS {
            if self.gameDatas.count-1 == indexPath.row {
                
                let actionSheet = UIAlertController.init(title:"温馨提示", message:nil, preferredStyle: UIAlertController.Style.actionSheet)
                let Action_sava = UIAlertAction.init(title: "保存图片", style: UIAlertAction.Style.default) { (Action) in
            
                    if self.imageOR?.image == nil{
                        return
                    }
        
                    UIImageWriteToSavedPhotosAlbum((self.imageOR?.image)!, self, #selector(self.saveImage(image:didFinishSavingWithError:contextInfo:)), nil)
                    
                }
                let Action_saosao = UIAlertAction.init(title:"识别二维码", style: UIAlertAction.Style.default) { (Action) in
             
                    if self.imageOR?.image == nil {
                        return
                    }

                    UIImageWriteToSavedPhotosAlbum((self.imageOR?.image)!, self, #selector(self.readQRcode(image:didFinishSavingWithError:contextInfo:)), nil)
                    
                }
                let Action_Cancel = UIAlertAction.init(title:"取消", style: UIAlertAction.Style.cancel, handler: nil)
                actionSheet.addAction(Action_sava)
                actionSheet.addAction(Action_saosao)
                actionSheet.addAction(Action_Cancel)
                
                self.present(actionSheet, animated: true, completion: nil)
                
            }
        }
    }
 
    
    
    @objc private func onCopyBtn(ui:UIButton){
        if self.gameDatas.isEmpty{
            return
        }
        let data = self.gameDatas[ui.tag]
        if !isEmptyString(str: data.value){
            UIPasteboard.general.string = data.value
            showToast(view: self.view, txt: "复制成功")
        }else{
            showToast(view: self.view, txt: "没有内容,无法复制")
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
//            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
//        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
        }
        
        
        let model = self.gameDatas[indexPath.row]
        cell.textTV.numberOfLines = 0
        cell.textTV.text = model.text
        cell.valueTV.isHidden = false
        cell.valueTV.text = model.value
        
        if model.text.contains("订单提示") {
            cell.valueTV.numberOfLines = 0
        }else {
            cell.valueTV.numberOfLines = 1
        }
        
        
        let copyBtn1 = UIButton()
        cell.addSubview(copyBtn1)
        copyBtn1.setTitle("复制", for: .normal)
        copyBtn1.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
        copyBtn1.setTitleColor(UIColor.black, for: .normal)
        copyBtn1.setBackgroundImage(UIImage.init(named:"fast_money_normal_bg"), for: UIControl.State.normal)
        copyBtn1.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.width.equalTo(50)
            make.height.equalTo(30)
            make.centerY.equalTo(cell)
        }
        copyBtn1.tag = indexPath.row
        copyBtn1.addTarget(self, action: #selector(onCopyBtn(ui:)), for: .touchUpInside)


        
        
        if qrcodeIS {
            if self.gameDatas.count-1 == indexPath.row {
                let cells = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cells")
                imageOR = UIImageView.init()
                cells.addSubview(imageOR!)
                imageOR?.snp.makeConstraints { (make) in
                    make.center.equalTo(cells)
                    make.width.height.equalTo(180)
                }
                
                updateQrcodeImage(url:qrcodeUrl, image: imageOR)
                
                
                let chargeOK:UILabel = UILabel.init()
                chargeOK.text = "若订单提交但还未扫码支付，请在此再次扫码付款！"
                chargeOK.font = UIFont.systemFont(ofSize: 12)
                chargeOK.textColor = UIColor.red
                chargeOK.textAlignment = NSTextAlignment.center
                cells.addSubview(chargeOK)
                chargeOK.snp.makeConstraints { (make) in
                    make.left.right.equalTo(0)
                    make.bottom.equalTo(-5)
                }
                chargeOK.textAlignment = .center
                
                
                return cells
            }
        }
        
        
        return cell
    }
    
    private func getHeightForTitle(title:String,width:CGFloat) -> CGFloat {
        let height = String.getStringHeight(str: title, strFont: 14, w: width)
        return height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
    }

}

extension String {
    //返回字数
    var count: Int {
        let string_NS = self as NSString
        return string_NS.length
    }

}
