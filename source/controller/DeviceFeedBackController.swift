//
//  DeviceFeedBackController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/19.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

class DeviceFeedBackController: BaseController {
    
    @IBOutlet weak var tipsLabel: UILabel!
//    @IBOutlet weak var inputText: UITextView!
    @IBOutlet weak var commitBtn:UIButton!
    
    lazy var inputText: UITextView = {
        var textView = UITextView()
        view.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.top.equalTo(tipsLabel.snp.bottom).offset(8)
            make.bottom.equalTo(commitBtn.snp.top).offset(-20)
            make.height.equalTo(200)
            make.leading.equalTo(commitBtn)
            make.trailing.equalTo(commitBtn)
        }
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupthemeBgView(view: self.view)
        self.navigationItem.title = "建议反馈"
        commitBtn.layer.cornerRadius = 5
        commitBtn.theme_backgroundColor = "Global.themeColor"
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: self.tipsLabel)
        setupNoPictureAlphaBgView(view: self.inputText,alpha: 0.1)
//        setupNoPictureAlphaBgView(view: self.tipsLabel,alpha: 0.1)
        
        inputText.layer.cornerRadius = 3.0
        inputText.layer.borderColor = UIColor.lightGray.cgColor
        inputText.layer.borderWidth = 1.0
        inputText.backgroundColor = UIColor.groupTableViewBackground
        
        commitBtn.addTarget(self, action: #selector(onCommit), for: UIControl.Event.touchUpInside)
    }
    
    @objc func onCommit() -> Void {
        if isEmptyString(str: inputText.text!){
            showToast(view: self.view, txt: "请输入您的建议")
            return
        }
        
        let parameters = ["content":inputText.text!] as [String : Any]
        
        self.request(frontDialog: true, method:.post,url: URL_POSTFEEDBACK,params:parameters) {[weak self] (json, status) in
            if let weakSelf = self {
                showToast(view: weakSelf.view, txt: "提交建议成功，感谢您的支持")
                weakSelf.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
}
