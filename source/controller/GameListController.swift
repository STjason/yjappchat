//
//  GameListController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/24.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher

class GameListController: BaseController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var bottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var collections:UICollectionView!
    var datas = [gameDetailItem]()
    var gameCode = ""
    var myTitle = ""
    var isHomeSubView = false
    var nb_game_redpacket = false
    var moduleCode:Int = 0
    
    //红包
    lazy var dragBtnRed_packet: FloatDragButton = {
        var dragBtnRed = FloatDragButton.init()
        self.redpacket(view: dragBtnRed)
        dragBtnRed.clickClosure = {
            [weak self]
            (dragBtn) in
            self?.dragBtnRed_packetClickAction(dragBtn)
        }
        
        return dragBtnRed
    }()
    
    func dragBtnRed_packetClickAction(_ btn : FloatDragButton) {
        let url = String.init(format: "%@", BASE_URL + PORT + URL_FORWARD_NBCHESS)
        openActiveDetail(controller: self, title: "NB红包", content:"",foreighUrl: url)
    }
    
    func refreshWithType(isHomeSubView: Bool) {
        if isHomeSubView {bottomConstaint.constant = 49}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let config = getSystemConfigFromJson() {
            if config.content != nil {
                print(config.content.switch_nb_game_redpacket)
                if config.content.switch_nb_game_redpacket == "on" {
                    nb_game_redpacket = true
                }
            }
        }


        if #available(iOS 11, *) {} else {
            if self.responds(to: #selector(setter: edgesForExtendedLayout)) {
                self.edgesForExtendedLayout = UIRectEdge.bottom
            }
        }
        
        self.navigationItem.title = !isEmptyString(str: myTitle) ? myTitle : "游戏列表"
        collections.delegate = self
        collections.dataSource = self
        collections.register(OtherPageCell.self, forCellWithReuseIdentifier: "gameListCell")
        collections.showsVerticalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: ( kScreenWidth-2-0.5*6)/3, height: 150)
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0.5
        collections.collectionViewLayout = layout
        setViewBackgroundColorTransparent(view: collections)
        //load games data from web
        self.getData()

        if gameCode.contains("nb") {
            if nb_game_redpacket {
                 self.view.addSubview(self.dragBtnRed_packet)
                 self.dragBtnRed_packet.snp.makeConstraints { (make) in
                    make.right.equalTo(0)
                    make.centerY.equalTo(self.view)
                    make.width.equalTo(kScreenWidth/3)
                    make.height.equalTo(kScreenWidth/3+51)
                 }
            }
        }
    }
    
    func redpacket(view:UIView){
        let imageViewRed = UIImageView.init()
        let buttonRed = UIImageView.init() //如果需求要求财神爷不要点击就button
        view.addSubview(imageViewRed)
        view.addSubview(buttonRed)
        
        imageViewRed.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(0)
            make.height.width.equalTo(kScreenWidth/3)
        }
        buttonRed.snp.makeConstraints { (make) in
            make.top.equalTo(imageViewRed.snp.bottom).offset(1)
            make.left.equalTo(imageViewRed.snp.left)
            make.right.equalTo(0)
            make.height.equalTo(50)
        }
        
        imageViewRed.image = UIImage.init(named: "nbredpacket")
        imageViewRed.contentMode = .scaleAspectFit
        buttonRed.image = UIImage.init(named: "getredpacket")
        buttonRed.contentMode = .scaleAspectFit
        
//        buttonRed.setImage(UIImage.init(named: "getredpacket"), for: UIControl.State.normal)
//        buttonRed.addTarget(self, action: #selector(redpacketClick), for: .touchUpInside)
    }
    

    
    func getData() {
        
        let chessURL = THIRD_GAME_URL
        request(frontDialog: true, method: .get, loadTextStr: "正在获取数据...", url: chessURL, params: ["gameType":self.gameCode]) { (resultJson:String, resultStatus:Bool) in
            
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            if let result = NBModel.deserialize(from: resultJson) {
                if result.success{
                    self.datas.removeAll()
                    for item in result.content! {
                        
                        //棋牌
                        self.datas.append(item)

                       
//                        let model = GameItemResult()
//
//                        if let image = item.buttonImagePath{
//                            model.img = image
//                        } else{
//                            if let name = item.img{
//                                model.img = name
//                            }
//                        }
//
//                        if let name = item.displayName{
//                            model.name = name
//                        } else{
//                            if let name = item.name{
//                                model.name = name
//                            }
//                        }
//                        if let lid = item.lapisId{
//                            model.lapisId = lid
//                        }
//                        if let url = item.url{
//                            model.url = url
//                        }

//                        self.datas.append(model)
                    }
                    self.collections.reloadData()
                }else{
                    if !isEmptyString(str: result.msg){
                        showToast(view: self.view, txt: result.msg)
                    }else{
                        showToast(view: self.view, txt: "获取失败")
                    }
                    if (result.code == 0 || result.code == -1) {
                        loginWhenSessionInvalid(controller: self)
                    }
                }
            }
        }
    }
    
    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("GameListController，numberOfItemsInSection：\(datas.count)")
        return datas.count
    }
    //返回自定义的cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gameListCell", for: indexPath) as! OtherPageCell
        
        let data = self.datas[indexPath.row]
        if !isEmptyString(str: data.name) {
            cell.titleLabel?.text = data.name
        }else if !isEmptyString(str: data.displayName){
            cell.titleLabel?.text = data.displayName
        }else{
            cell.titleLabel?.text = "暂无名称"
        }
        
        var imgUrl:String = ""
        if !isEmptyString(str: data.buttonImagePath) {
            //电子游戏图片
            imgUrl = BASE_URL + PORT + data.buttonImagePath
        }else if !isEmptyString(str: data.img){
            //棋牌
            imgUrl = BASE_URL + PORT + data.img
        }else{
            return cell
        }
        if isEmptyString(str: imgUrl){
            return cell
        }
        guard let uRL = URL(string: imgUrl) else {
            return cell
        }
       
        
        downloadImage(url: uRL, imageUI: cell.imgView!)

//        let imgPath = data.img
////        let imgPath = data.buttonImagePath
//        //彩种的图地址是根据彩种编码号为姓名构成的
//        var imgUrl = ""
//        if self.gameCode == "mg"{
//            imgUrl = BASE_URL + PORT + imgPath
//        }else if self.gameCode == "pt"{
//            imgUrl = BASE_URL + PORT + imgPath
//        }else if self.gameCode == "ag" {
//            imgUrl = BASE_URL + PORT + imgPath
//        }else if self.gameCode == "qt" {
//            imgUrl = BASE_URL + PORT + imgPath
//        }else if ["nb","kyqp","leyou","ygqp"].contains(self.gameCode) {
//            cell.titleLabel?.text = data.name
//            let imgPath = data.img
//
//            if self.gameCode == "nb"{
//                imgUrl = BASE_URL + PORT  + imgPath
//            }else if self.gameCode == "kyqp"{
//                imgUrl = BASE_URL + PORT + imgPath
//            }else if self.gameCode == "leyou" {
//                imgUrl = BASE_URL + PORT + imgPath
//            }else if self.gameCode == "ygqp"{
//                imgUrl = BASE_URL + PORT + imgPath
//            }
//
//        }else if self.gameCode == "skywind" {
//            imgUrl = BASE_URL + PORT + imgPath
//        }else if self.gameCode == "cq9" {
//            imgUrl = BASE_URL + PORT + imgPath
//        }else if self.gameCode == "ybqp"{
//            imgUrl = BASE_URL + PORT + imgPath
//        }else{
//            imgUrl = BASE_URL + PORT + imgPath
//        }
//
//        if isEmptyString(str: imgUrl){
//            return cell
//        }
//        let uRL = URL(string: imgUrl)
//        if let url = uRL{
//            downloadImage(url: url, imageUI: cell.imgView!)
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lotData = self.datas[indexPath.row]
        let url = lotData.forwardUrl
        var name = lotData.name
        if name.isEmpty{
            name = lotData.displayName
        }
        enterGame(forwardUrl: url,titleName:name)
    }
    
}
//MARK:进入游戏界面
extension GameListController{
    
    func enterGame(forwardUrl:String,titleName:String){
        
        forwardRealWeb(controller: self, forwardUrl: forwardUrl, titleName: titleName)

    }
}
