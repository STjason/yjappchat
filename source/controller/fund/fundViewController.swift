//
//  fundViewController.swift
//  jijin
//
//  Created by ken on 2019/3/19.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit
import MJRefresh
class fundViewController: BaseController,UITableViewDataSource,UITableViewDelegate{
    private let tabble = UITableView()
    let refreshHeader = MJRefreshNormalHeader()
    let refreshFooter = MJRefreshBackNormalFooter()
    var jine:Float?
    var money  = ""
    var scaleEnd  = ""
    var incomeMon  = ""
    var totalRateMoney = ""
    var sevendayRate  = ""
    var yesterdayEarnMoney  = ""
    var incomeYear  = ""
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }

    /** 余额生金data */
    func incomeData(){
        request(frontDialog: false, method: .get, loadTextStr: "", url: INCOME_MONEY, params: [:]) { (resultJson:String, resultStatus:Bool) in
            if resultStatus{
                if !self.isX() {
                    self.tabble.mj_header?.endRefreshing()
                }
                if let result = BalanceInterestWraper.deserialize(from: resultJson){
                    if result.success{
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                        if let content = result.content{
                            var model = BalanceInterestContentData();
                            model = content;
                            self.money=model.money
                            self.incomeMon=model.incomeMon
                            self.totalRateMoney=model.totalRateMoney
                            self.sevendayRate=model.sevendayRate
                            self.yesterdayEarnMoney=model.yesterdayEarnMoney
                            self.incomeYear=model.incomeYear
                            self.scaleEnd=model.scaleEnd
                            self.tabble.reloadData()
                        }
                    }else{
                        if !isEmptyString(str: result.msg){
                            self.print_error_msg(msg: result.msg)
                        }else{
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }
                        if (result.code == 0) {
                            loginWhenSessionInvalid(controller: self)
                        }
                    }
                }
            }
        }
    }
    
    func navUI()->Void{
        self.title="余额生金"
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)]
        
        let button1 = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 30))
        button1 .setTitle("返回", for: UIControl.State.normal)
        button1 .setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.navigationItem.leftBarButtonItem=UIBarButtonItem.init(customView: button1)
        button1.addTarget(self, action:#selector(exit), for: UIControl.Event.touchUpInside);
        
        let button2 = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 14, height: 20))
        button2.setImage(UIImage.init(named: "账单icon"), for: UIControl.State.normal)
        //        self.navigationItem.rightBarButtonItem=UIBarButtonItem.init(customView: button2)
        button2.addTarget(self, action:#selector(billClick), for: UIControl.Event.touchUpInside);
        
        let button3 = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 20))
        button3.setImage(UIImage.init(named: "答疑icon"), for: UIControl.State.normal)
        button3.addTarget(self, action:#selector(dayi), for: UIControl.Event.touchUpInside);
        
        self.navigationItem.rightBarButtonItems=[UIBarButtonItem.init(customView: button2),UIBarButtonItem.init(customView: button3)]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabble.frame=CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        tabble.delegate=self
        tabble.dataSource=self;
        self.view.addSubview(tabble)
        
        tabble.register(UINib.init(nibName: "AmountTableCell", bundle: nil), forCellReuseIdentifier: "AmountTableCell")
        tabble.register(UINib.init(nibName: "YieldButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "YieldButtonTableViewCell")
        tabble.register(UINib.init(nibName: "adBannerCell", bundle: nil), forCellReuseIdentifier: "adBannerCell")
        
        tabble.separatorStyle = .none;
        
        self.view.backgroundColor=UIColor.init(red: 243/255.0, green: 243/255.0, blue: 248/255.0, alpha: 1.0)
        tabble.backgroundColor=self.view.backgroundColor
        
        UserDefaults.standard.set(false, forKey:"LTimer")
       
        
        incomeData()
        
        //适配iphoneX
        if !isX() {
            refreshHeader.setRefreshingTarget(self, refreshingAction: #selector(headerRefresh))
            tabble.mj_header = refreshHeader
        }else{
//            tabble.contentInsetAdjustmentBehavior = .never
//            tabble.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
//            tabble.scrollIndicatorInsets = tabble.contentInset
        }
        
    }
    
    func isX() -> Bool {
        if UIScreen.main.bounds.height == 812 {
            return true
        }
        return false
    }
    
    @objc fileprivate func headerRefresh() {
        incomeData()
    }

    
    @objc func exit(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func billClick(){
        let root = FundBillViewController()
        let nav = UINavigationController.init(rootViewController: root)
        self.present(nav, animated:true, completion: nil)
    }
    
    @objc func dayi(){
        let url = BASE_URL + MONEY_ISSUE
        let loginVC = UIStoryboard(name: "active_detail_page", bundle: nil).instantiateViewController(withIdentifier: "activeDetail")
        let recordPage = loginVC as! ActiveDetailController
        recordPage.titleStr = "余额生金"
        recordPage.htmlContent = ""
        recordPage.foreignUrl = url
        recordPage.CookieIs = true
        if self.navigationController == nil{
            let nav = UINavigationController.init(rootViewController: recordPage)
            self.present(nav, animated: true, completion: nil)
        }else{
            self.navigationController?.pushViewController(recordPage, animated: true)
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0  {
            return 350 //290
        }else if indexPath.row == 1 {
            return 120
        }
        return 250
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AmountTableCell", for: indexPath) as! AmountTableCell
            cell.selectionStyle = .none;
            self.addView(cell: cell)
            self.notClick(cell: cell)
            
            cell.AmountLabel.text="总金额\(self.money)元"
            cell.YesterdayEarnings.text=self.yesterdayEarnMoney

            if !self.yesterdayEarnMoney.isEmpty{
                 cell.LableAmount(lable: cell.YesterdayEarnings, Amount: self.yesterdayEarnMoney)
            }
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "YieldButtonTableViewCell", for: indexPath) as! YieldButtonTableViewCell
            cell.selectionStyle = .none;
            cell.AnnualIncome.text=self.incomeYear
            cell.MonthlyIncome.text=self.incomeMon
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "adBannerCell", for: indexPath) as! adBannerCell
        cell.selectionStyle = .none;
        return cell
    }
    
    func addView(cell:UITableViewCell)->Void{
        var RateView:AnnualizedRate!
        
        RateView = Bundle.main.loadNibNamed("AnnualizedRate", owner: self, options: nil)?.last as! AnnualizedRate
        let yyy = cell.bounds.size.height - 130
        RateView.frame=CGRect.init(x: 10, y: yyy, width: self.view.bounds.size.width-20, height: 120)
        cell.addSubview(RateView)
        
        RateView.layer.masksToBounds = true;
        RateView.layer.cornerRadius = 10;
        
       
        RateView.Customer.addTarget(self, action:#selector(CustomerClick), for: UIControl.Event.touchUpInside);
    
        
        RateView.AccumulatedLabel.text=self.totalRateMoney
        
        RateView.CopiesLable.text=self.scaleEnd
        
        RateView.SevenDaysLable.text=self.sevendayRate
        
    }
    
    //通知栏时间处理
    func notClick(cell:AmountTableCell)->Void{
        cell.exit.addTarget(self, action:#selector(exit), for: UIControl.Event.touchUpInside);
        cell.bill.addTarget(self, action:#selector(billClick), for: UIControl.Event.touchUpInside);
        cell.dayi.addTarget(self, action:#selector(dayi), for: UIControl.Event.touchUpInside);
    }
    
    //联系客服
   @objc func CustomerClick(){
        if let config = getSystemConfigFromJson() {
            if config.content != nil {
                let url = String.init(format: "%@", config.content.customerServiceUrlLink)
                openActiveDetail(controller: self, title: "在线客服", content: "",foreighUrl: url)
                return
            }
        }
        showToast(view: self.view, txt: "地址获取失败，请联系客服")
   }
    
    
}


