//
//  YieldButtonTableViewCell.swift
//  jijin
//
//  Created by ken on 2019/3/19.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit

class YieldButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var MonthlyImage: UIImageView!
    @IBOutlet weak var AnnualImage: UIImageView!
    @IBOutlet weak var AnnualIncome: UILabel!
    @IBOutlet weak var MonthlyIncome: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        MonthlyImage.layer.masksToBounds = true
        MonthlyImage.layer.shadowColor = UIColor.init(red: 58/255.0, green: 132/255.0, blue: 255/255.0, alpha:0.12).cgColor
        MonthlyImage.layer.shadowOpacity = 1.0
        
        AnnualImage.layer.masksToBounds = true
        AnnualImage.layer.shadowColor = UIColor.init(red: 58/255.0, green: 132/255.0, blue: 255/255.0, alpha:0.12).cgColor
        AnnualImage.layer.shadowOpacity = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
