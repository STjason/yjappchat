//
//  AmountTableCell.swift
//  jijin
//
//  Created by ken on 2019/3/19.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit

class AmountTableCell: UITableViewCell {
    static var iiiiii :Int = 0 //小数点后3，4
    static var jjjjjj :Int = 0 //小时候点后1，2
    static var qqqqqq :Int = 0 //个位数以上
    @IBOutlet weak var exit: UIButton!
    @IBOutlet weak var bill: UIButton!
    @IBOutlet weak var dayi: UIButton!
    @IBOutlet weak var yingchang: UIButton!
    @IBOutlet weak var AmountLabel: UILabel! //总金额3000.22元
    @IBOutlet weak var YesterdayEarnings: UILabel! //昨日金额
    @IBOutlet weak var bagImage: UIImageView!
    var yingchangOn = true
    var AmountLabelStr = ""
    var LTimer:Timer?
    var Amountflonat:Double?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bagImage.layer.masksToBounds = true
        bagImage.layer.shadowColor = UIColor.init(red: 58/255.0, green: 132/255.0, blue: 255/255.0, alpha:0.12).cgColor
        bagImage.layer.shadowOpacity = 1.0
        
        yingchang.addTarget(self, action:#selector(yingchangClick), for: UIControl.Event.touchUpInside);
        
    }
    
    @objc func LableAmountTimer(){
        let tiaodong = "\(AmountTableCell.qqqqqq).\(AmountTableCell.jjjjjj)\(AmountTableCell.iiiiii)"
        let tiaodongfloat = Double(tiaodong)
        self.YesterdayEarnings.text = "\(tiaodong)"
        
        if CGFloat.init(tiaodongfloat!) <= CGFloat.init(self.Amountflonat!) {
            AmountTableCell.jjjjjj+=1
            AmountTableCell.iiiiii+=1
            if CGFloat.init(self.Amountflonat!) >= 100 { //大于100元以后三个变量同时跳动
                    AmountTableCell.qqqqqq += 1
                if AmountTableCell.jjjjjj >= 99 {
                    AmountTableCell.jjjjjj = 0
                }
            }else if CGFloat.init(self.Amountflonat!) >= 5 { //大于5元以后小数点后2位等于39，个十位进1
                if AmountTableCell.jjjjjj >= 39 {
                    AmountTableCell.qqqqqq += 1
                    AmountTableCell.jjjjjj = 10
                }
            }else{
                if AmountTableCell.jjjjjj >= 99 {
                    AmountTableCell.qqqqqq += 1
                    AmountTableCell.jjjjjj = 0
                }
            }
            if AmountTableCell.iiiiii >= 99 {
                AmountTableCell.iiiiii = 0
            }
        }else{
            AmountTableCell.iiiiii=0
            AmountTableCell.jjjjjj=0
            AmountTableCell.qqqqqq=0
            UserDefaults.standard.set(false, forKey:"LTimer")
            self.YesterdayEarnings.text = String(format:"%.4f",self.Amountflonat!)
            self.LTimer?.invalidate()
        }
    }
    
    func LableAmount(lable:UILabel,Amount:String){
        let defaultsLtimer = UserDefaults.standard.object(forKey: "LTimer") as! Bool
        if  !defaultsLtimer {
            UserDefaults.standard.set(true, forKey:"LTimer") //正在timer中
            lable.text="0"
            self.Amountflonat = Double(Amount)
            self.LTimer = Timer.scheduledTimer(timeInterval: TimeInterval(0.01), target: self, selector: #selector(LableAmountTimer), userInfo: nil, repeats: true)
            RunLoop.current.add(self.LTimer!, forMode: RunLoop.Mode.common)
            self.LTimer?.fire()
        }
    }
    
    
   @objc func yingchangClick(){
        if yingchangOn {
            AmountLabelStr = AmountLabel.text!
            yingchang .setImage(UIImage.init(named:"余额不可见"), for: UIControl.State.normal)
            AmountLabel.text="总金额******元"
            yingchangOn = false
        }else{
            yingchang.setImage(UIImage.init(named:"余额可见icon"), for: UIControl.State.normal)
            yingchangOn = true
            AmountLabel.text=AmountLabelStr
        }
   }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
