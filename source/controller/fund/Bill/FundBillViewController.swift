//
//  FundBillViewController.swift
//  jijin
//
//  Created by ken on 2019/3/19.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit
import MJRefresh
let PageSize = 20
class FundBillViewController:BaseController,UITableViewDataSource,UITableViewDelegate{
    let refreshHeader = MJRefreshNormalHeader()
    let refreshFooter = MJRefreshBackNormalFooter()
    private let tabble = UITableView()
    var dataRows:[FundBillRowsData] = []
    /** 账单data */
    private func incomeListData(pageNumber:NSInteger,pageSize:NSInteger,type:Bool){
        var PageNuber = pageNumber
        if !type {
            if self.dataRows.count % pageSize == 0 {
                PageNuber = self.dataRows.count / pageSize + 1
            }else {
                self.noMoreDataStatusRefresh()
                self.endRefresh()
                return
            }
        }
        let params = ["pageNumber":PageNuber,"pageSize":pageSize] as [String : Any]
        request(frontDialog: false, method: .get, loadTextStr: "", url: INCOMELIST, params:params) { (resultJson:String, resultStatus:Bool) in
            if resultStatus{
                if let result = FundBillWraper.deserialize(from: resultJson){
                    if result.success{
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                        if result.content != nil{
                            guard let contentP = result.content else {return}
                            guard let rowsP = contentP.rows else {return}
                            if type {
                                self.dataRows.removeAll()
                            }
                            self.dataRows += rowsP
                            self.noMoreDataStatusRefresh(noMoreData: rowsP.count % pageSize != 0 || rowsP.count == 0)
                            self.tabble.reloadData()
                            self.endRefresh()
                        }
                    }else{
                        if !isEmptyString(str: result.msg){
                            self.print_error_msg(msg: result.msg)
                        }else{
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }
                        if (result.code == 0) {
                            loginWhenSessionInvalid(controller: self)
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let button1 = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 30))
        button1 .setTitle("返回", for: UIControl.State.normal)
        button1 .setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.navigationItem.leftBarButtonItem=UIBarButtonItem.init(customView: button1)
        button1.addTarget(self, action:#selector(exit), for: UIControl.Event.touchUpInside);
        
        
        self.navigationController?.navigationBar
            .setBackgroundImage(UIImage(named: "账单背景"), for: .default)
        
        self.title="账单列表"
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)]
        

        tabble.frame=CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height-45)
        tabble.delegate=self
        tabble.dataSource=self;
        self.view.addSubview(tabble)
        tabble.register(UINib.init(nibName: "BillCell", bundle: nil), forCellReuseIdentifier: "BillCell")
        
        
        incomeListData(pageNumber:1, pageSize:PageSize,type: true)
        setupRefreshView()
    }
    
    @objc func exit(){
        self.dismiss(animated: true, completion: nil)
    }
    
//    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
//        return "2月"
//    }
//
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.dataRows.count
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillCell", for: indexPath) as! BillCell
        cell.income.text = "+\(self.dataRows[indexPath.row].income)"
        cell.Explanation.text = "余额生金-\(self.dataRows[indexPath.row].statDate)-收益发放"
        cell.date.text=Timestamp(self.dataRows[indexPath.row].statDate,type: false)
        cell.week.text=getDayOfWeekStr(self.dataRows[indexPath.row].statDate)
        return cell
    }
    
    func Timestamp(_ today:String,type:Bool) -> String {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)
        let timeInterval:TimeInterval = todayDate!.timeIntervalSince1970
        let timeStamp = Int(timeInterval) + (60*60*24)
//        print(timeStamp)
        let timeIntervalstr:TimeInterval = TimeInterval(timeStamp)
        let date = Date(timeIntervalSince1970: timeIntervalstr)
        let dformatter = DateFormatter()
        if type {
               dformatter.dateFormat = "yyyy-MM-dd"
        }else{
               dformatter.dateFormat = "MM-dd"
        }
        return dformatter.string(from: date)
    }

    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func getDayOfWeekStr(_ today:String) -> String{
        switch getDayOfWeek(Timestamp(today,type: true)) {
        case 1:
            return "周日"
        case 2:
            return "周一"
        case 3:
            return "周二"
        case 4:
            return "周三"
        case 5:
            return "周四"
        case 6:
            return "周五"
        case 7:
            return "周六"
        default:
             return "失败"
        }
    }
}


extension FundBillViewController {
    
    //MARK: - 刷新
    private func setupRefreshView() {
        refreshHeader.setRefreshingTarget(self, refreshingAction: #selector(headerRefresh))
        refreshFooter.setRefreshingTarget(self, refreshingAction: #selector(footerRefresh))
        self.tabble.mj_header = refreshHeader
        self.tabble.mj_footer = refreshFooter
    }
    
    @objc fileprivate func headerRefresh() {
        incomeListData(pageNumber:1, pageSize:PageSize,type: true)
    }
    
    @objc fileprivate func footerRefresh() {
        incomeListData(pageNumber:2,pageSize:PageSize,type: false)
    }
    
    private func endRefresh() {
        self.tabble.mj_footer?.endRefreshing()
        self.tabble.mj_header?.endRefreshing()
    }
    
    func noMoreDataStatusRefresh(noMoreData: Bool = true) {
        if noMoreData {
            self.tabble.mj_footer?.endRefreshingWithNoMoreData()
        }else {
            self.tabble.mj_footer?.resetNoMoreData()
        }
    }
    
    
}
