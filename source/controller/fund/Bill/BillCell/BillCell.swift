//
//  BillCell.swift
//  jijin
//
//  Created by ken on 2019/3/19.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit

class BillCell: UITableViewCell {

    @IBOutlet weak var week: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var income: UILabel!
    @IBOutlet weak var Explanation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
