//
//  FeeConvertRecordController.swift
//  gameplay
//
//  Created by admin on 2019/1/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class FeeConvertRecordController: BaseController {
    
    var rowDatas = [FeeConvertRecordRowModel]()
    var selectedRows = [Int]()
    /** ["全部状态","转账失败","转账成功","处理中"] */
    var status = ["全部状态","转账失败","转账成功","处理中"]
    /** ["全部转入方式","从系统转入三方","从三方转入系统"] */
    var transformMethod = ["全部转入方式","从系统转入三方","从三方转入系统"]
//    var thirdPartyType = ["三方类型","AG","BBIN",
//                          "MG","QT","ALLBET","PT",
//                          "OG","DS","NB","SKYWIND","KY",
//                          "浮亚电竞","BGK捕鱼","财神棋牌","天美棋牌"]
    var thirdPartyType:[String] = ["三方类型"]
    
    var quatoPlatforms:[FeeConverContentItem] = [FeeConverContentItem]()
    
    //MARK: -lazy
    lazy var mainTaleView:UITableView = {
        let table = UITableView()
        table.backgroundColor = UIColor.clear
        table.delegate = self
        table.dataSource = self
        table.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight - CGFloat(KNavHeight))
        table.tableFooterView = UIView()
        table.tableHeaderView = getTableViewHeader()
        table.register(UINib.init(nibName: "FeeConvertRecordCell", bundle: nil), forCellReuseIdentifier: "feeConvertRecordCell")
        table.separatorInset = UIEdgeInsets.zero
        return table
    }()
    
    lazy var filterView:FeeConvertRecordFilter = {
        let filter = Bundle.main.loadNibNamed("FeeConvertRecordFilter", owner: self, options: nil)!.last as! FeeConvertRecordFilter
        filter.frame = CGRect.init(x: 0, y: -200, width: screenWidth, height: 200)
        
        filter.startTimeField.text = getTodayZeroTime()
        filter.endTimeField.text = getNowTime()
        
        filter.cancelHandler = {() -> Void in
            self.filterView.isShow = false
            self.showOrHiddenFilter()
        }
        
        filter.confirmHandler = {(startTime, endTime, status, transformType, thirdType) -> Void in
            self.filterView.isShow = false
            self.showOrHiddenFilter()
            self.requestRecordsData()
        }
        
        filter.startTimehandler = {() -> Void in
            self.hideAllComponents()
            self.datePickerView.isStartDate = true
            self.showDatePickerView(show: true)
        }
        
        filter.endTimeHandler = {() -> Void in
            self.hideAllComponents()
            self.datePickerView.isStartDate = false
            self.showDatePickerView(show: true)
        }
        
        filter.allStatusHandler = {() -> Void in
            self.hideAllComponents()
            self.showUserTypeListDialog(type: 0)
        }
        
        filter.transformHandler = {() -> Void in
            self.hideAllComponents()
            self.showUserTypeListDialog(type: 1)
        }
        
        filter.thirdTypeHandler = {() -> Void in
            self.hideAllComponents()
            self.showUserTypeListDialog(type: 2)
        }
        
        return filter
    }()
    
    var datePickerView: NewCustomDatePicker!

    //MARK: -lifeCycle
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view, alpha: 0)
        
        setupView()
        requestRecordsData()
        //额度转换平台
        requestQuatoChange()
    }
    
    //MARK: -UI
    private func setupView() {
        self.view.addSubview(mainTaleView)
        self.view.addSubview(filterView)
        setupRightNavBarItem()
        setUpDatePickerView()
    }
    
    //MARK: -   设置条件选择视图 、 日期选择视图
    private func setUpDatePickerView() {
        
        self.datePickerView = Bundle.main.loadNibNamed("NewCustomDatePicker", owner: self, options: nil)?.last as! NewCustomDatePicker
        self.datePickerView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.datePickerView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.view.addSubview(self.datePickerView)
        self.view.bringSubviewToFront( self.datePickerView)
        
        self.datePickerView.cancelHandler = {() -> Void in
            self.showDatePickerView(show: false)
        }
        
        self.datePickerView.confirmHandler = {(timeString) -> Void in
            
            self.showDatePickerView(show: false)
            if self.datePickerView.isStartDate {
                self.filterView.startTimeField.text = "\(timeString)"
            }else {
                self.filterView.endTimeField.text = "\(timeString)"
            }
        }
    }
    
    //MARK: 显示、隐藏 条件视图、日期选择器
    private func showDatePickerView(show: Bool) {
        self.datePickerView.isHidden = !show
    }
    
    private func setupRightNavBarItem() {
        let button = UIButton(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        button.setTitle("筛选", for: .normal)
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(rightBarButtonItemAction(button:)), for: .touchUpInside)
        button.theme_setTitleColor("Global.barTextColor", forState: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
    }
    
    @objc private func rightBarButtonItemAction(button:UIButton) {
        filterView.isShow = !filterView.isShow
        showOrHiddenFilter()
    }
    
    private func showOrHiddenFilter() {
        UIView.animate(withDuration: 0.35) {
            self.filterView.y = CGFloat(self.filterView.isShow ? KNavHeight : (-200 - KNavHeight))
            self.mainTaleView.y =  CGFloat(self.filterView.isShow ? (200 + KNavHeight) : KNavHeight)
        }
    }
    
    private func hideAllComponents() {
        
    }
    
    //MARK: -Network
    private func requestRecordsData() {
        let params = readyParams()
        request(frontDialog: true, method: .get, url: FEECONVERT_RECORD,params:params,callback: {(resultJson:String,resultStatus:Bool) -> Void in
            
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if let result = FeeConvertRecordWrapper.deserialize(from: resultJson) {
                if result.success{
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    if let content =  result.content {
                        self.rowDatas = content.rows
                        self.mainTaleView.reloadData()
                    }else{
                        showToast(view: self.view, txt: "获取数据失败")
                    }
                }else{
                    if !isEmptyString(str: result.msg){
                        showToast(view: self.view, txt: result.msg)
                    }else{
                        showToast(view: self.view, txt: convertString(string: "获取数据失败"))
                    }
                    if result.code == "0" {
                        loginWhenSessionInvalid(controller: self)
                    }
                }
            }
        })
    }
    
    private func readyParams() -> [String:String] {
        let status = "\(getStatusWithTitle(title: filterView.allStatusField.text ?? ""))"
        //平台code
        let platform = getPlatform(with: filterView.thirdPartyField.text ?? "")
        let type = "\(getTransformMethod(with: filterView.transformField.text ?? ""))"
        
        var startTime = "\(filterView.startTimeField.text ?? getTodayZeroTime())"
        startTime = isEmptyString(str: startTime) ? getTodayZeroTime() : startTime
        var endTime = "\(filterView.endTimeField.text ?? getNowTime())"
        endTime = isEmptyString(str: endTime) ? getNowTime() : endTime
        
        var params = [String:String]()
        if status != "-1" {
            params["status"] = status
        }
        
        if platform != "-1" {
            params["platform"] = platform
        }
        
        if type != "-1" {
            params["type"] = type
        }
        
        params["pageNumber"] = "1"
        params["pageSize"] = "200"
        params["startTime"] = startTime
        params["endTime"] = endTime
        return params
    }
    
    //MARK: -Logic
    //MARK: getCodeWithTitle
    //-1表示不传参数
    private func getStatusWithTitle(title:String) -> Int {
        switch title {
        case status[0]:
            return -1
        case status[1]:
            return 1
        case status[2]:
            return 2
        case status[3]:
            return 3
        default:
            return -1
        }
    }
    
    //-1表示不传参数
    private func getTransformMethod(with title:String) -> Int {
        switch title {
        case transformMethod[0]:
            return -1
        case transformMethod[1]:
            return 2
        case transformMethod[2]:
            return 1
        default:
            return -1
        }
    }
    
    //-1表示不传参数
    private func getPlatform(with title:String) -> String {
        for (index, value) in quatoPlatforms.enumerated() {
            if value.name == title{
                return value.platform
            }
        }
        return "-1"
    }
    
    /** type 0:状态，1：转入方式，2：三方类型 */
    private func showUserTypeListDialog(type:Int){
        var selectedViewDatas = [String]()
        var title = ""
        if type == 0 {
            selectedViewDatas = status
            title = "状态"
        }else if type == 1 {
            selectedViewDatas = transformMethod
            title = "转入方式"
        }else if type == 2 {
            selectedViewDatas = thirdPartyType
            title = "三方类型"
        }
        let selectedView = LennySelectView(dataSource: selectedViewDatas, viewTitle: "请选择\(title)")
//        selectedView.selectedIndex = self.selectedUserTypeIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            guard let weakSelf = self else{return}
            if type == 0 {
                weakSelf.filterView.allStatusField.text = self?.status[index]
            }else if type == 1 {
                weakSelf.filterView.transformField.text = self?.transformMethod[index]
            }else if type == 2 {
                weakSelf.filterView.thirdPartyField.text = self?.thirdPartyType[index]
            }
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension FeeConvertRecordController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.tableViewDisplayWithMessage(datasCount: rowDatas.count, tableView: tableView)
        return rowDatas.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !selectedRows.contains(indexPath.row) {
            selectedRows.append(indexPath.row)
            
            let offsetY = getPoint(with: indexPath, tableView:tableView)
            if offsetY > 0 {
//                tableView.contentOffset = CGPoint.init(x: 0, y: offsetY)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    tableView.scrollRectToVisible(CGRect.init(x: 0, y: offsetY - 1, width: screenWidth, height: 1), animated: true)
                }
                
            }
        }else {
            for (index,value) in selectedRows.enumerated() {
                if value == indexPath.row {
                    selectedRows.remove(at: index)
                    break
                }
            }
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    private func getPoint(with indexPath: IndexPath, tableView:UITableView) -> CGFloat {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "feeConvertRecordCell", for: indexPath)
        if let cell = tableView.cellForRow(at: indexPath) {
            let point = cell.convert(CGPoint.zero, to: self.view)
            
            if point.y + 176 > screenHeight {
                return cell.convert(CGPoint.zero, to: tableView).y + 176
            }
        }
        
        return 0
    }
}

//MARK:网络请求相关
extension FeeConvertRecordController{
    //
    func requestQuatoChange(){
        request(frontDialog: true, url: "/native/quota_conversion_record.do") { (resultJson:String, resultStatus:Bool) in
            if resultStatus{
                print(resultJson)
                guard let feeConverItem = FeeConverPlatformItem.deserialize(from: resultJson) else {return}
                if feeConverItem.success{
                    //所有平台数据
                    self.quatoPlatforms = feeConverItem.content
                    
                    for item in feeConverItem.content{
                        //平台名称
                        self.thirdPartyType.append(item.name)
                    }
                }
            }
        }
    }
}

extension FeeConvertRecordController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "feeConvertRecordCell") as? FeeConvertRecordCell else {
            fatalError("dequeueReusableCell FeeConvertRecordCell error")
        }
        
        cell.selectionStyle = .none
        
        cell.configWith(model: rowDatas[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedRows.contains(indexPath.row) {
            return 176
        }else {
            return 44
        }
    }
    
    func getTableViewHeader() -> UIView {
        let header = UIView()
        header.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 44)
        header.backgroundColor = UIColor.black
        
        let labelWidth:CGFloat = screenWidth * 0.25
        let titles = ["游戏类型","转账金额","转账状态","详情"]
        
        for index in 0...3 {
            let label = UILabel()
            label.frame = CGRect(x: labelWidth * CGFloat(index), y: 0, width: labelWidth, height: 44)
            label.backgroundColor = UIColor.clear
            label.text = titles[index]
            label.textColor = UIColor.white
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 14.0)
            header.addSubview(label)
        }
        
        return header
    }
}









