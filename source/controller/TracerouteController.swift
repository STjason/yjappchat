///
//  TracerouteController.swift
//  gameplay
//
//  Created by admin on 2019/5/25.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Alamofire
import HandyJSON

class TracerouteController: BaseController {
    
    var labelDomain = UILabel()
    var btnDetection = UIButton()
    var textViewResults = UITextView()
    let traceMaxrouteTTL:Int32 = 30
    
    var isFromStartUp:Bool = false
    //给域名加密
    var netDomain:String = ""
    var localIpInfoString:String = ""
    let net = NetworkReachabilityManager()
     var currentNetType:Int32 = 0
    
    /**复制域名*/
    var btnCopyDomain:UIButton = {
        let btn = UIButton()
        btn.isHidden = true
        btn.layer.cornerRadius = 3.0
        btn.backgroundColor = UIColor.red
        btn.setTitle("复制域名", for: .normal)
        btn.addTarget(self, action: #selector(clickCopyDomain), for: .touchUpInside)
        return btn
    }()
    
    /// 路由检测Service
    var traceRouteService:LDNetDiagnoService?
    /// 是否正在检测
    var traceIsDetecting = false {
        didSet {
            DispatchQueue.main.async {
                if self.traceIsDetecting {
                    self.btnDetection.backgroundColor = UIColor.gray
                }else {
                    self.btnDetection.backgroundColor = UIColor.red
                }
            }
        }
    }
    var traceRouteResults = ""
    
    let ISDetecting = "正在检测"
    let CopyResults = "复制结果"
    
    //MARK: - LifeCircle
    override func viewDidLoad() {
        super.viewDidLoad()
        setDomain()
        setupUI()
        setupTraceService()
        //监听网络
        net?.startListening()
        
        net?.listener = { status in
            
            if self.net?.isReachable ?? false{
                switch status{
                case .notReachable:
                    print("the noework is not reachable")
                case .unknown:
                    print("It is unknown whether the network is reachable")
                case .reachable(.ethernetOrWiFi):
                    print("通过WiFi链接")
                    self.currentNetType = 1
                case .reachable(.wwan):
                    //@"2G", @"3G", @"4G", @"5G"
                    print("通过移动网络链接")
                }
                
            } else {
                print("网络不可用")
                self.currentNetType = 0
            }
            
            self.traceRouteService?.startNetDiagnosis(self.currentNetType)
            
        }
//        traceRouteService?.startNetDiagnosis()
    }
    
    ///加密
    func encryption(contents:String) -> String? {
        var array = [UInt8](contents.utf8)
        print(array)//[104, 101, 108, 108, 111]
        for (index,_) in array.enumerated() {
            array[index] += 1
        }
        
        return String(bytes: array, encoding: .utf8)
    }
    
    //MARK: - UI
    private func setupUI() {
        
        self.view.backgroundColor = UIColor.groupTableViewBackground
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        self.title = "路由检测"
        
        labelDomain.textAlignment = .center
        labelDomain.font = UIFont.systemFont(ofSize: 14)
        
        btnDetection.layer.cornerRadius = 3.0
        btnDetection.backgroundColor = UIColor.red
        btnDetection.setTitle(ISDetecting, for: .normal)
        btnDetection.setTitleColor(UIColor.white, for: .normal)
        btnDetection.addTarget(self, action: #selector(clickBtnDetection), for: .touchUpInside)
        
        textViewResults.textAlignment = .left
        textViewResults.isEditable = false
        
        self.view.addSubview(labelDomain)
        self.view.addSubview(btnDetection)
        self.view.addSubview(textViewResults)
        self.view.addSubview(btnCopyDomain)
        
        
        setupLayout()
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.isNavigationBarHidden = false
        //获取本机IP
        self.getLocalIp()
        
        //        navigationController?.navigationBar.barTintColor = UIColor.white
        //
        //        navigationController?.navigationBar.backgroundColor = UIColor.red
        
    }
    
    //设置域名加蜜
    func setDomain(){
//        let domainString =  Encryption.Endcode_AES_ECB(key:"5Po&)11n&v3#M.{:", iv: "0>2$#~*6(~9a7#D$", strToEncode: getDomainUrl())
//        netDomain = domainString
        
        //        netDomain = encryption(contents: getDomainUrl()) ?? ""
        netDomain = encryption(contents: BASE_URL) ?? ""
    }
    //域名解密
    func unlockDomain(domain:String){
        
        let domain = Encryption.Decode_AES_ECB(key:"5Po&)11n&v3#M.{:", iv: "0>2$#~*6(~9a7#D$", strToDecode: netDomain)
        print(domain)
    }
    
    //MARK:获取公网ip
    func getLocalIp(){
        
        request(frontDialog: true, url: "https://logou8.com/domain/getServerIp") {[weak self] (response, status) in
            guard let weakSelf = self else {return}
            
            weakSelf.localIpInfoString = String.init(format:"\nIP信息:%@\n标识：%@",String(describing:response),weakSelf.netDomain)
            let indicateNet = "\n当前本机IP"
            let range = weakSelf.traceRouteResults.range(of: indicateNet)
            weakSelf.traceRouteResults.insert(contentsOf: weakSelf.localIpInfoString, at: (range?.lowerBound)!)
            //            str.insert(",", at: str.endIndex)
            
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                weakSelf.textViewResults.text = weakSelf.traceRouteResults
            }
        }
        
        
//        //        let headers = ["content-type": "text/plain"]
//        var request = URLRequest(url:URL(string: "http://ip-api.com/json/?lang=zh-CN")!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
//        //        request.allHTTPHeaderFields = headers
//
//        let sessionConfigure = URLSessionConfiguration.default
//        sessionConfigure.httpAdditionalHeaders = ["Content-Type": "text/plain"]
//        sessionConfigure.timeoutIntervalForRequest = 30
//        sessionConfigure.requestCachePolicy = .reloadIgnoringLocalCacheData
//        let session = URLSession(configuration: sessionConfigure)
//        let dataTask = session.dataTask(with: request) { (data, respons, error) in
//
//            print(error as Any)
//            if data == nil {return}
//            if respons == nil {return}
//            guard let infoData = data else{return}
//            guard let str = String(data: infoData, encoding: .utf8) else {
//                return
//            }
//
//            class IPInfoModel:HandyJSON {
//                var query = ""
//                var country = ""
//                var city = ""
//                required init() {}
//            }
//
//            if let ipIfoModel = IPInfoModel.deserialize(from: str) {
//                let ipInfo = "{ip:\(ipIfoModel.query), countryOrArea:\(ipIfoModel.country), city:\(ipIfoModel.city)}"
//                self.localIpInfoString = String.init(format:"IP信息:%@\n标识：%@",String(describing: ipInfo),self.netDomain)
//                let indicateNet = "当前本机IP"
//                let range = self.traceRouteResults.range(of: indicateNet)
//                self.traceRouteResults.insert(contentsOf: self.localIpInfoString, at: (range?.lowerBound)!)
//                //            str.insert(",", at: str.endIndex)
//
//                DispatchQueue.main.asyncAfter(deadline: .now()) {
//                    self.textViewResults.text = self.traceRouteResults
//                }
//
//            }else {
//                return
//            }
//        }
//        dataTask.resume()
    }
    
    //MARK: layout
    private func setupLayout() {
        labelDomain.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.view.snp.top).offset(KNavHeight)
            maker.left.equalTo(15)
            maker.right.equalTo(-15)
            maker.height.equalTo(0)
        }
        
        btnDetection.snp.makeConstraints { (maker) in
            maker.top.equalTo(labelDomain.snp.bottom).offset(5)
            maker.left.equalTo(labelDomain)
            maker.height.equalTo(40)
            maker.width.equalTo(120)
        }
        
        //复制
        btnCopyDomain.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalTo(120)
            make.top.equalTo(labelDomain.snp.bottom).offset(5)
            make.left.equalTo(btnDetection.snp.right).offset(10)
        }
        
        textViewResults.snp.makeConstraints { (maker) in
            maker.top.equalTo(btnDetection.snp.bottom).offset(10)
            maker.left.equalTo(15)
            maker.bottom.equalTo(-15)
            maker.right.equalTo(-15)
        }
    }
    
    //MARK: - Data and Logic
    /// 初始化traceRouteService
    private func setupTraceService() {
        var domain = getDormainForTraceroute(url: BASE_URL)
        if domain.length == 0 {
            showToast(view: self.view, txt: "域名不正确")
            btnDetection.isEnabled = false
            btnDetection.backgroundColor = UIColor.gray
            return
        }else{
            //是否能找到：有端口号
            if (domain.contains(":")) {
                let locationIndicateArray = domain.components(separatedBy: ":")
                var url = ""
                //有端口
                // eg: https : //jwfoilwoi93poe236492papp.com : 59789/
                if locationIndicateArray.count > 3{
                    url = locationIndicateArray.first ?? "" + locationIndicateArray[1]
                }else if locationIndicateArray.count == 2{
//                    url = locationIndicateArray.first! + locationIndicateArray.last!
                    url = locationIndicateArray.first!
                }
                domain = url.isEmpty ? domain : url
                
            }
        }
        
        traceRouteService = LDNetDiagnoService.init(appCode: "", appName: getAppName(), appVersion: getVerionName(), userID: "", deviceID: "", dormain: domain, carrierName: "", isoCountryCode: "", mobileCountryCode: "", mobileNetCode: "",traceMaxrouteTTL:traceMaxrouteTTL)
        traceRouteService?.delegate = self
    }
    
    //MARK: - Events
    @objc  override func onBackClick() {
        if traceIsDetecting {
            self.showConfirBackAlert()
        }else {
            if isFromStartUp == true {
                self.dismiss(animated: true, completion: nil)
            }else{
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    private func showConfirBackAlert() {
        let alert = UIAlertController.init(title: nil, message: "正在检测,确定退出?", preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        let confirmAction = UIAlertAction.init(title: "确定", style: .default) { (action) in
            self.traceRouteService?.stopNetDialogsis()
            if self.isFromStartUp == true{
                self.dismiss(animated: true, completion: nil)
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        self.present(alert,animated: true,completion: nil)
    }
    
    
    
    //MARK: - handle
    private func getDormainForTraceroute(url:String) -> String {
        var finalURL = ""
        
        if url.hasPrefix("https://") {
            finalURL = url.subString(start: 8, length: url.length - 8)
        }else if url.hasPrefix("http://") {
            finalURL = url.subString(start: 7, length: url.length - 7)
        }
        
        return finalURL
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension TracerouteController:LDNetDiagnoServiceDelegate {
    
    func netDiagnosisDidStarted() {
        DispatchQueue.main.async {
            self.traceIsDetecting = true
            self.traceRouteResults = ""
            self.btnDetection.setTitle(self.ISDetecting, for: .normal)
        }
    }
    
    func netDiagnosisStepInfo(_ stepInfo: String!) {
        self.traceIsDetecting = true
        traceRouteResults += stepInfo
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.textViewResults.text = self.traceRouteResults
        }
    }
    
    func netDiagnosisDidEnd(_ allLogInfo: String!) {
        DispatchQueue.main.async {
            self.traceIsDetecting = false
            self.btnDetection.setTitle(self.CopyResults, for: .normal)
            self.btnCopyDomain.isHidden = false
        }
    }
    
}
//MARK:响应事件
extension TracerouteController{
    @objc private func clickBtnDetection() {
        if traceIsDetecting {
            showToast(view: self.view, txt: "正在检测中...")
            return
        }
        
        if traceRouteResults.length > 0 {
            UIPasteboard.general.string = traceRouteResults
            showToast(view: self.view, txt: "复制成功")
        }else {
            showToast(view: self.view, txt: "还没有检测结果")
        }
    }
    
    //复制域名
    @objc private func clickCopyDomain(){
        if netDomain.count > 0{
            UIPasteboard.general.string = netDomain
            showToast(view: self.view, txt: "复制成功")
            //            unlockDomain(domain: "")
        }else{
            showToast(view: self.view, txt: "复制失败")
        }
    }
}
