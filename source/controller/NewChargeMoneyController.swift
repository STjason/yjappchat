//
//  NewChargeMoneyController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

//新充值页
class NewChargeMoneyController: BaseController {
    
    
    @IBOutlet weak var payFunctionHeader: UIView!
    @IBOutlet weak var headerBgImage: UIImageView!
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var headerImg:UIImageView!
    @IBOutlet weak var accountTV:UILabel!
    @IBOutlet weak var balanceTV:UILabel!
    @IBOutlet weak var tableView:UITableView!
    var meminfo:Meminfo?
    var payMethods:PayMethodResult!
    var selectPayType = PAY_METHOD_ONLINE;//已选择的支付方式
    
    var use_wap_icons = true // 是否使用网页版充值图标
    
    var datas:[Dictionary<String,String>] = [
        ["img":"MemberPage.Charge.topup_zaixianzhifu","text":"在线支付"],
        ["img":"MemberPage.Charge.topup_yinhangkazhifu","text":"银行卡转账支付"],
        ["img":"MemberPage.Charge.topup_weixinzhifu","text":"微信支付"],["img":"MemberPage.Charge.topup_zhifubaozhifu","text":"支付宝支付"]]

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showAnnounce(controller: self)
    }
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view,alpha: 0)
        
        setupNoPictureAlphaBgView(view: payFunctionHeader,alpha: 0.4)
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        self.title = "充值"
//        headerImg.theme_image = "General.placeHeader"
        headerBgImage.contentMode = .scaleAspectFill
        headerBgImage.clipsToBounds = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView.init()
//        tableView.reloadData()
        // 新需求每次进入页面都必须刷新余额 modify JK 19-09-23
//        if self.meminfo == nil{
//            syncAccount()
//        }else{
//            //赋值帐户名及余额
//            self.updateAccount(memInfo: self.meminfo!)
//            self.syncPayMethod()
//        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        headerView.isUserInteractionEnabled = true
        addGestureRecognizer(headerView: headerView)
        
        headerImg.layer.cornerRadius = 30.0
        headerImg.layer.masksToBounds = true
        headerImg.contentMode = UIView.ContentMode.scaleAspectFill
        
        //更新头像
        if let sysconfig = getSystemConfigFromJson(){
            if sysconfig.content != nil{
                let logoImg = sysconfig.content.member_page_logo_url
                use_wap_icons = sysconfig.content.charge_icon_wap_switch == "on"
                if isEmptyString(str: logoImg){
                    if YiboPreference.getCACHEAVATARdata().count >= 1 {
                       headerImg.image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
                    }else{
                       headerImg.theme_image = "General.placeHeader"
                    }
                    headerBgImage.theme_image = "General.personalHeaderBg"
                }else{
                    updateAppLogo(icon: self.headerImg)
                }
            }
        }
        updateHeaderBgLogo(imageView: headerBgImage)
        print("use wap icon flag === ",use_wap_icons)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        syncAccount()
    }
    
    private func addGestureRecognizer(headerView:UIView) {
        let longPress = UITapGestureRecognizer(target: self, action: #selector(self.pressClick))
        headerView.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(longPress)
    }
    
    @objc func pressClick(){
        self.navigationController?.pushViewController(UserInfoController(), animated: true)
    }
    
    func updateAccount(memInfo:Meminfo) -> Void {
        var accountNameStr = ""
        var leftMoneyName = ""
        if !isEmptyString(str: memInfo.account){
            accountNameStr = memInfo.account
        }else{
            accountNameStr = "暂无名称"
        }
        accountTV.text = accountNameStr
        if !isEmptyString(str: memInfo.balance){
            leftMoneyName = "余额:\(memInfo.balance)"
        }else{
            leftMoneyName = "0"
        }
        balanceTV.text = String.init(format: "%@元",  leftMoneyName)
    }
    
    // 遍历数组,取出公告赋值
    private func forArrToAlert(notices: Array<NoticeResult>) {
        
        var noticesP = notices
        noticesP = noticesP.sorted { (noticesP1, noticesP2) -> Bool in
            return noticesP1.sortNum < noticesP2.sortNum
        }
        
        var models = [NoticeResult]()
        for index in 0..<noticesP.count {
            let model = noticesP[index]
            if model.isDeposit {
                models.append(model)
            }
        }
        
        if models.count > 0 {
            let weblistView = WebviewList.init(noticeResuls: models)
            weblistView.show()
        }
    }
    
    
    
    private func showAnnounce(controller:BaseController) {
        if YiboPreference.isShouldAlert_isAll() == "" {
            YiboPreference.setAlert_isAll(value: "on" as AnyObject)
        }
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if !shouldShowNoticeWIndow() {
                return
            }
        }
        //获取公告弹窗内容
        controller.request(frontDialog: false, url:ACQURIE_NOTICE_POP_URL,params:["code":19],
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            
                            if let result = NoticeResultWraper.deserialize(from: resultJson){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                PopupAlertListView(resultJson: resultJson,controller:self,not:"",anObject:nil,from: 2)
                            }
        })
    }
}

extension NewChargeMoneyController{
    
    func syncAccount() -> Void {
        request(frontDialog: true,method: .get,loadTextStr: "同步帐号信息...",url:MEMINFO_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        return
                    }
                    if let result = MemInfoWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let memInfo = result.content{
                                //赋值帐户名及余额
                                self.updateAccount(memInfo: memInfo)
                                self.syncPayMethod()
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "同步帐户信息失败"))
                            }
                            if result.code == 0  || result.code == -1{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    func updatePayListAfterSyncMethods(){
        if self.payMethods == nil{
            return
        }
        self.datas.removeAll()
        if !self.payMethods.online.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.topup_zaixianzhifu","text":"在线支付"])
            }else{
                self.datas.append(["img":"topup_wap_online","text":"在线支付"])
            }
        }
        if !self.payMethods.bank.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.topup_yinhangkazhifu","text":"银行卡转账支付"])
            }else{
                self.datas.append(["img":"topup_wap_bankcard","text":"银行卡转账支付"])
            }
        }
        if !self.payMethods.fast.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.topup_weixinzhifu","text":"微信支付"])
            }else{
                self.datas.append(["img":"topup_wap_weixin","text":"微信支付"])
            }
        }
        if !self.payMethods.fast_qq.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.qqfu","text":"QQ支付"])
            }else{
                self.datas.append(["img":"topup_wap_qq","text":"QQ支付"])
            }
        }
        if !self.payMethods.fast2.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.topup_zhifubaozhifu","text":"支付宝支付"])
            }else{
                self.datas.append(["img":"topup_wap_zfb","text":"支付宝支付"])
            }
        }
        if !self.payMethods.fast_ysf.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.yunshanfu","text":"云闪付"])
            }else{
                self.datas.append(["img":"topup_wap_ysf","text":"云闪付"])
            }
        }
        if !self.payMethods.fast_meituan.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"MemberPage.Charge.meituan","text":"美团"])
            }else{
                self.datas.append(["img":"topup_wap_meituan","text":"美团"])
            }
        }
        if !self.payMethods.weixin_alipay.isEmpty{
            if !use_wap_icons{  
                self.datas.append(["img":"MemberPage.Charge.topup_wecharAirpay","text":"微信|支付宝"])
            }else{
                self.datas.append(["img":"topup_wap_weixin_alipay","text":"微信|支付宝"])
            }
        }
        if !self.payMethods.USDT.isEmpty{
            if !use_wap_icons{
                self.datas.append(["img":"topup_USDT","text":"USDT"])
            }else{
                self.datas.append(["img":"topup_USDT","text":"USDT"])
            }
        }
        if !self.payMethods.PAYPAL.isEmpty{
            if !self.payMethods.PAYPAL.isEmpty{
                if !use_wap_icons{
                    self.datas.append(["img":"topup_PAYPAL","text":"PAYPAL"])
                }else{
                    self.datas.append(["img":"topup_PAYPAL","text":"PAYPAL"])
                }
            }
        }
        self.tableView.reloadData()
    }
    
    func syncPayMethod() -> Void {
        request(frontDialog: true,method: .get,loadTextStr: "获取充值方式中...",url:GET_PAY_METHODS_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        return
                    }
                    if let result = PayMethodWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let payMethods = result.content{
                                self.payMethods = payMethods
                                self.updatePayListAfterSyncMethods()
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取充值方式失败"))
                            }
                            if result.code == 0 || result.code == -1{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    
}

extension NewChargeMoneyController : UITableViewDelegate,UITableViewDataSource{
    
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
//    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "payCell") as? PayListCell  else {
            fatalError("The dequeued cell is not an instance of PayListCell.")
        }
        
        let model = self.datas[indexPath.row]
        cell.setModel(model: model, theme: !use_wap_icons)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let row = indexPath.row
        let data = self.datas[row]
        let name = data["text"]
        let version = switch_payVersion()
        if version == "V1" || version == "V3" {
            chooseChargeViewV1(name: name, version:version)
        }else if version == "V2" || version ==  "V4"{
            chooseChargeViewV2(name: name,payIcon: data["img"]!,version: version)
        }
    }
    
    private func chooseChargeViewV2(name: String?,payIcon: String,version:String) {
        if let methods = self.payMethods{
            if name == "在线支付"{
//                goOnlinePayControllerV2(onlines: methods.online)
                
                //wap版
                //先通道
                if name == "在线支付"{
                    if version == "V2" {
                        goOnlinePayControllerV2(onlines: methods.online)
                    }else if version == "V4"{
                        //先原生
                        goOnlinePayMethodV2Controller()
                    }
            }
            }else if name == "微信支付"{
//                goWeixinPayController(weixins: methods.fast)
                goAlipayPayControllerV2(alipays: methods.fast,title: name!,payIcon: payIcon)
            }else if name == "支付宝支付"{
                goAlipayPayControllerV2(alipays: methods.fast2,title: name!,payIcon: payIcon)
            }else if name == "银行卡转账支付"{
                goBankPayControllerV2(banks: methods.bank)
            }else if name == "QQ支付"{
                goAlipayPayControllerV2(alipays: methods.fast_qq,title: name!,payIcon: payIcon)
            }else if name == "云闪付"{
                goAlipayPayControllerV2(alipays: methods.fast_ysf,title: name!,payIcon: payIcon)
            }else if name == "美团"{
                goAlipayPayControllerV2(alipays: methods.fast_meituan,title: name!,payIcon: payIcon)
            }else if name == "微信|支付宝"{
                goAlipayPayControllerV2(alipays: methods.weixin_alipay,title: name!,payIcon: payIcon)
            }
        }
    }
    
    //MARK: 先方法版-原生版
    private func goOnlinePayMethodV1Controller() {
        let vc = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v2") as! OnlineMethodPayV1Controller
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 先方法版-wap版
    private func goOnlinePayMethodV2Controller() {
        let vcP = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v4") as! OnlineMethodPayV2Controller
        self.navigationController?.pushViewController(vcP, animated: true)
    }
    
    private func chooseChargeViewV1(name: String?,version:String) {
        if let methods = self.payMethods{
            if name == "在线支付"{
//                goOnlinePayController(onlines: methods.online)
                
                //原生版
                //先通道版
                if version == "V1" {
                    goOnlinePayController(onlines: methods.online)
                }else if version == "V3" {
                    //先方法版
                    goOnlinePayMethodV1Controller()
                }
                
            }else if name == "微信支付"{
                goWeixinPayController(weixins: methods.fast)
            }else if name == "支付宝支付"{
                goAlipayPayController(alipays: methods.fast2,title: name!)
            }else if name == "银行卡转账支付"{
                goBankPayController(banks: methods.bank)
            }else if name == "QQ支付"{
                goAlipayPayController(alipays: methods.fast_qq,title: name!)
            }else if name == "云闪付"{
                goAlipayPayController(alipays: methods.fast_ysf,title: name!)
            }else if name == "美团"{
                goAlipayPayController(alipays: methods.fast_meituan,title: name!)
            }else if name == "微信|支付宝"{
                goAlipayPayController(alipays: methods.weixin_alipay,title: name!)
            }
        }
    }
    
    private func goWeixinPayController(weixins:[FastPay]){
        let vc = UIStoryboard(name: "fast_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "fast_pay_info") as! FastPayInfoController
        vc.fasts = weixins
        vc.is_wx = true
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goAlipayPayController(alipays:[FastPay],title: String){
        let vc = UIStoryboard(name: "fast_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "fast_pay_info") as! FastPayInfoController
        vc.payFunction = title
        vc.fasts = alipays
        vc.is_wx = false
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goAlipayPayControllerV2(alipays:[FastPay],title: String,payIcon: String){
        let vc = UIStoryboard(name: "fast_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "fast_pay_info_v2") as! FastPayInfoControllerV2
        vc.payFunction = title
        vc.fasts = alipays
        vc.payIcon = payIcon
        vc.is_wx = title == "微信支付"
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    private func goBankPayController(banks:[BankPay]){
        let vc = UIStoryboard(name: "bank_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "bank_pay_info") as! BankPayInfoController
        let config = getSystemConfigFromJson()
        vc.banks = banks
        vc.meminfo = meminfo
        vc.propmtString = didHtml(strhtml: (config?.content.pay_tips_deposit_general)!).trimmingCharacters(in: .whitespacesAndNewlines)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goBankPayControllerV2(banks:[BankPay]){
        let vc = UIStoryboard(name: "bank_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "bank_pay_info_v2") as! BankPayInfoControllerV2
        vc.banks = banks
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    private func goOnlinePayController(onlines:[OnlinePay]){
        let vc = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay") as! OnlinePayInfoController
        vc.onlines = onlines
        vc.meminfo = meminfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goOnlinePayControllerV2(onlines:[OnlinePay]){
//        let vc = UIStoryboard(name: "online_pay_info_page_v2",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v2") as! OnlinePayInfoControllerV2
//        vc.onlines = onlines
//        vc.meminfo = meminfo
//        self.navigationController?.pushViewController(vc, animated: true)
        
        let vcP = UIStoryboard(name: "online_pay_info_page",bundle:nil).instantiateViewController(withIdentifier: "online_pay_v3") as! OnlineNewPayInfoControllerV2
        vcP.onlines = onlines
        vcP.meminfo = meminfo
        self.navigationController?.pushViewController(vcP, animated: true)
    }
    
}

extension NewChargeMoneyController{
    
    func didHtml(strhtml:String)->String{
        let str = strhtml.replacingOccurrences(of:"</span>", with:"")
        var strhtml1="";
        var text:NSString?
        let scanner = Scanner(string: str)
        while scanner.isAtEnd == false {
            scanner.scanUpTo("<", into: nil)
            scanner.scanUpTo(">", into: &text)
            strhtml1 = strhtml.replacingOccurrences(of:"\(text == nil ? "" : text!)>", with: "")
        }
        let str1 = strhtml1.replacingOccurrences(of:"</span>", with:"")
        let str2 = str1.replacingOccurrences(of:" ", with:"")
        return str2
    }
}












