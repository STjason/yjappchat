//
//  BetPageController.swift
//  pageControllerDemo
//
//  Created by admin on 2019/5/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class BetPageController: BaseController {
    
    private var pageFinishedAnimatedHandler:((Int) -> Void)?
    
    private var pageController:UIPageViewController!
    private var subControllers:[UIViewController] = []
    private var latestIndex = 0
    private var segment = UISegmentedControl()
    
    var titleBtn:UIButton?
    var titleIndictor:UIImageView = UIImageView.init()
    var titleView:BetSegView?
    var groupCode = ""
    
    var right_top_menu:SwiftPopMenu!//右上角悬浮框
    var version_top_menu:SwiftPopMenu!//版本切换悬浮框
    var tag:Int = 1
    var right_top_datasources = [(icon:"TouzhOffical.SwiftPopMenu.firstImage",title:"投注记录"),(icon:"TouzhOffical.SwiftPopMenu.secodeImage",title:"历史开奖"),(icon:"TouzhOffical.SwiftPopMenu.thirdImage",title:"玩法说明"),(icon:"TouzhOffical.SwiftPopMenu.fourthImage",title:"设置"),(icon:"TouzhOffical.SwiftPopMenu.fiveImage",title:"切换主题"),
                                 (icon:"TouzhOffical.SwiftPopMenu.sixImage",title:"切换音效"),
                                 ]
    

    
    var firstIn = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        //设置导航栏切换彩种和聊天室
        if switch_chatRoomOn() {
            setupTitleView()
        }else{
            customTitleView()
        }
        
        setupRightNaviItem()
     
        setupHandler()
    }
    
    
    @objc override func onBackClick() {
        super.onBackClick()
        disBetTimer()
    }
    
    /// 推出控制器时，计时器没有被销毁，这里需要手动invalidate
    private func disBetTimer() {
        let vc = self.getBetController()
        if let timer = vc.disableBetCountDownTimer {
            timer.invalidate()
            vc.disableBetCountDownTimer = nil
        }
        
        if let timer = vc.endlineTouzhuTimer {
            timer.invalidate()
            vc.endlineTouzhuTimer = nil
        }
        
        if let timer = vc.lastKaiJianResultTimer {
            timer.invalidate()
            vc.lastKaiJianResultTimer = nil
        }
        
        if let timer = vc.judgeTimer {
            timer.invalidate()
            vc.judgeTimer = nil
        }
    }
    
    private func updateHasChatTitleView(title:String) {
        if getBetController().isPeilvVersion() {
            titleView?.leftButton.setTitle(title, for: .normal)
            titleView?.leftButton.setTitle(title, for: .selected)
        }else {
            titleView?.leftButton.setTitle(title, for: .normal)
            titleView?.leftButton.setTitle(title, for: .selected)
        }
    }
    
    private func setupHandler() {
        let vc = getBetController()
        
        vc.hasNotLoginHandler = {() in
            loginWhenSessionInvalid(controller: self)
        }
        
        vc.gotoBetNewPeilvHandler = {(datas, peilvs,lhcLogic, subPlayName, subPlayCode, cpTypeCode, cpBianHao, current_rate,cpName, cpVersion,lotteryICON) in
            openPeilvBetOrderPage(controller: self, order: datas, peilvs: peilvs, lhcLogic: lhcLogic, subPlayName: subPlayName, subPlayCode: subPlayCode, cpTypeCode: cpTypeCode, cpBianHao: cpBianHao, current_rate: current_rate, cpName: cpName,cpversion: cpVersion,icon:lotteryICON)
        }
        
        vc.gotoBetNewOfficalHandler = {(_ data:[OrderDataInfo],_ lotCode:String,_ lotName:String,_ subPlayCode:String,_ subPlayName:String,
            _ cpTypeCode:String,_ cpVersion:String,_ officail_odds:[PeilvWebResult],_ icon:String,_ meminfo:Meminfo?,_ officalBonus:Double?) in
            
            openBetOrderPage(controller: self, data: data,lotCode: lotCode,lotName: lotName,subPlayCode: subPlayCode,subPlayName: subPlayName,cpTypeCode: cpTypeCode,cpVersion:cpVersion,officail_odds:officail_odds,icon: icon,meminfo:meminfo,officalBonus:officalBonus)
            
        }
        
        if switch_chatRoomOn() {
            if let vc = getBetController() as? TouzhPlainController {
                vc.updateTitleHandler = {(title) in
                    self.updateHasChatTitleView(title: title)
                }
            }
            
            if let vc = getBetController() as? TouzhNewController {
                vc.updateTitleHandler = {(title) in
                    self.updateHasChatTitleView(title: title)
                }
            }
            
            if self.subControllers.count > 1 {
                if let chatVC = subControllers[1] as? ChatViewController {
                    chatVC.hasNotLoginHandler = {() in
                        loginWhenSessionInvalid(controller: self)
                    }
                }
            }
        }
        
        pageFinishedAnimatedHandler = {[weak self](index) in
            guard let weakSelf = self else {
                return
            }
            weakSelf.titleView?.setupBtnAfterSelected(index: index)
            
            if index == 1 {
                weakSelf.navigationItem.rightBarButtonItem = nil
            }else {
                weakSelf.setupRightNaviItem()
            }
        }
        
        getBetController().viewLotRecordHandler = {() in
            let page = GoucaiQueryController()
            page.isAttachInTabBar = false
            self.navigationController?.pushViewController(page, animated: true)
        }
        
        getBetController().gotoResultsPageHandler = {(cpTypeCode,cpBianHao) in
            let moduleStyle = YiboPreference.getMallStyle()
            let (xibname,_) = selectMainStyleByModuleID(styleID: moduleStyle)
            
            let page = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: "notice") as! LotteryResultsController
            
            page.lotType = cpTypeCode
            page.isAttachInTabBar = false
            page.code = cpBianHao
            
            self.navigationController?.pushViewController(page, animated: true)
        }
        
        getBetController().goTrendChartHandler = {(cpTypeCode,cpBianHao) in
            let moduleStyle = YiboPreference.getMallStyle()
            let (xibname,_) = selectMainStyleByModuleID(styleID: moduleStyle)
            
            let page = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: "notice") as! LotteryResultsController
            
            page.lotType = cpTypeCode
            page.isAttachInTabBar = false
            page.code = cpBianHao
            page.offSet = kScreenWidth
            self.navigationController?.pushViewController(page, animated: true)
        }
        
        getBetController().playIntroduceHandler = {(cpBianHao,groupName,code) in
            var url = ""
            if self.getBetController().isPeilvVersion(){
                
                url = String.init(format: "%@%@?code=%@&name=%@&lottery=%@", BASE_URL,PLAY_INTRODUCE_CREDIT_RUL,code,groupName,cpBianHao)
            }else{
                url = String.init(format: "%@%@?code=%@", BASE_URL,PLAY_INTRODUCE_RUL,cpBianHao)
                
            }
            
            var charSet = CharacterSet.urlQueryAllowed
            charSet.insert(charactersIn: "#")
            url = url.addingPercentEncoding(withAllowedCharacters: charSet)!
            
            if let config = getSystemConfigFromJson(){
                if config.content != nil{
                    let play_show_style = config.content.play_introduce_show_style
                    if play_show_style == "V1"{
                        openActiveDetail(controller: self, title: "玩法说明", content: "", foreighUrl: url,outsideOpen: true)
                    }else if play_show_style == "V2"{
                        let result = NoticeResult()
                        result.content = url
                        let models:[NoticeResult] = [result]
//                        let weblistView  =  WebviewList.init(noticeResuls: models, urlContent: true, dialogTitle: "玩法说明")
                        let weblistView = WebviewList.init(noticeResuls: models, inSideDatas: nil, urlContent: true, dialogTitle: "玩法说明", type: 1)
                        weblistView.show()
                    }
                }
            }
        }
        
        getBetController().openSettingHandler = {() in
            openSetting(controller: self,from: 1)
        }
        
        getBetController().switchSoundEffectHandler = {() in
            self.getBetController().switchSoundEffect()
        }
        
        getBetController().switchThemeBetVCHandler = {(lotdata) in
//            self.getBetController().switchTheme()
            self.switchThemeBetVC(lotdata: lotdata)
        }
        getBetController().gotoLongDragonPageHandler = {[weak self](gameCode: String, groupName: String) in
            let longDragonVc = LongDragonViewController()
            longDragonVc.groupName = groupName
            longDragonVc.gameCode = gameCode
            self?.navigationController?.pushViewController(longDragonVc, animated: true)
        }
    }
    
    //MARK: 简约主题和其他主题的切换
    private func switchThemeBetVC(lotdata:LotteryData) {

        let currentThmeIndex = YiboPreference.getCurrentThme()
        let vc = self.getBetController()
        let selectedView = LennySelectView(dataSource: themes, viewTitle: "主题风格切换")
        selectedView.selectedIndex = currentThmeIndex
        
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            
            let themeStr = getCurrentThemeListNameWithName(name:content)

            if let selfVC = self
            {
                let vcArrayP = selfVC.navigationController?.viewControllers

                if let vcArray = vcArrayP
                {
                    if let indexOfVC = vcArray.index(of: selfVC)
                    {
                        var vcArrayM = vcArray
                        let chatVC = ChatViewController()
                        chatVC.chatViewType = .ChatFromBetPage
                        
                        let isNativeChat = switch_nativeOrWapChat()
                        
                          if vc is TouzhPlainController && themeStr != "FrostedPlain"
                        {
                            
                            self?.disBetTimer()
                            
                            let touzhuController = UIStoryboard(name: "touzh_page",bundle:nil).instantiateViewController(withIdentifier: "touzhu_v2")
                            let touzhuPage = touzhuController as! TouzhNewController
                            touzhuPage.lotData = lotdata
                            touzhuPage.groupCode = lotdata.groupCode

                            
                            var pageController:BetPageController?
                            
                            if isNativeChat {
                                pageController = BetPageController.init(subControllers:[touzhuPage], latestIndex: 0, scrollEnabled: false)
                            }else {
                                pageController = BetPageController.init(subControllers:switch_chatRoomOn() && !switch_nativeOrWapChat() ? [touzhuPage,chatVC] : [touzhuPage], latestIndex: 0, scrollEnabled: false)
                            }
                            
                            vcArrayM.remove(at: indexOfVC)
                            selfVC.navigationController?.setViewControllers(vcArrayM, animated: false)

                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                let last = vcArrayM.last
                                if let wraperPage = pageController {
                                    last?.navigationController?.pushViewController(wraperPage, animated: false)
                                }
                            })
                            
                        }else if vc is TouzhNewController && themeStr == "FrostedPlain"
                        {
                            self?.disBetTimer()
                            let touzhuController = UIStoryboard(name: "touzh_page",bundle:nil).instantiateViewController(withIdentifier: "touzhu_v3")
                            let touzhuPage = touzhuController as! TouzhPlainController
                            touzhuPage.lotData = lotdata
                            
                            var pageController:BetPageController?
                            
                            if isNativeChat {
                                pageController = BetPageController.init(subControllers:[touzhuPage], latestIndex: 0, scrollEnabled: false)
                            }else {
                               pageController = BetPageController.init(subControllers:switch_chatRoomOn() && !switch_nativeOrWapChat() ? [touzhuPage,chatVC] : [touzhuPage], latestIndex: 0, scrollEnabled: false)
                            }
                            
                            vcArrayM.remove(at: indexOfVC)
                            selfVC.navigationController?.setViewControllers(vcArrayM, animated: false)

                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {

                                let last = vcArrayM.last
                                if let wraperPage = pageController {
                                    last?.navigationController?.pushViewController(wraperPage, animated: false)
                                }
                            })
                            
                        }
                    }
                }
            }

            ThemeManager.setTheme(plistName: themeStr, path: .mainBundle)

            self?.getBetController().switchThemeHandler?()

            YiboPreference.setCurrentTheme(value: index as AnyObject)
            YiboPreference.setCurrentThemeByName(value: themeStr as AnyObject)
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
        }
    }
    
    convenience init(subControllers:[UIViewController],latestIndex:Int = 0,scrollEnabled:Bool = true) {
        self.init()
        self.subControllers = subControllers
        self.latestIndex = latestIndex
        
        configPageController(isScrollEnabled: scrollEnabled,latestIndex:self.latestIndex)
    }
    
    //MARK: 导航栏相关控件 和逻辑
    @objc func showRightTopMenuWhenResponseClick(ui:UIButton) -> Void {
     self.getBetController().showRightTopMenuWhenResponseClick(ui: ui)
    }

    //MARK:  开奖结果
    @objc func gotoResultsPage(){

        let moduleStyle = YiboPreference.getMallStyle()
        let (xibname,_) = selectMainStyleByModuleID(styleID: moduleStyle)

        let page = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: "notice") as! LotteryResultsController

        //        let page = LotteryResultsController()
        page.lotType = getBetController().cpTypeCode //cpTypeCode
        page.isAttachInTabBar = false
        page.code = getBetController().cpBianHao //self.cpBianHao
        self.navigationController?.pushViewController(page, animated: true)
    }
    
    open func getBetController() -> TouzhuBaseCntroller {
        let subCtrl =  subControllers[0] as! TouzhuBaseCntroller
        subCtrl.groupCode = groupCode
        return subCtrl
    }
    
    //MARK: navTitleView,聊天室开启时
    private func setupTitleView() {

        self.titleView = Bundle.main.loadNibNamed("BetSegView", owner: self, options: nil)?.last as? BetSegView
        
        if getBetController().isPeilvVersion() {
            let title = switchOnlyPeillvSingleForceOffical() ? "官方下注" : "信用下注"
            
            titleView?.leftButton.setTitle(title, for: .normal)
            titleView?.leftButton.setTitle(title, for: .selected)
        }else {
            titleView?.leftButton.setTitle("官方下注", for: .normal)
            titleView?.leftButton.setTitle("官方下注", for: .selected)
        }
        
        getBetController().lotCpCodeChangedHandler = { [weak self](lotTypeCode) in
            guard let weakSelf = self else{return}
            if isSixMark(lotCode: lotTypeCode) || onlyHasVersionOfficalOrPeilv() {
                weakSelf.titleView?.leftTipImage.image = nil
            }else {
                weakSelf.titleView?.leftTipImage.theme_image = "SegmentColor.downArrow"
            }
        }
        
        titleView?.clickButtonHandler = {[weak self](index) in
            guard let weakSelf = self else{return}
            weakSelf.updatePageIndex(index: index, frist: false)
        }
        self.navigationItem.titleView = self.titleView
    }
    
    //聊天室关闭下，自定义标题栏，方便点击标题栏切换彩票版本
    func customTitleView() -> Void {
        self.navigationItem.titleView?.isUserInteractionEnabled = true
        let titleView = UIView.init(frame: CGRect.init(x: kScreenWidth/4, y: 0, width: kScreenWidth/2, height: 44))
        titleBtn = UIButton.init(frame: CGRect.init(x: titleView.bounds.width/2-56, y: 0, width: 100, height: 44))
        titleBtn?.isUserInteractionEnabled = false
        titleView.addSubview(titleBtn!)
        if getLotVersionConfig() == VERSION_V1V2{
            
            titleIndictor = UIImageView.init(frame: CGRect.init(x:(titleBtn?.x ?? 0) + (titleBtn?.bounds.width)!, y: 22-6, width: 12, height: 12))
            
            titleIndictor.theme_image = "FrostedGlass.Touzhu.navDownImage"
            titleView.addSubview(titleIndictor)
        }
        titleBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        update_title_label()
        self.navigationItem.titleView = titleView
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(titleClickEvent(recongnizer:)))
        self.navigationItem.titleView?.addGestureRecognizer(tap)
    }
    
    //处理顶部标题栏点击事件
    @objc func titleClickEvent(recongnizer:UIPanGestureRecognizer) -> Void {
        changeLotVersionPop()
    }
    
    //切换官方，信用弹窗的点击
    private func changeLotVersionPop() {
        //显示彩票版本切换弹出框
        if getLotVersionConfig() == VERSION_V1V2 {
            if !isSixMark(lotCode: getBetController().cpBianHao) && !isKuaiLeShiFeng(lotType: getBetController().cpTypeCode) { //非快乐彩，非六合彩
                if YiboPreference.getCurrentThmeByName() ==  "FrostedPlain" {
                    if let vc = getBetController() as? TouzhPlainController {
                        vc.backLotterTitleCloser = { [weak self] titleString in
                            guard let weakSelf = self else {return}
                            weakSelf.titleView?.leftButton.setTitle(titleString, for: .selected)
                            weakSelf.titleView?.leftButton.setTitle(titleString, for: .normal)
                            weakSelf.titleBtn?.setTitle(titleString, for: .normal)
                        }
                        vc.showVersionSwitchMenuWhenResponseClick()
                    }
                }else {
                    if let vc = getBetController() as? TouzhNewController {
                        vc.backLotterTitleCloser = {[weak self] titleString in
                            guard let weakSelf = self else {return}
                            weakSelf.titleView?.leftButton.setTitle(titleString, for: .selected)
                            weakSelf.titleView?.leftButton.setTitle(titleString, for: .normal)
                            weakSelf.titleBtn?.setTitle(titleString, for: .normal)
                        }
                        vc.showVersionSwitchMenuWhenResponseClick()
                    }
                }
            }
        }else if stationCode() == v023_ID {
            if let vc = subControllers[0] as? TouzhPlainController {
                vc.showLotsWindow()
                return
            }

            if let vc = subControllers[0] as? TouzhNewController {
                vc.showLotsWindow()
            }
        }
    }
    
    func update_title_label() {
        var title = ""
        if stationCode() == v023_ID && getLotVersionConfig() != VERSION_V1V2 {
            let vc = getBetController()
            vc.updateLotName = {[weak self] (name) in
                guard let weakSelf = self else {return}
                title = name
                weakSelf.titleBtn?.setTitle(title, for: .normal)
            }
        }else {
            title = getBetController().cpVersion == VERSION_1 ? "官方下注" : "信用下注"
            if getBetController().lotData?.lotType == 6 || getBetController().lotData?.lotType == 66
            {
                title = "信用下注"
                titleIndictor.image = UIImage.init(named: "")
            }else
            {
                titleIndictor.theme_image = "FrostedGlass.Touzhu.navDownImage"
            }
            
            titleBtn?.theme_setTitleColor("Global.barTextColor", forState: .normal)
            
            if switchOnlyPeillvSingleForceOffical() {
                title = "官方下注"
            }
        }
                
        titleBtn?.setTitle(title, for: .normal)
    }
    
    //MARK: - 小助手
    private func setupRightNaviItem() {
        let moreBtn = UIButton(type: .custom)
        moreBtn.frame = CGRect.init(x: 0, y: 0, width: 60, height: 44)
        
        if #available(iOS 11, *){} else {
            if #available(iOS 10, *) {
                moreBtn.frame = CGRect.init(x: 0, y: 0, width: 65, height: 44)
            }
        }
        
        moreBtn.setTitle("小助手", for: .normal)
        moreBtn.theme_setTitleColor("Global.barTextColor", forState: .normal)
        moreBtn.contentHorizontalAlignment = .right
        moreBtn.addTarget(self, action: #selector(showRightTopMenuWhenResponseClick(ui:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: moreBtn)
    }
    
    private func updatePageIndex(index:Int,frist:Bool) {
        
        if index == 0 && stationCode() == v023_ID && getLotVersionConfig() != VERSION_V1V2 && !frist {
            
            if index == 1 {
                self.navigationItem.rightBarButtonItem = nil
            }else {
                self.setupRightNaviItem()
            }
            
            if self.latestIndex == index {
                if let vc = subControllers[0] as? TouzhPlainController {
                    vc.showLotsWindow()
                    return
                }

                if let vc = subControllers[0] as? TouzhNewController {
                    vc.showLotsWindow()
                }
            }
            
            pageController.setViewControllers([subControllers[index]], direction: index > latestIndex ? .forward : .reverse, animated: true) { (completed) in
                self.latestIndex = index
            }
            
        }else
            if switch_nativeOrWapChat() {
            if index == 0 {
                if index == latestIndex && index == 0 && !frist {
                    changeLotVersionPop()
                }else {
                    pageController.setViewControllers([subControllers[index]], direction: index > latestIndex ? .forward : .reverse, animated: true) { (completed) in
                        self.latestIndex = index
                    }
                }
            }else if index == 1 {
                if let vc = getChatController() {
                    vc.backPreviousViewHandler = { () in
                        self.updatePageIndex(index: 0, frist: true)
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else {
            if index == 1 {
                self.navigationItem.rightBarButtonItem = nil
            }else {
                self.setupRightNaviItem()
            }
            
            if index == latestIndex && index == 0 && !frist {
                changeLotVersionPop()
            }else {
                pageController.setViewControllers([subControllers[index]], direction: index > latestIndex ? .forward : .reverse, animated: true) { (completed) in
                    self.latestIndex = index
                }
            }
        }
        
        firstIn = false
    }
    
    private func configPageController(isScrollEnabled:Bool,latestIndex:Int) {
        pageController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController.delegate = self
        pageController.dataSource = self
        
        self.view.addSubview(pageController.view)
        isAllowedScroll(isScrollEnabled: isScrollEnabled, pageCOntroller: pageController)
        updatePageIndex(index: latestIndex, frist: true)
    }
    
    //MARK: - page视图是否可以滚动
    private func isAllowedScroll(isScrollEnabled:Bool,pageCOntroller:UIPageViewController) {
        if let view = getLowestLevelScrollViewFrom(pageController: pageCOntroller) {
            view.isScrollEnabled = isScrollEnabled
        }
    }
    
    private func getLowestLevelScrollViewFrom(pageController:UIPageViewController) -> UIScrollView? {
        for (_, view) in pageController.view.subviews.enumerated() {
            if view is UIScrollView {
                return view as? UIScrollView
            }
        }
        
        return nil
    }
    
    //MARK: -others
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}



extension BetPageController:UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let controllers = pageViewController.viewControllers {
            if let index = subControllers.index(of: controllers[0]) {
                latestIndex = index
                
                pageFinishedAnimatedHandler?(latestIndex)
            }
        }
    }
}



extension BetPageController:UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = subControllers.index(of: viewController) {
            return index != subControllers.count - 1 ? subControllers[index + 1] : nil
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = subControllers.index(of: viewController) {
            return index != 0 ? subControllers[index - 1] : nil
        }
        return nil
    }
    
}

