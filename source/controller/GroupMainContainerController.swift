//
//  SegmentMallContainerController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/14.
//  Copyright © 2018年 yibo. All rights reserved.
// 首页

import UIKit
import GTMRefresh
import Kingfisher


class GroupMainContainerController: BaseContainerController {
    /** 中秋活动入口按钮 */
    var _activityBtn : FloatDragButton?
    
    var attachTabBar = false
    private var titles: [String] = []
    var allDatas = [LotteryData]()//all the game data
    
    let leftNaviButtonW: CGFloat = 96
    let leftNaviButtonH: CGFloat = 40
    private var viewControllers: [BaseController] = []
    
    var semiCircelView:SemicircleMenuView!
    /** 临时活动数据 */
    var generalActivityModel : GeneralActivity?
    /** 新增 在线人数显示背景 */
    var _onlineNumBgView : UIView?
    /** 新增 在线人数显示 Label */
    var _onlineNumLabel : UILabel?
    
    //红包
    lazy var dragBtnRed_packet: FloatDragButton = {
        var dragBtnRed = FloatDragButton(frame: CGRect(x:kScreenWidth-60, y:kScreenHeight-200, width: 60, height: 60))
//        dragBtnRed.theme_setBackgroundImage("PopView.popup_抢红包", forState: UIControl.State.normal)
        dragBtnRed.setBackgroundImage(UIImage.init(named: "icon_float_redpacket"), for: .normal)
//        dragBtnRed.backgroundColor = UIColor.white
//        dragBtnRed.layer.cornerRadius = 30;
        dragBtnRed.backgroundColor = UIColor.clear //clear
        
        
        dragBtnRed.backgroundColor = UIColor.clear //clear
        
        dragBtnRed.clickClosure = { [weak self] (dragBtn) in
            guard let weakSelf = self else{return}
            weakSelf.dragBtnRed_packetClickAction(dragBtn)
        }
        
        return dragBtnRed
    }()
    

    lazy var dragBtn: FloatDragButton = {
        var dragBtn = FloatDragButton(frame: CGRect(x: 0, y: 200, width: 60, height: 60))
        self.chatFloatBtnHoldVC = self
        dragBtn.clickClosure = { [weak self] (dragBtn) in
            if let weakSelf = self {
                weakSelf.dragButtonClickAction(dragBtn)
            }
        }

        dragBtn.autoDockEndClosure = { [weak self](dragBtn) in
            if let weakSelf = self {
                weakSelf.navigationController?.interactivePopGestureRecognizer?.isEnabled = dragBtn.x > 0
            }
        }
        return dragBtn
    }()

    //MARK: - 红包
    func dragBtnRed_packetClickAction(_ btn : FloatDragButton) {
        
        if !YiboPreference.getLoginStatus(){
//            showToast(view: self.view, txt: "请先登录再使用")
            loginWhenSessionInvalid(controller: self)
            return
        }
        
        if let sys = getSystemConfigFromJson() {
            if sys.content.rob_redpacket_version == "v1" {
                let vc = RedPackageViewController()
                let navP = MainNavController.init(rootViewController: vc)
                self.present(navP, animated: true, completion: nil)
                return
            }
        }
        let vcp = gunRedEnvelopeViewController.init(nibName: "gunRedEnvelopeViewController", bundle: nil);
        self.present(vcp, animated: true, completion: nil)

    }
    
    //临时活动悬浮
    lazy var dragBtnTemp_activity: FloatDragButton = {
        var dragBtnTemp = FloatDragButton(frame: CGRect(x:0, y:kScreenHeight/2-30, width: 100, height: 100))
        var icon = generalActivityModel?.imageUrl ?? "";
        let iconUrl = URL.init(string: BASE_URL + icon)
        dragBtnTemp.kf.setImage(with: ImageResource(downloadURL: iconUrl!), for: .normal, placeholder: UIImage(named: "homePage_activit_icon"), options: nil, progressBlock: nil, completionHandler: nil)
        dragBtnTemp.clickClosure = {[weak self] (dragBtn) in
            guard let weakSelf = self else{return}
            weakSelf.dragBtnTemp_activityClickAction(dragBtn)
        }
        return dragBtnTemp
    }()
    
    lazy var floatDragBtn: SemiCircleFloatDragButton = {
        
        var floatDragBtnP = SemiCircleFloatDragButton()
        floatDragBtnP.isSelected = false
        
        let buttonWH: CGFloat = 60
        floatDragBtnP.frame = CGRect(x: screenWidth - buttonWH, y: screenHeight * 0.5 - buttonWH * 0.5, width: buttonWH, height: buttonWH)
        
        floatDragBtnP.layer.cornerRadius = buttonWH * 0.5
        floatDragBtnP.theme_setBackgroundImage("SemiCircleMenu.floattingBtn_menu", forState: .normal)
        
        floatDragBtnP.clickClosure = {(button) in
            self.floatDragButtonClickAction(floatDragBtnP)
        }
        
        floatDragBtnP.autoDockEndClosure = {(button) in
            let isRight = button.frame.origin.x > 0
            self.semiCircelView.cellType = isRight ? self.semiCircelView.CellTypeRight : self.semiCircelView.CellTypeLeft
            
            self.semiCircelView.dialLayout.shouldFlip = isRight
            self.semiCircelView.collectionView.reloadData()
        }
        return floatDragBtnP
    }()
    
    lazy var lotterHeadView:UIView = {
        let headView = UIView.init(frame: CGRect(x: 0, y: 0, width: Int(kScreenWidth), height: 44 ))
        headView.backgroundColor = UIColor.clear
        return headView
    }()
    //官方
    lazy var governmentButton:UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(governmentClick(button:)), for: .touchUpInside)
        button.theme_setTitleColor("FrostedGlass.homeTitleNormalTextColor", forState: .normal)
        button.setTitle("官方", for: .normal)
        setupNoPictureAlphaBgView(view: button)
        button.backgroundColor = UIColor.white.withAlphaComponent(0)
        return button
    }()
    //信用
    lazy var creditableButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(creditableClick(button:)), for: .touchUpInside)
        button.setTitle("信用", for: .normal)
        setupNoPictureAlphaBgView(view: button)
        button.backgroundColor = UIColor.white.withAlphaComponent(0)
        button.theme_setTitleColor("FrostedGlass.homeTitleNormalTextColor", forState: .normal)
        return button
    }()
    
    lazy var lotterScrollView:UIScrollView = {
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        scrollView.contentSize = CGSize(width: self.view.frame.width * CGFloat(2), height: kScreenHeight)
        scrollView.tag = 999
        scrollView.isPagingEnabled = true
        scrollView.isUserInteractionEnabled = true
        scrollView.delegate = self
        scrollView.bounces = false
        return scrollView
    }()
    //下划线
    lazy var lotterUnderLineView:UIView = {
        let view = UIView()
        view.theme_backgroundColor = "Global.themeColor"
        lotterHeadView.addSubview(view)
        return view
    }()
    //官方
    lazy var governmetCtrl:BaseController = {
        let ctrl  = UIStoryboard(name: "group_sub_main_page",bundle:nil).instantiateViewController(withIdentifier: "group_sub_main_2") as! BaseController
        setViewBackgroundColorTransparent(view: ctrl.view)
        return ctrl
    }()
    //信用
    lazy var creditbleCtrl:BaseController = {
        let ctrl  = UIStoryboard(name: "group_sub_main_page",bundle:nil).instantiateViewController(withIdentifier: "group_sub_main_2") as! BaseController
        setViewBackgroundColorTransparent(view: ctrl.view)
        return ctrl
    }()

    
    
    func floatDragButtonClickAction(_ btn : SemiCircleFloatDragButton) {
        let isSelected = btn.isSelected
        semiCircelView.isHidden = isSelected
        
        if !isSelected
        {
            semiCircelView.collectionView.contentOffset = CGPoint.init(x: 0, y: 240)
            handleCircleMenuData()
            self.semiCircelView.collectionView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.semiCircelView.hideItemTitle()
            }
        }
        btn.isSelected = !btn.isSelected
        
        let buttonWH: CGFloat = 60
        let dragbtnX = btn.frame.origin.x == 0 ? 0 : screenWidth - buttonWH
        
        btn.frame = CGRect(x: dragbtnX, y: screenHeight * 0.5 - buttonWH * 0.5, width: buttonWH, height: buttonWH)
    }
    
    //MARK: - 临时活动点击事件
    func dragBtnTemp_activityClickAction(_ btn : FloatDragButton) {
        
        openGeneralActivityController(controller: self, url: (self.generalActivityModel?.url)!, name: (self.generalActivityModel?.name)!)
    }
    
    
    private func handleCircleMenuData() {
        let semicircleTitles = YiboPreference.getSemicircelMenuItems()
        var datasP = YiboPreference.getSemiCircleTuplesWithTitles(value: semicircleTitles)
        
        var currentDatas = datasP
        
        if let config = getSystemConfigFromJson()
        {
            let exchange_score = config.content.exchange_score
            if exchange_score != "on"
            {
                YiboPreference.deleteMenuItems(value: ["积分兑换"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["积分兑换"], datas: currentDatas)
            }
            
            let onoff_turnlate = config.content.onoff_turnlate
            if onoff_turnlate != "on"
            {
                YiboPreference.deleteMenuItems(value: ["大转盘"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["大转盘"], datas: currentDatas)
            }
            
            let onoff_member_mobile_red_packet = config.content.onoff_member_mobile_red_packet
            if onoff_member_mobile_red_packet != "on"
            {
                YiboPreference.deleteMenuItems(value: ["抢红包"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["抢红包"], datas: currentDatas)
            }
            
            let onoff_dian_zi_you_yi = config.content.onoff_dian_zi_you_yi
            if onoff_dian_zi_you_yi != "on"
            {
                YiboPreference.deleteMenuItems(value: ["YG棋牌(NB)"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["YG棋牌(NB)"], datas: currentDatas)
            }
            
            let onoff_sport_switch = config.content.onoff_sport_switch
            if onoff_sport_switch != "on"
            {
                YiboPreference.deleteMenuItems(value: ["皇冠体育"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["皇冠体育"], datas: currentDatas)
            }
            let switch_sign_in = config.content.switch_sign_in
            if switch_sign_in != "on"
            {
                YiboPreference.deleteMenuItems(value: ["签到"])
                currentDatas = deleteItemWithTitleFromTuple(titles: ["签到"], datas: currentDatas)
            }
        }
        
        datasP = currentDatas
        self.semiCircelView.items = datasP
    }
    
    private func deleteItemWithTitleFromTuple(titles: Array<String>,datas: Array<(String,String)>) ->  [(String,String)] {
        var resultsDatas = datas
        for index in 0..<datas.count
        {
            let item = datas[index]
            
            for inIndex in 0..<titles.count
            {
                if titles[inIndex] == item.0
                {
                    resultsDatas.remove(at: index)
                }
            }
        }
        
        return resultsDatas
    }
    
    private lazy var layout: LTLayout = {
        let layout = LTLayout()
        layout.titleViewBgColor = UIColor.white.withAlphaComponent(0.0)
        layout.isAverage = true
        layout.isColorAnimation = true
        return layout
    }()
    
    private var simpleManager: LTSimpleManager!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //下载用户头像
        DownloadAvatar(Controller: self)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        //页面出现时刷新 顶部数据
        let allData:[VisitRecords] = WHC_ModelSqlite.query(VisitRecords.self, where: "userName = '\(YiboPreference.getUserName())'", order: "by num desc", limit: "10") as! [VisitRecords]
        simpleManager.header.dataArray = allData
        simpleManager.header.updateData()
        simpleManager.header.collectionView.reloadData()
        
        if let view = getSemicycleButton() {
            view.isHidden = !showSemicycleButton()
        }
    }
    
    //MARK: 添加环形菜单
    private func addSemiCircleMenu() {
        if let keyWindowP = UIApplication.shared.keyWindow {
            let windowFrame = keyWindowP.frame
            semiCircelView = SemicircleMenuView.init(frame: windowFrame)
            
            let semicircleTitles = YiboPreference.getSemicircelMenuItems()
            self.semiCircelView.items = YiboPreference.getSemiCircleTuplesWithTitles(value: semicircleTitles)
            self.semiCircelView.collectionView.reloadData()
            
            if let tabP = self.tabBarController as? RootTabBarViewController
            {
                semiCircelView.holdVC = tabP
            }
            
            semiCircelView.isHidden = true
            semiCircelView.cellType = semiCircelView.CellTypeRight
            semiCircelView.hideSemicircleHandler = {() -> Void in
                self.floatDragBtn.isSelected = false
            }
            keyWindowP.addSubview(semiCircelView)
        }
    }
    
    private func addCircleMenuAndDragButton() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.addSemiCircleMenu()
            
            if let windowP = UIApplication.shared.keyWindow
            {
                if let config = getSystemConfigFromJson()
                {
                    if config.content != nil
                    {
                        if config.content.native_float_funcbar_switch != "on" || YiboPreference.isShouldOpenFloatTool() != "on"
                        {
                            self.floatDragBtn.isHidden = true
                        }
                    }
                }
                
                windowP.addSubview(self.floatDragBtn)
            }
        }
    }
    
    //MARK: - showNewFunctionTipsPage
    private func showNewFunctionTipsPage() {
        
        let showTips = YiboPreference.getShowScanCodeTipsPage()
        if showTips == "off" {return}

//        if isEmptyString(str: showTips) {
//            let tipsViewButton = self.getNewFunctionTipsPage(image: "NewTipsScanCode")
//            tipsViewButton.tag = 10000 + 100
//            tipsViewButton.addTarget(self, action: #selector(showNextTipsView), for: .touchUpInside)
//        }
    }
    
    @objc func showNextTipsView(sender: UIButton) {
        if sender.tag == 10000 + 100{
            sender.isHidden = true
            sender.removeFromSuperview()
            YiboPreference.setScanCodeTipsPage(value: "off")
        }
    }
    
    private func getNewFunctionTipsPage(image: String) -> UIButton {
        let tipsViewButton = UIButton()
        tipsViewButton.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(tipsViewButton)
        tipsViewButton.setBackgroundImage(UIImage.init(named: image), for: .normal)
        
        return tipsViewButton
    }
    
    @objc func webContrllerViewNotification(notification : NSNotification){
        let resultJson = notification.userInfo!["Request_String"] as! String
        let loginVC = UIStoryboard(name: "bbin_page", bundle: nil).instantiateViewController(withIdentifier: "bbin")
        let recordPage = loginVC as! BBinWebContrllerViewController
        recordPage.htmlContent = resultJson
        recordPage.httpis = true
        self.navigationController?.pushViewController(recordPage, animated: true)
    }
    
    @objc func PopupAlertListViewNotification(notification : NSNotification){
        let resultJson = notification.userInfo!["resultJson"] as! String
        PopupAlertListView(resultJson: resultJson,controller:self,not:"",anObject:nil,from: 0)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        let isUpdatePwd = UserDefaults.standard.bool(forKey: "isUpdatePwd")
        if isUpdatePwd {
            let alertCtrl = UIAlertController(title: "您已经长期没有更换密码，为了您的账户安全，请定期更换密码", message: "", preferredStyle: .alert)
            let sureAction = UIAlertAction(title: "去修改密码", style: .default, handler: { (action) in
                
                self.navigationController?.pushViewController(PasswordModifyController(), animated: true)
            })
            let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
            alertCtrl.addAction(cancelAction)
            alertCtrl.addAction(sureAction)
            self.present(alertCtrl, animated: true, completion: nil)
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(PopupAlertListViewNotification(notification:)),name:NSNotification.Name(rawValue: "MainHeaderView_PopupAlertView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(webContrllerViewNotification(notification:)),name:NSNotification.Name(rawValue: "Request_String"), object: nil)
        
        setupthemeBgView(view: self.view, alpha: 0)
        
        debugConfigURLs(controller: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showNewFunctionTipsPage()
        }
        
        if self.navigationController?.children.count == 1 {
            addCircleMenuAndDragButton()
        }
        
        //这个通知是顶部点击事件传递
        NotificationCenter.default.addObserver(self, selector: #selector(commonRecords(nofi:)), name: NSNotification.Name(rawValue:"commonUseRecord"), object: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        //根据配置决定显示tab个数
        titles = figure_tabs_from_config()
        let lotters = getLotterGovernmentAndCredit()
        for title in titles{
            if title == "真人" || title == "体育" || title == "电子" || title == HomeTabNameChess || title == HomeRedPacketGame || title == HomeGaming || title == HomeGameFishing {
                let vc = UIStoryboard(name: "sub_main_segment_page",bundle:nil).instantiateViewController(withIdentifier: "sub_segment")
                setViewBackgroundColorTransparent(view: vc.view)
                viewControllers.append(vc as! BaseController)
            }else{
                
                let vc = UIStoryboard(name: "group_sub_main_page",bundle:nil).instantiateViewController(withIdentifier: "group_sub_main_2")
                setViewBackgroundColorTransparent(view: vc.view)
                viewControllers.append(vc as! BaseController)
                
                if title == "彩票" && lotters.count == 2{
                    //
                    let width = kScreenWidth * 0.5
                    
                    vc.view.addSubview(lotterScrollView)
                    vc.view.addSubview(lotterHeadView)
                    lotterHeadView.addSubview(governmentButton)
                    lotterHeadView.addSubview(creditableButton)
                    lotterHeadView.isHidden = false
                    lotterHeadView.addSubview(lotterUnderLineView)
                    if lotters.first == "官方"{
                        governmetCtrl.view.frame = CGRect(x: 0, y: 44, width: kScreenWidth, height: kScreenHeight - KTabBarHeight - 60 - 44 - 46)
                        creditbleCtrl.view.frame = CGRect(x: kScreenWidth, y: 44, width: kScreenWidth, height: kScreenHeight - KTabBarHeight - 60 - 44 - 46)
                        governmentButton.snp.makeConstraints { (make) in
                            make.left.top.equalTo(lotterHeadView)
                            make.bottom.equalTo(lotterHeadView).offset(-1)
                            make.width.equalTo(width)
                        }
                        creditableButton.snp.makeConstraints { (make) in
                            make.left.equalTo(governmentButton.snp.right)
                            make.top.right.equalTo(lotterHeadView)
                            make.bottom.equalTo(lotterHeadView).offset(-1)
                        }
                    }else{
                        governmetCtrl.view.frame = CGRect(x: kScreenWidth, y: 44, width: kScreenWidth, height: kScreenHeight - KTabBarHeight - 60 - 44 - 46)
                        creditbleCtrl.view.frame = CGRect(x: 0, y: 44, width: kScreenWidth, height: kScreenHeight - KTabBarHeight - 60 - 44 - 46)
                        creditableButton.snp.makeConstraints { (make) in
                            make.left.top.equalTo(lotterHeadView)
                            make.bottom.equalTo(lotterHeadView).offset(-1)
                            make.width.equalTo(width)
                        }
                        governmentButton.snp.makeConstraints { (make) in
                            make.left.equalTo(creditableButton.snp.right)
                            make.top.right.equalTo(lotterHeadView)
                            make.bottom.equalTo(lotterHeadView).offset(-1)
                        }
                    }
                    
                    
                    lotterScrollView.addSubview(governmetCtrl.view)
                    lotterScrollView.addSubview(creditbleCtrl.view)
                    
                    vc.addChild(governmetCtrl)
                    vc.addChild(creditbleCtrl)
                    
                    
                    
                    lotterHeadView.snp.remakeConstraints { (make) in
                        make.top.equalTo(vc.view.snp.top).offset(44)
                        make.height.equalTo(44)
                        make.left.right.equalTo(0)
                    }
                }
            }
        }
        
        simpleManager = {
            
            let Y: CGFloat = /*glt_iphoneX*/UIScreen.main.bounds.height >= 812.0 ? 88.0 : 64
            let H: CGFloat = /*glt_iphoneX*/UIScreen.main.bounds.height >= 812.0 ? (view.bounds.height - Y - 34) : view.bounds.height - Y
            let simpleManager = LTSimpleManager(frame: CGRect(x: 0, y: Y, width: view.bounds.width, height: H), viewControllers: viewControllers, titles: titles, currentViewController: self, layout: layout)
            simpleManager.delegate = self
            simpleManager.isClickScrollAnimation = true
            setViewBackgroundColorTransparent(view: simpleManager)
            return simpleManager
        }()
        
        view.addSubview(simpleManager)
        simpleManagerConfig()
        loadGameDatas()
        userMessage()
        
        //自动登录没有开启时，在住页面先打开登录页
        if !YiboPreference.getAutoLoginStatus() && !YiboPreference.getLoginStatus(){
            loginWhenSessionInvalid(controller: self,canBack: true)
        }
        
        if switch_chatRoomOn() {
            chatRoomSetup()
        }
        
        //是否开启了红包
        if let sys = getSystemConfigFromJson() {
            if sys.content.redpacket_float_inmainpage == "on" {
                redSetup();
            }
        }
        
        //是否开启了临时活动
        if let sys = getSystemConfigFromJson() {
            if sys.content.show_temp_activity_switch == "on" {
                // 加载活动数据
                getGeneralActivityUrl()
            }
        }
        
        // 19-9-10 add  JK
        let activitySwitch = getSystemConfigFromJson()?.content.switch_mid_autumn;
        if activitySwitch == "on" {
//            self.view.addSubview(self.activityBtn())
        }
        
        // 新增在线人数显示 jk
        if let sys = getSystemConfigFromJson() {
            if sys.content.switch_mainpage_online_count == "on" {
                
                view.addSubview(self.onlieNumBgView())
                view.addSubview(self.onlineNumLabel())
                // 请求在线人数
                self.requestOnlineNum()
            }
        }
    }
    
    func requestOnlineNum() {
        kRequest(frontDialog: false, url: NATIVE_ONLINE_COUNT) { (content) in
            var fakeCountInt = 0
            if let sys = getSystemConfigFromJson() {
                let fakeCount = sys.content.online_count_fake
                if !isEmptyString(str: fakeCount) && isPurnInt(string: fakeCount){
                    fakeCountInt = Int(fakeCount) ?? 0
                }
            }
            fakeCountInt = fakeCountInt + (content as! Int)
            self.onlineNumLabel().text = "\(fakeCountInt)人在线"
        }
    }
    
    func redSetup(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.view.addSubview(self.dragBtnRed_packet)
        }
    }
    
    func chatRoomSetup() {
        if YiboPreference.getChATVIEWONdata(){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self.dragBtn.backgroundColor = UIColor.clear
                self.dragBtn.setBackgroundImage(UIImage.init(named: "floadButtonBgImagae_normal"), for: .normal)
                self.dragBtn.setBackgroundImage(UIImage.init(named: "floadButtonBgImagae_selected"), for: .highlighted)
                if let window = UIApplication.shared.keyWindow {
                    window.addSubview(self.dragBtn)
                }
                
                showDragBtnHandler = {[weak self](hide,holdVV) -> Void in
                    if let weakSelf = self {
                        
                        if hide {
                            weakSelf.chatFloatBtnHoldVC = nil
                        }else {
                            weakSelf.chatFloatBtnHoldVC = holdVV
                        }
                        
                        if switch_chatRoomOn() {
                            weakSelf.dragBtn.isHidden = hide
                        }
                        
                        if getSystemConfigFromJson()?.content.show_temp_activity_switch == "on" {
                            weakSelf.dragBtnTemp_activity.isHidden = hide
                        }
                    }
                    
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.navigationController?.children.count == 1
        {
            self.setupLeftNavigation()
            self.adjustRightBtn()
        }else
        {
            self.title = "游戏大厅"
        }
        
        showDragBtnHandler?(false,self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        GroupMainContainerController.recordScroll = Array<CGFloat>()
        
        showDragBtnHandler?(true,self)
    }
    
    //加载临时活动url
    func getGeneralActivityUrl() {
        request(frontDialog: true, loadTextStr:"跳转中...", url:GENERAL_ACTIVITY,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = GeneralActivityWraper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            if let content = result.content{
                                if !isEmptyString(str: content.url!){
                                    self.generalActivityModel = result.content
                                    self.tempActivitySetup();
                                }else{
                                    showToast(view: self.view, txt: "没有活动链接，请检查是否配置")
                                }
                            }
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: self.view, txt: errorMsg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                        }
                    }
        })
    }
    //MARK:展示活动窗口
    func tempActivitySetup(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.view.addSubview(self.dragBtnTemp_activity)

        }
    }
    private func setupLeftNavigation() {
        
        let btn = UIButton()
        setViewBackgroundColorTransparent(view: btn)
        updateAppLogo(logo: btn)
    }
    
    private func configLeftBarItem(img: UIImage?,name: String?) {
        self.navigationItem.leftBarButtonItems?.removeAll()

        if img != nil {
            let leftButton = UIButton()
            leftButton.imageView?.contentMode = .scaleAspectFit
            leftButton.contentMode = .center
            leftButton.layer.masksToBounds = true
            leftButton.backgroundColor = UIColor.white.withAlphaComponent(0)
            leftButton.isUserInteractionEnabled = false
            
            leftButton.frame = CGRect.init(x: 0, y: 0, width: leftNaviButtonW, height: leftNaviButtonH)
            leftButton.setImage(img, for: .normal)
            
            let leftBarItem = UIBarButtonItem.init(customView: leftButton)
            self.navigationItem.setLeftBarButton(leftBarItem, animated: false)
        }else {
            let leftLabel = UILabel()
            leftLabel.textAlignment = .center
            leftLabel.layer.masksToBounds = true
            leftLabel.backgroundColor = UIColor.white.withAlphaComponent(0)
            leftLabel.theme_textColor = "Global.barTextColor"
            
            if let stationName = name {
                leftLabel.frame = CGRect.init(x: 0, y: 0, width: screenWidth * 0.3, height: leftNaviButtonH)
                leftLabel.font = UIFont.systemFont(ofSize: 14.0)
                leftLabel.numberOfLines = 0
                leftLabel.lineBreakMode = .byTruncatingTail
                leftLabel.text = stationName
            }
            
            let leftBarItem = UIBarButtonItem.init(customView: leftLabel)
            self.navigationItem.setLeftBarButton(leftBarItem, animated: false)
        }
    }
    
    func updateAppLogo(logo:UIButton) -> Void {
        guard let sys = getSystemConfigFromJson() else{return}
        
        var logoImg = ""
        let lottery_page_logo_url = sys.content.lottery_page_logo_url
        let mobile_station_index_logo = sys.content.mobile_station_index_logo
        let stationName = sys.content.station_name
//        logoImg = !isEmptyString(str: mobile_station_index_logo) ? mobile_station_index_logo : lottery_page_logo_url
        logoImg = mobile_station_index_logo
        
        if !isEmptyString(str: logoImg){
            logoImg = handleImageURL(logoImgP: logoImg)
            
            if let imageURL = URL(string: logoImg) {
                logo.kf.setImage(with: ImageResource(downloadURL: imageURL), for: .normal, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                    if let imageP = image {
                //                        let scaleW = self.leftNaviButtonW / imageP.size.width
                                        let scaleH = self.leftNaviButtonH / imageP.size.height
                //                        let scaleWH =  scaleW > scaleH ? scaleH : scaleW
                                        let finalImage = imageP.scaleImage(scaleSize: scaleH)
                                        
                                        self.configLeftBarItem(img: finalImage, name: "")
                                    }
                                })
                                
                            }
            }
        }else {
            
        }
        
        if !stationName.isEmpty, sys.content.switch_native_show_station_name == "on"{
            DispatchQueue.main.async {
                self.navigationItem.titleView = UILabel()
                let titleView: UILabel = self.navigationItem.titleView as! UILabel
                titleView.textColor = .white
                titleView.adjustsFontSizeToFitWidth = true
                titleView.text = stationName
                titleView.sizeToFit()
            }
            
        }
        
    }
    
    /// 获取在线人数
    
    
    func userMessage(){
        request(frontDialog: false, method: .get, loadTextStr: "", url: "/native/unread_msg_count_v3.do", params: [:]) { (resultJson:String, resultStatus:Bool) in
            var num = "0"
            if resultStatus{
                if let modelP = CenterUserMessageNumber.deserialize(from: resultJson) {
                    if modelP.success {
                        if let content = modelP.content {
                            num = "\(content.count)"
                        }
                    }
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unredMessageChangeNoti"), object: num)
        }
    }
    
    //MARK: 地址可能是相对地址的处理, 应该拿到公共方法里取
    private func handleImageURL(logoImgP: String) -> String{
        var logoImg = logoImgP
        if logoImg.contains("\t"){
            let strs = logoImg.components(separatedBy: "\t")
            if strs.count >= 2{
                logoImg = strs[1]
            }
        }
        logoImg = logoImg.trimmingCharacters(in: .whitespaces)
        if !logoImg.hasPrefix("https://") && !logoImg.hasPrefix("http://"){
            logoImg = String.init(format: "%@/%@", BASE_URL,logoImg)
        }
        
        return logoImg
    }
    
    
    override func adjustRightBtn() -> Void {
        super.adjustRightBtn()
        if YiboPreference.getLoginStatus(){
            self.navigationItem.rightBarButtonItems?.removeAll()
            let menuBtn = UIBarButtonItem.init(image: UIImage.init(named: "icon_menu"), style: .plain, target: self, action: #selector(BaseMainController.actionMenu))
            self.navigationItem.rightBarButtonItems = [menuBtn]
        }
    }
  
    //加载彩种等所有数据
    @objc func loadGameDatas() {
        
        if self.titles.count == 0 {
            return
        }
        
        request(frontDialog: true, loadTextStr:"获取中...", url:ALL_GAME_DATA_URL,
                callback: {[weak self](resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "获取失败"))
                        }else{
                            //                            showToast(view: self.view, txt: resultJson)
                            let btn:UIButton = UIButton()
                            btn.setBackgroundImage(UIImage.init(named: "fast_money_normal_bg"), for: .normal)
                            btn.setTitle("点击刷新", for: .normal)
                            btn.setTitleColor(UIColor.black, for: .normal)
                            btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                            setViewBackgroundColorTransparent(view: btn)
                            weakSelf.view.addSubview(btn)
                            btn.width = 80
                            btn.height = 40
                            btn.centerX = weakSelf.view.centerX
                            btn.centerY = weakSelf.view.centerY + 100
                            btn.addTarget(weakSelf, action: #selector(weakSelf.reloadGameDatas(button:)), for: .touchUpInside)
                        }
                        return
                    }
                    if let result = LotterysWraper.deserialize(from: resultJson){
                        if result.success{
                            weakSelf.allDatas.removeAll()
                            weakSelf.allDatas = weakSelf.allDatas + result.content!
                            
                            var allDatasP = [LotteryData]()
                            for lotteryData in weakSelf.allDatas {
                                var subDatas = lotteryData.subData
                                
                                subDatas = subDatas.sorted {(subDatasP1,subDatasP2) -> Bool in
                                    return subDatasP1.sortNo > subDatasP2.sortNo
                                }
                                
                                lotteryData.subData = subDatas
                                allDatasP.append(lotteryData)
                            }
                            
                            weakSelf.allDatas = allDatasP
                            
                            weakSelf.allDatas = weakSelf.allDatas.sorted {(allDatasP1,allDatasP2) -> Bool in
                                return allDatasP1.sortNo > allDatasP2.sortNo
                            }
                            
                            if (weakSelf.navigationController?.children.count)! == 1 {
                                weakSelf.semiCircelView.allDatas = weakSelf.allDatas
                            }
                            
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            YiboPreference.saveLotterys(value: resultJson as AnyObject)
                            
                            guard let config = getSystemConfigFromJson() else {return}
                            let version = config.content.lottery_version
                            let switch_xfwf = config.content.switch_xfwf
                            
                            let officalPeilvIndexes = getOfficalAndPeilvIndex(tabs: weakSelf.titles)
                            
                            if version == VERSION_V1V2 {
                                weakSelf.simpleManager.scrollToIndex(index: switch_xfwf == "off" ? officalPeilvIndexes.0 : officalPeilvIndexes.1)
                                weakSelf.reload_when_page_change(index: switch_xfwf == "off" ? officalPeilvIndexes.0 : officalPeilvIndexes.1)
                            }else {
                                weakSelf.reload_when_page_change(index: version == VERSION_1 ? officalPeilvIndexes.0 : officalPeilvIndexes.1)
                                weakSelf.simpleManager.scrollToIndex(index: version == VERSION_1 ? officalPeilvIndexes.0 : officalPeilvIndexes.1)
                            }
                            
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: weakSelf.view, txt: errorMsg)
                            }else{
                                showToast(view: weakSelf.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: weakSelf)
                            }
                        }
                    }
        })
    }
    
    @objc func reloadGameDatas(button:UIButton){
        
        loadGameDatas()
        button.removeFromSuperview()
    }
    
    func reload_when_page_change(index:Int){
        if !self.titles.isEmpty{
            if index > self.titles.count || index < 0{
                return
            }
            var titleString = self.titles[index]
            let lotters = getLotterGovernmentAndCredit()
            //二级分栏开关是开启的
            if titleString == "彩票" && lotters.count == 2{
                guard let config = getSystemConfigFromJson() else {return}
                //默认选中的是信用还是官方 on选择信用 off选择官方
                let switch_xfwf = config.content.switch_xfwf
                if switch_xfwf == "off"{
                    governmentClick(button: self.governmentButton)
                }else{
                    creditableClick(button: self.creditableButton)
                }
                //1.彩票类型
                //2.官方或信用
                titleString = lotters[0]
                if let page:CategoryMainController = self.viewControllers[index] as? CategoryMainController{
                    for item in lotters{
                        var datas = self.prepare_data_for_subpage(tabName: item)
                        datas = datas.sorted{(subDataP1,subDataP2) -> Bool in
                            return subDataP1.sortNo > subDataP2.sortNo
                        }
                        if item == "官方"{
                            let governmentCtrl = page.children[0] as? CategoryMainController
                            governmentCtrl?.gameDatas = datas
                            governmentCtrl?.reload()
                        }else if item == "信用"{
                            let creditableCtrl = page.children[1] as? CategoryMainController
                            creditableCtrl?.gameDatas = datas
                            creditableCtrl?.reload()
                        }
                    }
                }
            }else if titleString == "彩票"{
                
                if let titleString = lotters.first{
                    var datas = self.prepare_data_for_subpage(tabName: titleString)
                    datas = datas.sorted{(subDataP1,subDataP2) -> Bool in
                        return subDataP1.sortNo > subDataP2.sortNo
                    }
                    let page:CategoryMainController = self.viewControllers[index] as! CategoryMainController
                    if page.gameDatas.isEmpty {
                        
                        page.gameDatas = datas
                        if page.tableView != nil {
                            page.reload()
                        }
                    }
                }
            }else  {
                var datas = self.prepare_data_for_subpage(tabName: self.titles[index])
                datas = datas.sorted{(subDataP1,subDataP2) -> Bool in
                    return subDataP1.sortNo > subDataP2.sortNo
                }
                if self.titles[index] == "真人" || self.titles[index] == "体育" || self.titles[index] == "电子" || self.titles[index] ==  HomeTabNameChess || titles[index] == HomeRedPacketGame || self.titles[index] == HomeGaming || self.titles[index] ==  HomeGameFishing{
                    let page:SubSegmentController = self.viewControllers[index] as! SubSegmentController
                    if page.gameDatas.isEmpty {
                        page.gameDatas = datas
                        page.reload()
                    }
                }else{
                    let page:CategoryMainController = self.viewControllers[index] as! CategoryMainController
                    
                    if page.gameDatas.isEmpty {
                       
                        page.gameDatas = datas
                        if page.tableView != nil {
                            page.reload()
                        }
                    }
                }
            }
            
        }
        
            
//            else{
//                let page:CategoryMainController = self.viewControllers[index] as! CategoryMainController
//
//                if page.gameDatas.isEmpty {
//                    page.gameDatas = datas
//                    if page.tableView != nil {
//                        page.reload()
//                    }
//                }
//            }
    }
    
    func prepare_data_for_subpage(tabName:String) -> [LotteryData]{
        if self.allDatas.isEmpty{
            return []
        }
        var gameDatas:[LotteryData] = []
        
        for sub in self.allDatas{
            let moduleCode = sub.moduleCode
            let version_str = String.init(format: "%d", sub.lotVersion)
            if tabName == "官方"{
                if moduleCode == CAIPIAO_MODULE_CODE && (version_str == VERSION_1 || version_str == VERSION_V1V2){
                    gameDatas.append(sub)
                }
            }else if tabName == "信用"{
                if moduleCode == CAIPIAO_MODULE_CODE && (version_str == VERSION_2 || version_str == VERSION_V1V2){
                    gameDatas.append(sub)
                }
            }else if tabName == "真人"{
                if moduleCode == REAL_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if tabName == "体育"{
                if moduleCode == SPORT_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if tabName == "电子"{
                if moduleCode == GAME_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if tabName == HomeTabNameChess {
                if moduleCode == CHESS_MODULE_CODE {
                    gameDatas.append(sub)
                }
            }else if tabName == HomeRedPacketGame{
                if moduleCode == RED_PACKET_GAME_CODE{
                    //红包游戏
                    gameDatas.append(sub)
                }
            }else if tabName == HomeGaming{
                if moduleCode == GAMING_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if tabName == HomeGameFishing{
                if moduleCode == GAME_FISHING_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }
        }
        return gameDatas
    }
}

extension GroupMainContainerController {
    
    //MARK: 具体使用请参考以下
    private func simpleManagerConfig() {
        
        ///MARK: headerView设置
        simpleManager.configHeaderView {[weak self] in
            guard let strongSelf = self else { return nil }
            let headerView = strongSelf.containerHeader()
            return headerView
        }
        
        ///MARK: pageView点击事件
        simpleManager.didSelectIndexHandle { (index) in
            self.reload_when_page_change(index: index)
        }
        
        
        ///MARK: 控制器刷新事件
        simpleManager.refreshTableViewHandle { (scrollView, index) in
        }
        
    }
    
    //通知实现跳转
    @objc func commonRecords(nofi:Notification){
        
        if let last = self.navigationController?.viewControllers.last
        {
            if last is SelectedTBVC
            {
                return
            }
        }
        
        if YiboPreference.getLoginStatus() == false {//未登陆则跳到登陆页面
            loginWhenSessionInvalid(controller: self)
            return
        }
        
        let value = nofi.userInfo!["value"] as! VisitRecords
        if value.cpName == "nodata" || value.cpName == "max_data"{
            let vc = SelectedTBVC()
            self.navigationController?.pushViewController(vc, animated: true)
//        }else if value.cpName == "max_data" {
//            showToast(view: self.view, txt: "最多只能添加10个常玩游戏")
        }else{
            let lotData:LotteryData = LotteryData()
            lotData.name = value.cpName!
            lotData.czCode = value.czCode
            lotData.ago = 0
            lotData.code = value.cpBianHao //编码
            lotData.lotType = Int(value.lotType!)!
            lotData.lotVersion = Int(value.lotVersion!)!
            
            chooseControllerWithController(controller: self, lottery: lotData)
        }
    }
    
    @objc private func tapLabel(_ gesture: UITapGestureRecognizer)  {
        
    }
    
}


extension GroupMainContainerController: LTSimpleScrollViewDelegate {
    
    static var recordScroll:[CGFloat] = Array<CGFloat>()
    func glt_scrollViewDidScroll(_ scrollView: UIScrollView) {
        GroupMainContainerController.recordScroll.append(scrollView.contentOffset.y)
    }
    
    func glt_scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        var lastY:CGFloat = 0.0
        var lastY2:CGFloat = 0.0
        if GroupMainContainerController.recordScroll.count > 10 {
            let last1 = GroupMainContainerController.recordScroll.count - 1
            let last2 = GroupMainContainerController.recordScroll.count - 2
            lastY = GroupMainContainerController.recordScroll[last1]
            lastY2 = GroupMainContainerController.recordScroll[last2]
        }
        
        //下拉到一定程度就弹出(松开了手时触发)
        if   scrollView.contentOffset.y < -40.5 && (lastY < lastY2) {
            scrollView.triggerRefreshing()
        }
        //上滑到一定程度就收回(松开了手时触发) scrollView.contentOffset.y < -leftNaviButtonW.5  &&
        if scrollView.contentOffset.y > -105 && (lastY > lastY2) {
            scrollView.endRefreshing(isSuccess: true)
        }
        
    }
    
 
}

extension GroupMainContainerController {
    
    private func containerHeader() -> UIView {
        let headerView = Bundle.main.loadNibNamed("main_header", owner: nil, options: nil)?.first as? MainHeaderView
        headerView?.mainDelegate = self
        //        headerView?.wmdelegate = self
        headerView?.isUserInteractionEnabled = true
        if let view = headerView {
            view.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 280)
            view.didGetNotices = {(hasNotices: Bool) -> Void in
                view.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth, height: hasNotices ? 280 : 250) //230 350
                self.simpleManager.glt_headerHeight =  hasNotices ? 280 : 250
            }

            //同步主界面头部的数据
            view.syncSomeWebData(controller: self)
        }
        return headerView!
    }
}

extension GroupMainContainerController : MainViewDelegate{
    func clickFunc(tag: Int) {
        switch tag {
            
        case 100:
            
            if !YiboPreference.getLoginStatus(){
                showToast(view: self.view, txt: "请先登录再使用")
                loginWhenSessionInvalid(controller: self)
                return
            }
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            
            let str = switchMainPageVersion()
            if ["V1","V7","V8","V9"].contains(str) {
                if let delegate = self.menuDelegate{
                    delegate.menuEvent(isRight: true)
                }
                return
            }else if str  == "V2" || str == "V4" {
                accountWeb(vcType: 1)
                return
            }
            
            if let delegate = self.menuDelegate{
                delegate.menuEvent(isRight: true)
            }
            
        case 101:
            
            let str = switchMainPageVersion()
            
            if !YiboPreference.getLoginStatus() && str != "V7" {
                showToast(view: self.view, txt: "请先登录再使用")
                loginWhenSessionInvalid(controller: self)
                return
            }
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            
            if str == "V7" && YiboPreference.getLoginStatus() {
                showToast(view: self.view, txt: "您已登录，不能进行该操作!")
                return
            }
            
            if str == "V1" {
                if let delegate = self.menuDelegate{
                    delegate.menuEvent(isRight: true)
                }
                return
            }else if str  == "V2" || str == "V4" {
                accountWeb(vcType: 2)
                return
            }else if str == "V3" {
                if let sys = getSystemConfigFromJson() {
                    if sys.content != nil{
                        let lottery_dynamic_odds = sys.content.lottery_dynamic_odds
                        if isEmptyString(str: lottery_dynamic_odds) || lottery_dynamic_odds == "on"{
                            let vc = SpreadVC()
                            self.navigationController?.pushViewController(vc, animated: true)
                            return
                        }
                    }
                }
                
                let vc = TueijianViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                
                return
            }else if ["V5","V9"].contains(str) {
                let root = fundViewController()
                self.present(root, animated: true, completion: nil)
                return
            }else if str == "V6" {
                /// 规避侧边栏被展示
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showCenterNotiActionName"), object: nil)
                if switch_nativeOrWapChat() {
                    if !YiboPreference.getLoginStatus(){
                        showToast(view: self.view, txt: "请先登录再使用")
                        loginWhenSessionInvalid(controller: self)
                        return
                    }
                    
                    //隐藏多功能入口按钮
                    if let view = getSemicycleButton() {
                        view.isHidden = true
                    }
                    kCheckLoginStatus(vc: self)
                    
                }else {
                    self.navigationController?.pushViewController(ChatViewController(), animated: true)
                }
                
                return
            }else if str == "V7" {
                freeRegister(controller: self, showDialog: true, showText: "试玩注册中...",delegate: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    if YiboPreference.getLoginStatus() {
                        self.adjustRightBtn()
                    }else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.adjustRightBtn()
                        })
                    }
                }
                return
            }else if str == "V8" {
                if let sys = getSystemConfigFromJson()  {
                    let url = sys.content.open_bet_website_enter
                    if url.contains("https://") || url.contains("http://") {
                        openBrower(urlString:url)
                    }else{
                        openBrower(urlString:"https://" + url)
                    }
                }
                return
            }else if str == "V10"{
                //活动大厅
                openEventHall(controller: self)
                break
            }
            
            
            if let delegate = self.menuDelegate{
                delegate.menuEvent(isRight: true)
            }
            
        case 102:
            
            let str = switchMainPageVersion()
            if ["V1","V7","V8","V9"].contains(str) {
                openAPPDownloadController(controller:self)
                return
            }else if str  == "V2" {
                openActiveController(controller: self)
                return
            }else if str == "V4"
            {
                if shouldOpenExchangMoney(controller: self)
                {
                    openConvertMoneyPage(controller: self)
                }
                return
            }
            
            openAPPDownloadController(controller:self)
        case 103:
            let str = switchMainPageVersion()
            if !["V9"].contains(str) {
                openContactUs(controller: self)
            }else {
                // 开奖网
                if let sys = getSystemConfigFromJson()  {
                    let url = sys.content.open_bet_website_enter
                    if url.contains("https://") || url.contains("http://") {
                        openBrower(urlString:url)
                    }else{
                        openBrower(urlString:"https://" + url)
                    }
                }
            }
            return
        case 104:
            openContactUs(controller: self)
            return
            
        default:
            break;
        }
    }
    
    /**
     @vcType: 1表示跳转到存款，2表示提款
     */
    private func accountWeb(vcType: Int) -> Void {
        //帐户相关信息
        request(frontDialog: false, url:MEMINFO_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        return
                    }
                    if let result = MemInfoWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let memInfo = result.content{
                                //更新帐户名，余额等信息
                                //                                self.meminfo = memInfo
                                if vcType == 1 {
                                    openChargeMoney(controller: self, meminfo:memInfo)
                                }else if vcType == 2 {
                                    openPickMoney(controller: self, meminfo: memInfo)
                                }
                            }else {
                                showToast(view: self.view, txt: "未获取到账号信息")
                            }
                        }
                    }
        })
    }
    
    //#MARK: ------------------------- 实例化 ---------------------------------
    func activityBtn() -> FloatDragButton {
        if _activityBtn == nil {
            _activityBtn = FloatDragButton()
            _activityBtn?.frame = kCGRect(x: kScreenWidth - kCurrentScreen(x: 500), y: kCurrentScreen(x: 600), width: kCurrentScreen(x: 500), height: kCurrentScreen(x: 500))
            _activityBtn?.setImage(UIImage(named: "activityResource.bundle/MidAutumnFestival_icon"), for: .normal)
            _activityBtn?.imageView?.contentMode = .scaleAspectFit
            
            
            _activityBtn?.clickClosure = {(clickBtn) in
                if YiboPreference.getToken() == ""
                {
                    let loginVc = LoginController()
                    self.navigationController?.pushViewController(loginVc, animated: true)
                }else
                {
                    let vc = activityViewController()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return _activityBtn!
    }
    
    func onlieNumBgView() -> UIView {
        if _onlineNumBgView == nil {
            _onlineNumBgView = UIView(frame: kCGRect(x: kScreenWidth - kCurrentScreen(x: 350), y: KNavHeight, width: kCurrentScreen(x: 350), height: kCurrentScreen(x: 75)))
            _onlineNumBgView?.backgroundColor = UIColor.black
            _onlineNumBgView?.alpha = 0.45
            _onlineNumBgView?.layer.cornerRadius = kCurrentScreen(x: 35)
            
        }
        return _onlineNumBgView!
    }
    
    func onlineNumLabel() -> UILabel {
        if _onlineNumLabel == nil {
            _onlineNumLabel = UILabel(frame: self.onlieNumBgView().frame)
            _onlineNumLabel?.tag = 999
            _onlineNumLabel?.textColor = .white
            _onlineNumLabel?.textAlignment = .center
            _onlineNumLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
            _onlineNumLabel?.text = "在线人数获取中..."
        }
        return _onlineNumLabel!
    }
}

//MARK:事件响应
extension GroupMainContainerController{
    //官方彩种
    @objc func governmentClick(button:UIButton){
        
        let lotters = getLotterGovernmentAndCredit()
        creditableButton.theme_setTitleColor("FrostedGlass.homeTitleNormalTextColor", forState: .normal)
        governmentButton.theme_setTitleColor("Global.themeColor", forState: .normal)
        if lotters.first == "官方"{
            lotterScrollView.setOffsetX(offsetX: 0, animated: true)
            lotterUnderLineView.snp.remakeConstraints { (make) in
                make.width.equalTo(kScreenWidth * 0.5)
                make.bottom.equalTo(lotterHeadView)
                make.height.equalTo(2)
                make.left.equalTo(lotterHeadView)
            }
        }else{
            lotterScrollView.setOffsetX(offsetX: kScreenWidth, animated: true)
            lotterUnderLineView.snp.remakeConstraints { (make) in
                make.width.equalTo(kScreenWidth * 0.5)
                make.bottom.equalTo(lotterHeadView)
                make.height.equalTo(2)
                make.right.equalTo(lotterHeadView)
            }
           
        }
        
    }
    //信用彩种
    @objc func creditableClick(button:UIButton){
        
        let lotters = getLotterGovernmentAndCredit()
        governmentButton.theme_setTitleColor("FrostedGlass.homeTitleNormalTextColor", forState: .normal)
        creditableButton.theme_setTitleColor("Global.themeColor", forState: .normal)
        if lotters.first == "官方"{
            lotterScrollView.setOffsetX(offsetX:kScreenWidth, animated: true)
            lotterUnderLineView.snp.remakeConstraints { (make) in
                make.width.equalTo(kScreenWidth * 0.5)
                make.bottom.equalTo(lotterHeadView)
                make.height.equalTo(2)
                make.right.equalTo(lotterHeadView)
            }
        }else{
            //默认信用
            
            lotterScrollView.setOffsetX(offsetX:0, animated: true)
            lotterUnderLineView.snp.remakeConstraints { (make) in
                make.width.equalTo(kScreenWidth * 0.5)
                make.bottom.equalTo(lotterHeadView)
                make.height.equalTo(2)
                make.left.equalTo(lotterHeadView)
            }
            
        }
        
    }

}

extension GroupMainContainerController:UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let lotters = getLotterGovernmentAndCredit()
        let index = Int(scrollView.contentOffset.x / scrollView.width)
        if  scrollView == lotterScrollView {
            if lotters.first == "官方"{
                if index == 0{
                    governmentClick(button: governmentButton)
                }else{
                    creditableClick(button: creditableButton)
                }
            }else{
                if index == 0{
                    creditableClick(button: creditableButton)
                }else{
                    governmentClick(button: governmentButton)
                }
            }
        }
    }

}

