//
//  InteriorOpenServiceController.swift
//  YiboGameIos
//
//  Created by block on 2019/1/14.
//  Copyright © 2019年 com.lvwenhan. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

/** 内部打开在线客服 */
class InteriorOpenServiceController: BaseController {
    
    lazy var HUDView:MBProgressHUD = {
        let hud = getHUD()
        return hud
    }()
    
    //MARK: 初始化 MBProgressHUD
    func getHUD(text:String = "") -> MBProgressHUD {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = text
        hud.label.textColor = UIColor.white
        hud.bezelView.color = UIColor.black
        hud.label.layoutMargins = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        hud.backgroundView.style = .solidColor
        hud.isUserInteractionEnabled = false
//        hud.activityIndicatorColor = UIColor.white
        hud.graceTime = 0.5
        
        return hud
    }

    var webURL:String?
    
    var progressViewLayer:CALayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "在线客服"
        //监听WKWebView进度条
        webview.addObserver(self, forKeyPath: "estimatedProgress", options: [.old,.new], context: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        autoLayoutSubView()
        if let webURLValue = self.webURL?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
            let url = URL.init(string: webURLValue) {
            let request = URLRequest.init(url: url)
            webview.load(request)
        }else {
            showToast(view: self.view, txt: "链接不正确")
        }
    }
    
    deinit {
        //移除监听
        webview.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
    
    //#MARK: 业务事件
    var assURL:String{
        get{
            return self.webURL!
        }
        set{
            self.webURL = newValue
            let url = URL(string: self.webURL!);
            let request = URLRequest(url: url!);
            self.webview.load(request)
        }
    }
    //#MARK: 点击事件
    override func onBackClick(){
        if self.navigationController != nil{
            let count = self.navigationController?.viewControllers.count
            if count! > 1{
                self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    //#MARK: 代理方法
    
    //#MARK: 约束
    func autoLayoutSubView (){
        self.webview.snp.makeConstraints { (make) in
            make.left.top.right.bottom.equalTo(0)
        }
    }
    
    //#MARK: 懒加载
    /** webView */
    lazy var webview : WKWebView = {
        let web = WKWebView.init(frame: .zero)
        web.uiDelegate = self
        web.navigationDelegate = self
        view.addSubview(web)
        
        let progressView = UIView.init(frame: CGRect(x: 0, y: 0, width: web.frame.width, height: 3))
        
        
        progressViewLayer = CALayer.init();
        progressViewLayer.backgroundColor = UIColor.colorWithHexString("#52e20b").cgColor
        progressViewLayer.frame = CGRect(x: 0, y: 0, width: web.frame.width * 0.05, height: 3)
        progressView.layer.addSublayer(progressViewLayer)
        view.addSubview(progressView)
      
        return web
    }()
}
//MARK:--监听进度条
extension InteriorOpenServiceController{
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress" {
            guard let changes = change else{return}
            
            let oldProgress = changes[NSKeyValueChangeKey.oldKey] as? Double ?? 0
            let newProgress = changes[NSKeyValueChangeKey.newKey] as? Double ?? 0
            
            if newProgress > oldProgress && newProgress > 0.05{
                progressViewLayer.frame = CGRect(x: 0, y: 0, width: Double(webview.frame.width) * newProgress, height: 3)
            }
            
            if newProgress >= 1.0{
                let time1 = DispatchTime.now() + 0.4
                let time2 = time1 + 0.1
                DispatchQueue.main.asyncAfter(deadline: time1) {
                    weak var weakself = self
                    weakself?.progressViewLayer.opacity = 0
                }
                DispatchQueue.main.asyncAfter(deadline: time2) {
                    weak var weakself = self
                    weakself?.progressViewLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 3)
                }
            }
        }
    }
}

//MARK:WKWebViewDelegate
extension InteriorOpenServiceController:WKUIDelegate,WKNavigationDelegate{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        HUDView = getHUD(text: "加载中")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUDView.hide(animated: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        //网页加载失败 隐藏Layer
        HUDView.hide(animated: true)
        progressViewLayer.opacity = 0
    }
}
