//
//  FastPayInfoControllerV2.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher
import IQKeyboardManagerSwift
//快速支付信息填写页

class FastPayInfoControllerV2: BaseController {
    
    var introducePics = [String]()
    var payIcon = ""
    var payFunction = ""
    var USDTTradingNumber: String = ""
    var fasts:[FastPay] = [] {
        didSet {
            fasts = fasts.sorted{(model_1,model_2) -> Bool in
                return model_1.sortNo < model_2.sortNo
            }
        }
    }
    var gameDatas = [FakeBankBean]()
    var is_wx = false
    var qrcodeImg:UIImageView!
    var inputMoney = ""
    var inputRemark = ""
    var meminfo:Meminfo?
    var onoff_payment_show_info = false
    var onoff_payment_show_info_row = 240
    @IBOutlet weak var moneyLimitTV:UILabel!
    @IBOutlet weak var tablview:UITableView!
    @IBOutlet weak var confirmBtn:UIButton!
    lazy var tutorialButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = moneyLimitTV.font
        button.setTitleColor(moneyLimitTV.textColor, for: .normal)
        button.addTarget(self, action: #selector(clickTutorialLink(sender:)), for: .touchUpInside)
        button.contentHorizontalAlignment = .left
        self.view.addSubview(button)
        return button
    }()
    //备注
    var propmtString:String = ""
    var propmtLabel:UILabel?
    var indicateView:UIView?
    
    var USDTPayTipsPropmtLabel:UILabel?
    var USDTPayTipsIndicateView:UIView?
    
    //备注提示
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    lazy var promptView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        indicateView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        indicateView!.layer.cornerRadius = 5
        indicateView!.layer.masksToBounds = true
        indicateView!.backgroundColor = UIColor.red
        //备注文字
        propmtLabel = UILabel()
        propmtLabel!.textColor = UIColor.white
        propmtLabel!.numberOfLines = 0
        propmtLabel!.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(propmtLabel!)
        view.addSubview(indicateView!)
        
        return view
    }()
    
    lazy var USDTPayTipsView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        USDTPayTipsIndicateView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        USDTPayTipsIndicateView!.layer.cornerRadius = 5
        USDTPayTipsIndicateView!.layer.masksToBounds = true
        USDTPayTipsIndicateView!.backgroundColor = UIColor.red
        //备注文字
        USDTPayTipsPropmtLabel = UILabel()
        USDTPayTipsPropmtLabel!.textColor = UIColor.white
        USDTPayTipsPropmtLabel!.numberOfLines = 0
        USDTPayTipsPropmtLabel!.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(USDTPayTipsPropmtLabel!)
        view.addSubview(USDTPayTipsIndicateView!)
    
        return view
    }()
    
    var currentChannelIndex = 0;//当前选择的通道索引
    var show_remark = false//是否显示入款备注
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.tablview != nil {
            currentChannelIndex = 0
            updateContentWhenChannelChange(index: self.currentChannelIndex)
            self.tablview.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
        //        self.title = is_wx ? "微信充值" : "支付宝充值"
        
        if is_wx {
            self.title = "微信充值"
        }else {
            self.title = payFunction
        }
        
        depoistGuidePictures()
        
        tablview.delegate = self
        tablview.dataSource = self
        tablview.showsVerticalScrollIndicator = false
        self.tablview.tableFooterView = tableHeaderViewLable()
        tablview.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "headerView")
        confirmBtn.addTarget(self, action: #selector(onCommitBtn(ui:)), for: .touchUpInside)
        confirmBtn.layer.cornerRadius = 5
        confirmBtn.theme_backgroundColor = "Global.themeColor"
        updateContentWhenChannelChange(index: self.currentChannelIndex)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
//        //当键盘收起的时候会向系统发出一个通知，
//        //这个时候需要注册另外一个监听器响应该通知
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                if config.content.onoff_payment_show_info == "off" {
                    onoff_payment_show_info = true
                    onoff_payment_show_info_row = 80
                    self.tablview.reloadData()
                }
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        IQKeyboardManager.shared.enable = true
    }
    
    //MARK: - 入款指南
    private func depoistGuidePictures() {
        requestDepositeGuidePictures(controller: self, bannerType: "6", success: {[weak self] (pictures) in
            if let weakSelf = self {
                if pictures.count > 0 {
                    weakSelf.introducePics = pictures
                    weakSelf.setupRightNavTitle(title: "存款指南")
                }
            }
        }) { (errorMsg) in
            
        }
    }
    
    func setupRightNavTitle(title:String) -> Void {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onRightMenuClick))
    }
    
    @objc func onRightMenuClick() -> Void {
        let pop = PagePicturePop.init(frame: .zero, urls: self.introducePics)
        pop.show()
    }
    
    //点击更换通道后，更新新支付信息到view
    private func updateContentWhenChannelChange(index:Int){
        if self.fasts.isEmpty{
            return
        }
        let fast = self.fasts[index]
        //后台配置的备注信息
        propmtString = fast.appRemark
        if !propmtString.isEmpty {
            propmtLabel?.text = propmtString
            let labeSize = propmtLabel?.sizeThatFits(CGSize.init(width: kScreenWidth - 40 - 20, height: CGFloat(MAXFLOAT)))
            let promptViewHeight = (labeSize?.height ?? 0) > CGFloat(50) ? labeSize?.height : 50
            view.addSubview(promptView)
            promptView.isHidden = false
            promptView.snp.remakeConstraints { (make) in
                make.top.equalTo(view).offset(KNavHeight)
                make.left.right.equalTo(view)
                make.height.equalTo(promptViewHeight ?? 0)
            }
            indicateView?.snp.remakeConstraints({ (make) in
                make.left.equalTo(promptView).offset(20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.width.height.equalTo(10)
            })
            propmtLabel?.snp.remakeConstraints { (make) in
                make.left.equalTo(indicateView!.snp.right).offset(10)
                make.right.equalTo(promptView).offset(-20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.top.bottom.equalTo(promptView)
            }
            
            tableViewTop.constant = promptViewHeight ?? 0
        }else{
            promptView.isHidden = true
            tableViewTop.constant = 0
        }
        self.gameDatas.removeAll()
        getFakeModels(fast: fast)

        updateQrcodeImage(url: fast.qrCodeImg, image: self.qrcodeImg)
        moneyLimitTV.text = String.init(format: "温馨提示: 最低充值金额%d元，最大金额%d元", fast.minFee,fast.maxFee)
        DispatchQueue.main.async {
            if let sysConfig = getSystemConfigFromJson(), sysConfig.content != nil, self.payFunction == "USDT"
            {
                if !sysConfig.content.pay_tips_deposit_usdt_url.isEmpty
                {
                    self.moneyLimitTV.sizeToFit()
                    self.tutorialButton.setTitle("USDT教程地址: " + sysConfig.content.pay_tips_deposit_usdt_url, for: .normal)
                    let height = self.tutorialButton.titleLabel?.sizeThatFits(CGSize.zero).height
                    self.tutorialButton.frame = CGRect.init(x: self.moneyLimitTV.x, y: self.moneyLimitTV.y+self.moneyLimitTV.height+5, width: self.confirmBtn.width, height: height ?? 0)
                }
            }

        }
    }
    
    @objc func clickTutorialLink(sender: UIButton){
        print("打开教程地址")
        guard let url = URL.init(string: getSystemConfigFromJson()!.content!.pay_tips_deposit_usdt_url) else {
            showToast(view:view , txt: "地址不正确，请联系客服!")
            return}
        openUrlWithBrowser(url: url, view: self.view)
    }
    
    func getFakeModels(fast:FastPay){
        let item2 = FakeBankBean()
        item2.text = "收款账号"
        item2.value = fast.receiveAccount
        gameDatas.append(item2)
        let item3 = FakeBankBean()
        item3.text = "收款人"
        item3.value = fast.receiveName
        gameDatas.append(item3)
        let item4 = FakeBankBean()
        item4.text = "收款银行"
        
        if !isEmptyString(str: fast.bankName) {
            item4.value = fast.bankName
        }else {
            if payFunction == "支付宝支付" || payFunction == "支付宝"{
                item4.value = "支付宝"
            }else if payFunction == "QQ支付" || payFunction == "QQ" {
                item4.value = "QQ"
            }else if payFunction == "云闪付" {
                item4.value = "云闪付"
            }else if payFunction == "美团" || payFunction == "美团支付"{
                item4.value = "美团"
            }else if payFunction == "微信|支付宝" {
                item4.value = "微信|支付宝"
            }else if payFunction == "微信支付" || payFunction == "微信" {
                item4.value = "微信"
            }else if payFunction == "USDT"{
                item4.value = "USDT"
                item2.text = "收款地址"
                item4.text = "收款货币"
                //不要收款人
                let index = gameDatas.firstIndex(of: item3)!
                gameDatas.remove(at: index)
            }
        }
        gameDatas.append(item4)
        
        if payFunction == "USDT"{
            let titles = ["汇率", "购买数量", "存入金额","区块链交易ID/交易号"]
            titles.forEach { (title) in
                let item = FakeBankBean()
                item.text = title
                item.value = ""
                gameDatas.append(item)
                if let sysConfig = getSystemConfigFromJson()
                {
                    if sysConfig.content != nil , title.contains("汇率"){
                        item.value = sysConfig.content.pay_tips_deposit_usdt_rate
                    }
                }
            }
        }else {
            let item5 = FakeBankBean()
            item5.text = "转账金额"
            item5.value = ""
            gameDatas.append(item5)
            
        }
        
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                show_remark = config.content.pay_whether_switch_remarks == "on"
                if show_remark{
                    let item6 = FakeBankBean()
                    item6.text = "备注"
                    item6.value = ""
                    gameDatas.append(item6)
                }
            }
        }
    }
    
    
    @objc func onCommitBtn(ui:UIButton){
        
        self.view.endEditing(true)
        
        let fast = self.fasts[currentChannelIndex]
        
        if isEmptyString(str: self.inputMoney){
            showToast(view: self.view, txt: "请输入充值金额")
            return
        }
        
        //        if !isPurnInt(string: self.inputMoney){
        //            showToast(view: self.view, txt: "请输入整数金额")
        //            return
        //        }
        
        let minFee = fast.minFee
        let maxFee = fast.maxFee
        
        guard let money = Float(self.inputMoney) else {
            showToast(view: self.view, txt: "金额格式不正确,请重新输入")
            return
        }
        if money == 0 {
            showToast(view: self.view, txt: "充值金额不能为0")
            return
        }
        if money < Float(minFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能小于%d元", minFee))
            return
        }
        
        if money > Float(maxFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能大于%d元", maxFee))
            return
        }
        
        if payFunction == "USDT" && isEmptyString(str: USDTTradingNumber){
            showToast(view: self.view, txt: "请输入区块链交易ID/交易号")
            return
        }
        
        if isEmptyString(str: inputRemark){
            if show_remark{
                showToast(view: self.view, txt: "请输入备注")
                return
            }
        }
        
        let payId = fast.id
        let payCode = fast.iconCss
        var depositName = ""
        let remark = inputRemark
        
        var shouldRandom = 0
        if let sysConfig = getSystemConfigFromJson(){
            if sysConfig.content != nil{
                //0是关闭 1先砍掉 然后1到5随机加上  2.1到50的小数
                shouldRandom = sysConfig.content.fast_deposit_add_money_select
            }
        }
        
        var postMoney = self.inputMoney
        postMoney = getPayInfoMoney(postMoney: postMoney, shouldRandom: shouldRandom)
//        var postMoney = self.inputMoney
//        if (!postMoney.contains(".")) && shouldRandom {
//            let randomNum = Float(arc4random() % 98) / 100 + 0.01 + money
//            let randomNumP = String.init(format: "%.2f", randomNum)
////            self.inputMoney = randomNumP
//            postMoney = randomNumP
//        }

        print("inputmoney ==== ",postMoney)
        if let dn = meminfo?.account.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            depositName = dn
        }
        
        if payFunction == "USDT"{
            depositName = USDTTradingNumber
        }
        
        if show_remark{
            depositName = !isEmptyString(str: remark) ? remark : depositName
        }
        let parameter = ["payCode": payCode,"amount":postMoney,"payId":payId,"depositName":depositName,
                         "remark":remark] as [String : Any]
        
        self.request(frontDialog: true, method:.post, loadTextStr:"提交中...", url:API_SAVE_OFFLINE_CHARGE_NEW,params: parameter,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                        
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = OfflineChargeResultWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                showToast(view: self.view, txt: "提交成功")
                                
                                if let meminfo = self.meminfo{
                                    var orderno = ""
                                    let account = meminfo.account
                                    let amount = postMoney
                                    let payName = fast.payName
                                    let qrcode = fast.qrCodeImg
                                    var fycode = ""
                                    var remark = ""
                                    if let content = result.content{
                                        fycode = content.fycode
                                        remark = content.remark
                                        orderno = content.orderid
                                    }
                                    
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                        self.openConfirmPayController(orderNo: orderno, accountName: account, chargeMoney: amount, payMethodName: payName, receiveName: "", receiveAccount: "", dipositor: "", dipositorAccount: "", qrcodeUrl: qrcode, payType: PAY_METHOD_FAST, payJson: "",
                                                                      remark:remark,fycode: fycode)

                                    })
                                    
                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                                }
                                if (result.code == 0) {
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
                        
        })
    }
    
    func openConfirmPayController(orderNo:String,accountName:String,chargeMoney:String,
                                  payMethodName:String, receiveName:String,receiveAccount:String,dipositor:String,dipositorAccount:String,qrcodeUrl:String,payType:Int,payJson:String,
                                  remark:String="",fycode:String="") -> Void {
        if self.navigationController != nil{
            openConfirmPay(controller: self, orderNo: orderNo, accountName: accountName, chargeMoney: chargeMoney, payMethodName: payMethodName, receiveName: receiveName, receiveAccount: receiveAccount, dipositor: dipositor, dipositorAccount: dipositorAccount, qrcodeUrl: qrcodeUrl, payType: payType, payJson: payJson,
                           remark: remark,fycode: fycode)
        }
    }
    
    private func addLongPressGestureRecognizer(qrcode:UIImageView) {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressClick))
        qrcode.isUserInteractionEnabled = true
        qrcode.addGestureRecognizer(longPress)
    }
    
    @objc func longPressClick(){
        let alert = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction.init(title: "保存到相册", style: .default, handler: {(action:UIAlertAction) in
            if self.qrcodeImg.image == nil{
                return
            }
            UIImageWriteToSavedPhotosAlbum(self.qrcodeImg.image!, self, #selector(self.save_image(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        let action2 = UIAlertAction.init(title: "识别二维码图片", style: .default, handler: {(action:UIAlertAction) in
            if self.qrcodeImg.image == nil{
                return
            }
            UIImageWriteToSavedPhotosAlbum(self.qrcodeImg.image!, self, #selector(self.readQRcode(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(action2)
        alert.addAction(cancel)
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect.init(x: kScreenWidth/4, y: kScreenHeight, width: kScreenWidth/2, height: 300)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func readQRcode(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer){
        
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy : CIDetectorAccuracyHigh])
        let imageCI = CIImage.init(image: self.qrcodeImg.image!)
        let features = detector?.features(in: imageCI!)
        guard (features?.count)! > 0 else { return }
        let feature = features?.first as? CIQRCodeFeature
        let qrMessage = feature?.messageString
        
        guard var code = qrMessage else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确")
            return
        }
        if !isEmptyString(str: code){
            var appname = ""
            code = code.lowercased()
            if code.contains("weixin"){
                appname = "微信"
            }else if code.contains("alipay"){
                
                if payFunction == "支付宝支付" {
                    appname = "支付宝"
                }else if payFunction == "QQ支付" {
                    appname = "QQ"
                }else if payFunction == "云闪付" {
                    appname = "云闪付"
                }else if payFunction == "美团" {
                    appname = "美团"
                }
                
            }else{
                showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            }
            if error == nil {
                let ac = UIAlertController.init(title: "保存成功",
                                                message: String.init(format: "您可以打开%@,从相册选取并识别此二维码", appname), preferredStyle: .alert)
                ac.addAction(UIAlertAction(title:"去扫码",style: .default,handler: {(action:UIAlertAction) in
                    // 跳转扫一扫
                    if appname == "微信"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "weixin://")!){
                            openBrower(urlString: "weixin://")
                        }else{
                            showToast(view: self.view, txt: "您未安装微信，无法打开扫描")
                        }
                    }else if appname == "支付宝" || appname == "QQ" || appname == "云闪付"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "alipay://")!){
                            openBrower(urlString: "alipay://")
                        }else{
                            
                            showToast(view: self.view, txt: "您未安装\(appname)，无法打开扫描")
                        }
                    }
                }))
                self.present(ac, animated: true, completion: nil)
            } else {
                let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
        }else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            return
        }
    }
    
    //保存二维码
    @objc func save_image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            showToast(view: self.view, txt: "保存图片成功")
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    
    
}

extension FastPayInfoControllerV2 :UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1
        {
            let headerViewP = headerView()
            updateContentWhenChannelChange(index: currentChannelIndex)
            print("viewForHeaderInSection currentChannelIndex = \(currentChannelIndex)")
            return headerViewP
        }else if section == 0
        {
            let headerP = UIView()
            headerP.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 44)
            headerP.backgroundColor = UIColor.white
            
            let titleP = UILabel()
            titleP.text = "快速入款"
            titleP.textAlignment = .center
            titleP.textColor = UIColor.red
            titleP.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 43)
            
            let line = UIView()
            line.frame = CGRect.init(x: 0, y: 43, width: screenWidth, height: 1)
            line.backgroundColor = UIColor.red
            
            headerP.addSubview(titleP)
            headerP.addSubview(line)
            return headerP
        }
        else {return nil}
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 44 : CGFloat(onoff_payment_show_info_row)
    }
    
    func headerView() -> UIView{
        let header = UIView.init(frame: CGRect.init(x: 0, y: 0, width: kScreenWidth, height:
            CGFloat(onoff_payment_show_info_row)))
        setupNoPictureAlphaBgView(view: header, alpha: 0.2)
        qrcodeImg = UIImageView.init(frame: CGRect.init(x: kScreenWidth/2-60, y: 40, width: 120, height: 120))
        qrcodeImg.contentMode = UIView.ContentMode.scaleAspectFit
        let tip = UILabel.init(frame: CGRect.init(x: 30, y: 0, width: kScreenWidth-60, height: 30))
        tip.textAlignment = NSTextAlignment.center
        tip.textColor = UIColor.red
        tip.font = UIFont.systemFont(ofSize: 12)
        
        header.backgroundColor=UIColor.yellow
        
        
//        let fastPayDesView = UIView.init()
//        header.addSubview(fastPayDesView)
//        fastPayDesView.frame = CGRect.init(x: 0, y: 170, width: screenWidth, height: 30)
//        fastPayDesView.backgroundColor = UIColor.red
        
//        let fastPayDesLabel = UILabel.init()
//        fastPayDesView.addSubview(fastPayDesLabel)
//        fastPayDesLabel.frame = CGRect.init(x: 15, y: 0, width: screenWidth - 2 * 15, height: 30)
//        fastPayDesLabel.font = UIFont.systemFont(ofSize: 14.0)
//        fastPayDesLabel.textColor = UIColor.white
        
//        if let sysConfig = getSystemConfigFromJson()
//        {
//            if sysConfig.content != nil
//            {
//                fastPayDesLabel.text = sysConfig.content.pay_tips_deposit_fast
//            }
//        }
        
        let payTipsView = UIView.init()
        header.addSubview(payTipsView)
        payTipsView.frame = CGRect.init(x: 0, y:210, width: screenWidth, height: 40)
        payTipsView.backgroundColor = UIColor.clear
        
        let payTipsLabel = UILabel.init()
        payTipsView.addSubview(payTipsLabel)
        payTipsLabel.text = "请输入转账资料后点击【确认】即可到账"
        payTipsLabel.frame = CGRect.init(x: 15, y: 0, width: screenWidth - 2 * 15, height: 30)
        payTipsLabel.font = UIFont.systemFont(ofSize: 14.0)
        payTipsLabel.textColor = UIColor.red
        
        var payName = ""
        if payFunction == "支付宝支付" || payFunction == "支付宝"{
            payName = "支付宝"
        }else if payFunction == "QQ支付" || payFunction == "QQ"{
            payName = "QQ"
        }else if payFunction == "美团" {
            payName = "美团"
        }else if payFunction == "云闪付" {
            payName = "云闪付"
        }else if payFunction == "微信支付" || payFunction == "微信"{
            payName = "微信"
        }else if payFunction == "微信|支付宝" || payFunction == "微信|支付宝支付" {
            payName = "微信|支付宝"
        }
        
//        configTips(title: payName, payContents: fastPayDesLabel)
        
//        tip.text = self.is_wx ? "长按可保存识别,请使用微信扫描二维码或保存到本地至微信中识别" : "长按可保存识别,请使用\(payName)扫描二维码或保存到本地至\(payName)中识别"
        
        tip.text = "请使用 \(payName)扫描二维码或保存二维码至\(payName)中识别"
        
        if payFunction == "云闪付" || payFunction == "美团"{
            tip.text = "请使用\(payName)扫描(长按可以保存)"
        }
        
        tip.lineBreakMode = .byWordWrapping
        tip.numberOfLines = 2
        addLongPressGestureRecognizer(qrcode:qrcodeImg)
        header.addSubview(qrcodeImg)
        header.addSubview(tip)
        
        
//        onoff_payment_show_info = true
//        onoff_payment_show_info_row = 80
        
        if onoff_payment_show_info {
            qrcodeImg.isHidden=true
            tip.isHidden = true
//            fastPayDesView.frame = CGRect.init(x: 0, y: 10, width: screenWidth, height: 30)
            payTipsView.frame = CGRect.init(x: 0, y:50, width: screenWidth, height: 40)
         }
        
        
        return header
    }
    
    func didHtml(strhtml:String)->String{
        let str = strhtml.replacingOccurrences(of:"</span>", with:"")
        var strhtml1="";
        var text:NSString?
        let scanner = Scanner(string: str)
        while scanner.isAtEnd == false {
            scanner.scanUpTo("<", into: nil)
            scanner.scanUpTo(">", into: &text)
            strhtml1 = strhtml.replacingOccurrences(of:"\(text == nil ? "" : text!)>", with: "")
        }
        let str1 = strhtml1.replacingOccurrences(of:"</span>", with:"")
        let str2 = str1.replacingOccurrences(of:" ", with:"")
        return str2
    }
    
    func configTips(title: String, payContents:UILabel) {
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                let fastTips = config.content.pay_tips_deposit_fast
                
                switch title
                {
                case "在线支付":
                    payContents.text = didHtml(strhtml: config.content.pay_tips_deposit_third)
                case "微信支付","微信":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_weixin) ? config.content.pay_tips_deposit_weixin : fastTips)
                case "支付宝支付","支付宝":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_alipay) ? config.content.pay_tips_deposit_alipay : fastTips)
                case "QQ支付","QQ":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_qq) ? config.content.pay_tips_deposit_qq : fastTips)
                case "美团":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_meituan) ? config.content.pay_tips_deposit_meituan : fastTips)
                case "银行卡转账支付":
                    payContents.text = didHtml(strhtml: config.content.pay_tips_deposit_general)
                case "云闪付":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_yunshanfu) ? config.content.pay_tips_deposit_yunshanfu : fastTips)
                case "微信|支付宝":
                    payContents.text = didHtml(strhtml: !isEmptyString(str: config.content.pay_tips_deposit_weixin_alipay) ? config.content.pay_tips_deposit_weixin_alipay : fastTips)
                default:
                    print("未识别的类型")
                }
            }
        }
    }
    
    func updateQrcodeImage(url:String,image:UIImageView?){
        if image == nil{
            return
        }
        var qrcodeUrl = url
        if !isEmptyString(str: qrcodeUrl){
            //这里的logo地址有可能是相对地址
            if qrcodeUrl.contains("\t"){
                let strs = qrcodeUrl.components(separatedBy: "\t")
                if strs.count >= 2{
                    qrcodeUrl = strs[1]
                }
            }
            qrcodeUrl = qrcodeUrl.trimmingCharacters(in: .whitespaces)
            //            if ValidateUtil.URL(qrcodeUrl).isRight{
            let imageURL = URL(string: qrcodeUrl)
            if let url = imageURL{
                self.qrcodeImg.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage.init(named: "default_placeholder_picture"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            //            }
        }else{
            image?.image = UIImage.init(named: "default_placeholder_picture")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.fasts.count
        }else if section == 1 {
            return self.gameDatas.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
        }
        
        guard let payPathCell = tableView.dequeueReusableCell(withIdentifier: "newFastPayPathCell") as? NewFastPayPathCell else {
            fatalError("The dequeued cell is not an instance of NewFastPayPathCell.")
        }
        
        
        if indexPath.section == 0
        {
            let fast = self.fasts[indexPath.row]
//            let accountName = isEmptyString(str: fast.frontLabel) ? "没有名称" : fast.frontLabel
            let accountName = isEmptyString(str: fast.frontLabel) ? fast.receiveName : fast.frontLabel
            payPathCell.accountName.text = accountName
            
            let imgURL = fast.icon
            payPathCell.configIcon(icon: self.payIcon,imgURL: imgURL)
            
            let minChargeStr = String.init(format: "最小充值金额%d元", fast.minFee)
            let minChargeAttribute = NSMutableAttributedString.init(string:minChargeStr)
            minChargeAttribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange.init(location: 0, length: 6))
            minChargeAttribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange.init(location: minChargeStr.length - 1, length: 1))
            minChargeAttribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange.init(location: 6, length: minChargeStr.length - 7))
            payPathCell.minChargeLabel.attributedText = minChargeAttribute
            
            if currentChannelIndex == indexPath.row {
                payPathCell.selectionStatusBtn.theme_setImage("TouzhOffical.checkbox_selected", forState: .normal)
            }else {
                payPathCell.selectionStatusBtn.theme_setImage("TouzhOffical.checkbox_normal", forState: .normal)
            }
            
            return payPathCell
        }
        else if indexPath.section == 1
        {
            
            let model = self.gameDatas[indexPath.row]
            
            cell.inputTV.delegate = self
            cell.inputTV.tag = indexPath.row
            if model.text.contains("转账金额") || model.text.contains("购买数量") || model.text.contains("区块链交易ID/交易号") || model.text == "备注"{
                cell.inputTV.isHidden = false
                cell.valueTV.isHidden = true
                cell.inputTV.text = model.value
                if model.text == "备注"{
                    cell.inputTV.keyboardType = .default
                    cell.inputTV.placeholder = "请输入付款人姓名"
                }else if model.text == "区块链交易ID/交易号"{
                    cell.inputTV.keyboardType = .numberPad
                    cell.inputTV.placeholder = String.init(format: "请输入后5位")
                }else{
                    cell.inputTV.keyboardType = .decimalPad
                    cell.inputTV.placeholder = String.init(format: "请输入%@", model.text)
                }
            }else{
                cell.inputTV.isHidden = true
                cell.valueTV.isHidden = false
                cell.valueTV.text = model.value
            }
            
            cell.accessoryType = .none

//            model.text.contains("收款账号") ||
            if  model.text == "转账金额" || model.text.contains("购买数量") || model.text.contains("区块链交易ID/交易号") || model.text.contains("存入金额") || model.text.contains("汇率"){
                cell.copyBtn.isHidden = true
            }else{
                cell.copyBtn.isHidden = false
                cell.copyBtn.tag = indexPath.row
                cell.copyBtn.addTarget(self, action: #selector(onCopyBtn(ui:)), for: .touchUpInside)
            }
//            if indexPath.row == 0 || indexPath.row == self.gameDatas.count-2{
//                cell.copyBtn.isHidden = true
//            }else{
//                cell.copyBtn.isHidden = false
//                cell.copyBtn.tag = indexPath.row
//                cell.copyBtn.addTarget(self, action: #selector(onCopyBtn(ui:)), for: .touchUpInside)
//            }
            cell.textTV.text = model.text
            cell.inputTV.addTarget(self, action: #selector(onInputEnd(ui:)), for: .editingDidEnd)
            cell.inputTV.addTarget(self, action: #selector(onInputChange(ui:)), for: .editingChanged)
        }
        
        
        return cell
    }
    
    @objc private func onCopyBtn(ui:UIButton){
        if self.gameDatas.isEmpty{
            return
        }
        let data = self.gameDatas[ui.tag]
        if !isEmptyString(str: data.value){
            UIPasteboard.general.string = data.value
            showToast(view: self.view, txt: "复制成功")
        }else{
            showToast(view: self.view, txt: "没有内容,无法复制")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0
        {
            currentChannelIndex = indexPath.row
            print("didSelectRowAt currentChannelIndex = \(currentChannelIndex)")
            updateContentWhenChannelChange(index: currentChannelIndex)
            self.tablview.reloadData()
        }else
        {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    @objc func onInputEnd(ui:UITextField) {
        let text = ui.text!
        self.gameDatas[ui.tag].value = text
        let itemName = self.gameDatas[ui.tag].text
        if itemName == "转账金额"{
            self.inputMoney = text
            
            guard let money = Float(self.inputMoney) else {
                showToast(view: self.view, txt: "金额格式不正确,请重新输入")
                return
            }
            
            ui.text = getRandomValue(money: money)
            
            self.inputMoney = ui.text ?? ""
        }else if itemName == "备注"{
            self.inputRemark = text
        }
//        if ui.tag == 3{
//            self.inputMoney = text
//
//            guard let money = Float(self.inputMoney) else {
//                showToast(view: self.view, txt: "金额格式不正确,请重新输入")
//                return
//            }
//
//            ui.text = getRandomValue(money: money)
//            self.inputMoney = ui.text ?? ""
//
//        }else if ui.tag == 4{
//            self.inputRemark = text
//        }
    }

    
    private func getRandomValue(money:Float) -> String {
        var shouldRandom = 0
        if let sysConfig = getSystemConfigFromJson(){
            if sysConfig.content != nil{
                //0是关闭 1先砍掉 然后1到5随机加上  2.1到50的小数
                shouldRandom = sysConfig.content.fast_deposit_add_money_select
            }
        }
        var postMoney = formatPureIntIfIsInt(value: self.inputMoney)
        
        postMoney = getPayInfoMoney(postMoney: postMoney, shouldRandom: shouldRandom)
        
        return postMoney
    }
    
    
    @objc func onInputChange(ui:UITextField) {
        let text = ui.text!
        self.gameDatas[ui.tag].value = text
        let itemName = self.gameDatas[ui.tag].text
        if itemName == "转账金额"{
            self.inputMoney = text
        }else if itemName == "备注"{
            self.inputRemark = text
        }else if itemName == "区块链交易ID/交易号"{
            self.USDTTradingNumber = text
        }else if itemName == "购买数量"{
            guard let config = getSystemConfigFromJson(), config.content != nil else {return}
            self.gameDatas.forEach { (item: FakeBankBean) in
                if item.text == "存入金额"{
                    let USDTRate: Decimal = Decimal.init(string: config.content.pay_tips_deposit_usdt_rate) ?? 0
                    let money: Decimal = Decimal.init(string: text) ?? 0
                    let numberOfPurchase: Decimal = USDTRate * money
                    let displayString: String = numberOfPurchase == 0 ? "" : "\(numberOfPurchase)"
                    item.value = displayString
                    self.inputMoney = displayString
                    //同步存入金额显示
                    let indexOfItem = self.gameDatas.firstIndex(of: item)
                    var indexPath: IndexPath = IndexPath()
                    indexPath = IndexPath.init(row: indexOfItem ?? 0, section: 1)
                    self.tablview.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
        
//        if itemName == "备注" {
//            if (ui.text?.length)! > 150
//            {
//                showToast(view: self.view, txt: "备注不能超过150个字符")
//                let remark = ui.text
//                let remarkString = remark?.subString(start: 0, length: (remark?.length)! - 1)
//                ui.text = remarkString
//            }
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }

}

extension FastPayInfoControllerV2{
    func tableHeaderViewLable()->UIView {
        var text = ""
        if let sysConfig = getSystemConfigFromJson()
        {
            if sysConfig.content != nil
            {
                text = sysConfig.content.pay_tips_deposit_fast_tt
            }
        }
        
        if text.isEmpty {
            return UIView.init()
        }
        
        let viewf = UIView.init(frame: CGRect.init(x: 0, y: 0, width:0, height: 100))
        let lable = UILabel.initWith(text:"温馨提示：" + text, textColor:self.moneyLimitTV.textColor, fontSize: 14.0, fontName: 0)
        viewf.addSubview(lable)
        
        lable.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(5)
            make.height.greaterThanOrEqualTo(40)
        }
        lable.lineBreakMode = NSLineBreakMode.byCharWrapping
        lable.numberOfLines = 0
        
        return viewf
    }
}
