//
//  FastPayInfoController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher
import IQKeyboardManagerSwift
//快速支付信息填写页

class FastPayInfoController: BaseController {

    var payFunction = ""
    var fasts:[FastPay] = []
    var gameDatas = [FakeBankBean]()
    var is_wx = false
    var qrcodeImg:UIImageView!
    var inputMoney = ""
    var inputRemark = ""
    var meminfo:Meminfo?
    var introducePics = [String]()
    var USDTTradingNumber: String = ""
    
    @IBOutlet weak var tableViewConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var moneyLimitTV:UILabel!
    @IBOutlet weak var tablview:UITableView!
    @IBOutlet weak var confirmBtn:UIButton!
    lazy var tutorialButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = moneyLimitTV.font
        button.setTitleColor(moneyLimitTV.textColor, for: .normal)
        button.addTarget(self, action: #selector(clickTutorialLink(sender:)), for: .touchUpInside)
        button.contentHorizontalAlignment = .left
        self.view.addSubview(button)
        return button
    }()
    //备注
    var propmtString:String = ""
    var propmtLabel:UILabel?
    var indicateView:UIView?
    
    var USDTPayTipsPropmtLabel:UILabel?
    var USDTPayTipsIndicateView:UIView?
    
    var currentChannelIndex = 0;//当前选择的通道索引
    
    var show_remark = false//是否显示入款备注
    //备注提示
    lazy var promptView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        indicateView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        indicateView!.layer.cornerRadius = 5
        indicateView!.layer.masksToBounds = true
        indicateView!.backgroundColor = UIColor.red
        //备注文字
        propmtLabel = UILabel()
        propmtLabel!.textColor = UIColor.white
        propmtLabel!.numberOfLines = 0
        propmtLabel!.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(propmtLabel!)
        view.addSubview(indicateView!)
    
        return view
    }()
    
    lazy var USDTPayTipsView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        USDTPayTipsIndicateView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        USDTPayTipsIndicateView!.layer.cornerRadius = 5
        USDTPayTipsIndicateView!.layer.masksToBounds = true
        USDTPayTipsIndicateView!.backgroundColor = UIColor.red
        //备注文字
        USDTPayTipsPropmtLabel = UILabel()
        USDTPayTipsPropmtLabel!.textColor = UIColor.white
        USDTPayTipsPropmtLabel!.numberOfLines = 0
        USDTPayTipsPropmtLabel!.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(USDTPayTipsPropmtLabel!)
        view.addSubview(USDTPayTipsIndicateView!)
    
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tablview != nil {
            //默认选中第一个
            currentChannelIndex = 0
            updateContentWhenChannelChange(index: self.currentChannelIndex)
            self.tablview.reloadData()
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
//        self.title = is_wx ? "微信充值" : "支付宝充值"
        
        if is_wx {
            self.title = "微信充值"
        }else {
            self.title = payFunction
        }
        
        depoistGuidePictures()
        
        tablview.delegate = self
        tablview.dataSource = self
        tablview.showsVerticalScrollIndicator = false
        tablview.tableHeaderView = self.headerView()
        self.tablview.tableFooterView = tableHeaderViewLable()
        tablview.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "headerView")
        confirmBtn.addTarget(self, action: #selector(onCommitBtn(ui:)), for: .touchUpInside)
        confirmBtn.layer.cornerRadius = 5
        confirmBtn.theme_backgroundColor = "Global.themeColor"
        updateContentWhenChannelChange(index: self.currentChannelIndex)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
//        //当键盘收起的时候会向系统发出一个通知，
//        //这个时候需要注册另外一个监听器响应该通知
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        IQKeyboardManager.shared.enable = true
    }
    
    //MARK: - 入款指南
    private func depoistGuidePictures() {
        requestDepositeGuidePictures(controller: self, bannerType: "6", success: {[weak self] (pictures) in
            if let weakSelf = self {
                if pictures.count > 0 {
                    weakSelf.introducePics = pictures
                    weakSelf.setupRightNavTitle(title: "存款指南")
                }
            }
        }) { (errorMsg) in
            
        }
    }
    
    func setupRightNavTitle(title:String) -> Void {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onRightMenuClick))
    }
    
    @objc func onRightMenuClick() -> Void {
        let pop = PagePicturePop.init(frame: .zero, urls: self.introducePics)
        pop.show()
    }
    
    //点击更换通道后，更新新支付信息到view
    private func updateContentWhenChannelChange(index:Int){
        if self.fasts.isEmpty{
            return
        }
        let fast = self.fasts[index]
        //后台配置的备注信息
        propmtString = fast.appRemark
        if !propmtString.isEmpty {
            propmtLabel?.text = propmtString
            let labeSize = propmtLabel?.sizeThatFits(CGSize.init(width: kScreenWidth - 40 - 20, height: CGFloat(MAXFLOAT)))
            let promptViewHeight = (labeSize?.height ?? 0) > CGFloat(50) ? labeSize?.height : 50
            view.addSubview(promptView)
            promptView.isHidden = false
            promptView.snp.remakeConstraints { (make) in
                make.top.equalTo(view).offset(KNavHeight)
                make.left.right.equalTo(view)
                make.height.equalTo(promptViewHeight ?? 0)
            }
            indicateView?.snp.remakeConstraints({ (make) in
                make.left.equalTo(promptView).offset(20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.width.height.equalTo(10)
            })
            propmtLabel?.snp.remakeConstraints { (make) in
                make.left.equalTo(indicateView!.snp.right).offset(10)
                make.right.equalTo(promptView).offset(-20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.top.bottom.equalTo(promptView)
            }
            
            tableViewConstraintTop.constant = promptViewHeight ?? 0

            
        }else{
            promptView.isHidden = true
            tableViewConstraintTop.constant = 0
        }
        self.gameDatas.removeAll()
        getFakeModels(fast: fast)
        self.tablview.reloadData()
        updateQrcodeImage(url: fast.qrCodeImg, image: self.qrcodeImg)
        moneyLimitTV.text = String.init(format: "温馨提示: 最低充值金额%d元，最大金额%d元", fast.minFee,fast.maxFee)
        DispatchQueue.main.async {
            if let sysConfig = getSystemConfigFromJson(), sysConfig.content != nil, self.payFunction == "USDT"
            {
                if !sysConfig.content.pay_tips_deposit_usdt_url.isEmpty
                {
                    self.moneyLimitTV.sizeToFit()
                    self.tutorialButton.setTitle("USDT教程地址: " + sysConfig.content.pay_tips_deposit_usdt_url, for: .normal)
                    let height = self.tutorialButton.titleLabel?.sizeThatFits(CGSize.zero).height
                    self.tutorialButton.frame = CGRect.init(x: self.moneyLimitTV.x, y: self.moneyLimitTV.y+self.moneyLimitTV.height+5, width: self.confirmBtn.width, height: height ?? 0)
                }
            }

        }
        
    }
    
    @objc func clickTutorialLink(sender: UIButton){
        print("打开教程地址")
        guard let url = URL.init(string: getSystemConfigFromJson()!.content!.pay_tips_deposit_usdt_url) else {
            showToast(view:view , txt: "地址不正确，请联系客服!")
            return}
        openUrlWithBrowser(url: url, view: self.view)
    }
    
    func getFakeModels(fast:FastPay){
        let item1 = FakeBankBean()
        item1.text = "选择通道"
        item1.value = fast.frontLabel
        gameDatas.append(item1)
        let item2 = FakeBankBean()
        item2.text = "收款账号"
        item2.value = fast.receiveAccount
        gameDatas.append(item2)
        let item3 = FakeBankBean()
        item3.text = "收款人"
        item3.value = fast.receiveName
        gameDatas.append(item3)
        let item4 = FakeBankBean()
        item4.text = "收款银行"

        if !isEmptyString(str: fast.bankName) {
            item4.value = fast.bankName
        }else {
            if payFunction == "支付宝支付" || payFunction == "支付宝"{
                item4.value = "支付宝"
            }else if payFunction == "QQ支付" || payFunction == "QQ" {
                item4.value = "QQ"
            }else if payFunction == "云闪付" {
                item4.value = "云闪付"
            }else if payFunction == "美团" || payFunction == "美团支付"{
                item4.value = "美团"
            }else if payFunction == "微信|支付宝" {
                item4.value = "微信|支付宝"
            }else if payFunction == "微信支付" || payFunction == "微信" {
                item4.value = "微信"
            }else if payFunction == "USDT"{
                item4.value = "USDT"
                item2.text = "收款地址"
                item4.text = "收款货币"
                //不要收款人
                let index = gameDatas.firstIndex(of: item3)!
                gameDatas.remove(at: index)
            }
        }
        gameDatas.append(item4)

        if payFunction == "USDT"{
            let titles = ["汇率", "购买数量", "存入金额","区块链交易ID/交易号"]
            titles.forEach { (title) in
                let item = FakeBankBean()
                item.text = title
                item.value = ""
                gameDatas.append(item)
                if let sysConfig = getSystemConfigFromJson()
                {
                    if sysConfig.content != nil , title.contains("汇率"){
                        item.value = sysConfig.content.pay_tips_deposit_usdt_rate
                    }
                }
            }
        }else {
            let item5 = FakeBankBean()
            item5.text = "转账金额"
            item5.value = ""
            gameDatas.append(item5)
            
        }
        
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                show_remark = config.content.pay_whether_switch_remarks == "on"
                if show_remark{
                    let item6 = FakeBankBean()
                    item6.text = "备注"
                    item6.value = ""
                    gameDatas.append(item6)
                }
            }
        }
    }
    
    
    @objc func onCommitBtn(ui:UIButton){
        
        let fast = self.fasts[currentChannelIndex]
        
        if isEmptyString(str: self.inputMoney){
            showToast(view: self.view, txt: "请输入充值金额")
            return
        }
        
//        if !isPurnInt(string: self.inputMoney){
//            showToast(view: self.view, txt: "请输入整数金额")
//            return
//        }
        
        let minFee = fast.minFee
        let maxFee = fast.maxFee
        
        guard let money = Float(self.inputMoney) else {
            showToast(view: self.view, txt: "金额格式不正确,请重新输入")
            return
        }
        if money == 0 {
            showToast(view: self.view, txt: "充值金额不能为0")
            return
        }
        if money < Float(minFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能小于%d元", minFee))
            return
        }
        
        if money > Float(maxFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能大于%d元", maxFee))
            return
        }
        
        if payFunction == "USDT" && isEmptyString(str: USDTTradingNumber){
            showToast(view: self.view, txt: "请输入区块链交易ID/交易号")
            return
        }
        
        if isEmptyString(str: inputRemark){
            if show_remark{
                showToast(view: self.view, txt: "请输入备注")
                return
            }
        }
        
        let payId = fast.id
        let payCode = fast.iconCss
        var depositName = ""
        let remark = inputRemark
        
        //对金额进行随机处理
        var shouldRandom = 0
        if let sysConfig = getSystemConfigFromJson(){
            if sysConfig.content != nil{
                //0是关闭 1先砍掉 然后1到5随机加上  2.1到50的小数
                shouldRandom = sysConfig.content.fast_deposit_add_money_select
            }
        }
        var postMoney = self.inputMoney
        postMoney = getPayInfoMoney(postMoney: postMoney, shouldRandom: shouldRandom)

        print("input money after random = ",postMoney)
        if let dn = meminfo?.account.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            depositName = dn
        }
        
        if payFunction == "USDT"{
            depositName = USDTTradingNumber
        }
        
        //显示入款备注时 如果备注不为空则提交的时备注，否则提交的是存款帐号
        if show_remark{
            depositName = !isEmptyString(str: remark) ? remark : depositName
        }
        
        let parameter = ["payCode": payCode,
                         "amount":postMoney,
                         "payId":payId,
                         "depositName":depositName,
                         "remark":remark] as [String : Any]
        
        self.request(frontDialog: true, method:.post, loadTextStr:"提交中...", url:API_SAVE_OFFLINE_CHARGE_NEW,params: parameter,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in

                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }

                        if let result = OfflineChargeResultWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                showToast(view: self.view, txt: "提交成功")
                                if let meminfo = self.meminfo{
                                    var orderno = ""
                                    let account = meminfo.account
                                    let amount = postMoney
                                    let payName = fast.payName
                                    let qrcode = fast.qrCodeImg
                                    var fycode = ""
                                    var remark = ""
                                    if let content = result.content{
                                        fycode = content.fycode
                                        remark = content.remark
                                        orderno = content.orderid
                                    }
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                        self.openConfirmPayController(orderNo: orderno, accountName: account, chargeMoney: amount, payMethodName: payName, receiveName: "", receiveAccount: "", dipositor: "", dipositorAccount: "", qrcodeUrl: qrcode, payType: PAY_METHOD_FAST, payJson: "",
                                                                      remark:remark,fycode: fycode)

                                    })

                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                                }
                                if (result.code == 0) {
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }

        })
    }
    
    func openConfirmPayController(orderNo:String,accountName:String,chargeMoney:String,
                                  payMethodName:String, receiveName:String,receiveAccount:String,dipositor:String,dipositorAccount:String,qrcodeUrl:String,payType:Int,payJson:String,
                                  remark:String="",fycode:String="") -> Void {
        if self.navigationController != nil{
            openConfirmPay(controller: self, orderNo: orderNo, accountName: accountName, chargeMoney: chargeMoney, payMethodName: payMethodName, receiveName: receiveName, receiveAccount: receiveAccount, dipositor: dipositor, dipositorAccount: dipositorAccount, qrcodeUrl: qrcodeUrl, payType: payType, payJson: payJson,
                           remark: remark,fycode: fycode)
        }
    }
    //MARK:添加手势
    private func addLongPressGestureRecognizer(qrcode:UIImageView) {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressClick))
        qrcode.isUserInteractionEnabled = true
        qrcode.addGestureRecognizer(longPress)
    }
    
    @objc func longPressClick(){
        let alert = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction.init(title: "保存到相册", style: .default, handler: {(action:UIAlertAction) in
            if self.qrcodeImg.image == nil{
                return
            }
            UIImageWriteToSavedPhotosAlbum(self.qrcodeImg.image!, self, #selector(self.save_image(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        let action2 = UIAlertAction.init(title: "识别二维码图片", style: .default, handler: {(action:UIAlertAction) in
            if self.qrcodeImg.image == nil{
                return
            }
            UIImageWriteToSavedPhotosAlbum(self.qrcodeImg.image!, self, #selector(self.readQRcode(image:didFinishSavingWithError:contextInfo:)), nil)
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(action2)
        alert.addAction(cancel)
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect.init(x: kScreenWidth/4, y: kScreenHeight, width: kScreenWidth/2, height: 300)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func readQRcode(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer){
        
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy : CIDetectorAccuracyHigh])
        let imageCI = CIImage.init(image: self.qrcodeImg.image!)
        let features = detector?.features(in: imageCI!)
        guard (features?.count)! > 0 else { return }
        let feature = features?.first as? CIQRCodeFeature
        let qrMessage = feature?.messageString
        
        guard var code = qrMessage else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确")
            return
        }
        if !isEmptyString(str: code){
            var appname = ""
            code = code.lowercased()
            if code.contains("weixin"){
                appname = "微信"
            }else if code.contains("alipay"){
                if payFunction == "支付宝支付" {
                    appname = "支付宝"
                }else if payFunction == "QQ支付" {
                    appname = "QQ"
                }else if payFunction == "云闪付" {
                    appname = "云闪付"
                }else if payFunction == "美团" {
                    appname = "美团"
                }
                
            }else{
                showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            }
            if error == nil {
                let ac = UIAlertController.init(title: "保存成功",
                                                message: String.init(format: "您可以打开%@,从相册选取并识别此二维码", appname), preferredStyle: .alert)
                ac.addAction(UIAlertAction(title:"去扫码",style: .default,handler: {(action:UIAlertAction) in
                    // 跳转扫一扫
                    if appname == "微信"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "weixin://")!){
                            openBrower(urlString: "weixin://")
                        }else{
                            showToast(view: self.view, txt: "您未安装微信，无法打开扫描")
                        }
                    }else if appname == "支付宝" || appname == "QQ" || appname == "云闪付"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "alipay://")!){
                            openBrower(urlString: "alipay://")
                        }else{
                            
                            showToast(view: self.view, txt: "您未安装\(appname)，无法打开扫描")
                        }
                    }
                }))
                self.present(ac, animated: true, completion: nil)
            } else {
                let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
        }else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            return
        }
    }
    
    //保存二维码
    @objc func save_image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            showToast(view: self.view, txt: "保存图片成功")
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
}

extension FastPayInfoController:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func headerView() -> UIView{
    
        let row = self.fasts.count / 3
        var collectionViewHeight: CGFloat = 0
        
        if row == 0 {
            collectionViewHeight = CGFloat(15) * CGFloat(2) +  CGFloat(40)
        }else if self.fasts.count > row * 3 {
            collectionViewHeight = CGFloat(row) * CGFloat(15) + CGFloat(15) * CGFloat(2) + (CGFloat(row) + CGFloat(1)) * CGFloat(40)
        }else {
            collectionViewHeight = (CGFloat(row) - CGFloat(1)) * CGFloat(15) + CGFloat(15) * CGFloat(2)  + CGFloat(row) * CGFloat(40)
        }
        let header = UIView.init(frame: CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 0 + collectionViewHeight + 40 + 200))

//        header.backgroundColor = UIColor.groupTableViewBackground
        setupNoPictureAlphaBgView(view: header, alpha: 0.2)
        qrcodeImg = UIImageView.init(frame: CGRect.init(x: kScreenWidth/2-60, y: 25, width: 120, height: 120))
        qrcodeImg.contentMode = UIView.ContentMode.scaleAspectFit
        //提示语
        let tip = UILabel.init(frame: CGRect.init(x: 30, y: 155, width: kScreenWidth-60, height: 30)) //-120
        tip.textAlignment = .center
        tip.textColor = UIColor.red
        tip.font = UIFont.systemFont(ofSize: 12)
        
        
        
        
        var payName = ""
        if payFunction == "支付宝支付" || payFunction == "支付宝"{
            payName = "支付宝"
        }else if payFunction == "QQ支付" || payFunction == "QQ"{
            payName = "QQ"
        }else if payFunction == "美团" {
            payName = "美团"
        }else if payFunction == "云闪付" {
            payName = "云闪付"
        }else if payFunction == "微信支付" || payFunction == "微信"{
            payName = "微信"
        }else if payFunction == "微信|支付宝" || payFunction == "微信|支付宝支付" {
            payName = "微信|支付宝"
        }
        
        tip.text = self.is_wx ? "长按可保存识别,请使用微信扫描二维码或保存到本地至微信中识别" : "长按可保存识别,请使用\(payName)扫描二维码或保存到本地至\(payName)中识别"
        
        if payFunction == "云闪付" || payFunction == "美团"{
            tip.text = "请使用\(payName)扫描(长按可以保存)"
        }
        
        tip.lineBreakMode = .byWordWrapping
        tip.numberOfLines = 2
        addLongPressGestureRecognizer(qrcode:qrcodeImg)
        header.addSubview(qrcodeImg)
        header.addSubview(tip)
        
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                //是否显示二维码
                if config.content.onoff_payment_show_info == "off" {
                    let header2 = createCollectionView(collectionViewHeight:collectionViewHeight,yyy:200)
                    header.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 0+collectionViewHeight+40)
                    header.addSubview(header2)
                    tip.isHidden=true
                    qrcodeImg.isHidden=true
                    return header
                }
            }
        }
        
        let header2 = createCollectionView(collectionViewHeight: collectionViewHeight, yyy: 0)
        header.addSubview(header2)
        
        return header
    }
    
    func updateQrcodeImage(url:String,image:UIImageView?){
        if image == nil{
            return
        }
        var qrcodeUrl = url
        if !isEmptyString(str: qrcodeUrl){
            //这里的logo地址有可能是相对地址
            if qrcodeUrl.contains("\t"){
                let strs = qrcodeUrl.components(separatedBy: "\t")
                if strs.count >= 2{
                    qrcodeUrl = strs[1]
                }
            }
            qrcodeUrl = qrcodeUrl.trimmingCharacters(in: .whitespaces)
            //            if ValidateUtil.URL(qrcodeUrl).isRight{
            let imageURL = URL(string: qrcodeUrl)
            if let url = imageURL{
                self.qrcodeImg.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage.init(named: "default_placeholder_picture"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            //            }
        }else{
            image?.image = UIImage.init(named: "default_placeholder_picture")
        }
    }
    
    
    private func createCollectionView(collectionViewHeight: CGFloat,yyy:NSInteger) -> UIView {
        let header = UIView.init(frame: CGRect(x: 0, y: 200-CGFloat.init(yyy), width: kScreenWidth, height: collectionViewHeight + 40)) //+40
        
        let tipView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 40))

        setupNoPictureAlphaBgView(view: tipView,bgViewColor: "FrostedGlass.viewGrayGlassOtherGray")
        header.addSubview(tipView)
        
        let necessaryImg = UIImageView.init(frame: CGRect(x: 10, y: 0, width: 10, height: 40))
        tipView.addSubview(necessaryImg)
        
        
        
        let tipLable = UILabel.init(frame: CGRect(x: 10 + 10, y: 0, width: kScreenWidth -  10 - 10, height: 40))
        tipView.addSubview(tipLable)
        tipLable.text = "支付通道 : "
        tipLable.textColor = UIColor.black
        tipLable.font = UIFont.systemFont(ofSize: 16)
        
        let layout = UICollectionViewFlowLayout()
        
        let margin = 15
        let width = (kScreenWidth - CGFloat(60)) / CGFloat(3)
        layout.itemSize = CGSize(width: width, height: 40)
        layout.sectionInset = UIEdgeInsets(top: CGFloat(margin), left: CGFloat(margin), bottom: CGFloat(margin), right: CGFloat(margin))
        layout.minimumLineSpacing = CGFloat(margin)
        layout.minimumInteritemSpacing = CGFloat(margin)
    
        let colltionView = UICollectionView(frame: CGRect(x: 0, y: 40, width: kScreenWidth, height: collectionViewHeight), collectionViewLayout: layout)
        colltionView.backgroundColor = UIColor.blue
//        colltionView.backgroundColor = UIColor.groupTableViewBackground
        setViewBackgroundColorTransparent(view: colltionView)
        //注册Cell
        let nib = UINib(nibName: "NormalButtonCollectionCell", bundle: nil)
        colltionView.register(nib, forCellWithReuseIdentifier: "normalButtonCollectionCell")
        colltionView.delegate = self
        colltionView.dataSource = self
        
        header.addSubview(colltionView)
        return header
    }

    //MARK: - <UICollectionViewDataSource,UICollectionViewDelegate>
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "normalButtonCollectionCell", for: indexPath) as! NormalButtonCollectionCell
    
        let fast = self.fasts[indexPath.row]
//        cell.normaltext.text = (isEmptyString(str: fast.frontLabel) ? "没有名称" : fast.frontLabel, for: .normal)
        cell.normaltext.text = isEmptyString(str: fast.frontLabel) ? "没有名称" : fast.frontLabel
        cell.backgroundColor = UIColor.clear
        
     
        if kScreenHeight <= 568 { //iphone5
//            cell.normalButton.titleLabel?.font = UIFont.systemFont(ofSize: 12.0)
            cell.normaltext.font = UIFont.systemFont(ofSize: 12.0)
        }else{
            if fast.frontLabel.length > 6 {
//                cell.normalButton.titleLabel?.font = UIFont.systemFont(ofSize: 10.0)
                cell.normaltext.font = UIFont.systemFont(ofSize: 12.0)
            }
        }
        
        if currentChannelIndex == indexPath.row {
//            cell.normalButton.theme_setBackgroundImage("MemberPage.Charge.payMethedBgSelectedImage", forState: .normal)
            cell.normalV.isHidden = false
            cell.normaltext.layer.borderColor = UIColor.colorWithRGB(r: 236, g: 40, b: 41, alpha: 1).cgColor
        }else {
//            cell.normalButton.theme_setBackgroundImage("MemberPage.Charge.payMethedBgNormalImage", forState: .normal)
            cell.normaltext.layer.borderColor = UIColor.lightGray.cgColor
            cell.normalV.isHidden = true
        }
        
        
        return cell
    }

    //选中
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "normalButtonCollectionCell", for: indexPath) as! NormalButtonCollectionCell
//        cell.normalButton.setBackgroundImage(UIImage(named: ""), for: .normal)
        currentChannelIndex = indexPath.row
        updateContentWhenChannelChange(index: indexPath.row)
        collectionView.reloadData()
//        self.tablview.reloadData()
    }

    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var labels:[String] = []
        for fast in self.fasts{
            labels.append(fast.frontLabel)
        }
        if labels.isEmpty{
            return 0
        }
        return labels.count
    }

    private func getRandomValue(money:Float) -> String {
        
        var shouldRandom = 0
        if let sysConfig = getSystemConfigFromJson(){
            if sysConfig.content != nil{
                //0是关闭 1先砍掉 然后1到5随机加上  2.1到50的小数
                shouldRandom = sysConfig.content.fast_deposit_add_money_select
            }
        }
        var postMoney = formatPureIntIfIsInt(value: self.inputMoney)
        
        postMoney = getPayInfoMoney(postMoney: postMoney, shouldRandom: shouldRandom)
        
        return postMoney
    }
    private func showChannelDialog(){
        
        var labels:[String] = []
        for fast in self.fasts{
            labels.append(fast.frontLabel)
        }
        if labels.isEmpty{
            return
        }
        let selectedView = LennySelectView(dataSource: labels, viewTitle: "请选择通道")
        selectedView.selectedIndex = self.currentChannelIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.currentChannelIndex = index
            self?.updateContentWhenChannelChange(index: index)
            self?.tablview.reloadData()
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
            
        }
    }
    
}

//MARK:UITableViewDelegate,UITableViewDataSource
extension FastPayInfoController:UITableViewDelegate,UITableViewDataSource{
 
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        guard let config = getSystemConfigFromJson(), config.content != nil, !config.content.pay_tips_deposit_usdt.isEmpty else {
//            return UIView()
//        }
//
//        let headerView: UITableViewHeaderFooterView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerView") ?? UITableViewHeaderFooterView()
//        headerView.contentView.backgroundColor = UIColor.black
//        if !headerView.contentView.subviews.contains(USDTPayTipsView)
//        {
//            headerView.contentView.addSubview(USDTPayTipsView)
//            USDTPayTipsPropmtLabel?.text = config.content.pay_tips_deposit_usdt
//            USDTPayTipsView.snp.remakeConstraints { (make) in
//                make.edges.equalTo(headerView.contentView)
//            }
//            USDTPayTipsIndicateView?.snp.remakeConstraints({ (make) in
//                make.left.equalTo(USDTPayTipsView).offset(20)
//                make.centerY.equalTo(USDTPayTipsView.snp.centerY)
//                make.width.height.equalTo(10)
//            })
//            USDTPayTipsPropmtLabel?.snp.remakeConstraints { (make) in
//                make.left.equalTo(USDTPayTipsIndicateView!.snp.right).offset(10)
//                make.right.equalTo(USDTPayTipsView).offset(-20)
//                make.centerY.equalTo(USDTPayTipsView.snp.centerY)
//                make.top.bottom.equalTo(USDTPayTipsView)
//            }
//        }
//        return headerView
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        guard let config = getSystemConfigFromJson(), config.content != nil, !config.content.pay_tips_deposit_usdt.isEmpty else {
//            return 0
//        }
//        let font = UIFont.systemFont(ofSize: 14)
//        let height = config.content.pay_tips_deposit_usdt.height(width: kScreenWidth - 40 - 20, font: font)
//        return height > 50 ? height : 50
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameDatas.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //        if indexPath.row == 0{
        //            self.showChannelDialog()
        //        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 0
        }else {
            return 44.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
        }
        let model = self.gameDatas[indexPath.row]
        cell.inputTV.delegate = self
        cell.inputTV.tag = indexPath.row
        if model.text.contains("转账金额") || model.text.contains("购买数量") || model.text.contains("区块链交易ID/交易号") || model.text == "备注"{
            cell.inputTV.isHidden = false
            cell.valueTV.isHidden = true
            cell.inputTV.text = model.value
            if model.text == "备注"{
                cell.inputTV.keyboardType = .default
                cell.inputTV.placeholder = "请输入付款人姓名"
            }else if model.text == "区块链交易ID/交易号"{
                cell.inputTV.keyboardType = .numberPad
                cell.inputTV.placeholder = String.init(format: "请输入后5位")
            }else{
                cell.inputTV.keyboardType = .decimalPad
                cell.inputTV.placeholder = String.init(format: "请输入%@", model.text)
            }
        }else{
            cell.inputTV.isHidden = true
            cell.valueTV.isHidden = false
            cell.valueTV.text = model.value
        }
        if indexPath.row == 0{
            cell.accessoryType = .disclosureIndicator
        }else{
            cell.accessoryType = .none
        }
        if model.text.contains("选择通道") || model.text == "转账金额" || model.text.contains("购买数量") || model.text.contains("区块链交易ID/交易号") || model.text.contains("存入金额") || model.text.contains("汇率"){
            cell.copyBtn.isHidden = true
        }else{
            cell.copyBtn.isHidden = false
            cell.copyBtn.tag = indexPath.row
            cell.copyBtn.addTarget(self, action: #selector(onCopyBtn(ui:)), for: .touchUpInside)
        }
        //        if indexPath.row == 0 || indexPath.row == self.gameDatas.count-2{
        //            cell.copyBtn.isHidden = true
        //        }else{
        //            cell.copyBtn.isHidden = false
        //            cell.copyBtn.tag = indexPath.row
        //            cell.copyBtn.addTarget(self, action: #selector(onCopyBtn(ui:)), for: .touchUpInside)
        //        }
        cell.textTV.text = model.text
        cell.inputTV.addTarget(self, action: #selector(onInput(ui:)), for: .editingChanged)
        cell.inputTV.addTarget(self, action: #selector(onInputEnd(ui:)), for: .editingDidEnd)
        return cell
    }
}
extension FastPayInfoController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
//MARK:事件响应
extension FastPayInfoController{
    //复制
    @objc private func onCopyBtn(ui:UIButton){
        if self.gameDatas.isEmpty{
            return
        }
        let data = self.gameDatas[ui.tag]
        if !isEmptyString(str: data.value){
            UIPasteboard.general.string = data.value
            showToast(view: self.view, txt: "复制成功")
        }else{
            showToast(view: self.view, txt: "没有内容,无法复制")
        }
    }
    //输入
    @objc func onInput(ui:UITextField){
        let text = ui.text!
        self.gameDatas[ui.tag].value = text
        let itemName = self.gameDatas[ui.tag].text
        if itemName == "转账金额"{
            self.inputMoney = text
        }else if itemName == "区块链交易ID/交易号"{
            self.USDTTradingNumber = text
        }else if itemName == "备注"{
            self.inputRemark = text
        }else if itemName == "购买数量"{
            guard let config = getSystemConfigFromJson(), config.content != nil else {return}
            self.gameDatas.forEach { (item: FakeBankBean) in
                if item.text == "存入金额"{
                    let USDTRate: Decimal = Decimal.init(string: config.content.pay_tips_deposit_usdt_rate) ?? 0
                    let money: Decimal = Decimal.init(string: text) ?? 0
                    let numberOfPurchase: Decimal = USDTRate * money
                    let displayString: String = numberOfPurchase == 0 ? "" : "\(numberOfPurchase)"
                    item.value = displayString
                    self.inputMoney = displayString
                    //同步存入金额显示
                    let indexOfItem = self.gameDatas.firstIndex(of: item)
                    let indexPath: IndexPath = IndexPath.init(row: indexOfItem ?? 0, section: 0)
                    self.tablview.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
    }
    //结束输入
    @objc func onInputEnd(ui:UITextField) {
        let text = ui.text!
        self.gameDatas[ui.tag].value = text
        let itemName = self.gameDatas[ui.tag].text
        if itemName == "转账金额"{
            self.inputMoney = text
            
            guard let money = Float(self.inputMoney) else {
                showToast(view: self.view, txt: "金额格式不正确,请重新输入")
                return
            }
            
            ui.text = getRandomValue(money: money)
            
            self.inputMoney = ui.text ?? ""
        }
        //        else if ui.tag == 5{
        //            self.inputRemark = text
        //        }
    }
    
    func tableHeaderViewLable()->UIView {
        var text = ""
        if let sysConfig = getSystemConfigFromJson()
        {
            if sysConfig.content != nil
            {
                text = sysConfig.content.pay_tips_deposit_fast_tt
            }
        }
        
        if text.isEmpty {
            return UIView.init()
        }
        
        let viewf = UIView.init(frame: CGRect.init(x: 0, y: 0, width:0, height: 100))
        let lable = UILabel.initWith(text:"温馨提示：" + text, textColor:self.moneyLimitTV.textColor, fontSize: 14.0, fontName: 0)
        viewf.addSubview(lable)
        
        lable.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(5)
            make.height.greaterThanOrEqualTo(40)
        }
        lable.lineBreakMode = NSLineBreakMode.byCharWrapping
        lable.numberOfLines = 0
        
        return viewf
    }
}
