//
//  SubSegmentController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/14.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher
/*
 主界面分栏子视图控制器
 官方，信用，真人，电子，体育
 */
class SubSegmentController: BaseController ,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ChessAndCardPayPopViewDelegate,BGChangeMoneyPopViewDelegate{

    var popView:ChessAndCardPayPopView?
    var gameDatas = [LotteryData]()//the lottery game data for single tab page
    @IBOutlet weak var gridview: UICollectionView!
    var use_new_lottery_groud_icons = ""
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setViewBackgroundColorTransparent(view: self.view)
        setViewBackgroundColorTransparent(view: gridview)
        
        
        self.extendedLayoutIncludesOpaqueBars = true
        
        //当键盘弹起的时候会向系统发出一个通知，
        //这个时候需要注册一个监听器响应该通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        //当键盘收起的时候会向系统发出一个通知，
        //这个时候需要注册另外一个监听器响应该通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        gridview.delegate = self
        gridview.dataSource = self
        gridview.register(CPViewCell.self, forCellWithReuseIdentifier:"cell")
        gridview.showsVerticalScrollIndicator = false
        self.gridview.reloadData()
        
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                use_new_lottery_groud_icons = config.content.use_new_lottery_groud_icons
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //判断是否为纯体育版或纯电竞版，如果是则向上补齐
        let str = switchMainPageVersion()
        if str == "V11" || str == "V12"{
            gridview.y-=44
        }
    }
    
    //MARK: - 键盘响应
    @objc override func keyboardWillShow(notification: NSNotification) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            if let userInfo = notification.userInfo,
                let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
                let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
                let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
                
                if self.keyBoardNeedLayout {
                    
                    UIView.animate(withDuration: 0.3, delay: 0.0,
                                   options: UIView.AnimationOptions(rawValue: curve),
                                   animations: {
                                    let frame = UIScreen.main.bounds
                                    if let pop = self.popView {
                                        pop.frame = CGRect.init(x: frame.origin.x, y: -110, width: frame.size.width, height: frame.size.height)
                                    }
                                    
                                    self.keyBoardNeedLayout = false
                                    self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
    }
    
    //键盘隐藏响应
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            
            UIView.animate(withDuration: 0.3, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            
                            if let pop = self.popView {
                                pop.frame = UIScreen.main.bounds
                            }
                            
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)

        }
    }
    
    func reload(){
        if self.gridview != nil {
            self.gridview.reloadData()
        }
    }
    //MARK:子视图代理方法
    
    /** 点击立即进入游戏 */
    func clickNowEntranceGame(playIndexPath: IndexPath) {
        let lotData = self.gameDatas[playIndexPath.row]
        if lotData.moduleCode == CAIPIAO_MODULE_CODE{
            if !YiboPreference.getLoginStatus() {
                loginWhenSessionInvalid(controller: self)
                return
            }
            chooseControllerWithController(controller: self, lottery: lotData)
        }else{
            guard var dCode = lotData.czCode else {return}
            if lotData.moduleCode == SPORT_MODULE_CODE{
                if lotData.isListGame == 2{
                    let vc = UIStoryboard(name: "new_sport_page", bundle:nil).instantiateViewController(withIdentifier: "sport")
                    let sport = vc as! NewSportController
                    self.navigationController?.pushViewController(sport, animated: true)
                }else if lotData.isListGame == 1{
                    let vc = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
                    vc.gameCode = dCode
                    self.navigationController?.pushViewController(vc, animated: true)
                }else {
                    forwardRealWeb(controller: self, forwardUrl: lotData.forwardUrl, titleName: lotData.name)
                }
            }else if lotData.moduleCode == REAL_MODULE_CODE{
                if lotData.isListGame == 1{
                    let vc = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
                    vc.gameCode = dCode
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    forwardRealWeb(controller: self, forwardUrl: lotData.forwardUrl, titleName: lotData.name)
                }
                
            }else if lotData.moduleCode == GAME_MODULE_CODE{
                
                if lotData.isListGame == 1{
                    let vc = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
                    vc.gameCode = dCode
                    vc.myTitle = lotData.name
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    forwardRealWeb(controller: self, forwardUrl: lotData.forwardUrl, titleName: lotData.name)
                }
            }else if lotData.moduleCode == CHESS_MODULE_CODE{
                //棋牌
                if lotData.isListGame == 1{
                    let vc = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
                    vc.gameCode = dCode
                    vc.myTitle = lotData.name
                    vc.moduleCode = CHESS_MODULE_CODE
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    forwardRealWeb(controller: self, forwardUrl: lotData.forwardUrl, titleName: lotData.name)
                }
               
            }else if lotData.moduleCode == RED_PACKET_GAME_CODE{
                //红包游戏
                if lotData.isListGame == 1{
                    let vc = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
                    vc.gameCode = dCode
                    vc.myTitle = lotData.name
                    vc.moduleCode = CHESS_MODULE_CODE
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    forwardRealWeb(controller: self, forwardUrl: lotData.forwardUrl, titleName: "")
                }
            }else{
                if lotData.isListGame == 1{
                    let vc = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
                    vc.gameCode = dCode
                    vc.myTitle = lotData.name
                    vc.moduleCode = CHESS_MODULE_CODE
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    forwardRealWeb(controller: self, forwardUrl: lotData.forwardUrl, titleName: lotData.name)
                }
            }
        }
    }
    
    //#MARK 子视图代理方法
    //点击BG电子或者BG捕鱼立即进入游戏
    
    func clickNowPlayGame(playIndexPath:IndexPath,gameCode:String,forwardUrl:String?){
        showToast(view: self.view, txt: convertString(string: "游戏获取中。。。"))
        self.request(frontDialog: false,method: .post,url: forwardUrl ?? "",params:[:],
                                     callback: {(resultJson:String,resultStatus:Bool)->Void in
            if isEmptyString(str: resultJson){
                showToast(view: self.view, txt: convertString(string: "游戏获取失败，请重试"))
                return
            }
             print("打印获取到的数据\(resultJson)")
            let data = resultJson.data(using: String.Encoding.utf8)
            if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
                if dict["success"] as! Bool != true{
                    let msg = dict["msg"] as! String
                    if !isEmptyString(str: msg){
                        showToast(view: self.view, txt: msg)
                    }else{
                        showToast(view: self.view, txt: convertString(string: "游戏获取失败，请重试"))
                    }
                }else{
                    if let urlValue = URL.init(string: dict["url"] as! String){
                        if #available(iOS 10, *) {
                            UIApplication.shared.open(urlValue, options: [:],
                                                      completionHandler: {
                                                        (success) in
                                                        print("open success")
                            })
                        }else{
                            UIApplication.shared.openURL(urlValue)
                        }
                    }
                }
            }else{
                showToast(view: self.view, txt: convertString(string: "游戏获取失败，请重试"))
            }
        })
    }

    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = switchMainPageVersion()
        if str == "V11" || str == "V12" {
            return CGSize.init(width: kScreenWidth, height: 90)
        }
        return CGSize.init(width: (kScreenWidth-0.5*6)/3, height: 130)
    }
    
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gameDatas.count
    }
    //返回自定义的cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CPViewCell
        let data = self.gameDatas[indexPath.row]
        cell.setupData(data: data,attach:self.use_new_lottery_groud_icons == "on")

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let lotData = self.gameDatas[indexPath.row]
        guard var dCode = lotData.czCode else {return}
        let name = lotData.name
        
        let str = switchMainPageVersion()
        //判断是不是纯体育或者纯电竞
        if str == "V11" || str == "V12" {
            clickNowEntranceGame(playIndexPath: indexPath)
        }else if lotData.popFrame == true && getSystemConfigFromJson()?.content.third_auto_exchange == "off"{
            //有弹窗 是否免额度开关是否关闭
            //免额度开关
            if !lotData.gameType.isEmpty{
                dCode = lotData.gameType
            }
            let popView = ChessAndCardPayPopView.init(frame: UIScreen.main.bounds, dcode: dCode, gameName: name)
            popView.assController = self
            popView.playIndexPath = indexPath
            popView.delegate = self
            popView.gameName = name
            
            UIApplication.shared.keyWindow?.addSubview(popView)
            self.popView = popView
        }else{
            clickNowEntranceGame(playIndexPath: indexPath)
        }
    }
    
}

