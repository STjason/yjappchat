//
//  LoginController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/4.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit
import HandyJSON
import IQKeyboardManagerSwift

class LoginController: BaseController,UITextFieldDelegate,LoginAndRegisterDelegate {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var rememberBtn: UIButton!
    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var inputAccount:CustomFeildText!
    @IBOutlet weak var inputPwd:CustomFeildText!
    @IBOutlet weak var savePwd:UISwitch!
    @IBOutlet weak var registerNow:UILabel!
    @IBOutlet weak var loginBtn:UIButton!
    @IBOutlet weak var freePlay:UILabel!
    @IBOutlet weak var logoUI:UIImageView!
    @IBOutlet weak var pcButton:UIButton!
    @IBOutlet weak var onlineService:UILabel!
    @IBOutlet weak var deleteAccount:UIButton!
    @IBOutlet weak var deletePwd:UIButton!
    @IBOutlet weak var verifyTextField: CustomFeildText!
    @IBOutlet weak var verifyPic: UIImageView!
    @IBOutlet weak var verifyFieldH: NSLayoutConstraint!
    @IBOutlet weak var verifyFieldTop: NSLayoutConstraint!
    
    var openFromOtherPage = false
    var canBack = true;//是否显示返回按钮
    var codeView:WMZCodeView? //滑动验证 和文字点击验证view
    var webView:WKWebView?
    var password = "" //密码
    var account = "" //用户名
    var wrongInfoTimes = 0//登录次数
    
    lazy var bgView:UIButton =  {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.frame = UIScreen.main.bounds
        view.addTarget(self, action: #selector(clickBgView), for: .touchUpInside)
        return view
    }()
    
    lazy var loading:UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView()
        loading.hidesWhenStopped = true
        loading.style = .gray
        return loading
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        refreshVerifyUI()
    }
    
    func refreshVerifyUI() {
        if switchLoginverifyNeed() || wrongInfoTimes > 0 {
            requestVerify()
            verifyFieldH.constant = 40
            verifyPic.isHidden = false
        }else {
            verifyFieldH.constant = 0
            verifyPic.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  UIScreen.main.nativeBounds.height == 1136 {
            inputAccount.snp.updateConstraints { (make) in
                make.height.equalTo(40)
            }
            inputPwd.snp.updateConstraints { (make) in
                make.height.equalTo(40)
            }
            loginBtn.snp.updateConstraints { (make) in
                make.height.equalTo(40)
            }
        }
    
        setupthemeBgView(view: self.view)
        
        self.navigationItem.title = "登录"
        
        if let config = getSystemConfigFromJson() {
            if config.content != nil {
                if config.content.switch_back_computer != "off" {
                    pcButton.isHidden = false
                }
            }
        }
        
        setupUI()
        inputAccount.addTarget(self, action: #selector(onAcountInput(view:)), for: UIControl.Event.editingChanged)
        inputPwd.addTarget(self, action: #selector(onAcountInput(view:)), for: UIControl.Event.editingChanged)
        setupNoPictureAlphaBgView(view: inputAccount)
        setupNoPictureAlphaBgView(view: inputPwd)
        setupNoPictureAlphaBgView(view: verifyTextField)
        inputAccount.layer.cornerRadius = 5.0
        inputPwd.layer.cornerRadius = 5.0
        verifyTextField.layer.cornerRadius = 5.0
        
        deleteAccount.addTarget(self, action: #selector(onDeleteClick(btn:)),for: .touchUpInside)
        deletePwd.addTarget(self, action: #selector(onDeleteClick(btn:)),for: .touchUpInside)
        loginBtn.addTarget(self, action: #selector(onLoginClick(btn:)),for: .touchUpInside)
        self.savePwd.addTarget(self, action: #selector(rememberPwdAction(view:)), for: UIControl.Event.valueChanged)
        savePwd.addTarget(self, action: #selector(onSavePwdSwitch(ui:)), for: UIControl.Event.valueChanged)
        savePwd.isOn = YiboPreference.getPwdState()
        
        loginBtn.layer.cornerRadius = 3.0
        
        registerClickEvents()
        //将存储的用户名密码置入输入框
        inputAccount.text = YiboPreference.getUserName()
        inputPwd.text = YiboPreference.getPwd()
        if !isEmptyString(str: YiboPreference.getUserName()){
            deleteAccount.isHidden = false
        }
        if !isEmptyString(str: YiboPreference.getPwd()){
            deletePwd.isHidden = false
        }
        
        inputAccount.delegate = self
        inputPwd.delegate = self
        pcButton.addTarget(self, action: #selector(goPCButton), for: .touchUpInside)
        
        let str = NSMutableAttributedString.init(string: "没有账号?现在注册")
        str.addAttribute(.underlineStyle, value: 1, range: NSRange.init(location: 0, length: str.length))
        registerNow.attributedText = str
        
        //在线客服
        let str1 = NSMutableAttributedString.init(string: "在线客服")
        str1.addAttribute(.underlineStyle, value: 1, range: NSRange.init(location: 0, length: str1.length))
        onlineService.attributedText = str1
        //免费试玩
        let str2 = NSMutableAttributedString.init(string: "免费试玩")
        str2.addAttribute(.underlineStyle, value: 1, range: NSRange.init(location: 0, length: str2.length))
        freePlay.attributedText = str2
        
        let system = getSystemConfigFromJson()
        if let value = system{
            let datas = value.content
            if !isEmptyString(str: (datas?.onoff_mobile_guest_register)!) && datas?.onoff_mobile_guest_register == "on"{
                freePlay.isHidden = false
            }else{
                freePlay.isHidden = true
            }
            
            if !isEmptyString(str: (datas?.online_customer_showphone)!) && datas?.online_customer_showphone == "on"{
                onlineService.isHidden = false
            }else{
                onlineService.isHidden = false
            }
            
            if let appReg = datas?.onoff_register{
                if !isEmptyString(str:appReg) && appReg == "on"{
                    registerNow.isHidden = false
                }else{
                    registerNow.isHidden = true
                }
            }else{
                registerNow.isHidden = false
            }
            if let appReg = datas?.native_register_switch{//注册app单独开关
                if !isEmptyString(str:appReg) && appReg == "on"{
                    registerNow.isHidden = false
                }else{
                    registerNow.isHidden = true
                }
            }else{
                registerNow.isHidden = false
            }
//            backButton.isHidden = !YiboPreference.getAutoLoginStatus()
        }
        updateAppLogo(logo: self.logoUI)
        
        freePlay.backgroundColor = UIColor.black
        openPopViewinitializationClick(requestSelf: self, controller: self)
    }
    
    
    @objc func goPCButton(){
        let url = String.init(format: "%@/?toPC=1", BASE_URL)
        openActiveDetail(controller: self, title: "电脑版", content: "",foreighUrl: url)
    }
    
    func updateAppLogo(logo:UIImageView) -> Void {
        guard let sys = getSystemConfigFromJson() else{return}
        
        var newLogoImg = sys.content.login_page_logo_url
        
        if !isEmptyString(str: newLogoImg) {
            
            newLogoImg = handleImageURL(logoImgP: newLogoImg)
            
            let imageURL = URL(string: newLogoImg)
            logo.kf.setImage(with: ImageResource(downloadURL: imageURL!), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
        }else {
            
        }
    }
    
    //MARK: 地址可能是相对地址的处理
    private func handleImageURL(logoImgP: String) -> String{
        var logoImg = logoImgP
        if logoImg.contains("\t"){
            let strs = logoImg.components(separatedBy: "\t")
            if strs.count >= 2{
                logoImg = strs[1]
            }
        }
        logoImg = logoImg.trimmingCharacters(in: .whitespaces)
        if !logoImg.hasPrefix("https://") && !logoImg.hasPrefix("http://"){
            logoImg = String.init(format: "%@/%@", BASE_URL,logoImg)
        }
        
        return logoImg
    }
    
    @objc func onSavePwdSwitch(ui:UISwitch) -> Void {
        YiboPreference.savePwdState(value: ui.isOn as AnyObject)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func registerClickEvents() -> Void {
        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(recongnizer:)))
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(recongnizer:)))
        //        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(recongnizer:)))
        let tap4 = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(recongnizer:)))
        registerNow.addGestureRecognizer(tap1)
        freePlay.addGestureRecognizer(tap2)
        //        forgetPwd.addGestureRecognizer(tap3)
        onlineService.addGestureRecognizer(tap4)
    }
    
    func fromRegToLogin() {
        if openFromOtherPage{
            onBackClick()
//            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func tapEvent(recongnizer: UIPanGestureRecognizer) {
        let label = recongnizer.view as! UILabel
        switch label.tag {
        case 10:
            let loginVC = UIStoryboard(name: "register_page", bundle: nil).instantiateViewController(withIdentifier: "newRegisterController")
            let recordPage = loginVC as! NewRegisterController
            recordPage.loginAndRegDelegate = self
            self.navigationController?.pushViewController(recordPage, animated: true)
            
        case 11:
            freeRegister(controller: self, showDialog: true, showText: "试玩注册中...", delegate: nil)
        case 12:
            
            break
        case 13:
            openContactUs(controller: self)
        default:
            break
        }
    }
    
    
    
    @objc func rememberPwdAction(view:UISwitch) -> Void{
        YiboPreference.savePwdState(value: view.isOn as AnyObject)
    }
    
    @objc func onDeleteClick(btn:UIButton) -> Void {
        if btn.tag == 0{
            inputAccount.text = ""
            deleteAccount.isHidden = true
        }else{
            inputPwd.text = ""
            deletePwd.isHidden = true
        }
    }
    
    //接口-登录
    func loginRequest() {
        let vertifyCode = verifyTextField.text ?? ""
        
        let staionID = stationID()
        let versionName = getVerionName()
        let iphoneModel = iphoneType()
        
        let preffix = staionID + ":" + "ios/\(versionName)" + ":" + "iPhone" + "-" + iphoneModel
        
        
        let source = preffix + "---\(account)" + "---\(password)" + "---\(vertifyCode)"
        
        let keyEncrypt = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: preffix)
        let sourceEncrypt = Encryption.Endcode_AES_ECB(key: aesKey, iv: aesIV, strToEncode: source)
        
//        print("---------> 登录参数")
//        print("preffix: \(preffix)")
////        print("source: \(source)")
////        print("keyEncrypt: \(preffix)")
////        print("sourceEncrypt: \(source)")
//        
//        print("---------> 请求头参数")
//        let headerString = defaultHeader["User-agent"] ?? ""
//        print("defaultHeader[\"User-agent\"]: \(headerString)")
        
        let params = ["key":keyEncrypt,
                      "data":sourceEncrypt,
                      "apiVersion":1] as [String : Any]
        
        request(frontDialog: true, method: .post, loadTextStr: "正在登录中...", url:LOGIN_NEW_URL,params:params,
                callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        
                        weakSelf.wrongInfoTimes += 1
                        if !switchLoginverifyNeed() {
                            weakSelf.refreshVerifyUI()
                            weakSelf.refreshVerifyCode()
                        }else {
                            weakSelf.refreshVerifyCode()
                        }
                        
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "登录失败"))
                        }else{
                            showToast(view: weakSelf.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = LoginResultWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.saveLoginStatus(value: true as AnyObject)
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            //记住密码
                            if (YiboPreference.getPwdState()) {
                                YiboPreference.savePwd(value: weakSelf.inputPwd.text as AnyObject)
                            }else{
                                YiboPreference.savePwd(value: "" as AnyObject)
                            }
                            
                            weakSelf.isSixMarkRequest()
                            
                            if let content = result.content{
                                let accountType = content.accountType
                                YiboPreference.setAccountMode(value:accountType as AnyObject)
                                //自动登录的情况下，要记住帐号
                                if !isEmptyString(str: content.account){
                                    YiboPreference.saveUserName(value:content.account as AnyObject)
                                }else{
                                    YiboPreference.saveUserName(value:weakSelf.inputPwd.text! as AnyObject)
                                }
                                //提醒用户更新密码
                                let isUpdatePwd = content.updatePwd
                                
                                
                                
                                UserDefaults.standard.set(isUpdatePwd, forKey:"isUpdatePwd")
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notiLoginAction"), object: nil)
                                
                                if weakSelf.navigationController != nil {
                                    for controller in weakSelf.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: LoginController.self) || controller.isKind(of: NewRegisterController.self) {
                                            controller.dismiss(animated: false, completion: nil)
                                            weakSelf.navigationController?.popViewController(animated: false)
                                        }else {
                                            controller.dismiss(animated: false, completion: nil)
                                            weakSelf.navigationController?.popViewController(animated: false)
                                            break
                                        }
                                    }
                                }
                            }
                        }else{
                            
                            weakSelf.wrongInfoTimes += 1
                            if !switchLoginverifyNeed() {
                                weakSelf.refreshVerifyUI()
                                weakSelf.refreshVerifyCode()
                            }else {
                                weakSelf.refreshVerifyCode()
                            }
                            
                            if let errorMsg = result.msg{
                                showToast(view: weakSelf.view, txt: errorMsg)
                            }else{
                                showToast(view: weakSelf.view, txt: convertString(string: "登录失败"))
                            }
                        }
                    }
                    
        })
    }
    
    
    /// 获取登录验证码
    func requestVerify() -> Void {
        let imageURL = URL(string: (BASE_URL + PORT + VERITY_LOGIN))
        if let url = imageURL{
            downloadImage(url: url, imageUI: self.verifyPic)
        }
    }
    
    @objc func onLoginClick(btn:UIButton) -> Void {
        account = inputAccount.text ?? ""
        password = inputPwd.text ?? ""

        if isEmptyString(str: account){
            showToast(view: self.view, txt: "请输入用户名")
            return
        }
        if isEmptyString(str: password){
            showToast(view: self.view, txt: "请输入密码")
            return
        }
        
        if isEmptyString(str: verifyTextField.text ?? "") && (switchLoginverifyNeed()  || wrongInfoTimes > 0) {
            showToast(view: self.view, txt: "请输入验证码")
            return
        }
        
        if switchRecaptcha_verify() {
            //行为验证码
            setupWebviewVerity(loginOrRegister: 0)
        }else {
            loginRequest()
        }
    }
    
    @objc func onAcountInput(view:UITextField) -> Void {
        let txt = view.text
        if let txtValue = txt{
            if isEmptyString(str: txtValue){
                if view.tag == 0{
                    deleteAccount.isHidden = true
                }else{
                    deletePwd.isHidden = true
                }
            }else{
                if view.tag == 0{
                    deleteAccount.isHidden = false
                }else{
                    deletePwd.isHidden = false
                }
            }
        }
    }
    
    @IBAction func rememberAction(_ sender: UIButton) {
        rememberBtn.isSelected = !sender.isSelected
        YiboPreference.savePwdState(value: rememberBtn.isSelected as AnyObject)
    }
    
    @objc func refreshVerifyCode() { //刷新验证码
        requestVerify()
    }
    
    private func setupUI() {
        
        verifyPic.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(refreshVerifyCode))
        verifyPic.addGestureRecognizer(tap)
        
        if glt_iphoneX {
            backBtnTopConstraint.constant = 54
        }else {
            backBtnTopConstraint.constant = 30
        }
//        YiboPreference.savePwdState(value: rememberBtn.isSelected as AnyObject)
        let shouldSave = YiboPreference.getPwdState()
        rememberBtn.isSelected = shouldSave
        
        rememberBtn.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        rememberBtn.theme_setImage("Global.Login_Register.rememberCheckboxNormal", forState: .normal)
        rememberBtn.theme_setImage("Global.Login_Register.rememberCheckboxSelected", forState: .selected)
        
//        rememberBtn.imageView?.contentMode = .scaleAspectFit
//        rememberBtn.imageEdgeInsets = UIEdgeInsetsMake(15.0, 15.0, 15.0, 5.0)
        
//        rememberBtn.imageEdgeInsets = UIEdgeInsetsMake(25,0,25,25)
        
        backgroundImg.isHidden = false
        backgroundImg.theme_image = "Global.Login_Register.background"
        setTextFiledLeftView(img:"Global.Login_Register.fieldAccountImg", textField: inputAccount)
        setTextFiledLeftView(img: "Global.Login_Register.fieldPwdImg", textField: inputPwd)
        setTextFiledLeftView(img: "Global.Login_Register.fieldVerifymg", textField: verifyTextField)
        inputAccount.background = UIImage(named: "")
        inputPwd.background = UIImage(named: "")
        verifyTextField.background = UIImage(named: "")
    }
    
    private func setTextFiledLeftView(img: String,textField: UITextField) {
        let imgView = UIImageView.init(frame:  CGRect(x: 10, y: 10, width: textField.height - 10 * 2, height: textField.height - 10 * 2))
        imgView.theme_image = ThemeImagePicker(keyPath: img)
        textField.leftView = imgView
        textField.leftViewMode = UITextField.ViewMode.always
    }
    
    
    @IBAction func backAction() {
        if (self.presentingViewController != nil)
        {
            self.dismiss(animated: true, completion: nil)
        }else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //是否是六合彩接口判断,返回数组
    func isSixMarkRequest() {
        request(frontDialog: false, method: .get, url: IS_SIXMARK, callback: { (resultJson:String,resultStatus:Bool) -> Void in
            if !resultStatus {
                return
            }
            
            class SixMarkModel:HandyJSON {
                required init() {}
                var success = false
                var accessToken = ""
                var content = [String]()
            }
            
            if let result = SixMarkModel.deserialize(from: resultJson){
                if result.success{
                    sixMarkArray = result.content
                    if !isEmptyString(str: result.accessToken){
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                    }
                    
                }else {}
            }
        })
    }
}

extension LoginController:WKScriptMessageHandler, WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loading.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let trust = challenge.protectionSpace.serverTrust {
                let urlCrentCard = URLCredential.init(trust: trust)
                completionHandler(.useCredential,urlCrentCard)
            }
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        clickBgView()
        showToast(view: view, txt: "验证成功")
        
        loginRequest()
    }
    
    //MARK: webview行为验证码
    ///loginOrRegister 0 login，1 register
    func setupWebviewVerity(loginOrRegister:Int) {
        var url = ""
        var configName = ""
        
        if loginOrRegister == 0 {
            url = BASE_URL + ACTIVITY_LOGIN_CODE_WEB
            configName = "nativeVertifyLogin"
        }else if loginOrRegister == 1 {
            url = BASE_URL + ACTIVITY_REGISTER_CODE_WEB
            configName = "nativeVertifyRegister"
        }else {
            showToast(view: self.view, txt: "验证码类型不存在")
            return
        }
        
        let webConfiguration = WKWebViewConfiguration()
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        webConfiguration.preferences = preferences
        webConfiguration.userContentController = WKUserContentController()
        webConfiguration.userContentController.add(self, name: configName)
        
        webView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        webView?.scrollView.bouncesZoom = false
        webView?.scrollView.showsVerticalScrollIndicator = false
        webView?.scrollView.showsHorizontalScrollIndicator = false
        webView?.scrollView.isScrollEnabled = false
        webView?.sizeToFit()
        webView?.navigationDelegate = self
        guard let myURL = URL(string: url) else {
            return
        }
        
        let myRequest = URLRequest(url: myURL)
        
        guard let web = webView else {return}
        web.load(myRequest)
        
        view.addSubview(bgView)
        view.addSubview(web)
        view.addSubview(loading)
        
        loading.startAnimating()
        
        web.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(kScreenWidth * 0.8)
            make.height.equalTo(kScreenWidth * 0.8 * 2 / 3)
        }
        
        loading.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(30)
        }
    }
    
    @objc func clickBgView() {
        loading.stopAnimating()
        webView?.removeFromSuperview()
        bgView.removeFromSuperview()
        loading.removeFromSuperview()
        webView = nil
    }
}



