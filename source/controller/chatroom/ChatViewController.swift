//
//  ChatViewController.swift
//  gameplay
//
//  Created by William on 2018/8/28.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import WebKit

class ChatViewController: BaseController,WKNavigationDelegate {
    
    var hasNotLoginHandler:(() -> Void)?
    enum ChatViewType:Int {
        case ChatFromOthers
        case ChatFromBetPage
    }
    
    var ItemIS=false
    var chatViewType = ChatViewType.ChatFromOthers
    
    var progressLayer:CALayer!
    
    var webView:WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "聊天室"
        
        if chatViewType == ChatViewType.ChatFromBetPage {
            self.view.frame = CGRect.init(x: 0, y: CGFloat(CGFloat(KNavHeight) - kStatusHeight), width: screenWidth, height: screenHeight - 44)
        }
        
        if ItemIS {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        }
        getData()
    }
    
    override func viewDidLayoutSubviews() {
        if chatViewType == ChatViewType.ChatFromBetPage {
            self.view.frame = CGRect.init(x: 0, y: CGFloat(CGFloat(KNavHeight) - kStatusHeight), width: screenWidth, height: screenHeight - 44)
        }
    }
    
    func getData() {
        request(frontDialog: true, method: .post, loadTextStr: "正在加载...", url: CHATROOM_LINK) { (resultJson:String, resultStatus:Bool) in
            
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if let result = ChatModel.deserialize(from: resultJson) {
                if result.success{
                    self.setUpWKwebView(str:result.content)
                }else{
                    if !isEmptyString(str: result.msg){
                        showToast(view: self.view, txt: result.msg)
                    }else{
                        showToast(view: self.view, txt: "获取失败")
                    }
                    
                    if result.code == 0  || result.code == -1{
                        self.hasNotLoginHandler?()
                    }
                }
            }
        }
    }
    

    func setUpWKwebView(str:String) {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        //监听网页开始加载
        webView!.addObserver(self, forKeyPath: "estimatedProgress", options: [.new,.old], context: nil)
        guard let myURL = URL(string: str) else{return}
        let myRequest = URLRequest(url: myURL)
        webView!.load(myRequest)
        view.addSubview(webView!)
        webView!.navigationDelegate = self //WKNavigationDelegate
        var progress:UIView?
        if chatViewType == ChatViewType.ChatFromBetPage {
            progress = UIView(frame: CGRect(x: 0, y: Int(kStatusHeight), width: Int(webView!.frame.width), height: 3))
        }else{
            progress = UIView(frame: CGRect(x: 0, y: KNavHeight, width: Int(webView!.frame.width), height: 3))
        }
        
        webView!.addSubview(progress!)
        progressLayer = CALayer()
        progressLayer.backgroundColor = UIColor.colorWithHexString("#52e20b").cgColor
        progress!.layer.addSublayer(progressLayer!)
        
        progressLayer.frame = CGRect(x: 0, y: 0, width: webView!.frame.width * 0.1, height: 3)

    
    }
    //移除监听
    deinit {
    
        webView?.removeObserver(self, forKeyPath: "estimatedProgress")
        
    }
    
    //WKNavigationDelegate 提供了可用来追踪加载过程的代理方法
    //页面开始加载时调用
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        print("页面开始加载")
        
        showToast(view:webView, txt: "正在加载....")
       
    }
    //当内容开始返回时调用
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!){
        print("内容开始返回")
      
    }
    // 页面加载完成之后调用
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        print("页面加载完成")
        hideDialog()
    }
    //页面加载失败时调用
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        print("页面加载失败")
        var urlString = ""
        if let url = webView.url {
            urlString = url.absoluteString
        }
        
        let errorInfo = "url: \(urlString);err:\(error.localizedDescription);'failNav'"
        showToast(view: self.view, txt: errorInfo)
        hideDialog()
        progressLayer!.opacity = 0
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        var urlString = ""
        if let url = webView.url {
            urlString = url.absoluteString
        }
        
        let errorInfo = "url: \(urlString);err:\(error.localizedDescription);'failPro'"
        showToast(view: self.view, txt: errorInfo)
        hideDialog()
        progressLayer!.opacity = 0

    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

        let cookies = HTTPCookieStorage.shared.cookies
        if #available(iOS 11.0, *) {
            let cookieStroe = webView.configuration.websiteDataStore.httpCookieStore
            for cookie in cookies!{
                cookieStroe.setCookie(cookie) {
                    //
                }
            }
        } else {
            // Fallback on earlier versions
        }
       
       
    
        decisionHandler(.allow)
       
    }
}
//MARK:--监听进度条的值
extension ChatViewController{

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

        if keyPath == "estimatedProgress" {
            
            guard let changes = change else { return }
            let newProgress = changes[NSKeyValueChangeKey.newKey] as? Double ?? 0
            let oldProgress = changes[NSKeyValueChangeKey.oldKey] as? Double ?? 0
            //只有在进度大于0.1后再进行变化
            if newProgress > oldProgress && newProgress > 0.1 {
                progressLayer?.frame = CGRect(x: 0, y: 0, width: Double(Double(webView!.frame.width) * newProgress), height: 3)
            }
            
            // 当进度为100%时，隐藏progressLayer并将其初始值改为0
            if newProgress == 1.0 {
                let time1 = DispatchTime.now() + 0.4
                let time2 = time1 + 0.1
                DispatchQueue.main.asyncAfter(deadline: time1) {
                    weak var weakself = self
                    weakself?.progressLayer!.opacity = 0
                }
                DispatchQueue.main.asyncAfter(deadline: time2) {
                    weak var weakself = self
                    weakself?.progressLayer!.frame = CGRect(x: 0, y: 0, width: 0, height: 3)
                }
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}
