//
//  OnlineNewPayInfoMethodCell.swift
//  gameplay
//
//  Created by admin on 2018/10/5.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class OnlineNewPayInfoMethodCell: UICollectionViewCell {
    @IBOutlet weak var contentsBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self)
        setViewBackgroundColorTransparent(view: contentsBtn)
    }
    
    func configWithTitle(title: String,isSelected:Bool) {
        contentsBtn.setImage(nil, for: .normal)
        contentsBtn.setTitle(title, for: .normal)
        contentsBtn.theme_setBackgroundImage(isSelected ? "MemberPage.Charge.payMethedBgSelectedImage" : "MemberPage.Charge.payMethedBgNormalImage", forState: .normal)
    }
    
    func configWithImage(payName:String,isSelected:Bool,version:String = "V2") {
        contentsBtn.setTitle(nil, for: .normal)
        contentsBtn.imageView?.contentMode = .scaleAspectFit
        
        if isEmptyString(str: payName){
            contentsBtn.setImage(nil, for: .normal)
            return
        }
        
        contentsBtn.theme_setBackgroundImage(isSelected ? "MemberPage.Charge.payMethedBgSelectedImage" : "MemberPage.Charge.payMethedBgNormalImage", forState: .normal)
    
        let imageCompontents =  version == "V2" ? "/native/resources/images/" : URL_ICON_ONLINE_PAY_METHOD
        let imageURL = URL(string: BASE_URL + PORT + imageCompontents + payName + ".png")
        
        if let url = imageURL
        {
            contentsBtn.kf.setImage(with: ImageResource(downloadURL: url), for: .normal, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                if error == nil && image != nil
                {
                    self.contentsBtn.setImage(image, for: .normal)
                }else
                {
                    self.contentsBtn.setImage(nil, for: .normal)
                }
            }
        }
    }
    
}









