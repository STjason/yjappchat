//
//  EventHallApplyView.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/27.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//

import UIKit
import SnapKit

class EventHallApplyView: UIView {
    var itemSelectedIndexs = ["0"] //选中项
    ///0 查询活动进度，1 申请活动,2 查询的活动进度的结果
    var popType = 0
    var didSelectRowAt:((_ index:Int) -> Void)? //点击cell
    var applyActivity:(() -> Void)? //点击申请按钮
    var titles = [String]()
    let cellId = "EventHallDetailCell"
    let progressCellId = "EventProgessCell"
    let cellHeight:CGFloat = 50
    let margin:CGFloat = 20 //视图距离边缘的间距
    let viewMargin:CGFloat = 10 //视图间间距
    let labelHeight:CGFloat = 25 //label高度
    let applyBtnHeight:CGFloat = 50
    var applyprogresses:[EventHallProgressModel]? //查询的活动进度的结果
    
    lazy var titleTip:UILabel = {
        let view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 14)
        view.text = "申请活动："
        return view
    }()
    
    lazy var titleLabel:UILabel = {
        let view = UILabel()
        view.textColor = UIColor.red
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
    lazy var tableView:UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.bounces = false
        view.backgroundColor = UIColor.red
        view.separatorInset = UIEdgeInsets.zero
        view.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        view.register(UINib.init(nibName: "EventProgessCell", bundle: nil), forCellReuseIdentifier: progressCellId)
        return view
    }()
    
    lazy var applyBtn:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.orange
        view.setTitleColor(UIColor.white, for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        
        if popType == 0 {
            view.setTitle("进度查询", for: .normal)
        }else {
            view.setTitle("提交申请", for: .normal)
        }

        view.layer.cornerRadius = 3
        view.addTarget(self, action: #selector(applyAction), for: .touchUpInside)
        return view
    }()
    
    convenience init(titles:[String],title:String,popType:Int) {
        self.init()
        
        self.popType = popType
        self.titles = titles
        titleLabel.text = title
        setupUI()
    }
    
    func setupUI() {
        self.backgroundColor = UIColor.groupTableViewBackground
        self.layer.cornerRadius = 3
        addSubview(titleTip)
        addSubview(titleLabel)
        addSubview(tableView)
        addSubview(applyBtn)
        
        let titleTipWidth = titleTip.sizeThatFits(CGSize.init(width: 100, height: CGFloat(MAXFLOAT))).width
        titleTip.snp.makeConstraints { (make) in
            make.top.equalTo(margin)
            make.left.equalTo(margin)
            make.height.equalTo(labelHeight)
            
            if self.popType == 0 {
                make.width.equalTo(0)
            }else if self.popType == 1 {
                make.width.equalTo(titleTipWidth)
            }
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleTip.snp.centerY)
            make.left.equalTo(titleTip.snp.right)
            make.height.equalTo(titleTip.snp.height)
            make.right.equalTo(-margin)
        }
        
        let tableHeight = getTableHeight()
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleTip.snp.bottom).offset(viewMargin)
            make.left.equalTo(margin)
            make.height.equalTo(tableHeight)
            make.right.equalTo(-margin)
        }
        
        applyBtn.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(viewMargin)
            make.left.equalTo(margin)
            make.height.equalTo(applyBtnHeight)
            make.right.equalTo(-margin)
            make.bottom.equalTo(-margin)
        }
    }
    
    ///视图的总高度
    func getViewHeight() -> CGFloat {
        let lablHM = labelHeight + margin //申请活动label和间距的和
        let tableHM = getTableHeight() + viewMargin
        
        var btnHM:CGFloat = 0
        if popType == 0 || popType == 1 {
            btnHM = applyBtnHeight + viewMargin + margin
        }else if popType == 2 {
            btnHM = margin
        }
        
        return lablHM + tableHM + btnHM
    }
    
    func getTableHeight() -> CGFloat {
        var tableHeight:CGFloat = 0
        
        if popType == 0 || popType == 1{
            tableHeight = CGFloat(titles.count) * cellHeight
            if tableHeight > screenHeight * 0.5 {
                tableHeight = screenHeight * 0.5
            }
        }else if popType == 2 {
            guard let models = applyprogresses else {
                return tableHeight
            }
            
            tableHeight = CGFloat(models.count) * cellHeight
            if tableHeight > screenHeight * 0.5 {
                tableHeight = screenHeight * 0.5
            }
        }
        
        return tableHeight
    }
    
    @objc func applyAction() {
        applyActivity?()
    }
}


extension EventHallApplyView:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if popType == 0 || popType == 1 {
            
            itemSelectedIndexs.removeAll()
            itemSelectedIndexs.append("\(indexPath.row)")
            self.didSelectRowAt?(indexPath.row)
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
}

extension EventHallApplyView:UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if popType == 0 || popType == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            let title = titles[indexPath.row]
            cell.textLabel?.text = title
        
            cell.accessoryType = itemSelectedIndexs.contains("\(indexPath.row)") ? .checkmark : .none
            cell.selectionStyle = .none
            return cell
        }else if popType == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: progressCellId, for: indexPath) as? EventProgessCell else {
                fatalError("dequeueReusableCell EventProgessCell error")
            }
            
            if let models = applyprogresses {
                let model = models[indexPath.row]
                cell.configWith(model: model,isHeader: indexPath.row == 0)
            }
            
            return cell
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if popType == 0 || popType == 1 {
            return titles.count
        }else if popType == 2 {
            return applyprogresses?.count ?? 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

//MARK: - 查询活动结果的展示
extension EventHallApplyView {
    //刷新页面，展示查询活动结果
    func updateWith(popType:Int,applyprogresses:[EventHallProgressModel]) {
        self.popType = popType
        self.applyprogresses = applyprogresses
        titleLabel.text = "审核进度"
        
        tableView.reloadData()
        reLayoutViews()
    }
    
    func reLayoutViews() {
        titleTip.snp.remakeConstraints { (make) in
            make.top.equalTo(margin)
            make.left.equalTo(margin)
            make.height.equalTo(labelHeight)
            make.width.equalTo(0)
        }
        
        let tableHeight = getTableHeight()
        
        tableView.snp.remakeConstraints { (make) in
            make.top.equalTo(titleTip.snp.bottom).offset(viewMargin)
            make.left.equalTo(margin)
            make.height.equalTo(tableHeight)
            make.right.equalTo(-margin)
        }
        
        applyBtn.snp.remakeConstraints { (make) in
            make.width.equalTo(0)
            make.height.equalTo(0)
            make.top.equalTo(0)
            make.left.equalTo(0)
        }
    }
    
}








