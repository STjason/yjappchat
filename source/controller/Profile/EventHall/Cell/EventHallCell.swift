//
//  EventHallCell.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/25.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher

class EventHallCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var desLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.init(hexString: "#093f78")
    }
    
    func configWith(des:String,img:String) {
        desLabel.text = des
        
        let imgurl = img.trimmingCharacters(in: .whitespacesAndNewlines)
        if let url = URL.init(string: imgurl) {
            image.kf.setImage(with: ImageResource.init(downloadURL: url), placeholder: UIImage.init(named: ""), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}
