//
//  EventHallModel.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/26.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class EventHallWraper: BaseWraper {
    var content:EventHallModel?
}

class EventHallModel: HandyJSON {
    var activeLogo = ""
    var active:[EventHallActive]?
    required init() {}
}

class EventHallActive: HandyJSON {
    var img = ""
    var id = ""
    var title = ""
    var titleImgUrl = ""
    required init() {}
}

