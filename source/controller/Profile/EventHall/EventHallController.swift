//
//  EventHallController.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/25.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//  活动大厅

import UIKit
import HandyJSON
import SnapKit

class EventHallController: BaseMainController {
    let cellIdentifier = "EventHallCell"
    let collectionFooterId = "EventHallFooter"
    let footerHeight:CGFloat = 80
    var headerMargin:CGFloat = 5
    var cellMargin:CGFloat = 10 //cell间的间距
    var items:[EventHallActive]? //首页活动的items
    var applyView:EventHallApplyView? //查询活动的进度view
    var selectedItem:EventHallActive? //要查询进度的活动的 item
    var applyProgressModels:[EventHallProgressModel]? //某个申请活动的状态列表
    
    lazy var bgView:UIButton = { //置灰背景
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addTarget(self, action: #selector(bgViewAction), for: .touchUpInside)
        return view
    }()
    
    lazy var collection:UICollectionView = {
        var cellWidth:CGFloat = (screenWidth - 2 * headerMargin - cellMargin - 1) * CGFloat(0.5)
        var cellHeight:CGFloat = cellWidth * 0.5 + 5 + 40 //5 img距top的高度，30 label的高度
        var layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = cellMargin
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = .zero
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize.init(width:cellWidth , height: cellHeight)
        layout.footerReferenceSize = CGSize.init(width: screenWidth, height: 80)
        layout.sectionInset = UIEdgeInsets(top: cellMargin, left: headerMargin, bottom: cellMargin, right: cellMargin)

        var collection = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.black
        collection.register(UINib.init(nibName: "EventHallCell", bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        collection.register(EventHallFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: collectionFooterId)
        
        collection.dataSource = self
        collection.delegate = self
        return collection
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        requestHallHallData()
    }
    
    func setupUI() {
       
        title = "活动大厅"
        self.view.backgroundColor = UIColor.black
        self.navigationItem.rightBarButtonItems?.removeAll()
        let searchBtn = UIBarButtonItem.init(image: UIImage.init(named: "eventHallApllyHis"), style: .plain, target: self, action: #selector(progressSearch))
        self.navigationItem.rightBarButtonItems = [searchBtn]
        
        view.addSubview(collection)
        
        collection.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    //查询进度
    @objc func progressSearch() {
        view.addSubview(bgView)
        
        if let models = items {
            if models.count > 0 {
                selectedItem = models[0]
            }
            
            var titles = [String]()
            for item in models {
                titles.append(item.title)
            }
            
            applyView = EventHallApplyView.init(titles: titles, title: "审核进度查询",popType:0)
            
            if let view = applyView {
                bgView.addSubview(view)
                
                view.didSelectRowAt = { [weak self] (index) in
                    guard let weakSelf = self else {return}
                    let item = models[index]
                    weakSelf.selectedItem = item
                }
                
                view.applyActivity = { [weak self] in
                    guard let weakSelf = self else {return}
                    if let model = weakSelf.selectedItem {
                        weakSelf.requestApplyProgress(activeId: model.id)
                    }
                }
                
                layoutView(viewHeight: view.getViewHeight())
            }
        }
    }
    
    //布局
    func layoutView(viewHeight:CGFloat) {
        bgView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        if let view = applyView {
            view.snp.makeConstraints { (make) in
                make.center.equalTo(bgView.snp.center)
                make.width.equalTo(ScreenWidth*0.9)
                make.height.equalTo(viewHeight)
            }
        }
    }
    
    //刷新 获得活动状态结果页面
    func layoutProgressPopView(viewHeight:CGFloat) {
        if let view = applyView {
            view.snp.remakeConstraints { (make) in
                make.center.equalTo(bgView.snp.center)
                make.width.equalTo(ScreenWidth*0.9)
                make.height.equalTo(viewHeight)
            }
        }
    }
    
    @objc func bgViewAction() {
        bgView.removeFromSuperview()
        applyView?.removeFromSuperview()
        applyView = nil
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: 数据请求
extension EventHallController {
    func requestHallHallData() {
        request(frontDialog: true, url: EVENT_HALL_LIST) {[weak self] (response, success) in
            guard let wraper = EventHallWraper.deserialize(from: response),let weakSelf = self else {return}
            
            if let content = wraper.content,let actives = content.active {
                weakSelf.items = actives
                weakSelf.collection.reloadData()
            }
        }
    }
    
    ///查询申请进度
    func requestApplyProgress(activeId:String) {
        request(frontDialog: true, url: EVENT_HALL_PROGRESS, params: ["activeId":activeId]) {[weak self] (response, success) in
            guard let weakSelf = self else {return}
            
            if !success {
                showToast(view: weakSelf.view, txt: response)
                return
            }
            
            let errorMsg = "查询进度失败"
            if let wraper = EvenHallProgressWraper.deserialize(from: response) {
                if wraper.success {
                    guard let models = wraper.content else {
                        showToast(view: weakSelf.view, txt: errorMsg)
                        return
                    }
                    
                    if models.count > 0 {
                        weakSelf.applyProgressModels = models
                        let model = EventHallProgressModel()
                        model.account = "会员帐号"
                        model.productName = "申请项目"
                        model.createDatetime = "申请时间"
                        model.status = "title"
                        weakSelf.applyProgressModels?.insert(model, at: 0)
                        
                        if let view = weakSelf.applyView {
                            view.updateWith(popType: 2, applyprogresses: weakSelf.applyProgressModels!)
                            
                            weakSelf.layoutProgressPopView(viewHeight: view.getViewHeight())
                        }
                    }else {
                        showToast(view: weakSelf.view, txt: "该项目没有申请记录")
                    }
                    
                }else {
                    let msg = wraper.msg.isEmpty ? errorMsg : wraper.msg
                    showToast(view: weakSelf.view, txt: msg)
                }
            }else {
                let msg = errorMsg
                showToast(view: weakSelf.view, txt: msg)
            }
            
        }
    }
}

extension EventHallController:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? EventHallCell,let models = items else {
            fatalError("dequeueReusableCell EventHallCell error")
        }
        
        let model = models[indexPath.row]
        cell.configWith(des: model.title, img: model.titleImgUrl)
        
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionFooter {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: collectionFooterId, for: indexPath)
            let labelTip = UILabel()
            let labelContent = UILabel()

            labelTip.textColor = UIColor.red
            labelTip.font = UIFont.systemFont(ofSize: 15)
            labelTip.text = "温馨提示："
            labelContent.textColor = UIColor.white
            labelContent.font = UIFont.systemFont(ofSize: 16)
            labelContent.numberOfLines = 0
            labelContent.text = "点击对应的活动申请，提交申请后专员将于2小时内审核办理，提交申请后点击查看申请进度!"

            let tipWidth = labelTip.sizeThatFits(CGSize.init(width: 120, height: CGFloat(MAXFLOAT))).width

            labelTip.frame = CGRect.init(x: 10, y: 5, width: tipWidth, height: 25)
            labelContent.frame = CGRect.init(x: 10+tipWidth, y: 5, width: screenWidth - (10+tipWidth+10), height:(footerHeight-2*10))

            view.addSubview(labelTip)
            view.addSubview(labelContent)
            
            return view
        }
        
        return UICollectionReusableView()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return items?.count != 0 ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: screenWidth, height: 80)
    }
}

extension EventHallController:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let items = items {
            let item = items[indexPath.row]
            
            let vc = EventHallDetailController()
            vc.activeId = item.id
            vc.title = item.title
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}


class EventHallFooter: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}



