//
//  EventHallProgressModel.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/27.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class EvenHallProgressWraper: BaseWraper {
    var content:[EventHallProgressModel]?
}

class EventHallProgressModel: HandyJSON {
    required init() {}
    var account = ""
    var productName = ""
    var createDatetime = ""
    var status = "" // 1 未处理, 2 处理成功, 3 处理失败,4 已取消
    var statusDes:String {
        get {
            var des = ""
            if status == "1" {
                des = "未处理"
            }else if status == "2" {
                des = "处理成功"
            }else if status == "3" {
                des = "处理失败"
            }else if status == "4" {
                des = "已取消"
            }else if status == "title" {
                des = "申请状态"
            }
            
            return des
        }
    }
//    "account": "coder31",
//    "accountId": 865134,
//    "activeId": 30,
//    "awardIndex": 1,
//    "awardType": 1,
//    "awardValue": 5.00,
//    "betRate": 1.000,
//    "createDatetime": 1582795743251,
//    "createUserAccount": "aqin1",
//    "createUserId": 682408,
//    "dataVersion": 0,
//    "id": 10254,
//    "productName": "有效投注1000+申请5元",
//    "remark": "",
//    "stationId": 30,
//    "status": 3
}

