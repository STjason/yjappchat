//
//  EventProgessCell.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/27.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//  活动查询结果的cell

import UIKit

class EventProgessCell: UITableViewCell {
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var applyTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //是否是表头
    func configWith(model:EventHallProgressModel,isHeader:Bool) {
        accountLabel.text = model.account
        projectLabel.text = model.productName
        if isHeader {
            applyTimeLabel.text = model.createDatetime
        }else {
            if let time = Int64(model.createDatetime) {
                applyTimeLabel.text = timeStampToString(timeStamp: time)
            }
        }

        statusLabel.text = model.statusDes
    }
    
}
