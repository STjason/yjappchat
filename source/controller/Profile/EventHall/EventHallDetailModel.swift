//
//  EventHallDetailModel.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/26.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//  活动大厅 活动详情

import UIKit
import HandyJSON

class EventHallDetailWraper: BaseWraper {
    var content:EventHallDetailModel?
}

class EventHallDetailModel: HandyJSON {
    var activity:EventHallDetailActivity?
    var awards:[EventHallDetailAwards]?
    required init() {}
}

class EventHallDetailActivity: HandyJSON {
    var activeType = ""
    var content = ""
    var id = ""
    var levelIds = ""
    var overTime = ""
    var paiXu = ""
    var stationId = ""
    var status = ""
    var title = ""
    var titleImgUrl = ""
    var updateTime = ""
    required init() {}
}

class EventHallDetailAwards: HandyJSON {
    var activeId = ""
    var awardIndex = ""
    var awardName = ""
    var awardType = 0
    var awardValue = ""
    var betRate = ""
    var id = ""
    required init() {}
}







