//
//  EventHallDetailController.swift
//  YiboGameIos
//
//  Created by admin on 2020/2/26.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//  活动大厅 详情页面

import UIKit
import SnapKit
import WebKit

class EventHallDetailController: BaseMainController {
    var activeId = ""
    var detailModel:EventHallDetailModel? //详情页面所有数据model
    var applyView:EventHallApplyView? //申请活动的view
    var selectedAward:EventHallDetailAwards? //选择的申请的活动
    
    lazy var bgView:UIButton = { //置灰背景
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addTarget(self, action: #selector(bgViewAction), for: .touchUpInside)
        return view
    }()
    //申请活动
    lazy var applyBtn:UIButton = {
        let view = UIButton()
        view.layer.cornerRadius = 3
        view.titleLabel?.font = UIFont.systemFont(ofSize: 25)
        view.setTitleColor(UIColor.darkText, for: .normal)
        view.setTitle("申请活动", for: .normal)
        view.addTarget(self, action: #selector(applyAction), for: .touchUpInside)
        return view
    }()
    
    lazy var webView:WKWebView = {
        let view = WKWebView.init()
        view.sizeToFit()
        view.scrollView.bounces = false
        view.isOpaque = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        requestEventHallDetail(activeId: activeId)
    }
    
    func setupUI() {
        view.backgroundColor = UIColor.black
        
        view.addSubview(applyBtn)
        view.addSubview(webView)
        
        let margin:CGFloat = 40
        let frame = CGRect.init(x: 0, y: 0, width: (screenWidth - margin * 2), height: 50)
        
        let gradientView = UIView()
        view.insertSubview(gradientView, belowSubview: applyBtn)
        createGradientLayer(view: gradientView, frame: frame, hexColors: ["#caa04b","#fdfd8f"],cornerRadius: 3)
        
        applyBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(30 + KNavHeight)
            make.left.equalTo(margin)
            make.right.equalTo(-margin)
            make.height.equalTo(50)
        }
        
        gradientView.snp.makeConstraints { (make) in
            make.edges.equalTo(applyBtn.snp.edges)
        }
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(applyBtn.snp.bottom).offset(10)
            make.left.equalTo(20)
            make.bottom.equalTo(-10)
            make.right.equalTo(-20)
        }
    }
    
    @objc func applyAction() {
        view.addSubview(bgView)
        
        if let model = detailModel,let awards = model.awards {
            if awards.count > 0 {
                selectedAward = awards[0]
            }
            
            var titles =  [String]()
            for award in awards {
                titles.append(award.awardName)
            }
            
            applyView = EventHallApplyView.init(titles: titles, title: (model.activity?.title ?? "没有名称"),popType: 1)
            
            if let view = applyView {
                bgView.addSubview(view)
                
                view.didSelectRowAt = { [weak self] (index) in
                    guard let weakSelf = self else {return}
                    let award = awards[index]
                    weakSelf.selectedAward = award
                }
                
                view.applyActivity = { [weak self] in
                    guard let weakSelf = self else {return}
                    if let model = weakSelf.selectedAward {
                        weakSelf.requestApply(activeId: model.activeId, activeName: model.awardIndex)
                    }
                }
                
                layoutView(viewHeight: view.getViewHeight())
            }
        }
    }
    
    @objc func bgViewAction() {
        bgView.removeFromSuperview()
        applyView?.removeFromSuperview()
        applyView = nil
    }
    
    //布局
    func layoutView(viewHeight:CGFloat) {
        bgView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        if let view = applyView {
            view.snp.makeConstraints { (make) in
                make.center.equalTo(bgView.snp.center)
                make.width.equalTo(ScreenWidth*0.9)
                make.height.equalTo(viewHeight)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

//数据相关
extension EventHallDetailController {
    //请求本页面所有数据
    func requestEventHallDetail(activeId:String) {
        request(frontDialog: true, method: .get, url: EVENT_HALL_DETAIL, params: ["activeId":activeId]) {[weak self] (response, success) in
            guard let weakSelf = self else {return}
            
            if let wraper = EventHallDetailWraper.deserialize(from: response),let model = wraper.content {
                weakSelf.detailModel = model
                weakSelf.loadWeb(model: model)
            }
        }
    }
    
    //申请活动
    func requestApply(activeId:String, activeName:String) {
        request(frontDialog: true, method: .get, url: EVENt_HALL_APPLY, params: ["activeId":activeId,"activeName":activeName]) {[weak self] (response, success) in
            guard let weakSelf = self else {return}
            
            if let wraper = EventHallWraper.deserialize(from: response) {
                if wraper.success {
                   showToast(view: weakSelf.view, txt: "活动申请成功")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        weakSelf.bgViewAction()
                    })
                }else {
                    var msg = "申请活动失败"
                    if !wraper.msg.isEmpty {
                        msg = wraper.msg
                    }
                    
                    showToast(view: weakSelf.view, txt: msg)
                }
            }
        }
    }
    
    //用html代码加载webview
    func loadWeb(model:EventHallDetailModel) {
        if let activety = model.activity {
//            sb.append("<html><head><meta http-equiv='content-type' content='text/html; charset=utf-8'>");
//            sb.append("<meta charset='utf-8'  content='1'></head><body style='color: black'><p></p>");
            //
            //< meta http-equiv="refresh"content="time" url="url" >
            //添加文件的内容
//            sb.append(content);
         let htmlString = "<html><head><meta http-equiv='content-type' content='text/html; charset=utf-8'>" +  "<meta charset='utf-8' content='1'></head><body style='color: black'> <p></p>" +  "\(activety.content)" + "</body></html>"
            //加载本地文件
            // sb.append("<img src='file:///"+AContext.getFileUtil().getDownloadsPath()+"'>");
//            sb.append("</body></html>");
            webView.loadHTMLString(htmlString, baseURL: nil)
        }
    }
}










