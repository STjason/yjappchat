//
//  PublicWebViewController.swift
//  YiboGameIos
//
//  Created by JK on 2019/10/7.
//  Copyright © 2019 com.lvwenhan. All rights reserved.
//

import UIKit
import WebKit

class PublicWebViewController: BaseController, WKNavigationDelegate, WKUIDelegate {
    
    /** wkWebView */
    var _webView : WKWebView?
    /** 进度条 */
    var progressViewLayer:CALayer!
    /** 接口 */
    var url : String?
    /** 领取按钮 */
    var _recordBtn : UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 添加webView
        view.addSubview(self.webView())
        // 添加领取按钮
        view.addSubview(self.recordBtn())
        // 添加领取记录入口
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "记录", style: UIBarButtonItem.Style.plain, target: self, action: #selector(changeToRecord))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", style: .plain, target: self, action: #selector(backLastCtrl))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style:.plain , target: self, action: #selector(goBack))
    }
    
    // 返回
    @objc func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // 跳转领取记录
    @objc func changeToRecord() {
        let vc = CollectionRecordsViewController()
        if url == BONUS_PAGE_DATA {
            vc.title = "每日加奖记录"
            vc.url = NATIVE_RECEIVE_BONUS
        }else if url == NATIVEDEFICIT_PAGE_DATA {
            vc.title = "周周转运记录"
            vc.url = NATIVE_RECEIVE_DEFICIT
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //返回上一界面
    @objc func  backLastCtrl(){
        dismiss(animated: true, completion: nil)
    }
    
    // 领取事件
    @objc func recordClick() {
        if url == BONUS_PAGE_DATA {
            self.requestRecord(url: NATIVE_RECEIVE_BONUS)
        }else if url == NATIVEDEFICIT_PAGE_DATA {
            self.requestRecord(url: NATIVE_RECEIVE_DEFICIT)
        }
    }
    
    func requestRecord(url: String) {
        
        request(frontDialog: true, method: .post, loadTextStr: "领取奖励..", url: url, params: [:]) { (resultJson, resultStatus) in
            if resultStatus == true {
                let dic = stringValueDict(resultJson)
                if dic!["success"] as! Bool == true {
                    showErrorHUD(errStr: "领取成功!")
                }else
                {
                    showErrorHUD(errStr: "领取失败!")
                }
            }else {
                showErrorHUD(errStr: "请求超时,请检查网络!")
            }
        }
    }
    
    //#MARK: ------------------------- webViewDelegate ---------------------------------
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let jsStr = "document.getElementsByClassName(\"bar bar-nav\")[0].style.display=\'none\';document.getElementsByClassName(\"content\")[0].style.marginTop=\'-50px\';document.getElementById(\"bonusBtn\").style.display=\'none\';"
        webView.evaluateJavaScript(jsStr, completionHandler: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress"{
            guard let changes = change else{return}
            
            let oldProgress = changes[NSKeyValueChangeKey.oldKey] as? Double ?? 0
            let newProgress = changes[NSKeyValueChangeKey.newKey] as? Double ?? 0
            
            //进度要大于0.1 给的默认值
            if newProgress > oldProgress && newProgress > 0.1{
                progressViewLayer.frame = CGRect(x: 0, y: 0, width: newProgress * Double(self.webView().frame.width), height: 3)
            }
            
            if newProgress >= 1.0{
                let time1 = DispatchTime.now() + 0.4
                let time2 = time1 + 0.1
                DispatchQueue.main.asyncAfter(deadline: time1) {
                    weak var weakself = self
                    weakself?.progressViewLayer!.opacity = 0
                }
                DispatchQueue.main.asyncAfter(deadline: time2) {
                    weak var weakself = self
                    weakself?.progressViewLayer!.frame = CGRect(x: 0, y: 0, width: 0, height: 3)
                }
            }
        }else{
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }

    //#MARK: ------------------------- 实例化 ---------------------------------
    func webView() -> WKWebView {
        if _webView == nil {
            let config = WKWebViewConfiguration()
            let webHeight = IS_IPHONE_X ? kScreenHeight - CGFloat(KNavHeight) - kCurrentScreen(x: 150) - CGFloat(34) : kScreenHeight - CGFloat(KNavHeight) - kCurrentScreen(x: 150)
            _webView = WKWebView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: webHeight), configuration: config)
            _webView?.uiDelegate = self
            _webView?.navigationDelegate = self
            _webView?.scrollView.bounces = false
            
            var request = URLRequest.init(url: URL.init(string: "\(BASE_URL)" + "\(url ?? "")")!)
            
            //监听WKWebView进度条属性
            _webView?.addObserver(self, forKeyPath: "estimatedProgress", options: [.old,.new], context: nil)
            
            let progressView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 3))
            _webView?.addSubview(progressView)
            
            progressViewLayer = CALayer()
            progressViewLayer.backgroundColor = UIColor.colorWithHexString("#52e20b").cgColor
            progressViewLayer.frame = CGRect(x: 0, y: 0, width: kScreenWidth * 0.05, height: 3)
            progressView.layer.addSublayer(progressViewLayer)
            
            // 同步cookie
            let DomainUrl = getDomainUrl()
            let localhostComponents = DomainUrl.components(separatedBy: "//")
            var localhost = "localhost"
            if localhostComponents.count >= 2 {
                localhost = localhostComponents[1]
            }
            
            var cookieProperties0 =  [HTTPCookiePropertyKey : Any]()
            cookieProperties0[HTTPCookiePropertyKey.name] = "SESSION"
            cookieProperties0[HTTPCookiePropertyKey.value] = YiboPreference.getToken()
            cookieProperties0[HTTPCookiePropertyKey.domain] = localhost
            cookieProperties0[HTTPCookiePropertyKey.originURL] = DomainUrl
            cookieProperties0[HTTPCookiePropertyKey.path] = "/"
            cookieProperties0[HTTPCookiePropertyKey.version] = "0"
            cookieProperties0[HTTPCookiePropertyKey.expires] = Date(timeIntervalSinceNow: 60 * 60)
            cookieProperties0[HTTPCookiePropertyKey.discard] = 0
            
            let cookie0 = HTTPCookie(properties: cookieProperties0)
            HTTPCookieStorage.shared.setCookie(cookie0!)
            //            loadRequest前获取token
            if let cookies = HTTPCookieStorage.shared.cookies {
                for cookie in cookies {
                    print("cookies: \(cookie)\n")
                }
            }
            request.addValue("SESSION=\(YiboPreference.getToken())", forHTTPHeaderField: "Cookie")
            
            _webView?.load(request)
            
        }
        return _webView!
    }
    
    func recordBtn() -> UIButton {
        if _recordBtn == nil {
            _recordBtn = UIButton(type: .custom)
            _recordBtn?.frame = kCGRect(x: 0, y: self.webView().frame.maxY, width: kScreenWidth, height: kCurrentScreen(x: 150))
            _recordBtn?.backgroundColor = .red
            _recordBtn?.setTitle("领取奖励", for: .normal)
            _recordBtn?.setTitleColor(.white, for: .normal)
            _recordBtn?.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 60))
            _recordBtn?.layer.cornerRadius = kCurrentScreen(x: 25)
            _recordBtn?.clipsToBounds = true
            
            _recordBtn?.addTarget(self, action: #selector(recordClick), for: .touchUpInside)
        }
        return _recordBtn!
    }
}
