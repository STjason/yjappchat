//
//  ProfileWeekLuckDataItem.swift
//  gameplay
//
//  Created by Gallen on 2019/12/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class ProfileWeekLuckDataItem: HandyJSON {
    var success = false
    var accessToken = ""
    var content:weekLucklyContentItem?

    required init(){}
}

class weekLucklyContentItem:HandyJSON{
    
    var deficitData:[weekLucklyDeficitDataItem]?
    var model = false
    var recordData:weekLucklyRecordDataItem?
    var curLevelName = ""
    var betNum:[betNumsItem]?
    var scale:bounsDayScaleItem?
    var levels:[levelBounsItem]?
    var firstBonus:firstBonusItem?
    var totalData:totalDataItem?
    required init(){}
}

class weekLucklyDeficitDataItem:HandyJSON{
    var maxMoney = ""
    var minMoney = ""
    var money = ""
    var multiple = ""
    var stationId = ""
    var status = ""
    var type = ""
    required init(){}
}
class weekLucklyRecordDataItem:HandyJSON{
    var record:bonusRecordItem?
    var bonus:bonusItem?
    required init(){}
}

class recivcePrizeItem:HandyJSON{
    var success:Bool = false
    var accessToken = ""
    var msg:String = ""
    var content:[bonusRecordItem]?
    required init(){}
}

class bounsDayScaleItem:HandyJSON{
    /*{
     "9_49": 200,
     "8_49": 1.5,
     "12_49": 1.5,
     "10_49": 1.5,
     "11_49": 2.5
     }*/
//    var 8_49 = ""
//    var 9_49 = ""
//    var 10_49 = ""
//    var 11_49 = ""
//    var 12_49 = ""
    required init(){}
}

class betNumsItem:HandyJSON{
    required init(){}
}

class levelBounsItem:HandyJSON{
    var betNum = ""
    var createDatetime = ""
    var depositMoney = ""
    var icon = ""
    var id = ""
    var levelDefault = ""
    var levelType = ""
    var memberCount = ""
    var name = ""
    var remark = ""
    var sendMsgFlag = ""
    var stationId = ""
    var status = ""
    /**等级*/
    var levelValue = ""
    var levelKey:Int = 0
    required init(){}
}
class firstBonusItem:HandyJSON{
    var betNum = 0
    var id = ""
    var levelId = ""
    var levelName = ""
    var multiple = ""
    var numScale = ""
    var stationId = ""
    var status = ""
   required init(){}
}
class bonusRecordItem:HandyJSON{
    //    "account": "test900",
//    "accountId": 1348,
//    "balance": 84.15,
//    "createTime": "2020-01-06 19:32:45",
//    "deficitMoney": 4207.80,
//    "id": 7,
//    "stationId": 2,
//    "status": 1
    
//}
    
//    "account": "test900",
//    "accountId": 1348,
//    "balance": 125.0000,
//    "betNum": 5000.00,
//    "id": 9,
//    "scale": 2.50,
//    "statDate": "2020-01-07",
//    "stationId": 2,
//    "status": 1
    var balance = ""
    var betNum = ""
    var scale = ""
    var status = ""
    var deficitMoney = ""
    var createTime = ""
    var accountId = ""
    var account = ""
    var id = ""
    var stationId = ""
    var statDate = ""
    required init(){}
}
class bonusItem:HandyJSON{
//    "betNum": 49,
//    "id": 6,
//    "levelId": 10,
//    "levelName": "黄金会员",
//    "multiple": 1,
//    "numScale": 1.5,
//    "stationId": 2,
//    "status": 2
    var betNum = ""
    var id = ""
    var levelId = ""
    var levelName = ""
    var multiple = ""
    var numScale = ""
    var stationId = ""
    var status = ""
    required init(){}
}

class totalDataItem:HandyJSON{
    var accountId = ""
    /**每日加奖的领取金额*/
    var bonusAmount = ""
    /**每日加奖的领取次数*/
    var bonusTimes :Int = 0
    /**周周转运的领取累计金额*/
    var deficitAmount = ""
    /**周周转运的领取次数*/
    var deficitTimes = 0
    
    var stationId = ""
    required init(){}
}

class contentDataItem:HandyJSON{
//    "account": "test900",
//    "accountId": 1348,
//    "balance": 125,
//    "betNum": 5000,
//    "id": 9,
//    "scale": 2.5,
//    "statDate": "2020-01-07",
//    "stationId": 2,
//    "status": 1
    var account = ""
    var accountId = ""
    var balance = ""
    var betNum = ""
    var id = ""
    var scale = ""
    var statDate = ""
    var stationId = ""
    var status = ""
    required init(){}}


