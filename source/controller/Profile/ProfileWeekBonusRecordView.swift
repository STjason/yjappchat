//
//  ProfileWeekBonusRecordView.swift
//  gameplay
//
//  Created by Gallen on 2019/12/31.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class ProfileWeekBonusRecordView: UIView {
    //是否显示
    var isShow = false
    
    var prizeKind:prizeType?{
        didSet{
            lastWeekDeficitLabel.text = prizeKind == .week ? "上周亏损" : "加奖比例"
            lucklyMoneyLabel.text = prizeKind == .week ? "转运金" : "加奖金额"
        }
    }
    
    var receiveRecords:[bonusRecordItem] = [bonusRecordItem](){
        didSet{
            recordTableView.reloadData()
        }
    }
    
    var labelWidth =  (kScreenWidth - 60) * 0.25
    let labelHeight = CGFloat(30)

    lazy var backView:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.backgroundColor = UIColor.white
        view.layer.masksToBounds = true
        view.layer.borderColor = UIColor.colorWithHexString("#33b23f").cgColor
        view.layer.borderWidth = 2
        return view
    }()
    
    lazy var recordTitleLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithHexString("#33b23f")
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.text = (prizeKind == .week) ?  "周周转运金记录(只显示最近90天的记录)":"每日加奖金记录(只显示最近90天的记录)"
        return label
    }()
    //关闭
    lazy var closeButton:UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle("X", for: .normal)
        button.addTarget(self, action: #selector(closeRecordViewClick), for: .touchUpInside)
        return button
    }()
    
    lazy var titleView:UIView = {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: labelHeight))
        view.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        return view
    }()
    //领取时间
    lazy var reciveDateLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: 0, y: 0, width: labelWidth, height: labelHeight))
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "领取时间"
        label.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        label.textAlignment = .center
        return label
    }()
    //打码量
    lazy var betNumLabel:UILabel = {
        let x = prizeKind == .week ? 0:labelWidth
        let label = UILabel.init(frame: CGRect(x: reciveDateLabel.frame.maxX, y: 0, width: x, height: labelHeight))
        label.text = "打码量"
        label.font = UIFont.systemFont(ofSize: 13)
        label.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        label.textAlignment = .center
        return label
    }()
    
    //上周亏损
    lazy var lastWeekDeficitLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: betNumLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = prizeKind == .week ? "上周亏损" : "加奖比例"
        label.textAlignment = .center
        label.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        return label
    }()
    //转运金
    lazy var lucklyMoneyLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: lastWeekDeficitLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = prizeKind == .week ? "转运金" : "加奖金额"
        label.textAlignment = .center
        label.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        return label
    }()
    //领取状态
    lazy var reciveStatusLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: lucklyMoneyLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "状态"
        label.textAlignment = .center
        label.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        return label
    }()
    
    lazy var recordTableView:UITableView = {
        let tableView = UITableView()
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(ProfileLucklyRecordCell.self, forCellReuseIdentifier: "ProfileLucklyRecordCell")
        return tableView
    }()
    
    init(frame: CGRect,records:[bonusRecordItem],prizeKind:prizeType) {
        super.init(frame: frame)
        self.receiveRecords = records
        self.prizeKind = prizeKind
        
        labelWidth = prizeKind == .week ?  ((kScreenWidth - 60) * 0.25) : ((kScreenWidth - 60) * 0.2)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        addSubview(backView)
        backView.addSubview(recordTitleLabel)
        backView.addSubview(closeButton)
        backView.addSubview(recordTableView)
        backView.addSubview(titleView)
        
        titleView.snp.makeConstraints { (make) in
            make.top.equalTo(recordTitleLabel.snp.bottom).offset(20)
            make.left.right.equalTo(backView)
            make.height.equalTo(labelHeight)
        }
        
        titleView.addSubview(reciveDateLabel)
        titleView.addSubview(betNumLabel)
        titleView.addSubview(lastWeekDeficitLabel)
        titleView.addSubview(lucklyMoneyLabel)
        titleView.addSubview(reciveStatusLabel)
        
        backView.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(400)
            make.centerY.equalTo(self.snp.centerY)
        }
        recordTitleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView)
            make.top.equalTo(40)
        }
        closeButton.snp.makeConstraints { (make) in
            make.right.equalTo(backView).offset(-5)
            make.top.equalTo(backView).offset(5)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        recordTableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleView.snp.bottom)
            make.left.right.bottom.equalTo(backView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension ProfileWeekBonusRecordView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receiveRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileLucklyRecordCell") as?  ProfileLucklyRecordCell
        //接收
        let recordItem = receiveRecords[indexPath.row]
        cell?.prizeType = prizeKind ?? .week
        cell?.weekLucklyItem = recordItem
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
//MARK:事件响应
extension ProfileWeekBonusRecordView{
    //关闭视图
    @objc private func closeRecordViewClick(){
        removeFromSuperview()
        isShow = false
    }
}
