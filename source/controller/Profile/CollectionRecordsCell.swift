//
//  CollectionRecordsCell.swift
//  YiboGameIos
//
//  Created by JK on 2019/10/7.
//  Copyright © 2019 com.lvwenhan. All rights reserved.
//

import UIKit

class CollectionRecordsCell: UITableViewCell {
    
    /** 昨日打码 */
    var _yesterdayCodeLabel : UILabel?
    /** 金额 */
    var _amountMoneyLabel : UILabel?
    /** 领取日期 */
    var _dateLabel : UILabel?
    /** 领取状态 */
    var _statusLabel : UILabel?
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.yesterdayCodeLabel())
        self.contentView.addSubview(self.amountMoneyLabel())
        self.contentView.addSubview(self.dateLabel())
        self.contentView.addSubview(self.statusLabel())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getRecordsModel(model: CollectionRecordsModel, status: ActivityStatus) {
        if status == .DailyBonusStatus {
            self.yesterdayCodeLabel().text = "昨日打码:\(model.betNum!)"
            self.amountMoneyLabel().text = "加奖金额:\(model.balance ?? "")"
            self.dateLabel().text = "领取时间:\(model.statDate ?? "")"
        }else
        {
            self.yesterdayCodeLabel().text = "上周亏损\(model.deficitMoney ?? "")"
            self.amountMoneyLabel().text = "转运金额:\(model.balance ?? "")"
            self.dateLabel().text = "领取时间:\(model.createTime ?? "")"
        }
        
        if model.status == "1" {
            self.statusLabel().text = "领取成功"
            self.statusLabel().textColor = .green
        }else {
            self.statusLabel().text = "领取失败"
            self.statusLabel().textColor = .red
        }
        
    }
    
    //#MARK: ------------------------- 实例化 ---------------------------------
    func yesterdayCodeLabel() -> UILabel {
        if _yesterdayCodeLabel == nil {
            _yesterdayCodeLabel = UILabel(frame: kCGRect(x: kCurrentScreen(x: 30), y: kCurrentScreen(x: 20), width: kScreenWidth/2 - kCurrentScreen(x: 30), height: kCurrentScreen(x: 50)))
            _yesterdayCodeLabel?.textColor = .black
            _yesterdayCodeLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _yesterdayCodeLabel?.textAlignment = .left
            
        }
        return _yesterdayCodeLabel!
    }
    
    func amountMoneyLabel() -> UILabel {
        if _amountMoneyLabel == nil {
            _amountMoneyLabel = UILabel(frame: kCGRect(x: kScreenWidth/2, y: kCurrentScreen(x: 20), width: kScreenWidth/2 - kCurrentScreen(x: 30), height: kCurrentScreen(x: 50)))
            _amountMoneyLabel?.textColor = .black
            _amountMoneyLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _amountMoneyLabel?.textAlignment = .right
            
        }
        return _amountMoneyLabel!
    }
    
    func dateLabel() -> UILabel {
        if _dateLabel == nil {
            _dateLabel = UILabel(frame: kCGRect(x: kCurrentScreen(x: 30), y: self.amountMoneyLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth, height: kCurrentScreen(x: 45)))
            _dateLabel?.textColor = .black
            _dateLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
            _dateLabel?.textAlignment = .left
            
        }
        return _dateLabel!
    }
    
    func statusLabel() -> UILabel {
        if _statusLabel == nil {
            _statusLabel = UILabel(frame: kCGRect(x: kScreenWidth/2, y: self.amountMoneyLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth/2 - kCurrentScreen(x: 30), height: kCurrentScreen(x: 50)))
            _statusLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _statusLabel?.textAlignment = .right
            
        }
        return _statusLabel!
    }
    
}
