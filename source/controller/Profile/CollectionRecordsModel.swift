//
//  CollectionRecordsModel.swift
//  YiboGameIos
//
//  Created by JK on 2019/10/7.
//  Copyright © 2019 com.lvwenhan. All rights reserved.
//

import UIKit

class CollectionRecordsModel: NSObject {

    /** 账户信息 */
    var account : String?
    /** 账户id */
    var accountId : String?
    /** 余额 */
    var balance : String?
    /** 打码量 */
    var betNum : String?
    /** 亏损金额 */
    var deficitMoney : String?
    /** id */
    var id : String?
    /** 每日加奖金额 */
    var scale : String?
    /** 每日加奖领取日期 */
    var statDate : String?
    /** 周周转运领取日期 */
    var createTime : String?
    /**  */
    var stationId : String?
    /** 领取状态 */
    var status : String?
    
    func getCurrentJson(dic: NSDictionary) -> CollectionRecordsModel {
        let model = CollectionRecordsModel()
        
        model.account       = String(format: "%@", dic.kValue(forKey: "account") as CVarArg)
        model.accountId     = String(format: "%@", dic.kValue(forKey: "accountId") as CVarArg)
        model.balance       = String(format: "%@", dic.kValue(forKey: "balance") as CVarArg)
        model.betNum        = String(format: "%@", dic.kValue(forKey: "betNum") as CVarArg)
        model.deficitMoney  = String(format: "%@", dic.kValue(forKey: "deficitMoney") as CVarArg)
        model.scale         = String(format: "%@", dic.kValue(forKey: "scale") as CVarArg)
        model.id            = String(format: "%@", dic.kValue(forKey: "id") as CVarArg)
        model.statDate      = String(format: "%@", dic.kValue(forKey: "statDate") as CVarArg)
        model.stationId     = String(format: "%@", dic.kValue(forKey: "stationId") as CVarArg)
        model.status        = String(format: "%@", dic.kValue(forKey: "status") as CVarArg)
        model.createTime    = String(format: "%@", dic.kValue(forKey: "createTime") as CVarArg)
        
        return model
    }
}
