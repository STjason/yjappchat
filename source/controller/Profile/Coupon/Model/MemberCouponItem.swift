//
//  MemberCouponItem.swift
//  gameplay
//
//  Created by Gallen on 2020/1/15.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class MemberCouponItem: HandyJSON {
    
    var success:Bool = false
    var accessToken:couponAccessTokenItem?
    var msg = ""
    required init(){}

}

class couponAccessTokenItem:HandyJSON{
    var currentPageNo = ""
    var hasNext = ""
    var hasPre = ""
    var nextPage = ""
    var pageSize = ""
    var prePage = ""
    var start = ""
    var total = ""
    var totalPageCount = ""
    var rows:[CouponRowsItem]?
    required init(){}
}

class CouponRowsItem:HandyJSON{
    //"cid": 2,
//    "couponsName": "Ray专用",
//    "couponsTypeStr": "平台回馈",
//    "createDatetime": 1579098848446,
//    "denomination": 10.000000,
//    "id": 2,
//    "stationId": 2,
//    "status": 1,
//    "useEndDate": 1579622400000,
//    "useStartDate": 1579104000000,
//    "useType": 1,
//    "userId": 1395,
//    "userName": "test901"
    var cid:String = ""
    var couponsName:String = ""
    var couponsTypeStr:String = ""
    var createDatetime:String = ""
    var denomination:String = ""
    var id :String = ""
    var stationId:String = ""
    /**1未使用*/
    var status:String = ""
    var useEndDate:TimeInterval = 0
    var useStartDate:TimeInterval = 0
    var useType:String = ""
    var userId:String = ""
    var userName:String = ""
    var minAmount = ""
    var maxAmount = ""
    required init(){}
}
