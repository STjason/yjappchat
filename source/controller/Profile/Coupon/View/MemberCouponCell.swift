//
//  MemberCouponCell.swift
//  gameplay
//
//  Created by Gallen on 2020/1/15.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit


protocol MemberCouponCellDelegate:NSObjectProtocol {
    /**去使用*/
    func goUseClick(couponItem:CouponRowsItem?)
    /**删除当时代金券*/
    func deleteCurrentCoupon(couponItem:CouponRowsItem?)
}

class MemberCouponCell: UITableViewCell {
    //金额
    lazy var moneyLabel:UILabel = {
        let label = UILabel()
        label.text = "金额:10元"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var couponNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "老用户回馈-10元代金券"
        return label
    }()
    
    lazy var remarkLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "今日充值满0元可用"
        return label
    }()
    
    lazy var statusLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "长期有效"
        return label
    }()
    //去使用
    lazy var goUserButton:UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.colorWithHexString("#ef0f82")
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        button.addTarget(self, action: #selector(goUseClick), for: .touchUpInside)
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.setTitle("去使用", for: .normal)
        return button
    }()
    //删除
    lazy var deleteButton:UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.colorWithHexString("#ef0f82")
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("删除", for: .normal)
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        button.addTarget(self, action: #selector(deleteClick), for: .touchUpInside)
        return button
    }()
    
    var couponCellDelegate:MemberCouponCellDelegate?
    
    var couponItem:CouponRowsItem?{
        didSet{
            //
            if let couponsItem = couponItem{
                moneyLabel.text = String.init(format:"金额:%@元",couponsItem.denomination)
                couponNameLabel.text = couponsItem.couponsTypeStr + " - " + couponsItem.couponsName
                //显示有效时长
                if couponsItem.useStartDate > 0{
                    let validityDuartion = NSString(string: "有效期:" +  String().getStringForTimeStamp(time: couponsItem.useStartDate) + "~" +  String().getStringForTimeStamp(time: couponsItem.useEndDate))
                    let range = validityDuartion.range(of: "有效期:")
                    //富文本
                    let atrribute = NSMutableAttributedString.init(string: validityDuartion as String)
                    atrribute.addAttributes([NSAttributedString.Key.foregroundColor:UIColor.black], range: range)
                    statusLabel.textColor = UIColor.colorWithHexString("#228b22")
                    statusLabel.attributedText = atrribute
                }else{
                    statusLabel.text = "长期有效"
                    statusLabel.textColor = UIColor.black
                }
                if !isEmptyString(str: couponsItem.maxAmount) && couponsItem.maxAmount != "0" {
                    
                    remarkLabel.text = "备注:今日充值满" + couponsItem.minAmount + "~" + couponsItem.maxAmount + "元可用"
                }else{
                    remarkLabel.text = "备注:今日充值满0元可用"
                }
                //过期
                if couponsItem.status == "1"{
                    goUserButton.setTitle("去使用", for: .normal)
                    goUserButton.backgroundColor = UIColor.colorWithHexString("#ef0f82")
                }else if couponsItem.status == "2"{
                    goUserButton.setTitle("已使用", for: .normal)
                    goUserButton.backgroundColor = UIColor.gray
                }else{
                    goUserButton.backgroundColor = UIColor.gray
                    goUserButton.setTitle("已过期", for: .normal)
                }
                
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        contentView.addSubview(moneyLabel)
        contentView.addSubview(couponNameLabel)
        contentView.addSubview(remarkLabel)
        contentView.addSubview(statusLabel)
        contentView.addSubview(goUserButton)
        contentView.addSubview(deleteButton)
        self.backgroundColor = UIColor.clear

        
        moneyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(10)
            make.top.equalTo(contentView).offset(10)
            make.height.equalTo(16)
        }
        
        couponNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(moneyLabel.snp.left)
            make.top.equalTo(moneyLabel.snp.bottom).offset(10)
            make.height.equalTo(16)
        }
        
        remarkLabel.snp.makeConstraints { (make) in
            make.top.equalTo(couponNameLabel.snp.bottom).offset(10)
            make.left.equalTo(moneyLabel.snp.left)
            make.right.equalTo(contentView).offset(-10)
            make.height.greaterThanOrEqualTo(16)
        }
        
        statusLabel.snp.makeConstraints { (make) in
            make.top.equalTo(remarkLabel.snp.bottom).offset(10)
            make.left.equalTo(moneyLabel.snp.left)
            make.right.equalTo(contentView).offset(-10)
            make.bottom.equalTo(contentView).offset(-10)
            make.height.equalTo(16)
        }
        
        deleteButton.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-10)
            make.centerY.equalTo(moneyLabel.snp.centerY)
            make.width.equalTo(40)
            make.height.equalTo(25)
        }
        
        goUserButton.snp.makeConstraints { (make) in
            make.right.equalTo(deleteButton.snp.left).offset(-5)
            make.centerY.equalTo(deleteButton.snp.centerY)
            make.width.equalTo(40)
            make.height.equalTo(25)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension MemberCouponCell{
    //去使用
    @objc func goUseClick(){
        //去使用当前代金券
        couponCellDelegate?.goUseClick(couponItem: couponItem)
    }
    @objc func deleteClick(){
        //删除当前代金券
        couponCellDelegate?.deleteCurrentCoupon(couponItem: couponItem)
    }
}
