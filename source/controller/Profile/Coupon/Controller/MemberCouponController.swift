//
//  MemberCouponController.swift
//  gameplay
//
//  Created by Gallen on 2020/1/15.
//  Copyright © 2020年 yibo. All rights reserved.
//  代金券

import UIKit

enum couponStatus {
    /**未使用*/
    case noUse
    /**已使用*/
    case alreadlyUse
    /**已过期*/
    case expired
}

class MemberCouponController: BaseController {
    /**默认的代金券的状态*/
    var type:couponStatus = .noUse
    
    let buttonWidth = kScreenWidth / 3
    
    var filterStartTime = ""
    var filterEndTime = ""
    
    var firstEnter:Bool = true
    
    var coupons:[CouponRowsItem] = [CouponRowsItem]()
    //未使用
    var alreadlyUseCoupons:[CouponRowsItem] = [CouponRowsItem]()
    //已过期
    var expiredCoupons:[CouponRowsItem] = [CouponRowsItem]()
    
    lazy var couponTableView:UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 249, alpha: 1.0)
    
        tableView.register(MemberCouponCell.self, forCellReuseIdentifier: "MemberCouponCell")
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tableView
    }()
    
    lazy var tipsLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "    我的代金券(使用说明:当日累计充值满足条件即可使用代金券,使用成功后账号余额直接加代金券对应面额。)"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    
    lazy var headView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var noUserButton:UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: buttonWidth, height: 40))
        button.setTitle("未使用", for: .normal)
        button.backgroundColor = UIColor.colorWithRGB(r: 82, g: 82, b: 82, alpha: 1.0)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.isSelected = true
        button.setTitleColor(UIColor.white, for: .selected)
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(useStatusClick(button:)), for: .touchUpInside)
        return button
    }()
    
    lazy var alreadlyButton:UIButton = {
        let button = UIButton(frame: CGRect(x: noUserButton.frame.maxX, y: 0, width: buttonWidth, height: 40))
        button.setTitle("已使用", for: .normal)
        button.setTitleColor(UIColor.white, for: .selected)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.addTarget(self, action: #selector(useStatusClick(button:)), for: .touchUpInside)
        return button
    }()
    
    lazy var expiredButton:UIButton = {
        let button = UIButton(frame: CGRect(x: alreadlyButton.frame.maxX, y: 0, width: buttonWidth, height: 40))
        button.setTitle("已过期", for: .normal)
        button.setTitleColor(UIColor.white, for: .selected)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.addTarget(self, action: #selector(useStatusClick(button:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "代金券"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", style: .plain, target: self, action: #selector(backCtrl))
        
        let button = UIButton(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        button.setTitle("筛选", for: .normal)
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(rightBarButtonItemAction(button:)), for: .touchUpInside)
        button.isSelected = false
        button.theme_setTitleColor("Global.barTextColor", forState: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
        //defulat today
        filterStartTime = getTimeWithDate(date: Date(), dateFormat: "yyyy-MM-dd")
        filterEndTime = getTimeWithDate(date: Date(), dateFormat: "yyyy-MM-dd")
        view.addSubview(tipsLabel)
        view.addSubview(headView)
        
        headView.addSubview(noUserButton)
        headView.addSubview(alreadlyButton)
        headView.addSubview(expiredButton)
       
        view.addSubview(couponTableView)
        tipsLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(KNavHeight)
            make.height.equalTo(40)
        }
        
        headView.snp.makeConstraints { (make) in
            make.top.equalTo(tipsLabel.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(40)
        }
        
        couponTableView.snp.makeConstraints { (make) in
            make.top.equalTo(headView.snp.bottom)
            make.left.right.bottom.equalTo(view)
        }
        //获取代金券数据
        couponDataFromServer()
       
        
    }
}
extension MemberCouponController{
    //MARK:代金券数据
    func couponDataFromServer(){
        var paramters:[String : AnyObject] = [String : AnyObject]()
        paramters["startTime"] = filterStartTime as AnyObject
        paramters["endTime"] = filterEndTime as AnyObject
        request(frontDialog: true, method: .post, loadTextStr: "正在加载", url: "/native/couponUseData.do", params: paramters) {[weak self] (response, isSuccess) in
            guard let weakSelf = self else{return}
            if isSuccess{
                guard let couponItem = MemberCouponItem.deserialize(from: response) else{return}
                if couponItem.success{
                    weakSelf.coupons.removeAll()
                    weakSelf.alreadlyUseCoupons.removeAll()
                    weakSelf.expiredCoupons.removeAll()
                    for item in (couponItem.accessToken?.rows ?? []){
                        if item.status == "3"{
                            //过期
                            weakSelf.expiredCoupons.append(item)
                        }else if item.status == "2"{
                            weakSelf.alreadlyUseCoupons.append(item)
                        }else if item.status == "1"{
                            weakSelf.coupons.append(item)
                        }
                    }
                    
                    if weakSelf.firstEnter{
                         weakSelf.useStatusClick(button:weakSelf.noUserButton)
                        weakSelf.firstEnter = false
                    }
                    weakSelf.couponTableView.reloadData()
//
                }else{
                    showToast(view: weakSelf.view, txt: couponItem.msg)
                }
            }
        }
    }
    //去使用
    func goWorkCoupon(couponItem:CouponRowsItem?){
        //native/couponRecharge.do
        var paramters:[String:AnyObject] = [String:AnyObject]()
        paramters["cid"] =  couponItem?.cid as AnyObject
        paramters["id"] = couponItem?.id as AnyObject
        request(frontDialog: true, method: .post, loadTextStr: "正在加载中...", url: "/native/couponRecharge.do", params: paramters) { (response, isSuccess) in
            if isSuccess{
                guard let couponItem = MemberCouponItem.deserialize(from: response) else{return}
                if couponItem.success{
                    
                }else{
                    showToast(view: self.view, txt: couponItem.msg)
                }
            }else{
                
            }
        }
    }
    //删除当时代金券
    func deleteCoupon(couponItem:CouponRowsItem?){
        var paramters:[String:AnyObject] = [String:AnyObject]()
        paramters["cid"] =  couponItem?.cid as AnyObject
        paramters["id"] = couponItem?.id as AnyObject
        request(frontDialog: true, method: .post, loadTextStr: "正在加载中...", url: "/native/deleteCs.do", params: paramters) {[weak self] (response, isSuccess) in
            //
            guard let weakSelf = self else {return}
            if isSuccess{
                guard let couponItem = MemberCouponItem.deserialize(from: response) else{return}
                if couponItem.success{
                    weakSelf.coupons.removeAll()
                    weakSelf.alreadlyUseCoupons.removeAll()
                    weakSelf.expiredCoupons.removeAll()
                    weakSelf.couponDataFromServer()
                }else{
                    showToast(view: weakSelf.view, txt: couponItem.msg)
                }
            }
        }
    }
    
    
}
//MARK: UITableViewDelegate && UITableViewDataSource
extension MemberCouponController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        if type == .noUse{
            
            tableView.tableViewDisplayWithMessage(datasCount: coupons.count, tableView: tableView)
            return coupons.count
        }else if type == .alreadlyUse{
            
            tableView.tableViewDisplayWithMessage(datasCount: alreadlyUseCoupons.count, tableView: tableView)
            return alreadlyUseCoupons.count
        }else if type == .expired{
            
            tableView.tableViewDisplayWithMessage(datasCount: expiredCoupons.count, tableView: tableView)
            return expiredCoupons.count
        }
        tableView.tableViewDisplayWithMessage(datasCount: coupons.count, tableView: tableView)
        return coupons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let couponCell = tableView.dequeueReusableCell(withIdentifier: "MemberCouponCell") as? MemberCouponCell
        var couponItem:CouponRowsItem?
        if type == .noUse {
            couponItem = coupons[indexPath.row]
        }else if type == .alreadlyUse{
            couponItem = alreadlyUseCoupons[indexPath.row]
        }else if type == .expired{
            couponItem = expiredCoupons[indexPath.row]
        }
        couponCell?.couponItem = couponItem
        couponCell?.couponCellDelegate = self
        return couponCell ??  UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}
//MARK:响应事件
extension MemberCouponController{
    //返回上一个界面
    @objc func backCtrl(){
        self.navigationController?.popViewController(animated: true)
    }
    //筛选
    //MARK:筛选
    @objc func rightBarButtonItemAction(button:UIButton){
        
        if button.isSelected == false {
            if self.view.viewWithTag(101) != nil { return }
            let filterHeight: CGFloat = 120
            let filterView = ReportViewFilter(height: filterHeight,controller:self)
            filterView.initializeDate(start: self.filterStartTime, end: self.filterEndTime)
            
            filterView.didClickCancleButton = {
                self.rightBarButtonItemAction(button: button)
            }
            filterView.didClickConfirmButton = {[weak self](lotCode:String,startTime:String,endTime:String)->Void in
                guard let weakSelf = self else{return}
                weakSelf.rightBarButtonItemAction(button: button)
                var startDate = startTime
                var endDate = endTime
                //                if startDate.length > 10{
                //                   startDate = startDate.subString(start: 0, length: 10)
                //                }
                //
                //                if endDate.length > 10{
                //                    endDate = endDate.subString(start: 0, length: 10)
                //                }
                weakSelf.filterStartTime = startDate
                weakSelf.filterEndTime = endDate
                weakSelf.couponDataFromServer()
            }
            filterView.tag = 101
            self.view.addSubview(filterView)
            let filerviewHeight: CGFloat = 120
            filterView.whc_Top(CGFloat(KNavHeight)-filterHeight).whc_Left(0).whc_Right(0).whc_Height(filerviewHeight)
            //            self.view.layoutIfNeeded()
            self.couponTableView.contentInset = UIEdgeInsets(top: 120 - 80, left: 0, bottom: 0, right: 0)
            UIView.animate(withDuration: 0.5, animations: {
                filterView.whc_Left(0).whc_Top(CGFloat(KNavHeight)).whc_Right(0).whc_Height(filerviewHeight)
            }) { ( _) in
                button.isSelected = true
            }
        }else {
            self.couponTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            let filerviewHeight: CGFloat = 120
            button.isSelected = false
            let filterView = self.view.viewWithTag(101)
            UIView.animate(withDuration: 0.5, animations: {
                filterView?.alpha = 0
                filterView?.whc_Left(0).whc_Top(0-filerviewHeight).whc_Right(0).whc_Height(filerviewHeight)
                
            }) { ( _) in
                filterView?.removeFromSuperview()
                
                
            }
        }
    }
    //使用状态的按钮事件响应
    @objc func useStatusClick(button:UIButton){
        if button.isSelected{
            button.setTitleColor(UIColor.white, for: .selected)
        }else{
            button.setTitleColor(UIColor.black, for: .normal)
        }
        if button.currentTitle == "未使用" {
            //
            noUserButton.backgroundColor = UIColor.colorWithRGB(r: 82, g: 82, b: 82, alpha: 1.0)
            alreadlyButton.backgroundColor = UIColor.white
            expiredButton.backgroundColor = UIColor.white
            noUserButton.isSelected = true
            alreadlyButton.isSelected = false
            expiredButton.isSelected = false
            type = .noUse
            couponTableView.reloadData()
        }else if button.currentTitle == "已使用"{
            //
            noUserButton.backgroundColor = UIColor.white
            alreadlyButton.backgroundColor = UIColor.colorWithRGB(r: 82, g: 82, b: 82, alpha: 1.0)
            expiredButton.backgroundColor = UIColor.white
            alreadlyButton.isSelected = true
            noUserButton.isSelected = false
            expiredButton.isSelected = false
            type = .alreadlyUse
            couponTableView.reloadData()
        }else if button.currentTitle == "已过期"{
            //
            noUserButton.backgroundColor = UIColor.white
            alreadlyButton.backgroundColor = UIColor.white
            expiredButton.backgroundColor = UIColor.colorWithRGB(r: 82, g: 82, b: 82, alpha: 1.0)
            expiredButton.isSelected = true
            alreadlyButton.isSelected = false
            noUserButton.isSelected = false
            type = .expired
            couponTableView.reloadData()
        }
        couponTableView.reloadData()
        
    }
}
//MARK:MemberCouponCellDelegate
extension MemberCouponController:MemberCouponCellDelegate{
    func goUseClick(couponItem:CouponRowsItem?) {
        
        let alertCtrl = UIAlertController(title:"", message: "确定使用当前代金券", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style:.default, handler: nil)
        let sureAction = UIAlertAction(title: "确定", style: .default) { (action) in
            //去使用当前代金券
            self.goWorkCoupon(couponItem: couponItem)
        }
        alertCtrl.addAction(cancelAction)
        alertCtrl.addAction(sureAction)
        self.present(alertCtrl, animated: true, completion: nil)
       
    }
    
    func deleteCurrentCoupon(couponItem:CouponRowsItem?) {
       
        
        let alertCtrl = UIAlertController(title:"", message: "确定删除当前代金券", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style:.default, handler: nil)
        let sureAction = UIAlertAction(title: "确定", style: .default) { (action) in
            //去使用当前代金券
            self.deleteCoupon(couponItem: couponItem)
            //删除当前代金券
        }
        alertCtrl.addAction(cancelAction)
        alertCtrl.addAction(sureAction)
        self.present(alertCtrl, animated: true, completion: nil)
    }

}
