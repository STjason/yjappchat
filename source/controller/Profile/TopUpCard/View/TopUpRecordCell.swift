//
//  TopUpRecordCell.swift
//  gameplay
//
//  Created by Gallen on 2020/1/13.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit

class TopUpRecordCell: UITableViewCell {
    
    @IBOutlet weak var sendDateLabel: UILabel!
    @IBOutlet weak var useDateLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBOutlet weak var topUpStatusLabel: UILabel!
    var recordRowsItem:RecordRowsItem?{
        didSet{
            if let recordItem = recordRowsItem{
                sendDateLabel.text = String().getStringForTimeStamp(time: recordItem.createDatetime)
                useDateLabel.text = String().getStringForTimeStamp(time: recordItem.useDatetime)
                if recordItem.useDatetime == 0{
                    useDateLabel.text = ""
                }
                moneyLabel.text = recordItem.denomination + "元"
                cardNumLabel.text = recordItem.cardNo
                if recordItem.status == "2"{
                    topUpStatusLabel.text = "已充值"
                    topUpStatusLabel.textColor = UIColor.colorWithRGB(r: 102, g: 102, b: 102, alpha: 1.0)
                }else if recordItem.status == "1"{
                    topUpStatusLabel.text = "未使用"
                    topUpStatusLabel.textColor = UIColor.colorWithHexString("#228b22")
                }
            
            }
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    
}
