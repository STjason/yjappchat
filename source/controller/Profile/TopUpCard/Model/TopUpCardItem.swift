//
//  TopUpCardItem.swift
//  gameplay
//
//  Created by Gallen on 2020/1/13.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class TopUpCardItem: HandyJSON {
    //"{\"success\":true,\"accessToken\":\"4e561ae5-8d0a-4862-9a21-2378c3575d64\"}"
    var success:Bool = false
    var accessToken = ""
    var msg = ""
    required init(){}
}
