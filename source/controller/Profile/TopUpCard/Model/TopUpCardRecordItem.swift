//
//  TopUpCardRecordItem.swift
//  gameplay
//
//  Created by Gallen on 2020/1/13.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class TopUpCardRecordItem: HandyJSON {
    var success:Bool = false
    var accessToken:AccessTokenItem?
    required init(){}
}

class AccessTokenItem: HandyJSON {
    var currentPageNo = 0
    var hasNext = false
    var hasPre = false
    var nextPage = 0
    var pageSize = 0
    var prePage = 0
    var start = 0
    var total = 0
    var totalPageCount = 0
    var rows:[RecordRowsItem]?
    required init(){}
}


class RecordRowsItem: HandyJSON {
    /*{
    "betNum": 1,
    "cardNo": "79908422120858",
    "cardPassWord": "***",
    "createDatetime": 1578904931262,
    "createUserid": 0,
    "createUsername": "***",
    "denomination": 1000,
    "id": 18,
    "stationId": 2,
    "status": 2,
    "useDatetime": 1578905950861,
    "useUserid": 1395,
    "useUsername": "test901"
    }*/
 
    var betNum = ""
    var cardNo = ""
    var cardPassWord = ""
    var createDatetime:TimeInterval = 0
    var createUserid = ""
    var createUsername = ""
    var denomination = ""
    var id = ""
    var stationId = ""
    var status = ""
    var useDatetime:TimeInterval = 0
    var useUserid = ""
    var useUsername = ""
    required init(){}
}
