//
//  TopUpCardController.swift
//  gameplay
//
//  Created by Gallen on 2020/1/13.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import SnapKit
import HandyJSON

class TopUpCardController: BaseController {
    
    weak var showHidePasswordButton:UIButton?
    /**卡号*/
    var cardNum:String = ""
    /**卡密码*/
    var cardPassword:String = ""
    //充值卡
    lazy var indicateImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.image = UIImage(named: "topup_card_indicate")
        return imageV
    }()
    /**充值卡号码*/
    lazy var topUpCardNumTextField:UITextField = {
        let textField = UITextField()
        textField.placeholder = "请输入卡号"
        textField.layer.borderColor = UIColor.colorWithHexString("#c7c7c7").cgColor
        textField.layer.borderWidth = 0.5
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        textField.addTarget(self, action: #selector(numTextFieldChange(textField:)), for: UIControl.Event.editingChanged)
    
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        textField.leftView = leftView
        textField.leftViewMode =  .always
        return textField
    }()
    /**充值卡密码*/
    lazy var topUpCardPasswordTextField:UITextField = {
        let textField = UITextField()
        textField.placeholder = "请输入密码"
        textField.layer.borderColor = UIColor.colorWithHexString("#c7c7c7").cgColor
        textField.layer.borderWidth = 0.5
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        textField.isSecureTextEntry = true
        textField.addTarget(self, action: #selector(passwordTextFieldChange(textField:)), for: UIControl.Event.editingChanged)
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        textField.leftView = leftView
        textField.leftViewMode =  .always
        
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        textField.rightView = rightView
        textField.rightViewMode =  .always
        
        let hidePasswordButton = UIButton(frame:CGRect(x: 0, y: 0, width: 30, height: 40))
        hidePasswordButton.addTarget(self, action: #selector(showHidePassword(buttton:)), for: .touchUpInside)
        hidePasswordButton.setImage(UIImage(named: "topup_show_password"), for: .normal)
        hidePasswordButton.setImage(UIImage(named: "topup_hide_password"), for: .selected)
        hidePasswordButton.setTitle(" ", for: .normal)
        hidePasswordButton.setTitle(" ", for: .selected)
        rightView.addSubview(hidePasswordButton)
        showHidePasswordButton = hidePasswordButton
        
        return textField
    }()
    
    lazy var commitButton:UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.setTitle("确认", for: .normal)
        button.theme_backgroundColor = "Global.themeColor"
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(commitClick), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "充值卡"
        view.backgroundColor = UIColor.white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", style: .plain, target: self, action: #selector(backCtrl))
        setupIndicate()
    }
}
//MARK:UI
extension TopUpCardController{
    //处理UI
    func setupIndicate(){
        view.addSubview(indicateImageV)
        view.addSubview(topUpCardNumTextField)
        view.addSubview(topUpCardPasswordTextField)
        view.addSubview(commitButton)
        
        indicateImageV.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(0 + KNavHeight)
            make.centerX.equalTo(view.snp.centerX)
            make.size.equalTo(CGSize(width: 240, height: 240))
        }
        
        topUpCardNumTextField.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
            make.height.equalTo(45)
            make.top.equalTo(indicateImageV.snp.bottom).offset(15)
        }
        
        topUpCardPasswordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(topUpCardNumTextField.snp.bottom).offset(15)
            make.left.equalTo(topUpCardNumTextField.snp.left)
            make.height.equalTo(topUpCardNumTextField.snp.height)
            make.right.equalTo(topUpCardNumTextField.snp.right)
        }
        
        commitButton.snp.makeConstraints { (make) in
            make.top.equalTo(topUpCardPasswordTextField.snp.bottom).offset(20)
            make.left.equalTo(topUpCardNumTextField.snp.left)
            make.right.equalTo(topUpCardNumTextField.snp.right)
            make.height.equalTo(50)
        }
    }
}
//MARK:处理网络数据
extension TopUpCardController{
    //处理网络数据
    func setupNetData(){
        var parmters:[String : AnyObject] = [String : AnyObject]()
        if isEmptyString(str: cardNum) {
            showToast(view: view, txt: "卡号不能为空")
            return
        }
        if  isEmptyString(str: cardPassword) {
            showToast(view: view, txt: "密码不能为空")
            return
        }
        parmters["card"] = cardNum as AnyObject
        parmters["password"] = cardPassword as AnyObject
        request(frontDialog: true, method: .post, loadTextStr: "正在提交", url: "/native/rechargeCard.do", params: parmters) { (response, isSuccess) in
            //
            if isSuccess{
                guard let cardItem = TopUpCardItem.deserialize(from: response) else {return}
                if cardItem.success{
                    //成功的请求
                    showToast(view: self.view, txt: "充值成功")
                    self.navigationController?.popViewController(animated: true)
                }else{
                    showToast(view: self.view, txt: cardItem.msg)
                }
            }else{
                showToast(view: self.view, txt: "网络异常 请稍候再试")
            }
            
            
        }
    }
}
//MARK:事件响应
extension TopUpCardController{
    
    @objc private func backCtrl(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc private func commitClick(){
        //
        setupNetData()
    }
    //密码展示或隐藏
    @objc private func showHidePassword(buttton:UIButton){
        buttton.isSelected = !buttton.isSelected
        topUpCardPasswordTextField.isSecureTextEntry = !buttton.isSelected
    }
    //
    @objc private func numTextFieldChange(textField:UITextField){
        print("num = " + (textField.text ?? ""))
        cardNum = textField.text ?? ""
    }
    
    @objc private func passwordTextFieldChange(textField:UITextField){
        print("password = " + (textField.text ?? ""))
        cardPassword = textField.text ?? ""
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
