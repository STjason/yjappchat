//
//  TopUpCardRecordController.swift
//  gameplay
//
//  Created by Gallen on 2020/1/13.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit

class TopUpCardRecordController: BaseController {
    
    var filterStartTime = ""
    var filterEndTime = ""

    
    lazy var recordTableView:UITableView = {
        let tableView = UITableView.init(frame: view.frame)
        tableView.backgroundColor = UIColor.colorWithHexString("#f2f2f9")
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: "TopUpRecordCell", bundle: nil), forCellReuseIdentifier: "TopUpRecordCell")
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    lazy var records:[RecordRowsItem] = [RecordRowsItem]()

 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "充值卡记录详情"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", style: .plain, target: self, action: #selector(backCtrl))
        
        let button = UIButton(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        button.setTitle("筛选", for: .normal)
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(rightBarButtonItemAction(button:)), for: .touchUpInside)
        button.isSelected = false
        button.theme_setTitleColor("Global.barTextColor", forState: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
        //defulat today
        filterStartTime = getTimeWithDate(date: Date(), dateFormat: "yyyy-MM-dd")
        filterEndTime = getTimeWithDate(date: Date(), dateFormat: "yyyy-MM-dd")
        view.addSubview(recordTableView)

        
        recordTableView.snp.makeConstraints { (make) in
            make.top.equalTo(view)
            make.left.right.bottom.equalTo(view)
        }
        //筛选
        //请求当天的纪录
        getTopUpCardRecordDataForServer()
    }

}
extension TopUpCardRecordController{
    //请求充值卡的使用记录
    func getTopUpCardRecordDataForServer(){
        //native/rechargeCardData.do
        var paramters:[String:AnyObject] = [String:AnyObject]()
        paramters["startTime"] = filterStartTime as AnyObject
        paramters["endTime"] = filterEndTime as AnyObject
        request(frontDialog: true, method: .post, loadTextStr: "正在加载中...", url: "/native/rechargeCardData.do", params: paramters) { [weak self](response, isSuccess) in
            //
            guard let weakSelf = self else{return}
            if isSuccess{
                guard let recordItem = TopUpCardRecordItem.deserialize(from: response) else {return}
                if recordItem.success{
                    //成功
                    weakSelf.records.removeAll()
                    weakSelf.records = recordItem.accessToken?.rows ?? []
                    weakSelf.recordTableView.reloadData()
                }else{
                    
                }
            }
        }
    }
}
//MARK:UITableViewDelegate && UITableViewDataSource
extension TopUpCardRecordController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.tableViewDisplayWithMessage(datasCount:records.count, tableView: tableView)
        return records.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let recordCell = tableView.dequeueReusableCell(withIdentifier: "TopUpRecordCell") as? TopUpRecordCell
        let recordRowsItem = self.records[indexPath.row]
        recordCell?.recordRowsItem = recordRowsItem
        return recordCell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 27))
        //27
        view.backgroundColor = UIColor.colorWithRGB(r: 242, g: 242, b: 242, alpha: 1.0)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 27))
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "注:查询时间为发卡时间"
        view.addSubview(label)
     
        
        label.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.centerY.equalTo(view.snp.centerY)
        }
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 27
    }
}
//MARK:响应事件
extension TopUpCardRecordController{
    
    @objc func backCtrl(){
        self.navigationController?.popViewController(animated: true)
    }
  
    //MARK:筛选
    @objc func rightBarButtonItemAction(button:UIButton){
        
        if button.isSelected == false {
            if self.view.viewWithTag(101) != nil { return }
            let filterHeight: CGFloat = 120
            let filterView = ReportViewFilter(height: filterHeight,controller:self)
            filterView.initializeDate(start: self.filterStartTime, end: self.filterEndTime)
            
            filterView.didClickCancleButton = {
                self.rightBarButtonItemAction(button: button)
            }
            filterView.didClickConfirmButton = {(lotCode:String,startTime:String,endTime:String)->Void in
                self.rightBarButtonItemAction(button: button)
                var startDate = startTime
                var endDate = endTime
//                if startDate.length > 10{
//                   startDate = startDate.subString(start: 0, length: 10)
//                }
//
//                if endDate.length > 10{
//                    endDate = endDate.subString(start: 0, length: 10)
//                }
                self.filterStartTime = startDate
                self.filterEndTime = endDate
                self.getTopUpCardRecordDataForServer()
            }
            filterView.tag = 101
            self.view.addSubview(filterView)
            let filerviewHeight: CGFloat = 120
            filterView.whc_Top(CGFloat(KNavHeight)-filterHeight).whc_Left(0).whc_Right(0).whc_Height(filerviewHeight)
//            self.view.layoutIfNeeded()
            self.recordTableView.contentInset = UIEdgeInsets(top: 120, left: 0, bottom: 0, right: 0)
            UIView.animate(withDuration: 0.5, animations: {
                filterView.whc_Left(0).whc_Top(CGFloat(KNavHeight)).whc_Right(0).whc_Height(filerviewHeight)
            }) { ( _) in
                button.isSelected = true
            }
        }else {
            self.recordTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            let filerviewHeight: CGFloat = 120
            button.isSelected = false
            let filterView = self.view.viewWithTag(101)
            UIView.animate(withDuration: 0.5, animations: {
                filterView?.alpha = 0
                filterView?.whc_Left(0).whc_Top(0-filerviewHeight).whc_Right(0).whc_Height(filerviewHeight)

            }) { ( _) in
                filterView?.removeFromSuperview()
            }
        }
    }
    
}
