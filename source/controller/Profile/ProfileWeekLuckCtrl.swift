//
//  ProfileWeekLuckCtrl.swift
//  gameplay
//
//  Created by Gallen on 2019/12/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
enum prizeType {
    /**每日加奖*/
    case day
    /**周周转运*/
    case week
}
class ProfileWeekLuckCtrl: BaseController {

    @IBOutlet weak var dataTableView: UITableView!
    var titleLabelWidth = (kScreenWidth - 30)
    weak var headView:UIView?
    /**可得奖金*/
    weak var receviePrizeMoneyLabel:UILabel?
    /**当前等级*/
    weak var currentUserLevelLabel:UILabel?
    /**昨日投注*/
    weak var yesterdayLotteryBetLabel:UILabel?
    /**领取*/
    weak var reciveButton:UIButton?
    //标题
    weak var addPrizeMoneyLabel:UILabel?
    /**周周转运 可得转运金*/
    weak var getPrizeMoneyLabel:UILabel?

    var prizeTypes:prizeType = .week
    /**达到最低等级的级别*/
    var miniLevel:String = ""
    /**当前账户的级别*/
    var curLevelName:String = ""
    var records:[bonusRecordItem] = [bonusRecordItem]()
    var dayWeekStr = ""
    /**最低打码量*/
    var betNums:[Int] = []{
        didSet{
            if  betNums.count > 0{
                let count =  (CGFloat(1) / CGFloat((betNums.count + 1)))
                titleLabelWidth = (kScreenWidth - 30) * count
                weekLostTotalTitleLabel.frame = CGRect(x: 0, y: 0, width: titleLabelWidth, height: 40)
                for (index,num) in betNums.enumerated(){
                    if index == 0{
                        changeLucklyProportionLabel.frame =  CGRect(x: (weekLostTotalTitleLabel.frame.maxX), y: 0, width: titleLabelWidth, height: 40)
                        changeLucklyProportionLabel.text = "\(num)" + "+"
                    }else if index == 1{
                        fourThousandLabel.frame = CGRect(x: (changeLucklyProportionLabel.frame.maxX), y: 0, width: titleLabelWidth, height: 40)
                        fourThousandLabel.text = "\(num)" + "+"
                    }else if index == 2{
                        twentyThousandLabel.frame = CGRect(x: (fourThousandLabel.frame.maxX), y: 0, width: titleLabelWidth, height: 40)
                        twentyThousandLabel.text =  "\(num)" + "+"
                    }else if index == 3{
                        fourthTitleLabel.frame = CGRect(x: (twentyThousandLabel.frame.maxX), y: 0, width: titleLabelWidth, height: 40)
                        fourthTitleLabel.text =  "\(num)" + "+"
                    }
                }
            }
        }
    }
    
    lazy var contentTableView:UITableView = {
        let tableView = UITableView()
        tableView.register(ProfileWeekLucklyCell.self, forCellReuseIdentifier: "ProfileWeekLucklyCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0,left: 0, bottom: 0,right: 0);
        tableView.separatorColor = UIColor.lightGray
        return tableView
    }()
    var deficitDatas:[weekLucklyDeficitDataItem] = [weekLucklyDeficitDataItem](){
        didSet{
            contentTableView.reloadData()
        }
    }
    //每日加奖等级
    var levels:[[levelBounsItem]] = [[levelBounsItem]](){
        didSet{
            
            
            contentTableView.reloadData()
        }
    }
    lazy var weekLostTotalTitleLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var changeLucklyProportionLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: weekLostTotalTitleLabel.frame.maxX, y: 0, width: 0, height: 0))
        label.text = "49+"
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()

    //4000
    lazy var fourThousandLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: changeLucklyProportionLabel.frame.maxX, y: 0, width: 0, height: 0))
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.text = "4000+"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //2w+
    lazy var twentyThousandLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: fourThousandLabel.frame.maxX, y: 0, width: 0, height: 0))
        label.text = "20000+"
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var fourthTitleLabel:UILabel = {
        let label = UILabel.init(frame: CGRect(x: twentyThousandLabel.frame.maxX, y: 0, width: 0, height: 0))
        label.text = "20000+"
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var explanationLabel:UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textColor = UIColor.colorWithHexString("#fff0cb")
        return label
    }()
    
    var url = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //设置UI
        setupUI()
        if  prizeTypes == .week{
            //请求数据
            getDataFromServer(url: url)
        }else{
            getBonuspageDataFromServer(url:url)
        }
    }
}
extension ProfileWeekLuckCtrl{
    //UI设置
    func setupUI(){
        
        setupHeadView()
        
        setupFootView()
    }
    //设置表头
    //450-
    func setupHeadView(){
        let headView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 450))
        
        let headImageV = UIImageView()
        headImageV.isUserInteractionEnabled = true
        headImageV.image = UIImage(named: "profile_week_luckly")
        
        headView.addSubview(headImageV)
        headImageV.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(headView)
            make.top.equalTo(headView).offset(40)
        }
        
        let detailTitleLabel = UILabel()
        detailTitleLabel.text = "加奖比例"
        detailTitleLabel.textColor = UIColor.colorWithRGB(r: 238, g: 114, b: 47, alpha: 1.0)
        detailTitleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        headImageV.addSubview(detailTitleLabel)
        
        detailTitleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(headImageV.snp.centerX)
            make.top.equalTo(headImageV.snp.top).offset(35)
        }
        //累计加奖标题
        let addPrizeLabel = UILabel()
        addPrizeLabel.text = "已累计加奖0次，总领取0元"
        addPrizeLabel.font = UIFont.systemFont(ofSize: 15)
        addPrizeLabel.textColor = UIColor.colorWithHexString("#775c00")
        headImageV.addSubview(addPrizeLabel)
        addPrizeMoneyLabel = addPrizeLabel
        
        addPrizeLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(headImageV.snp.centerX)
            make.top.equalTo(detailTitleLabel.snp.bottom).offset(15)
        }
        
        let headTitleView = UIView()
        headTitleView.backgroundColor = UIColor.colorWithRGB(r: 243, g: 246, b: 254, alpha: 1.0)
        headImageV.addSubview(headTitleView)
        
        headTitleView.snp.makeConstraints { (make) in
            make.top.equalTo(addPrizeLabel.snp.bottom).offset(3)
            make.height.equalTo(40)
            make.left.equalTo(15)
            make.right.equalTo(-15)
        }
        
        var labelWidth = (kScreenWidth - 30) / 3
        if prizeTypes == .day {
            labelWidth = titleLabelWidth
        }
        
        weekLostTotalTitleLabel.frame = CGRect(x: 0, y: 0, width: labelWidth, height: 40)
        weekLostTotalTitleLabel.text = prizeTypes == .day ? "等级/投注额" : "周累计亏损金额"
        headTitleView.addSubview(weekLostTotalTitleLabel)
        let detetailLabelWidth = prizeTypes == .week ? labelWidth : titleLabelWidth
        
        changeLucklyProportionLabel.frame = CGRect(x: weekLostTotalTitleLabel.frame.maxX, y: 0, width: detetailLabelWidth, height: 40)
        changeLucklyProportionLabel.text = "转运比例"
        headTitleView.addSubview(changeLucklyProportionLabel)
        
        if prizeTypes == .day {
            fourThousandLabel.frame = CGRect(x: changeLucklyProportionLabel.frame.maxX, y: 0, width: detetailLabelWidth, height: 40)
            headTitleView.addSubview(fourThousandLabel)
            
            twentyThousandLabel.frame = CGRect(x: fourThousandLabel.frame.maxX, y: 0, width: detetailLabelWidth, height: 40)
            headTitleView.addSubview(twentyThousandLabel)
            
            fourthTitleLabel.frame = CGRect(x: twentyThousandLabel.frame.maxX, y: 0, width: detetailLabelWidth, height: 40)
            headTitleView.addSubview(fourthTitleLabel)
        }
       
        
        if prizeTypes == .week {
            let recevieMoneyLabel = UILabel(frame: CGRect(x: changeLucklyProportionLabel.frame.maxX, y: 0, width: labelWidth, height: 40))
            recevieMoneyLabel.text = "可得转运金"
            recevieMoneyLabel.textColor = UIColor.colorWithHexString("#666")
            recevieMoneyLabel.textAlignment = .center
            recevieMoneyLabel.font = UIFont.systemFont(ofSize: 14)
            headTitleView.addSubview(recevieMoneyLabel)
        }
        //周周转运   //取领/不可领取
        let unableReciveButton = UIButton()
        unableReciveButton.layer.cornerRadius = 3
        unableReciveButton.layer.masksToBounds = true
        unableReciveButton.backgroundColor = UIColor.colorWithRGB(r: 153, g: 153, b: 153, alpha: 1.0)
        unableReciveButton.setTitleColor(UIColor.white, for: .normal)
        unableReciveButton.addTarget(self, action: #selector(receviceClick), for: .touchUpInside)
        unableReciveButton.setTitle("不可领取", for: .normal)
        headImageV.addSubview(unableReciveButton)
        reciveButton = unableReciveButton
        
        //领取记录
        let receiveRecordButton = UIButton()
        unableReciveButton.isUserInteractionEnabled = false
        receiveRecordButton.layer.cornerRadius = 3
        receiveRecordButton.layer.masksToBounds = true
        receiveRecordButton.addTarget(self, action: #selector(checkReceiveHistory), for: .touchUpInside)
        receiveRecordButton.setTitle("领取记录", for: .normal)
        receiveRecordButton.backgroundColor = UIColor.colorWithRGB(r: 178, g: 93, b: 6, alpha: 1.0)
        receiveRecordButton.setTitleColor(UIColor.white, for: .normal)
        headImageV.addSubview(receiveRecordButton)
        
        receiveRecordButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(headImageV.snp.bottom).offset(-10)
            make.right.equalTo(headImageV.snp.right).offset(-15)
            make.height.equalTo(32)
            make.width.equalTo(160)
        }
       
        unableReciveButton.snp.makeConstraints { (make) in
            make.height.equalTo(32)
            make.width.equalTo(160)
            make.left.equalTo(headImageV).offset(15)
            make.bottom.equalTo(-10)
        }
        
        //每日加奖
        if prizeTypes == .day {
            //当前等级
            let currentLevelLabel = UILabel()
            currentLevelLabel.text = "当前等级: --"
            currentLevelLabel.textColor = UIColor.white
            headImageV.addSubview(currentLevelLabel)
            currentUserLevelLabel = currentLevelLabel
            
            /// 可得奖金
            let recivePrizeLabel = UILabel()
            recivePrizeLabel.text = "可得奖金: --"
            recivePrizeLabel.textColor = UIColor.white
            headImageV.addSubview(recivePrizeLabel)
            receviePrizeMoneyLabel = recivePrizeLabel
            
            
            recivePrizeLabel.snp.makeConstraints { (make) in
                make.bottom.equalTo(receiveRecordButton.snp.top).offset(-5)
                make.left.equalTo(receiveRecordButton.snp.left)
            }
            currentLevelLabel.snp.makeConstraints { (make) in
                make.left.equalTo(receiveRecordButton.snp.left)
                make.bottom.equalTo(recivePrizeLabel.snp.top).offset(-3)
            }
        }
        //昨日投注  周周转运 上周累计亏损
        let yesterdayBetLabel = UILabel()
        yesterdayBetLabel.text = prizeTypes == .day ? "昨日投注: --" : "上周累计亏损: --"
        yesterdayBetLabel.textColor = UIColor.white
        headImageV.addSubview(yesterdayBetLabel)
        yesterdayLotteryBetLabel = yesterdayBetLabel
        
        //加奖比例  周周转运 可得转运金
        let addPrizeProportionLabel = UILabel()
        addPrizeProportionLabel.text = prizeTypes == .day ? "加奖比例: --" : "可得转运金: --"
        addPrizeProportionLabel.textColor = UIColor.white
        headImageV.addSubview(addPrizeProportionLabel)
        getPrizeMoneyLabel = addPrizeProportionLabel
        
        addPrizeProportionLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(unableReciveButton.snp.top).offset(-5)
            make.left.equalTo(headImageV).offset(15)
        }
        
        yesterdayBetLabel.snp.makeConstraints { (make) in
            make.left.equalTo(headImageV).offset(15)
            make.bottom.equalTo(addPrizeProportionLabel.snp.top).offset(-3)
        }
    
        headImageV.addSubview(contentTableView)
        
        contentTableView.snp.makeConstraints { (make) in
            make.left.equalTo(headImageV.snp.left).offset(15)
            make.top.equalTo(headTitleView.snp.bottom)
            make.right.equalTo(headImageV.snp.right).offset(-15)
            make.bottom.equalTo(yesterdayBetLabel.snp.top).offset(-10)
        }
        
        self.headView = headView
        
        dataTableView.tableHeaderView = headView
    }
    
    func setupFootView(){
        //表尾
        let footView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 400))
        
        let footTitleLabel = UILabel()
        footTitleLabel.textColor = UIColor.white
        footTitleLabel.text = "活动说明"
        footTitleLabel.font = UIFont.boldSystemFont(ofSize: 19)
        footView.addSubview(footTitleLabel)
        
        //段落样式
        let paraStyle = NSMutableParagraphStyle.init()
        //行间距
        paraStyle.lineSpacing = 5.0;
        //使用
        //文本段落样式
        let attributes = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15),
                          NSAttributedString.Key.paragraphStyle: paraStyle]
    
        dayWeekStr = " 1、活动奖在每周一凌晨00:30后开放领取。\n 2、转运固定金额模式:如上周亏损金额达到5000可领取88元(具体数据看活动规则)。\n 3、转运百分比模式:如上周亏损金额达到5000元,赠送比例为1%,则可领取50元(具体数据看活动规则)。\n 4、活动奖金可领取截止时间为每周日23:59:59时内,未领取视为自动放弃活动资格。\n 5、计算方式:上周总中奖-上周总投注+上周活动+上周总返佣=上周亏损金额。\n 6、本活动长期有效。"
        if prizeTypes == .day {
            dayWeekStr = " 1、每日加奖在次日凌晨00:30后开放领取。\n 2、加奖比例是根据会员等级以及昨日累计投注金额进行一定比例的加奖。\n 3、需" + "\(miniLevel)" + "及以上且昨日投注额大于或等于才能获得加奖。\n 4、彩票撤单和其他无效投注将不计算在内。\n 5、活动奖金领取截止时间为每天23:59:59,时间内未领取，视为自动放弃活动资格。\n 6、本活动长期有效。"
        }
        footView.addSubview(explanationLabel)
        explanationLabel.attributedText = NSAttributedString(string: dayWeekStr, attributes: attributes)
        
        
        footTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.centerX.equalTo(footView.snp.centerX)
        }
        
        explanationLabel.snp.makeConstraints { (make) in
            make.top.equalTo(footTitleLabel.snp.bottom).offset(10)
            make.left.equalTo(footView).offset(5)
            make.right.equalTo(footView).offset(-10)
        }
        
        dataTableView.tableFooterView = footView
    }
}

extension ProfileWeekLuckCtrl{
    //周周转运数据
    func getDataFromServer(url:String){
        request(frontDialog: true, method: .post, loadTextStr: "正在加载中..", url: url, params: [:]) {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else{return}
            if resultStatus == true {
                guard let weekLucklyItem = ProfileWeekLuckDataItem.deserialize(from: resultJson) else{
                    return
                }
                weakSelf.currentUserLevelLabel?.text = "当前等级:" + (weekLucklyItem.content?.curLevelName ?? "--")
                weakSelf.yesterdayLotteryBetLabel?.text = "上周累计亏损:" +  (weekLucklyItem.content?.recordData?.record?.deficitMoney ?? "--")
                weakSelf.getPrizeMoneyLabel?.text = "可得转运金:" +  (weekLucklyItem.content?.recordData?.record?.balance ?? "--")
                if let weekRecordItem = weekLucklyItem.content?.recordData?.record{
                    //记录详情
                    weakSelf.records.append(weekRecordItem)
                }
                
                //可领取的状态
                if weekLucklyItem.content?.recordData?.record?.status == "4"{
                    weakSelf.reciveButton?.isUserInteractionEnabled = true
                    weakSelf.reciveButton?.backgroundColor = UIColor.colorWithHexString("#ffcb4e")
                    weakSelf.reciveButton?.setTitle("领取", for: .normal)
                }else if weekLucklyItem.content?.recordData?.record?.status == "1"{
                    //已经领取
                    weakSelf.reciveButton?.isUserInteractionEnabled = false
                    weakSelf.reciveButton?.backgroundColor = UIColor.colorWithHexString("#999")
                    weakSelf.reciveButton?.setTitle("已领取", for: .normal)
                }
                //领取了多少次
                let totalCount = weekLucklyItem.content?.totalData?.deficitTimes ?? 0
                //总共领取了多少
                let totalBonusAmount = weekLucklyItem.content?.totalData?.deficitAmount ?? "--"
                //#ff795f
            
                let detailTitle = "已累计加奖" + "\(totalCount)" + "次,总领取" + "\(totalBonusAmount)" + "元"
                let NSDetailString = NSString(string: detailTitle)
                let countRange = NSDetailString.range(of: "\(totalCount)")
                let bonusRange = NSDetailString.range(of: "\(totalBonusAmount)")
                //富文本
                let attributed = NSMutableAttributedString(string: detailTitle)
                attributed.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.colorWithHexString("#ff795f")] , range: countRange)
                attributed.addAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)] , range: countRange)
                attributed.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.colorWithHexString("#ff795f")] , range: bonusRange)
                attributed.addAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)] , range: bonusRange)
                weakSelf.addPrizeMoneyLabel?.attributedText = attributed
                //按亏损的比例领取
                if (weekLucklyItem.content?.deficitData?.count ?? 0) > 0{
                    weakSelf.deficitDatas = weekLucklyItem.content?.deficitData ?? []
                    weakSelf.contentTableView.reloadData()
                }
            }else {
                showErrorHUD(errStr: "请求超时,请检查网络!")
            }
        }
    }
    //每日加奖数据
    func getBonuspageDataFromServer(url:String){
        request(frontDialog: true, method: .post, loadTextStr: "正在加载中..", url: url, params: [:]) {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else{return}
            if resultStatus == true {
                
                guard let bounsDayItem = ProfileWeekLuckDataItem.deserialize(from: resultJson) else{return}
                //达到该等级的最低打码量可领取
                
                let paraStyle = NSMutableParagraphStyle.init()
                //行间距
                paraStyle.lineSpacing = 5.0;
                //使用
                //文本段落样式
                let attributes = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15),
                                  NSAttributedString.Key.paragraphStyle: paraStyle]
             
                let dict = getDictionaryFromJSONString(jsonString: resultJson)
                //scales 所有的值
                let contentDict = dict["content"] as? [String : Any] ?? [:]
                
                let scaleValues = contentDict["scale"] as? [String : Any] ?? [:]
                //最低打码量
                let betNums = contentDict["betNum"] as? [Int] ?? []
                weakSelf.betNums = betNums
                
                //按亏损的比例领取
                if (bounsDayItem.content?.levels?.count ?? 0) > 0{
                    
                    for item in bounsDayItem.content?.levels ?? []{
                        var contents:[levelBounsItem] = [levelBounsItem]()
                        for betNum in betNums{
                            
                            let scaleKey = item.name + "_" + "\(betNum)"
                            
                            if scaleValues.keys.contains(scaleKey){
                                let value =  String(format:"%@",scaleValues[scaleKey] as! CVarArg)
                                let contentItem = levelBounsItem()
                                contentItem.levelValue = value
                                contentItem.levelKey = betNum
                                contentItem.name = item.name
                                contents.append(contentItem)
                                continue
                                
                            }
                            
                        }
                        weakSelf.levels.append(contents)
                    }
                    
                    weakSelf.miniLevel =  weakSelf.levels[0][0].name
                    let minBetNum = weakSelf.levels[0][0].levelKey
                    weakSelf.dayWeekStr = " 1、每日加奖在次日凌晨00:30后开放领取。\n 2、加奖比例是根据会员等级以及昨日累计投注金额进行一定比例的加奖。\n 3、需" + "\(weakSelf.miniLevel)" + "及以上且昨日投注额大于或等于"  + "\(minBetNum)" + "才能获得加奖。\n 4、彩票撤单和其他无效投注将不计算在内。\n 5、活动奖金领取截止时间为每天23:59:59,时间内未领取，视为自动放弃活动资格。\n 6、本活动长期有效。"
                    
                    weakSelf.explanationLabel.attributedText = NSAttributedString(string: weakSelf.dayWeekStr, attributes: attributes)
                    weakSelf.contentTableView.reloadData()
                }
              
//               weakSelf.changeLucklyProportionLabel.text =   bounsDayItem.content?.curLevelName
                weakSelf.receviePrizeMoneyLabel?.text = "可得奖金:"  + (bounsDayItem.content?.recordData?.record?.balance  ?? "--")
                weakSelf.currentUserLevelLabel?.text = "当前等级:" + (bounsDayItem.content?.curLevelName ?? "--")
                weakSelf.yesterdayLotteryBetLabel?.text = "昨日投注:" +  (bounsDayItem.content?.recordData?.record?.betNum ?? "--")
                if let addPrize = bounsDayItem.content?.recordData?.bonus?.numScale{
                    weakSelf.getPrizeMoneyLabel?.text =  String.init(format:"加奖比例:%@%%",addPrize)
                }else{
                    weakSelf.getPrizeMoneyLabel?.text =  String.init(format:"加奖比例:%@",bounsDayItem.content?.recordData?.bonus?.numScale ?? "--")
                }
               
                //可领取的状态
                if bounsDayItem.content?.recordData?.record?.status == "4"{
                    weakSelf.reciveButton?.isUserInteractionEnabled = true
                    weakSelf.reciveButton?.backgroundColor = UIColor.colorWithHexString("#ffcb4e")
                    weakSelf.reciveButton?.setTitle("领取", for: .normal)
                }else if bounsDayItem.content?.recordData?.record?.status == "1"{
                    //已经领取
                    weakSelf.reciveButton?.isUserInteractionEnabled = false
                    weakSelf.reciveButton?.backgroundColor = UIColor.colorWithHexString("#999")
                    weakSelf.reciveButton?.setTitle("已领取", for: .normal)
                }
                //领取了多少次
                let totalCount = bounsDayItem.content?.totalData?.bonusTimes ?? 0
                //总共领取了多少
                let totalBonusAmount = bounsDayItem.content?.totalData?.bonusAmount ?? "--"
                //#ff795f
                
                let detailTitle = "已累计加奖" + "\(totalCount)" + "次,总领取" + "\(totalBonusAmount)" + "元"
                let NSDetailString = NSString(string: detailTitle)
                let countRange = NSDetailString.range(of: "\(totalCount)")
                let bonusRange = NSDetailString.range(of: "\(totalBonusAmount)")
                //富文本
                let attributed = NSMutableAttributedString(string: detailTitle)
                attributed.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.colorWithHexString("#ff795f")] , range: countRange)
                attributed.addAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)] , range: countRange)
                attributed.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.colorWithHexString("#ff795f")] , range: bonusRange)
                attributed.addAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)] , range: bonusRange)
                weakSelf.addPrizeMoneyLabel?.attributedText = attributed
            }else {
                showErrorHUD(errStr: "请求超时,请检查网络!")
            }
        }
    }
}
//MARK:UITableViewDelegate && UITableViewDataSource
extension ProfileWeekLuckCtrl:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if prizeTypes == .week{
            return  deficitDatas.count
        }else{

            return levels.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "ProfileWeekLucklyCell") as? ProfileWeekLucklyCell
        cell?.status = prizeTypes
        if prizeTypes == .week {
            let contentItem =  self.deficitDatas[indexPath.row]
            cell?.contentItem = contentItem
        }else{
            cell?.betNums = self.betNums
            let bonusItem = self.levels[indexPath.row]
            cell?.bonudsLevelItem = bonusItem
        }
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
//MARK:事件响应
extension ProfileWeekLuckCtrl{
    //查看领取纪录
    @objc private func checkReceiveHistory(){
        //每日加奖领取记录 周周转运
        let recordUrl = prizeTypes == .day ? "/native/mrjjRecord.do":"/native/zzzyRecord.do"
//
        request(frontDialog: true, url: recordUrl) { [weak self] (resultJson, resultStatus) in
            //
            guard let weakSelf = self else{return}
            if resultStatus == true {
                guard let prizeItem = recivcePrizeItem.deserialize(from: resultJson) else{return}
                weakSelf.records = prizeItem.content ?? []
                if weakSelf.records.count == 0{
                    showToast(view: weakSelf.view, txt: "暂无领取记录")
                    return
                }
                let recordView = ProfileWeekBonusRecordView.init(frame: UIScreen.main.bounds, records: weakSelf.records, prizeKind: weakSelf.prizeTypes)
                if !recordView.isShow{
                    UIApplication.shared.keyWindow?.addSubview(recordView)
                    recordView.isShow = true
                }
            }else {
                showErrorHUD(errStr: "请求超时,请检查网络!")
            }
        }
       
    }
    //领取转运金（周周转运 每日加奖）
    @objc private func receviceClick(){
        //NATIVE_RECEIVE_DEFICIT 领取周周转运
        var url = NATIVE_RECEIVE_DEFICIT
        if prizeTypes == .day {
            
           url = NATIVE_RECEIVE_BONUS
        }
        request(frontDialog: true, method: .post, loadTextStr: "领取转运金...", url: url, params: [:]) {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else{return}
            if resultStatus == true {
                let recivceItem = recivcePrizeItem.deserialize(from: resultJson) ??  recivcePrizeItem()
                if recivceItem.success{
                    showToast(view: weakSelf.view, txt: "领取成功")
                    weakSelf.reciveButton?.isUserInteractionEnabled = false
                    weakSelf.reciveButton?.backgroundColor = UIColor.colorWithRGB(r: 153, g: 153, b: 153, alpha: 1.0)
                    //刷新界面UI 重新请求一次
                    weakSelf.prizeTypes == .day ? weakSelf.getBonuspageDataFromServer(url: BONUS_PAGE_DATA) : weakSelf.getDataFromServer(url: NATIVEDEFICIT_PAGE_DATA)
                    
                }else{
                    showToast(view: weakSelf.view, txt: recivceItem.msg)
                }
                
            }else {
                showErrorHUD(errStr: "请求超时,请检查网络!")
            }
        }
        
    }
}
