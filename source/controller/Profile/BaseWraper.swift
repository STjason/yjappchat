//
//  BaseWraper.swift
//  Alamofire
//
//  Created by admin on 2020/2/26.
//

import UIKit
import HandyJSON

class BaseWraper: HandyJSON {
    var success = false
    var msg = ""
    var accessToken = ""
    required init() {}
}
