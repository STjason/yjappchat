//
//  ProfileWeekLucklyCell.swift
//  gameplay
//
//  Created by Gallen on 2019/12/25.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class ProfileWeekLucklyCell: UITableViewCell {
    
    var recordLevel = ""
    
    var cellWidth = (kScreenWidth - 30) * 0.5
    
    var status:prizeType = .week{
        didSet{
            if status == .day {
                reciveGoodLucklyMoneyLabel.isHidden = true
                changeGoodLucklyProportionLabel.frame =  CGRect(x: loseMoneyLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                reciveGoodLucklyMoneyLabel.frame =  CGRect(x: changeGoodLucklyProportionLabel.frame.maxX, y: 0, width: cellWidth * 0, height: 40)
//                 createLine(lineCount: 1)
            }else{
                //周周转运
                cellWidth = (kScreenWidth - 30) / 3
                reciveGoodLucklyMoneyLabel.isHidden = false
                loseMoneyLabel.frame =  CGRect(x: 0, y: 0, width: cellWidth, height: 40)
                changeGoodLucklyProportionLabel.frame =  CGRect(x: loseMoneyLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                reciveGoodLucklyMoneyLabel.frame =  CGRect(x: changeGoodLucklyProportionLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                createLine(lineCount: 2)
            }
        }
    }
    //周累计亏损金额
    lazy var loseMoneyLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: cellWidth, height: 40))
        label.textAlignment = .center
        label.textColor = UIColor.colorWithHexString("#666")
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //转运比例 49
    lazy var changeGoodLucklyProportionLabel:UILabel = {
        
        let label = UILabel(frame: CGRect(x: loseMoneyLabel.frame.maxX, y: 0, width: cellWidth, height: 40))
        label.textAlignment = .center
        label.textColor = UIColor.colorWithHexString("#666")
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //可得转运金  周周转运
    lazy var reciveGoodLucklyMoneyLabel:UILabel = {
        
        let label = UILabel(frame: CGRect(x: changeGoodLucklyProportionLabel.frame.maxX, y: 0, width: 0 , height: 40))
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //4000+
    lazy var fourThousandLevelLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: changeGoodLucklyProportionLabel.frame.maxX, y: 0, width: 0, height: 40))
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //20000+
    lazy var twentyThousandLevelLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: fourThousandLevelLabel.frame.maxX, y: 0, width: 0, height: 40))
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var fourthLevelLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: twentyThousandLevelLabel.frame.maxX, y: 0, width: 0, height: 40))
        label.textColor = UIColor.colorWithHexString("#666")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    var contentLabels:[UILabel] = [UILabel]()
    
    var contentItem:weekLucklyDeficitDataItem?{
        didSet{
            if let luckItem = contentItem{
                //亏损的范围
                loseMoneyLabel.text = luckItem.minMoney + "~" +  luckItem.maxMoney
                //转运比例
                if status == .week{
                    //周周转运有金额 和赔率版
                    if luckItem.type ==  "2"{
                        //赔率比例
                        reciveGoodLucklyMoneyLabel.text = "--"
                        changeGoodLucklyProportionLabel.text = String(format:"%@%%",luckItem.money)
                    }else if luckItem.type == "1"{
                        //可得金额
                        reciveGoodLucklyMoneyLabel.text = luckItem.money
                        changeGoodLucklyProportionLabel.text = "--"
                    }
                }else{
                    
                    changeGoodLucklyProportionLabel.text = String(format:"%@%%",luckItem.multiple)
                    //可以领取
                    reciveGoodLucklyMoneyLabel.text = luckItem.money
                }
             
            }
        }
    }
    
    var bonudsLevelItem:[levelBounsItem]?{
        didSet{
            if let bonudsItems = bonudsLevelItem{
                //每天加奖名称
                loseMoneyLabel.text = bonudsItems[0].name
                for (index ,value) in bonudsItems.enumerated(){
                    //当前等级不是记录等级
                    for (level,levelValue) in self.betNums!.enumerated(){
                        //属于这个等级的一列
                        if value.levelKey == levelValue{
                            recordLevel = value.name
                            contentLabels[level].text  = String(format:"%@%%",bonudsItems[index].levelValue)
                            
                        }else{
                            //不是当前vip等级 对应的门槛
                            //
                            var flag = true
                            for (index1,item) in bonudsItems.enumerated(){
                                if item.levelKey == levelValue{
                                    flag = false
                                }
                            }
                            if flag{
                                 contentLabels[level].text  = "--"
                            }
                        }
                    }
                    
                }
            }
        }
    }
    //最低打码量
    var betNums:[Int]?{
        didSet{
            if let nums = betNums{
                //1 0.5 2 0.33.3 0.25
                let count =  (CGFloat(1) / CGFloat((nums.count + 1)))
                cellWidth = (kScreenWidth - 30) * count
                
                loseMoneyLabel.frame = CGRect(x: 0, y: 0, width: cellWidth, height: 40)
                changeGoodLucklyProportionLabel.frame = CGRect(x: loseMoneyLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                createLine(lineCount: nums.count)
                
                for (index,num) in nums.enumerated(){
                    if index == 0{
                        changeGoodLucklyProportionLabel.frame = CGRect(x: loseMoneyLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                    }else if index == 1{
                        fourThousandLevelLabel.frame = CGRect(x: changeGoodLucklyProportionLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                    }else if index == 2{
                        twentyThousandLevelLabel.frame = CGRect(x: fourThousandLevelLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                    }else if index == 3{
                        fourthLevelLabel.frame = CGRect(x: twentyThousandLevelLabel.frame.maxX, y: 0, width: cellWidth, height: 40)
                    }
                }
            }
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        addSubview(loseMoneyLabel)
        addSubview(changeGoodLucklyProportionLabel)
        addSubview(reciveGoodLucklyMoneyLabel)
        addSubview(fourThousandLevelLabel)
        addSubview(twentyThousandLevelLabel)
        addSubview(fourthLevelLabel)
        
       contentLabels =  [changeGoodLucklyProportionLabel,
         fourThousandLevelLabel,
         twentyThousandLevelLabel,
         fourthLevelLabel]
       
    }
    
    func createLine(lineCount:Int) {
        for index in 0..<lineCount {
            let line = UIView()
            line.frame = kCGRect(x: cellWidth + CGFloat(index) * cellWidth, y: 0, width: 0.5, height: 40)
            line.backgroundColor = UIColor.lightGray
            addSubview(line)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
