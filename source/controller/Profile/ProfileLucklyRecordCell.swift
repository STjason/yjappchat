//
//  ProfileLucklyRecordCell.swift
//  gameplay
//
//  Created by Gallen on 2019/12/31.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class ProfileLucklyRecordCell: UITableViewCell {
    
    var labelWidth = (kScreenWidth - 60) * 0.25
    let labelHeight = CGFloat(40)
    lazy var receiveDateLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: 2, y: 0, width: labelWidth, height: labelHeight))
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "领取时间"
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var betCountLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: receiveDateLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.text = "打码量"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    //加奖比例
    lazy var addPrizeProportionLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: betCountLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.text = "加奖比例"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    //加奖金额
    lazy var addPrizeMoneyLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: addPrizeProportionLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.text = "加奖金额"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    //状态
    lazy var statusLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: addPrizeMoneyLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight))
        label.text = "状态"
        label.textColor = UIColor.colorWithHexString("#33b23f")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    var prizeType:prizeType = .week{
        didSet{
            if prizeType == .week {
                labelWidth = (kScreenWidth - 60) * 0.25
            }else{
                labelWidth = (kScreenWidth - 60) * 0.2
                receiveDateLabel.frame = CGRect(x: 2, y: 0, width: labelWidth, height: labelHeight)
                betCountLabel.frame = CGRect(x: receiveDateLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight)
                addPrizeProportionLabel.frame = CGRect(x: betCountLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight)
                addPrizeMoneyLabel.frame = CGRect(x: addPrizeProportionLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight)
                statusLabel.frame = CGRect(x: addPrizeMoneyLabel.frame.maxX, y: 0, width: labelWidth, height: labelHeight)
            }
        }
    }
    
    //周周转运的记录
    var weekLucklyItem:bonusRecordItem?{
        didSet{
            if let recordItem = weekLucklyItem{
                if prizeType == .week{
                    receiveDateLabel.text = recordItem.createTime
                    betCountLabel.text = recordItem.deficitMoney
                    addPrizeProportionLabel.text = recordItem.balance
                    addPrizeMoneyLabel.text = recordItem.status == "1" ? "领取成功":"--"
                    addPrizeMoneyLabel.textColor = UIColor.colorWithHexString("#33b23f")
                }else{
                    receiveDateLabel.text = recordItem.statDate
                    betCountLabel.text = recordItem.betNum
                    addPrizeProportionLabel.text = String.init(format:"%@%%",recordItem.scale)
                    addPrizeMoneyLabel.textColor = UIColor.black
                    addPrizeMoneyLabel.text = recordItem.balance
                    statusLabel.text = recordItem.status == "1" ? "领取成功":"--"
                }
            }
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(receiveDateLabel)
        contentView.addSubview(betCountLabel)
        contentView.addSubview(addPrizeProportionLabel)
        contentView.addSubview(addPrizeMoneyLabel)
        contentView.addSubview(statusLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
