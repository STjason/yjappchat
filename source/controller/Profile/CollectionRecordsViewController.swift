//
//  CollectionRecordsViewController.swift
//  YiboGameIos
//
//  Created by JK on 2019/10/7.
//  Copyright © 2019 com.lvwenhan. All rights reserved.
//

import UIKit

class CollectionRecordsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /** 列表 */
    var _listView : UITableView?
    /** 接口 */
    var url : String?
    /** 列表数据源 */
    var listData = Array<CollectionRecordsModel>()
    /** 当前列表状态  */
    var status : ActivityStatus?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.requestRecordsData()
        view.addSubview(self.listView())
    }
    
    /// 请求领取记录数据
    func requestRecordsData() {
        kRequest(isContent: true, frontDialog: true, method: .get, loadTextStr: "加载领取记录..", url: url ?? "", params: [:]) { (content) in
            let data = content as? Array<Any> ?? Array<Any>()
            for obj in data {
                let model = CollectionRecordsModel().getCurrentJson(dic: obj as? NSDictionary ?? NSDictionary())
                self.listData.append(model)
            }
            if self.url == NATIVE_RECEIVE_BONUS {
                self.status = .DailyBonusStatus
            }else if self.url == NATIVE_RECEIVE_DEFICIT {
                self.status = .WeeklyTransportStatus
            }
            self.listView().reloadData()
        }
    }
    
    //#MARK: ------------------------- tab Delegate ---------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kCurrentScreen(x:170)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "collectionRecordsCell") as! CollectionRecordsCell
        
        cell.getRecordsModel(model: self.listData[indexPath.row], status: self.status ?? .DailyBonusStatus)
        cell.selectionStyle = .none
        
        return cell
    }

    //#MARK: ------------------------- 实例化 ---------------------------------
    func listView() -> UITableView {
        if _listView == nil {
            let listHeight = IS_IPHONE_X ? kScreenHeight - CGFloat(KNavHeight) - CGFloat(34) : kScreenHeight - CGFloat(KNavHeight)
            _listView = UITableView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: listHeight), style: .plain)
            _listView?.delegate = self
            _listView?.dataSource = self
            _listView?.tableFooterView = UIView()
            
            _listView?.register(CollectionRecordsCell.self, forCellReuseIdentifier: "collectionRecordsCell")
        }
        return _listView!
    }
}
