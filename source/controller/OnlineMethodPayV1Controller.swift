//
//  OnlineMethodPayV1Controller.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
//正在修改的页面
//在线支付信息填写页
//OnlinePayInfoController
class OnlineMethodPayV1Controller: BaseController,NewSelectViewDelegate {
    @IBOutlet weak var shouyingView: UIView!
    @IBOutlet weak var shouyintaiBgView: UIView!
    @IBOutlet weak var payMethodBgView: UIView!
    @IBOutlet weak var payMethodTV:UILabel!
    @IBOutlet weak var moneyInput:CustomFeildText!
    @IBOutlet weak var fastMoney:UICollectionView!
    @IBOutlet weak var shouyingtai:UICollectionView! //通道
    @IBOutlet weak var payChannelCollection: UICollectionView! //支付方式
    
    @IBOutlet weak var bankListCollectionView: UICollectionView!
    @IBOutlet weak var confirmBtn:UIButton!
    @IBOutlet weak var moneyLimitTV:UILabel!
    @IBOutlet weak var moneyCollectViewHeightConstrait:NSLayoutConstraint! //固定金额高度
    @IBOutlet weak var subPayCollectViewHeightConstrait:NSLayoutConstraint! //支付通道的高度
    
    @IBOutlet weak var payChannelHeightConstrait: NSLayoutConstraint! //支付方式的高度？
    
    @IBOutlet weak var tipsTextView: UILabel!
    
    @IBOutlet weak var bankListTitleLabel: UILabel!
    @IBOutlet weak var bankViewConstraint: NSLayoutConstraint!
    var isBank:Bool = false
    /** 输入的金额 */
    var inputMoney = ""
    
    @IBOutlet weak var topTableViewConstraint: NSLayoutConstraint!
    //备注
    var propmtString:String = ""
    var propmtLabel:UILabel?
    var indicateView:UIView?
    var introducePics = [String]()
    let tagFastMoney = 101
    let tagShouyingtai = 102
    let tagPayChannel = 103
    let bankChannel = 104
    
    var selectedIndex = 0
    var moneyDatas:[String] = []
    var shouyintais:[String] = []
    var bankLists:[String] = []
    var currentPayIndex = 0
    var onlines:[OnlineFunFirstPay] = []//支付通道列表
    var payMethodNames:[String] = []//支付方式列表
    var currSubPayRow = 0
    var currSubBankRow = 0
    var meminfo:Meminfo?
    var jsonData: [String: Any]?
    var currtagFastRow = 0
    //备注提示
    lazy var promptView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        indicateView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        indicateView!.layer.cornerRadius = 5
        indicateView!.layer.masksToBounds = true
        indicateView!.backgroundColor = UIColor.red
        //备注文字
        propmtLabel = UILabel()
        propmtLabel!.textColor = UIColor.white
        propmtLabel!.numberOfLines = 0
        propmtLabel!.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(propmtLabel!)
        view.addSubview(indicateView!)
        
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        moneyInput.text = ""
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setThemeLabelTextColorGlassWhiteOtherRed(label: tipsTextView)
        setThemeLabelTextColorGlassWhiteOtherRed(label: moneyLimitTV)
        
        depoistGuidePictures()
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        self.title = "在线支付"
        fastMoney.delegate = self
        fastMoney.dataSource = self
        fastMoney.tag = tagFastMoney
        fastMoney.register(PayInfoMoneyCell.self, forCellWithReuseIdentifier:"cell")
        fastMoney.showsVerticalScrollIndicator = false
        self.fastMoney.reloadData()
        payMethodTV.isUserInteractionEnabled = true
        
        payChannelCollection.delegate = self
        payChannelCollection.dataSource = self
        payChannelCollection.tag = tagPayChannel
        let nib = UINib(nibName: "NormalButtonCollectionCell", bundle: nil)
        payChannelCollection.register(SubPayMethodCell.self, forCellWithReuseIdentifier:"cell")
        bankListCollectionView.register(SubPayMethodCell.self, forCellWithReuseIdentifier: "cell")
        //        payChannelCollection.register(nib, forCellWithReuseIdentifier: "normalButtonCollectionCell")
        payChannelCollection.showsVerticalScrollIndicator = false
        payChannelCollection.reloadData()
        
        //        payMethodTV.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onPayMethodSwitch)))
        confirmBtn.layer.cornerRadius = 5
        moneyInput.addTarget(self, action: #selector(inputMoney(textField:)), for: .editingChanged)
        confirmBtn.addTarget(self, action: #selector(onConfirm(ui:)), for: .touchUpInside)
        
        confirmBtn.theme_backgroundColor = "Global.themeColor"
        
        shouyingtai.delegate = self
        shouyingtai.dataSource = self
        shouyingtai.tag = tagShouyingtai
        //        shouyingtai.register(SubPayMethodCell.self, forCellWithReuseIdentifier:"cell")
        shouyingtai.register(nib, forCellWithReuseIdentifier: "normalButtonCollectionCell")
        //        shouyingtai.backgroundColor = UIColor.red
        
        //        if !self.onlines.isEmpty{
        //            let online = self.onlines[(self.currentPayIndex)]
        //            updateCurrentPayInfo(index: self.currentPayIndex)
        //            self.updateMoneyDatas(index: self.currentPayIndex)
        //            self.syncSysPayMethod(payId:(online.id))
        //        }
        //
        //        getPayMethodNamesData()
        
        bankListCollectionView.delegate = self
        bankListCollectionView.dataSource = self
        bankListCollectionView.tag = bankChannel
        bankViewConstraint.constant = 0
        bankListTitleLabel.isHidden = true
        
        requestPayMethod()
        
        setupTipsData()
        
        //        setupNoPictureAlphaBgView(view: self.payMethodBgView)
        setupNoPictureAlphaBgView(view: self.shouyintaiBgView)
        //        setupNoPictureAlphaBgView(view: self.payChannelCollection)
        //        setupNoPictureAlphaBgView(view: self.fastMoney)
        
        //        setupNoPictureAlphaBgView(view: self.payMethodBgView,bgViewColor: "FrostedGlass.viewGrayGlassOtherGray")
        self.payMethodBgView.backgroundColor=UIColor.white
        self.shouyingtai.backgroundColor = UIColor.white
        self.payChannelCollection.backgroundColor = UIColor.white
//        bankListCollectionView.backgroundColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //键盘监听
    //键盘监听
    @objc override func keyboardWillShow(notification: NSNotification) {
        shouyingView.snp.updateConstraints { (make) in
            make.height.equalTo(0)
        }
        payMethodBgView.snp.updateConstraints { (make) in
            make.height.equalTo(0)
        }
        
    }

    @objc override func keyboardWillHide(notification: NSNotification) {
        shouyingView.snp.updateConstraints { (make) in
            make.height.equalTo(45)
        }
        payMethodBgView.snp.updateConstraints { (make) in
            make.height.equalTo(40)
        }
    }
    
    //MARK: - 入款指南
    private func depoistGuidePictures() {
        requestDepositeGuidePictures(controller: self, bannerType: "8", success: {[weak self] (pictures) in
            if let weakSelf = self {
                if pictures.count > 0 {
                    weakSelf.introducePics = pictures
                    weakSelf.setupRightNavTitle(title: "存款指南")
                }
            }
        }) { (errorMsg) in
            
        }
    }
    
    func setupRightNavTitle(title:String) -> Void {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onRightMenuClick))
    }
    
    @objc func onRightMenuClick() -> Void {
        let pop = PagePicturePop.init(frame: .zero, urls: self.introducePics)
        pop.show()
    }
    
    func updateMoneyDatas(index:Int){
        if self.onlines.isEmpty{
            return
        }
        
        let tipString = self.onlines[index].appRemark
        
        if !tipString.isEmpty {
            view.addSubview(promptView)
            propmtLabel?.text = tipString
            let labeSize = propmtLabel?.sizeThatFits(CGSize.init(width: kScreenWidth - 40 - 20, height: CGFloat(MAXFLOAT)))
            let promptViewHeight = (labeSize?.height ?? 0) > CGFloat(50) ? labeSize?.height : 50

            promptView.isHidden = false
            promptView.snp.remakeConstraints { (make) in
                make.top.equalTo(view).offset(KNavHeight)
                make.left.right.equalTo(view)
                make.height.equalTo(promptViewHeight ?? 0)
            }
            indicateView?.snp.remakeConstraints({ (make) in
                make.left.equalTo(promptView).offset(20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.width.height.equalTo(10)
            })
            propmtLabel?.snp.remakeConstraints { (make) in
                make.left.equalTo(indicateView!.snp.right).offset(10)
                make.right.equalTo(promptView).offset(-20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.top.bottom.equalTo(promptView)
            }
            
            topTableViewConstraint.constant = promptViewHeight ?? 0
            
        }else{
            promptView.isHidden = true
            topTableViewConstraint.constant = 0
            
        }
        
        let minMoney = self.onlines[index].min
        let maxMoney = self.onlines[index].max
        moneyLimitTV.text = String(format: "温馨提示: 最低充值金额%d元，最大金额%d元", minMoney,maxMoney)
        //        let isFix = self.onlines[index].isFixedAmount
        self.moneyDatas.removeAll()
        
        //        if isFix != 1{
        
        var moneys = self.onlines[index].fixedAmount
        if  isBank {
            moneys = inputMoney
        }
        if !isEmptyString(str: moneys){
//            moneyInput.isUserInteractionEnabled = false
            let moneyArr = moneys.components(separatedBy: ",")
            self.moneyDatas = moneyArr
            moneyInput.text = self.moneyDatas[0]
            self.currtagFastRow = 0
        }else {
            moneyInput.isUserInteractionEnabled = true
            moneyInput.text = ""
        }
        
        //        }else {
        //            moneyInput.isUserInteractionEnabled = true
        //        }
        
        var lines = CGFloat(self.moneyDatas.count/4)
        if self.moneyDatas.count % 4 != 0{
            lines = lines + 1
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.moneyCollectViewHeightConstrait.constant = lines > 0 ? lines*35+10 : 0
            
            self.fastMoney.reloadData()
            bankListCollectionView.reloadData()
            
//        }
    }
    
    private func setupTipsData() {
        
        if shouldShowBrowsersChooseView()
        {
            var tips = "温馨提示：由于不同第三方支付的浏览器限制造成无法正常支付，请尝试切换其他浏览器来进行支付；您也可在设置中清除您选择的默认的支付浏览器，若有其他疑问，请联系客服。"
            if let config = getSystemConfigFromJson(){
                if config.content != nil{
                    let tipsContents = config.content.tip_for_multi_browser_pay
                    if !isEmptyString(str: tipsContents) {
                        //                        tips = tipsContents
                        tips = "温馨提示：" + tipsContents
                    }
                }
            }
            self.tipsTextView.text = tips
        }
        
    }
    @objc func onConfirm(ui:UIButton){
        
        if self.onlines.isEmpty{
            showToast(view: self.view, txt: "没有支付方式，无法发起支付")
            return
        }
        let money = moneyInput.text!
        if isEmptyString(str: money){
            showToast(view: self.view, txt: "请输入充值金额")
            return
        }
        
        let minFee = self.onlines[self.currSubPayRow].min
        let maxFee = self.onlines[self.currSubPayRow].max
        
        guard let mmoney = Float(money) else {
            showToast(view: self.view, txt: "金额格式不正确,请重新输入")
            return
        }
        if mmoney == 0 {
            showToast(view: self.view, txt: "充值金额不能为0")
            return
        }
        if mmoney < (Float(minFee)){
            showToast(view: self.view, txt: String.init(format: "充值金额不能小于%d元", minFee))
            return
        }
        
        if mmoney > (Float(maxFee)){
            showToast(view: self.view, txt: String.init(format: "充值金额不能大于%d元", maxFee))
            return
        }
        
        if self.shouyintais.isEmpty{
            showToast(view: self.view, txt: "没有收银台数据")
            return
        }
        
        let payId = self.onlines[self.currSubPayRow].id
       
        let bankCode = self.onlines[self.currSubPayRow].payType
        //        print("the onlines = ",self.onlines)
        //        print("bankcodes = ",payMethodNames)
        

        if payId == 0{
            showToast(view: self.view, txt: "请选择支付方式!")
            return
        }
        
        if isEmptyString(str: bankCode){
            showToast(view: self.view, txt: "请选择支付通道!")
            return
        }
        print("bankcode ",bankCode)
        let params:[String:AnyObject] = ["payId":payId,
                                          "amount":money,
                                          "bankCode":bankCode] as [String:AnyObject]
        request(frontDialog: true,method: .post,loadTextStr: "正在提交中...",url:ONLINE_PAY_URL,
                params: params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    if !resultStatus {
                        showToast(view: self.view, txt: convertString(string: "提交失败"))
                        return
                    }
                    if isEmptyString(str: resultJson){
                        showToast(view: self.view, txt: "提交失败")
                        return
                    }
                    
                    guard let data = resultJson.data(using: String.Encoding.utf8, allowLossyConversion: true) else {
                        return
                    }
                
                    
                    guard let json = try? JSONSerialization.jsonObject(with: data,options:.allowFragments) as! [String: Any] else{
                        return
                    }
                
                    
                    
                    if (json.keys.contains("success")){
                        if !(json["success"] as! Bool){
                            let msg = json["msg"] as! String
                            showToast(view: self.view, txt: json["msg"] as! String)
                            if msg.contains("其他地方") || msg.contains("您未登录"){
                                loginWhenSessionInvalid(controller: self)
                            }
                            return
                        }
                        //判断是否弹窗
                        let browerType = YiboPreference.getDefault_brower_type()
                        if browerType == -1 && shouldShowBrowsersChooseView(){
                            self.showBrowerSelectedView(json: json)
                        }else if browerType != -1 && shouldShowBrowsersChooseView(){
                            self.handleData(json: json,browerType:browerType)
                        }else {
                            self.handleData(json: json,browerType:BROWER_TYPE_SAFARI)
                        }
                    }
        })
    }
    
    func gotoBrowerWithType(type: Int, allways: Bool) {
        if let json = self.jsonData {
            if allways {
                YiboPreference.setDefault_brower_type(value: "\(type)")
            }
            self.handleData(json: json,browerType:type)
        }
    }
    
    func directUrlssUTF8(str:String)->String{
        var escaped = str.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        escaped = escaped.replacingOccurrences(of: "+", with: "%2b")
        escaped = escaped.replacingOccurrences(of: "=", with: "%3d")
        escaped = escaped.replacingOccurrences(of: "&", with: "%26")
        escaped = escaped.replacingOccurrences(of: "?", with: "%3f")
        escaped = escaped.replacingOccurrences(of: "+", with: "%2B")
        escaped = escaped.replacingOccurrences(of: ",", with: "%2c")
        escaped = escaped.replacingOccurrences(of: ":", with: "%3a")
        escaped = escaped.replacingOccurrences(of: ";", with: "%3b")
        escaped = escaped.replacingOccurrences(of: "@", with: "％40")
        escaped = escaped.replacingOccurrences(of: "$", with: "%24")
        escaped = escaped.replacingOccurrences(of: "/", with: "%2f")
        escaped = escaped.replacingOccurrences(of: "%252F", with: "%2f")
        
        return escaped
    }
    
    private func handleData(json: [String: Any],browerType: Int) {
        print(json)
        
        //        ["returnType": postSubmit, "dataMap": {
        //            customerid = 10017;
        //            notifyurl = "http://pay.slcp36.com/onlinePay/notify/huibaopay.do";
        //            paytype = alipay;
        //            returnurl = "https://skyw232.yunji9.com";
        //            sdorderno = D2019051214424000949;
        //            sign = 82f7574dd5b7b072c3384a080fe59b90;
        //            "total_fee" = "100.00";
        //            version = "1.0";
        //            }, "url": http://jinweizf.com:8086/monarch/apisubmit, "orderId": D2019051214424000949, "success": 1]
        
        if let method = json["form_method"]{
            if (method as! String) == "get"{
                let formActionStr = json["url"] as! String
                let formParams = json["dataMap"] as! [String:Any]
                
                var params = ""
                for item in formParams{
                    let key = item.key
                    let value = item.value
                    params += "\(key)=\(value)&"
                }
                if params.count > 0{
                    params = params.substrfromBegin(length: params.length-1)
                }
                
                let url = String.init(format: "%@?%@",formActionStr,params)
                print("the open url = ",url)
                let urlString = url.replacingOccurrences(of: "+", with: "%2b")
                openInBrowser(url: urlString,browerType:browerType,view: self.view)
                return
            }
        }
        
        if let type = json["returnType"]{
            let returnType = type as! String
            if returnType == "qrcodeUrl"{
                
                let formActionStr = BASE_URL + PORT + "/onlinePay/qrcodeRedirect4App.do"
                var formParams:Dictionary<String,Any> = [:]
                formParams["rechargeSubmitFormData"] = json["url"] as! String
                formParams["rechargeSubmitOrderId"] = json["orderId"] as! String
                formParams["rechargeSubmitPayName"] = json["payName"] as! String
                formParams["rechargeSubmitPayType"] = json["payType"] as! String
                formParams["rechargeSubmitOrderTime"] = json["orderTime"] as! String
                formParams["rechargeSubmitPayAmount"] = json["payAmount"] as! String
                formParams["rechargeSubmitPayFlag"] = json["flag"] as! String
                let params = getJSONStringFromDictionary(dictionary: formParams as NSDictionary)
                guard var escapedString = params.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {return}
                //将特殊字符替换成转义后的编码
                escapedString = escapedString.replacingOccurrences(of: "=", with: "%3D")
                escapedString = escapedString.replacingOccurrences(of: "&", with: "%26")
                escapedString = escapedString.replacingOccurrences(of: "+", with: "%2b")
               
                let url = String.init(format: "%@%@%@?returnType=%@&url=%@&data=%@", BASE_URL,PORT,PAY_RESULT_SAFARI,returnType,formActionStr,escapedString)
                print("the open url = ",url)
                openInBrowser(url: url,browerType:browerType,view: self.view)
                
            }else if returnType == "postSubmit"{
                //url:https://hujingpay.net/pay.php
                let formActionStr = json["url"] as! String
                let formParams = json["dataMap"] as! [String:AnyObject]
                let params = getJSONStringFromDictionary(dictionary: formParams as NSDictionary)
                
                guard var escapedString = params.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed) else {return}
                print(params)
                //将特殊字符替换成转义后的编码
                escapedString = escapedString.replacingOccurrences(of: "=", with:"%3D")
                escapedString = escapedString.replacingOccurrences(of: "&", with:"%26")
                escapedString =  escapedString.replacingOccurrences(of: "+", with: "%2b")
                
                
//                https://skyw191.yunji9.com 支付通道有点问题
//                params = params.replacingOccurrences(of: "{", with: "%7b")
//                params = params.replacingOccurrences(of: "}", with: "%7d")

                var url = String.init(format: "%@%@%@?url=%@&data=%@&returnType=%@", BASE_URL,PORT,PAY_RESULT_SAFARI,formActionStr,escapedString,returnType)
                
                if url.contains("http://jinweizf.com"){ //
                    var escapedparams = ""
                    for (kkk,vvv) in formParams {
                        let escapedkey = String.init(format:"%@=%@",kkk,vvv as! CVarArg)
                        let escapedparamsHB = String.init(format:"%@&",escapedkey)
                        escapedparams += escapedparamsHB
                    }
                    url = String.init(format: "%@",formActionStr)
                }
                
                
                var form_s_method = "post"
                if formParams.keys.contains("form_s_method"){
                    form_s_method = formParams["form_s_method"] as! String
                }
                if form_s_method == "get" {
                    print("the open url = ",url)
                    openInBrowser(url: url,browerType:browerType,view: self.view)
                    return
                }
                
                /*
                 主要修复站点w256支付宝支付
                 form_s_method：请求方式，注意：参数未测试
                 form_s_method默认post，则表单不会存在。如果是get，则会出现
                 如果是get方式，则使用浏览器方式，post方式则需再次请求一次，获取表单。
                 */
            
                self.request(frontDialog: true,method:.post,loadTextStr: "支付中...",url:formActionStr,params:formParams,
                             callback: {(resultJson:String,resultStatus:Bool)->Void in
                                print(resultJson)
                                if resultJson.contains("<!DOCTYPE"){
                                    let loginVC = UIStoryboard(name: "bbin_page", bundle: nil).instantiateViewController(withIdentifier: "bbin")
                                    let recordPage = loginVC as! BBinWebContrllerViewController
                                    recordPage.htmlContent = resultJson
                                    self.navigationController?.pushViewController(recordPage, animated: true)
                                }else{
                                    print(url)
                                    openInBrowser(url: url,browerType:browerType,view: self.view)
                                }
                })
                return
                
            }else if returnType == "href"{
                let formActionStr = json["url"] as! String
                openBrower(urlString: formActionStr)
            }else if returnType == "write"{
                let formActionStr = json["url"] as! String
                if formActionStr.starts(with: "<!DOCTYPE") || formActionStr.starts(with: "<!doctype"){
                    openActiveDetail(controller: self, title: "支付", content: formActionStr)
                }else if formActionStr.contains("window.location.href=\'") {
                    //截取链接开始到最后一位
                    let components = "window.location.href=\'"
                    let startLocation_1 = formActionStr.positionOf(sub: components)
                    let results_1 = formActionStr.subString(start: startLocation_1 + components.length, length: formActionStr.length - startLocation_1 - components.length)
                    // 截取最终链接
                    let startLocation_2 = results_1.positionOf(sub: "\';")
                    let results_2 = results_1.substrfromBegin(length: startLocation_2)
                    openBrower(urlString: results_2)
                    
                }else{
                    openBrower(urlString: formActionStr)
                }
            }else{
                let formActionStr = json["url"] as! String
                let url = String.init(format: "%@%@%@?returnType=%@&url=%@&data=%@", BASE_URL,PORT,PAY_RESULT_SAFARI,returnType,formActionStr,"")
                print("the open url = ",url)
                openInBrowser(url: url,browerType:browerType,view: self.view)
            }
        }else{
            if let url = json["url"] as? String {
                openInBrowser(url: url,browerType:browerType,view: self.view)
            }else {
                showToast(view: self.view, txt: "没有跳转链接，无法支付",afterDelay:2)
            }
        }
    }
    
    
    //MARK: 弹窗显示浏览器选择
    private func showBrowerSelectedView(json: [String: Any]) {
        let tupeArray = [("Browser_safari","Safari浏览器"),("Browser_uc","UC浏览器"),
                         ("Browser_qq","QQ浏览器"),("Browser_google","谷歌浏览器"),
                         ("Browser_firefox","火狐浏览器")]
        let selectedView = NewSelectView(dataSource: tupeArray, viewTitle: "请选择浏览器")
        selectedView.delegate = self
        selectedView.selectedIndex = selectedIndex
        self.jsonData = json
        
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.selectedIndex = index
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
    }
    
    
    func actionCommitOrder(amount:String,payId:Int,bankCode:String){
        
    }
    
    func openConfirmPayController(orderNo:String,accountName:String,chargeMoney:String,
                                  payMethodName:String, receiveName:String,receiveAccount:String,dipositor:String,dipositorAccount:String,qrcodeUrl:String,payType:Int,payJson:String) -> Void {
        if self.navigationController != nil{
            openConfirmPay(controller: self, orderNo: orderNo, accountName: accountName, chargeMoney: chargeMoney, payMethodName: payMethodName, receiveName: receiveName, receiveAccount: receiveAccount, dipositor: dipositor, dipositorAccount: dipositorAccount, qrcodeUrl: qrcodeUrl, payType: payType, payJson: payJson)
        }
    }
    //通道高计算？
    private func getPayMethodNamesData(type:Int) {
      
        shouyintais.removeAll()
        if !self.onlines.isEmpty{
            for item in self.onlines{
                if isEmptyString(str: item.payAlias) {
                    self.shouyintais.append(item.payName)
                }else {
                    self.shouyintais.append(item.payAlias)
                }
            }
        }
        currSubPayRow = 0
        
        var row = self.shouyintais.count / 3
        let rows = self.shouyintais.count % 3
        if rows >= 1 {
            row += 1
        }
        
        subPayCollectViewHeightConstrait.constant = CGFloat(row * 40)
        
        if self.shouyintais.isEmpty {
            showToast(view: self.view, txt: "没有通道，请联系客服")
        }
        
        shouyingtai.reloadData()
        bankListCollectionView.reloadData()
        
        self.updateMoneyDatas(index: (self.currSubPayRow))
    
    }
    //MARK:计算银行的高度
    func calculateBankListViewHeight(){
        var row = self.bankLists.count / 3
        let rows = self.bankLists.count % 3
        if rows >= 1 {
            row += 1
        }
        currSubBankRow = 0
        bankViewConstraint.constant = CGFloat(row * 50) + 40
        
       
        self.fastMoney.reloadData()
        bankListCollectionView.reloadData()
        shouyingtai.reloadData()
        
        currSubPayRow = 0
        self.updateMoneyDatas(index: (self.currSubPayRow))
    }
    
    
    
    func updateCurrentPayInfo(index:Int){
        //        if self.onlines.isEmpty{
        //            return
        //        }
        //        payMethodTV.text = self.onlines[index].payName
    }
    
    private func showPayDialog(){
        let selectedView = LennySelectView(dataSource: self.payMethodNames, viewTitle: "请选择支付")
        selectedView.selectedIndex = self.currentPayIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.currentPayIndex = index
            self?.currSubPayRow = 0
            self?.updateCurrentPayInfo(index:(self?.currentPayIndex)!)
            self?.updateMoneyDatas(index: (self?.currentPayIndex)!)
            
            //            let online = self?.onlines[(self?.currentPayIndex)!]
            //            self?.syncSysPayMethod(payId:(online?.id)!)
            
            if let path = self?.payMethodNames[(self?.currentPayIndex)!] {
                self?.requestPayPath(path: path)
            }
            
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
            //            self.setSelected(false, animated: true)
        }
    }
    
    //获取收银台列表
    func syncSysPayMethod(payId:String){
        request(frontDialog: true,method: .get,loadTextStr: "正在同步中...",url:SYNC_SHOUYINGTAI_LIST,params: ["payId":payId],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        showToast(view: self.view, txt: convertString(string: "同步失败"))
                        return
                    }
                    if isEmptyString(str: resultJson){
                        showToast(view: self.view, txt: "同步失败")
                        return
                    }
                    if resultJson == "{}"{
                        showToast(view: self.view, txt: "没有支付方式")
                        self.shouyintais.removeAll()
                        self.shouyingtai.reloadData()
                        return
                    }
                    
                    if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                        loginWhenSessionInvalid(controller: self)
                        return
                    }
                    
                    do {
                        let data = resultJson.data(using: String.Encoding.utf8)
                        let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String]
                        print("json == ",json)
                        self.shouyintais.removeAll()
                        self.shouyintais = self.shouyintais + json
                        var lines = CGFloat(self.shouyintais.count/3)
                        if self.shouyintais.count % 3 != 0{
                            lines = lines + 1
                        }
                        if lines > 0{
//                            self.subPayCollectViewHeightConstrait.constant = lines*35+10 > 150 ? 150 : lines*35+10 ???
                        }else{
                            self.subPayCollectViewHeightConstrait.constant = 0
                        }
                        self.shouyingtai.reloadData()
                    }catch let error {
                        print("convert: error \(error)")
                    }
        })
    }
}

extension OnlineMethodPayV1Controller {
    //MARK: -网络请求
    /** 获取支付方法 */
    func requestPayMethod() {
        let params = ["payCode":"online"] as [String:AnyObject]
        request(frontDialog: true, url: URL_ONLINE_PAY_METHOD,params:params) { (resultJson, success) in
            if !success {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取支付方法失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if resultJson == "{}"{
                showToast(view: self.view, txt: "没有支付方法")
                return
            }
            
            if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                loginWhenSessionInvalid(controller: self)
                return
            }
            
            do {
                if let data = resultJson.data(using: String.Encoding.utf8) {
                    if let datas = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, Any> {
                        if let onlineList = datas["onlineList"] as? [String] {
                            self.payMethodNames = onlineList
                            
                            var lines = CGFloat(self.payMethodNames.count/3)
                            if self.payMethodNames.count % 3 != 0{
                                lines = lines + 1
                            }
                            if lines > 0{
                                //                                self.payChannelHeightConstrait.constant = lines*35+10 > 200 ? 200 : lines*35+50 //？？？
                                print("lines")
                                print(lines)
                                self.payChannelHeightConstrait.constant = lines * 40 + 10 //支付方式高度
                            }else{
                                self.payChannelHeightConstrait.constant = 0
                            }
                            
                            if onlineList.count > 0 {
                                self.currentPayIndex = 0
                                
                                let path = self.payMethodNames[(self.currentPayIndex)]
                                if path == "OTHER"{
                                    self.getBankList(path: path)
                                    self.bankViewConstraint.constant = 100
                                }else{
                                    self.bankViewConstraint.constant = 0
                                    self.requestPayPath(path: path)
                                }
                              
                            }
                            
                            self.payChannelCollection.reloadData()
                            
                        }
                    }
                }
            }catch let error {
                showToast(view: self.view, txt: error.localizedDescription)
            }
            
            print("支付方法:",resultJson)
        }
    }
    
    /** 获取支付通道 */
    func requestPayPath(path:String) {
        let params = ["payType":path]
        request(frontDialog: true, url: URL_ONLINE_PAY_PATH,params:params) { (resultJson, success) in
            if !success {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取支付方法失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if resultJson == "{}"{
                showToast(view: self.view, txt: "没有支付方法")
                return
            }
            
            if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                loginWhenSessionInvalid(controller: self)
                return
            }
            
            if let result = OnlineFunFirstPayWraper.deserialize(from: resultJson) {
                self.onlines.removeAll()
                self.onlines = result.onlineList
                self.moneyDatas.removeAll()
               
                self.getPayMethodNamesData(type:  self.isBank ? self.bankChannel : self.tagPayChannel)
                if self.isBank{
                     self.calculateBankListViewHeight()
                }
              
//                  self.getPayMethodNamesData(type:  self.tagPayChannel)
            }
            
            print("支付通道:",resultJson)
        }
    }
    //获取银行列表
    func getBankList(path:String){
        let  url = URL_ONLINE_PAY_BANK
        let params = ["bank":path]
        request(frontDialog: true, url:url,params:params) { (resultJson, success) in
            if !success {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取支付方法失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if resultJson == "{}"{
                showToast(view: self.view, txt: "没有支付方法")
                return
            }
            
            if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                loginWhenSessionInvalid(controller: self)
                return
            }
            
            if let result = OnlineFunFirstPayWraper.deserialize(from: resultJson) {
                self.currSubBankRow = 0
                self.bankLists.removeAll()
                self.bankLists = result.bankList
                self.bankListTitleLabel.isHidden = false
                self.bankViewConstraint.constant = 100
                self.requestPayPath(path: self.bankLists[self.currSubBankRow])
                //计算银行高度
//                self.getPayMethodNamesData(type: self.bankChannel)

            }
        }
    }
    
//    @objc func loadTip() {
//        request(frontDialog: true,method: .get,loadTextStr: "加载配置中...", url:SYS_CONFIG_URL,
//                callback: {(resultJson:String,resultStatus:Bool)->Void in
//                    if !resultStatus {
//                        if resultJson.isEmpty {
//                            showToast(view: self.view, txt: convertString(string: "获取配置失败,请重试"))
//                        }else{
//                            showToast(view: self.view, txt: resultJson)
//                        }
//                        //                        self.gojumpTouterTest()
//                        return
//                    }
//                    print(resultJson)
////                    if let result = SystemWrapper.deserialize(from: resultJson){
////                        if result.success{
////                            if let token = result.accessToken{
////                                UserDefaults.standard.setValue(token, forKey: "token")
////                            }
////                            //save lottery version
////                            if let config = result.content{
////                                if !isEmptyString(str: config.yjf){
////                                    YiboPreference.setYJFMode(value: config.yjf as AnyObject)
////                                }
////                                YiboPreference.saveMallStyle(value: config.native_style_code as AnyObject)
////                                YiboPreference.setMallImageTextTabStyle(value: config.switch_optimize_mainpage_tabs as AnyObject)
////                            }
////                            //save system config to user default
////                            YiboPreference.saveConfig(value: resultJson as AnyObject)
////                            //                            setupTheme()
////                            //                            self.doAfterDomain()
////                        }else{
////                            if let errorMsg = result.msg{
////                                showToast(view: self.view, txt: errorMsg)
////                            }else{
////                                showToast(view: self.view, txt: convertString(string: "获取配置失败,请重试"))
////                            }
////                            //                            self.gojumpTouterTest()
////                        }
////                    }
//        })
//    }
}

extension OnlineMethodPayV1Controller : UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == tagFastMoney{
            return moneyDatas.count
            
        }else if collectionView.tag == tagShouyingtai{
            
            return shouyintais.count
            
        }else if collectionView.tag == tagPayChannel {
            //支付通道
            return self.payMethodNames.count
            
        }else if collectionView.tag == bankChannel{
            
            return bankLists.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == tagFastMoney{
            return CGSize.init(width: (kScreenWidth-0.5*6)/4, height: 35)
        }else if collectionView.tag == tagShouyingtai {
            var fast = self.shouyintais[indexPath.row]
            if isEmptyString(str: fast) {
                fast = "没有名称"
            }
            return CGSize.init(width: (kScreenWidth-10)/3, height: 35)
        }else if collectionView.tag == bankChannel{
            return CGSize(width: (kScreenWidth-0.5*6)/4, height: 35)
        }else {
            return CGSize.init(width: (kScreenWidth-0.5*6 - 15 * 2)/3, height: 35)
        }
    }
    
    //返回自定义的cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == tagShouyingtai {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "normalButtonCollectionCell", for: indexPath) as! NormalButtonCollectionCell
            
            var fast = self.shouyintais[indexPath.row] //支付通道data
            if isEmptyString(str: fast) {
                fast = "没有名称"
            }
            cell.normaltext.font = UIFont.systemFont(ofSize: 17.0)
            cell.normaltext.text = fast
            if kScreenHeight <= 568 { //iphone5
                cell.normaltext.font = UIFont.systemFont(ofSize: 12.0)
            }else{
                if fast.length > 6 {
                    cell.normaltext.font = UIFont.systemFont(ofSize: 10.0)
                }
            }
            
            if currSubPayRow == indexPath.row {
                cell.normalV.isHidden = false
                cell.normaltext.layer.borderColor = UIColor.colorWithRGB(r: 236, g: 40, b: 41, alpha: 1).cgColor
                
            }else {
                cell.normaltext.layer.borderColor = UIColor.lightGray.cgColor
                cell.normalV.isHidden = true
            }
            
            return cell
        }else if collectionView.tag == tagFastMoney{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PayInfoMoneyCell
            let data = self.moneyDatas[indexPath.row]
            cell.setupBtn(money: data)
            
            if self.currtagFastRow == indexPath.row {
                cell.moneyBtn.layer.borderColor = UIColor.colorWithRGB(r: 236, g: 40, b: 41, alpha: 1).cgColor
                cell.moneyV.isHidden = false
            }else{
                cell.moneyBtn.layer.borderColor = UIColor.lightGray.cgColor
                cell.moneyV.isHidden = true
            }
            return cell
        }else if collectionView.tag == bankChannel{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SubPayMethodCell
            let data = self.bankLists[indexPath.row]
            cell.setupBtn(payName: data, funcFirst: false, isBank: true)
            print(currentPayIndex)
            if currSubBankRow == indexPath.row {
                cell.moneyMAGE.layer.borderColor = UIColor.colorWithRGB(r: 236, g: 40, b: 41, alpha: 1).cgColor
                cell.moneyV.isHidden = false
            }else{
                cell.moneyMAGE.layer.borderColor = UIColor.lightGray.cgColor
                cell.moneyV.isHidden = true
            }
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SubPayMethodCell
            let data = self.payMethodNames[indexPath.row]
            cell.setupBtn(payName: data)
            if self.currentPayIndex == indexPath.row {
                cell.moneyMAGE.layer.borderColor = UIColor.colorWithRGB(r: 236, g: 40, b: 41, alpha: 1).cgColor
                cell.moneyV.isHidden = false
            }else{
                cell.moneyMAGE.layer.borderColor = UIColor.lightGray.cgColor
                cell.moneyV.isHidden = true
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == tagFastMoney{
            let data = self.moneyDatas[indexPath.row]
            self.moneyInput.text = data
            self.currtagFastRow = indexPath.row
            collectionView.reloadData()
        }else if collectionView.tag == tagShouyingtai{
            self.currSubPayRow = indexPath.row
            self.updateMoneyDatas(index: (self.currSubPayRow))
            collectionView.reloadData()
        }else if collectionView.tag == bankChannel{
            //选择支付银行
            let code = bankLists[indexPath.item]
            currSubBankRow = indexPath.item
            //获取支付通道
            self.requestPayPath(path:code)
            isBank = true
            collectionView.reloadData()
        }else {
            
            currentPayIndex = indexPath.row
            let path = self.payMethodNames[(self.currentPayIndex)]
            if path == "OTHER"{
                getBankList(path: path)
                isBank = true
            }else{
                bankViewConstraint.constant = 0
                bankListTitleLabel.isHidden = true
                isBank = false
                requestPayPath(path: path)
            }
            self.currSubPayRow = 0
            self.updateCurrentPayInfo(index:(self.currentPayIndex))
            self.updateMoneyDatas(index: (self.currSubPayRow))
            
            payChannelCollection.reloadData()
        }
    }

}
extension OnlineMethodPayV1Controller{
    
    @objc private func inputMoney(textField: CustomFeildText){
        inputMoney = textField.text ?? ""
    }
}
