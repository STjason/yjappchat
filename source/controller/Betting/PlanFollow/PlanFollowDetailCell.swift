//
//  PlanFollowDetailCell.swift
//  gameplay
//
//  Created by admin on 2020/8/30.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class PlanFollowDetailCell: UITableViewCell {

    @IBOutlet weak var ballsViewConsW: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var periodLabel: ASLabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var ballsView: BallsView!
    
    var lotTypeCode = "" //如'BJSC 北京赛车'
    var cpVersion = "" //1官方，2信用
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self.bgView)
        self.bgView.layer.cornerRadius = 5
        
        statusLabel.textColor = UIColor.white
        statusLabel.layer.cornerRadius = 15
        statusLabel.layer.masksToBounds = true
    }
    
    func configWith(model:PlanPreHistoryModel) {
        let periodValue = "\(model.qiHao)期"
        let periodValueRange = NSRange.init(location: 0, length: (periodValue.length - 1)) //期
        let periodTitleRange = NSRange.init(location: periodValueRange.length, length: 1)//具体期号
        
        periodLabel.setAttributeString(text: periodValue, ranges: [periodValueRange,periodTitleRange], colors: [UIColor.init(hexString: "#388ec5")!,UIColor.black],fonts: [14,16])
    
        if model.status == "1" {
            statusLabel.text = "待"
            statusLabel.backgroundColor = UIColor.lightGray
        }else if model.status == "2" {
            statusLabel.text = "中"
            statusLabel.backgroundColor = UIColor.init(hexString: "#ff625c")
        }else if model.status == "3" {
            statusLabel.text = "挂"
            statusLabel.backgroundColor = UIColor.init(hexString: "#00bf71")
        }
        
        let nums = model.haoMa.components(separatedBy: ",")
        let ballWidth:CGFloat = 20
        let offset:CGFloat = 2
        let ballViewW = ballWidth * CGFloat(nums.count) + offset * CGFloat(nums.count - 1) + 5//5 距离右边屏幕宽度
        ballsViewConsW.constant = ballViewW
        
        ballsView.basicSetupBalls(nums: nums, offset: Int(offset), lotTypeCode: self.lotTypeCode, cpVersion: self.cpVersion, ballWidth:ballWidth, gravity_bottom:false,ballsViewWidth: ballViewW,focusNum: model.winHaoMa)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}

