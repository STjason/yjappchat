//
//  PlanFollowBetWraper.swift
//  gameplay
//
//  Created by admin on 2020/9/4.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class PlanFollowBetWraper: HandyJSON {
    var success = true
    var msg = ""
    var follow = [PFFollowModel]()
    var plan = [PFPlanModel]()

    required init() {}
}

class PFFollowModel: HandyJSON {
    var ballNum = ""//: 5,
    var followNum = "" //剩余跟投；//: 2,
    var infoId = ""//跟投的id //: 1068,
    var playName = ""//: "亚军",
    var predictorName = "" //计划员名字//": "9999号计划员"
    required init() {}
}

class PFPlanModel: HandyJSON {
    var ballNum = ""// 5,
    var cl = ""// 0,
    var currentNum = ""// 13,
    var haoMa = "" // "02,06,03,05,07",
    var infoId = ""// 1076,
    var playName = ""// "第八名",
    var predictorName = ""// "9999号计划员",
    var qiHao = ""// "749628",
    var totalNum = "" // 23,
    var winNum = ""// 1,
    var winRate = "" // 8.3300
    
    var code = "" //dyj //赛车 亚军code
    required init() {}
}
