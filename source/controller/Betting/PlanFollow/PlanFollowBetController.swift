//
//  PlanFollowBetController.swift
//  gameplay
//
//  Created by admin on 2020/8/23.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import HandyJSON

class PlanFollowBetController: BaseMainController {
    @IBOutlet weak var stackConsH: NSLayoutConstraint!
    @IBOutlet weak var stackMiddle: UIView!
    @IBOutlet weak var stackBottom: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var segmentBgView: UIView!
    @IBOutlet weak var middleLeftBtn: UIButton!
    @IBOutlet weak var middleMiddleBtn: UIButton!
    @IBOutlet weak var middleRightLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var closingTimeLabel: UILabel!
    @IBOutlet weak var openingTimeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var remainCount = 0  //剩余跟投期数
    var tapRulePop:SwiftPopMenu? //点击玩法弹出的玩法选择pop
    var titleIndictor:UIImageView = UIImageView.init()
    var titleBtn:UIButton?
    var follows = [PFFollowModel]() //已跟专家list数据
    var viewType = 0 //0专家排名，1已跟专家
    var endlineTouzhuTimer:Timer?//距离停止下注倒计时器
    var disableBetCountDownTimer:Timer?//禁止下注倒计时器
    var endBetTime:Int = 0//距离停止下注的时间
    var disableBetTime:Int64 = 0//距离再次开始下注的剩余时间
    var endBetDuration:Int = 0;//距离本期不能下注的剩余时间
    var viewShowing = true //该页面是否正在显示
    var currentQihao:String = "" { //当前期号
        didSet {
            if currentQihao != oldValue {
//                //期数改变 lotData.ago 时间之后刷新 requePlanList
//                DispatchQueue.main.asyncAfter(deadline: .now() + TimeInterval(ago)) {
//                    self.requestPlanList()
//                }
                self.requestPlanList()
            }
        }
    }
    var ago:Int64 = 0//开奖时间与封盘时间差,单位秒
    var oneToTenRules = [HonestResult]()
    var baseTag = 10000
    var ballNums = ["5","6","7"] //头部 ‘’码计划
    var playRuleType = 0 //0 （选择 玩法 冠亚军等排名），1（5，6，7码）
    var meminfo:Meminfo?
    var planCodes = [PlanCodeModel?]()
    
    var code:String = "" { //彩种code
        didSet {
            endlineTouzhuTimer?.invalidate()
            disableBetCountDownTimer?.invalidate()
            
            plans.removeAll()
            follows.removeAll()
            
            //如果一秒后没有获取到最新数据，要刷新tableview，以清空垃圾数据
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.plans.count == 0 {
                    self.tableView.reloadData()
                }
            }
            
            sync_playrule_from_current_lottery(lotType: (lotData.czCode ?? "3"), lotCode: code, lotVersion: ("\(lotData.lotVersion)")) {[weak self] (lot) in
                guard let weakSelf = self else {return}
                if let wraper = lot {
                    weakSelf.lotData = wraper
                    weakSelf.getCountDownByCpCodeAction()
                }
            }
        }
    }
    
    var plans = [PFPlanModel]() {  //专家排名list数据
        didSet {
            if plans.count > 0 {
                let model = plans.first!
                middleRightLabel.text = "当前轮数：\(model.currentNum)/\(model.totalNum)"
            }
        }
    }
    
    var lotData:LotteryData = LotteryData() {
        didSet {
            ago = lotData.ago
        }
    }
    var disableBet = false { //可以下注
        didSet {
            print("disableBet = \(disableBet)")
            followBetPop.configDisableBet(disable: disableBet)
            tableView.reloadData()
            
            if navigationController?.topViewController?.isKind(of: PlanFollowDetailController.self) ?? false {
                if let vc = navigationController?.topViewController as? PlanFollowDetailController {
                    vc.disableBet = disableBet
                    vc.remainCount = remainCount
                    vc.followBetPop.remainCount = remainCount
                    vc.tableView.reloadData()
                }
            }
        }
    }
    var accountBanlance = 0.0 { //余额
        didSet {
            followBetPop.balanceLabel.text = String.init(format: "%.2f元", accountBanlance)
        }
    }
    
    var selectRuleIndex = 0 //选择的玩法index
    var selectBallNumIndex = 0 //选择的ballNum Index
   
    lazy var followBetPop:PlanFollowBetPop = { //跟投，自动跟投PopView
        let view = Bundle.main.loadNibNamed("PlanFollowBetPop", owner: self, options: nil)?.last as! PlanFollowBetPop
        view.layer.cornerRadius = 5
        view.dismissHandler = {[weak self] in
            guard let weakSelf = self else {return}
            weakSelf.hidePopView()
        }
        
        view.planBetHandler = {[weak self] (items,reverseBet) in
            guard let weakSelf = self else {return}
            weakSelf.requestFollowBet(items: items,reverseBet:reverseBet)
        }
        
        view.planAutoBetHandler = {[weak self] (items,infoInd) in
            guard let weakSelf = self else {return}
            weakSelf.requestPlanAutoBet(items: items, infoId: infoInd)
        }
        return view
    }()
    
    lazy var bgView:UIButton =  {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.frame = UIScreen.main.bounds
        view.addTarget(self, action: #selector(clickBgView), for: .touchUpInside)
        return view
    }()
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view, alpha: 0)
        segmentBgView.theme_backgroundColor = "Global.barTintColor"
        tableView.backgroundColor = UIColor.clear
        setupNoPictureAlphaBgView(view: stackMiddle)
        setupNoPictureAlphaBgView(view: stackBottom)
        
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewShowing = true
        self.getCountDownByCpCodeAction()
        self.requestPlanList()
        self.requestFlanCodes()
        
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func releaseData() {
        viewShowing = false
        self.endlineTouzhuTimer?.invalidate()
        self.disableBetCountDownTimer?.invalidate()
        
        IQKeyboardManager.shared.enable = false
    }
    
    deinit {
        releaseData()
    }
    
    func setupView() {
        self.customTitleView()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        setupMiddleBtn(btn:middleLeftBtn)
        setupMiddleBtn(btn:middleMiddleBtn)
                
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib.init(nibName: "PlanFollowBetCell", bundle: nil) //专家排名
        tableView.register(nib, forCellReuseIdentifier: "PlanFollowBetCell")
        
        let nibFollow = UINib.init(nibName: "FollowPredictorCell", bundle: nil)//已跟专家
        tableView.register(nibFollow, forCellReuseIdentifier: "FollowPredictorCell")
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        
        setupSegment()
        
        middleLeftBtn.addTarget(self, action: #selector(tapMiddleLeftBtn), for: .touchUpInside)
        middleMiddleBtn.addTarget(self, action: #selector(tapMiddleMiddleBtn), for: .touchUpInside)
        middleLeftBtn.tag = baseTag
        middleMiddleBtn.tag = baseTag + 1
        
        let name = self.oneToTenRules[self.selectRuleIndex].name
        self.middleLeftBtn.setTitle(name, for: .normal)
    }
    
    func setupSegment() {
        segment.selectedSegmentIndex = 0
        segment.layer.borderColor = UIColor.white.cgColor
        segment.layer.borderWidth = 1
        
        if #available(iOS 13.0, *) {
            
            let themeBgName = YiboPreference.getCurrentThmeByName()
            
            var selectedColor = UIColor.red
            if themeBgName == "Red" {
                selectedColor = UIColor.init(hexString: "#ec2829")!
            }else if themeBgName == "Blue" || themeBgName == "FrostedPlain"{
                selectedColor = UIColor.init(hexString: "#0046bc")!
            }else if themeBgName == "Green" {
                selectedColor = UIColor.init(hexString: "#107846")!
            }else if themeBgName.contains("Frosted") {
                selectedColor = UIColor.init(hexString: "#000000")!
                segment.layer.borderColor = UIColor.black.cgColor
            }else { //可下载主题时
                selectedColor = UIColor.init(hexString: "#ec2829")!
            }
            
        segment.setBackgroundImage(UIImage().createImageWithColor(color: UIColor.white), for: .selected, barMetrics: .default)
        segment.setBackgroundImage(UIImage().createImageWithColor(color: selectedColor), for: .normal, barMetrics: .default)
            
        let font = UIFont.boldSystemFont(ofSize: 17)

        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:font], for: .normal)
            
        segment.theme_setTitleTextAttributes(ThemeStringAttributesPicker(keyPath: "Global.themeColor") {
                value -> [NSAttributedString.Key : AnyObject]? in
                guard let rgba = value as? String else {
                    return nil
                }
                
                let color = UIColor(rgba: rgba)
                let titleTextAttributes = [
                    NSAttributedString.Key.foregroundColor: color,
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                    ]
                
                return titleTextAttributes
            }, forState: .selected)
        }
        
        segment.theme_backgroundColor =  "Global.barTintColor"
        segment.theme_tintColor = "Global.barTextColor"
        
        segment.addTarget(self, action: #selector(segValueChangeAction), for: .valueChanged)
    }
    
    //MARK: SwiftPop
    func setupPopMenu(tapView: UIView) {
        let tag = tapView.tag - baseTag
        
        var tapRulePopDatas = [String]()
        if tag == 0 {
            for model in oneToTenRules {
                tapRulePopDatas.append(model.name)
            }
        }else if tag == 1 {
            for title in ballNums {
                tapRulePopDatas.append("\(title)码计划")
            }
        }else if tag == 2 {
            for model in planCodes {
                if let wraper = model {
                    tapRulePopDatas.append(wraper.name)
                }
            }
        }
        
        guard let delegate = UIApplication.shared.delegate,let window = delegate.window else {return}
        
//        let isSender = message.userId == CRDefaults.getUserID()
        let popWidth:CGFloat = 120 //菜单宽
        let point = tapView.convert(CGPoint.zero, to: window)
        let popHeight:CGFloat = CGFloat(44 * tapRulePopDatas.count)
        
        let x = point.x
//        if  isSender {
//            x = point.x - 5 - SwiftPopMenu.arrowViewHeight - popWidth
//        }else {
//            x = point.x + tapView.width + 5 + SwiftPopMenu.arrowViewHeight
//        }
        
        var y:CGFloat = 0 //菜单相对screen的 y坐标
        var arrowMargin:CGFloat = 0 //箭头相对菜单 坐标 Y的偏移
        
        if point.y >= (window?.height)! * 0.5 {
            y = point.y - popHeight + tapView.height
            arrowMargin = popHeight - tapView.height * 0.5
        }else {
            y = point.y + 30
            arrowMargin = tapView.height * 0.5
        }
        
        let frame = CGRect.init(x: x, y: y, width: popWidth, height: popHeight)
        
        tapRulePop = SwiftPopMenu.init(frame: frame, arrowMargin: arrowMargin)
        tapRulePop?.arrowDirection = .top
        tapRulePop?.popDataArray = tapRulePopDatas
        
        tapRulePop?.didSelectMenuBlock = { [weak self](index:Int)->Void in
            guard let weakSelf = self else {
                return
            }
            
            weakSelf.tapRulePop?.dismiss()
            
            if tag == 0 {
                weakSelf.selectRuleIndex = index
                let name = weakSelf.oneToTenRules[weakSelf.selectRuleIndex].name
                weakSelf.middleLeftBtn.setTitle(name, for: .normal)
            }else if tag == 1 {
                weakSelf.selectBallNumIndex = index
                let name = weakSelf.ballNums[weakSelf.selectBallNumIndex]
                weakSelf.middleMiddleBtn.setTitle("\(name)码计划", for: .normal)
            }else if tag == 2 {
                if let model = weakSelf.planCodes[index] {
                    weakSelf.title = model.name
                    weakSelf.code = model.code
                    weakSelf.titleBtn?.setTitle(model.name, for: .normal)
                }
            }
            
            weakSelf.requestPlanList()
        }
        
        tapRulePop?.show()
    }
    
    //MARK: 聊天室关闭下，自定义标题栏，方便点击标题栏切换彩票版本
    func customTitleView() -> Void {
        self.navigationItem.titleView?.isUserInteractionEnabled = true
        let titleView = UIView.init(frame: CGRect.init(x: kScreenWidth/4, y: 0, width: kScreenWidth/2, height: 44))
        titleBtn = UIButton.init(frame: CGRect.init(x: titleView.bounds.width/2-56, y: 0, width: 100, height: 44))
        titleBtn?.isUserInteractionEnabled = false
        titleBtn?.tag = baseTag + 2
        titleView.addSubview(titleBtn!)
        titleIndictor = UIImageView.init(frame: CGRect.init(x:(titleBtn?.x ?? 0) + (titleBtn?.bounds.width)!, y: 22-6, width: 12, height: 12))
        
        titleIndictor.theme_image = "FrostedGlass.Touzhu.navDownImage"
        titleView.addSubview(titleIndictor)
        titleBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        titleBtn?.setTitle(self.title ?? "", for: .normal)
        
        self.navigationItem.titleView = titleView
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(titleClickEvent(recongnizer:)))
        self.navigationItem.titleView?.addGestureRecognizer(tap)
    }
    
    //处理顶部标题栏点击事件
    @objc func titleClickEvent(recongnizer:UIPanGestureRecognizer) -> Void {
        self.setupPopMenu(tapView: titleBtn!)
    }
    
    ///设置开盘时间
    func setupOpeningTime(time:String) {
        let openingTime = "开盘：\(time)"
        let openingTitleRange = NSRange.init(location: 0, length: 3)
        let openingValueRange = NSRange.init(location: openingTitleRange.length, length: (openingTime.length - openingTitleRange.length))
        
        openingTimeLabel.setAttributeString(text: openingTime, ranges: [openingTitleRange,openingValueRange], colors: [UIColor.black,UIColor.init(hexString: "#107846")!],fonts: [16,14])
    }
    
    ///设置封盘时间
    func setupClosingTime(time:String) {
        let clossingTime = "封盘：\(time)"
        let clossingTitleRange = NSRange.init(location: 0, length: 3)
        let clossingValueRange = NSRange.init(location: clossingTitleRange.length, length: (clossingTime.length - clossingTitleRange.length))
        
        closingTimeLabel.setAttributeString(text: clossingTime, ranges: [clossingTitleRange,clossingValueRange], colors: [UIColor.black,UIColor.red],fonts: [16,14])
    }
    
    ///设置期号
    func setupPeriod(period:String) {
        //期号
        if let wrapPeriod = Int(period) {
            let periodValue = "\(wrapPeriod)期"
            let periodValueRange = NSRange.init(location: 0, length: (periodValue.length - 1)) //期
            let periodTitleRange = NSRange.init(location: periodValueRange.length, length: 1)//具体期号
            
            periodLabel.setAttributeString(text: periodValue, ranges: [periodValueRange,periodTitleRange], colors: [UIColor.init(hexString: "#388ec5")!,UIColor.black],fonts: [14,16])
        }
    }
    
    //MARK: 设置要跟单的彩种信息筛选的button
    func setupMiddleBtn(btn:UIButton) {
        btn.titleLabel?.textAlignment = .left
        btn.contentHorizontalAlignment = .left
        btn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 0)
        
        btn.layer.borderWidth = 0.5
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.cornerRadius = 3
    }
    
    //MARK: - 定时器
    /**
        * 创建封盘到开奖的倒计时器
        * @param duration
        */
   func createDisableBetCountDownTimer() -> Void {
       self.disableBetCountDownTimer?.invalidate()
       YiboPreference.setAbleBet(value: (self.disableBetTime > 0 ? "off" : "on"))
       
       self.disableBetCountDownTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(disableBetTickDown), userInfo: nil, repeats: true)
       
       if let timer = self.disableBetCountDownTimer {
           RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
       }
   }
       
   @objc func disableBetTickDown() {
       //将剩余时间减少1秒
       self.disableBetTime -= 1
    
       if self.disableBetTime > 0{
           let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.disableBetTime)))
           self.setupOpeningTime(time: dealDuration)
       }else if self.disableBetTime <= 0{
            self.disableBetCountDownTimer?.invalidate()
            self.setupOpeningTime(time: "开盘中")
        
            self.requestPlanList()
            //封盘时间结束后再次获取下一期的倒计时时间
            self.getCountDownByCpCodeAction()
       }
   }
       
   /**
    * 创建停止下注倒计时
    * @param duration
    */
   func createEndBetTimer() -> Void {
       self.disableBet = false
       self.endlineTouzhuTimer?.invalidate()
       self.endlineTouzhuTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(endBetTickDown), userInfo: nil, repeats: true)
       
       if let timer = self.endlineTouzhuTimer {
           RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
       }
   }
       
   @objc func endBetTickDown() {
        //将剩余时间减少1秒
        self.endBetTime -= 1
        
        if self.endBetTime > 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.endBetTime)))
            
            self.setupClosingTime(time: dealDuration)
        }else {
            self.disableBet = true
            YiboPreference.setAbleBet(value: "off")
            self.setupClosingTime(time: "已封盘")
            self.endlineTouzhuTimer?.invalidate()
        }
   }
    
    //MARK: - 数据 && 接口请求
    //MARK: 获取彩种
    
    //根据最新的彩种信息获取对应的玩法数据
    //网络获取
    //说明:当当前彩种变化时都需要调用此方法同步下玩法数据
    func sync_playrule_from_current_lottery(lotType:String,lotCode:String,lotVersion:String,handler:@escaping (_ lotteryData: LotteryData?) -> ()) {
        
        //多玩法投注，记录选择的玩法
        let parameters = ["lotType":lotType,"lotCode":lotCode,"lotVersion":lotVersion] as [String : Any]
        request(frontDialog: true, loadTextStr:"获取玩法中", url:GAME_PLAYS_URL,params:parameters,
                callback: { [weak self] (resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "获取玩法失败"))
                        }else{
                            showToast(view: weakSelf.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = LotPlayWraper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            handler(result.content)
                        }else{
                            weakSelf.print_error_msg(msg: result.msg)
                            if result.code == 0{
                                loginWhenSessionInvalid(controller: weakSelf)
                            }
                        }
                    }
        })
    }
    
    
    //MARK: 请求可以跟单的彩种
    func requestFlanCodes() {
        request(frontDialog: true, method: .post, url: URL_PLAN_CODE, params: [:], callback: {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
                           
               if !resultStatus {
                   if resultJson.isEmpty {
                       showToast(view: weakSelf.view, txt: convertString(string: "获取跟单彩种列表失败"))
                   }else{
                       showToast(view: weakSelf.view, txt: resultJson)
                   }
                
                   return
               }
            
            if let results = [PlanCodeModel].deserialize(from: resultJson) {
                weakSelf.planCodes = results
            }else {
                showToast(view: weakSelf.view, txt: "获取跟单彩种列表失败")
            }
        })
    }
    
    //MARK: 删除单个跟投
    func requestDelFollow(infoId:String) {
        request(frontDialog: true, method: .post, loadTextStr: "跟单删除中...", url: URL_DEL_ONE_FOLLOW, params: ["infoId":infoId], callback: {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
                           
               if !resultStatus {
                   if resultJson.isEmpty {
                       showToast(view: weakSelf.view, txt: convertString(string: "跟单删除求失败"))
                   }else{
                       showToast(view: weakSelf.view, txt: resultJson)
                   }
                
                   return
               }
            
            if let result = CRBaseModel.deserialize(from: resultJson) {
                if result.success {
                    showToast(view: weakSelf.view, txt: "跟单已删除")
                    weakSelf.requestPlanList()
                }else {
                    showToast(view: weakSelf.view, txt: (result.msg.isEmpty ? "跟单删除失败" : result.msg))
                }
            }
        })
    }
    
    //MARK: 某个计划员下的跟投,编辑跟投
    func requestPredictorFollow(infoId:String,followModel:PFFollowModel) {
        request(frontDialog: true, method: .post, loadTextStr: "跟单获取中...", url: URL_FOLLOW_LIST, params: ["infoId":infoId], callback: {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
                           
               if !resultStatus {
                   if resultJson.isEmpty {
                       showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                   }else{
                       showToast(view: weakSelf.view, txt: resultJson)
                   }
                
                   return
               }
            
            if let models = [PredictorFollowModel].deserialize(from: resultJson) {
                //把返回的数据转换成跟投时的本地数据，以直接复用
                var autoPlanItems = [PFAutoPlanItem]()
                for model in models {
                    let item = PFAutoPlanItem()
                    item.money = model!.money
                    item.Q = model!.qiHao
                    item.reverse = model!.isFollow == "1"
                    autoPlanItems.append(item)
                }
                
                weakSelf.showFollowBetPop()
                weakSelf.followBetPop.infoId = infoId
                weakSelf.followBetPop.configWith(models: autoPlanItems, follower: followModel)
            }else {
                showToast(view: weakSelf.view, txt: "获取跟单出错")
            }
            
//            let planner = self.plans[index]
//            if let qihao = Int64(planner.qiHao) {
//                showFollowBetPop()
//                let play = self.oneToTenRules[self.selectRuleIndex]
//                planner.code = play.code
//                self.followBetPop.currentPeriod = qihao
//                self.followBetPop.infoId = planner.infoId
//                self.followBetPop.configPopType(reverseBet: true, isAutoFollow: true, planModel: planner)
//            }
        })
    }
    
    ///自动跟投
    func requestPlanAutoBet(items:[PFAutoPlanItem],infoId:String) {
        
        var itemsDic = [[String:Any]]()
        for model in items {
            let dic = ["Q":model.Q,(model.reverse ? "U" : "F"):model.money]
            itemsDic.append(dic as [String : Any])
        }
        
        if (JSONSerialization.isValidJSONObject(itemsDic)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: itemsDic, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            
            let params = ["plan":str!,"infoId":infoId] as [String:Any]
            request(frontDialog: true, method: .post, loadTextStr: "跟投中...", url: URL_PLAN_AUTO_BET, params: params) {[weak self] (resultJson, resultStatus) in
                guard let weakSelf = self else {return}
                
                weakSelf.hidePopView()
                
                if !resultStatus {
                    if resultJson.isEmpty {
                        showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                    }else{
                        showToast(view: weakSelf.view, txt: resultJson)
                    }
                    return
                }
                
                if let result = CRBaseModel.deserialize(from: resultJson) {
                    if result.success {
                        showToast(view: weakSelf.view, txt: "投注成功")
                        weakSelf.requestPlanList()
                    }else {
                        if !isEmptyString(str: result.msg){
                            showToast(view: weakSelf.view, txt: result.msg)
                        }else{
                            showToast(view: weakSelf.view, txt: convertString(string: "投注失败"))
                        }
                        //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                        //所以此接口当code == 0时表示帐号被踢，或登录超时
                        if (result.code == "0") {
                            loginWhenSessionInvalid(controller: weakSelf)
                            return
                        }
                    }
                }
            }
        }
    }
    
    ///下注接口
    func requestFollowBet(items:[PFPlanItem],reverseBet:Bool) {
        var orderList = [[String:String]]()
        for model in items {
            let playNum = reverseBet ? model.reverseNum : model.playNum
            let dic = ["i":model.code,"a":model.money,"d":(model.code+playNum),"c":playNum]
            orderList.append(dic)
        }

        let postData = ["data":orderList,"kickback":"0","lotCode":lotData.code ?? ""] as [String:Any]
        if (JSONSerialization.isValidJSONObject(postData)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: postData, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            
            request(frontDialog: true, method: .post, loadTextStr: "下注中...", url: DO_PEILVBETS_URL, params: ["data":str!]) {[weak self] (resultJson, resultStatus) in
                guard let weakSelf = self else {return}
                
                weakSelf.hidePopView()
                
                if !resultStatus {
                    if resultJson.isEmpty {
                        showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                    }else{
                        showToast(view: weakSelf.view, txt: resultJson)
                    }
                    return
                }
                
                if let result = DoBetWrapper.deserialize(from: resultJson){
                    if result.success{
                        YiboPreference.setToken(value: result.accessToken as AnyObject)

                        showToast(view: weakSelf.view, txt: "下注成功")
                        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
                        
                    }else{
                        if !isEmptyString(str: result.msg){
                            showToast(view: weakSelf.view, txt: result.msg)
                        }else{
                            showToast(view: weakSelf.view, txt: convertString(string: "下注失败"))
                        }
                        //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                        //所以此接口当code == 0时表示帐号被踢，或登录超时
                        if (result.code == 0) {
                            loginWhenSessionInvalid(controller: weakSelf)
                            return
                        }
                    }
                }
            }
        }
    }
    
    //MARK: 封盘时间结束后再次获取下一期的倒计时时间
    private func getCountDownByCpCodeAction(shouldReStartDisableTimer:Bool = true) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            if self.viewShowing {
                self.getCountDownByCpCodeAction()
            }
        }
        
        self.getCountDownByCpcode(bianHao: (self.lotData.code ?? ""), lotVersion: "\(self.lotData.lotVersion)", controller: self, shouldReStartDisableTimer:shouldReStartDisableTimer,failureHandler:
            { (currentQiHaoP,countDownP) -> Void in

        }, successHandler:
            { (dealDuration) -> Void in
        })
    }
    
    //MARK: 专家排名，已跟专家list接口
    func requestPlanList() {
        if oneToTenRules.count == 0 {
            return
        }
        
        let code = self.code
        let playCode = self.oneToTenRules[self.selectRuleIndex].code
        let ballNum = self.ballNums[self.selectBallNumIndex]
        let params = ["code":code,"playCode":playCode,"ballNum":ballNum]
        
        print("debug 获取跟单列表参数: \(params.description)")
        
        request(frontDialog: true, url: URL_PLANLIST, params: params) {[weak self] (resultJson, resultStatus) in
            
            print("debug 获取跟单列表结果: \(resultJson)")

            guard let weakSelf = self else {return}
            
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                }else{
                    showToast(view: weakSelf.view, txt: resultJson)
                }
                return
            }
            
            if let result = PlanFollowBetWraper.deserialize(from: resultJson) {
                if result.success{
                    weakSelf.plans = result.plan
                    weakSelf.follows = result.follow
                    
                    if weakSelf.viewType == 0 {
                        weakSelf.tableView.tableViewDisplayWithMessage(datasCount: weakSelf.plans.count, tableView: weakSelf.tableView,message: "暂无任何专家排名")
                        
                    }else if weakSelf.viewType == 1 {
                        weakSelf.tableView.tableViewDisplayWithMessage(datasCount: weakSelf.follows.count, tableView: weakSelf.tableView,message: "暂无任何跟单计划")
                    }
                    
                    weakSelf.tableView.reloadData()
                }else{
                    if !isEmptyString(str: result.msg){
                        showToast(view: weakSelf.view, txt: result.msg)
                    }else{
                        showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                    }
                }
            }
        }
    }
    
    
    /// 跟投
    /// - Parameter reverse: true: 反投
    func requestFollowPlan(reverse:Bool) {
        request(frontDialog: true, url: URL_PLANLIST, params: [:]) {[weak self] (resultJson, resultStatus) in

            guard let weakSelf = self else {return}
            
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                }else{
                    showToast(view: weakSelf.view, txt: resultJson)
                }
                return
            }
        }
    }
    
    ///获取帐号信息
    func accountWeb(handler:@escaping (_ leftMoney: String) -> ()) {
        request(frontDialog: false, url:MEMINFO_URL,
                callback: {[weak self](resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        return
                    }
                    if let result = MemInfoWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let memInfo = result.content{
                                //更新余额等信息
                                weakSelf.updateAccount(memInfo:memInfo, handler: {(content) in
                                        handler(content)
                                    }
                                )
                            }
                        }
                    }
        })
    }

    func updateAccount(memInfo:Meminfo,handler:(_ leftMoneyP: String) -> ()) {
        self.meminfo = memInfo
        if !isEmptyString(str: memInfo.balance){
            let leftMoneyName = "\(memInfo.balance)"

            handler(leftMoneyName)
            
            if let value = Double(leftMoneyName) {
                accountBanlance = value
            }
        }
    }

    /**
     * 开始获取当前期号离结束投注倒计时
     * @param bianHao 彩种编号
     * @param lotVersion 彩票版本
     */
    //MARK: 当前期号离结束投注倒计时
    func getCountDownByCpcode(bianHao:String,lotVersion:String,controller:BaseController,shouldReStartDisableTimer:Bool,failureHandler:@escaping (_ currentQiHao:String,_ countDown:String) -> (),successHandler: @escaping (_ dealDurationOutP: String) -> ()) {
        
        request(frontDialog: false, url:LOTTERY_COUNTDOWN_URL,params:["lotCode":bianHao,"version":lotVersion],
                callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                    
                    guard let weakSelf = self else {return}
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "获取当前期号失败"))
                        }else{
                            showToast(view: weakSelf.view, txt: resultJson)
                        }
                        
                        return
                    }
                    if let result = LocCountDownWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            //更新当前这期离结束投注的倒计时显示
                            if let value = result.content{
                                weakSelf.updateCurrenQihaoCountDown(countDown: value,shouldReStartDisableTimer:shouldReStartDisableTimer,successHandler:
                                    {(dealDurationInP) in
                                })
                            }
                        }
                    }
        })
    }
    
    ///剩余可投注注数
    func remainPeriod(periodSeconds:Int,endBetTime:Int) {
        let remainSeconds = todayRemainSeconds() //今日剩余秒
        let remainExceptCurrent = remainSeconds - endBetTime //今天剩余时间 减去本期时间
        let remainCount = remainExceptCurrent / periodSeconds //剩余可投注注数
        self.remainCount = remainCount
        self.followBetPop.remainCount = remainCount
    }
    
    func updateCurrenQihaoCountDown(countDown:CountDown,shouldReStartDisableTimer:Bool,successHandler:(_ dealDurationP:String) -> ()) -> Void {
            //创建开奖周期倒计时器
            let serverTime = countDown.serverTime;
            let activeTime = countDown.activeTime;
            let value = abs(activeTime) - abs(serverTime)
            let preStartTime = countDown.preStartTime
            self.endBetDuration = Int(abs(value))/1000
        
        //一期所需要的时间 单位秒
        let onePeriodSeconds = abs(countDown.nextStartTime - countDown.preStartTime) / 1000
        if onePeriodSeconds > 0 {
            remainPeriod(periodSeconds:Int(onePeriodSeconds), endBetTime: endBetDuration)
        }

    //        if self.disableBetTime == 0 {
                self.endBetTime = self.endBetDuration
    //        }
            
            self.currentQihao = countDown.qiHao
            let period = trimQihao(currentQihao: self.currentQihao)
            self.setupPeriod(period: period)
        
//            let dealDuration = getFormatTime(secounds: TimeInterval(Int64(self.endBetDuration)))

            if  preStartTime - serverTime < 0{
                //封盘时间
                self.createEndBetTimer()
                
                //开盘剩余时间
                self.disableBetTime = Int64(self.endBetDuration) + ago
                self.createDisableBetCountDownTimer()
            }else if shouldReStartDisableTimer {
                //封盘
                let agoTime = preStartTime - serverTime
                if agoTime < ago * 1000 {
                    self.disableBetTime = agoTime / 1000
                    self.createDisableBetCountDownTimer()
                    
                    self.setupClosingTime(time: "已封盘")
                }else {
                    self.disableBetTime = agoTime / 1000
                    self.createDisableBetCountDownTimer()
                    
                    self.setupClosingTime(time: "已封盘")
                }
            }
        }
        
    //MARK: - Events
    @objc func clickBgView() {
        hidePopView()
    }
    
    func showGrayBgView() {
        view.addSubview(self.bgView)
    }
    
    @objc override func onBackClick() {
        super.onBackClick()
        releaseData()
    }
    
    @objc func segValueChangeAction(sender:UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        viewType = index
        stackConsH.constant = index == 0 ? 150 : 100
        if viewType == 0 {
            tableView.tableViewDisplayWithMessage(datasCount: plans.count, tableView: tableView,message: "暂无任何专家排名")
        }else if viewType == 1 {
            tableView.tableViewDisplayWithMessage(datasCount: follows.count, tableView: tableView,message: "暂无任何跟单计划")
        }
        
        self.tableView.reloadData()
    }
    
    @objc func tapMiddleLeftBtn(sender:UIButton) {
        self.playRuleType = 0
        setupPopMenu(tapView: sender)
    }
    
    @objc func tapMiddleMiddleBtn(sender:UIButton) {
        self.playRuleType = 1
        setupPopMenu(tapView: sender)
    }
    
    ///显示跟投 pop,isAuto是否是自动跟投
    func showFollowBetPop() {
        self.accountWeb { _ in}
        
        showGrayBgView()
        view.addSubview(self.followBetPop)
        
        self.followBetPop.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalTo(ScreenWidth*0.95)
        }
    }
    
    func hidePopView() {// 需要隐藏时动画优化
        self.followBetPop.resetAction()
        self.followBetPop.removeFromSuperview()
        self.bgView.removeFromSuperview()
    }
}

//MARK: - UITableViewDataSource
extension PlanFollowBetController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewType == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlanFollowBetCell") as? PlanFollowBetCell else {
                fatalError("dequeueReusableCell PlanFollowBetCell fail")
            }
            
            cell.selectionStyle = .none
            cell.planDelegate = self
            cell.index = indexPath.row
            cell.gameName = self.title ?? ""
            cell.cpVersion = "\(self.lotData.lotVersion)"
            cell.lotTypeCode = self.lotData.czCode ?? ""
            
            let model = plans[indexPath.row]
            cell.configPlanModel(model: model,disableBet: self.disableBet)
            
            return cell
        }else if self.viewType == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowPredictorCell") as? FollowPredictorCell else {
                fatalError("dequeueReusableCell FollowPredictorCell fail")
            }
            
            cell.selectionStyle = .none
            cell.followDelegate = self
            cell.index = indexPath.row
            cell.gameName = self.title ?? ""
            cell.cpVersion = "\(self.lotData.lotVersion)"
            cell.lotTypeCode = self.lotData.czCode ?? ""
            
            let model = self.follows[indexPath.row]
            cell.configPlanModel(model: model)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewType == 0 ? plans.count : follows.count
    }
}

//MARK: - UITableViewDelegate
extension PlanFollowBetController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.viewType == 0 {
            let play = self.oneToTenRules[self.selectRuleIndex]
            let model = self.plans[indexPath.row]
            model.code = play.code
            
            let vc = PlanFollowDetailController()
            vc.honestResult = play
            vc.cpVersion = "\(self.lotData.lotVersion)"
            vc.lotTypeCode = self.lotData.czCode ?? ""
            vc.plansModel = model
            vc.infoId = model.infoId
//            vc.oneToTenRules = self.oneToTenRules
            vc.disableBet = self.disableBet
            vc.lotData = self.lotData
            vc.remainCount = self.remainCount
            vc.title = self.title
            self.navigationController?.pushViewController(vc, animated: true)
        }else if self.viewType == 1 {
//            cell.gameName = self.title ?? ""
//            cell.cpVersion = "\(self.lotData.lotVersion)"
//            cell.lotTypeCode = self.lotData.czCode ?? ""
//
//            let model = self.follows[indexPath.row]
//            cell.configPlanModel(model: model)
            
            let vc = FollowPreDetailController()
            vc.title = self.title
            vc.infoId = follows[indexPath.row].infoId
            vc.followModel = follows[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:FollowPredictorDelegate
extension PlanFollowBetController: FollowPredictorDelegate  {
    func editFollowBet(index: Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        showFollowBetPop()
        let planner = self.follows[index]
        //请求已经跟投了的数据
        requestPredictorFollow(infoId: planner.infoId,followModel:planner)
    }
    
    func delFollowBet(index: Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        let follow = self.follows[index]
        requestDelFollow(infoId: follow.infoId)
    }
}

//MARK: PlanFollowBetDelegate
extension PlanFollowBetController: PlanFollowBetDelegate {
    ///跟投
    func followBet(index:Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        showFollowBetPop()
        
        let play = self.oneToTenRules[self.selectRuleIndex]
        let planner = self.plans[index]
        planner.code = play.code
        self.followBetPop.honestResult = play 
        self.followBetPop.configPopType(reverseBet:false,isAutoFollow: false, planModel: planner)
    }
    
    ///反投
    func counterFollowBet(index:Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        showFollowBetPop()
        let play = self.oneToTenRules[self.selectRuleIndex]
        let planner = self.plans[index]
        planner.code = play.code
        self.followBetPop.honestResult = play
        self.followBetPop.configPopType(reverseBet:true,isAutoFollow: false, planModel: planner)
    }
    
    ///自动跟投
    func autoFollowBet(index:Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        let planner = self.plans[index]
        if let qihao = Int64(planner.qiHao) {
            showFollowBetPop()
            let play = self.oneToTenRules[self.selectRuleIndex]
            planner.code = play.code
            self.followBetPop.currentPeriod = qihao
            self.followBetPop.infoId = planner.infoId
            self.followBetPop.configPopType(reverseBet: false, isAutoFollow: true, planModel: planner)
        }
    }
}


class PlanCodeModel: HandyJSON {
    var code = ""
    var name = ""
    required init() {}
}
