//
//  PlanPreHistoryModel.swift
//  gameplay
//
//  Created by admin on 2020/9/11.
//  Copyright © 2020 yibo. All rights reserved.
//  历史预测列表数据

import UIKit
import HandyJSON

class PlanPreHistoryModel: HandyJSON {
    var haoMa = "" //预测的号码 //: "10,09,03,02,08",
    var infoId = "" //
    var playName = "" //: "亚军",
    var predictorName = ""//专家名字
    var qiHao = ""//期号
    ///1:跟单中2:跟单成功3:跟单失败
    var status = ""
    var winHaoMa = "" //中奖的号码
    required init() {}
}
