//
//  PredictorFollowModel.swift
//  gameplay
//
//  Created by admin on 2020/9/10.
//  Copyright © 2020 yibo. All rights reserved.
//  某个专家下的已跟计划，用于编辑跟投

import UIKit
import HandyJSON

class PredictorFollowModel: HandyJSON {
    /// 1反投，2正投
    var isFollow = ""
    var money = ""
    var qiHao = ""
    var status = "" //1:跟单中2:跟单成功3:跟单失败
    
    required init() {}
}
