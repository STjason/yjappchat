//
//  PlanFollowBetCell.swift
//  gameplay
//
//  Created by admin on 2020/8/23.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

protocol FollowPredictorDelegate:NSObjectProtocol {
    func editFollowBet(index:Int)//编辑跟投
    func delFollowBet(index:Int) //删除跟投
}

class FollowPredictorCell: UITableViewCell {
    
    @IBOutlet weak var moreInfoImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var topBgView: UIView!
    @IBOutlet weak var bottomBgView: UIView!
    @IBOutlet weak var topLeftLabel: UILabel! //计划员
    @IBOutlet weak var gameNameLabel: UILabel! //游戏名称
    @IBOutlet weak var planTypeLabel: UILabel! //计划类型
    @IBOutlet weak var ballNumLabel: UILabel! //球号 冠亚军
    @IBOutlet weak var remainFollowLabel: UILabel! //剩余跟投
    @IBOutlet weak var editFollowBtn: UIButton!
    @IBOutlet weak var delFollowBtn: UIButton! //删除跟投
    
    weak var followDelegate:FollowPredictorDelegate?
    let tagBase = 10000 //跟单tag
    var index = 0 //cell's index
    var planModel = PFPlanModel() //专家排名
    var followModel = PFPlanModel() //已跟专家
    var gameName = "" //游戏名字
    var lotTypeCode = ""//bjsc
    var cpVersion = "" //1官 2信
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self.bgView)
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        bgView.layer.cornerRadius = 10
        setupButton(btns: [editFollowBtn,delFollowBtn])
    }
    
    func setupButton(btns:[UIButton]) {
        for (index, btn) in btns.enumerated() {
            btn.tag = tagBase + index
            btn.addTarget(self, action: #selector(tapFollowBtn), for: .touchUpInside)
            btn.layer.cornerRadius = 10
            btn.layer.borderWidth = 1
            btn.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    func configPlanModel(model:PFFollowModel) {
        editFollowBtn.backgroundColor = UIColor.white
        delFollowBtn.backgroundColor = UIColor.white
         
        topLeftLabel.text = model.predictorName //计划员名字
        gameNameLabel.text = "游戏：\(self.gameName)"
        planTypeLabel.text = "计划类型：\(model.ballNum)码"
        ballNumLabel.text = "球号：\(model.playName)"
        remainFollowLabel.text = "剩余跟投：\(model.followNum)"
    }
    
    @objc func tapFollowBtn(sender:UIButton)  {
        if let delegate = followDelegate {
            let tag = sender.tag - tagBase
            if tag == 0 {
                delegate.editFollowBet(index:index)
            }else if tag == 1 {
                delegate.delFollowBet(index:index)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
