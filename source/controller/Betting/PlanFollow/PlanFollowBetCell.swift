//
//  PlanFollowBetCell.swift
//  gameplay
//
//  Created by admin on 2020/8/23.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

protocol PlanFollowBetDelegate:NSObjectProtocol {
    func followBet(index:Int) //跟投
    func counterFollowBet(index:Int)//反投
    func autoFollowBet(index:Int) //自动跟投
}

class PlanFollowBetCell: UITableViewCell {
    
    @IBOutlet weak var moreInfoImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var topBgView: UIView!
    @IBOutlet weak var bottomBgView: UIView!
    @IBOutlet weak var topLeftLabel: UILabel! //计划员
    @IBOutlet weak var topMiddleLabel: UILabel! //胜率
    @IBOutlet weak var topRightLabel: UILabel! //盈亏
    @IBOutlet weak var gameNameLabel: UILabel! //游戏名称
    @IBOutlet weak var planTypeLabel: UILabel! //计划类型
    @IBOutlet weak var ballNumLabel: UILabel! //球号 冠亚军
    @IBOutlet weak var dragonLabel: UILabel! //长龙
    @IBOutlet weak var followBtn: UIButton! //跟投
    @IBOutlet weak var counterFollowBtn: UIButton! //反投
    @IBOutlet weak var autoFollowBtn: UIButton! //自动跟投
    @IBOutlet weak var ballsView: BallsView!
    
    weak var planDelegate:PlanFollowBetDelegate?
    let tagBase = 10000 //跟单tag
    var index = 0 //cell's index
    var planModel = PFPlanModel() //专家排名
    var followModel = PFPlanModel() //已跟专家
    var gameName = "" //游戏名字
    var lotTypeCode = ""//bjsc
    var cpVersion = "" //1官 2信
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupNoPictureAlphaBgView(view: self.bgView)
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        bgView.layer.cornerRadius = 10
        setupButton(btns: [followBtn,counterFollowBtn,autoFollowBtn])
    }
    
    func setupButton(btns:[UIButton]) {
        for (index, btn) in btns.enumerated() {
            btn.tag = tagBase + index
            btn.addTarget(self, action: #selector(tapFollowBtn), for: .touchUpInside)
            btn.layer.cornerRadius = 10
            btn.layer.borderWidth = 1
            btn.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
        
    func configPlanModel(model:PFPlanModel,disableBet:Bool) {
        followBtn.backgroundColor = disableBet ? UIColor.groupTableViewBackground : UIColor.white
        counterFollowBtn.backgroundColor = disableBet ? UIColor.groupTableViewBackground : UIColor.white
        autoFollowBtn.backgroundColor = disableBet ? UIColor.groupTableViewBackground : UIColor.white
        
        //胜率 100%,
        let winRate = "胜率：\(model.winRate)%"
        let winRateTitleRange = NSRange.init(location: 0, length: 3)
        let winRateValueRange = NSRange.init(location: winRateTitleRange.length, length: (winRate.length - winRateTitleRange.length))
        
        topMiddleLabel.setAttributeString(text: winRate, ranges: [winRateTitleRange,winRateValueRange], colors: [UIColor.black,UIColor.init(hexString: "#388ec5")!],fonts: nil)
        
        //盈亏 2
//        let profitLoss = "盈亏：¥\(2)"
        let profitLoss = ""
        if !profitLoss.isEmpty {
            let profitLossTitleRange = NSRange.init(location: 0, length: 3)
            let profitLossValueRange = NSRange.init(location: profitLossTitleRange.length, length: (profitLoss.length - profitLossTitleRange.length))

            topRightLabel.setAttributeString(text: profitLoss, ranges: [profitLossTitleRange,profitLossValueRange], colors: [UIColor.black,UIColor.red],fonts: nil)
        }else {
            topRightLabel.text = ""
        }
        
        topLeftLabel.text = model.predictorName //计划员名字
        gameNameLabel.text = "游戏：\(self.gameName)"
        planTypeLabel.text = "计划类型：\(model.ballNum)码"
        ballNumLabel.text = "球号：\(model.playName)"
        dragonLabel.text = "长龙：\(model.cl)"
        
        let nums = model.haoMa.components(separatedBy: ",")
        ballsView.basicSetupBalls(nums: nums, offset: 2, lotTypeCode: self.lotTypeCode, cpVersion: self.cpVersion, ballsViewWidth: kScreenWidth*0.7)
        
    }
    
    @objc func tapFollowBtn(sender:UIButton)  {
        if let delegate = planDelegate {
            let tag = sender.tag - tagBase
            if tag == 0 {
                delegate.followBet(index:index)
            }else if tag == 1 {
                delegate.counterFollowBet(index:index)
            }else if tag == 2 {
                delegate.autoFollowBet(index:index)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
