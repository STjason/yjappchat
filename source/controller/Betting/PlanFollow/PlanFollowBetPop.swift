//
//  PlanFollowBetPop.swift
//  gameplay
//
//  Created by admin on 2020/8/30.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class PlanFollowBetPop: UIView, UITextFieldDelegate {
    @IBOutlet weak var reverseFollowAllLabel: UILabel!
    @IBOutlet weak var followAllLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var planerNameLabel: UILabel!
    @IBOutlet weak var topTitleView: UIView! //弹窗titleView
    @IBOutlet weak var autoFollowTopBottom: UIView! //自动跟投的顶部的bottmView
    @IBOutlet weak var followTopBottom: UIView!//跟投的顶部的bottmView
    @IBOutlet weak var autoBottomTop: UIView! //自动跟投的底部的topView
    @IBOutlet weak var bottomTop: UIView!//跟投的底部的topView
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var resetButton: UIButton! //重置
    @IBOutlet weak var betButton: UIButton! //投注
    @IBOutlet weak var increaseMoneyButton: UIButton! //金额递增
    @IBOutlet weak var reduceMoneyButton: UIButton! //金额递减
    @IBOutlet weak var betMoneyField: CustomFeildText! //投注金额
    @IBOutlet weak var periodsNumField: CustomFeildText! //期数递增
    @IBOutlet weak var reducePeriodsBtn: UIButton! //期数递减
    @IBOutlet weak var increasePeriodsBtn: UIButton! //期数递增
    @IBOutlet weak var periods100Btn: UIButton! //100期
    @IBOutlet weak var periods50Btn: UIButton! //50期
    @IBOutlet weak var periods10Btn: UIButton! //10期
    @IBOutlet weak var reverseFollowSwitch: UISwitch! //全反 / 全跟
    @IBOutlet weak var selectBetsNumLabel: UILabel! //跟投 已选注数
    @IBOutlet weak var balanceLabel: UILabel!
    
    var dismissHandler:(() -> Void)? //点击右上角关闭pop按钮
    var planBetHandler:((_ items:[PFPlanItem],_ reverseBet:Bool) -> Void)? //投注
    var planAutoBetHandler:((_ items:[PFAutoPlanItem],_ infoId:String) -> Void)? //投注
    var isAutoFollow:Bool = false //false 计划跟投，true 自动计划跟投
    let identifierPlanFollow = "PlanFollowPopCell"
    let identifierPlanFollowAuto = "PlanFollowAutoPopCell"
//    var playRule = BcLotteryPlay() //玩法
    var planModel = PFPlanModel() //计划详情
    var cellHeight:CGFloat = 50
    var reverseBet = false
    var currentPeriod:Int64 = 0//当前期号
    var infoId = "" //计划列表的id
    var baseTag = 10000
    var honestResult = HonestResult() //赔率数据和玩法
    var firset = false //是否是第一次设置 自动跟投期数
    var editAutoPlan = false //是否是编辑跟投
    var remainCount = 0  //剩余跟投期数
    var defaultFollowNums = 10
    var tableScale:CGFloat = 0.5
    
    var autoFollowNums = 10 { //自动跟投期数
        didSet {
            
            if remainCount < autoFollowNums && !firset {
                periodsNumField.text = "\(remainCount)"
                autoFollowNums = remainCount
            }
            
            if editAutoPlan && firset {
                firset = false
                editAutoPlan = false
            }else {
                let count = firset ? autoFollowNums : (autoFollowNums - oldValue)
                
                if firset {
                    firset = false
                }
                
                //当可以下注期数小于 默认value 10的时候
                // count > 0 新增,否则删除
                updateAutoFollowModels(count: count, append:count > 0 ? true : false)
            }
                  
            if autoFollowNums != 10 && autoFollowNums != 50 && autoFollowNums != 100 {
                setupBtn(button: periods10Btn)
                setupBtn(button: periods50Btn)
                setupBtn(button: periods100Btn)
            }else if autoFollowNums == 10 {
                setBtnsSelected(selected:periods10Btn)
            }else if autoFollowNums == 50 {
                setBtnsSelected(selected:periods50Btn)
            }else if autoFollowNums == 100 {
                setBtnsSelected(selected:periods100Btn)
            }
        }
    }
    
    var items = [PFPlanItem]()  { //跟单列表数据
        didSet {
            if !reverseBet {
                selectBetsNumLabel.text = "已选\(items.count)注"
                
                let count = CGFloat(self.items.count)
                var height:CGFloat = cellHeight * count
                if height > screenHeight * tableScale {
                    height = screenHeight * tableScale
                }
                
                tableView.snp.updateConstraints { (make) in
                    make.height.equalTo(height)
                }
                
                tableView.reloadData()
            }
        }
    }
    
    var reverseItems = [PFPlanItem]()  { //跟单列表数据
        didSet {
            if reverseBet {
                selectBetsNumLabel.text = "已选\(reverseItems.count)注"
                
                let count = CGFloat(self.reverseItems.count)
                var height:CGFloat = cellHeight * count
                if height > screenHeight * tableScale {
                    height = screenHeight * tableScale
                }
                
                tableView.snp.updateConstraints { (make) in
                    make.height.equalTo(height)
                }
                
                tableView.reloadData()
            }
        }
    }
    
    var autoFollowItems = [PFAutoPlanItem]() //自动跟投数据
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        reverseFollowAllLabel.theme_textColor = "Global.themeColor"
        followAllLabel.theme_textColor = "Global.themeColor"
        
        topTitleView.theme_backgroundColor = "Global.themeColor"
        setupTableView()
        betButton.addTarget(self, action: #selector(betAction), for: .touchUpInside)
        resetButton.addTarget(self, action: #selector(resetAction), for: .touchUpInside)
        reverseFollowSwitch.addTarget(self, action: #selector(switchValueChange)
            , for: .valueChanged)
        reducePeriodsBtn.addTarget(self, action: #selector(reducePeriodsAcction), for: .touchUpInside)
        increasePeriodsBtn.addTarget(self, action: #selector(increasePeriodsAction), for: .touchUpInside)
        increaseMoneyButton.addTarget(self, action: #selector(increaseMoneyAction), for: .touchUpInside)
        reduceMoneyButton.addTarget(self, action: #selector(reduceMoneyAction), for: .touchUpInside)
        
//        betMoneyField.addTarget(self, action: #selector(betMoneyFieldValueChaned), for: .editingChanged)
//        periodsNumField.addTarget(self, action: #selector(periodsFieldValueChaned), for: .editingChanged)
        betMoneyField.tag = baseTag + 1
        periodsNumField.tag = baseTag + 2
        betMoneyField.delegate = self
        periodsNumField.delegate = self
        
        self.betButton.layer.cornerRadius = 5
        self.resetButton.layer.cornerRadius = 5
        
        self.periods10Btn.tag = baseTag + 10
        self.periods50Btn.tag = baseTag + 50
        self.periods100Btn.tag = baseTag + 100
        setupBtn(button: self.periods10Btn)
        setupBtn(button: self.periods50Btn)
        setupBtn(button: self.periods100Btn)
        self.periods10Btn.theme_backgroundColor = "Global.themeColor"
        self.periods10Btn.setTitleColor(UIColor.white, for: .normal)
    }
    
    //设置10，50，100期数的button
    private func setupBtn(button:UIButton) {
        button.layer.cornerRadius = 5
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 0.5
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(clickFastPeriodAction), for: .touchUpInside)
    }
    
    @objc func clickFastPeriodAction(sender:UIButton) {
        let tag = sender.tag - baseTag
        
        if remainCount < tag {
            self.periodsNumField.text = "\(tag)"
            self.autoFollowNums = tag
            showToast(view: self, txt: "最多可投注\(remainCount)期")
        }else {
            setBtnsSelected(selected: sender)
            self.periodsNumField.text = "\(tag)"
            self.autoFollowNums = tag
        }
    }
    
    func setBtnsSelected(selected:UIButton) {
        for btn in [self.periods10Btn,self.periods50Btn,self.periods100Btn] {
            if selected == btn {
                selected.setTitleColor(UIColor.white, for: .normal)
                selected.theme_backgroundColor = "Global.themeColor"
            }else {
                btn?.setTitleColor(UIColor.darkGray, for: .normal)
                btn?.backgroundColor = UIColor.white
            }
        }
    }
    
    //    ///底部bar投注金额变化
    //    @objc func betMoneyFieldValueChaned(textField:CustomFeildText) {
    //        if let value = Double(textField.text ?? "") {
    //            textField.text = value.cleanZero
    //        }
    //    }
    //
    //    ///底部bar投注期数变化
    //    @objc func periodsFieldValueChaned(textField:CustomFeildText) {
    //
    //    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == baseTag + 1 { //金额
            if let value = Double(textField.text ?? "") {
                textField.text = value.cleanZero
                
                self.changePlanBetMoney(value: "\(value.cleanZero)")
            }else {
                showToast(view: self, txt: "请输入正确金额")
            }
        }else if textField.tag == baseTag + 2 { //期号
            if let value = Int(textField.text ?? "") {
                textField.text = "\(value)"
                autoFollowNums = value
            }else {
                showToast(view: self, txt: "请输入整数")
            }
        }
    }
    
    ///修改跟投/自动跟投的金额
    func changePlanBetMoney(value:String) {
        if isAutoFollow {
            for model in autoFollowItems {
                model.money = value
            }
        }else {
            if self.reverseBet {
                for model in reverseItems {
                    model.money = value
                }
            }else {
                for model in items {
                    model.money = value
                }
            }
        }

        self.tableView.reloadData()
    }
    
    ///期数递减
    @objc func reducePeriodsAcction() {
        reduceValue(textField: self.periodsNumField)
    }
    
    ///期数递增
    @objc func increasePeriodsAction() {
        increaseValue(textField: self.periodsNumField)
    }
    
    ///金额递增
    @objc func increaseMoneyAction() {
        increaseValue(textField: self.betMoneyField)
    }
    
    ///金额递减
    @objc func reduceMoneyAction(textField:CustomFeildText) {
        reduceValue(textField: self.betMoneyField)
    }
    
    func reduceValue(textField:CustomFeildText) {
        if let value = Int(textField.text ?? "") {
            if value <= 1 {
                textField.text = ""
            }else {
                textField.text = "\(value - 1)"
            }
        }else {
            textField.text = ""
        }
        
        if textField == self.betMoneyField {
            self.changePlanBetMoney(value: textField.text ?? "")
        }else if textField == self.periodsNumField {
            if let value = Int(textField.text ?? "") {
                self.autoFollowNums = value
            }else {
                showToast(view: self, txt: "请输入整数")
            }
        }
    }
    
    func increaseValue(textField:CustomFeildText) {
        if let value = Int(textField.text ?? "") {
            if value < 0 {
                textField.text = "1"
            }else {
                textField.text = "\(value + 1)"
            }
        }else {
            textField.text = "1"
        }
        
        if textField == self.betMoneyField {
            self.changePlanBetMoney(value: textField.text ?? "")
        }else if textField == self.periodsNumField {
           if let value = Int(textField.text ?? "") {
            
            if remainCount < value {
                self.autoFollowNums = remainCount
                showToast(view: self, txt: "最多可投注\(remainCount)期")
            }else {
                self.autoFollowNums = value
            }
            
           }else {
               showToast(view: self, txt: "请输入整数")
           }
        }
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        let planFollowCellNib = UINib.init(nibName: "PlanFollowPopCell", bundle: nil)
        let planFollowAutoCellNib = UINib.init(nibName: "PlanFollowAutoPopCell", bundle: nil)
        tableView.register(planFollowCellNib, forCellReuseIdentifier: identifierPlanFollow)
        tableView.register(planFollowAutoCellNib, forCellReuseIdentifier: identifierPlanFollowAuto)
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
    }
    
    func updateAutoFollowTableHeight() {
        let count = CGFloat(self.autoFollowItems.count)
        var height:CGFloat = cellHeight * count
        if height > screenHeight * tableScale {
            height = screenHeight * tableScale
        }
        
        tableView.snp.updateConstraints { (make) in
            periodsNumField.text = "\(autoFollowItems.count)"
            make.height.equalTo(height)
        }
        
        self.tableView.reloadData()
    }
    
    func configDisableBet(disable:Bool) {
        if disable {
            self.betButton.backgroundColor = UIColor.lightGray
        }else {
            self.betButton.theme_backgroundColor = "Global.themeColor"
        }
        
        self.betButton.isUserInteractionEnabled = !disable
    }
    
    //MARK: 格式化编辑跟投的数据
    func configWith(models:[PFAutoPlanItem],follower:PFFollowModel) {
        self.isAutoFollow = true
        self.reverseBet = false
        self.planerNameLabel.text = follower.predictorName
        autoFollowTopBottom.isHidden = !isAutoFollow
        autoBottomTop.isHidden = !isAutoFollow
        followTopBottom.isHidden = isAutoFollow
        bottomTop.isHidden = isAutoFollow
        
        items.removeAll()
        reverseItems.removeAll()
        autoFollowItems.removeAll()
        
        self.firset = true
        self.editAutoPlan = true
        
        self.reverseFollowSwitch.isOn = true
        self.autoFollowItems = models
        
        self.autoFollowNums = models.count
        self.updateAutoFollowTableHeight()
        
        for model in self.autoFollowItems {
            if model.reverse {
                reverseFollowSwitch.isOn = false
                return
            }
        }
    }
    
    //动态更新自动跟单数据,count需要新增的自动跟单数,append true新增 否则 删除
    func updateAutoFollowModels(count:Int,append:Bool) {
        if append {
            
            var appendModels = [PFAutoPlanItem]()
            let lastPeriod = self.autoFollowItems.count == 0 ? currentPeriod : Int64(self.autoFollowItems.last!.Q)!
            
            for index in 1...count {
                let model = PFAutoPlanItem()
                model.F = ""
                model.U = ""
                model.Q = "\(lastPeriod + Int64(index))"
                model.reverse = !reverseFollowSwitch.isOn
                
                if let value = Float(betMoneyField.text ?? "") {
                    model.money = value.cleanZero
                }
                
                appendModels.append(model)
            }
            
            self.autoFollowItems.append(contentsOf: appendModels)
        }else {
            let delCount = count <= self.autoFollowItems.count ? count : self.autoFollowItems.count
            self.autoFollowItems.removeLast(delCount * -1)
        }
        
        self.updateAutoFollowTableHeight()
    }
    
    /// 格式化投注/自动跟投 数据
    /// - Parameter isAutoFollow: ture：是
    func configPopType(reverseBet:Bool,isAutoFollow:Bool,planModel:PFPlanModel) {
        self.isAutoFollow = isAutoFollow
        self.reverseBet = reverseBet
        self.planerNameLabel.text = planModel.predictorName
        autoFollowTopBottom.isHidden = !isAutoFollow
        autoBottomTop.isHidden = !isAutoFollow
        followTopBottom.isHidden = isAutoFollow
        bottomTop.isHidden = isAutoFollow
        
        items.removeAll()
        reverseItems.removeAll()
        autoFollowItems.removeAll()
        
        if isAutoFollow {
            self.firset = true
            self.reverseFollowSwitch.isOn = true
            if remainCount < defaultFollowNums {
                autoFollowNums = remainCount
            }else {
                autoFollowNums = 10
            }
//            self.updateAutoFollowModels(count: 10, append: true)
        }else {
            var numsAll = [String]() // 1到10的数组
            for index in 1...10 {
                numsAll.append("\(index)")
            }
            
            let nums = planModel.haoMa.components(separatedBy: ",") + [String]()
            var delZeroNums = [String]() // 预测的数组（头部不含0）
            var reverseNums = [String]() // 取反预测数组

            for num in numsAll {
                if (nums.contains(num) || nums.contains("0\(num)") ) {
                    delZeroNums.append(num)
                }else {
                    reverseNums.append(num)
                }
            }
            
            //格式化预测数组，取反数组
            for (_,num) in delZeroNums.enumerated() {
                let item = PFPlanItem()
                item.playRule = planModel.playName
                item.playNum = num
                item.money = "10"
                item.code = planModel.code
                item.peilv = getpeilvWithCodeAndNum(num: num)
                
                items.append(item)
            }
            
            for (_,num) in reverseNums.enumerated() {
                let item = PFPlanItem()
                item.playRule = planModel.playName
                item.reverseNum = num
                item.money = "10"
                item.code = planModel.code
                item.peilv = getpeilvWithCodeAndNum(num: num)
                
                reverseItems.append(item)
            }
        }
    }
    
    //MARK: 根据code和号码获得赔率
    func getpeilvWithCodeAndNum(num:String) -> String {
        for peilv in self.honestResult.odds {
            if peilv.numName == num {
                return "\(peilv.maxOdds.cleanZero)"
            }
        }
        
        return ""
    }
    
    //MARK: - EVENTS
    @objc func switchValueChange(sender:UISwitch) {
        for model in self.autoFollowItems {
            model.reverse = !sender.isOn
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func closePopAction(_ sender: Any) {
        dismissHandler?()
    }
    
    @objc func betAction() {
        self.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            
            if self.isAutoFollow {
                for model in self.autoFollowItems {
                    if let money = Double(model.money) {
                        model.money = money.cleanZero
                    }else {
                        showToast(view: self, txt: "请输入金额")
                        return
                    }
                }
                
                self.planAutoBetHandler?(self.autoFollowItems,self.infoId)
            }else {
                
                for model in (self.reverseBet ? self.reverseItems : self.items) {
                    if let money = Double(model.money) {
                        model.money = money.cleanZero
                    }else {
                        showToast(view: self, txt: "请输入金额")
                        return
                    }
                }
                
                self.planBetHandler?((self.reverseBet ? self.reverseItems : self.items),self.reverseBet)
            }
        }
    }
    
    @objc func resetAction() {
        
        self.betMoneyField.text = ""
        
        if self.isAutoFollow {
            self.periodsNumField.text = "10"
            self.autoFollowNums = 10
            for model in self.autoFollowItems {
                model.money = ""
            }
        }else {
            for model in (self.reverseBet ? self.reverseItems : self.items) {
                model.money = ""
            }
        }
        
        self.tableView.reloadData()
    }
}

//MARK: - UITableViewDataSource
extension PlanFollowBetPop:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isAutoFollow ? autoFollowItems.count : (self.reverseBet ? self.reverseItems.count : self.items.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isAutoFollow {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifierPlanFollowAuto, for: indexPath) as? PlanFollowAutoPopCell else {
                fatalError("dequeueReusableCell PlanFollowAutoPopCell fail")
            }
            
            if indexPath.row < self.autoFollowItems.count {
                let model = self.autoFollowItems[indexPath.row]
                cell.configWith(model: model)
                
                cell.moneyChangedHandler = {[weak self] (value) in
                    guard let weakSelf = self else {return}
                    model.money = value
                    weakSelf.tableView.reloadData()
                }
            }
            
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifierPlanFollow, for: indexPath) as? PlanFollowPopCell else {
                fatalError("dequeueReusableCell PlanFollowBetCell fail")
            }
            
            cell.selectionStyle = .none

            let items = self.reverseBet ? self.reverseItems : self.items

            if indexPath.row < items.count {
                let item = items[indexPath.row]
                cell.configWith(model: item, reverseBet:self.reverseBet)
                cell.delItemHandler = {[weak self] in
                    guard let weakSelf = self else {return}
                    
                    if weakSelf.reverseBet {
                        weakSelf.reverseItems.remove(at: indexPath.row)
                    }else {
                        weakSelf.items.remove(at: indexPath.row)
                    }
                }
                
                cell.moneyChangedHandler = {[weak self] (value) in
                    guard let weakSelf = self else {return}
                    item.money = value
                    weakSelf.tableView.reloadData()
                }
            }

            return cell
        }
    }
}

//MARK: - UITableViewDelegate
extension PlanFollowBetPop:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

//跟单model
class PFPlanItem {
    var money = ""
    var peilv = "" //从玩法赔率中获取并赋值给该对象
    var playRule = ""
    var playNum = ""
    var reverseNum = ""//反投号码
    var code = "" //dyj 赛车 亚军
}

class PFAutoPlanItem {
    var F = "" //正投金额
    var U = "" //反投金额
    var Q = "" //期号
    var money = "" //输入框中的金额
    var reverse = false //true 反投，false 正跟投
    var status = "" //1:跟单中2:跟单成功3:跟单失败
}
