//
//  PlanFollowAutoPopCell.swift
//  gameplay
//
//  Created by admin on 2020/8/31.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class PlanFollowAutoPopCell: UITableViewCell, UITextFieldDelegate {
    
        @IBOutlet weak var reverseFollowAllLabel: UILabel!
    @IBOutlet weak var followAllLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var moneyTextField: CustomFeildText!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var reduceButton: UIButton!
    @IBOutlet weak var reverseSwitch: UISwitch!
    var moneyChangedHandler:((_ money:String) -> Void)?
    var model = PFAutoPlanItem()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reverseSwitch.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        
        setupNoPictureAlphaBgView(view: self)
        self.layer.cornerRadius = 5
        
        moneyTextField.delegate = self
        
        increaseButton.addTarget(self, action: #selector(increaseMoneyAction), for: .touchUpInside)
        reduceButton.addTarget(self, action: #selector(reduceMoneyAction), for: .touchUpInside)
        
        reverseFollowAllLabel.theme_textColor = "Global.themeColor"
        followAllLabel.theme_textColor = "Global.themeColor"
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let text = textField.text ?? ""
        if let value = Double(text) {
            textField.text = value.cleanZero
        }else {
            showToast(view: self, txt: "请输入数字")
        }
        
        self.moneyChangedHandler?(text)
    }
    
    ///金额递增
    @objc func increaseMoneyAction() {
        increaseValue(textField: self.moneyTextField)
    }
    
    ///金额递减
    @objc func reduceMoneyAction(textField:CustomFeildText) {
        reduceValue(textField: self.moneyTextField)
    }
    
    func reduceValue(textField:CustomFeildText) {
        if let value = Int(textField.text ?? "") {
            if value <= 1 {
                textField.text = ""
            }else {
                textField.text = "\(value - 1)"
            }
        }else {
            textField.text = ""
        }
        
        self.moneyChangedHandler?(textField.text ?? "")
    }
    
    func increaseValue(textField:CustomFeildText) {
        if let value = Int(textField.text ?? "") {
            if value < 0 {
                textField.text = "1"
            }else {
                textField.text = "\(value + 1)"
            }
        }else {
            textField.text = "1"
        }
        
        self.moneyChangedHandler?(textField.text ?? "")
    }
    
    @objc func valueChanged(sender:UISwitch) {
        model.reverse = !sender.isOn
    }
    
    func configWith(model:PFAutoPlanItem) {
        self.model = model
        self.periodLabel.text = model.Q
        self.moneyTextField.text = model.money
        reverseSwitch.isOn = !model.reverse
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
