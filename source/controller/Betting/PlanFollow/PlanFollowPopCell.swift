//
//  PlanFollowPopCell.swift
//  gameplay
//
//  Created by admin on 2020/8/31.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class PlanFollowPopCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var moneyTextField: CustomFeildText!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var reduceButton: UIButton!
    @IBOutlet weak var palyRuleAndNumLabel: UILabel! //玩法
    @IBOutlet weak var oddsLabel: UILabel! //赔率
    @IBOutlet weak var delButton: UIButton!
    var delItemHandler:(() -> Void)?
    var moneyChangedHandler:((_ money:String) -> Void)?
    var item = PFPlanItem()
    
    func configWith(model:PFPlanItem,reverseBet:Bool) {
        self.moneyTextField.text = model.money
        self.palyRuleAndNumLabel.text = model.playRule + " " + (reverseBet ? model.reverseNum : model.playNum)
        self.oddsLabel.text = model.peilv
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delButton.addTarget(self, action: #selector(delItemAction), for: .touchUpInside)
        
        setupNoPictureAlphaBgView(view: self)
        self.layer.cornerRadius = 5
        
        moneyTextField.delegate = self
        
        increaseButton.addTarget(self, action: #selector(increaseMoneyAction), for: .touchUpInside)
        reduceButton.addTarget(self, action: #selector(reduceMoneyAction), for: .touchUpInside)
    }
    
    @objc func delItemAction() {
        delItemHandler?()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let text = textField.text ?? ""
        if let value = Double(text) {
            textField.text = value.cleanZero
        }else {
            showToast(view: self, txt: "请输入数字")
        }
        
        self.moneyChangedHandler?(text)
    }
    
    ///金额递增
    @objc func increaseMoneyAction() {
        increaseValue(textField: self.moneyTextField)
    }
    
    ///金额递减
    @objc func reduceMoneyAction(textField:CustomFeildText) {
        reduceValue(textField: self.moneyTextField)
    }
    
    func reduceValue(textField:CustomFeildText) {
        if let value = Int(textField.text ?? "") {
            if value <= 1 {
                textField.text = ""
            }else {
                textField.text = "\(value - 1)"
            }
        }else {
            textField.text = ""
        }
        
        self.moneyChangedHandler?(textField.text ?? "")
    }
    
    func increaseValue(textField:CustomFeildText) {
        if let value = Int(textField.text ?? "") {
            if value < 0 {
                textField.text = "1"
            }else {
                textField.text = "\(value + 1)"
            }
        }else {
            textField.text = "1"
        }
        
        self.moneyChangedHandler?(textField.text ?? "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
