//
//  FollowPreDetailCell.swift
//  gameplay
//
//  Created by admin on 2020/9/11.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class FollowPreDetailCell: UITableViewCell {

    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var typeAndStatusLabel: UILabel!
    var autoPlanItem = PFAutoPlanItem()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupNoPictureAlphaBgView(view: self)
        self.layer.cornerRadius = 5
    }
    
    func configWith(model:PFAutoPlanItem) {
        //期号
        if let wrapPeriod = Int(model.Q) {
            let periodValue = "\(wrapPeriod)期"
            let periodValueRange = NSRange.init(location: 0, length: (periodValue.length - 1)) //期
            let periodTitleRange = NSRange.init(location: periodValueRange.length, length: 1)//具体期号
            
            periodLabel.setAttributeString(text: periodValue, ranges: [periodValueRange,periodTitleRange], colors: [UIColor.init(hexString: "#388ec5")!,UIColor.black],fonts: [14,16])
        }
        
        if let money = Double(model.money) {
            let finalStr = "金额：\(money.cleanZero)"

            let moneyTitleRange = NSRange.init(location: 0, length: 3)// '金额：'
            let moneyValueRange = NSRange.init(location: moneyTitleRange.length, length: (finalStr.length - 3)) // 具体的金额
            
            moneyLabel.setAttributeString(text: finalStr, ranges: [moneyTitleRange,moneyValueRange], colors: [UIColor.black,UIColor.init(hexString: "#388ec5")!],fonts: [14,16])
        }
        
        let reverse = model.reverse ? "反投" : "正投"
        let status = (model.status == "1" || model.status == "2") ? "*已下注" : "*跟投失败"
        let statusValue = reverse + status
        let typeRange = NSRange.init(location: 0, length: reverse.length)
        let statusRange = NSRange.init(location: typeRange.length, length:status.length)
        
        typeAndStatusLabel.setAttributeString(text: statusValue, ranges: [typeRange,statusRange], colors: [UIColor.init(hexString: "#388ec5")!,UIColor.black],fonts: [14,16])
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
