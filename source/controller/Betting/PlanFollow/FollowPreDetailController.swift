//
//  FollowPreDetailController.swift
//  gameplay
//
//  Created by admin on 2020/9/11.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class FollowPreDetailController: BaseMainController {
    var infoId = "" //专家排名预测 itemId
    var followModel = PFFollowModel() //已跟专家
    var autoPlanItems = [PFAutoPlanItem]()
    
    lazy var tableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .grouped)
        view.separatorStyle = .none
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.tableFooterView = UIView.init()
        let nib = UINib.init(nibName: "FollowPreDetailCell", bundle: nil)
        view.register(nib, forCellReuseIdentifier: "FollowPreDetailCell")
        view.sectionFooterHeight = 0
        
        return view
    }()
    
    lazy var headerCell:FollowPredictorCell = {
        let cell = Bundle.main.loadNibNamed("FollowPredictorCell", owner: self, options: nil)?.last as! FollowPredictorCell
        cell.followDelegate = self
        setupNoPictureAlphaBgView(view: cell.bgView)
        cell.moreInfoImg.isHidden = true
        cell.editFollowBtn.isHidden = true
        return cell
    }()
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view, alpha: 0)

        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
                
        self.headerCell.frame = CGRect.init(x: 10, y: glt_iphoneX ? (24+64+10) : (64+10), width: (ScreenWidth - 20), height: 210)
        self.view.addSubview(self.headerCell)
        
        self.headerCell.gameName = self.title ?? ""
        self.headerCell.configPlanModel(model: self.followModel)
        
        view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.headerCell.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(15)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-15)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.requestPredictorFollow(infoId: self.infoId)
    }
    
    //MARK: - 数据 && 接口请求
    //MARK: 删除单个跟投
    func requestDelFollow(infoId:String) {
        request(frontDialog: true, method: .post, loadTextStr: "跟单删除中...", url: URL_DEL_ONE_FOLLOW, params: ["infoId":infoId], callback: {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
                           
               if !resultStatus {
                   if resultJson.isEmpty {
                       showToast(view: weakSelf.view, txt: convertString(string: "跟单删除求失败"))
                   }else{
                       showToast(view: weakSelf.view, txt: resultJson)
                   }
                
                   return
               }
            
            if let result = CRBaseModel.deserialize(from: resultJson) {
                if result.success {
                    showToast(view: weakSelf.view, txt: "跟单已删除")
                    weakSelf.onBackClick()
                }else {
                    showToast(view: weakSelf.view, txt: (result.msg.isEmpty ? "跟单删除失败" : result.msg))
                }
            }
        })
    }
    
    //MARK: 某个计划员下的跟投,编辑跟投
    func requestPredictorFollow(infoId:String) {
        request(frontDialog: true, method: .post, loadTextStr: "跟单获取中...", url: URL_FOLLOW_LIST, params: ["infoId":infoId], callback: {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
                           
               if !resultStatus {
                   if resultJson.isEmpty {
                       showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                   }else{
                       showToast(view: weakSelf.view, txt: resultJson)
                   }
                
                   return
               }
            
            if let models = [PredictorFollowModel].deserialize(from: resultJson) {
                //把返回的数据转换成跟投时的本地数据，以直接复用
                weakSelf.autoPlanItems.removeAll()
                for model in models {
                    let item = PFAutoPlanItem()
                    item.money = model!.money
                    item.Q = model!.qiHao
                    item.reverse = model?.isFollow == "1"
                    item.status = model?.status ?? ""
                    weakSelf.autoPlanItems.append(item)
                }
                
                weakSelf.tableView.reloadData()
            }else {
                showToast(view: weakSelf.view, txt: "获取跟单出错")
            }
        })
    }

}

extension FollowPreDetailController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return autoPlanItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowPreDetailCell") as? FollowPreDetailCell else {fatalError("dequeueReusableCell FollowPreDetailCell fail")}
        
        cell.selectionStyle = .none
        
        let model = self.autoPlanItems[indexPath.section]
        cell.configWith(model: model)
        return cell
    }
}

extension FollowPreDetailController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}


//MARK:FollowPredictorDelegate
extension FollowPreDetailController: FollowPredictorDelegate  {
    func editFollowBet(index: Int) {}
    
    func delFollowBet(index: Int) {
        requestDelFollow(infoId: infoId)
    }
}
