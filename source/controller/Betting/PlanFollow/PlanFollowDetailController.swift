//
//  PlanFollowDetailController.swift
//  gameplay
//
//  Created by admin on 2020/8/29.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit
import SnapKit

class PlanFollowDetailController: BaseMainController {
    var lotTypeCode = "" //如'BJSC 北京赛车'
    var cpVersion = "" //1官方，2信用
    var infoId = "" //专家排名预测 itemId
    var preHistoryes = [PlanPreHistoryModel?]()
    var plansModel = PFPlanModel()
    var meminfo:Meminfo?
//    var oneToTenRules = [HonestResult]()
    var lotData:LotteryData = LotteryData()
    var honestResult = HonestResult() //赔率数据和玩法
    var remainCount = 0  //剩余跟投期数
    var accountBanlance = 0.0 { //余额
        didSet {
            followBetPop.balanceLabel.text = String.init(format: "%.2f元", accountBanlance)
        }
    }
    
    var disableBet = false { //是否封盘
        didSet {
            self.headerCell.configPlanModel(model: plansModel,disableBet: disableBet)
            
            self.followBetPop.configDisableBet(disable: disableBet)
        }
    }
    
    lazy var tableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .plain)
        view.separatorStyle = .none
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.tableFooterView = UIView.init()
        let nib = UINib.init(nibName: "PlanFollowDetailCell", bundle: nil)
        view.register(nib, forCellReuseIdentifier: "PlanFollowDetailCell")
        
        return view
    }()
    
    lazy var headerCell:PlanFollowBetCell = {
        let cell = Bundle.main.loadNibNamed("PlanFollowBetCell", owner: self, options: nil)?.last as! PlanFollowBetCell
        cell.planDelegate = self
        setupNoPictureAlphaBgView(view: cell.bgView)
        cell.moreInfoImg.isHidden = true
        return cell
    }()

    lazy var followBetPop:PlanFollowBetPop = { //跟投，自动跟投PopView
        let view = Bundle.main.loadNibNamed("PlanFollowBetPop", owner: self, options: nil)?.last as! PlanFollowBetPop
        view.layer.cornerRadius = 5
        view.dismissHandler = {[weak self] in
            guard let weakSelf = self else {return}
            weakSelf.hidePopView()
        }
        
        view.planBetHandler = {[weak self] (items,reverseBet) in
            guard let weakSelf = self else {return}
            weakSelf.requestFollowBet(items: items,reverseBet:reverseBet)
        }
        
        view.planAutoBetHandler = {[weak self] (items,infoInd) in
            guard let weakSelf = self else {return}
            weakSelf.requestPlanAutoBet(items: items, infoId: infoInd)
        }
        return view
    }()
    
    lazy var bgView:UIButton =  {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.frame = UIScreen.main.bounds
        view.addTarget(self, action: #selector(clickBgView), for: .touchUpInside)
        return view
    }()
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view, alpha: 0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
                
        self.headerCell.frame = CGRect.init(x: 10, y: glt_iphoneX ? (24+64+10) : (64+10), width: (ScreenWidth - 20), height: 210)
        self.view.addSubview(self.headerCell)
        
        self.headerCell.cpVersion = self.cpVersion
        self.headerCell.lotTypeCode = self.lotTypeCode
        
        let model = self.plansModel
        self.headerCell.gameName = self.title ?? ""
        self.headerCell.configPlanModel(model: model,disableBet: self.disableBet)
        
        view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.headerCell.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(15)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-15)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        requestHistory(infoId: self.infoId)
    }
    
    @objc override func onBackClick() {
        super.onBackClick()
    }
    
    @objc func clickBgView() {
        hidePopView()
    }
    
    func showGrayBgView() {
        view.addSubview(self.bgView)
    }
    
    ///显示跟投 pop,isAuto是否是自动跟投
    func showFollowBetPop() {
        self.accountWeb { _ in}
        
        showGrayBgView()
        view.addSubview(self.followBetPop)
        
        self.followBetPop.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalTo(ScreenWidth*0.95)
        }
    }
    
    func hidePopView() {// 需要隐藏时动画优化
        self.followBetPop.resetAction()
        self.followBetPop.removeFromSuperview()
        self.bgView.removeFromSuperview()
    }
    
    //MARK: 预测历史列表
    func requestHistory(infoId:String) {
        request(frontDialog: true, method: .post, loadTextStr: "获取预测历史", url: URL_PLAN_HISTORY, params: ["infoId":infoId], callback: {[weak self] (resultJson, resultStatus) in
            guard let weakSelf = self else {return}
                           
               if !resultStatus {
                   if resultJson.isEmpty {
                       showToast(view: weakSelf.view, txt: convertString(string: "获取预测历史失败"))
                   }else{
                       showToast(view: weakSelf.view, txt: resultJson)
                   }
                
                   return
            }
            
            if let models = [PlanPreHistoryModel].deserialize(from: resultJson) {
                weakSelf.preHistoryes = models
                weakSelf.tableView.reloadData()
            }else {
                showToast(view: weakSelf.view, txt: "未获取预测历史")
            }
        })
    }
    
    ///获取帐号信息
    func accountWeb(handler:@escaping (_ leftMoney: String) -> ()) {
        request(frontDialog: false, url:MEMINFO_URL,
                callback: {[weak self](resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        return
                    }
                    if let result = MemInfoWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let memInfo = result.content{
                                //更新余额等信息
                                weakSelf.updateAccount(memInfo:memInfo, handler: {(content) in
                                        handler(content)
                                    }
                                )
                            }
                        }
                    }
        })
    }
    
    ///下注接口
    func requestFollowBet(items:[PFPlanItem],reverseBet:Bool) {
        
        var orderList = [[String:String]]()
        for model in items {
            let playNum = reverseBet ? model.reverseNum : model.playNum
            let dic = ["i":model.code,"a":model.money,"d":(model.code+playNum),"c":playNum]
            orderList.append(dic)
        }

        let postData = ["data":orderList,"kickback":"0","lotCode":lotData.code ?? ""] as [String:Any]
        if (JSONSerialization.isValidJSONObject(postData)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: postData, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            
            request(frontDialog: true, method: .post, loadTextStr: "下注中...", url: DO_PEILVBETS_URL, params: ["data":str!]) {[weak self] (resultJson, resultStatus) in
                guard let weakSelf = self else {return}
                
                weakSelf.hidePopView()
                
                if !resultStatus {
                    if resultJson.isEmpty {
                        showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                    }else{
                        showToast(view: weakSelf.view, txt: resultJson)
                    }
                    return
                }
                
                if let result = DoBetWrapper.deserialize(from: resultJson){
                    if result.success{
                        YiboPreference.setToken(value: result.accessToken as AnyObject)

                        showToast(view: weakSelf.view, txt: "下注成功")
                        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
                        
                    }else{
                        if !isEmptyString(str: result.msg){
                            showToast(view: weakSelf.view, txt: result.msg)
                        }else{
                            showToast(view: weakSelf.view, txt: convertString(string: "下注失败"))
                        }
                        //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                        //所以此接口当code == 0时表示帐号被踢，或登录超时
                        if (result.code == 0) {
                            loginWhenSessionInvalid(controller: weakSelf)
                            return
                        }
                    }
                }
            }
        }
    }
    
    //MARK: 自动跟投
    func requestPlanAutoBet(items:[PFAutoPlanItem],infoId:String) {
        
        var itemsDic = [[String:Any]]()
        for model in items {
            let dic = ["Q":model.Q,(model.reverse ? "U" : "F"):model.money]
            itemsDic.append(dic as [String : Any])
        }
        
        if (JSONSerialization.isValidJSONObject(itemsDic)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: itemsDic, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            
            let params = ["plan":str!,"infoId":infoId] as [String:Any]
            request(frontDialog: true, method: .post, loadTextStr: "跟投中...", url: URL_PLAN_AUTO_BET, params: params) {[weak self] (resultJson, resultStatus) in
                guard let weakSelf = self else {return}
                
                weakSelf.hidePopView()
                
                if !resultStatus {
                    if resultJson.isEmpty {
                        showToast(view: weakSelf.view, txt: convertString(string: "跟单列表请求失败"))
                    }else{
                        showToast(view: weakSelf.view, txt: resultJson)
                    }
                    return
                }
                
                if let result = CRBaseModel.deserialize(from: resultJson) {
                    if result.success {
                        showToast(view: weakSelf.view, txt: "投注成功")
                    }else {
                        if !isEmptyString(str: result.msg){
                            showToast(view: weakSelf.view, txt: result.msg)
                        }else{
                            showToast(view: weakSelf.view, txt: convertString(string: "投注失败"))
                        }
                        //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                        //所以此接口当code == 0时表示帐号被踢，或登录超时
                        if (result.code == "0") {
                            loginWhenSessionInvalid(controller: weakSelf)
                            return
                        }
                    }
                }
            }
        }
    }

    func updateAccount(memInfo:Meminfo,handler:(_ leftMoneyP: String) -> ()) {
        self.meminfo = memInfo
        if !isEmptyString(str: memInfo.balance){
            let leftMoneyName = "\(memInfo.balance)"

            handler(leftMoneyName)
            
            if let value = Double(leftMoneyName) {
                accountBanlance = value
            }
        }
    }

}

//MARK: - UITableViewDataSource
extension PlanFollowDetailController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.preHistoryes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlanFollowDetailCell", for: indexPath) as? PlanFollowDetailCell else {fatalError("dequeueReusableCell PlanFollowDetailCell fail")}

        cell.selectionStyle = .none
        cell.lotTypeCode = self.lotTypeCode
        cell.cpVersion = self.cpVersion
        
        if let model = preHistoryes[indexPath.row]  {
            cell.configWith(model: model)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let edges = UIEdgeInsets.init(top: 5, left: 15, bottom: 10, right: 15)
        cell.separatorInset = edges
        cell.layoutMargins = edges
    }
}

extension PlanFollowDetailController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension PlanFollowDetailController: PlanFollowBetDelegate {
    ///跟投
    func followBet(index:Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        showFollowBetPop()
        self.followBetPop.honestResult = self.honestResult
        self.followBetPop.configPopType(reverseBet:false,isAutoFollow: false, planModel: self.plansModel)
    }
    
    ///反投
    func counterFollowBet(index:Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        showFollowBetPop()
        self.followBetPop.honestResult = self.honestResult
        self.followBetPop.configPopType(reverseBet:true,isAutoFollow: false, planModel: self.plansModel)
    }
    
    ///自动跟投
    func autoFollowBet(index:Int) {
        if disableBet {
            showToast(view: self.view, txt: "封盘中")
            return
        }
        
        if let qihao = Int64(self.plansModel.qiHao) {
            showFollowBetPop()
            self.followBetPop.remainCount = self.remainCount
            self.followBetPop.currentPeriod = qihao
            self.followBetPop.infoId = self.plansModel.infoId
            self.followBetPop.configPopType(reverseBet: false, isAutoFollow: true, planModel: self.plansModel)
        }
    }
    
}
