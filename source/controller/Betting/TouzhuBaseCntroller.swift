
//
//  TouzhuBaseCntroller.swift
//  gameplay
//
//  Created by admin on 2018/10/1.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import SwiftTheme
import HandyJSON

class TouzhuBaseCntroller: BaseController {
    var viewShowing = false //该页面是否正在显示
    var officalBonus:Double? //奖金
    var fakeSubPlayCode = "" // 标注从后台返回的玩法数据中自己分离出来的玩法
    var fakeSubPlayName = "" // 标注从后台返回的玩法数据中自己分离出来的玩法
    var ruleNameLength = 5 //当玩法名称 <= 4 则字体加大
    /** 新增玩法类型数据 */
    var groupName : String?
    var groupCode:String?
    var updateLotName:((_ name: String) -> Void)?
    var hasNotLoginHandler:(() -> Void)?
    var lotCpCodeChangedHandler:((_ lotCpCode:String) -> Void)?
    var viewLotRecordHandler:(() -> Void)?
    var gotoResultsPageHandler:((_ cpTypeCode:String,_ cpBianHao:String) -> Void)?
    
    var gotoLongDragonPageHandler:((_ gameCode: String, _ groupName: String) -> Void)?
    var goTrendChartHandler:((_ cpTypeCode:String,_ cpBianHao:String) -> Void)?
    var playIntroduceHandler:((_ cpTypeCode:String,_ cpBianHao:String, _ code : String) -> Void)?
    var gotoPlanFollowBetHandler:((_ code:String,_ lotteryName:String,_ lotData:LotteryData,_ oneToTenRules:[HonestResult]) -> Void)?
    var openSettingHandler:(() -> Void)?
    var switchThemeBetVCHandler:((_ lotData:LotteryData) -> Void)?
    var switchSoundEffectHandler:(() -> Void)?

    var lotteryPathView:LotteryPathView!
    var switchThemeHandler:(() -> Void)?
    var betShouldPayMoney = 0.0
    var accountBanlance = 0.0
    var meminfo:Meminfo?
    var awardNum = ""
    var multiplyValue:Float = 1.0
    var bombSoundEffect: AVAudioPlayer?
    
    let PLAY_RULE_TABLVIEW_TAG = 0
    let PLAY_PANE_TABLEVIEW_TAG = 1
    let RECENT_RESULTS_TABLEVIEW_TAG = 2
    
    var lotData:LotteryData?
    
    var peilvListDatas:[BcLotteryPlay] = []//赔率版彩票列表数据源
    var selectNumList:[PeilvWebResult] = []//选择好的赔率项数据
    var ballonDatas:[BallonRules] = []//官方版彩票列表数据源
    var officail_odds:[PeilvWebResult] = []//官方版所有赔率
    var currentOfficialOdds = [PeilvWebResult]() //选中的官方号码球赔率
    var honest_odds:[HonestResult] = []//信用版用户选择的侧边打玩法对应的所有小玩法对应的所有赔率列表数据
    var selectPlay:BcLotteryPlay?//当前选择的侧边玩法
    
    let BALL_COUNT_PER_LINE = 5
    let BALL_COUNT_PER_LINE_WHEN_EXPAND = 5
    
    var is_fast_bet_mode = true//是否快捷下注模式
    var currentQihao:String = "";//当前期号
    var cpVersion:String = VERSION_1;//当前彩票版本
    var subPlayCode:String = ""
    var subPlayName = ""
    var czCode:String = ""
    var cpBianHao:String = "";//彩票编码
    var cpTypeCode:String = ""////彩票类型代号
    var lotteryICON = ""//icon
    var cpName:String = ""
    var ago:Int64 = 0//开奖时间与封盘时间差,单位秒
    let offset:Int = 1//偏差1秒
    var tickTime:Int64 = 0////倒计时查询最后开奖的倒计时时间
    var endBetDuration:Int = 0;//秒
    var endBetTime:Int = 0//距离停止下注的时间
    var disableBetTime:Int64 = 0//距离再次开始下注的剩余时间
    
    var selectMode = "";//金额模式，元模式
    var selectedBeishu = 1;//选择的倍数
    var selectedMoney = 0.0;//总投注金额,单位元
    var winExample = "";
    var detailDesc = "";
    var playMethod = "";
    
    var playRules:[BcLotteryPlay] = []//所有叶子玩法列表数据
    var selected_rule_position = 0;//用户在侧边玩法栏选择的玩法位置
    var titleBtn:UIButton!
    var titleIndictor:UIImageView = UIImageView.init()
    var lotWindow:LotsMenuView!//彩种菜单
    var allLotDatas:[LotteryData] = []
    var shouldPlayKaiJianVolume = false
    //是否停售
    var isStopSell:Bool = false
    
    var gotoBetNewPeilvHandler:(([OrderDataInfo],[BcLotteryPlay],LHCLogic2,String,String,String,String,Float,String,String,String) -> Void)?
    

    var gotoBetNewOfficalHandler:((_ data:[OrderDataInfo],_ lotCode:String,_ lotName:String,_ subPlayCode:String,_ subPlayName:String,
    _ cpTypeCode:String,_ cpVersion:String,_ officail_odds:[PeilvWebResult],_ icon:String,_ meminfo:Meminfo?,_ bobus:Double?) -> Void)?
    
    var firstResultQiHao = "" {
        didSet {
            if oldValue != firstResultQiHao && shouldPlayKaiJianVolume{
                playKaiJianVolume()
            }
        }
    }
    
    var recentResults:[BcLotteryData] = []
    var isRecentResultOpen = false
    var isPlayBarHidden = false
    var right_top_menu:SwiftPopMenu!//右上角悬浮框
    var version_top_menu:SwiftPopMenu!//版本切换悬浮框
    let KSCREEN_WIDTH:CGFloat = UIScreen.main.bounds.size.width
    var tag:Int = 1
    
    var lastKaiJianResultTimer:Timer?//查询最后开奖结果的倒计时器
    var endlineTouzhuTimer:Timer?//距离停止下注倒计时器
    var disableBetCountDownTimer:Timer?//禁止下注倒计时器
    // 很小概率出现的倒计时停止现象,矫正
    var judgeTimer:Timer?
    
    var lhcSelect = false;//是否在奖金版本中选中了六合彩，十分六合彩
    var betMoneyWhenPeilv = ""//每注下注金额
    var sortWhenSelectPeilvNumber = 0;////用户选择号码的顺序
    
    var totalPeilvMoney = 0;//总投注金额
    
    var official_orders:[OfficialOrder] = []//官方版所有下注主单列表
    var honest_orders:[PeilvOrder] = []////信用版所有下注主单列表
    var current_rate:Float = 0;//当前拖动选择的反水比例
    var current_odds:Float = 0;//当前奖金或赔率
    
    var isViewVisible = true
    var selectedSubPlayCode:String = ""//赔率版左侧玩法栏玩法code
    var choosedPlay:BcLotteryPlay?;
    var lhcLogic = LHCLogic2()//特殊六合彩玩法的处理类
    
    var codeRank:[CodeRankModel]?
    var showCodeRank = true//是否显示冷热遗漏数据
    var fixRateStep:Float = 0//返水条加减点击步长，后台配置
    
    var right_top_allDataSources = [(icon:"TouzhOffical.SwiftPopMenu.firstImage",title:"投注记录"),(icon:"TouzhOffical.SwiftPopMenu.secodeImage",title:"历史开奖"),(icon:"TouzhOffical.SwiftPopMenu.thirdImage",title:"玩法说明"),(icon:"TouzhOffical.SwiftPopMenu.fourthImage",title:"设置"),(icon:"TouzhOffical.SwiftPopMenu.fiveImage",title:"切换主题"),
                                    (icon:"TouzhOffical.SwiftPopMenu.sixImage",title:"切换音效"),
                                    (icon:"TouzhOffical.SwiftPopMenu.eightImage",title:"走势图"),
                                    (icon:"TouzhOffical.SwiftPopMenu.tenImage",title:"长龙")]
    
    var right_top_datasources = [(icon:String,title:String)]()
    
    var version_switch_datasources = [(icon:"shake_touzhu",title:"官方下注"),(icon:"shake_touzhu",title:"信用下注")]
    
    var version_liuhe_switch_datasources = [(icon:"shake_touzhu",title:"官方下注"),(icon:"",title:"信用下注")]
    
    var selectedPlayRulesModel = SelectedPlayRulesModel() //多玩法投注，记录选择的玩法
    var peilvListDatasSuperSet:[[BcLotteryPlay]] = [] //多玩法投注多个玩法的赔率列表数据集合
    var headerCode = "" //有header选择的彩种的code，如连肖/连码等
    var headerName = "" //有header选择的彩种的name，如连肖/连码等
    var moreHeadersSubCode = "" //合肖等有多个子header(如一肖中，一肖不中)对应的code，以标识是‘中’或‘不中’
    
    ///玩法名称长度
    func getLotNameLength(data:[BcLotteryPlay]) -> Int {
        var length = 0
        
        for (_,item) in data.enumerated() {
            let name = isEmptyString(str: item.fakeParentName) ? item.name : item.fakeParentName
            if name.length > length {
                length = name.length
            }
        }
        
        return length
    }
    
    //MARK: - 播放声音
    func playVolume() -> Void{
        let pathIndex = YiboPreference.getCurrentSoundEffect()
        let pathStr = SoundEffectPaths[pathIndex]
        let path = Bundle.main.path(forResource: pathStr, ofType: "mp3")
        playSoundWithPath(path: path!)
    }
    
    func playKaiJianVolume() -> Void{
        let path = Bundle.main.path(forResource: "open_result", ofType: "mp3")
        playSoundWithPath(path: path!)
    }
    
    func playSoundWithPath(path: String){
        let url = NSURL(fileURLWithPath: path)
        
        do {
//            #if DEBUG
//
//            #else
            bombSoundEffect = try AVAudioPlayer(contentsOf: url as URL)
            bombSoundEffect?.play()
//            #endif
        } catch {
            print("音频文件找不到了")
        }
    }
    
    func playVolumeWhenStartShake() -> Void {
        let path = Bundle.main.path(forResource: "rock", ofType: "mp3")
        playSoundWithPath(path: path!)
    }
    
    func playVolumeWhenEndShake() -> Void {
        if !YiboPreference.getShakeTouzhuStatus(){
            return
        }
        
        let path = Bundle.main.path(forResource: "rock_end", ofType: "mp3")
        playSoundWithPath(path: path!)
    }
    
    //MARK: - 接口
    //MARK: 获取公告弹窗内容
    func showAnnounce(controller:BaseController) {
        if YiboPreference.isShouldAlert_isAll() == "" {
            YiboPreference.setAlert_isAll(value: "on" as AnyObject)
        }
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if !shouldShowNoticeWIndow() {
                return
            }
        }
        
        controller.request(frontDialog: false, url:ACQURIE_NOTICE_POP_URL,params:["code":19],
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
                            if !resultStatus {
                                return
                            }
                            
                            if let result = NoticeResultWraper.deserialize(from: resultJson){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                PopupAlertListView(resultJson: resultJson,controller:self,not:"",anObject:nil,from: 1)
                            }
        })
    }
    
    //MARK: 今日输赢信息
    func syncWinLost() -> Void {
        request(frontDialog: true, method: .get, url:WIN_LOST_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = WinLostWraper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }
                            guard let content = result.content else {return}
                            self.showWinLostDialog(result:  content)
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                            //所以此接口当code == 0时表示帐号被踢，或登录超时
                            if (result.code == 0) {
                                self.hasNotLoginHandler?()
                                return
                            }
                        }
                    }
        })
    }
    
    func  syncLhcBetServerTime(lotCode:String) -> Void {
        request(frontDialog: true, method: .get, url:get_server_bettime_for_lhc,params:["lotCode":lotCode],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        return
                    }
                    if let result = LhcServerTimeWraper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }
                            self.lhcLogic.lhcBetServerTime = result.content
                        }else{
                            
                        }
                    }
        })
    }
    
    //获取完彩种信息并同步玩法数据后，同步一下六合彩的服务器时间
    func startSyncLhcServerTime(lotCode:String){
        if isSixMark(lotCode: lotCode){
            print("获取完彩种信息并同步玩法数据后，同步一下六合彩的服务器时间---------")
            syncLhcBetServerTime(lotCode: lotCode)
        }
    }
    
    /**
     * 开始获取当前期号离结束投注倒计时
     * @param bianHao 彩种编号
     * @param lotVersion 彩票版本
     */
    //MARK: 当前期号离结束投注倒计时
    func getCountDownByCpcode(bianHao:String,lotVersion:String,controller:BaseController,shouldReStartDisableTimer:Bool,failureHandler:@escaping (_ currentQiHao:String,_ countDown:String) -> (),successHandler: @escaping (_ dealDurationOutP: String) -> ()) {
        
        request(frontDialog: false, url:LOTTERY_COUNTDOWN_URL,params:["lotCode":bianHao,"version":lotVersion],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    let blockFailureHandler = {
                        self.currentQihao = "????"
                        failureHandler(self.currentQihao,"00 : 00 : 00")
                    }
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取当前期号失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        
                        blockFailureHandler()
                        return
                    }
                    if let result = LocCountDownWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            //更新当前这期离结束投注的倒计时显示
                            if let value = result.content{
                                self.updateCurrenQihaoCountDown(countDown: value,shouldReStartDisableTimer:shouldReStartDisableTimer,successHandler:
                                    {(dealDurationInP) in
                                        successHandler(dealDurationInP)
                                })
                            }
                        }else{
                            blockFailureHandler()
                            //彩票停售时弹框显示
                            if result.code == 1300{
                                if !self.isStopSell{
                                    self.showStopSaleWindow()
                                }
                              
                                self.isStopSell = true
                                return
                            }
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取当前期号失败"))
                            }
                            if result.code == 0{
                                self.hasNotLoginHandler?()
                            }
                        }
                    }
        })
    }
    
    func showStopSaleWindow(){
        let stopSaleView:StopSaleWindow = Bundle.main.loadNibNamed("stop_sale_window", owner: nil, options: nil)?.first as! StopSaleWindow
        stopSaleView.controller = self
        stopSaleView.show()
    }
    
    //MARK: account
    func accountWeb(handler:@escaping (_ leftMoney: String) -> ()) {
        //帐户相关信息
        request(frontDialog: false, url:MEMINFO_URL,
                callback: {[weak self](resultJson:String,resultStatus:Bool)->Void in
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        return
                    }
                    if let result = MemInfoWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let memInfo = result.content{
                                //更新余额等信息
                                weakSelf.updateAccount(memInfo:memInfo, handler: {(content) in
                                    handler(content)
                                }
                                    
                                )
                            }
                        }
                    }
        })
    }
    
    
    //根据最新的彩种信息获取对应的玩法数据
    //网络获取
    //说明:当当前彩种变化时都需要调用此方法同步下玩法数据
    func sync_playrule_from_current_lottery(lotType:String,lotCode:String,lotVersion:String,handler:@escaping (_ lotteryData: LotteryData?) -> ()) {
        
        self.peilvListDatasSuperSet = [[BcLotteryPlay]]()
        self.selectedPlayRulesModel = SelectedPlayRulesModel() //多玩法投注，记录选择的玩法
        
        let parameters = ["lotType":lotType,"lotCode":lotCode,"lotVersion":lotVersion] as [String : Any]
        request(frontDialog: true, loadTextStr:"获取玩法中", url:GAME_PLAYS_URL,params:parameters,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取玩法失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = LotPlayWraper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            handler(result.content)
                        }else{
                            self.print_error_msg(msg: result.msg)
                            if result.code == 0{
                                self.hasNotLoginHandler?()
                            }
                        }
                    }
        })
    }
    
    //MRAK: 点击侧边栏获取赔率
    func sync_official_peilvs_after_playrule_click(lotType:String,playCode:String,showDialog:Bool,handler:@escaping (_ rightResult:PeilvWebResult) -> ()) -> Void{
        
        let params:Dictionary<String,AnyObject> = ["playCode":playCode as AnyObject,
                                                   "lotType":lotType as AnyObject,
                                                   "version":VERSION_1 as AnyObject]
        
        expandRequest(frontDialog: showDialog, loadTextStr:"赔率获取中..",url:GET_JIANJIN_ODDS_URL,params:params,
                      callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                        
                        guard let weakSelf = self else {return}
                        
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: weakSelf.view, txt: convertString(string: "获取赔率失败"))
                            }else{
                                showToast(view: weakSelf.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = PeilvWebResultWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                weakSelf.officail_odds.removeAll()
                                
                                weakSelf.officail_odds = weakSelf.officail_odds + result.content
                                
                                //选出返水比例值并同步拖动条
                                weakSelf.update_slide_when_peilvs_obtain(webResults:weakSelf.officail_odds,handler: {(rightResult) in
                                    handler(rightResult)
                                })
                            }else{
                                weakSelf.print_error_msg(msg: result.msg)
                                if result.code == 0{
                                    weakSelf.hasNotLoginHandler?()
                                }
                            }
                        }
        })
    }
    
    //MARK: 纯粹的赔率获取，之前的方法其他操作太多
    func sync_honest_plays_odds_obtainForPlan(fakeParentCode:String = "",lotCategoryType:String,subPlayCode:String,showDialog:Bool,handler:@escaping (_ honest_odds:[HonestResult]) -> ()) -> Void {
        
        expandRequest(frontDialog: showDialog, loadTextStr:"赔率获取中..",url:GET_HONEST_ODDS_URL,
                      params:["playCodes":subPlayCode,"lotType":lotCategoryType,"version":self.cpVersion],
                      callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "获取赔率失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = PeilvHonestResultWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                
                                handler(result.content)
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "获取赔率失败"))
                                }
                                if result.code == 0 || result.code == 10000{
                                    self.hasNotLoginHandler?()
                                }
                            }
                        }
        })
    }
    
    //MARK: 赔率获取
    func sync_honest_plays_odds_obtain(fakeParentCode:String = "",lotCategoryType:String,subPlayCode:String,showDialog:Bool,handler:@escaping () -> ()) -> Void {
        expandRequest(frontDialog: showDialog, loadTextStr:"赔率获取中..",url:GET_HONEST_ODDS_URL,
                      params:["playCodes":subPlayCode,"lotType":lotCategoryType,"version":self.cpVersion],
                      callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "获取赔率失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = PeilvHonestResultWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                //更新赔率面板号码区域赔率等数据
                                
                                let content = result.content
                                //时时彩，分分彩，1-5球 本地数据处理;过滤掉大小单双

                                if fakeParentCode == "ffc_ssc_1To5" {
                                    for (_,resultModel) in content.enumerated() {
                                        var newOdds = [PeilvWebResult]()
                                        
                                        for (_,webResult) in resultModel.odds.enumerated() {
                                            if !["大","小","单","双"].contains(webResult.numName) {
                                                newOdds.append(webResult)
                                            }
                                        }
                                        
                                        resultModel.odds = newOdds
                                    }
                                }
                            
                                if fakeParentCode == "ffc_ssc_lm" { //两面，过滤掉 0-9
                                    for (_,resultModel) in content.enumerated() {
                                        var newOdds = [PeilvWebResult]()
                                        
                                        for (_,webResult) in resultModel.odds.enumerated() {
                                            if !["0","1","2","3","4","5","6","7","8","9"].contains(webResult.numName) {
                                                newOdds.append(webResult)
                                            }
                                        }
                                        
                                        resultModel.odds = newOdds
                                    }
                                }
                                
                                self.honest_odds = content
                                self.selectPlay = self.playRules[self.selected_rule_position]
                                
                                handler()
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "获取赔率失败"))
                                }
                                if result.code == 0 || result.code == 10000{
                                    self.hasNotLoginHandler?()
                                }
                            }
                        }
        })
    }
    
    //真正开始赔率下注
    /*
     @param order 下注注单
     @param rateback 用户选择的返水
     */
    //MARK: 赔率下注接口
    func do_peilv_bet(datas:[PeilvOrder],rateback:Float,handler:@escaping () -> ()){
        
        if datas.isEmpty{
            showToast(view: self.view, txt: "没有需要提交的订单，请先投注!")
            return
        }
        //构造下注POST数据
        var bets = [Dictionary<String,AnyObject>]()
        for order in datas{
            var bet = Dictionary<String,AnyObject>()
            bet["i"] = order.i as AnyObject
            bet["c"] = order.c as AnyObject
            bet["d"] = order.d as AnyObject
            bet["a"] = order.a as AnyObject
            bets.append(bet)
        }
        let postData = ["lotCode":self.cpBianHao,
                        "data":bets,
                        "kickback":rateback] as [String : Any]
        if (JSONSerialization.isValidJSONObject(postData)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: postData, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            //do bet
            request(frontDialog: true, method: .post, loadTextStr: "正在下注...", url:DO_PEILVBETS_URL,params: ["data":str!],
                    callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "下注失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = DoBetWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
                                
                                //把投注成功的结果记录在常用玩法的数据库
                                let record:VisitRecords = VisitRecords()
                                record.cpName = self.cpName //名字
                                record.czCode = self.cpTypeCode //类型
                                record.ago = "0" // 时间差 传默认值
                                record.cpBianHao = self.cpBianHao //编号
                                record.lotType = self.cpTypeCode //类型
                                record.lotVersion = self.cpVersion //版本
                                record.icon = self.lotteryICON //icon
                                CommonRecords.updateRecordInDB(record: record);
                                
                                self.showBetSuccessDialog()
                                //clear select balls after bet success
                                
                                handler()
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "下注失败"))
                                }
                                //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                                //所以此接口当code == 0时表示帐号被踢，或登录超时
                                if (result.code == 0) {
                                    self.hasNotLoginHandler?()
                                    return
                                }
                            }
                        }
            })
        }
    }
    
    //MARK: 获取冷热遗漏数据
    func getCodeRank(lotCode:String,getDataSuccess:@escaping () -> ()){
        request(frontDialog: false,method: .get,loadTextStr: "获取中...", url:API_COLD_HOT,params: ["lotCode":lotCode],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: "获取数据失败")
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = CodeRankModelWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let results = result.content{
                                self.codeRank = results
                                getDataSuccess()
                            }
                        }else{
                            if isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                self.hasNotLoginHandler?()
                                return
                            }
                        }
                    }
        })
    }
    
    //MARK: 获取开奖结果
    func lastOpenResult(cpBianHao:String,lastLotteryExcpHandler:@escaping (_ contents:String) -> (),updateLotteryHandler:@escaping (_ results:[BcLotteryData]) -> ()) -> Void {
        request(frontDialog: false, url:LOTTERY_LAST_RESULT_URL,params: ["lotCode":cpBianHao,"pageSize":50],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取最近开奖结果失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = LastResultWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            //更新开奖结果
                            if let results = result.content{
                                updateLotteryHandler(results)
                                
//                                self.updateLastKaiJianResult(result:results)
                            }
                            //开始请求开奖结果倒计时，时长以对应彩种中封盘时间与开奖时间差为主
                            if let timer = self.lastKaiJianResultTimer{
                                timer.invalidate()
                            }
                            
                            self.createLastResultTimer()
                        }else{
                            if let timer = self.lastKaiJianResultTimer{
                                timer.invalidate()
                            }
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取最近开奖结果失败"))
                            }
                            
                            
                            lastLotteryExcpHandler("没有开奖结果")
//                            self.updateLastKaiJianExceptionResult(result: "没有开奖结果")
                            
                            //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                            //所以此接口当code == 0时表示帐号被踢，或登录超时
                            if (result.code == 0) {
                                self.hasNotLoginHandler?()
                                return
                            }
                        }
                    }
        })
    }

    
    /**
     * 创建查询开奖结果倒计时
     * @param duration
     */
    func createLastResultTimer() -> Void {
        self.lastKaiJianResultTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(lastResultTickDown), userInfo: nil, repeats: true)
        if let timer = lastKaiJianResultTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    
    @objc func lastResultTickDown() -> Void {
        //将剩余时间减少1秒
        self.tickTime -= 1
    }
    
    
    // 遍历数组,取出公告赋值
    private func forArrToAlert(notices: Array<NoticeResult>) {
        
        var noticesP = notices
        noticesP = noticesP.sorted { (noticesP1, noticesP2) -> Bool in
            return noticesP1.sortNum < noticesP2.sortNum
        }
        
        var models = [NoticeResult]()
        for index in 0..<noticesP.count {
            let model = noticesP[index]
            if model.isBet {
                models.append(model)
            }
        }
        
        if models.count > 0 {
            let weblistView = WebviewList.init(noticeResuls: models)
            weblistView.show()
        }
    }
    
    //把时间戳转换成想要的日期格式
    func changeDate(timestamp:Int64) -> String {
        let d1 = Date(timeIntervalSince1970: (TimeInterval(timestamp / 1000)))
        
        let timeZone = TimeZone.init(identifier: "GTM")
        let formatter = DateFormatter()
        formatter.timeZone = timeZone
        formatter.locale = Locale.init(identifier: "zh_CN")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.string(from: d1)
        return date
    }
    
    func updateCurrenQihaoCountDown(countDown:CountDown,shouldReStartDisableTimer:Bool,successHandler:(_ dealDurationP:String) -> ()) -> Void {
        //创建开奖周期倒计时器
        let serverTime = countDown.serverTime;
        let activeTime = countDown.activeTime;
        let value = abs(activeTime) - abs(serverTime)
        let preStartTime = countDown.preStartTime
        self.endBetDuration = Int(abs(value))/1000

//        if self.disableBetTime == 0 {
            self.endBetTime = self.endBetDuration
//        }
        
        self.currentQihao = countDown.qiHao
        let dealDuration = getFormatTime(secounds: TimeInterval(Int64(self.endBetDuration)))

        if  preStartTime - serverTime < 0{
            self.createEndBetTimer() //开盘中
        }else if shouldReStartDisableTimer {
            //封盘中
            let agoTime = preStartTime - serverTime
            if agoTime < ago * 1000 {
                self.disableBetTime = agoTime / 1000
                self.createDisableBetCountDownTimer()
            }else {
                self.disableBetTime = agoTime / 1000
                self.createDisableBetCountDownTimer()
            }
        }
        
        successHandler(dealDuration)
    }
    
    /**
     * 创建分盘到开奖的倒计时器
     * @param duration
     */
    func createDisableBetCountDownTimer() -> Void {
        if let timer = self.endlineTouzhuTimer {
            timer.invalidate()
        }
        
        if let timer = self.disableBetCountDownTimer {
            timer.invalidate()
        }
        
        if self.disableBetTime > 0 {
            YiboPreference.setAbleBet(value: "off")
        }
        
        self.disableBetCountDownTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(disableBetTickDown), userInfo: nil, repeats: true)
        
        if let timer = self.disableBetCountDownTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    @objc func disableBetTickDown() {
        
    }
    
    /**
     * 创建停止下注倒计时
     * @param duration
     */
    func createEndBetTimer() -> Void {
        if let timer = self.endlineTouzhuTimer {
            timer.invalidate()
        }
        
        if let timer = self.disableBetCountDownTimer {
            timer.invalidate()
        }
        
        YiboPreference.setAbleBet(value: "on")

        self.endlineTouzhuTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(endBetTickDown), userInfo: nil, repeats: true)
        
        if let timer = self.endlineTouzhuTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    @objc func endBetTickDown() {
    
    }
     
    func showWinLostDialog(result:WinLost) -> Void {
        if !isViewVisible{
            return
        }
        let message = String.init(format: "今日消费:%.3f元\n今日中奖:%.3f元\n今日盈亏:%.3f元", result.allBetAmount,result.allWinAmount,result.yingkuiAmount)
        let alertController = UIAlertController(title: "今日输赢",
                                                message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "好的", style: .default, handler: {
            action in
            
        })
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    func updateAccount(memInfo:Meminfo,handler:(_ leftMoneyP: String) -> ()) {
        self.meminfo = memInfo
        if !isEmptyString(str: memInfo.balance){
            let leftMoneyName = "\(memInfo.balance)"

            handler(leftMoneyName)
            
            if let value = Double(leftMoneyName) {
                accountBanlance = value
            }
        }
    }
    
    //MARK: - 小助手
    private func setupRightNaviItem() {
        let moreBtn = UIButton(type: .custom)
        moreBtn.frame = CGRect.init(x: 0, y: 0, width: 60, height: 44)
        
        if #available(iOS 11, *){} else {
            if #available(iOS 10, *) {
                moreBtn.frame = CGRect.init(x: 0, y: 0, width: 65, height: 44)
            }
        }
        
        moreBtn.setTitle("小助手", for: .normal)
        moreBtn.theme_setTitleColor("Global.barTextColor", forState: .normal)
        moreBtn.contentHorizontalAlignment = .right
        moreBtn.addTarget(self, action: #selector(showRightTopMenuWhenResponseClick(ui:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: moreBtn)
    }
    
    @objc func showRightTopMenuWhenResponseClick(ui:UIButton) -> Void {
        
        right_top_datasources = right_top_allDataSources
        //frame 为整个popview相对整个屏幕的位置 arrowMargin ：指定箭头距离右边距离
        right_top_menu = SwiftPopMenu(frame:  CGRect(x: KSCREEN_WIDTH - 150-5, y: 56, width: 150, height: 250), arrowMargin: 18)
        // 六合彩、官方PC蛋蛋 无走势图和长龙，官方无长龙，信用PC蛋蛋无走势图
        if ((groupName == "PC蛋蛋" && lotData?.lotVersion == 1) || ((lotData?.code?.contains("LHC"))! || (lotData?.name.contains("六合彩"))!)) && right_top_datasources.count >= 8 {
            right_top_datasources.remove(at: right_top_datasources.count - 1)
            right_top_datasources.remove(at: right_top_datasources.count - 1)
        }
        else if (lotData?.lotVersion == 1) && right_top_datasources.count >= 8{
            right_top_datasources.remove(at: right_top_datasources.count - 1)
        }else if (groupName == "PC蛋蛋" && lotData?.lotVersion != 1 && right_top_datasources.count >= 8)
        {
            right_top_datasources.remove(at: right_top_datasources.count - 2)
        }
        right_top_menu.popData = right_top_datasources
        //点击菜单事件
        right_top_menu.didSelectMenuBlock = { [weak self](index:Int)->Void in
            self?.right_top_menu.dismiss()
            self?.onMenuListClick(index: index)
        }
        right_top_menu.show()
        tag += 1
    }
    
    //右上角悬浮框列表相点击回调事件
    func onMenuListClick(index:Int) -> Void {
        switch index {
        case 0://touzhu record
            viewLotRecord()
            break
        case 1:
            gotoResultsPage()
            break
        case 2://play rule introduction
            playIntroduceHandler?(lotData?.code ?? cpBianHao, lotData?.name ?? (groupName ?? ""), groupCode ?? "" )
        case 3:
            openSettingHandler?()
        case 4:
//            switchTheme()
            if let data = lotData {
                switchThemeBetVCHandler?(data)
            }
            
        case 5:
//            switchSoundEffect()
            switchSoundEffectHandler?()
            
            /// 投注界面-右上角的小助手-添加走势图入口  JK add 19-09-17
        case 6:
            do {
                if (groupName == "PC蛋蛋" && lotData?.lotVersion != 1)
                {
                    gotoLongDragonPageHandler?(cpBianHao, groupName ?? "")
                }
                else {
                    gotTrendCahrtPage()
                }
            }
            break
            /// 投注界面-右上角的小助手-添加长龙
        case 7:
            gotoLongDragonPageHandler?(cpBianHao, groupName ?? "")
            break
            
        default:
            break
        }
    }
    
    //MARK: 投注记录
    func viewLotRecord(){        
        viewLotRecordHandler?()
    }
    
    //MARK:  开奖结果
    @objc func gotoResultsPage(){
        gotoResultsPageHandler?(cpTypeCode,cpBianHao)
    }
    @objc func gotTrendCahrtPage() {
        goTrendChartHandler?(cpTypeCode,cpBianHao)
    }
    
    //MARK:切换音效
    func switchSoundEffect() {
        OnSoundEffectItemClick(dataSource: SoundEffects, viewTitle: "音效切换")
    }
    
    func switchTheme() {
        onItemClick(dataSource: themes,viewTitle: "主题风格切换")
    }
    
    private func OnSoundEffectItemClick(dataSource: [String], viewTitle: String) {
        let currentSoundEffectIndex = YiboPreference.getCurrentSoundEffect()
        let selectedView = LennySelectView(dataSource: dataSource, viewTitle: viewTitle)
        selectedView.selectedIndex = currentSoundEffectIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            YiboPreference.setCurrentSoundEffect(value: index as AnyObject)
        }
        
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
        }
    }
    
    // 切换主题风格
    private func onItemClick(dataSource: [String], viewTitle: String){
    }
    
    //MARK: - datas
    func clearAfterBetSuccess(isRandom:Bool = false,handler:() -> ()) -> Void {
        if !isPeilvVersion(){
            for ball in self.ballonDatas{
                for ballNumInfo in ball.ballonsInfo{
                    ballNumInfo.isSelected = false
                }
                if ball.showWeiShuView{
                    for weishuData in ball.weishuInfo{
                        weishuData.isSelected = false
                    }
                }
            }
        }else{
            for peilvData in self.peilvListDatas{
                let sub = peilvData.peilvs
                for item in sub{
//                    if !isRandom {
                        item.isSelected = false
//                    }
                    
                    item.inputMoney = 0
                }
            }
        }
        self.selectNumList.removeAll()
        
        handler()
    }
        
    /** 当大玩法下的小玩法，含有header，可以选择更多小玩法时，计算所有该打玩法下的注数 */
     func calculateSubHeaderRulesCount() -> Int {
        var localHonest_orders:[PeilvOrder] =  []
        
        for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
            
            if peilvListDatas.count > 0 {
                if !isEmptyString(str: subPlayCode) && peilvListDatas[0].parentCode == subPlayCode {
                    localHonest_orders = localHonest_orders + self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                }
            }
        }
        
        return localHonest_orders.count
    }

    
    //清除下注底部兰的视图数据
    func clearBottomValue(clearModeAndBeiShu: Bool,clear_orders:Bool=true,resetSelectMode:Bool = true,clearBeishu:Bool = true,handler:() -> ()) -> Void {
        if clearModeAndBeiShu {
            selectMode = resetSelectMode ? "y"  : selectMode
            selectedBeishu = clearBeishu ? 1 : selectedBeishu //选择的倍数
        }
        selectedMoney = 0;//总投注金额
        if clear_orders{
            if !switchCanMutiRulesBet(isPeilv: isPeilvVersion()) {
                self.betMoneyWhenPeilv = ""
            }
            
            self.sortWhenSelectPeilvNumber = 0
            self.honest_orders.removeAll()
            self.official_orders.removeAll()
        }
        totalPeilvMoney = 0
        handler()
//        clearBottomUIValue()
    }
    
    func update_lotname_button(handler:(_ name:String) -> ()){
        let name = String.init(format: "%@ %@", self.cpName,(self.lotWindow != nil && self.lotWindow.isShow) ? "<<" : ">>")
        updateLotName?(self.cpName)
        handler(name)
//        self.lot_name_button.setTitle(name, for: .normal)
    }
    
    func updateLocalConstants(lotData:LotteryData?,handler:@escaping (_ name: String) -> ()) -> Void {
        if let lotName = lotData?.name{
            self.cpName = lotName
            self.update_lotname_button(handler: {(name) in
                handler(name)
            })
        }
        if let lotCode = lotData?.czCode{
            self.czCode = lotCode
        }
        if let groupsCode = lotData?.groupCode{
             groupCode = groupsCode
        }
       
        if let lotAgo = lotData?.ago{
            self.ago = lotAgo
            self.disableBetTime = lotAgo
        }
        if let lotCpCode = lotData?.code{
            self.cpBianHao = lotCpCode
            lotCpCodeChangedHandler?(lotCpCode)
        }
        if let lotteryICON = lotData?.lotteryIcon {
            self.lotteryICON = lotteryICON
        }
        if let lotCodeType = lotData?.lotType{
            self.cpTypeCode = String.init(describing: lotCodeType)
        }
        if let lotVersion = lotData?.lotVersion{
            self.cpVersion = String.init(describing: lotVersion)
        }
        self.tickTime = self.ago + Int64(self.offset)
        if isSixMark(lotCode: self.cpBianHao){
            lhcSelect = true
            self.cpVersion = VERSION_2
        }
    }
    
    //MARK: - 公共方法
    //MARK: 是否赔率版下注(信用版)
    func isPeilvVersion() ->  Bool{
        if self.cpVersion == VERSION_2 || lhcSelect{
            return true
        }
        return false
    }
    
    //MARK: - LifeCircle
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupPathView()
        setupRightNaviItem()
        openPopViewinitializationClick(requestSelf:self, controller: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewShowing = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewShowing = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isViewVisible = false
        self.view.endEditing(true)
        shouldPlayKaiJianVolume = false
        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
        if let timer = self.lastKaiJianResultTimer{
            timer.invalidate()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isSixMarkRequest()
    }
    
    //是否是六合彩接口判断,返回数组
    func isSixMarkRequest() {
        request(frontDialog: false, method: .get, url: IS_SIXMARK, callback: { (resultJson:String,resultStatus:Bool) -> Void in
            if !resultStatus {
                return
            }
            
            class SixMarkModel:HandyJSON {
                required init() {}
                var success = false
                var accessToken = ""
                var content = [String]()
            }
            
            if let result = SixMarkModel.deserialize(from: resultJson){
                if result.success{
                    sixMarkArray = result.content
                    if !isEmptyString(str: result.accessToken){
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                    }
                    
                }else {}
            }
        })
    }

    
    //MARK: - Logic
    //MARK: 下注成功的时候提示框
    func showBetSuccessDialog() -> Void {
        let alertController = UIAlertController(title: "温馨提示",
                                                message: "下注成功!", preferredStyle: .alert)
        let viewAction = UIAlertAction(title: "查看记录", style: .cancel, handler: {
            action in
            self.viewLotRecord()
        })
        let okAction = UIAlertAction(title: "继续下注", style: .default, handler: {
            action in
        })
        alertController.addAction(viewAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Bet Logic
    /**
     * 开始计算投注号码串及注数(赔率版)
     * @param datasAfterSelected 非机选投注时，用户已经选择完投注号码的所有赔率列表数据
     */
    func calc_bet_orders_for_honest(datasAfterSelected:[BcLotteryPlay],updateBottomUIHandler:@escaping () ->()) -> Void {
        DispatchQueue.global().async {
            self.selectNumList.removeAll()
            for play in self.peilvListDatas{
                if !play.peilvs.isEmpty{
                    for result in play.peilvs{
                        if datasAfterSelected.count > 0 {
                            result.headerCode = datasAfterSelected[0].code
                        }
                        if result.isSelected{
                            self.selectNumList.append(result)
                        }
                    }
                }
            }
            
            datasAfterSelected.forEach { (BcLotteryPlay) in
                if BcLotteryPlay.code == "siqz" {
                    if self.selectNumList.count > 12 {
                        DispatchQueue.main.async {
                            showToast(view:self.view, txt: "最多只能选择12个号码,超过则不计算注数！")
                        }
                        return ;
                    }
                }
            }
            //计算注数
            let datas = self.lhcLogic.calcOrder(selectDatas: self.selectNumList)
            DispatchQueue.main.async {
                self.honest_orders.removeAll()
                self.honest_orders = datas
                updateBottomUIHandler()
            }
        }
    }
    
    
    func clickSingleDouble(isSingle:Bool,cellPos:Int,handler:() -> ()) -> Void {
        let ball = self.ballonDatas[cellPos]
        for ballInfo in ball.ballonsInfo{
            if isPurnInt(string: ballInfo.num){
                let scanner = Scanner(string: ballInfo.num)
                scanner.scanUpToCharacters(from: CharacterSet.decimalDigits, into: nil)
                var number :Int = 0
                scanner.scanInt(&number)
                if !isSingle{
                    if number % 2 == 0{
                        ballInfo.isSelected = true
                    }else{
                        ballInfo.isSelected = false
                    }
                }else{
                    if number % 2 == 0{
                        ballInfo.isSelected = false
                    }else{
                        ballInfo.isSelected = true
                    }
                }
            }else{
                ballInfo.isSelected = false
            }
        }
        
        handler()
    }
    
    private func update_slide_when_peilvs_obtain(webResults:[PeilvWebResult],handler:(_ rightResult:PeilvWebResult) -> ()){
        if !isPeilvVersion(){
            //这里根据赔率列表来决定是否显示返水拖动条
            if officail_odds.isEmpty{
                return
            }
            
            let result = getMaxOddsFromOfficailOdds(webResults: webResults)
            
            if let rightResult = result {
                handler(rightResult)
            }else{
              //  //                oddSlider.isHidden = true
            }
        }
    }
    
    //获得奖金最大的 PeilvWebResult 对象
    private func getMaxOddsFromOfficailOdds(webResults:[PeilvWebResult]) -> PeilvWebResult? {
        var rightResult:PeilvWebResult?
        
        for odd in webResults{
            if odd.maxOdds > (rightResult?.maxOdds ?? 0) {
                rightResult = odd
            }
        }
        
        return rightResult
    }
    
    //根据侧边大玩法对应的所有小玩法列表，来取出最大的返水比例
    func update_honest_seekbar(selectPlay:BcLotteryPlay,odds:[HonestResult],handler:(_ maxOddResult:PeilvWebResult) -> ()){
        //这里根据赔率列表来决定是否显示返水拖动条
        if isEmptyString(str: cpTypeCode) || isEmptyString(str: selectedSubPlayCode){
            return
        }
        var maxOddResult:PeilvWebResult!
        if !odds.isEmpty{
            for subPlay in selectPlay.children{
                for result in odds{
                    if subPlay.code == result.code{
                        for odd in result.odds{
                            if maxOddResult == nil{
                                maxOddResult = odd
                            }else{
                                if odd.rakeback > maxOddResult.rakeback{
                                    maxOddResult = odd
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
        if maxOddResult != nil{
            handler(maxOddResult)
        }
    }
    
    
    /**
     * 开始计算投注号码串及注数
     * @param selectedDatas 非机选投注时，用户已经选择完投注球的球列表数据
     * @param cpVersion 彩票版本
     * @param czCode 彩种代号
     * @param subCode 小玩法
     */
    func calc_bet_orders(selectedDatas:[BallonRules],selectNumHandler:@escaping (_ rakeback:Float,_ maxOdds:Float,_ minOdds:Float) -> (),updateBottomHandler:@escaping () -> ()) -> Void {
        DispatchQueue.global().async {
            let modeInt = self.convertYJFModeToInt(mode: self.selectMode)
            let orders = LotteryOrderManager.calcOrders(list: selectedDatas, rcode: self.subPlayCode, lotType: Int(self.cpTypeCode)!, rateback: 0, mode: modeInt, beishu: self.selectedBeishu, oddsList: self.officail_odds)
            
            DispatchQueue.main.async {
                self.official_orders.removeAll()
                self.official_orders = self.official_orders + orders
                
                //更新一些特殊玩法的拖动条
                if self.subPlayCode == "qwxczw" || self.subPlayCode == "qwxdds" ||
                    self.subPlayCode == "k3hz2"{
                    self.update_slide_when_user_select_number(handler: {(rakeback,maxOdds, minOdds) in
                        selectNumHandler(rakeback,maxOdds, minOdds)
                    })
                }
                
                updateBottomHandler()
            }
        }
    }
    
    //MARK: - 官方投注
    //准备注单数据并进入主单列表页
    func formBetDataAndEnterOrderPage(order:[OfficialOrder],handler:() -> ()) {
        let order = fromBetOrder(official_orders: order, subPlayName: self.subPlayName, subPlayCode: self.subPlayCode, selectedBeishu: self.selectedBeishu, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: self.current_rate, selectMode: self.selectMode)
        
        handler()
    
        gotoBetNewOfficalHandler?(order, self.cpBianHao,self.cpName,self.subPlayCode, self.subPlayName, self.cpTypeCode,self.cpVersion,self.officail_odds, self.lotteryICON,self.meminfo,self.officalBonus)
    }
    
    private func update_slide_when_user_select_number(handler:@escaping (_ rakeback:Float,_ maxOdds:Float,_ minOdds:Float) -> ()){
        if !isPeilvVersion(){
            //这里根据赔率列表来决定是否显示返水拖动条
            if officail_odds.isEmpty{
                return
            }
            var rightMaxOdds:Float = 0.0;
            var rightMinOdds:Float = 0.0;
            var rightRakeback:Float = 0.0;
            
            for order in self.official_orders{
                var peilv:PeilvWebResult!
                for result in self.officail_odds{
                    if order.i == result.code{
                        peilv = result
                        break;
                    }
                }
                if peilv == nil && !self.officail_odds.isEmpty{
                    peilv = self.officail_odds[0]
                }
                if peilv != nil{
                    if peilv.maxOdds > rightMaxOdds{
                        rightMaxOdds = peilv.maxOdds
                    }
                    if peilv.minOdds > rightMinOdds{
                        rightMinOdds = peilv.minOdds
                    }
                    rightRakeback = peilv.rakeback
                }
            }
            
            if self.official_orders.count == 0 {
                if let webResult = getMaxOddsFromOfficailOdds(webResults: officail_odds) {
                    rightMaxOdds = webResult.maxOdds
                }
            }
            
            handler(rightRakeback,rightMaxOdds,rightMinOdds)
        }
    }
    
    //MARK: 投注
    func commitBetAction(isNormalStyle:Bool,creditbottomTopText:String?,reloadAndClearBottom:@escaping () -> (),formBetDataAndEnterOrderPage:@escaping () -> (),formPeilvDataAndOrder:@escaping () -> ()) {
        
        let accoundMode = YiboPreference.getAccountMode()
        if !self.verifyAccountAndBet() && accoundMode != GUEST_TYPE{
            showToast(view: self.view, txt: "余额不足,请充值")
            if let meminfo = self.meminfo {
                openChargeMoney(controller: self, meminfo: meminfo)
                return
            }
        }
        
        if isPeilvVersion(){
            if self.selectedPlayRulesModel.getTotoalCount() == 0 {
                let errorMsg = lhcLogic.find_bet_unpass_msg(selectDatas: self.selectNumList)
                if !isEmptyString(str: errorMsg){
                    showToast(view: self.view, txt: errorMsg)
                    return
                }
            }
            
            let orderJson = YiboPreference.getTouzhuOrderJson()
            
            if isEmptyString(str: orderJson) {
                
                if selectedPlayRulesModel.getTotoalCount() > 200 {
                    showToast(view: self.view, txt: "最多选中200注")
                    return
                }
                
                if let contentsP = creditbottomTopText {
                    if isEmptyString(str: contentsP) {
                        if !isNormalStyle
                        {
                            showToast(view: self.view, txt: "请输入金额")
                            return
                        }
                    }
                }else {
                    if !isNormalStyle
                    {
                        showToast(view: self.view, txt: "请输入金额")
                        return
                    }
                }
            }
            
            //如果有选择过的存储注单，说明是从购彩列表中返回来再手选一注的，去时点下注需要再进入购彩注单页
//            self.lhcLogic.clearAfterBetSuccess()
//            self.lhcLogic.initializeIndexTitleWithoutIndex()
            self.selectNumList.removeAll()
            
            reloadAndClearBottom()
            
            
            if isMulSelectMode(subCode: self.selectedSubPlayCode) || is_fast_bet_mode{
                
            }else{
                //如果是快捷下注或多选下注时将金额框中的金额遍历存入每个注单
                var allNotMoney = true
                var sb = ""
                for data in self.honest_orders{
                    if data.a == 0{
                        sb.append("\"")
                        sb.append(!isEmptyString(str: data.c) ? data.c : "")
                        sb.append("\",")
                    }else{
                        allNotMoney = false
                    }
                }
                if allNotMoney{
                    showToast(view: self.view, txt: "请输入金额(整数金额)")
                    return
                }
                if sb.count > 0{
                    sb = (sb as NSString).substring(to: sb.count - 1)
                    showToast(view: self.view, txt: sb + "号码未输入金额,请输入后再投注")
                    return
                }
            }
            
            self.formPeilvBetDataAndEnterOrderPage(order: self.honest_orders, peilvs: self.peilvListDatas, lhcLogic: self.lhcLogic,handler: {() -> Void in
                formPeilvDataAndOrder()
            })
            
        }else{
            var total_zhushu = 0
            for order in self.official_orders{
                total_zhushu += order.n
            }
            if total_zhushu == 0{
                showToast(view: self.view, txt: "下注号码不正确，请重新选择")
                return;
            }
            
            //准备数据，进入下一页主单列表
            formBetDataAndEnterOrderPage()
        }
    }
    
    //MARK: - 多玩法投注相关
    //MARK: 格式化赔率版 数据，进入投注页面
    func formPeilvBetDataAndEnterOrderPage(isRandom:Bool = false,order:[PeilvOrder],peilvs:[BcLotteryPlay],lhcLogic:LHCLogic2,handler:() -> ()) {
        var datas = [OrderDataInfo]()

        if isRandom {
            datas = fromPeilvBetOrder(peilv_orders: order, subPlayName: self.subPlayName, subPlayCode: self.subPlayCode, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: self.current_rate)
        }else {
            if peilvListDatasSuperSet.count > 0 {
                for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
                    let data = calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                    
                    for order in data{
                        if !isEmptyString(str: self.betMoneyWhenPeilv) {
                            order.a = Float(self.betMoneyWhenPeilv)!
                        }
                    }
                    
//                    if data.count > 0 {
//                        datas = datas + fromPeilvBetOrder(peilv_orders: data, subPlayName: peilvListDatas[0].parentName, subPlayCode: peilvListDatas[0].parentCode, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: self.current_rate)
//                    }
//                    
                    if data.count > 0 {
                        let name = isEmptyString(str: peilvListDatas[0].fakeParentName) ? peilvListDatas[0].parentName : peilvListDatas[0].fakeParentName
                        datas = datas + fromPeilvBetOrder(peilv_orders: data, subPlayName: name, subPlayCode: peilvListDatas[0].parentCode, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: self.current_rate)
                    }
                }
            }
        }
        
        handler()
        
                gotoBetNewPeilvHandler?(datas,peilvs,lhcLogic,subPlayName,self.subPlayCode,self.cpTypeCode,self.cpBianHao,self.current_rate,self.cpName,self.cpVersion,self.lotteryICON)
    }
    
    /** 用以遍历peilvlistdatas，获得所有 honest_orders 投注数据 */
    func calculateBetDatasWithPeilvListDatas(peilvListDatas:[BcLotteryPlay]) -> [PeilvOrder] {
        var localSelectNumList:[PeilvWebResult] = []
        var localHonest_orders:[PeilvOrder] = []
        let localPeilvListDatas:[BcLotteryPlay] = [] + peilvListDatas
        
        for play in localPeilvListDatas {
            if !play.peilvs.isEmpty{
                for result in play.peilvs{
                    if result.isSelected{
                        if peilvListDatas.count > 0 {
                            result.headerCode = peilvListDatas[0].code
                        }
                        if isEmptyString(str: result.itemName) {
                            result.itemName = play.name
                        }
                        localSelectNumList.append(result)
                    }
                }
            }
        }
        
        //计算注数
        let datas = self.lhcLogic.calcOrder(selectDatas: localSelectNumList,currentCount:self.selectedPlayRulesModel.getTotoalCount())
        
        localHonest_orders = datas
        return localHonest_orders
    }
    
    /**
     * 计算出对应玩法下的号码及赔率面板区域显示数据
     *
     * @param peilvWebResults 赔率数据
     */
    func prapare_peilv_datas_for_tableview(peilvWebResults:[BcLotteryPlay]){
        self.peilvListDatas.removeAll()
        self.peilvListDatas = peilvWebResults
        
        if peilvWebResults.count == 0 {
            return
        }else {
            
            headerName = peilvWebResults[0].name
            headerCode = peilvWebResults[0].code
        }
        
        if peilvListDatasSuperSet.count == 0 {
            peilvListDatasSuperSet = [peilvWebResults]
            self.peilvListDatas = peilvListDatasSuperSet[0]
        }else {
            //判断peilvListDatasSuperSet 是否存在当前选择的 peilvListDatas
            var hadSelected = true
            for (index,peilvs) in peilvListDatasSuperSet.enumerated() {
                
                if !isEmptyString(str: peilvWebResults[0].parentCode) && !isHeaderPlay(parentCode: peilvWebResults[0].parentCode) {
                    //表示已经选择过该左侧边栏玩法
                    if !isEmptyString(str: peilvWebResults[0].fakeParentName) && peilvs[0].fakeParentName == peilvWebResults[0].fakeParentName {
                        self.peilvListDatas = peilvs
                        return
                    }else if peilvs[0].parentCode == peilvWebResults[0].parentCode && isEmptyString(str: peilvWebResults[0].fakeParentName) && isEmptyString(str: peilvs[0].fakeParentName) {
                        //                    if peilvs[0].code == peilvWebResults[0].code {
                        self.peilvListDatas = peilvs
                        return
                    }else if index == peilvListDatasSuperSet.count - 1 {
                        // 未选择过该左侧边栏玩法
                        hadSelected = false
                    }
                }else if !isEmptyString(str: peilvWebResults[0].code){
                    if peilvs[0].code == peilvWebResults[0].code {
                        
                        if peilvs[0].parentCode == "hx" {
                            let peilvsPeilvs = peilvs[0].peilvs
                            let peilvWebResultsPeilvs = peilvWebResults[0].peilvs
                            if peilvsPeilvs.count > 0 && peilvWebResultsPeilvs.count > 0 {
                                if peilvsPeilvs[0].code == peilvWebResultsPeilvs[0].code {
                                    self.peilvListDatas = peilvs
                                    return
                                }else if index == peilvListDatasSuperSet.count - 1 {
                                    // 未选择过该玩法(如"合肖不中"等有两个header可选的玩法)
                                    hadSelected = false
                                }
                            }
                        }else {
                            self.peilvListDatas = peilvs
                            return
                        }
                    }else if index == peilvListDatasSuperSet.count - 1 {
                        // 未选择过该左侧边栏玩法
                        hadSelected = false
                    }
                }
            }
            
            if !hadSelected {
                peilvListDatasSuperSet.append(peilvWebResults)
                if let peilvs = peilvListDatasSuperSet.last {
                    self.peilvListDatas = peilvs
                }
            }
            
        }
    }
    
    //MARK: 获取快速金额
    func getFastMoneySetting() -> [String] {
        var str = ""
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                str = config.content.fast_money_setting
            }
        }
        if isEmptyString(str: str){
            str = "10,20,50,100,200,500,1000,2000,5000"
        }
        
        str = str.trimmingCharacters(in: .whitespaces)
        let moneys = str.components(separatedBy: ",")
        return moneys
    }
    
    //MARK: 圆角分模式
    func convertYJFModeToInt(mode:String) -> Int {
        if mode == YUAN_MODE {return 1}
        else if mode == JIAO_MODE {return 10}
        else if mode == FEN_MODE {return 100}
        else {return 1}
    }
    
    func show_mode_switch_dialog(sender:UIButton,handler:@escaping (_ title: String) -> ()) -> Void {
        let alert = UIAlertController.init(title: "模式切换", message: nil, preferredStyle: .actionSheet)
        
        let mode = YiboPreference.getYJFMode();
        
        if YiboPreference.getYJFMode() == YUAN_MODE{
            let action = UIAlertAction.init(title: "元", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "元",handler: {(contents) in handler(contents)   })
            })
            alert.addAction(action)
        }else if YiboPreference.getYJFMode() == JIAO_MODE{
            let action1 = UIAlertAction.init(title: "元", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "元",handler: {(contents) in handler(contents)   })
            })
            let action2 = UIAlertAction.init(title: "角", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "角",handler: {(contents) in handler(contents)   })
            })
            alert.addAction(action1)
            alert.addAction(action2)
        }else if YiboPreference.getYJFMode() == FEN_MODE{
            let action1 = UIAlertAction.init(title: "元", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "元",handler: {(contents) in handler(contents)   })
            })
            let action2 = UIAlertAction.init(title: "角", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "角",handler: {(contents) in handler(contents)   })
            })
            let action3 = UIAlertAction.init(title: "分", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "分",handler: {(contents) in handler(contents)   })
            })
            alert.addAction(action1)
            alert.addAction(action2)
            alert.addAction(action3)
        }
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        self.present(alert,animated: true,completion: nil)
    }
    
    private func selectModeFromDialog(title:String,handler:(_ title: String) -> ()) -> Void{
        if title == "元"{
            self.selectMode = YUAN_MODE
            multiplyValue = 1.0
        }else if title == "角"{
            self.selectMode = JIAO_MODE
            multiplyValue = 0.1
        }else if title == "分"{
            self.selectMode = FEN_MODE
            multiplyValue = 0.01
        }
        
        handler(title)
    }
    
    //MARK: 根据返水的变化，改变所有球的赔率
    func dynamicCalculateOdds(currentRate: Float,handler:() -> ()) {
        
        var tempHonest_odds:[HonestResult] = []
        for honestIndex in 0..<(self.honest_odds.count) {
            let honestResult: HonestResult = self.honest_odds[honestIndex]
            
            let tempWebResultsArray: Array = honestResult.odds
            var webResultsArray: [PeilvWebResult] = []
            for webIndex in 0..<(tempWebResultsArray.count) {
                let webResult: PeilvWebResult = tempWebResultsArray[webIndex]
                webResult.currentOdds = webResult.maxOdds - currentRate*abs(webResult.maxOdds - webResult.minOdds)
//                webResult.currentSecondOdds = webResult.minOdds - currentRate*abs(webResult.maxOdds - webResult.minOdds)
                
                webResult.currentSecondOdds = webResult.secondMaxOdds - currentRate*abs(webResult.secondMaxOdds - webResult.secondMinodds)
                
                if webResult.currentSecondOdds == 0.0 {
                    webResult.currentSecondOdds = webResult.minOdds
                }
                
                webResultsArray.append(webResult)
            }
            
            honestResult.odds = webResultsArray
            tempHonest_odds.append(honestResult)
        }
        
        self.honest_odds = tempHonest_odds
        handler()
    }
    
    //MARK: -UI
    private func setupPathView()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            let height:CGFloat = 360
            self.lotteryPathView = LotteryPathView.init(frame: CGRect.init(x: 0, y: screenHeight - height, width: screenWidth, height: height))
            self.lotteryPathView.configWithTitles(titles: ["球号","单双","大小"], cpBianHao: self.cpBianHao,lotType:self.cpTypeCode)
            self.view.addSubview(self.lotteryPathView)
        }
        
    }
    
    //MARK: - 完全无UI相关
    //MARK: 验证余额 true可以购买
    func verifyAccountAndBet() -> Bool {
        return betShouldPayMoney <= accountBanlance
    }
    
    
    
    //MARK: 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


class SelectedPlayRulesModel: NSObject {
    var currentIndex = 0 //当前选中的列表
    var currentConunt:Int = 0  { //当前位置的注数
        didSet {
            allIndexAndCount["\(currentIndex)"] = currentConunt
        }
    }
    
    var allIndexAndCount:[String:Int] = [:] //形成注单的列表索引
    
    /** 重置model,如 点击清除按钮时 */
    func clearModel() {
        allIndexAndCount = ["\(currentIndex)":0]
    }
    
    /** 取出所有注数的总和 */
    func getTotoalCount() -> Int {
        var total = 0
        for (_,value) in allIndexAndCount.values.enumerated() {
            total = value + total
        }
        
        return total
    }
    
    func getCurrentCount() -> Int {
        return getCountOfThisPlay(Index:currentIndex)
    }
    
    func getCountOfThisPlay(Index:Int) -> Int {
        if allIndexAndCount.keys.contains("\(Index)") {
            if let value = allIndexAndCount["\(Index)"] {
                return value
            }
        }
        return 0
    }
    
}






