//
//  TouzhOfficalBottom.swift
//  gameplay
//
//  Created by admin on 2018/10/10.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class TouzhOfficalBottom: UIView {

    var betBtnClickHandler:(() -> Void)?
    var clearBtnClickHandler:(() -> Void)?
    var minusClickHandler:((UITapGestureRecognizer) -> Void)?
    var plusClickHandler:((UITapGestureRecognizer) -> Void)?
    var sliderChangeHandler:((CustomSlider) -> Void)?
    var modeBtnClickHandler:((UIButton) -> Void)?
    var moneyTextChangeHandler:((UITextField) -> Void)?
    var plusMultipleHandler:(() -> Void)?
    var minusMultipleHandler:(() -> Void)?
    
    lazy var xibView:UIView = {
        return Bundle.main.loadNibNamed("TouzhOfficalBottom", owner: self, options: nil)?.first as! UIView
    }()
    
    
    @IBOutlet weak var beishu_money_input:CustomFeildText!
    @IBOutlet weak var modeBtn: UIButton!
    
    @IBOutlet weak var leftConsProporW: NSLayoutConstraint!
    @IBOutlet weak var multipleView: UIView!
    
    @IBOutlet weak var sliderViewConstraintH: NSLayoutConstraint!
    @IBOutlet weak var multipleConstraintH: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var sliderBGView: UIView!
    @IBOutlet weak var bottomMoneyTV: UILabel!
    @IBOutlet weak var bottomZhushuTV: UILabel!
    @IBOutlet weak var currentOddTV: UILabel!
    @IBOutlet weak var oddSlider: CustomSlider!
    @IBOutlet weak var official_bet_kick_add: UIImageView!
    @IBOutlet weak var official_bet_kick_minus: UIImageView!
    @IBOutlet weak var ratebackTV: UILabel!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var betBtn: UIButton!
    
    //MARK: - 初始化
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibView.frame = bounds
        addSubview(xibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(self.xibView)
        xibView.frame = self.bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        ratebackTV.textColor = UIColor.colorWithHexString("a2a2a2")
//        currentOddTV.textColor = UIColor.colorWithHexString("a2a2a2")
        
        ratebackTV.textColor = UIColor.white
        currentOddTV.textColor = UIColor.white
        
        modeBtn.layer.cornerRadius = 3.0
        modeBtn.layer.masksToBounds = true
        
        beishu_money_input.layer.cornerRadius = 3.0
        beishu_money_input.layer.masksToBounds = true
        
        setupTheme()
        setupBtn(btn: betBtn)
        setupBtn(btn: clearBtn)
        setupMinus(image: official_bet_kick_minus, ismMinus: true)
        setupMinus(image: official_bet_kick_add, ismMinus: false)
        setupSlider(slider: oddSlider)
        
        beishu_money_input.addTarget(self, action: #selector(moneyTextChange(textField:)), for: UIControl.Event.editingChanged)
        modeBtn.addTarget(self, action: #selector(onModeSwitch(sender:)), for: .touchUpInside)
    }
    
    //MARK: - 私有方法
    //MARK: 主题色
    private func setupTheme() {
        setViewBackgroundColorTransparent(view: sliderBGView, alpha: 0.8,color: UIColor.black)
        setViewBackgroundColorTransparent(view: bottomView, alpha: 0.8,color: UIColor.black)
        setViewBackgroundColorTransparent(view: multipleView, alpha: 0.8,color: UIColor.black)
    }
    
    //MARK: 减少、返水图片
    private func setupMinus(image:UIImageView,ismMinus:Bool) {
        if ismMinus
        {
            image.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(minusClick(ui:))))
        }else
        {
            image.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(plusClick(ui:))))
        }
    }
    
    //MARK: 设置按钮
    private func setupBtn(btn: UIButton) {
        btn.layer.cornerRadius = 3.0
        btn.layer.masksToBounds = true
    }
    
    //MARK: 设置slider
    private func setupSlider(slider:CustomSlider) {
        slider.addTarget(self, action: #selector(sliderChange), for: UIControl.Event.valueChanged)
    }
    
    //MARK: - 事件方法
    
    @objc private func moneyTextChange(textField:UITextField)
    {
        moneyTextChangeHandler?(textField)
    }
    
    @IBAction func minusMultipleAction() {
        minusMultipleHandler?()
    }
    
    @IBAction func plusMultipleAction() {
        plusMultipleHandler?()
    }
    
    //奖金模式点击事件
    @objc func onModeSwitch(sender:UIButton){
        modeBtnClickHandler?(sender)
    }
    
    //MARK: 拖动条滑动事件
    @objc func sliderChange(slider:CustomSlider) -> Void{
        sliderChangeHandler?(slider)
    }
    
    @IBAction func betBtnClick(_ sender: Any) {
        betBtnClickHandler?()
    }
    
    @IBAction func clearBtnClick(_ sender: Any) {
        clearBtnClickHandler?()
    }
    
    @objc private func minusClick(ui:UITapGestureRecognizer) {
        self.minusClickHandler?(ui)
    }
    
    @objc private func plusClick(ui:UITapGestureRecognizer) {
        self.plusClickHandler?(ui)
    }

}
