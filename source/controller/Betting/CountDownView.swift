//
//  CountDownView.swift
//  gameplay
//
//  Created by admin on 2018/10/9.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class CountDownView: UIView {
    
    lazy var xibView:UIView = {
        return Bundle.main.loadNibNamed("CountDownXib", owner: self, options: nil)?.first as! UIView
    }()
    
    @IBOutlet weak var hour0Label: UILabel!
    @IBOutlet weak var hour1Label: UILabel!
    @IBOutlet weak var minute0Label: UILabel!
    @IBOutlet weak var minute1Label: UILabel!
    @IBOutlet weak var second0Label: UILabel!
    @IBOutlet weak var second1Label: UILabel!
    
    @IBOutlet weak var firstHourLabel: UILabel!
    @IBOutlet weak var firstHourWidthConstraint: NSLayoutConstraint!
    var countdownString = ""
    
    lazy var timeLabelArray:[UILabel] = {
        var array = [UILabel]()
        array.append(firstHourLabel)
        array.append(hour0Label)
        array.append(hour1Label)
        array.append(minute0Label)
        array.append(minute1Label)
        array.append(second0Label)
        array.append(second1Label)

        setupLabelWithLabelArray(array: array)
        return array
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        firstHourLabel.isHidden = true
        firstHourWidthConstraint.constant = 0
        xibView.frame = bounds
        addSubview(xibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(xibView)
        xibView.frame = self.bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViewBackgroundColorTransparent(view: self)
    }
    
    /// 设置时间显示
    ///
    /// - Parameter time: 没有空格的 时间字符串，格式必须为"00:00:00",其中‘:’可以为任意字符
    public func configWithTime(time:String) {
        setupTimeWithTime(time: time)
    }
    
    //UI统一设置
    private func setupLabelWithLabelArray(array: [UILabel]) {
        for label in array {
            setupNoPictureAlphaBgViewAndRadius(view: label)
        }
    }
    
    /** 设置时间 */
    private func setupTimeWithTime(time:String) {

        let timeP = deleteCharterWithoutInt(string: time)
        countdownString = timeP
    
        if timeP.length > 6 {
            firstHourLabel.isHidden = false
            firstHourWidthConstraint.constant = 23
        }else{
            firstHourLabel.isHidden = true
            firstHourWidthConstraint.constant = 0
        }
        for index in 0..<timeP.length{
            let string = timeP.subString(start: index, length: 1)
            if timeP.length == 6{
                timeLabelArray[index + 1].text = string
            }else{
                timeLabelArray[index].text = string
            }
            
        }
    }
    
    private func deleteCharterWithoutInt(string:String) -> String {
        if string.contains(":") {
            let values = string.components(separatedBy: ":")
            
            var finalValue = ""
            for (_,value) in values.enumerated() {
                finalValue += value
            }
            
            return finalValue
        }
        
        
        return string
    }
    
    private func setupNoPictureAlphaBgViewAndRadius(view:UIView?) {
        view?.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view?.layer.cornerRadius = 3.0
        view?.layer.masksToBounds = true
    }
    
}
