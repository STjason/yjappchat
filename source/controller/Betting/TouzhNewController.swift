 
//  JianjinTouzhNewController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2017/12/15.
//  Copyright © 2017年 com.lvwenhan. All rights reserved.
//


class TouzhNewController: TouzhuBaseCntroller,UITableViewDataSource,UITableViewDelegate,
CellBtnsDelegate,PeilvCellDelegate,UITextFieldDelegate,SeekbarChangeEvent,LotsMenuDelegate{
    
    //切换彩种的标题回调
    var backLotterTitleCloser:((_ title : String)->(Void))?
    /** 敬请期待 提示框 */
    var _messageLabel : UILabel?
    
    var dealBetFlag = true //防止短时间内多次点击,true可点击，false不可点击
    var lastCountdownText = ""
    var updateTitleHandler:((_ title:String) -> Void)?
    
    
    @IBOutlet weak var lotNameBtnConsH: NSLayoutConstraint!
    @IBOutlet weak var leftDrawTopView: UIView!
    @IBOutlet weak var numViewTri: NSLayoutConstraint!
    @IBOutlet weak var officialOddsSliderH: NSLayoutConstraint!
    @IBOutlet weak var chipsBgView: UIView!
    @IBOutlet weak var lastQihaoConstTop: NSLayoutConstraint!
    @IBOutlet weak var NumsConstbottom: NSLayoutConstraint!
    @IBOutlet weak var headerBottom: UIView!
    @IBOutlet weak var playPanBGView: UIView!
    @IBOutlet weak var bet_kick_minus: UIImageView!
    @IBOutlet weak var bet_kick_add: UIImageView!
    @IBOutlet weak var official_bet_kick_minus: UIImageView!
    @IBOutlet weak var official_bet_kick_add: UIImageView!
    @IBOutlet weak var touzhuHeaderBgView: UIView!
    @IBOutlet weak var playPaneTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var recentTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var normalModeButton: UIButton!
    @IBOutlet weak var fastModeButton: UIButton!
    @IBOutlet weak var bottomDottedLine: UIView!
    @IBOutlet weak var topDottedLine: UIView!
    @IBOutlet weak var bottomViewTopLine: UIView!
    @IBOutlet weak var bottomLine: UIView! // 顶部视图的
    @IBOutlet weak var recent_open_result_tableview:UITableView!//底层最近开奖结果列表
    @IBOutlet weak var line_between_header_and_betarea:UIView!//投注栏头部与下注区的分隔线
    @IBOutlet weak var topHeaderImg:UIImageView!//头部背景
    @IBOutlet weak var lastQihaoUI:UILabel!//上一期期号
    @IBOutlet weak var currentQihaoUI:UILabel!//当前期号
    @IBOutlet weak var countDownUI:UILabel!//倒计时时间
    @IBOutlet weak var numViews:BallsView!//开奖号码
    @IBOutlet weak var countDownView: CountDownView!
    @IBOutlet weak var exceptionNumTV:UILabel!//没有开奖结果时的文字
    @IBOutlet weak var verticalLine:UIImageView!//分割线
    @IBOutlet weak var topVerticalLine: UIView!
    @IBOutlet weak var betDeadlineDesLabel: UILabel!
    @IBOutlet weak var balanceButton: UIButton!
    @IBOutlet weak var middleHeaderView:UIView!//头部栏中间view
    @IBOutlet weak var lot_name_button:UIButton!//当前彩票按钮
    @IBOutlet weak var random_bet_button:UIButton!//机选按钮
    @IBOutlet weak var random_bet_imgBtn: UIButton!
    @IBOutlet weak var recent_open_result_button:UIButton!//最近几期开奖结果按钮
    @IBOutlet weak var current_play_label:UILabel!//当前玩法
    @IBOutlet weak var play_introduce_view:UIView!//玩法说明view
    @IBOutlet weak var play_introduce_label:UILabel!//玩法说明文字
    @IBOutlet weak var coolHotButton: UIButton!
    @IBOutlet weak var missingNumButton: UIButton!
    @IBOutlet weak var bet_mode_switch_view:UIView!//下注模式切换view
    @IBOutlet weak var fast_bet_button:UIButton!//左边快捷下注
    @IBOutlet weak var normal_bet_button:UIButton!//右边普通下注
    @IBOutlet weak var pullpushBar:UIButton!//玩法推拉条
    @IBOutlet weak var pullpushButton: UIButton!
    @IBOutlet weak var playRuleTableView:UITableView!//玩法列表
    @IBOutlet weak var playPaneTableView:UITableView!//玩法球列表
    @IBOutlet weak var creditBottomTopBar: UIView!
    @IBOutlet weak var creditBottomTopFastImgBtn: UIButton!
    @IBOutlet weak var creditbottomTopField: CustomFeildText!
    @IBOutlet weak var bottomtopTipsLabel: UILabel!
    @IBOutlet weak var creditbottomSlideLeftLabel: UILabel!
    @IBOutlet weak var balanceUI: UILabel!
    @IBOutlet weak var creditBottomHistoryLabel: UILabel!
    @IBOutlet weak var creditBottomHistoryImg: UIImageView!
    @IBOutlet weak var creditBottomHistoryButton: UIButton!
    @IBOutlet weak var creditbottomTopSlider: CustomSlider!
    @IBOutlet weak var bottomBgBarHeight: NSLayoutConstraint!
    @IBOutlet weak var creditSliderBgBar: UIView!
    
    //底部栏控件
    @IBOutlet weak var money_beishu_mode_view:UIView!
    @IBOutlet weak var bet_record_view:UIView!
    @IBOutlet weak var modeBtn:UIButton!
    @IBOutlet weak var beishuTV:UILabel!
    @IBOutlet weak var beishu_money_input:CustomFeildText!
    @IBOutlet weak var ratebackTV:UILabel!
    @IBOutlet weak var oddSlider:CustomSlider!
    @IBOutlet weak var currentOddTV:UILabel!
    @IBOutlet weak var clearBtn:UIButton!
    @IBOutlet weak var betBtn:UIButton!
    @IBOutlet weak var bottomZhushuTV:UILabel!
    @IBOutlet weak var bottomMoneyTV:UILabel!
    @IBOutlet weak var playRuleLayoutConstraint:NSLayoutConstraint!
    @IBOutlet weak var playPaneLayoutConstraint:NSLayoutConstraint!
    //    @IBOutlet weak var playPaneHeightLayoutConstraint:NSLayoutConstraint!
    @IBOutlet weak var resultTBTOP: NSLayoutConstraint!
   //加倍数
    @IBOutlet weak var addBetBtn: UIButton!
    var moreDrawInfoView = UIView()
    let rightMoreInfoViewW:CGFloat = 105
    
    //减倍数
    @IBOutlet weak var subBetBtn: UIButton!
    @IBAction func minusMultipleAction() {
        if isEmptyString(str: beishu_money_input.text!) {
            beishu_money_input.text = "1"
        }else {
            let nowNumP = Int(beishu_money_input.text!)
            if let nowNum = nowNumP {
                var nowChangeNum = nowNum
                if nowChangeNum > 1 {
                    nowChangeNum -= 1
                    beishu_money_input.text = "\(nowChangeNum)"
                    
                    self.selectedBeishu = nowChangeNum
                    self.updateBottomUI()
                    
                    //                    let str = String.init(format: "%.3f", awardNum)
                    if let curentOddTvNum = Float(awardNum) {
                        let lastCurrentOddTVNum = Double(curentOddTvNum) * Double(nowChangeNum)
                        let string = String.init(format: "%.3", lastCurrentOddTVNum)
                        if let doubleValue = Double(string) {
                            bottomMoneyTV.text = "\(doubleValue)元"
                        }
                    }
                }
            }
        }
    }
    
    /** 输入框代理方法 限制输入内容 */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location > 6{
            showToast(view: self.view, txt: "当前允许输入最大长度为7位")
            return false
        }
        return true
    }
    @IBAction func plusMultipleAction() {
        if isEmptyString(str: beishu_money_input.text!) {
            beishu_money_input.text = "1"
        }else {
            let nowNumP = Int(beishu_money_input.text!)
            if let nowNum = nowNumP {
                var nowChangeNum = nowNum
                nowChangeNum += 1
                beishu_money_input.text = "\(nowChangeNum)"
                
                self.selectedBeishu = nowChangeNum
                self.updateBottomUI()
                
                if let curentOddTvNum = Float(awardNum) {
                    let lastCurrentOddTVNum = Double(curentOddTvNum) * Double(nowChangeNum)
                    let string = String.init(format: "%.3", lastCurrentOddTVNum)
                    if let doubleValue = Double(string) {
                        bottomMoneyTV.text = "\(doubleValue)元"
                    }
                    
                }
            }
        }
    }
    
//    lazy var dragBtn: FloatDragButton = {
//        var dragBtn = FloatDragButton(frame: CGRect(x: screenWidth - 60, y: 200, width: 60, height: 60))
//
//        dragBtn.clickClosure = {
//            [weak self]
//            (dragBtn) in
//            self?.dragButtonClickAction(dragBtn)
//        }
//
//        dragBtn.autoDockEndClosure = {
//            [weak self]
//            (dragBtn) in
//
//            self?.navigationController?.interactivePopGestureRecognizer?.isEnabled = dragBtn.x > 0
//        }
//        return dragBtn
//    }()
//
//    func dragButtonClickAction(_ btn : FloatDragButton) {
//        self.navigationController?.pushViewController(ChatViewController(), animated: true)
//    }
    
    //MARK: - showNewFunctionTipsPage
    private func showNewFunctionTipsPage() {
        let showNewFunctionPage = YiboPreference.getShowBetNewfunctionTipsPage()
        
        if showNewFunctionPage == "off" {return}
        
        if isEmptyString(str: showNewFunctionPage) {
            let tipsViewButton = self.getNewFunctionTipsPage(image: "chattingRoom")
            tipsViewButton.addTarget(self, action: #selector(showNextTipsView), for: .touchUpInside)
        }
    }
    
    @objc func showNextTipsView(sender: UIButton) {
        sender.isHidden = true
        sender.removeFromSuperview()
        YiboPreference.setShowBetNewfunctionTipsPage(value: "off")
    }
    
    private func getNewFunctionTipsPage(image: String) -> UIButton {
        let tipsViewButton = UIButton()
        tipsViewButton.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(tipsViewButton)
        tipsViewButton.setBackgroundImage(UIImage.init(named: image), for: .normal)
        
        return tipsViewButton
    }
    
    
    //无法抽取—主题改变时，刷新头视图 当前期号、开奖结果
    @objc func themeChanged() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.updateCurrenQihao()
            self.updateLastKaiJianResult()
        }
    }
    
    private func updateCurrenQihao() {
        let lotteryStr = String.init(format: "距第 %@期", trimQihao(currentQihao: self.currentQihao))
        if isPeilvVersion() {
            let attributeString = NSMutableAttributedString(string: lotteryStr)
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, 3))
            
            let themeColor = self.getHeaderTextColorWithThemeChange()
            
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: themeColor, range: NSMakeRange(3, (lotteryStr.length - 3)))
            
            self.currentQihaoUI.attributedText = attributeString
            
            self.countDownUI.theme_textColor = "Global.themeColor"
        }else {
            self.currentQihaoUI.textColor = UIColor.white
            self.currentQihaoUI.text = lotteryStr
            
            self.countDownUI.textColor = UIColor.white
        }
    }
    
    private func updateLastKaiJianResult() {
        if self.recentResults.isEmpty{
            return
        }
        
        let firstResult:BcLotteryData = self.recentResults[0]
        if isEmptyString(str: firstResult.qiHao) || isEmptyString(str: firstResult.haoMa){
            return
        }
        
        let qihaoStr = trimQihao(currentQihao: firstResult.qiHao)
        self.firstResultQiHao = qihaoStr
        if isPeilvVersion() {
            let lotteryStr = String.init(format: "第 %@期 开奖结果", qihaoStr)
            let attributeString = NSMutableAttributedString(string: lotteryStr)
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, 2))
            
            let themeColor = self.getHeaderTextColorWithThemeChange()
            
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: themeColor, range: NSMakeRange(2, (lotteryStr.length - 2 - 5)))
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange((lotteryStr.length - 4 - 1), 5))
            
            self.lastQihaoUI.attributedText = attributeString
        }else {
            self.lastQihaoUI.text = String.init(format: "第 %@期 开奖结果", qihaoStr)
            self.lastQihaoUI.textColor = UIColor.white
        }
    }
    
    private func getHeaderTextColorWithThemeChange() -> UIColor {
        
        let themeBgName = YiboPreference.getCurrentThmeByName()
        if themeBgName == "FrostedOrange" {
            return UIColor.white
        }else {
            return UIColor.red
        }
    }
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        playRuleTableView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        playPaneTableView.estimatedRowHeight = 200
        
        setupthemeBgView(view: self.view, alpha: 0)
        
        setupNavgation()
        
        switchThemeHandler = {() -> Void in
            self.playRuleTableView.reloadData()
            self.playPaneTableView.reloadData()
        }
        
        //无法抽取—主题改变时，刷新头视图 当前期号、开奖结果
        NotificationCenter.default.addObserver(self, selector: #selector(themeChanged), name: Notification.Name(rawValue: ThemeUpdateNotification), object: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            
            if let sysconfig = getSystemConfigFromJson() {
                var step = sysconfig.content.rateback_step_offset
                step = step.trimmingCharacters(in: .whitespaces)
                if !isEmptyString(str: step){
                    self.fixRateStep = Float(step)!
                }
            }
        }
        
        //在iPhone 5系列机型上 做些特殊设置
        if UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone 5" {
            resultTBTOP.constant = 35
        }
        
        if #available(iOS 11.0, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
        setupTheme()
        // 默认的 UI相关处理
        setupUI()
        
        refreshLotteryTime()
        
        balanceButton.theme_setTitleColor("FrostedGlass.Touzhu.separateLineColor", forState: .normal)
        
        coolHotButton.theme_setImage("TouzhOffical.checkbox_normal", forState: .normal)
        coolHotButton.theme_setImage("TouzhOffical.checkbox_selected", forState: .selected)
        coolHotButton.addTarget(self, action: #selector(TouzhNewController.clickCoolHotButton), for: .touchUpInside)
        
        missingNumButton.theme_setImage("TouzhOffical.checkbox_normal", forState: .normal)
        missingNumButton.theme_setImage("TouzhOffical.checkbox_selected", forState: .selected)
        missingNumButton.addTarget(self, action: #selector(TouzhNewController.clickMissingNumButton), for: .touchUpInside)
        
        fastModeButton.theme_setImage("TouzhOffical.checkbox_normal", forState: .normal)
        fastModeButton.theme_setImage("TouzhOffical.checkbox_selected", forState: .selected)
        fastModeButton.addTarget(self, action: #selector(TouzhNewController.clickFastModeButton), for: .touchUpInside)
        fastModeButton.imageView?.contentMode = .scaleAspectFit
        
        normalModeButton.theme_setImage("TouzhOffical.checkbox_normal", forState: .normal)
        normalModeButton.theme_setImage("TouzhOffical.checkbox_selected", forState: .selected)
        normalModeButton.addTarget(self, action: #selector(TouzhNewController.clickNormalModeButton), for: .touchUpInside)
        normalModeButton.imageView?.contentMode = .scaleAspectFit
        
        pullpushButton.theme_setBackgroundImage("TouzhOffical.handleLeft", forState: .normal)
        pullpushButton.addTarget(self, action: #selector(clickPlayRulePushpullBar), for: .touchUpInside)
        
        //初始化玩法推拉条相关数据
        pullpushBar.addTarget(self, action: #selector(clickPlayRulePushpullBar), for: .touchUpInside)
        pullpushBar.titleLabel?.font = UIFont(name: "Helvetica", size: 14)
        pullpushBar.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        pullpushBar.titleLabel?.numberOfLines = 0
        pullpushBar.setTitleColor(UIColor.init(hex: 0xF36664), for: .normal)
        pullpushBar.setTitleColor(UIColor.init(hex: 0xEF241D), for: .highlighted)
        //初始化并自定义标题兰
        self.customTitleView()
        
        self.recent_open_result_tableview.tag = RECENT_RESULTS_TABLEVIEW_TAG
        recent_open_result_tableview.delegate = self
        recent_open_result_tableview.dataSource = self
        recent_open_result_tableview.separatorInset = .zero
        recent_open_result_tableview.backgroundColor = UIColor.init(hex: 0xf2f1f8)
        line_between_header_and_betarea.theme_backgroundColor = "FrostedGlass.Touzhu.separateLineColor"
        
        play_introduce_view.isUserInteractionEnabled = true
        play_introduce_view.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(gotoResultsPage)))
        
        //绑定开奖号码视图的点击事件，打开开奖结果列表
        numViews.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(gotoResultsPage)))
        
        
        let headerBottomGesture = UITapGestureRecognizer.init(target: self, action: #selector(onOpenNumberClick))
        self.headerBottom.addGestureRecognizer(headerBottomGesture)
        
        recent_open_result_button.addTarget(self, action: #selector(onOpenNumberClick), for: .touchUpInside)
        
        initPlayRuleTable()
        initPlayPaneTable()
        
//        creditBottomTopFastImgBtn.addTarget(self, action: #selector(onFastMoneyClick(sender:)), for: .touchUpInside)
        
        random_bet_imgBtn.imageView?.contentMode = .scaleAspectFit
        random_bet_imgBtn.addTarget(self, action: #selector(onRandomBetClick), for: .touchUpInside)
        random_bet_button.addTarget(self, action: #selector(onRandomBetClick), for: .touchUpInside)
        lot_name_button.addTarget(self, action: #selector(onLotSwitch), for: .touchUpInside)
        fast_bet_button.addTarget(self, action: #selector(onFastBetButton), for: .touchUpInside)
        normal_bet_button.addTarget(self, action: #selector(onNormalBetButton), for: .touchUpInside)
        
        bet_record_view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(betRecordClickEvent(_:)))
        bet_record_view.addGestureRecognizer(tap)
        
        //init and bind ui response event
        modeBtn.addTarget(self, action: #selector(onModeSwitch(sender:)), for: .touchUpInside)
        
        beishu_money_input.addTarget(self, action: #selector(moneyTextChange(textField:)), for: UIControl.Event.editingChanged)
        beishu_money_input.delegate = self
        
        creditbottomTopField.addTarget(self, action: #selector(creditMoneyTextChange(textField:)), for: UIControl.Event.editingChanged)
        creditbottomTopField.delegate = self
        
        oddSlider.addTarget(self, action: #selector(sliderChange), for: UIControl.Event.valueChanged)
        oddSlider.delegate = self
        
        creditbottomTopSlider.addTarget(self, action: #selector(creditSliderChanged(slider:event:)), for: .valueChanged)
        
        
        creditbottomTopSlider.delegate = self
        
        betBtn.addTarget(self, action: #selector(click_bet_button), for: .touchUpInside)
        
        clearBtn.backgroundColor = UIColor.init(hexString: "9c9c9c")
        clearBtn.addTarget(self, action: #selector(click_bottom_clear_button), for: .touchUpInside)
        
        //当键盘弹起的时候会向系统发出一个通知，
        //这个时候需要注册一个监听器响应该通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        //当键盘收起的时候会向系统发出一个通知，
        //这个时候需要注册另外一个监听器响应该通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        //开始根据彩种信息获取对应的玩法
        //根据彩种信息更新本地彩票相关变量
        super.updateLocalConstants(lotData: self.lotData,handler: {[weak  self] (name) in
            guard let weakSelf = self else {return}
            if weakSelf.lotData!.lotType == 4 { //快三彩种开奖结果像左偏移开奖号码，用来显示 和 大小 单双
                weakSelf.numViewTri.constant = weakSelf.rightMoreInfoViewW
            }else {
                weakSelf.numViewTri.constant = 0
                weakSelf.moreDrawInfoView.removeFromSuperview()
            }
            
            weakSelf.lot_name_button.setTitle(name, for: .normal)
        })
        
        super.sync_playrule_from_current_lottery(lotType: self.cpTypeCode, lotCode: self.cpBianHao, lotVersion: self.cpVersion,handler: {(lotteryData) -> Void in
            self.sync_local_constan_restart_something_after_playrule_obtain(lottery: lotteryData)
        })

        lhcLogic.playButtonDelegate = self
        lhcLogic.initAllDatas()

        //johnson add 2018-10-09
        bet_kick_minus.isUserInteractionEnabled = true
        bet_kick_add.isUserInteractionEnabled = true
        bet_kick_minus.tag = 100
        bet_kick_add.tag = 101

        official_bet_kick_minus.isUserInteractionEnabled = true
        official_bet_kick_add.isUserInteractionEnabled = true
        official_bet_kick_minus.tag = 200
        official_bet_kick_add.tag = 201

        official_bet_kick_minus.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        official_bet_kick_add.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))

        bet_kick_minus.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        bet_kick_add.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        //johnson end

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.setupJuderTimer()
        }


        // 处理玩法禁用 不展示子玩法项 jk
        lhcLogic.blockProhibitPlay = {
            self.peilvListDatas = []
            self.playPaneTableView.reloadData()

            self.messageLabel().isHidden = false
        }
    }
    
    private func setupNavgation() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
    }
    
    @objc override func onBackClick(){
        if self.navigationController != nil{
            let count = self.navigationController?.viewControllers.count
            if count! > 1{
                
                customPop()
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func customPop() {
        
        if let vcArray = self.navigationController?.viewControllers {
            
            if let firstController = vcArray.first {
                if let _ = firstController.presentingViewController  {
                    firstController.dismiss(animated: false, completion: nil)
                }else {
                    self.navigationController?.popToViewController(firstController, animated: true)
                }
            }else {
                self.navigationController?.popViewController(animated: true)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "signOutToFirstTabNoti"), object: nil)
            }
        }
    }
    
    //返水条加减号点击事件
    //johnson add 2018-10-09
    @objc func onKickbackRaiseSubtract(ui:UITapGestureRecognizer){
        let tag = ui.view?.tag
        //        if self.fixRateStep == 0{
        //            return
        //        }
        if tag == 100 || tag == 101{
            var value = creditbottomTopSlider.value
            var stepProgress:Float = 0
            if creditbottomTopSlider.maxRakeback > 0{
                stepProgress = Float(self.fixRateStep)/creditbottomTopSlider.maxRakeback
            }
            if self.fixRateStep > creditbottomTopSlider.maxRakeback || self.fixRateStep == 0{
                if tag == 101{
                    value = creditbottomTopSlider.value + 0.01
                }else{
                    value = creditbottomTopSlider.value - 0.01
                }
                creditbottomTopSlider.setValue(value, animated: true)
                creditbottomTopSlider.changeSeekbar(currentProgress: creditbottomTopSlider.value,reload:true)
                return
            }
            if tag == 100{
                value = value - stepProgress
            }else{
                value = value + stepProgress
            }
            creditbottomTopSlider.changeSeekbarStepFixRate(fixRate: self.fixRateStep, currentProgress: value,add: tag == 101,reload: true)
        }else if tag == 200 || tag == 201{
            var value = oddSlider.value
            var stepProgress:Float = 0
            if oddSlider.maxRakeback > 0{
                stepProgress = Float(self.fixRateStep)/oddSlider.maxRakeback
            }
            if self.fixRateStep > oddSlider.maxRakeback  || self.fixRateStep == 0{
                var value:Float = 0
                if tag == 201{
                    value = oddSlider.value + 0.01
                }else{
                    value = oddSlider.value - 0.01
                }
                oddSlider.setValue(value, animated: true)
                oddSlider.changeSeekbar(currentProgress: oddSlider.value,reload:true)
                return
            }
            if tag == 200{
                value = value - stepProgress
            }else{
                value = value + stepProgress
            }
            oddSlider.changeSeekbarStepFixRate(fixRate: self.fixRateStep, currentProgress: value,add: tag == 201,reload: true)
        }
    }
    //johnson end add
    
    //MARK: - 键盘响应
    @objc override func keyboardWillShow(notification: NSNotification) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            if let userInfo = notification.userInfo,
                let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
                let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
                let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
                let frame = value.cgRectValue
                let intersection = frame.intersection(self.view.frame)
                let deltaY = intersection.height
                
                if self.keyBoardNeedLayout {
                    self.playPaneTopConstraint.constant = abs(deltaY - 160)
                    UIView.animate(withDuration: duration, delay: 0.0,
                                   options: UIView.AnimationOptions(rawValue: curve),
                                   animations: {
                                    self.view.frame = CGRect.init(x:0,y:-deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                                    self.keyBoardNeedLayout = false
                                    self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
    }
    
    //键盘隐藏响应
    @objc override func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
            
            playPaneTopConstraint.constant = 1
            UIView.animate(withDuration: duration, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            self.view.frame = CGRect.init(x:0,y:deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    private func selectMoneyFromDialog(money:String){
        if isEmptyString(str: money){
            return
        }
        self.betMoneyWhenPeilv = money
        self.updateBottomUI()
        creditbottomTopField.text = money
    }
    
    //快选金额点击事件
    @objc func onFastMoneyClick(sender:UIButton){
        let moneys = getFastMoneySetting()
        
        if moneys.isEmpty{
            showToast(view: self.view, txt: "没有快选金额，请联系客服")
            return
        }
        
        for m in moneys{
            if !isPurnInt(string: m){
                showToast(view: self.view, txt: "配置的快选金额格式不正确")
                return
            }
        }
        let alert = UIAlertController.init(title: "快选金额", message: nil, preferredStyle: .actionSheet)
        for m in moneys{
            let action = UIAlertAction.init(title: m, style: .default, handler: {(action:UIAlertAction) in
                self.selectMoneyFromDialog(money: m)
            })
            alert.addAction(action)
        }
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        self.present(alert,animated: true,completion: nil)
    }
    
    //获取到彩种及玩法信息后，更新本地相关变量，重新开始获取开奖结果，重新当前旗号和开始倒计时
    func sync_local_constan_restart_something_after_playrule_obtain(lottery:LotteryData?) -> Void {
        if let lot = lottery{
            self.lotData = lot
        }
        //根据彩种信息更新本地彩票相关变量
        refreshBottomBar()
        super.updateLocalConstants(lotData: self.lotData,handler: {(name) in
            self.lot_name_button.setTitle(name, for: .normal)
        })
        input_hint()
        update_lotname_button(handler: {(name) in
            self.lot_name_button.setTitle(name, for: .normal)
        })
        update_bet_header_seperator_line()
        update_mode_money_label_after_sync()
        swtichTouzhuHeader()
        switch_award_ui_after_obtain()
        refreshButtons()
        if (self.lotData?.rules.isEmpty)!{
            return
        }
        //奖获取到的玩法平铺成一级数组列表，展现出来的玩法是所有叶子结点的玩法数据
        if let lottery = self.lotData{
            let rules:[BcLotteryPlay] = PlayTool.leaf_play_rules(lottery: lottery)
            if !rules.isEmpty{
                playRules.removeAll()
                
                if let rules = handlePlayRules(czCode: lottery.czCode ?? "", rulesP: rules) {
                    playRules = rules
                    self.ruleNameLength = getLotNameLength(data: playRules)
                    
                    playRulePushpullBarAction(duration: 0,hidePlayBar: playRules.count == 1)
                }
            }
        }
        playRuleTableView.reloadData()
        if playRules.isEmpty{
            return
        }
        let first_play_obj = playRules[0]
        
        shouldPlayKaiJianVolume = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.shouldPlayKaiJianVolume = true
        }
        self.handlePlayRuleClick(playData: first_play_obj, position: 0)
        //获取开奖结果
        lastOpenResult(cpBianHao:self.cpBianHao, lastLotteryExcpHandler: {(contents) in
            self.updateLastKaiJianExceptionResult(result: contents)
        }, updateLotteryHandler: {(results) in
            self.updateLastKaiJianResult(result:results)
        })
        //获取截止下注倒计时
        self.getCountDownByCpCodeAction()
        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
        //获取冷热遗漏(官方版专有)
        if !isPeilvVersion(){
            super.getCodeRank(lotCode: cpBianHao,getDataSuccess:{() in
                self.playPaneTableView.reloadData()
            })
        }
        
        //获取完彩种信息并同步玩法数据后，同步一下六合彩的服务器时间
        if let code = self.lotData?.code{
            startSyncLhcServerTime(lotCode: code)
        }
    }
    
    func update_bet_header_seperator_line(){
        if isPeilvVersion(){
            verticalLine.isHidden = false
        }else{
            verticalLine.isHidden = true
        }
    }
    
    func switch_award_ui_after_obtain(){
        if isPeilvVersion(){
            currentOddTV.isHidden = true
        }else{
            currentOddTV.isHidden = false
        }
    }
    
    @objc func betRecordClickEvent(_ recongnizer: UIPanGestureRecognizer) {
        super.viewLotRecord()
    }
    
    func update_mode_money_label_after_sync(){
        if isPeilvVersion(){
            modeBtn.setTitle("金额", for: .normal)
            modeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            beishuTV.isHidden = true
        }else{
            modeBtn.setTitle(str_from_mode(mode: self.selectMode), for: .normal)
            modeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            beishuTV.isHidden = false
        }
    }
    
    func switch_bottom_money_input_when_betmode_change(){
        if is_fast_bet_mode{
            money_beishu_mode_view.isHidden = false
            bet_record_view.isHidden = true
        }else{
            money_beishu_mode_view.isHidden = true
            bet_record_view.isHidden = false
        }
    }
    
    func input_hint() -> Void {
        if isPeilvVersion(){
            beishu_money_input.placeholder = "请输入金额"
        }else{
            beishu_money_input.placeholder = "1"
        }
    }
    
    //自定义标题栏，方便点击标题栏切换彩票版本
    func customTitleView() -> Void {
        self.navigationItem.titleView?.isUserInteractionEnabled = true
        let titleView = UIView.init(frame: CGRect.init(x: kScreenWidth/4, y: 0, width: kScreenWidth/2, height: 44))
        titleBtn = UIButton.init(frame: CGRect.init(x: titleView.bounds.width/2-56, y: 0, width: 100, height: 44))
        titleBtn.isUserInteractionEnabled = false
        titleView.addSubview(titleBtn)
        if getLotVersionConfig() == VERSION_V1V2{
            
            titleIndictor = UIImageView.init(frame: CGRect.init(x:titleBtn.x + titleBtn.bounds.width, y: 22-6, width: 12, height: 12))
            
            titleIndictor.theme_image = "FrostedGlass.Touzhu.navDownImage"
            titleView.addSubview(titleIndictor)
        }
        titleBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        //debug 设置 navTitle
        update_title_label()
        self.navigationItem.titleView = titleView
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(titleClickEvent(recongnizer:)))
        self.navigationItem.titleView?.addGestureRecognizer(tap)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //赔率拖动条进度变化时的回调事件
    func onSeekbarEvent(currentRate: Float, currentWin: Float, progressRate: Float,reload: Bool) {
        var award = ""
        if !isPeilvVersion(){
            let awardStr = String.init(format: "%.3f", currentWin * multiplyValue)
            award = awardStr
            
            awardNum = awardStr
            award += "元"
        }else{
            award += String.init(format: "%.3f", currentWin)
        }
        self.current_odds = currentWin
        self.current_rate = currentRate
        currentOddTV.text = award
        
        let ratebackStr = String.init(format: "%.3f",currentRate)
        if let ratebackFloat = Float(ratebackStr) {
            ratebackTV.text = "\(ratebackFloat)"  + "%"
        }
        
        let creditbottomSlideLeftStr = String.init(format: "%.3f", currentRate)
        if let creditbottomSlideLeftFloat = Float(creditbottomSlideLeftStr) {
            creditbottomSlideLeftLabel.text = "\(creditbottomSlideLeftFloat)" + "%"
        }
        
        updateBottomUI()
        if isPeilvVersion(){
            if subPlayCode == "hx" {
                
            }
            dynamicCalculateOdds(currentRate: (1 - creditbottomTopSlider.value), handler: {[weak self] () in
                guard let weakSelf = self else {return}
                
                for result in weakSelf.honest_odds {
                    for model in weakSelf.peilvListDatas {
                        if result.code == model.code {
                            if model.peilvs.count == result.odds.count {
                                for (index,inModel) in model.peilvs.enumerated() {
                                    let inPeilv = result.odds[index]
                                    inModel.currentOdds = inPeilv.currentOdds
                                    inModel.currentSecondOdds = inPeilv.currentSecondOdds
                                }
                            }
                        }
                    }
                }
                
                weakSelf.playPaneTableView.reloadData()
            })
        }
    }
    
    //拖动条滑动事件
    @objc func sliderChange(slider:UISlider) -> Void{
        oddSlider.changeSeekbar(currentProgress: slider.value,reload:true)
    }
    
    @objc func creditSliderChange(slider:UISlider) -> Void{
        creditbottomTopSlider.changeSeekbar(currentProgress: slider.value,reload:true)
    }
    
    @objc func creditSliderChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .ended:
                dynamicCalculateOdds(currentRate: (1 - slider.value),handler: {() in
                    self.playPaneTableView.reloadData()
                })
                creditbottomTopSlider.changeSeekbar(currentProgress: slider.value,reload:true)
            default:
                break
            }
        }
    }
    
    @objc func moneyTextChange(textField:UITextField)->Void{
        let s = textField.text
        guard let svalue = s else {return}
        if isPeilvVersion(){
            if isEmptyString(str: svalue){
                self.betMoneyWhenPeilv = ""
                return
            }
            if !isPurnInt(string: svalue){
                return
            }
            
            self.betMoneyWhenPeilv = svalue
            self.updateBottomUI()
        }else{
            if isEmptyString(str: svalue){
                self.selectedBeishu = 1
                return
            }
            if !isPurnInt(string: svalue){
                return
            }
            
            let max = Int.max
            if let doubleSvalue:Double = Double(svalue)
            {
                if doubleSvalue > Double(max)
                {
                    showToast(view: self.view, txt: "输入数值过大")
                    let numSubStr = svalue.subString(start: 0, length: svalue.length - 1)
                    beishu_money_input.text = numSubStr
                    return
                }
            }
            self.selectedBeishu = Int(svalue)!
        }
        
        self.updateBottomUI()
        
        if let doubleSvalue:Double = Double(svalue) {
            if let curentOddTvNum = Float(awardNum) {
                let lastCurrentOddTVNum = Double(doubleSvalue) * Double(curentOddTvNum)
                let str = String.init(format: "%.3", lastCurrentOddTVNum)
                if let num = Double(str) {
                    bottomMoneyTV.text = "\(num)元"
                }
            }
        }
    }
    
    @objc func creditMoneyTextChange(textField:UITextField)->Void{
        let s = textField.text
        guard let svalue = s else {return}
        if isPeilvVersion(){
            if isEmptyString(str: svalue){
                self.betMoneyWhenPeilv = ""
                return
            }
            if !isPurnInt(string: svalue){
                return
            }
            
            let max = Int.max
            if let doubleSvalue:Double = Double(svalue) {
                if doubleSvalue > Double(max) {
                    
                    showToast(view: self.view, txt: "输入数值过大")
                    let numSubStr = svalue.subString(start: 0, length: svalue.length - 1)
                    creditbottomTopField.text = numSubStr
                    return
                }
            }
            
            self.betMoneyWhenPeilv = svalue
            self.updateBottomUI()
        }
    }
    
    //奖金模式点击事件
    @objc func onModeSwitch(sender:UIButton){
        super.show_mode_switch_dialog(sender: sender,handler: {(title) in
            self.modeBtn.setTitle(title, for: .normal)
            self.updateBottomUI()
        })
    }
    
    @objc func onLotSwitch(){
        self.showLotsWindow()
    }
    
    @objc func onRandomBetClick(){
        if self.peilvListDatas.isEmpty && isPeilvVersion(){
            return
        }
        
        if isBetNotAble() {
            showToast(view: self.view, txt: fenPanTips)
            return
        }
        
        let randomView = RandomBetSelectDialog(dataSource: ["一单","五单","十单"], viewTitle: "注单选择")
        randomView.selectedIndex = 0
        randomView.didSelected = { [weak self, randomView] (index, content) in
            var orderCount = 1
            if index == 0{
                orderCount = 1
            }else if index == 1{
                orderCount = 5
            }else if index == 2{
                orderCount = 10
            }
            self?.startRandomBet(orderCount: orderCount)
        }
        self.view.window?.addSubview(randomView)
        randomView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(randomView.kHeight)
        randomView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            randomView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func startRandomBet(orderCount:Int){
        if super.isPeilvVersion(){
            self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
                let orders = self.lhcLogic.randomBet(choosePlays: self.peilvListDatas, orderCount: orderCount)
                if orders.isEmpty{
                    showToast(view: (self.view)!, txt: "没有机选出注单，请重试")
                    return
                }
                //准备数据，进入下一页主单列表
                self.formPeilvBetDataAndEnterOrderPage(order: orders,peilvs:self.peilvListDatas,lhcLogic:self.lhcLogic,isRandom:true)
            } )
        }else{
            self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
                
                let orders = JianjinLotteryLogic.randomBet(orderCount: orderCount, cpCode: self.cpTypeCode, selectedSubCode: self.subPlayCode, peilv: self.officail_odds)
                if orders.isEmpty{
                    showToast(view: self.view, txt: "没有机选出注单，请重试")
                    return
                }
                //准备数据，进入下一页主单列表
                super.formBetDataAndEnterOrderPage(order: orders,handler: {() in
                    self.clearAfterBetSuccessAction()
                })
            } )
        }
    }
    
    @objc func onFastBetButton(){
        if is_fast_bet_mode{
            return
        }
        is_fast_bet_mode = true
        switch_bottom_money_input_when_betmode_change()
        delay(0.3){
            self.clearAfterBetSuccessAction()
        }
    }
    
    @objc func onNormalBetButton(){
        if !is_fast_bet_mode{
            return
        }
        is_fast_bet_mode = false
        switch_bottom_money_input_when_betmode_change()
        delay(0.3){
            self.clearAfterBetSuccessAction()
        }
    }
    
    func bindLotData(version:String) ->[LotteryData]{
        let lotterys = YiboPreference.getLotterys()
        if (isEmptyString(str:lotterys)) {
            return [];
        }
        //将彩票类型数据筛选出来
        var allDatas:[LotteryData] = []
        if let result = LotterysWraper.deserialize(from: lotterys){
            if result.success{
                allDatas.removeAll()
                for item in result.content!{
                    if item.moduleCode == 3{
                        for bean in item.subData{
                            if bean.lotVersion == Int(self.cpVersion)!{
                                allDatas.append(bean)
                            }
                        }
                    }
                }
            }
        }
        return allDatas
    }
    
    func showLotsWindow() -> Void {
        if lotWindow == nil{
            lotWindow = Bundle.main.loadNibNamed("lots_menu", owner: nil, options: nil)?.first as! LotsMenuView
            self.allLotDatas = self.allLotDatas + self.bindLotData(version: self.cpVersion)
        }
        
        lotWindow.windowDelegate = self
        self.allLotDatas = self.allLotDatas.sorted { (noticesP1, noticesP2) -> Bool in
            return noticesP1.sortNo > noticesP2.sortNo
        }
        lotWindow.setData(items: self.allLotDatas,lotCode:self.cpBianHao,lotName:self.cpName)
        lotWindow.show()
    }
    
    //处理顶部标题栏点击事件
    @objc func titleClickEvent(recongnizer:UIPanGestureRecognizer) -> Void {
        //显示彩票版本切换弹出框
        if getLotVersionConfig() == VERSION_V1V2 {
            if !isSixMark(lotCode: self.cpBianHao) && !isKuaiLeShiFeng(lotType: self.cpTypeCode){
                showVersionSwitchMenuWhenResponseClick()
            }
        }
    }
    
    @objc func openPlayIntroduce(){
        openPlayIntroduceController(controller: self, payRule: self.playMethod, touzhu: self.detailDesc, winDemo: self.winExample)
    }
    
    @objc func onOpenNumberClick() -> Void {
        recentTableViewShowOrHide()
    }
    
    private func recentTableViewShowOrHide() {
        
        self.isRecentResultOpen = !self.isRecentResultOpen
        if self.isRecentResultOpen{
            UIView.animate(withDuration: 0.3, animations: {
                self.playPaneTopConstraint.constant = 300
                self.recentTableHeightConstraint.constant = 300
                self.pullpushButton.isHidden = true
                self.view.layoutIfNeeded()
            }) { ( _) in
            }
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.playPaneTopConstraint.constant = 0
                self.recentTableHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }) { ( _) in
                self.pullpushButton.isHidden = false
            }
        }
    }
    
    @objc override func endBetTickDown() {
        //将剩余时间减少1秒
        self.endBetTime -= 1
        
        if self.endBetTime > 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.endBetTime)))
            
            if YiboPreference.getAbleBet() == "on" {
                clearDisableBetStatus()
                self.betDeadlineDesLabel.text = "截止剩余时间"
                self.countDownUI.text = String.init(format:"%@", dealDuration)
            }            
        }else if self.endBetTime <= 0{
            self.disableBetTime = self.ago
            self.createDisableBetCountDownTimer()
            if self.ago == 0 {
                self.getCountDownByCpCodeAction(shouldReStartDisableTimer: false)
            }
        }
    }
    
    override func createDisableBetCountDownTimer() {
        super.createDisableBetCountDownTimer()
        self.betDeadlineDesLabel.text = "开盘剩余时间"
        
        let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(self.disableBetTime))
        self.countDownUI.text = String.init(format:"%@", dealDuration)
        
        if isBetNotAble() {
            self.betBtn.backgroundColor = UIColor.lightGray
            self.betBtn.isEnabled = false
        }
    }
    
    @objc override func disableBetTickDown() -> Void {
        //将剩余时间减少1秒
        self.disableBetTime -= 1
        
        if self.disableBetTime > 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.disableBetTime)))
            self.countDownUI.text = String.init(format:"%@", dealDuration)
            
        }else if self.disableBetTime <= 0{
            clearDisableBetStatus()
            
            //封盘时间结束后再次获取下一期的倒计时时间
            self.getCountDownByCpCodeAction()
        }
    }
    
    //当前这期下注截止时间到时的弹框提示
    func showToastTouzhuEndlineDialog(qihao:String) -> Void {
        
        if !isViewVisible{
            return
        }
        let message = String.init(format: "%@期的投注已截止，投注时请注意检查当前期号是否正确！", qihao)
        let alertController = UIAlertController(title: "温馨提示",
                                                message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: {
            action in
            self.getCountDownByCpCodeAction()
        })
        let okAction = UIAlertAction(title: "好的", style: .default, handler: {
            action in
            self.getCountDownByCpCodeAction()
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: 封盘时间结束后再次获取下一期的倒计时时间
    private func getCountDownByCpCodeAction(shouldReStartDisableTimer:Bool = true) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
            if self.viewShowing {
                self.getCountDownByCpCodeAction()
            }
        }
        
        self.getCountDownByCpcode(bianHao: self.cpBianHao, lotVersion: self.cpVersion, controller: self, shouldReStartDisableTimer:shouldReStartDisableTimer,failureHandler:
            {(currentQiHaoP,countDownP) -> Void in
                self.currentQihaoUI.text = self.currentQihao
                self.countDownUI.text = "00 : 00 : 00"
        }, successHandler:
            {(dealDuration) -> Void in
                if YiboPreference.getAbleBet() == "on" {
                    self.countDownUI.text = String.init(format:"%@", dealDuration)
                }
                self.updateCurrenQihao()
        })
    }
    
    @objc override func lastResultTickDown() {
        
        super.lastResultTickDown()
        
        if self.tickTime <= 0{
            //取消定时器
            if let timer = lastKaiJianResultTimer{
                timer.invalidate()
            }
            
            //当前期数投注时间到时，继续请求同步服务器上下一期号及离投注结束倒计时时间
            //获取下一期的倒计时的同时，获取上一期的开奖结果
            self.tickTime = self.ago + Int64(self.offset)
            
            self.lastOpenResult(cpBianHao: self.cpBianHao, lastLotteryExcpHandler: {(contents) in
                self.updateLastKaiJianExceptionResult(result: contents)
            }, updateLotteryHandler: {(results) in
                self.updateLastKaiJianResult(result:results)
            })
        }
    }
    
    //MARK: 更新开奖结果
    /**
     * 更新开奖结果
     * @param result
     */
    func updateLastKaiJianResult(result:[BcLotteryData]) -> Void {
        
        self.recentResults.removeAll()
        if result.isEmpty{
            return
        }
        self.recentResults = self.recentResults + result
        let firstResult:BcLotteryData = result[0]
        if isEmptyString(str: firstResult.qiHao) || isEmptyString(str: firstResult.haoMa){
            return
        }
        
        self.updateLastKaiJianResult()
        
        if isEmptyString(str: firstResult.haoMa){
            return
        }
        let haomaArr = firstResult.haoMa.components(separatedBy: ",");
        var ballWidth:CGFloat = 30
        var small = false
        if isSaiche(lotType: self.cpTypeCode){
            ballWidth = 20
        }else if isFFSSCai(lotType:self.cpTypeCode){
            ballWidth = 30
            small = false
        }else if isXYNC(lotType: self.cpTypeCode){
            ballWidth = 20
        }
        numViews.isHidden = false
        exceptionNumTV.isHidden = true
        
        let lotType = lotData?.lotType ?? 0
        if lotType == 4 {
            let infos = setupKuaiSanInfo(lotType: "\(4)", values: haomaArr)
            setupKuaiSanInfoView(width: rightMoreInfoViewW, ballWidth: 30,infos: infos) //设置快三
        }
       
        //如何需要计算六合彩类彩种的生肖；这里需要根据开奖时间计算出当前这一期开奖结果所在的农历年份
        numViews.basicSetupBalls(nums: haomaArr, offset: 0, lotTypeCode: self.cpTypeCode, cpVersion: cpVersion,ballWidth: ballWidth,small: small,gravity_bottom:false,ballsViewWidth: numViews.width,isBetTopView:true,time:firstResult.date)
        //更新底层最近开奖结果列表
        self.recent_open_result_tableview.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.accountWeb(handler: {(contents) -> Void in
                self.balanceUI.text = "\(contents)元"
            })
        }
    }
    
    //MARK: 设置快三开奖结果信息 和，大小，单双
    func setupKuaiSanInfo(lotType:String,values: [String]) -> (String,String,String) {
        var total: Int = 0
        for value in values {
            total += Int(value)!
        }

        var diceResult = ""
        var oddOrEven = ""
        if lotType == "4" {
            diceResult = total <= 10 ? "小" : "大"
            oddOrEven = (total % 2 == 0 ? "双" : "单")
            if isLeopard(values: values,total: total)
            {
                diceResult = "豹子"
                oddOrEven = "豹子"
            }
        }
        
        return ("\(total)",diceResult,oddOrEven)
    }
    
    private func isLeopard(values:[String],total:Int) -> Bool {
        var allValueEqual = false
        for (index,value) in values.enumerated()
        {
            if index > 0
            {
                allValueEqual = value == values[index - 1]
                if !allValueEqual
                {
                    break
                }
            }
        }
        
        if let sys = getSystemConfigFromJson() {
            if allValueEqual && sys.content.k3_baozi_daXiaoDanShuang == "off" {
                return true
            }
        }
        
        
        return false
    }
    
    func updateLastKaiJianExceptionResult(result:String) -> Void {
        numViews.isHidden = true
        exceptionNumTV.isHidden = false
        exceptionNumTV.text = result
    }
    
    @objc func click_bottom_clear_button() -> Void {
        if !isPeilvVersion(){
            if !self.official_orders.isEmpty{
                self.official_orders.removeAll()
                self.refreshPaneAndClean()
            }
        }else{
            if !self.honest_orders.isEmpty{
                self.honest_orders.removeAll()
            }
            
            clearMutiPlaysBetAndReload()
            
            self.refreshPaneAndClean()
        }
    }
    
    private func clearMutiPlaysBetAndReload() {
        if !peilvListDatasSuperSet.isEmpty {
            peilvListDatasSuperSet.removeAll()
        }
        
        lhcLogic.clearHonest_oddsDic()
        
        self.peilvListDatas.removeAll()
        if isPeilvVersion() {
            getOddsFromAllPlayCodes(selectPlayBarPos: self.selected_rule_position, showDialog: false)
        }
        
        betMoneyWhenPeilv = ""
        selectedPlayRulesModel.clearModel()
        handlePeilvListDatasSuperSet(peilvListDatas: peilvListDatas, peilvListDatasSuperSet: peilvListDatasSuperSet)
        playRuleTableView.reloadData()
    }
    
    func refreshPaneAndClean() -> Void {
        self.clearAfterBetSuccessAction()
    }
    
    
    //底部投注按钮点击事件
    @objc func click_bet_button() {
        
        if dealBetFlag {
            dealBetFlag = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.dealBetFlag = true
            }
            
            self.view.endEditing(true)
            super.commitBetAction(isNormalStyle: normalModeButton.isSelected, creditbottomTopText: self.creditbottomTopField.text, reloadAndClearBottom: {() in
                self.playPaneTableView.reloadData()
                super.clearBottomValue(clearModeAndBeiShu: true,clear_orders: false, handler: {() in
                    self.clearBottomUIValue()
                })
            }, formBetDataAndEnterOrderPage: {() in
                super.formBetDataAndEnterOrderPage(order: self.official_orders,handler: {() in
                    self.clearAfterBetSuccessAction()
                })
            }, formPeilvDataAndOrder: {() in
                self.clearAfterBetSuccessAction()
            })
        }
    }
    
    /**
     * 投注动作
     * @param selectedDatas 用户已经选择完投注球的球列表数据
     */
    func asyncPeilvBet(selectedDatas:[PeilvPlayData]) -> Void{
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                if !selectedDatas.isEmpty{
                    
                    var allNotMoney = true
                    var sb = ""
                    for item in selectedDatas{
                        if item.money == 0{
                            sb = sb + String.init(format: "%@%@%@%@", "\"", !isEmptyString(str: item.itemName) ? item.itemName+"-" : "",item.number,"\"")
                        }else{
                            allNotMoney = false
                        }
                    }
                    if isMulSelectMode(subCode: self.subPlayCode){
                        
                        guard let money = self.creditbottomTopField.text else{
                            showToast(view: self.view, txt: "请先输入下注金额再投注")
                            self.creditbottomTopField.becomeFirstResponder()
                            return
                        }
                        
                        if isEmptyString(str: money){
                            showToast(view: self.view, txt: "请先输入下注金额再投注")
                            self.creditbottomTopField.becomeFirstResponder()
                            return
                        }
                        
                    }else{
                        if allNotMoney
                        {
                            //择好号码，但没有输入金额时，先弹出键盘输入金额
                            showToast(view: self.view, txt: "请先输入下注金额再投注")
                            self.creditbottomTopField.becomeFirstResponder()
                            return
                        }
                        
                        if !isEmptyString(str: sb){
                            showToast(view: self.view, txt: String.init(format: "%@号码未输入金额,请输入后再投注", sb))
                            return
                        }
                    }
                }else{
                    showToast(view: self.view, txt: "请先选择号码并投注")
                }
            }
        }
    }
    
    //显示下注版本切换框
    @objc func showVersionSwitchMenuWhenResponseClick() -> Void {
        
        //frame 为整个popview相对整个屏幕的位置 arrowMargin ：指定箭头距离右边距离
        version_top_menu = SwiftPopMenu(frame:  CGRect(x: Int(KSCREEN_WIDTH/2 - 120), y: KNavHeight - 10, width: 150, height: 100), arrowMargin: 18)
        version_top_menu.popData = version_switch_datasources
        //点击菜单事件
        version_top_menu.didSelectMenuBlock = { [weak self](index:Int)->Void in
            guard let weakSelf = self else {
                return
            }
            weakSelf.version_top_menu.dismiss()
            weakSelf.click_version_menu(index: index)
        }
        version_top_menu.show()
    }
    
    func click_version_menu(index:Int) {
        
        if !lhcSelect{
        //是否切换了官方信用，避免重复调用调用接口
            if (isPeilvVersion() && index == 1) || (!isPeilvVersion() && index == 0){
                return
            }
            self.cpVersion = index == 0 ? VERSION_1:VERSION_2
        }
    
        clearBottomValue(clearModeAndBeiShu: true, handler: {() in
            clearBottomUIValue()
        })
        let titleString = index == 0 ? "官方下注":"信用下注"
        backLotterTitleCloser?(titleString)
        playRuleTableView.reloadData()
        swtichTouzhuHeader()
        
        
        //重新选择彩票版本后，需要重新获取彩种对应的玩法
        super.sync_playrule_from_current_lottery(lotType: self.cpTypeCode, lotCode: self.cpBianHao, lotVersion: self.cpVersion,handler: {(lotteryData) -> Void in
            self.sync_local_constan_restart_something_after_playrule_obtain(lottery: lotteryData)
        })
    }
    
    func swtichTouzhuHeader(){
        if isPeilvVersion(){
            topHeaderImg.isHidden = true
            topHeaderImg.image = nil
            topHeaderImg.backgroundColor = UIColor.white
            //            bottomDottedLine.isHidden = false
            //            topDottedLine.isHidden = false
            topVerticalLine.isHidden = true
        }else{
            //            bottomDottedLine.isHidden = true
            //            topDottedLine.isHidden = true
            topVerticalLine.isHidden = false
            topHeaderImg.isHidden = false
            topHeaderImg.backgroundColor = UIColor.clear
            topHeaderImg.theme_image = "TouzhOffical.topHeaderImg"
        }
        
        refreshLotteryTime()
        update_bet_header_seperator_line()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onShakeStart), name: NSNotification.Name(rawValue:"shakeBegin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onShakEnd), name: NSNotification.Name(rawValue:"shakeEnd"), object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewVisible = true
        updatePlayRuleConstraint(isHidden: isPlayBarHidden)
        //同步账号
        self.accountWeb(handler: {(contents) -> Void in
            self.balanceUI.text = "\(contents)元"
        })

        super.showAnnounce(controller: self)
        if isPeilvVersion(){
            if !self.playRules.isEmpty{
                let select_rule = self.playRules[self.selected_rule_position]
                if isSpeicalLHCPlayCode(code: select_rule.code){
                    handlePlayRuleClick(playData: select_rule,position: self.selected_rule_position)
                }
            }
        }
        
        self.lastOpenResult(cpBianHao: self.cpBianHao, lastLotteryExcpHandler: {(contents) in
            self.updateLastKaiJianExceptionResult(result: contents)
        }, updateLotteryHandler: {(results) in
            self.updateLastKaiJianResult(result:results)
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.isPlayBarHidden = false
        //移除通知
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "shakeBegin"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "shakeEnd"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //设置侧边玩法兰列表的代理及数据原
    func initPlayRuleTable() -> Void {
        self.playRuleTableView!.delegate = self
        self.playRuleTableView!.dataSource = self
        self.playRuleTableView.separatorStyle = .none
    }
    
    func initPlayPaneTable() -> Void {
        playPaneTableView.delegate = self
        playPaneTableView.dataSource = self
        self.playPaneTableView.separatorStyle = .none
        playPaneTableView.backgroundColor = UIColor.white.withAlphaComponent(0)
    }
    
    //MARK: - 点击事件
    //MARK:遗漏 && 冷热
    @objc func clickMissingNumButton(sender:UIButton) -> Void {
        missingNumButton.isSelected = true
        coolHotButton.isSelected = false
        onMissingButton()
    }
    
    @objc func clickCoolHotButton(sender:UIButton) -> Void {
        coolHotButton.isSelected = true
        missingNumButton.isSelected = false
        onCoolHotButton()
    }
    
    @objc func clickFastModeButton(sender:UIButton) -> Void {
        
        if self.peilvListDatas.isEmpty && isPeilvVersion(){
            return
        }
        fastModeButton.isSelected = true
        normalModeButton.isSelected = false
        creditBottomHistoryImg.isHidden = true
        creditBottomHistoryLabel.isHidden = true
        creditBottomHistoryButton.isHidden = true
        
//        creditBottomTopFastImgBtn.isHidden = false
        bottomtopTipsLabel.isHidden = false
        creditbottomTopField.isHidden = false
        
        onFastBetButton()
        scrollToTop()
    }
    
    @objc func clickNormalModeButton(sender:UIButton) -> Void {
        
        if self.peilvListDatas.isEmpty && isPeilvVersion(){
            return
        }
        
        if isPeilvVersion() {
           betMoneyWhenPeilv = ""
        }
        
        normalModeButton.isSelected = true
        fastModeButton.isSelected = false
        
//        creditBottomTopFastImgBtn.isHidden = true
        bottomtopTipsLabel.isHidden = true
        creditbottomTopField.isHidden = true
        creditBottomHistoryImg.isHidden = false
        creditBottomHistoryLabel.isHidden = false
        creditBottomHistoryButton.isHidden = false
        
        onNormalBetButton()
        scrollToTop()
    }
    
    //MARK: 切换-冷热、遗漏
    func onCoolHotButton() {
        if self.codeRank == nil{
            showToast(view: self.view, txt: "还没有数据，请重试")
            return
        }
        playPaneTableView.reloadData()
    }
    
    func onMissingButton() {
        if self.codeRank == nil{
            showToast(view: self.view, txt: "还没有数据，请重试")
            return
        }
        playPaneTableView.reloadData()
    }
    
    //点击侧边玩法兰推拉条事件
    @objc func clickPlayRulePushpullBar() -> Void {
        isPlayBarHidden = !isPlayBarHidden
        playRulePushpullBarAction(duration: 0.3,hidePlayBar: isPlayBarHidden)
    }
    
    func playRulePushpullBarAction(duration:TimeInterval,hidePlayBar:Bool) {
        self.lot_name_button.isHidden = hidePlayBar
        isPlayBarHidden = hidePlayBar
        self.playPaneTableView.reloadData()
        UIView.animate(withDuration: duration, animations: {
            if self.isPlayBarHidden{
                self.playRuleLayoutConstraint.constant = -kScreenWidth*playRulePanWScale
                self.playPaneLayoutConstraint.constant = kScreenWidth*playRulePanWScale
            }else{
                self.playRuleLayoutConstraint.constant = 0
                self.playPaneLayoutConstraint.constant = 0
            }
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.updatePlayRuleConstraint(isHidden: self.isPlayBarHidden)
        }
    }
    
    func updatePlayRuleConstraint(isHidden:Bool) -> Void {
        pullpushButton.theme_setBackgroundImage(isPlayBarHidden ? "TouzhOffical.handleRight" : "TouzhOffical.handleLeft", forState: .normal)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == PLAY_RULE_TABLVIEW_TAG{
            return playRules.count
        }else if tableView.tag == PLAY_PANE_TABLEVIEW_TAG{
            if !isPeilvVersion(){
                return self.ballonDatas.count
            }else{
                return self.peilvListDatas.count
            }
        }else if tableView.tag == RECENT_RESULTS_TABLEVIEW_TAG{
            return self.recentResults.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == PLAY_PANE_TABLEVIEW_TAG{
            if !isPeilvVersion(){
                let ballCount = self.ballonDatas[indexPath.row].ballonsInfo.count
                let isWeishuShow = self.ballonDatas[indexPath.row].showWeiShuView
                if !isPlayBarHidden{
                    var lines = ballCount / BALL_COUNT_PER_LINE
                    if ballCount > BALL_COUNT_PER_LINE{
                        if ballCount % BALL_COUNT_PER_LINE != 0{
                            lines = lines + 1
                        }
                    }
                    if lines == 0{
                        lines = lines + 1
                    }
                    var height = CGFloat(CGFloat(lines)*44+50)
                    if isWeishuShow!{
                        height = height + 30
                    }
                    return height
                }else{
                    var lines = ballCount / BALL_COUNT_PER_LINE_WHEN_EXPAND
                    if ballCount > BALL_COUNT_PER_LINE_WHEN_EXPAND{
                        if ballCount % BALL_COUNT_PER_LINE_WHEN_EXPAND != 0{
                            lines = lines + 1
                        }
                    }
                    if lines == 0{
                        lines = lines + 1
                    }
                    var height = CGFloat(CGFloat(lines)*40+65)
                    if isWeishuShow!{
                        height = height + 30
                    }
                    return height
                }
            }else{
                let specialPlay = lhcLogic.isSingleLineLayout()
                if specialPlay{
                    let cellHeight = self.peilvListDatas[indexPath.row].peilvs.count
                    return CGFloat(cellHeight*44 + 30 + 10)
                }else{
                    //判断数是否是三字组合、三字定位那种比较大的数据
                    if self.peilvListDatas[indexPath.row].peilvs.count != 220 && self.peilvListDatas[indexPath.row].peilvs.count != 1000{
                        let cellHeight = self.peilvListDatas[indexPath.row].peilvs.count
                        var height = cellHeight/2
                        if cellHeight%2 > 0{
                            height += 1
                        }
                        return CGFloat(height*(is_fast_bet_mode ? 46 : 75) + 30)
                    }else {
                        return tableView.height
                    }
                }
                
            }
        }else if tableView.tag == RECENT_RESULTS_TABLEVIEW_TAG {
            if ["6","66"].contains(self.cpTypeCode) {
                return 50
            }
        }
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let _:UITableViewCell!
        if tableView.tag == PLAY_RULE_TABLVIEW_TAG{
            
            guard let cell = playRuleTableView.dequeueReusableCell(withIdentifier: "ruleCell") as? PlayRuleCell  else {
                fatalError("The dequeued cell is not an instance of PlayRuleCell.")
            }
            let row = indexPath.row
            if (row + 1) > self.playRules.count {
                return cell
            }
            
            let subRules = self.playRules[row]
            
            let hadSelected = self.selectedPlayRulesModel.getCountOfThisPlay(Index: indexPath.row) > 0
            
            let name = isEmptyString(str: subRules.fakeParentName) ? subRules.name : subRules.fakeParentName
            
            cell.ruleNameMaxLength = self.ruleNameLength
            
            cell.setupUI(name: name, isSelected: self.selected_rule_position == indexPath.row,hadSelected:hadSelected)
            return cell
            
        }else if tableView.tag == RECENT_RESULTS_TABLEVIEW_TAG{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "recent") as? RecentResultCell  else {
                fatalError("The dequeued cell is not an instance of RecentResultCell.")
            }
            let data = self.recentResults[indexPath.row]
            cell.setupData(qihao: data.qiHao, nums: data.haoMa, cpCode: self.cpTypeCode, cpVersion: self.cpVersion,date:data.date)
            return cell
        }else if tableView.tag == PLAY_PANE_TABLEVIEW_TAG{
            if !isPeilvVersion(){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "paneCell") as? JianjinPaneCell  else {
                    fatalError("The dequeued cell is not an instance of JianjinPaneCell.")
                }
                //bind cell delegate
                cell.cpTypeCode = self.cpTypeCode
                cell.playCode = self.subPlayCode
                cell.btnsDelegate = self
                
                let ruleTestStr: String! = self.ballonDatas[indexPath.row].ruleTxt
                cell.ruleUI.layer.cornerRadius = 10
                
                if ruleTestStr != nil {
                    cell.ruleUI.setTitle(ruleTestStr, for: .normal)
                }
                
                cell.weishuView.cellDelegate = self
                cell.funcView.cellDelegate = self
                cell.weishuView.cellPos = indexPath.row
                cell.funcView.cellPos = indexPath.row
                cell.toggleWeishuView(show: self.ballonDatas[indexPath.row].showWeiShuView)
                
                cell.initFuncView(playRuleShow:!isPlayBarHidden)
                
                if self.ballonDatas[indexPath.row].showWeiShuView{
                    cell.weishuView.setData(array: self.ballonDatas[indexPath.row].weishuInfo,playRuleShow:!isPlayBarHidden)
                }
                
                if let cr = self.codeRank{
                    if !cr.isEmpty{
                        if indexPath.row < cr.count{
                            let isColdHot = self.coolHotButton.isSelected && !self.missingNumButton.isSelected
                            cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,codeRank: cr[indexPath.row],
                                             isColdHot:isColdHot,showCodeRank:self.showCodeRank)
                        }else{
                            cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,showCodeRank:false)
                        }
                    }else{
                        cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,showCodeRank:self.showCodeRank)
                    }
                }else{
                    cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,showCodeRank:self.showCodeRank)
                }
                
                let shouldHideWeiShuView = !self.ballonDatas[indexPath.row].showWeiShuView
                let shouldHideFunctionView = !self.ballonDatas[indexPath.row].showFuncView
                cell.weishuView.isHidden = shouldHideWeiShuView
                cell.funcView.isHidden = shouldHideFunctionView
                
                return cell
            } else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "peilvCell") as? PeilvTouzhuCell  else {
                    fatalError("The dequeued cell is not an instance of peilvCell.")
                }
                cell.cellDelegate = self
                if !self.peilvListDatas.isEmpty{
                    if indexPath.row >= self.peilvListDatas.count{
                        return cell
                    }
                    let data = self.peilvListDatas[indexPath.row]
                    cell.cellRow = indexPath.row
                    cell.is_play_bar_show = !self.isPlayBarHidden
                    //lhcLogic.isSingleLineLayout()
                    DispatchQueue.main.async {
                        cell.setupData(data: data,mode:self.is_fast_bet_mode,
                                       specialMode: self.lhcLogic.isSingleLineLayout(),
                                       cpCode: self.cpTypeCode,cpVerison: self.cpVersion,lotCode: self.cpBianHao)
                    }
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if tableView.tag == PLAY_RULE_TABLVIEW_TAG {
//            return 44
//        }else {
//            return 0
//        }
//    }
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if tableView.tag == PLAY_RULE_TABLVIEW_TAG {
//            let view = UIView()
//            view.isUserInteractionEnabled = true
//            view.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth*playRulePanWScale, height: 44)
//            view.backgroundColor = UIColor.white.withAlphaComponent(0.2)
//
//            let indictor = UIImageView()
//            indictor.theme_image = "TouzhOffical.sideNavBox"
//            view.addSubview(indictor)
//
//            indictor.snp.makeConstraints { (make) in
//                make.centerY.equalToSuperview()
//                make.left.equalToSuperview().offset(2)
//                make.width.height.equalTo(8)
//            }
//
//            let label = UILabel()
//            label.font = UIFont.systemFont(ofSize: 14)
//            label.textAlignment = .center
//            label.backgroundColor = UIColor.clear
//            label.textColor = UIColor.white
//            label.text = "计划跟单"
////            indictor.theme_image = "TouzhOffical.sideNavBox"
//            label.theme_textColor = "FrostedGlass.Touzhu.ruleCellNormalTextColor"
//            view.addSubview(label)
//            label.snp.makeConstraints { (make) in
//                make.centerY.equalTo(indictor.snp.centerY)
//                make.left.equalTo(indictor.snp.right).offset(1)
//                make.right.equalToSuperview().offset(0)
//            }
//
//            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tapPlayRuleFooter))
//            view.addGestureRecognizer(gesture)
//            return view
//        }else {
//            return nil
//        }
//    }
//
    @objc func tapPlayRuleFooter() {
        //获取赛车🏎️ 1-10的玩法(暂时只适配了赛车的数据)
        if isSaiche(lotType: (self.lotData?.czCode ?? "")) {
            var oneToTenRules = [BcLotteryPlay]()
            var oneToTenCodes = ["dgj","dyj","ddsm","ddsim","ddwm","ddlm","ddqm","ddbm","ddjm","ddshm"]
            
            for model in self.playRules { //排序玩法
                if model.fakeParentCode == "sc1_5" || model.fakeParentCode == "sc6_10" {
                    for (_,innerModel) in model.children.enumerated() {
                        if oneToTenCodes.contains(innerModel.code) {
                            oneToTenRules = oneToTenRules + [innerModel]
                        }
                        else if oneToTenRules.count == 10 {
                            break
                        }
                    }
                }
            }
            
            oneToTenCodes.removeAll()
            for model in oneToTenRules {
                oneToTenCodes.append(model.code)
            }
            
            let arrayString = oneToTenCodes.joined(separator: ",")
            super.sync_honest_plays_odds_obtainForPlan(lotCategoryType: "3", subPlayCode: arrayString, showDialog: true) { [weak self] (honest_odds) in
                guard let weakSelf = self else {return}
                
                weakSelf.gotoPlanFollowBetHandler?((weakSelf.lotData?.code ?? ""),weakSelf.cpName,weakSelf.lotData ?? LotteryData(),honest_odds)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == PLAY_RULE_TABLVIEW_TAG{
            
            self.selectedPlayRulesModel.currentIndex = indexPath.row
            
            let select_rule = self.playRules[indexPath.row]
            
            if select_rule.code == "gdjh" {
                tapPlayRuleFooter()
            }else {
                handlePeilvListDatasSuperSet(peilvListDatas: peilvListDatas, peilvListDatasSuperSet: peilvListDatasSuperSet)
                
                self.handlePlayRuleClick(playData: select_rule,position: indexPath.row,fromPlayCellSelect:true)
                
                if isPeilvVersion() {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        //开始计算投注号码串及注数
                        if self.peilvListDatas.count == 0 {
                            return
                        }
                        self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
                            self.updateBottomUI()
                        } )
                    }
                }
            }
        }else if tableView.tag == RECENT_RESULTS_TABLEVIEW_TAG {
            recentTableViewShowOrHide()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    //MARK: 侧边玩法兰列表项点击事件
    func handlePlayRuleClick(playData:BcLotteryPlay,position:Int,fromPlayCellSelect:Bool = false) -> Void {
        
        selected_rule_position = position
        if playData.status != 2{
            showToast(view: self.view, txt: "该玩法已被关闭使用")
            return
        }
        
        if !switchCanMutiRulesBet(isPeilv: isPeilvVersion()) {
            if playData.code != self.subPlayCode{
                YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
            }
        }
        
        self.fakeSubPlayCode = playData.fakeParentCode
        self.fakeSubPlayName = playData.fakeParentName
        self.subPlayCode = playData.code
        self.subPlayName = playData.name
        self.winExample = playData.winExample
        self.detailDesc = playData.detailDesc
        self.playMethod = playData.playMethod
        
        //更新玩法label
        update_rule_label(playname: self.subPlayName)
        update_title_label()
        
        if (self.subPlayName == "自选不中" || self.subPlayName == "连码" || self.subPlayName == "连肖") && lotData?.lotType == 6
        {
            fastModeButton.isHidden = true
            normalModeButton.isHidden = true
        }else
        {
            fastModeButton.isHidden = false
            normalModeButton.isHidden = false
        }
        
        //重新刷新投注面板
        if !isPeilvVersion() {
            refreshButtons()
            //figure out play ballons
            //            clearBottomValue(clearModeAndBeiShu: true, handler: {() in
            //                clearBottomUIValue()
            //            })
            //根据彩票版本和玩法确定下注球列表数据
            let ballonRules = form_jianjing_pane_datasources(lotType: cpTypeCode, subCode: self.subPlayCode)
            ballonDatas.removeAll()
            self.ballonDatas = self.ballonDatas + ballonRules
            self.playPaneTableView.reloadData()
            self.scrollToTop()
            //获取奖金版玩法对应的赔率信息
            super.sync_official_peilvs_after_playrule_click(lotType: self.cpTypeCode, playCode: self.subPlayCode,showDialog: true,handler: {[weak self] (rightResult) in
                guard let weakSelf = self else {return}
                weakSelf.oddSlider.setupLogic(odd: rightResult,resetValue:false)
                weakSelf.hideOfficialSlider(hide: weakSelf.oddSlider.isHidden)
                
                weakSelf.scrollToTop(animated: false)
            })
        }else {
            //获取到玩法栏数据后，再根据选择玩法栏的所有子玩法code，获取子玩法下所有投注号码的赔率列表数据
            //开始网络获取
            self.getOddsFromAllPlayCodes(selectPlayBarPos: position, showDialog: true,fromPlayCellSelect:fromPlayCellSelect)
        }
        self.playRuleTableView.reloadData()
    }
    
    //根据所有玩法获取赔率列表
    func getOddsFromAllPlayCodes(selectPlayBarPos:Int,showDialog:Bool,fromPlayCellSelect:Bool = false) -> Void{
        var playCodes = ""
        if playRules.isEmpty{
            return
        }
        
        if selectPlayBarPos < playRules.count{
            //根据每一行的玩法code，从赔率表中获取对应的号码球赔率数据集
            let play = playRules[selectPlayBarPos]
            let children:[BcLotteryPlay] = play.children
            if !children.isEmpty{
                for p in children{
                    playCodes += p.code
                    playCodes.append(",")
                }
            }
            if playCodes == ""{
                showToast(view: self.view, txt: "没有开启该玩法，请联系客服")
                self.selectPlay = nil;
                self.refresh_page_after_honest_odds_obtain(selected_play: &self.selectPlay, odds: [])
                return
            }
            if playCodes.hasSuffix(","){
                playCodes = (playCodes as NSString).substring(to: playCodes.count - 1)
            }
            
            var currentFakeParentCode = ""
            if selectPlayBarPos < playRules.count {
                currentFakeParentCode = playRules[selectPlayBarPos].fakeParentCode
            }
            
            super.sync_honest_plays_odds_obtain(fakeParentCode:currentFakeParentCode,lotCategoryType: self.cpTypeCode, subPlayCode: playCodes, showDialog: showDialog,handler: {() in
                self.refresh_page_after_honest_odds_obtain(selected_play: &self.selectPlay, odds: self.honest_odds,fromPlayCellSelect:fromPlayCellSelect)
                self.startSyncLhcServerTime(lotCode: self.cpBianHao)
                //清除底部UI及变量
                super.clearBottomValue(clearModeAndBeiShu: false, handler: {() in
                    self.clearBottomUIValue(fromPlayCellSelect:fromPlayCellSelect)
                })
                self.updateBottomUI()
                self.scrollToTop(animated:false)
            })
        }
    }
    
    
    func update_rule_label(playname:String) -> Void {
        current_play_label.text = playname
    }
    
    //MARK: 获取navBar title应该显示什么
    func getNavBarTitle() -> String {
        var title = ""
        //v023站点，在只开了信用或官方的时候，导航栏上显示彩种名字
        if stationCode() == v023_ID && getLotVersionConfig() != VERSION_V1V2 {
            title = self.cpName
            lotNameBtnConsH.constant = 0
            lot_name_button.isHidden = true
        }else {
            title = self.cpVersion == VERSION_1 ? "官方下注" : "信用下注"
            if switchOnlyPeillvSingleForceOffical() {
                title = "官方下注"
            }
            lotNameBtnConsH.constant = 29
            lot_name_button.isHidden = false
        }
        
        return title
    }
    
    func update_title_label(){
        let title = getNavBarTitle()
        titleBtn.setTitle(title, for: .normal)
        updateTitleHandler?(title)
        
        if lotData?.lotType == 6 || lotData?.lotType == 66
        {
            titleIndictor.image = UIImage.init(named: "")
        }else
        {
            titleIndictor.theme_image = "FrostedGlass.Touzhu.navDownImage"
        }
        
        titleBtn.theme_setTitleColor("Global.barTextColor", forState: .normal)
    }
    
    //将玩法球列表滚动到第一行
    func scrollToTop(animated:Bool = true) -> Void{
        //        if isPeilvVersion(){
        DispatchQueue.main.async {
            if self.playPaneTableView.tag == self.PLAY_RULE_TABLVIEW_TAG{
                if self.playRules.count > 0{
                    self.playPaneTableView.scrollToRow(at: IndexPath.init(item: 0, section: 0), at: UITableView.ScrollPosition.top, animated: animated)
                }
            }else if self.playPaneTableView.tag == self.PLAY_PANE_TABLEVIEW_TAG{
                if !self.isPeilvVersion(){
                    if self.ballonDatas.count > 0{
                        self.playPaneTableView.scrollToRow(at: IndexPath.init(item: 0, section: 0), at: UITableView.ScrollPosition.top, animated: animated)
                    }
                }else{
                    if self.peilvListDatas.count > 0{
                        if self.peilvListDatas.first!.peilvs.count == 220 || self.peilvListDatas.first!.peilvs.count == 1000{
                            self.playPaneTableView.subviews.forEach { (subview) in
                                if subview.isKind(of: PeilvTouzhuCell.self){
                                    let cell = subview as! PeilvTouzhuCell
                                    cell.contentView.subviews.forEach { (cellSubview) in
                                        if cellSubview.isKind(of: UICollectionView.self){
                                            let collectionView = cellSubview as! UICollectionView
                                            collectionView.scrollToTopWidthAnimation(animation: false)
                                        }
                                    }
                                }
                            }
                        }
                        self.playPaneTableView.scrollToRow(at: IndexPath.init(item: 0, section: 0), at: UITableView.ScrollPosition.top, animated: animated)
                        
                    }
                    
                }
            }else if self.playPaneTableView.tag == self.RECENT_RESULTS_TABLEVIEW_TAG{
                if  self.recentResults.count > 0{
                    self.playPaneTableView.scrollToRow(at: IndexPath.init(item: 0, section: 0), at: UITableView.ScrollPosition.top, animated: animated)
                }
            }
        }
        //        }
    }
    
    //点击“万千百十个”位数按钮时的响应动作
    func clickWeiBtns(weiTag:Int,cellPos:Int,btnIndex:Int) -> Void {
        switch weiTag {
        default:
            let cell = self.playPaneTableView.cellForRow(at: IndexPath.init(row: cellPos, section: 0)) as! JianjinPaneCell
            let isSelected = self.ballonDatas[cellPos].weishuInfo?[btnIndex].isSelected
            let weiBtn = cell.viewWithTag(weiTag) as! UIButton
            
            if !isSelected! {
                weiBtn.setTitleColor(UIColor.white, for: .normal)
                weiBtn.theme_backgroundColor = "Global.themeColor"
            }else{
                weiBtn.backgroundColor = UIColor.white
                weiBtn.layer.theme_borderColor = "Global.themeColor"
                weiBtn.theme_setTitleColor("Global.themeColor", forState: .normal)
            }
            
            self.ballonDatas[cellPos].weishuInfo?[btnIndex].isSelected = !isSelected!
        }
    }
    
    //奖金版下注时--点击每个列玩法球中的辅助功能按钮的响应事件
    func onBtnsClickCallback(btnTag: Int, cellPos: Int) {
        
        if YiboPreference.isPlayTouzhuVolume(){
            playVolume()
        }
        switch btnTag {
        case 20,21,22,23,24://wan
            clickWeiBtns(weiTag: btnTag, cellPos: cellPos,btnIndex: btnTag - 20)
        //all
        case 10:
            let ball = self.ballonDatas[cellPos]
            for ballNumInfo in ball.ballonsInfo{
                ballNumInfo.isSelected = true
            }
            self.playPaneTableView.reloadData()
            break
        //big
        case 11:
            self.clickBigSmall(isBig: true, cellPos: cellPos)
            break
        //small
        case 12:
            self.clickBigSmall(isBig: false, cellPos: cellPos)
            break
        //single
        case 13:
            self.clickSingleDouble(isSingle: true, cellPos: cellPos,handler: {() in
                self.playPaneTableView.reloadData()
            })
            break
        //doule
        case 14:
            self.clickSingleDouble(isSingle: false, cellPos: cellPos,handler: {() in
                self.playPaneTableView.reloadData()
            })
            break
        case 15:
            let ball = self.ballonDatas[cellPos]
            for ballNumInfo in ball.ballonsInfo{
                ballNumInfo.isSelected = false
            }
            self.playPaneTableView.reloadData()
            break
        default:
            break
        }
        
        onNumBallClickCallback(number: "", cellPos: 0)
    }
    
    func clearBottomUIValue(resetBeishu:Bool = true,fromPlayCellSelect:Bool = false){
        if self.selectMode == YUAN_MODE {
            modeBtn.setTitle("元", for: .normal)
            multiplyValue = 1.0
        }else if self.selectMode == JIAO_MODE {
            modeBtn.setTitle("角", for: .normal)
            multiplyValue = 0.1
        }else if self.selectMode == FEN_MODE {
            modeBtn.setTitle("分", for: .normal)
            multiplyValue = 0.01
        }
        
        if isPeilvVersion(){
            if !fromPlayCellSelect {
                creditbottomTopField.text = ""
            }
            
            bottomZhushuTV.isHidden = true
            
            let count = self.selectedPlayRulesModel.getCurrentCount()
            bottomMoneyTV.text = String.init(format: "共%d注,%.1f元", count,0)
            self.sortWhenSelectPeilvNumber = 0;
        }else{
            bottomZhushuTV.isHidden = false
            if resetBeishu {
                beishu_money_input.text = "1"
            }
            bottomZhushuTV.text = "共选0注，共0.0元"
            bottomMoneyTV.text = String.init(format: "奖金:0.0元")
        }
    }
    
    //奖金版号码选择时回调的方法
    func onNumBallClickCallback(number: String, cellPos: Int) {
        
        // 特殊处理 修复快三二同号单选 选中号码bug 未篡改任何选号逻辑 jk
        if self.subPlayCode == "k3ethdx" {
            var identNumData        = self.ballonDatas[0].ballonsInfo!
            var defferentNumData    = self.ballonDatas[1].ballonsInfo!
            
            var isFound = false
            for index in 0..<identNumData.count {
                let identInfo = identNumData[index]
                let defferentInfo = defferentNumData[index]
                if identInfo.num == number && identInfo.isSelected == true && defferentInfo.isSelected == true {
                    defferentInfo.isSelected = false
                    self.ballonDatas[1].ballonsInfo[index] = defferentInfo
                    isFound = true
                    let indexPath = NSIndexPath(row: 1, section: 0)
                    self.playPaneTableView.reloadRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.automatic)
                    break
                }
            }
            if isFound == false {
                for index in 0..<defferentNumData.count {
                    let identInfo = identNumData[index]
                    let defferentInfo = defferentNumData[index]
                    if defferentInfo.num == number && defferentInfo.isSelected == true && identInfo.isSelected == true {
                        identInfo.isSelected = false
                        self.ballonDatas[0].ballonsInfo[index] = identInfo
                        let indexPath = NSIndexPath(row: 0, section: 0)
                        self.playPaneTableView.reloadRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.automatic)
                        break
                    }
                }
            }
        }
        //播放按键音
        if YiboPreference.isPlayTouzhuVolume(){
            playVolume()
        }
        //选择球的时候清除本地已选择的金额，倍数，注数等
        clearBottomValue(clearModeAndBeiShu: false, clearBeishu: false,handler: {() in
            clearBottomUIValue(resetBeishu:false)
        })
        //开始计算投注号码串及注数
        calc_bet_orders(selectedDatas: self.ballonDatas,selectNumHandler: {[weak self] (rightRakeback,rightMaxOdds, rightMinOdds) in
            guard let weakSelf = self else {return}
            
            weakSelf.current_odds = rightMaxOdds
            if rightRakeback > 0{
                weakSelf.hideOfficialSlider(hide: false)
                weakSelf.oddSlider.setupLogic(maxRakeback: rightRakeback, maxOdds: rightMaxOdds, minOdds: rightMinOdds)
            }else{
                weakSelf.hideOfficialSlider(hide: true)
            }
        },updateBottomHandler: {[weak self] () in
            guard let weakSelf = self else {return}
            weakSelf.updateBottomUI()
        })
    }
    
    //隐藏官方返水条相关控件
    func hideOfficialSlider(hide:Bool) {
        oddSlider.isHidden = hide
//        bet_kick_add.isHidden = hide
//        bet_kick_minus.isHidden = hide
        addBetBtn.isHidden = hide
        subBetBtn.isHidden = hide
        creditbottomSlideLeftLabel.isHidden = hide
        
        if !isPeilvVersion() {
            officialOddsSliderH.constant = hide ? 0 : 25
            bottomBgBarHeight.constant = hide ? 85 :110
        }
    }
    
    //MARK: - 已抽取
    private func clearAfterBetSuccessAction(isRandom:Bool = false) {
        clearAfterBetSuccess(isRandom:isRandom,handler: {() in
//            self.scrollToTop()
            clearMutiPlaysBetAndReload()
            self.playPaneTableView.reloadData()
            
            super.clearBottomValue(clearModeAndBeiShu: true, resetSelectMode:getResetBetYJFModel(),handler: {() in
                clearBottomUIValue(resetBeishu:false)
            })
        })
    }
    
    func formPeilvBetDataAndEnterOrderPage(order:[PeilvOrder],peilvs:[BcLotteryPlay],lhcLogic:LHCLogic2,isRandom:Bool = false) {
        clearAfterBetSuccessAction(isRandom: isRandom)
        
        super.formPeilvBetDataAndEnterOrderPage(isRandom:isRandom,order: order, peilvs: peilvs, lhcLogic: lhcLogic) {
        }
    }
    
    //更新底部视图数据
    func updateBottomUI() -> Void {
        //官方版本
        if !isPeilvVersion(){
            var zhushu = 0
            var total_money:Float = 0.0
            let currend_mode:Int = convertPostMode(mode: self.selectMode)
            if !self.official_orders.isEmpty{
                for item in self.official_orders{
                    zhushu += item.n
                    if currend_mode > 0{
                        //                        let money = Float((zhushu*self.selectedBeishu*2)/currend_mode)
                        //                        total_money += item.a
                    }
                }
            }
            total_money = (Float(zhushu) * Float(self.selectedBeishu) * Float(2))
            //                / Float(currend_mode)
            //            let total_win = current_odds - Float(zhushu*2)
            bottomZhushuTV.isHidden = false
            
            let total_moneyStr = String.init(format: "%.3f",(total_money * multiplyValue))
            if let total_moneyFloat = Float(total_moneyStr) {
                betShouldPayMoney = Double(total_moneyFloat)
                bottomZhushuTV.text = String.init(format: "共选%d注,共%.2f元",zhushu,total_moneyFloat)
            }
            
            let bottomMoneyStr = String.init(format: "%.3f",current_odds * multiplyValue * Float(selectedBeishu))
            officalBonus = Double(current_odds * multiplyValue)
            if let bottomMoneyFloat = Double(bottomMoneyStr) {
                bottomMoneyTV.text = String.init(format: "奖金:\(bottomMoneyFloat)元")
            }
            
            let awardStr = String.init(format: "%.3f", self.current_odds * multiplyValue)
            if let awardFloat = Double(awardStr) {
                currentOddTV.text = "\(awardFloat)元"
            }
            
        }else{
            
            var total_money:Float = 0.0
            
            var localHonest_orders:[PeilvOrder] =  []
            var isCurrentHonest_orders = false //遍历的是否是当前的 honest_orders
     
            for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
                
                if peilvListDatas.count > 0 {
                    //和上次 是 同一玩法,并且 【是】本地玩法,并且没有 header
                    if !isEmptyString(str: subPlayCode) && peilvListDatas[0].fakeParentCode == fakeSubPlayCode && !isHeaderPlay(parentCode: subPlayCode) && !isEmptyString(str: peilvListDatas[0].fakeParentCode) {
                        peilvListDatas[0].parentName = subPlayName
                        isCurrentHonest_orders = true
                        localHonest_orders = super.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                        break
                        
                        //和上次 是 同一玩法,并且 【不是】本地玩法,并且没有 header
                    }else if !isEmptyString(str: subPlayCode) && peilvListDatas[0].parentCode == subPlayCode && !isHeaderPlay(parentCode: subPlayCode) && isEmptyString(str: fakeSubPlayCode) && isEmptyString(str: peilvListDatas[0].fakeParentCode) {
                        peilvListDatas[0].parentName = subPlayName
                        isCurrentHonest_orders = true
                        localHonest_orders = super.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                        break
                        
                        //【有】header
                    }else if !isEmptyString(str: headerCode) && peilvListDatas[0].code == headerCode && isHeaderPlay(parentCode: subPlayCode) {
                        
                        //合肖是用以判断，合-中 or 合-不中
                        if peilvListDatas[0].peilvs.count > 0 && subPlayCode == "hx" {
                            if peilvListDatas[0].peilvs[0].code == moreHeadersSubCode {
                                peilvListDatas[0].parentName = subPlayName
                                isCurrentHonest_orders = true
                                localHonest_orders = super.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                                break
                            }
                        }else {
                            peilvListDatas[0].parentName = subPlayName
                            isCurrentHonest_orders = true
                            localHonest_orders = super.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                            break
                        }
                        
                    }else if !isEmptyString(str: headerCode) && peilvListDatas[0].code == headerCode && peilvListDatas[0].fakeParentCode.isEmpty  {
                        peilvListDatas[0].parentName = subPlayName
                        isCurrentHonest_orders = true
                        localHonest_orders = super.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                        break
                    }
                }
           
            }
            
            calculateTotalMoneyInNormal()
            
            var currentZhushu = 0 //当前玩法注数
            
            for item in localHonest_orders {
                if isCurrentHonest_orders {
                    currentZhushu += 1
                }
                
                if !is_fast_bet_mode{
                    total_money += item.a
                    betShouldPayMoney = Double(total_money)
                }else{
                    if is_fast_bet_mode && !isEmptyString(str: self.betMoneyWhenPeilv){
                        total_money += Float(self.betMoneyWhenPeilv)!
                        betShouldPayMoney = Double(total_money)
                    }else{
                        total_money += item.a
                    }
                }
            }
            
            bottomZhushuTV.isHidden = true
            
            if isHeaderPlay(parentCode: subPlayCode) {
                self.selectedPlayRulesModel.currentConunt = calculateSubHeaderRulesCount()
            }else {
                self.selectedPlayRulesModel.currentConunt = currentZhushu
            }
            
            let count = self.selectedPlayRulesModel.getTotoalCount()
            
            if is_fast_bet_mode && !isEmptyString(str: self.betMoneyWhenPeilv) {
                total_money = Float(Double(count * Int(betMoneyWhenPeilv)!))
            }
            
            bottomMoneyTV.text = String.init(format: "%d注,%.3f元", count,total_money)
            
            if self.selectedPlayRulesModel.getTotoalCount() > 200 {
                showToast(view: self.view, txt: "最多选中200注")
            }
        }
    }
    
    
    /// 遍历每一个点击过的注单
    private func calculateTotalMoneyInNormal() {

    }
    
    func clickBigSmall(isBig:Bool,cellPos:Int) -> Void{
        let ball = self.ballonDatas[cellPos]
        for index in 0...ball.ballonsInfo.count-1{
            if index >= ball.ballonsInfo.count/2{
                ball.ballonsInfo[index].isSelected = isBig ? true : false
            }else{
                ball.ballonsInfo[index].isSelected = isBig ? false : true
            }
        }
        self.playPaneTableView.reloadData()
    }
    
    //摇一摇结束的通知   let theViewControllerYouSee = UIViewController.currentViewController()
    @objc func onShakEnd(nofi : Notification){
        if !YiboPreference.getShakeTouzhuStatus(){
            return
        }
        
        if isBetNotAble() {
            showToast(view: self.view, txt: fenPanTips)
            return
        }
        
        playVolumeWhenEndShake();
        self.startRandomBet(orderCount: 1)
    }
    
    //摇一摇开始的通知
    @objc func onShakeStart(nofi : Notification){
        if !YiboPreference.getShakeTouzhuStatus(){
            return
        }
        playVolumeWhenStartShake()
    }
    
    /**
     * 根据玩法栏选择好的玩法，刷新列表数据
     * @param selected_play 用户选择的侧边玩法数据
     * @param odds  选择的侧边玩法对应的所有子玩法对应的所有赔率项数据
     */
    func refresh_page_after_honest_odds_obtain(selected_play: inout BcLotteryPlay?,odds:[HonestResult],fromPlayCellSelect:Bool = false) -> Void{
        
        self.messageLabel().isHidden = true
        
        if selected_play == nil{
            peilvListDatas.removeAll()
            playPaneTableView.tableHeaderView = nil
            playPaneTableView.reloadData()
            return
        }
        if isEmptyString(str: cpBianHao) || isEmptyString(str: subPlayCode){
            return
        }
        
        var hadSelected = false
        if peilvListDatasSuperSet.count == 0 {
            hadSelected = false
        }else {
            //判断peilvListDatas SuperSet 是否存在当前选择的 peilvListDatas
            

            for (index,peilvs) in peilvListDatasSuperSet.enumerated() {
                //表示已经选择过该左侧边栏玩法
                if !isEmptyString(str: selected_play?.fakeParentName ?? "") && peilvs[0].fakeParentName == selected_play?.fakeParentName {
                    hadSelected = true
                    break
                }else if peilvs[0].parentCode == selected_play?.code  && isEmptyString(str: selected_play?.fakeParentName ?? "") {
                    hadSelected = true
                    break
                }else if index == peilvListDatasSuperSet.count - 1 {
                    // 未选择过该左侧边栏玩法
                    hadSelected = false
                }
//                //表示已经选择过该左侧边栏玩法
////                || peilvs[0].name == selected_play?.name
//                if peilvs[0].parentCode == selected_play?.code  {
//                    hadSelected = true
//                    break
//                }else if index == peilvListDatasSuperSet.count - 1 {
//                    // 未选择过该左侧边栏玩法
//                    hadSelected = false
//                }
            }
        }
        
        if !hadSelected {
            PeilvLogic.insertOddsToPlayDatas(selectPlay: &selected_play, odds: odds)
        }
        
        /////////////////////////
        
//        PeilvLogic.insertOddsToPlayDatas(selectPlay: &selected_play, odds: odds)
        selectedSubPlayCode = (selected_play?.code)!
        choosedPlay = selected_play
        
        
        // 通过禁用子玩法项没有赔率及玩法项下所有子玩法项被禁用，移除该玩法
        if let data = self.choosedPlay?.children {
            
            var lotteryPlays = [BcLotteryPlay]()
            var lotterOdds = [HonestResult]()
            
            for index in 0..<data.count {
                let model = data[index]
                if model.peilvs.count != 0 {
                    honest_odds.forEach { (HonestResult) in
                        if HonestResult.code == model.code{
                            lotteryPlays.append(model)
                            lotterOdds.append(/*honest_odds[index]*/HonestResult)
                        }
                    }
                }
            }
            self.choosedPlay?.children = lotteryPlays
            self.honest_odds = lotterOdds
        }
        //若是连码等特殊玩法时，展示附加条件选择条
        lhcLogic.initializeIndexTitle()
        
        guard let play = selected_play else{return}
        if play.children.isEmpty{
            return
        }
        
        if let headerView = lhcLogic.createHeaderView(controller: self, playCode: selectedSubPlayCode, odds: self.honest_odds) {
            
            var headerFrame = headerView.frame
            headerFrame.size =  CGSize(width:isPlayBarHidden ? KSCREEN_WIDTH : KSCREEN_WIDTH * playPanWScale, height: headerFrame.size.height)
            playPaneTableView.tableHeaderView = headerView
            
            if isPeilvVersion(){
                dynamicCalculateOdds(currentRate: (1 - creditbottomTopSlider.value), handler: {() in
                    
                })
            }
            
            if let selectedPlay = self.choosedPlay{
                let map = lhcLogic.getListWhenSpecialClick(play: selectedPlay)
                if map != nil {
                    lhcLogic.setFixFirstNumCountWhenLHC(fixCount: (map?.keys.contains("fixFirstCount"))! ? map!["fixFirstCount"] as! Int : 0)
                    
                    lhcLogic.setFixFirstNumCountWhenLHC(fixCount: (map?.keys.contains("secondCategoryIndex"))! ? map!["secondCategoryIndex"] as! Int : 0)
                    let list = (map?.keys.contains("datas"))! ? map!["datas"] : ([] as AnyObject)
                    
                    if let datas = list as? [BcLotteryPlay] {
                        for (_,data) in datas.enumerated() {
                            data.parentCode = subPlayCode
                        }
                        
                        super.prapare_peilv_datas_for_tableview(peilvWebResults:datas)
                    }
                }
                
                clearBottomValue(clearModeAndBeiShu: true, handler: {() in
                    clearBottomUIValue(fromPlayCellSelect: switchCanMutiRulesBet(isPeilv: true))
                })
            }
        }else{
            lhcLogic.playName = (selected_play?.name)!
            playPaneTableView.tableHeaderView = nil
            
            super.prapare_peilv_datas_for_tableview(peilvWebResults:(selected_play?.children)!)

            clearBottomValue(clearModeAndBeiShu: true, handler: {() in
                clearBottomUIValue(fromPlayCellSelect: switchCanMutiRulesBet(isPeilv: true))
            })
        }
        //        scrollToTop()
         
        playPaneTableView.reloadData()
        
        if isPeilvVersion(){
            super.update_honest_seekbar(selectPlay: selected_play!, odds: odds,handler: {(maxOddResult) in
                oddSlider.setupLogic(odd: maxOddResult)
                if maxOddResult.rakeback > 0 && switchShowBetSliderBar() {
                    bet_kick_minus.isHidden = false
                    bet_kick_add.isHidden = false
                    creditbottomSlideLeftLabel.isHidden = false
                }else{
                    bet_kick_minus.isHidden = true
                    bet_kick_add.isHidden = true
                    creditbottomSlideLeftLabel.isHidden = true
                }
                creditbottomTopSlider.setupLogic(odd: maxOddResult)
                
            })
        }
    }
    
    private func formatModel(original to:[BcLotteryPlay]) {
        
    }
    
    func refreshPaneAndClean(noClearView:Bool) -> Void {
        self.playPaneTableView.reloadData()
        if !noClearView{
            clearBottomValue(clearModeAndBeiShu: noClearView, handler: {() in
                clearBottomUIValue()
            })
        }
    }
    
    //在直接赔率项输入项中输入金额后，回调此方法来计算注数
    func callAsyncCalcZhushu(data: PeilvWebResult, cellIndex: Int, row: Int, volume: Bool) {
        onCellSelect(data: data, cellIndex: cellIndex, row: row,volume:volume)
    }
    
    //赔率cellviewview cell点击回调
    func onCellSelect(data: PeilvWebResult, cellIndex: Int,row:Int,volume:Bool=true) {
        data.parentsPlayCode = subPlayCode.isEmpty ? selectedSubPlayCode : subPlayCode
        //播放按键音
        if volume{
            if YiboPreference.isPlayTouzhuVolume(){
                playVolume()
            }
        }
        let mCell = self.playPaneTableView.cellForRow(at: IndexPath.init(row: row, section: 0))
        if mCell == nil{
            return
        }
        let cell:PeilvTouzhuCell = mCell as! PeilvTouzhuCell
        var cell2:PeilvCollectionViewCell!
        
        if let s = cell.tableContent.cellForItem(at: IndexPath.init(row: cellIndex, section: 0)) as? PeilvCollectionViewCell {
            cell2 = s
        }
        
        if cell2 != nil{
            let clickData = self.peilvListDatas[row].peilvs[cellIndex]
            if !clickData.checkbox{
                if clickData.isSelected{
                    if !isEmptyString(str: self.betMoneyWhenPeilv){
                        cell2.moneyInputTV.text = self.betMoneyWhenPeilv
                        clickData.inputMoney = Float(self.betMoneyWhenPeilv)!
                    }
                }else{
                    cell2.moneyInputTV.text = ""
                    clickData.inputMoney = 0
                }
            }
            
            if clickData.isSelected{
                self.sortWhenSelectPeilvNumber += 1;
                clickData.selectedPosSortWhenClick = self.sortWhenSelectPeilvNumber
            }
        }
        
        //开始计算投注号码串及注数
        self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
            self.updateBottomUI()
        } )
    }
    
    private func clearDisableBetStatus() {
        if let timer = self.disableBetCountDownTimer {
            disableBetTime = 0
            timer.invalidate()
        }
        
        YiboPreference.setAbleBet(value: "on")
        setupAbleBetUI(able: !isBetNotAble())
        
        let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(0))
        self.countDownUI.text = String.init(format:"%@", dealDuration)
    }
    
    /// 设置是否可投注状态时，UI变化
    ///
    /// - Parameter able: true可投，false封盘
    private func  setupAbleBetUI(able:Bool) {
        self.betBtn.backgroundColor = able ? UIColor.red : UIColor.lightGray
        self.betBtn.isEnabled = able ? true : false
    }
    
    private func setupJuderTimer() {
        self.judgeTimer = Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(judgeTimerAction), userInfo: nil, repeats: true)
        
        if let timer = self.judgeTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    @objc private func judgeTimerAction() {
        if lastCountdownText == self.countDownUI.text {
            getCountDownByCpCodeAction()
        }else {
            lastCountdownText = self.countDownUI.text ?? ""
        }
    }
    
    //MARK: 头部，快三 开奖结果信息 View,infos 和 大小 单双
    func setupKuaiSanInfoView(width:CGFloat,ballWidth:CGFloat,infos:(String,String,String)) {
        let subViews = moreDrawInfoView.subviews
        for view in subViews {
            view.removeFromSuperview()
        }
        
        //快三彩种开奖结果像左偏移开奖号码，用来显示 和 大小 单双
        let view = UIView()
        view.backgroundColor = UIColor.clear
        
        let margin = (width - ballWidth * 3) / 4
        
        for i in 0...2 {
            let btn = UIButton()
            btn.isUserInteractionEnabled = false
            btn.backgroundColor = UIColor.clear
            btn.layer.cornerRadius = ballWidth * 0.5
            btn.setTitleColor(UIColor.white, for: .normal)
            btn.theme_setBackgroundImage("TouzhOffical.BetTopBall", forState: .normal)
            
            var title = ""
            if i == 0 { //以后换成数组，元祖在index是变量时不能直接调用
                title = infos.0
            }else if i == 1 {
                title = infos.1
            }else if i == 2 {
                title = infos.2
            }
            
            if title == "豹子" {
                btn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
            }else {
                btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            }
            
            btn.setTitle(title, for: .normal)
            
            let x = (CGFloat)(i + 1) * margin + ((CGFloat)(i) * ballWidth)
            let y:CGFloat = (CGFloat(50) - ballWidth) * 0.5
            
            btn.frame = CGRect.init(x: x, y: y, width: ballWidth, height: ballWidth)
            view.addSubview(btn)
        }
        
        view.backgroundColor = UIColor.clear
        
        moreDrawInfoView = view
        leftDrawTopView.addSubview(moreDrawInfoView)
        moreDrawInfoView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-30)
            make.top.equalTo(numViews.snp.top)
            make.bottom.equalTo(numViews.snp.bottom)
            make.width.equalTo(rightMoreInfoViewW)
        }
    }
    
    //彩种切换后的回调
    func onLotSelect(lotData:LotteryData) {
        
        if lotData.lotType == 4 { //快三彩种开奖结果像左偏移开奖号码，用来显示 和 大小 单双
            numViewTri.constant = rightMoreInfoViewW
        }else {
            numViewTri.constant = 0
            moreDrawInfoView.removeFromSuperview()
        }
        
        clearDisableBetStatus()
        
        lotWindow.dismiss()
        self.lotData = lotData
        super.updateLocalConstants(lotData: self.lotData,handler: {(name) in
            self.lot_name_button.setTitle(name, for: .normal)
        })
        if !isSixMark(lotCode: lotData.code!){
            lhcSelect = false
        }
        //        if !isXGLHC(lotCode: lotData.code!){
        self.lhcLogic.lhcBetServerTime = 0
        //        }
        clearBottomValue(clearModeAndBeiShu: true, resetSelectMode: isPeilvVersion() ? true : false,handler: {() in
            clearBottomUIValue()
        })
        self.swtichTouzhuHeader()
        //快乐彩情况下只有信用下注,不用在标题上切换
        if (self.cpTypeCode == "9") {
            self.titleIndictor.isHidden = true
        }
        //重新选择彩票后，需要重新获取彩种对应的玩法
        super.sync_playrule_from_current_lottery(lotType: self.cpTypeCode, lotCode: self.cpBianHao, lotVersion: self.cpVersion,handler: {(lotteryData) -> Void in
            self.sync_local_constan_restart_something_after_playrule_obtain(lottery: lotteryData)
        })
    }
    
    
    //#MARK: ------------------------- 实例化 ---------------------------------
    func messageLabel() -> UILabel {
        if _messageLabel == nil {
            _messageLabel = UILabel(frame: self.playPaneTableView.bounds)
            _messageLabel?.textAlignment = .center
            _messageLabel?.text = "暂未开放,敬请期待!"
            _messageLabel?.textColor = .black
            _messageLabel?.font = UIFont.boldSystemFont(ofSize: kCurrentScreen(x: 50))
            _messageLabel?.isHidden = true
            
            self.playPaneTableView.addSubview(_messageLabel!)
        }
        return _messageLabel!
    }
    
}

extension TouzhNewController : PlayButtonDelegate{
    func onButtonDelegate() {
        
        self.messageLabel().isHidden = true
        
        if let play = self.choosedPlay{
            let map = lhcLogic.getListWhenSpecialClick(play: play)
            if map != nil {
                lhcLogic.setFixFirstNumCountWhenLHC(fixCount: (map?.keys.contains("fixFirstCount"))! ? map!["fixFirstCount"] as! Int : 0)
                let list = (map?.keys.contains("datas"))! ? map!["datas"] : ([] as AnyObject)
                
                if let datas = list as? [BcLotteryPlay] {
                    for (_,data) in datas.enumerated() {
                        data.parentCode = subPlayCode
                    }
                    
                    //判断是否 是 合肖
                    let data = datas[0]
                    if data.peilvs.count > 0 && subPlayCode == "hx"{
                        moreHeadersSubCode = data.peilvs[0].code
                    }
                    
                    super.prapare_peilv_datas_for_tableview(peilvWebResults:datas)
                }
                
                if isPeilvVersion() {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        //开始计算投注号码串及注数
                        self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
                            self.updateBottomUI()
                        } )
                    }
                }
            }
            
            playPaneTableView.reloadData()
            clearBottomValue(clearModeAndBeiShu: true, handler: {() in
                clearBottomUIValue()
            })
        }
    }
    
    //MARK: -UI
    //MARK: - 主题相关 && UI初始设置
    private func setupUI() {
        coolHotButton.isSelected = true
        fastModeButton.isSelected = true
        creditBottomTopBar.isHidden = true
        
        modeBtn.layer.cornerRadius = 3.0
        modeBtn.layer.masksToBounds = true
        beishu_money_input.layer.cornerRadius = 3.0
        beishu_money_input.layer.masksToBounds = true
        clearBtn.layer.masksToBounds = true
        clearBtn.layer.cornerRadius = 3.0
        betBtn.layer.cornerRadius = 3.0
        betBtn.layer.masksToBounds = true
        creditbottomTopField.layer.cornerRadius = 3.0
        creditbottomTopField.layer.masksToBounds = true
        
        creditBottomHistoryButton.addTarget(self, action: #selector((betRecordClickEvent(_:))), for: .touchUpInside)
    }
    
    private func setupTheme() {
        setupNoPictureAlphaBgView(view: self.playPanBGView)
        setupNoPictureAlphaBgView(view: self.touzhuHeaderBgView)
        creditBottomHistoryImg.theme_image = "TouzhOffical.SwiftPopMenu.firstImage"
        
        playRuleTableView.backgroundColor = UIColor.white.withAlphaComponent(0)
        playRuleTableView.layer.borderWidth = 0.3
        playRuleTableView.layer.theme_borderColor = "FrostedGlass.Touzhu.separateLineColor"
        
        playPaneTableView.layer.borderWidth = 0.3
        playPaneTableView.layer.theme_borderColor = "FrostedGlass.Touzhu.separateLineColor"
        
        verticalLine.theme_backgroundColor = "FrostedGlass.Touzhu.separateLineColor"
        
        bottomLine.theme_backgroundColor = "FrostedGlass.Touzhu.separateLineColor"
        bottomViewTopLine.theme_backgroundColor = "FrostedGlass.Touzhu.separateLineColor"
        
        //        bottomDottedLine.backgroundColor = UIColor.clear
        //        topDottedLine.backgroundColor = UIColor.clear
        //
        //        TouzhViewLogic().drawDashLineAction(lineView: bottomDottedLine)
        //        TouzhViewLogic().drawDashLineAction(lineView: topDottedLine)
        
        //        lot_name_button.layer.cornerRadius = 3.0
        lot_name_button.theme_backgroundColor = "FrostedGlass.Touzhu.lotNameBtnColor"
        
        recent_open_result_button.layer.masksToBounds = true
        recent_open_result_button.theme_setBackgroundImage("TouzhOffical.dropDownHisLotteryBtnBackImg", forState: .normal)
        random_bet_imgBtn.theme_setImage("TouzhOffical.robot_bet_button", forState: .normal)
    }
    
    private func refreshBottomBar() {
        if isPeilvVersion() {
            bottomBgBarHeight.constant = 90 + 50 //50筹码
            creditBottomTopBar.isHidden = false
            creditSliderBgBar.isHidden = true
            money_beishu_mode_view.isHidden = true
            chipsBgView.isHidden = false
            
            setUpChipsView()
        }else {
            bottomBgBarHeight.constant = oddSlider.isHidden ? 85 :110
            creditBottomTopBar.isHidden = true
            creditSliderBgBar.isHidden = false
            money_beishu_mode_view.isHidden = false
            chipsBgView.isHidden = true
        }
        
        if !switchShowBetSliderBar() {
            oddSlider.isHidden = true
            creditbottomTopSlider.isHidden = true
            hideOfficialSlider(hide: true)
        }
    }
    
    //筹码视图
    private func setUpChipsView() {
        for view in chipsBgView.subviews {
            view.removeFromSuperview()
        }
        
        let moneyItems = getFastMoneySetting()
        
        let scrollView = UIScrollView()
        let containerView = UIView()
        chipsBgView.addSubview(scrollView)
        scrollView.addSubview(containerView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalTo(scrollView)
            make.height.equalTo(scrollView)
        }
        
        let moneyBtnWH:CGFloat = 45
        let btnMargin:CGFloat = 5 //筹码左右间距
        
        for (index,money) in moneyItems.enumerated() {
            let moneyBtn = UIButton()
            configBtnFontAndBgImg(money: money, button: moneyBtn,index: index)
            containerView.addSubview(moneyBtn)
            
            moneyBtn.snp.makeConstraints { (make) in
                make.centerY.equalTo(25)
                make.left.equalTo( CGFloat(index) * moneyBtnWH + CGFloat(index + 1) * btnMargin )
                make.width.height.equalTo(moneyBtnWH)
                
                if index + 1 == moneyItems.count {
                    make.right.equalTo(containerView.snp.right)
                }
            }
        }
    }
    
    func configBtnFontAndBgImg(money:String,button:UIButton,index:Int) {
        guard let num = Int64(money) else {return}
        
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitle(money, for: .normal)
        
        button.tag = 100*100 + index
        button.addTarget(self, action: #selector(clickChipAction), for: .touchUpInside)
        
        //设置背景图
        var imgName = ""
        if (num == 1) {
            imgName = "chip_1"
        } else if (num == 2) {
            imgName = "chip_2"
        } else if (num > 2 && num <= 5) {
            imgName = "chip_3"
        } else if (num > 5 && num <= 10) {
            imgName = "chip_4"
        } else if (num > 10 && num <= 20) {
            imgName = "chip_5"
        } else if (num > 20 && num <= 25) {
            imgName = "chip_6"
        } else if (num > 5 && num <= 50) {
            imgName = "chip_7"
        } else if (num > 50 && num <= 100) {
            imgName = "chip_8"
        } else if (num > 100 && num <= 200) {
            imgName = "chip_9"
        } else if (num > 200 && num <= 500) {
            imgName = "chip_10"
        } else if (num > 500 && num <= 1000) {
            imgName = "chip_11"
        } else if (num > 1000 && num <= 5000) {
            imgName = "chip_12"
        } else {
            imgName = "chip_13"
        }
        
        button.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
        
        //设置字号
        var fontSize:CGFloat = 16
        if money.length == 2 {
            fontSize = 15
        }else if money.length == 3 {
            fontSize = 13
        }else if money.length >= 4 {
            fontSize = 11
        }
        
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    @objc func clickChipAction(sender:UIButton) {
        let index = sender.tag - 100*100
        
        let moneyItems = getFastMoneySetting()
        if index < moneyItems.count {
            let moneyValue = moneyItems[index]
            selectMoneyFromDialog(money: moneyValue)
        }
    }
    
    private func refreshLotteryTime() {
        if isPeilvVersion() {
            betDeadlineDesLabel.textColor = UIColor.black
        }else {
            betDeadlineDesLabel.textColor = UIColor.white
        }
    }
    
    //MARK: 冷热、遗漏；一般，快捷的 hide，show
    private func refreshButtons() {
        if isPeilvVersion() {
            coolHotButton.isHidden = true
            missingNumButton.isHidden = true
        }else {
            if LotteryPlayLogic.showCoolMissingPlays(playCode: subPlayCode){
                coolHotButton.isHidden = false
                missingNumButton.isHidden = false
                self.showCodeRank = true //是否显示冷热遗漏数据
            }else{
                coolHotButton.isHidden = true
                missingNumButton.isHidden = true
                self.showCodeRank = false
            }
        }
        fastModeButton.isHidden = isPeilvVersion() ? false : true
        normalModeButton.isHidden = isPeilvVersion() ? false : true
    }
    
    override func viewDidLayoutSubviews() {
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        guard let headerView = playPaneTableView.tableHeaderView else {return}
        
        var headerFrame = headerView.frame
        headerFrame.size =  CGSize(width:isPlayBarHidden ? KSCREEN_WIDTH : KSCREEN_WIDTH * playPanWScale, height: headerFrame.size.height)
    }
}

