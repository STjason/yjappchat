//
//  SportsFilterSelectView.swift
//  gameplay
//
//  Created by ken on 2019/5/17.
//  Copyright © 2019 yibo. All rights reserved.
//
import UIKit

class SportsFilterSelectView: UIView {
    
    
    var arrayStr:[String] = ["所有","足球","蓝色"]
    var arrayIndex:[String] = ["0","1","2"]
    
    
    static let fixedTableViewRowHeight: CGFloat = 51.0
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    var selectedIndex: Int = 0
    var didSelected: ( ( Int, String) -> Void)?
    var kLenny_InsideDataSource: [AllLotteryTypesSubData]!
    
    var kTitleLabelHeight: CGFloat = 30
    
//    var kHeight: CGFloat {
//        let h = LennySelectView.fixedTableViewRowHeight * CGFloat(kLenny_InsideDataSource.count) + kTitleLabelHeight
//        return  h <  300 ? h : 300
//    }
    
    private var mainTableView = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    //type = 1 moren
    convenience init(type:NSInteger, viewTitle: String) {
        self.init(frame: CGRect.zero)
//        kLenny_InsideDataSource = dataSource
        
        if type == 2 {
            
            arrayStr = ["所有","未结算","已结算","比赛腰折"]
            arrayIndex = ["0","1","2","3"]
        }else{
            arrayStr = ["所有","足球","蓝球"]
            arrayIndex = ["0","1","2"]
        }
        
        let label = UILabel()
        addSubview(label)
        label.whc_Top(0).whc_Left(0).whc_Right(0).whc_Height(kTitleLabelHeight)
        //        label.backgroundColor = UIColor.mainColor()
        label.theme_backgroundColor = "Global.themeColor"
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "     " + viewTitle
        
        self.addSubview(mainTableView)
        mainTableView.whc_AutoSize(left: 0, top: 30, right: 0, bottom: 0)
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.tableFooterView = UIView()
        mainTableView.separatorStyle = .none
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        
        let view = UIView()
        view.backgroundColor = UIColor.init(white: 0.3, alpha: 0.5)
        self.window?.insertSubview(view, belowSubview: self)
        view.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
        view.tag = 1022
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(didTaped))
        view.addGestureRecognizer(tap)
        
        if selectedIndex >= 1 {
            mainTableView.scrollToRow(at: IndexPath.init(row: selectedIndex - 1, section: 0), at: UITableView.ScrollPosition.middle, animated: true)
        }
        
    }
    
    @objc func didTaped() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.1
            self.transform = CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        }) { (_) in
            
            let view = self.window?.viewWithTag(1022)
            view?.removeFromSuperview()
            self.mainTableView.whc_ResetConstraints().removeFromSuperview()
            self.alpha = 0
        }
    }
}

extension SportsFilterSelectView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        for cell in tableView.visibleCells {
            cell.imageView?.image = UIImage(named: "unselected")
        }
        tableView.cellForRow(at: indexPath)?.imageView?.image = UIImage(named: "selected")
        
        didSelected?(indexPath.row,arrayIndex[indexPath.row])
        didTaped()
    }
}

extension SportsFilterSelectView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayIndex.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return LennySelectView.fixedTableViewRowHeight
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell" + String(indexPath.row))
        
        cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell" + String(indexPath.row))
        cell?.textLabel?.text = arrayStr[indexPath.row]
        cell?.imageView?.image = UIImage(named: "unselected")
        if indexPath.row == selectedIndex {
            cell?.imageView?.image = UIImage(named: "selected")
        }
        return cell!
    }
}
