//
//  SportsFilterView.swift
//  gameplay
//
//  Created by ken on 2019/5/17.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class SportsFilterView: UIView {
    
    let arrayStr:[String] = ["所有","篮球","足球"]
    let arrayStr1:[String] = ["所有","未结算","已结算","比赛腰折"]
    
    private let button_Include = UIButton()
    private let button_dateEnd = UIButton()
    private let button_dateStart = UIButton()
    
    private let button_Cancel = UIButton()
    private let button_Confirm = UIButton()
    private let checkButton = UIButton()
    
    //    private let textField = UITextField()
    private let button_Selecter = UIButton()
    private let textField_UserName = UITextField()
    
    //new
     private let textField_UserName1 = UITextField()
     private let textField_UserName2 = UITextField()
     private let button_Selecter1 = UIButton()
    
    
    var controller:UIViewController!
    
    lazy var  start_Timer:CustomDatePicker = {
        let datePick = CustomDatePicker()
        datePick.tag = 101
        return datePick
    }()//开始时间选择器02
    
    lazy var end_Timer:CustomDatePicker = {
        let datePick = CustomDatePicker()
        datePick.tag = 102
        return datePick
    }()//结束时间选择器02
    
    
    private var startTime:String = ""
    private var endTime:String = ""
    private var includeSwitch = "0"
    

    private var qiuduitype = "0"
    private var touzhutype = "0"
    
    
    func TESTtext(mbt:UITextField,mblstr:String,mlt:UITextField,mlttop:NSInteger){
        addSubview(mbt)
        mbt.whc_Top(CGFloat(mlttop), toView: mlt).whc_RightEqual(mlt).whc_Width(200).whc_Height(30)
        mbt.borderStyle = .roundedRect
        mbt.font = UIFont.systemFont(ofSize: 14)
        mbt.placeholder = "请输入" + mblstr
       
        let mbl = UILabel()
        addSubview(mbl)
        mbl.whc_CenterYEqual(mbt).whc_Right(10, toView: mbt).whc_WidthAuto().whc_Height(10)
        mbl.font = UIFont.systemFont(ofSize: 14)
        mbl.textColor = UIColor.cc_51()
        mbl.text = mblstr + ":"
    }
    
    func buttontype(button:UIButton,action:Selector,lstr:String,mlt:UIView,mlttop:NSInteger){
        addSubview(button)
        button.whc_Top(CGFloat.init(mlttop), toView: mlt).whc_RightEqual(mlt).whc_Width(200).whc_Height(30)
        button.setTitle("所有", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button_Selecter.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setImage(UIImage(named: "pulldown"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 200 - 30, bottom: 0, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 120)
        button.setTitleShadowColor(UIColor.ccolor(with: 136, g: 136, b: 136), for: .highlighted)
        button.addTarget(self, action:action, for: .touchUpInside)
        button.whc_AddBottomLine(0.5, color: UIColor.ccolor(with: 224, g: 224, b: 224))
        button.layer.borderColor = UIColor.cc_224().cgColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        
//      #selector(button_SelectorClickHandle)
        
        let labal_Type = UILabel()
        addSubview(labal_Type)
        labal_Type.whc_CenterYEqual(button).whc_Right(5, toView: button).whc_WidthAuto().whc_Height(15)
        labal_Type.font = UIFont.systemFont(ofSize: 12)
        labal_Type.textColor = UIColor.cc_51()
        labal_Type.text = lstr
    }
    
    convenience init(height: CGFloat,controller:UIViewController) {
        self.init()
        
        self.theme_backgroundColor = "FrostedGlass.filterViewColor"
        
        //        initDate()
        self.controller = controller
        addSubview(textField_UserName)
        textField_UserName.whc_Top(20).whc_Right(44).whc_Width(200).whc_Height(30)
        textField_UserName.borderStyle = .roundedRect
        textField_UserName.font = UIFont.systemFont(ofSize: 14)
        //用户名的text
        
        let label_Name = UILabel()
        addSubview(label_Name)
        label_Name.whc_CenterYEqual(textField_UserName).whc_Right(10, toView: textField_UserName).whc_WidthAuto().whc_Height(10)
        label_Name.font = UIFont.systemFont(ofSize: 14)
        label_Name.textColor = UIColor.cc_51()
        label_Name.text = "用户名"
        textField_UserName.placeholder = "请输入用户名"
        
    
        TESTtext(mbt: textField_UserName1, mblstr: "主队名", mlt: textField_UserName, mlttop: 5)
        TESTtext(mbt: textField_UserName2, mblstr: "客队名", mlt: textField_UserName1, mlttop: 5)
        
        
        button_Selecter1.tag = 100
        buttontype(button: button_Selecter1, action: #selector(button_SelectorClickHandle(button:)), lstr: "球类:", mlt: textField_UserName2, mlttop: 5)
        
        buttontype(button: button_Selecter, action: #selector(button_SelectorClickHandle(button:)), lstr: "投注状态:", mlt: button_Selecter1, mlttop: 5)
        
        
        

        addSubview(button_dateEnd)
        button_dateEnd.whc_Top(5, toView: button_Selecter).whc_RightEqual(button_Selecter).whc_Height(30)
        //        .whc_Width(97)
        button_dateEnd.setTitleColor(UIColor.cc_136(), for: .normal)
        button_dateEnd.setTitle(self.endTime, for: .normal)
        button_dateEnd.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        button_dateEnd.layer.borderColor = UIColor.cc_224().cgColor
        button_dateEnd.layer.borderWidth = 0.5
        button_dateEnd.layer.cornerRadius = 3
        button_dateEnd.clipsToBounds = true
        button_dateEnd.addTarget(self, action: #selector(button_EndTimeClickHandle), for: .touchUpInside)
        
        let label = UILabel()
        addSubview(label)
        label.whc_CenterYEqual(button_dateEnd).whc_WidthAuto().whc_Height(5).whc_Right(3, toView: button_dateEnd)
        label.font = UIFont.systemFont(ofSize: 9)
        label.textColor = UIColor.black
        label.text = "--"
        
        addSubview(button_dateStart)
        
        button_dateStart.whc_CenterYEqual(button_dateEnd).whc_Right(3, toView: label).whc_WidthEqual(button_dateEnd).whc_HeightEqual(button_dateEnd)
        button_dateStart.frame = CGRect.init(x: 50, y: 110, width: 60, height: 60)
        button_dateStart.setTitle(self.startTime, for: .normal)
        button_dateStart.setTitleColor(UIColor.cc_136(), for: .normal)
        button_dateStart.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        button_dateStart.layer.borderWidth = 0.5
        button_dateStart.layer.borderColor = UIColor.cc_224().cgColor
        button_dateStart.layer.cornerRadius = 3
        button_dateStart.clipsToBounds = true
        button_dateStart.isHidden = false
        button_dateStart.addTarget(self, action: #selector(button_StartTimeClickHandle), for: .touchUpInside)
        
        let label_DateTitle = UILabel()
        addSubview(label_DateTitle)
        label_DateTitle.whc_CenterYEqual(button_dateStart).whc_Right(7, toView: button_dateStart).whc_Height(12).whc_WidthAuto()
        label_DateTitle.textAlignment = .right
        label_DateTitle.font  = UIFont.systemFont(ofSize: 12)
        label_DateTitle.textColor = UIColor.cc_51()
        label_DateTitle.text = "日期："
        
        addSubview(button_Confirm)
        button_Confirm.whc_Top(9, toView: button_dateEnd).whc_RightEqual(button_dateEnd).whc_Width(50).whc_Height(30)
        addSubview(button_Cancel)
        button_Cancel.whc_TopEqual(button_Confirm).whc_Right(8, toView: button_Confirm).whc_Width(50).whc_Height(30)
        
        addSubview(checkButton)
        checkButton.whc_Right(20,toView:button_Cancel).whc_TopEqual(button_Cancel).whc_HeightEqual(button_Cancel).whc_HeightWidthRatio(1)
        
        
        
        let checkLabel = UILabel()
        checkLabel.text = "下级"
        checkLabel.textColor = UIColor.black
        checkLabel.font = UIFont.systemFont(ofSize: 12)
        checkLabel.textAlignment = .center
        addSubview(checkLabel)
        checkLabel.whc_Right(5,toView:checkButton).whc_TopEqual(button_Cancel).whc_HeightEqual(checkButton)
        
//        if !(YiboPreference.getAccountMode() == AGENT_TYPE || YiboPreference.getAccountMode() == TOP_AGENT_TYPE ){
//            checkLabel.isHidden = true
//            checkButton.isHidden = true
//        }
        
        button_Confirm.setTitle("确认", for: .normal)
        button_Confirm.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button_Confirm.backgroundColor = UIColor.mainColor()
        button_Confirm.setTitleColor(UIColor.white, for: .normal)
        button_Confirm.layer.cornerRadius = 3
        button_Confirm.clipsToBounds = true
        button_Confirm.addTarget(self, action: #selector(button_ConfirmClickHandle), for: .touchUpInside)
        
        button_Cancel.setTitle("取消", for: .normal)
        button_Cancel.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button_Cancel.backgroundColor = UIColor.white
        button_Cancel.setTitleColor(UIColor.cc_136(), for: .normal)
        button_Cancel.layer.cornerRadius = 3
        button_Cancel.clipsToBounds = true
        button_Cancel.layer.borderWidth = 0.5
        button_Cancel.layer.borderColor = UIColor.lightGray.cgColor
        button_Cancel.addTarget(self, action: #selector(button_CancelClickHandle), for: .touchUpInside)
        
        checkButton.setImage(UIImage.init(named: "loginCheckbox_selected"), for: .selected)
        checkButton.setImage(UIImage.init(named: "loginCheckbox_normal"), for: .normal)
        checkButton.addTarget(self, action: #selector(checkButtonAction), for: .touchUpInside)
        
        
        button_Selecter1.titleLabel?.font = UIFont.systemFont(ofSize: 12)
    }
    
    func initializeDate(start:String,end:String){
        self.startTime = start
        self.endTime = end
        button_dateStart.setTitle(self.startTime, for: .normal)
        button_dateEnd.setTitle(self.endTime, for: .normal)
    }
    
    @objc private func checkButtonAction(sender:UIButton) {
        checkButton.isSelected = !sender.isSelected
        includeSwitch = checkButton.isSelected ? "1" : "0"
    }
    
    @objc private func button_SelectorClickHandle(button:UIButton) {
        if button.tag == 100 {
            let selectedView = SportsFilterSelectView(type: 1, viewTitle: "球类类型")
            selectedView.selectedIndex = selectIndex
            selectedView.didSelected = { [weak self, selectedView] (index, content) in
                
                self?.qiuduitype = content
                self?.button_Selecter1.setTitle(
                    self?.arrayStr[index], for: .normal)
            }
            
            window?.addSubview(selectedView) //高度根据内容row来判断？
            selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(300)
            selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.5, animations: {
                selectedView.transform = CGAffineTransform.identity
            }, completion: nil)
            
        }else{
            let selectedView = SportsFilterSelectView(type: 2, viewTitle: "投注状态类型")
            selectedView.selectedIndex = selectIndex
            selectedView.didSelected = { [weak self, selectedView] (index, content) in
                
                self?.touzhutype = content
                self?.button_Selecter.setTitle(
                self?.arrayStr1[index], for: .normal)
            }
            
            window?.addSubview(selectedView)
            selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(300)
            selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.5, animations: {
                selectedView.transform = CGAffineTransform.identity
            }, completion: nil)
        }
            
     
    }
    
    func initDate(){
        self.startTime = getTodayZeroTime()
        self.endTime = getTomorrowNowTime()
    }
    
    //MARK:打开时间选择
    
    func openDatePick(tag:Int)  {
        
        if tag == 101{
            self.start_Timer.canButtonReturnB = {
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
            }
            self.start_Timer.sucessReturnB = { returnValue in
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
                self.startTime = returnValue
                let task = delay(0.5){
                    self.button_dateStart.titleLabel?.lineBreakMode = .byTruncatingHead
                    self.button_dateStart.setTitle(returnValue, for: .normal)
                }
                //                cancel(task)
            }
            self.gototargetView(_targetView:self.start_Timer)
        }else if tag == 102{
            self.end_Timer.canButtonReturnB = {
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
            }
            self.end_Timer.sucessReturnB = { returnValue in
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
                self.endTime = returnValue
                let task = delay(0.5){
                    self.button_dateEnd.titleLabel?.lineBreakMode = .byTruncatingHead
                    self.button_dateEnd.setTitle(returnValue, for: .normal)
                }
            }
            self.gototargetView(_targetView:self.end_Timer)
        }
    }
    
    @objc private func button_StartTimeClickHandle() {
        openDatePick(tag: 101)
    }
    
    @objc private func button_EndTimeClickHandle() {
        openDatePick(tag: 102)
    }
    
    //MARK:打开底部弹出view
    
    func gototargetView(_targetView:UIView)  {
        controller.view.ttPresentFramePopupView(_targetView, animationType: TTFramePopupViewSlideBottomTop) {
            debugPrint("我要消失了")
        }
        _targetView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(controller.view)
            make.height.equalTo(250)
        }
    }
    
    //过滤view中点击确定时的响应方法
    @objc private func button_ConfirmClickHandle() {
        let username = textField_UserName.text != nil ? textField_UserName.text! : ""
        let zhuduiname = textField_UserName1.text != nil ? textField_UserName1.text! : ""
        let keduiname = textField_UserName2.text != nil ? textField_UserName2.text! : ""
        
//        let qiuduitype = "0"
//        let touzhutype = "0"
        
        didClickConfirmButton?(username,zhuduiname,keduiname, self.qiuduitype,self.touzhutype,self.startTime,self.endTime,includeSwitch)
        
        
//        用户名：code？：开始时间，结束时间：，是否下级？
        
        
        
        //数据上报
    }
    @objc private func button_CancelClickHandle() {
        didClickCancleButton?()
    }
    
    var didClickCancleButton: ( () -> Void)?
    var didClickConfirmButton: ( (String, String,String,String, String,String,String, String) -> Void)?
    
    
    
    var lotteryTypes: [AllLotteryTypesSubData]!//所有彩种信息
    
    var selectIndex: Int = 0
    var selectLotCode: String = ""
    
    override func didMoveToWindow() {
        
        button_Selecter.isEnabled = false
        if LennyModel.allLotteryTypesModel == nil {
            
            LennyNetworkRequest.obtainAllLotteryTypes(version: 0) { [weak self](model) in
                
                self?.button_Selecter.isEnabled = true
                self?.lotteryTypes = [AllLotteryTypesSubData]()
                
                for sub: AllLotteryTypesSubData in (model?.obtainAllLotteryWithIndex())! {
                    self?.lotteryTypes.append(sub)
                }
                if self?.lotteryTypes.count == 0 { return }
            }
        }else {
            self.lotteryTypes = [AllLotteryTypesSubData]()
            for sub: AllLotteryTypesSubData in (LennyModel.allLotteryTypesModel?.obtainAllLotteryWithIndex())! {
                self.lotteryTypes.append(sub)
            }
            
            button_Selecter.isEnabled = true
        }
    }
}

