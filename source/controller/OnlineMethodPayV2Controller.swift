//
//  OnlineMethodPayV2Controller.swift
//  gameplay
//
//  Created by admin on 2018/10/1.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class OnlineMethodPayV2Controller: BaseController,NewSelectViewDelegate {
    var introducePics = [String]()
    var jsonData: [String: Any]?
    var selectedIndex = 0
    
    let payMethodTag = 1001
    let payPathTag = 1002
    let fixedAmmoutTag = 1003
    
    var payPathEmptyTipsView = UIView()
    
    var payDesContents = "" {
        didSet {
            let htmlString = String.init(htmlEncodedString: payDesContents)
            payDesLabel.text = htmlString
        }
    }
    
    var onlines:[OnlineFunFirstPay] = [] {
        didSet {
            viewLogic.onlinesFuncFirst = onlines
            payPathCollection.reloadData()
            
            
            viewLogic.payMethodSelectedIndex = 0
            browerTipsLabel.text = viewLogic.getWarmTips(index: 0)
            fixedAmmounts = viewLogic.fixedAmmouts
            fixedAmountField.text = viewLogic.seletedFixedAmount
        }
    }
    
    var payPathDatas:[String] = [] {
        didSet {
            viewLogic.payPathDatas = payPathDatas
            viewLogic.payMethodSelectedIndex = 0
            
            if payPathDatas.count > 0 {
                let payPathName = payPathDatas[viewLogic.payMethodSelectedIndex]
                requestPayPath(path: payPathName)
            }
            
            payMethodCollection.reloadData()
            
//            viewLogic.payPathDatas = payPathDatas
//            payPathCollection.reloadData()
        }
    }
    
    var fixedAmmounts:[String] = [] {
        didSet {
            refreshFixedAmountCollectionH()
            chargeItemsCollection.reloadData()
        }
    }
    
    var meminfo:Meminfo?
    var viewLogic = OnlineNewPayInfoFuncFirstLogic()
    
    var payMethodCollectionH:CGFloat =  55 {
        didSet {
            payMethodCollectionConsH.constant = payMethodCollectionH
        }
    }
    
    var payPathCollectionH:CGFloat =  55 {
        didSet {
            payPathCollectionConsH.constant = payPathCollectionH
        }
    }
    
    var chargeItemsCollectionH:CGFloat =  55 {
        didSet {
            chargeItemsCollectionConsH.constant = chargeItemsCollectionH
        }
    }
    
    @IBOutlet weak var payMethodCollection: UICollectionView!
    @IBOutlet weak var payPathCollection: UICollectionView!
    @IBOutlet weak var chargeItemsCollection: UICollectionView!
    
    @IBOutlet weak var payDesLabelConsH: NSLayoutConstraint!
    @IBOutlet weak var payMethodCollectionConsH: NSLayoutConstraint!
    @IBOutlet weak var payPathCollectionConsH: NSLayoutConstraint!
    @IBOutlet weak var chargeItemsCollectionConsH: NSLayoutConstraint!
    
    @IBOutlet weak var payDesLabel: ASLabel!
    @IBOutlet weak var topDesLabel: UILabel!
    @IBOutlet weak var payMethodTips: UIView!
    @IBOutlet weak var payPathTips: UIView!
    @IBOutlet weak var chargeFieldBgView: UIView!
    
    @IBOutlet weak var payMethodExpandHandler: UIView!
    @IBOutlet weak var payPathExpandHandler: UIView!
    @IBOutlet weak var payMethodExpandImage: UIImageView!
    @IBOutlet weak var payPathExpandImage: UIImageView!
    @IBOutlet weak var warmTipsLabel: ASLabel!
    @IBOutlet weak var browerTipsLabel: ASLabel!
    @IBOutlet weak var fixedAmountField: CustomFeildText!
    @IBAction func commitAction(_ sender: Any) {
        commitRequest()
    }
    
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        
        let pointY:CGFloat = fixedAmountField.convert(CGPoint.init(x: fixedAmountField.x, y: fixedAmountField.y), to: self.view).y + 44
        
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
            if keyBoardNeedLayout {
                UIView.animate(withDuration: duration, delay: 0.0,
                               options: UIView.AnimationOptions(rawValue: curve),
                               animations: {
                                self.view.frame = CGRect.init(x:0,y:-abs(screenHeight - pointY - deltaY),width:self.view.bounds.width,height:self.view.bounds.height)
                                self.keyBoardNeedLayout = false
                                self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
            
            UIView.animate(withDuration: duration, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            self.view.frame = CGRect.init(x:0,y:deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.fixedAmountField.text = ""
        self.scrollViewTopConstraint.constant = CGFloat(KNavHeight)
    }
    
    //MARK: -LifeCircle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        
        setupUI()
        setupData()
        setupEvents()
    }
    
    //MARK: -UI,layout
    private func setupUI() {
        self.title = "在线支付"
        
        depoistGuidePictures()
        
        payMethodCollection.tag = payMethodTag
        payMethodCollection.delegate = self
        payMethodCollection.dataSource = self
        
        payPathCollection.tag = payPathTag
        payPathCollection.delegate = self
        payPathCollection.dataSource = self
        
        chargeItemsCollection.tag = fixedAmmoutTag
        chargeItemsCollection.delegate = self
        chargeItemsCollection.dataSource = self
        
        payMethodCollection.collectionViewLayout = OnIineNewPayInfoLogic.getPayMethodLayout()
        payPathCollection.collectionViewLayout = OnIineNewPayInfoLogic.getPayMethodLayout()
        
        payPathEmptyTipsView = OnIineNewPayInfoLogic.getEmptyTipsView(tips: "没有支付通道")
        payPathCollection.addSubview(payPathEmptyTipsView)
        payPathEmptyTipsView = payPathEmptyTipsView.whc_FrameEqual(payPathCollection)
        
        setupNoPictureAlphaBgView(view: payMethodCollection,alpha: 0.1)
        setupNoPictureAlphaBgView(view: payPathCollection,alpha: 0.1)
        setupNoPictureAlphaBgView(view: payMethodExpandHandler,alpha: 0.2)
        setupNoPictureAlphaBgView(view: payPathExpandHandler,alpha: 0.2)
        setupNoPictureAlphaBgView(view: payMethodTips,alpha: 0.5)
        setupNoPictureAlphaBgView(view: payPathTips,alpha: 0.5)
        setupNoPictureAlphaBgView(view: chargeFieldBgView,alpha: 0.5)
        
        setThemeLabelTextColorGlassWhiteOtherRed(label: warmTipsLabel)
        setThemeLabelTextColorGlassWhiteOtherRed(label: browerTipsLabel)
    }
    
    private func setupData() {
        
        requestPayMethod()
        
        fixedAmmounts = viewLogic.fixedAmmouts
        
        payDesContents = OnIineNewPayInfoLogic.getPayTips()
        
        browerTipsLabel.text = viewLogic.getWarmTips(index: 0)
        
        if let tips = OnIineNewPayInfoLogic.getBrowerTips() {
            if !isEmptyString(str: tips) {
                warmTipsLabel.text = "温馨提示\(tips)"
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        refreshPayMethodCollectionH()
        refreshPayPathCollectionH()
        refreshFixedAmountCollectionH()
    }
    
    //MARK: - 入款指南
    private func depoistGuidePictures() {
        requestDepositeGuidePictures(controller: self, bannerType: "8", success: {[weak self] (pictures) in
            if let weakSelf = self {
                if pictures.count > 0 {
                    weakSelf.introducePics = pictures
                    weakSelf.setupRightNavTitle(title: "存款指南")
                }
            }
        }) { (errorMsg) in
            
        }
    }
    
    func setupRightNavTitle(title:String) -> Void {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onRightMenuClick))
    }
    
    @objc func onRightMenuClick() -> Void {
        let pop = PagePicturePop.init(frame: .zero, urls: self.introducePics)
        pop.show()
    }
    
    private func refreshFixedAmountCollectionH() {
        
        if fixedAmmounts.count == 0
        {
            fixedAmountField.isUserInteractionEnabled = true
            chargeItemsCollectionH = OnIineNewPayInfoLogic.fixedAmountMiniH
        }else
        {
            fixedAmountField.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.chargeItemsCollectionH = self.chargeItemsCollection.contentSize.height
            }
        }
        
    }
    
    private func refreshPayMethodCollectionH() {
        if payPathDatas.count == 0
        {
            payMethodCollectionH = OnIineNewPayInfoLogic.payMethodMiniH
        }else
        {
            payMethodCollectionH = viewLogic.payMethodExpandIsUp ? payMethodCollection.contentSize.height : OnIineNewPayInfoLogic.payMethodMiniH
        }
    }
    
    private func refreshPayPathCollectionH() {
        payPathEmptyTipsView.isHidden = payPathDatas.count != 0
        
        if payPathDatas.count == 0
        {
            payPathCollectionH = OnIineNewPayInfoLogic.payMethodMiniH
        }else
        {
            payPathCollectionH = viewLogic.payPathExpandIsUp ? payPathCollection.contentSize.height : OnIineNewPayInfoLogic.payMethodMiniH
        }
        
    }
    
    //MARK: -Events
    private func setupEvents() {
        let gestureMethod = UITapGestureRecognizer.init(target: self, action: #selector(payMethodExpandClick))
        self.payMethodExpandHandler.addGestureRecognizer(gestureMethod)
        
        let gesturePath = UITapGestureRecognizer.init(target: self, action: #selector(payPathExpandClick))
        self.payPathExpandHandler.addGestureRecognizer(gesturePath)
    }
    
    @objc private func payMethodExpandClick() {
        viewLogic.payMethodExpandIsUp = !viewLogic.payMethodExpandIsUp
        payMethodExpandImage.image = UIImage.init(named: viewLogic.payMethodExpandIsUp ? "payUpArrow_black" : "payDownArrow_black")
        refreshPayMethodCollectionH()
    }
    
    @objc private func payPathExpandClick() {
        viewLogic.payPathExpandIsUp = !viewLogic.payPathExpandIsUp
        payPathExpandImage.image = UIImage.init(named: viewLogic.payPathExpandIsUp ? "payUpArrow_black" : "payDownArrow_black")
        refreshPayPathCollectionH()
    }
    
}

//MARK: - API
extension OnlineMethodPayV2Controller {
    
    //MARK: -网络请求
    /** 获取支付方法 */
    func requestPayMethod() {
        let params = ["payCode":"online"] as [String:AnyObject]
        request(frontDialog: true, url: URL_ONLINE_PAY_METHOD,params:params) { (resultJson, success) in
            if !success {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取支付方法失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if resultJson == "{}"{
                showToast(view: self.view, txt: "没有支付方法")
                return
            }
            
            if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                loginWhenSessionInvalid(controller: self)
                return
            }
            
            do {
                if let data = resultJson.data(using: String.Encoding.utf8) {
                    if let datas = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, Any> {
                        if let onlineList = datas["onlineList"] as? [String] {
                            self.payPathDatas = onlineList
                        }
                    }
                }
            }catch let error {
                showToast(view: self.view, txt: error.localizedDescription)
            }
            
            print("支付方法:",resultJson)
        }
    }
    
    /** 获取支付通道 */
    func requestPayPath(path:String) {
        let params = ["payType":path]
        request(frontDialog: true, url: URL_ONLINE_PAY_PATH,params:params) { (resultJson, success) in
            if !success {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取支付方法失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if resultJson == "{}"{
                showToast(view: self.view, txt: "没有支付方法")
                return
            }
            
            if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                loginWhenSessionInvalid(controller: self)
                return
            }
            
            if let result = OnlineFunFirstPayWraper.deserialize(from: resultJson) {
                self.onlines = result.onlineList
            }
            
            print("支付通道:",resultJson)
        }
    }
    
    func syncSysPayMethod(payId:Int){
        request(frontDialog: true,method: .get,loadTextStr: "正在同步中...",url:SYNC_SHOUYINGTAI_LIST,params: ["payId":payId],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        showToast(view: self.view, txt: convertString(string: "同步失败"))
                        return
                    }
                    if isEmptyString(str: resultJson){
                        showToast(view: self.view, txt: "同步失败")
                        return
                    }
                    if resultJson == "{}"{
                        showToast(view: self.view, txt: "没有支付方式")
                        return
                    }
                    
                    if resultJson.contains("登录") && YiboPreference.getLoginStatus() == false{
                        loginWhenSessionInvalid(controller: self)
                        return
                    }
                    
                    do {
                        let data = resultJson.data(using: String.Encoding.utf8)
                        let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String]
                        
                        self.payPathDatas = json
                    }catch let error {
                        print("convert: error \(error)")
                    }
        })
    }
    
    private func commitRequest() {
        
        if self.onlines.isEmpty{
            showToast(view: self.view, txt: "没有支付方式，无法发起支付")
            return
        }
        let money = fixedAmountField.text!
        if isEmptyString(str: money){
            showToast(view: self.view, txt: "请输入充值金额")
            return
        }
        //最小充值的金额
        let minFee = self.onlines[viewLogic.payMethodSelectedIndex].min
        //最大充值的金额
        let maxFee = self.onlines[viewLogic.payMethodSelectedIndex].max
        
        guard let mmoney = Float(money) else {
            showToast(view: self.view, txt: "金额格式不正确,请重新输入")
            return
        }
        if mmoney == 0 {
            showToast(view: self.view, txt: "充值金额不能为0")
            return
        }
        
        if mmoney < Float(minFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能小于%d元", minFee))
            return
        }

        if mmoney > Float(maxFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能大于%d元", maxFee))
            return
        }
        
        if payPathDatas.isEmpty{
            showToast(view: self.view, txt: "没有收银台数据")
            return
        }
        
        let payId = self.onlines[viewLogic.payMethodSelectedIndex].id
//        let bankCode = payPathDatas[viewLogic.payPathSelectedIdnex]
        let bankCode = self.onlines[viewLogic.payMethodSelectedIndex].payType
//        print("the onlines = ",self.onlines)
//        print("the banks = ",self.payPathDatas)
        
        if payId == 0{
            showToast(view: self.view, txt: "请选择支付通道!")
            return
        }
        
        if isEmptyString(str: bankCode){
            showToast(view: self.view, txt: "请选择支付方式!")
            return
        }
        var paramters:[String:AnyObject] = [:]
        paramters["payId"] = payId as AnyObject
        paramters["amount"] = money as AnyObject
        paramters["bankCode"] = bankCode as AnyObject
        print("bankcode ",bankCode)
        print(paramters)
        
        request(frontDialog: true,method: .post,loadTextStr: "正在提交中...",url:ONLINE_PAY_URL,
                params: paramters,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    if !resultStatus {
                        showToast(view: self.view, txt: convertString(string: "提交失败"))
                        return
                    }
                    if isEmptyString(str: resultJson){
                        showToast(view: self.view, txt: "提交失败")
                        return
                    }
                    
                    guard let data = resultJson.data(using: String.Encoding.utf8, allowLossyConversion: true) else {
                        return
                    }
                    guard let json = try? JSONSerialization.jsonObject(with: data,options:.allowFragments) as! [String: Any] else{
                        return
                    }
                    
                    
                    if (json.keys.contains("success")){
                        if !(json["success"] as! Bool){
                            let msg = json["msg"] as! String
                            showToast(view: self.view, txt: json["msg"] as! String)
                            if msg.contains("其他地方"){
                                loginWhenSessionInvalid(controller: self)
                            }
                            return
                        }
                        //判断是否弹窗
                        let browerType = YiboPreference.getDefault_brower_type()
                        if browerType == -1 && shouldShowBrowsersChooseView(){
                            self.showBrowerSelectedView(json: json)
                        }else if browerType != -1 && shouldShowBrowsersChooseView(){
                            self.handleData(json: json,browerType:browerType)
                        }else {
                            self.handleData(json: json,browerType:BROWER_TYPE_SAFARI)
                        }
                    }
        })
    }
    
    //MARK: 弹窗显示浏览器选择
    private func showBrowerSelectedView(json: [String: Any]) {
        let tupeArray = [("Browser_safari","Safari浏览器"),("Browser_uc","UC浏览器"),
                         ("Browser_qq","QQ浏览器"),("Browser_google","谷歌浏览器"),
                         ("Browser_firefox","火狐浏览器")]
        let selectedView = NewSelectView(dataSource: tupeArray, viewTitle: "请选择浏览器")
        selectedView.delegate = self
        selectedView.selectedIndex = selectedIndex
        self.jsonData = json
        
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.selectedIndex = index
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
    }
    
    func gotoBrowerWithType(type: Int, allways: Bool) {
        if let json = self.jsonData {
            if allways {
                YiboPreference.setDefault_brower_type(value: "\(type)")
            }
            self.handleData(json: json,browerType:type)
        }
    }
    
    private func handleData(json: [String: Any],browerType: Int) {
        if let type = json["returnType"]{
            let returnType = type as! String
            if returnType == "qrcodeUrl"{
                let formActionStr = BASE_URL + PORT + "/onlinePay/qrcodeRedirect4App.do"
                var formParams:Dictionary<String,Any> = [:]
                formParams["rechargeSubmitFormData"] = json["url"] as! String
                formParams["rechargeSubmitOrderId"] = json["orderId"] as! String
                formParams["rechargeSubmitPayName"] = json["payName"] as! String
                formParams["rechargeSubmitPayType"] = json["payType"] as! String
                formParams["rechargeSubmitOrderTime"] = json["orderTime"] as! String
                formParams["rechargeSubmitPayAmount"] = json["payAmount"] as! String
                formParams["rechargeSubmitPayFlag"] = json["flag"] as! String
                let params = getJSONStringFromDictionary(dictionary: formParams as NSDictionary)
                guard var escapedString = params.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {return}
                //将特殊字符替换成转义后的编码
                escapedString = escapedString.replacingOccurrences(of: "=", with: "%3D")
                escapedString = escapedString.replacingOccurrences(of: "&", with: "%26")
                escapedString =  escapedString.replacingOccurrences(of: "+", with: "%2b")
                let url = String.init(format: "%@%@%@?returnType=%@&url=%@&data=%@", BASE_URL,PORT,PAY_RESULT_SAFARI,returnType,formActionStr,escapedString)
                print("the open url = ",url)
                openInBrowser(url: url,browerType:browerType,view: self.view)
                
            }else if returnType == "postSubmit"{
                let formActionStr = json["url"] as! String
                let formParams = json["dataMap"] as! [String:Any]
                let params = getJSONStringFromDictionary(dictionary: formParams as NSDictionary)
                guard var escapedString = params.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {return}
                //将特殊字符替换成转义后的编码
                escapedString = escapedString.replacingOccurrences(of: "=", with: "%3D")
                escapedString = escapedString.replacingOccurrences(of: "&", with: "%26")
                escapedString =  escapedString.replacingOccurrences(of: "+", with: "%2b")
                let url = String.init(format: "%@%@%@?returnType=%@&url=%@&data=%@", BASE_URL,PORT,PAY_RESULT_SAFARI,returnType,formActionStr,escapedString)
                print("the open url = ",url)
                openInBrowser(url: url,browerType:browerType,view: self.view)
            }else if returnType == "href"{
                let formActionStr = json["url"] as! String
                openBrower(urlString: formActionStr)
            }else if returnType == "write"{
                let formActionStr = json["url"] as! String
                if formActionStr.starts(with: "<!DOCTYPE") || formActionStr.starts(with: "<!doctype") {
                    openActiveDetail(controller: self, title: "支付", content: formActionStr)
                }else if formActionStr.contains("window.location.href=\'") {
                    //截取链接开始到最后一位
                    let components = "window.location.href=\'"
                    let startLocation_1 = formActionStr.positionOf(sub: components)
                    let results_1 = formActionStr.subString(start: startLocation_1 + components.length, length: formActionStr.length - startLocation_1 - components.length)
                    // 截取最终链接
                    let startLocation_2 = results_1.positionOf(sub: "\';")
                    let results_2 = results_1.substrfromBegin(length: startLocation_2)
                    openBrower(urlString: results_2)
                    
                }else{
                    openBrower(urlString: formActionStr)
                }
            }else{
                let formActionStr = json["url"] as! String
                let url = String.init(format: "%@%@%@?returnType=%@&url=%@&data=%@", BASE_URL,PORT,PAY_RESULT_SAFARI,returnType,formActionStr,"")
                print("the open url = ",url)
                openInBrowser(url: url,browerType:browerType,view: self.view)
            }
        }else{
            if let url = json["url"] as? String {
                openInBrowser(url: url,browerType:browerType,view: self.view)
            }else {
                showToast(view: self.view, txt: "没有跳转链接，无法支付",afterDelay:2)
            }
            
        }
    }
}

//MARK: - <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
extension OnlineMethodPayV2Controller:UICollectionViewDelegate,UICollectionViewDataSource
    ,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == payMethodTag
        {
            let name = payPathDatas[indexPath.row]
            viewLogic.payPathSelectedIdnex = indexPath.row
            requestPayPath(path: name)
        }
        else if collectionView.tag == payPathTag
        {
//            let model = onlines[indexPath.row]
            viewLogic.payMethodSelectedIndex = indexPath.row
//            syncSysPayMethod(payId: model.id)
            
            browerTipsLabel.text = viewLogic.getWarmTips(index: indexPath.row)
            
            fixedAmmounts = viewLogic.fixedAmmouts
            fixedAmountField.text = viewLogic.seletedFixedAmount
        }
        else if collectionView.tag == fixedAmmoutTag
        {
            viewLogic.fixedAmmoutSelectedIndex = indexPath.row
            fixedAmountField.text = viewLogic.seletedFixedAmount
        }
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onlineNewPayInfoMethodCell", for: indexPath) as! OnlineNewPayInfoMethodCell
        
        if collectionView.tag == payMethodTag
        {
//            let model = onlines[indexPath.row]
//            let title = !isEmptyString(str: model.payAlias) ? model.payAlias : model.payName
//            cell.configWithTitle(title: title,isSelected: viewLogic.payMethodSelectedIndex == indexPath.row)
            
            let payPathName = payPathDatas[indexPath.row]
            cell.configWithImage(payName: payPathName,isSelected: viewLogic.payPathSelectedIdnex == indexPath.row,version:"V1")
        }
        else if collectionView.tag == payPathTag
        {
//            let payPathName = payPathDatas[indexPath.row]
//            cell.configWithImage(payName: payPathName,isSelected: viewLogic.payPathSelectedIdnex == indexPath.row)
            
            let model = onlines[indexPath.row]
            let title = !isEmptyString(str: model.payAlias) ? model.payAlias : model.payName
            cell.configWithTitle(title: title,isSelected: viewLogic.payMethodSelectedIndex == indexPath.row)
        }
        else if collectionView.tag == fixedAmmoutTag
        {
            let fixedAmmount = fixedAmmounts[indexPath.row]
            cell.configWithTitle(title: fixedAmmount, isSelected: viewLogic.fixedAmmoutSelectedIndex == indexPath.row)
        }
//        else
//        {
//            cell.configWithImage(payName: "",isSelected: true)
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == payMethodTag
        {
            return payPathDatas.count
        }else if collectionView.tag == payPathTag{
            return onlines.count
        }
        else if collectionView.tag == fixedAmmoutTag {
            return fixedAmmounts.count
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == payMethodTag
        {
            return viewLogic.sizeForPayPathCollection(index: indexPath)
        }else if  collectionView.tag == payPathTag
        {
            return viewLogic.sizeForPayMethodCollection(index: indexPath)
        }else if collectionView.tag == fixedAmmoutTag {
            return viewLogic.sizeForFixedAmountColletion(index: indexPath)
        } else
        {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return OnIineNewPayInfoLogic.cellSectionpadding
    }
}
