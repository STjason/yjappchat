import UIKit
//bin html content show page
class BBinWebContrllerViewController: BaseController,UIWebViewDelegate {
    var httpis = false
    var htmlContent = ""
    var playCode = ""
    var isHideNavBar:Bool = false
    //获取 AppDelegate 对象
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    @IBOutlet weak var webView:UIWebView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //该页面显示时可以横竖屏切换
        if playCode == "bbin" {
            appDelegate.interfaceOrientations = .allButUpsideDown
        }
        if isHideNavBar {
            navigationController?.isNavigationBarHidden = isHideNavBar
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if playCode == "bbin" {
            //页面退出时还原强制横屏状态
            appDelegate.interfaceOrientations = .portrait
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.scalesPageToFit = true
        if httpis {
            saveCookies()
            var urlString = htmlContent.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            if urlString.length == 0{
                return
            }
            if !htmlContent.contains("http"){
                urlString = String.init(format:"%@%@",BASE_URL,urlString)
            }
            guard let activeUrl = URL(string: urlString) else{return}
            webView.loadRequest(URLRequest.init(url: activeUrl))
            return
        }
        webView.loadHTMLString(htmlContent, baseURL: URL.init(string: BASE_URL))
    }
}

extension BBinWebContrllerViewController{
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        showDialog(view: self.view, loadText: "正在载入...")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
//        let js = "javascript:(function() {" +
//            "document.getElementsByClassName(\"bar bar-nav\")[0].style.display=\'none\';" + "" +
//            "document.getElementsByClassName(\"content\")[0].style.marginTop=\'-50px\';" + "" +
//        "})()";
//        webView.stringByEvaluatingJavaScript(from: js)
        hideDialog()
    }
    
}

extension BBinWebContrllerViewController{
    
    func saveCookies(){
        // 同步cookie
        let DomainUrl = getDomainUrl()
        let localhostComponents = DomainUrl.components(separatedBy: "//")
        var localhost = "localhost"
        if localhostComponents.count >= 2 {
            localhost = localhostComponents[1]
        }
        
        var fromAppDict:[HTTPCookiePropertyKey:Any] =  [HTTPCookiePropertyKey:Any]()
        fromAppDict[HTTPCookiePropertyKey.name] = "SESSION"
        fromAppDict[HTTPCookiePropertyKey.value] = YiboPreference.getToken()
        fromAppDict[HTTPCookiePropertyKey.domain] = localhost
        fromAppDict[HTTPCookiePropertyKey.originURL] = DomainUrl
        fromAppDict[HTTPCookiePropertyKey.path] = "/"
        fromAppDict[HTTPCookiePropertyKey.version] = "0"
        fromAppDict[HTTPCookiePropertyKey.expires] = Date(timeIntervalSinceNow: 60 * 60)
        fromAppDict[HTTPCookiePropertyKey.discard] = 0
        let cookie = HTTPCookie(properties: fromAppDict)
        HTTPCookieStorage.shared.setCookie(cookie!)
        
    }
   

}
