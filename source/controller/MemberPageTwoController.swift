//
//  MemberPageTwoController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/5.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class MemberPageTwoController: BaseMainController ,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var headerBgView: UIView!
    @IBOutlet weak var collectionHConstraint: NSLayoutConstraint!
    
    var sortedSectionArray:[String] = []
    var datas:[[MemberBean]] = []
    
    var bgImageView = UIImageView()
    
    /** 新版头部试图 带会员等级进度条 */
    var _userHeaderView : UserHeaderView?
    
    var messageNumber:Int = 0
    let sectionHeaderH:CGFloat = 44
    let cellH:CGFloat = 100
   
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        if  /*glt_iphoneX*/UIScreen.main.bounds.height >= 812.0  {
            let themeView = UIView.init()
            self.view.addSubview(themeView)
            themeView.whc_Left(0).whc_Right(0).whc_Top(0).whc_Height(CGFloat(KNavHeight) - kStatusHeight)
            themeView.theme_backgroundColor = "Global.themeColor"
        }
        
        setupthemeBgView(view: self.view, alpha: 0)
        
        headerBgView.addSubview(self.userHeaderView())
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(MemberCell.self, forCellWithReuseIdentifier:"memberCell")
        collectionView.showsVerticalScrollIndicator = false
        
        collectionView.register(MemberPageTwoHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "memberPageTwoHeader")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        self.loadMemberDatas()
        
        showDragBtnHandler?(false,self)
    }
    override func viewDidDisappear(_ animated: Bool) {
        showDragBtnHandler?(true,self)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.isNavigationBarHidden = true
        userMessage()
        var popDatas:[ReceiveMessageModelContentRow] = [ReceiveMessageModelContentRow]()
        LennyNetworkRequest.obtainMessageReceiveList { (model) in
            if model?.code == 0 && !(model?.success)!{
                loginWhenSessionInvalid(controller: self)
                return
            }
            for item in (model?.content?.rows) ?? []{
                if item.popStatus == 2{
                    //有弹窗
                    popDatas.append(item)
                }
            }
            if popDatas.count > 0  {
                if YiboPreference.getInsideStation(){
                    let webListView = WebviewList.init(noticeResuls: nil, inSideDatas: popDatas, urlContent: false, dialogTitle: "站内信", type: 4)
                    webListView.show()
                }
            }
        }
        /// 每次进入刷新会员数据
        self.accountWeb()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func accountWeb() -> Void {
        //帐户相关信息
        request(frontDialog: false, url:MEMINFO_URL,
                           callback: {(resultJson:String,resultStatus:Bool)->Void in
            if !resultStatus {
                return
            }
            if let result = MemInfoWraper.deserialize(from: resultJson){
                if result.success{
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    if let memInfo = result.content{
                        //更新帐户名，余额等信息
                        self.userHeaderView().refreshUserData(userData: memInfo)
                    }
                }
            }
        })
        
        if getSystemConfigFromJson()?.content.switch_level_show != "on"  {
            return
        }
        // 会员信息
        kRequest(isContent:true, frontDialog: false, url: GET_LEVEL_INFO) { (content) in
            let model = VipLevelMoel().getCurrentJson(dic: content as? NSDictionary ?? NSDictionary())
            self.userHeaderView().refreshVipData(vipData: model)
        }
    }
    
    private func claculateCollectionH() {
        let allHeaderH = (CGFloat)(datas.count) * sectionHeaderH
        var collectionH:CGFloat = 0
        for (_, values) in datas.enumerated() {
            let a = values.count / 3
            let b = values.count % 3
            if b == 0 {
                collectionH +=  cellH * CGFloat(a)
            }else {
                collectionH += cellH * CGFloat(a + 1)
            }
        }
        
        collectionH += allHeaderH
        collectionHConstraint.constant = collectionH
    }
    
    /** 获取未读消息数量 */
    func userMessage(){
        request(frontDialog: false, method: .get, loadTextStr: "", url: "/native/unread_msg_count_v3.do", params: [:]) { (resultJson:String, resultStatus:Bool) in
            if resultStatus{
                if let model = CenterUserMessageNumber.deserialize(from: resultJson) {
                    if model.success {
                        if model.content?.count != nil {
                            self.messageNumber = (model.content?.count)!
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - 分别抽出各个类别
    func loadMemberDatas() {
        
        if let sys = getSystemConfigFromJson() {
            if sys.content != nil{
                do {
                    if let pathValue = Bundle.main.path(forResource: "member_data", ofType: "json") {
                        let jsonData = NSData.init(contentsOfFile: pathValue)
                        let json = try JSONSerialization.jsonObject(with: jsonData! as Data, options: []) as! [AnyObject]
                        
                        self.datas.removeAll()
                        let itemsArray = getArraysOfMemberpage()
                        for (_,value) in itemsArray.enumerated() {
                            self.loadDataCategoryInfo(type: value, datasP: json, content: sys.content!)
                        }
                        self.claculateCollectionH()
                        self.collectionView.reloadData()
                    }
                    
                }catch let error as NSError{
                    print("error is \(error.localizedDescription)")
                }
            }
        }
        
    }
    
    func loadDataCategoryInfo(type:String, datasP: [AnyObject],content: SystemConfig) {

        var items:[MemberBean] = []
        switch type {
        case "1" :
            items = loadDataMyInfo(content: content, datas: datasP)
        case "2" :
            items = loadDataExchangeReport(content: content, datas: datasP)
        case "3" :
            items = loadDataMyReport(content: content, datas: datasP)
        case "4" :
            items = loadDataMyNote(content: content, datas: datasP)
        case "5" :
            items = loadDataWebsite(content: content, datas: datasP)
        case "6" :
            items = loadDataMySetting(content: content, datas: datasP)
        case "7" :
            items = loadDataOthers(content: content, datas: datasP)
        default:
            print("-+-+")
        }
        
        if items.count != 0 {
            sortedSectionArray.append(type)
            datas.append(items)
        }
        
    }
    
    private func shouldContainItems(txtName:String) -> Bool {
        let lotteryFitter = ["彩票注单","开奖结果","走势图","彩种信息","彩种限额","彩种信息"]
        if lotteryFitter.contains(txtName) {
            if !getSwitch_lottery() {
                return true
            }
        }
        
        return false
    }
    
    //1我的信息/功能
    func loadDataMyInfo(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
        
        var dataFilter:[String] = []
        
        /// 处理试玩账号进入有提示阻挡需求 没有配置文件的情况下做判断处理 JK add 19-09-17
        if YiboPreference.getAccountMode() == AGENT_TYPE || YiboPreference.getAccountMode() == TOP_AGENT_TYPE{
            dataFilter = ["我的推荐","推广链接","注册管理","个人总览","团队总览","用户列表","用户资料","周周转运","每日加奖","充值卡","充值卡记录","代金券","优惠活动大厅"]
        }else {
            dataFilter = ["我的推荐","推广链接","注册管理","个人总览","团队总览","用户资料","周周转运","每日加奖","充值卡","充值卡记录","代金券","优惠活动大厅"]
        }
        
        var items:[MemberBean] = []
        
        for item in datas {
            
            let iconName = item["icon_name"] as! String
            let txtName = item["txt_name"] as! String
            let result = MemberBean()
            result.iconName = iconName
            result.txtName = txtName
            
            if !dataFilter.contains(txtName) {
                continue
            }
            
            if !memberTypeCheck(itemName: txtName) {
                continue
            }
            
            if shouldContainItems(txtName: txtName) {continue}
            
            //我的推荐在动态赔率开关打开时，不显示
            if txtName == "我的推荐" {
                let lottery_dynamic_odds = content.lottery_dynamic_odds
                let same_rateback_when_dynamic = content.fixed_rate_model

                if lottery_dynamic_odds == "on" || (lottery_dynamic_odds != "on" && same_rateback_when_dynamic != "on"){
                    continue
                }
            }
            
            if txtName == "推广链接" {
                let lottery_dynamic_odds = content.lottery_dynamic_odds
                let same_rateback_when_dynamic = content.fixed_rate_model
                if (lottery_dynamic_odds != "on" && same_rateback_when_dynamic == "on"){
                    continue
                }
            }
            
            if txtName == "注册管理" {
                let fix_mode = content.fixed_rate_model
                let cp_dynamic_switch = content.lottery_dynamic_odds
                //固定模式，且下级返点一致时，不永旭设定返点
                if fix_mode == "on" && cp_dynamic_switch != "on"{
                    continue
                }
            }
            
            if txtName == "团队总览" {
                let fix_mode = content.fixed_rate_model //此项开启
                let cp_dynamic_switch = content.lottery_dynamic_odds //此项关闭
                if (fix_mode == "on" && cp_dynamic_switch == "off") && stationCode() != v023_ID {
                    continue
                }
            }
            
            if txtName == "充值卡" || txtName == "充值卡记录"{
                let topupcard_onoff = content.recharge_card_onoff
                if isEmptyString(str: topupcard_onoff) || topupcard_onoff == "off"{
                    continue
                }
            }
            if txtName == "代金券"{
                let coupon_onoff = content.coupons_onoff
                if isEmptyString(str: coupon_onoff) || coupon_onoff == "off"{
                    continue
                }
            }
            
            if (item["txt_name"] as? String)  == "每日加奖"{
                //每日加奖开关
                let bonusOnOff = content.one_bonus_onoff
                if isEmptyString(str: bonusOnOff) || bonusOnOff == "off" {
                    continue
                }
            }
            
            if (item["txt_name"] as? String) == "周周转运"{
                //周周转运开开
                let weekLuclyOnOff = content.week_deficit_onoff
                if isEmptyString(str: weekLuclyOnOff) || weekLuclyOnOff == "off" {
                    continue
                }
            }
            
            if txtName == "优惠活动大厅"{
                if content.onoff_application_active == "off"{
                    continue
                }
            }
            
            items.append(result)
        }
        
        return items
    }
    
    //2兑换/记录
    func loadDataExchangeReport(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
        
        let dataFilter:[String] = ["额度转换","额度转换记录","积分兑换","打码量变动记录","充提记录","开奖结果","走势图"]
        var items:[MemberBean] = []
        for item in datas {
            let iconName = item["icon_name"] as! String
            let txtName = item["txt_name"] as! String
            let result = MemberBean()
            result.iconName = iconName
            result.txtName = txtName
            
            if !dataFilter.contains(txtName) {
                continue
            }
            
            if !memberTypeCheck(itemName: txtName) {
                continue
            }
            
            if shouldContainItems(txtName: txtName) {continue}
            
            //额度转换开关没开时，不显示
            if txtName == "额度转换" || txtName == "额度转换记录" {
                let switch_money_change = content.switch_money_change
                if isEmptyString(str: switch_money_change) || switch_money_change == "off"{
                    continue
                }
            }
            
            //积分兑换开关没开时，不显示
            if txtName == "积分兑换" {
                let switch_money_change = content.exchange_score
                if isEmptyString(str: switch_money_change) || switch_money_change == "off"{
                    continue
                }
            }
            
            items.append(result)
            
        }
    
        return items
    }

    //3我的报表
    func loadDataMyReport(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
   
        let dataFilter:[String] = ["个人报表","团队报表","账变报表"]
        var items:[MemberBean] = []
        for item in datas {
            let iconName = item["icon_name"] as! String
            let txtName = item["txt_name"] as! String
            let result = MemberBean()
            result.iconName = iconName
            result.txtName = txtName
            
            if !dataFilter.contains(txtName) {
                continue
            }
            
            if shouldContainItems(txtName: txtName) {continue}
            
            if !memberTypeCheck(itemName: txtName) {
                continue
            }
            
            items.append(result)
        }
        
        return items
    }
    
    //4我的注单
    func loadDataMyNote(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
        let dataFilter:[String] = ["彩票注单","追号查询","真人注单","电子注单","PT电子注单","棋牌注单","体育注单","电竞注单","捕鱼注单"]
        var items:[MemberBean] = []
        for item in datas {
            let iconName = item["icon_name"] as! String
            let txtName = item["txt_name"] as! String
            let result = MemberBean()
            result.iconName = iconName
            result.txtName = txtName
            
            if !dataFilter.contains(txtName) {
                continue
            }
            
            if !memberTypeCheck(itemName: txtName) {
                continue
            }
            
            if shouldContainItems(txtName: txtName) {continue}
            
            if txtName == "追号查询" {
                let show_lottrack_menu = content.show_lottrack_menu
                if !isEmptyString(str: show_lottrack_menu) && show_lottrack_menu == "on" || !getSwitch_lottery() {
                    continue
                }
            }
            
            if txtName == "真人注单" {
                let onoff_zhen_ren_yu_le = content.onoff_zhen_ren_yu_le
                if isEmptyString(str: onoff_zhen_ren_yu_le) || onoff_zhen_ren_yu_le == "off"{
                    continue
                }
            }
            
            if txtName == "棋牌注单"{
                let onoff_chess = content.switch_chess
                if isEmptyString(str: onoff_chess) || onoff_chess == "off"{
                    continue
                }
            }
            
            if txtName == "电子注单" {
                let onoff_dian_zi_you_yi = content.onoff_dian_zi_you_yi
                if isEmptyString(str: onoff_dian_zi_you_yi) || onoff_dian_zi_you_yi == "off"{
                    continue
                }
            }
            
            if txtName == "PT电子注单" {
                let switch_pt_egame = content.switch_pt_egame
                let onoff_dian_zi_you_yi = content.onoff_dian_zi_you_yi
                if isEmptyString(str: switch_pt_egame) || switch_pt_egame == "off" || onoff_dian_zi_you_yi == "off" {
                    continue
                }
            }
            
            if txtName == "体育注单"
            {
                if content.onoff_sport_switch == "off"
                {
                    continue
                }
            }
            if txtName == "电竞注单" {
                let dianjing = content.switch_esport
                if isEmptyString(str: dianjing) || dianjing == "off" {
                    continue
                }
            }
            if txtName == "捕鱼注单" {
                let buyu = content.switch_fishing
                if isEmptyString(str: buyu) || buyu == "off" {
                    continue
                }
            }
            items.append(result)
        }
        
        return items
    }

    //5网站资料
    func loadDataWebsite(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
        let dataFilter:[String] = ["彩种信息","彩种限额","站内短信","优惠活动","网站公告","帮助中心"]
        var items:[MemberBean] = []
        for item in datas {
            
           
            
            let iconName = item["icon_name"] as! String
            let txtName = item["txt_name"] as! String
            let result = MemberBean()
            result.iconName = iconName
            result.txtName = txtName
            
            if !dataFilter.contains(txtName) {
                continue
            }
            
            if shouldContainItems(txtName: txtName) {continue}
            
            if !memberTypeCheck(itemName: txtName) {
                continue
            }
            
         
            
            items.append(result)
        }
        
        return items
    }

    //6我的设置
    func loadDataMySetting(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
        let dataFilter:[String] = ["银行卡","密码修改","建议反馈"]
        var items:[MemberBean] = []
        for item in datas {
            let iconName = item["icon_name"] as! String
            let txtName = item["txt_name"] as! String
            let result = MemberBean()
            result.iconName = iconName
            result.txtName = txtName
            
            if !dataFilter.contains(txtName) {
                continue
            }
            
            if !memberTypeCheck(itemName: txtName) {
                continue
            }
            
            if shouldContainItems(txtName: txtName) {continue}
            
            items.append(result)
        }
        
        return items
    }

    //7其他
    func loadDataOthers(content:SystemConfig, datas: [AnyObject]) -> [MemberBean] {
        let items:[MemberBean] = []
        
        return items
    }
    
    //MARK: 会员或试玩账号时，不显示
    private func memberTypeCheck(itemName: String) -> Bool {
        let dailiFilter:[String] = ["团队报表","团队总览","代理管理","推广链接","我的推荐","注册管理"]
        if (YiboPreference.getAccountMode() == MEMBER_TYPE || YiboPreference.getAccountMode() == GUEST_TYPE) {
            if dailiFilter.contains(itemName){
                if !(itemName == "团队总览" && stationCode() != v023_ID) {
                    return false
                }
            }
        }
        
        return true
    }

    //返回多少个组
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datas.count
    }
    
    //创建头视图
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var sectionTitle = ""
        if kind == UICollectionView.elementKindSectionHeader{
//            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
            
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "memberPageTwoHeader", for: indexPath) as! MemberPageTwoHeader
            //添加头部视图
            let type = sortedSectionArray[indexPath.section]
            
            switch type {
            case "1":
                sectionTitle = "我的信息/功能"
            case "2":
                sectionTitle = "兑换/记录"
            case "3":
                sectionTitle = "我的报表"
            case "4":
                sectionTitle = "我的注单"
            case "5":
                sectionTitle = "网站资料"
            case "6":
                sectionTitle = "我的设置"
            case "7":
                sectionTitle = "其他"
            default:
                print("index 错误")
            }
            
            header.configTitle(title: sectionTitle)
            return header
        }
        return UICollectionReusableView()
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: kScreenWidth, height: 44)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (kScreenWidth - 2.5) / 3, height: 100)
    }
    
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if datas.count > 0 {
            return datas[section].count
        }else {
            return 0
        }
    }
    //返回自定义的cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberCell", for: indexPath) as! MemberCell
        cell.tag = indexPath.row
        
        if indexPath.row + 1 > self.datas[indexPath.section].count {
            return cell
        }
        
        let model = self.datas[indexPath.section][indexPath.row]
        
        cell.setupData(data: model)
        
        cell.messageNumber.text = String(self.messageNumber)
        if model.txtName == "站内短信" {
            cell.messageNumber.isHidden = false
            cell.messageNumber.isHidden = self.messageNumber == 0
        }else{
            cell.messageNumber.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row + 1 > self.datas[indexPath.section].count {
            collectionView.reloadData()
            return
        }
        
        let data = self.datas[indexPath.section][indexPath.row]
        self.stackViewButtonsClickHandle(name: data.txtName)
    }
    
    //each item click method when response cell click
    @objc private func stackViewButtonsClickHandle(name: String) {
        switch name {
        case "真人注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 0
            self.navigationController?.pushViewController(page, animated: true)
        case "电子注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 1
            self.navigationController?.pushViewController(page, animated: true)
        case "捕鱼注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 3
            self.navigationController?.pushViewController(page, animated: true)
        case "电竞注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 4
            self.navigationController?.pushViewController(page, animated: true)
        case "PT电子注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 1
            page.singlePT = true
            page.singlePlatformCode = "PT"
            self.navigationController?.pushViewController(page, animated: true)
        case "体育注单":
            let loginVC = UIStoryboard(name: "touzhu_record", bundle: nil).instantiateViewController(withIdentifier: "touzhuRecord")
            let recordPage = loginVC as! TouzhuRecordController
            recordPage.titleStr = name
            recordPage.recordType = MenuType.SPORT_RECORD
            self.navigationController?.pushViewController(recordPage, animated: true)
        case "棋牌注单":
            let loginVC = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let recordPage = loginVC as! BetHistoryController
            recordPage.viewControllerType = 2
            self.navigationController?.pushViewController(recordPage, animated: true)
        case "额度转换":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            openConvertMoneyPage(controller: self)
            break
        case "额度转换记录":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            
            let vc = FeeConvertRecordController()
            vc.title = name
            self.navigationController?.pushViewController(vc, animated: true)
            
        case "积分兑换":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "score_change_page", bundle: nil).instantiateViewController(withIdentifier: "scoreExchange")
            let page = vc as! ScoreExchangeController
            self.navigationController?.pushViewController(page, animated: true)
            //            openScoreChange(controller: self)
            break
        case "彩票注单":
            let vc = GoucaiQueryController()
            vc.isAttachInTabBar = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "追号查询":
            self.navigationController?.pushViewController(ZuihaoQueryController(), animated: true)
            break
        case "个人报表":
            
//            if judgeIsMobileGuest() {
//                showToast(view: self.view, txt: tip_shiwan)
//                return
//            }
            self.navigationController?.pushViewController(GerenReportController(), animated: true)
            break
        case "团队报表":
            self.navigationController?.pushViewController(TeamReportsController(), animated: true)
            break
        case "打码量变动记录":
            self.navigationController?.pushViewController(CodeAmount(), animated: true)
            break
        case "账变报表":
            self.navigationController?.pushViewController(AccountChangeController(), animated: true)
            break
        case "充提记录":
            if judgeIsMobileGuest() {
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(TopupAndWithdrawRecords(), animated: true)
            break
        case "优惠活动":
            let vc = UIStoryboard(name: "active_page",bundle:nil).instantiateViewController(withIdentifier: "activePage") as! ActiveController
            vc.isAttachInTabBar = false
            vc.showDragBtnHandlerIS=false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "优惠活动大厅":
            openEventHall(controller: self)
            break
        case "用户资料":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(UserInfoController(), animated: true)
            break
        case "银行卡":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(BankCardListController(), animated: true)
        case "个人总览":
//            if YiboPreference.getAccountMode() == GUEST_TYPE{
//                showToast(view: self.view, txt: tip_shiwan)
//                return
//            }
            let vc = UIStoryboard(name: "geren_overview",bundle:nil).instantiateViewController(withIdentifier: "geren_overview") as! GerenTeamProfileController
            vc.fromTeam = false
            
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "密码修改":
//            if YiboPreference.getAccountMode() == GUEST_TYPE{
//                showToast(view: self.view, txt: tip_shiwan)
//                return
//            }
            if judgeIsMobileGuest() {
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(PasswordModifyController(), animated: true)
            break
        case "建议反馈":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            openFeedback(controller: self)
            break
        case "彩种信息":
            let vc = UIStoryboard(name: "lot_info_list",bundle:nil).instantiateViewController(withIdentifier: "lotInfo") as! LotInfoListController
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "彩种限额":
            let vc = UIStoryboard(name: "lot_fee_limit_page",bundle:nil).instantiateViewController(withIdentifier: "lotInfo") as! LotFeeLimitController
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "开奖结果":
            let vc = LotteryResultsController()
            vc.isAttachInTabBar = false
//            vc.code = "CQSSC"
            vc.code = DefaultLotteryNumber()
            vc.whichPage = "notRootVC"
            vc.showDragBtnHandlerIS=false;
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "走势图":
            let vc = LotteryResultsController()
            vc.isAttachInTabBar = false
            vc.code = DefaultLotteryNumber()
//           vc.code = "CQSSC"
            vc.showDragBtnHandlerIS=false;
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "团队总览":
            if YiboPreference.getAccountMode() == GUEST_TYPE && stationCode() != v023_ID {
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "geren_overview",bundle:nil).instantiateViewController(withIdentifier: "geren_overview") as! GerenTeamProfileController
            vc.fromTeam = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "用户列表":
            //            if YiboPreference.getAccountMode() == GUEST_TYPE{
            //                showToast(view: self.view, txt: "试玩账号不能进行此操作")
            //                return
            //            }
            if YiboPreference.getAccountMode() == AGENT_TYPE || YiboPreference.getAccountMode() == TOP_AGENT_TYPE{
                let vc = UIStoryboard(name: "user_list_page",bundle:nil).instantiateViewController(withIdentifier: "userList") as! UserListViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showToast(view: self.view, txt: tip_shiwan)
            }
            break
        case "我的推荐":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = TueijianViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "注册管理":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "register_manager_page",bundle:nil).instantiateViewController(withIdentifier: "register_manager") as! RegisterManagerContrller
            vc.title = "注册管理"
            self.navigationController?.pushViewController(vc, animated: true)
            //            self.navigationController?.pushViewController(RegistrationManagementController(), animated: true)
            break
        case "站内短信":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(InsideMessageController(), animated: true)
        case "周周转运":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let weekLuckyCtrl = ProfileWeekLuckCtrl.init(nibName: "ProfileWeekLuckCtrl", bundle: nil)
            weekLuckyCtrl.title = name
            weekLuckyCtrl.prizeTypes = .week
            weekLuckyCtrl.url = NATIVEDEFICIT_PAGE_DATA
            self.navigationController?.pushViewController(weekLuckyCtrl, animated: true)
            break
        case "每日加奖":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let weekLuckyCtrl = ProfileWeekLuckCtrl.init(nibName: "ProfileWeekLuckCtrl", bundle: nil)
            weekLuckyCtrl.title = name
            weekLuckyCtrl.prizeTypes = .day
            weekLuckyCtrl.url = BONUS_PAGE_DATA
            self.navigationController?.pushViewController(weekLuckyCtrl, animated: true)
            break
        case "充值卡":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let topUpCardCtrl = TopUpCardController()
            self.navigationController?.pushViewController(topUpCardCtrl, animated: true)
            break
        case "充值卡记录":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let topUpCardRecordCtrl = TopUpCardRecordController()
            self.navigationController?.pushViewController(topUpCardRecordCtrl, animated: true)
            break
        case "代金券":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let couponCtrl  = MemberCouponController()
            self.navigationController?.pushViewController(couponCtrl, animated: true)
            break
        case "网站公告":
            let vc = UIStoryboard(name: "notice_page",bundle:nil).instantiateViewController(withIdentifier: "notice_page") as! NoticesPageController
            self.navigationController?.pushViewController(vc, animated: true)
        case "帮助中心":
            let url = String.init(format: "%@%@%@", BASE_URL,PORT, HELP_CENTER_URL)
            openActiveDetail(controller: self, title: "帮助中心", content: "", foreighUrl: url)
            break;
        case "推广链接":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = SpreadVC()
            self.navigationController?.pushViewController(vc, animated: true)
            break;
        default:
            break
        }
    }
    func userHeaderView() -> UserHeaderView {
        if _userHeaderView == nil {
            _userHeaderView = UserHeaderView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: self.headerBgView.height))
            
        }
        return _userHeaderView!
    }
}
class MemberPageTwoHeader: UICollectionReusableView {
    
    private var titleLable = UILabel()
    
    func configTitle(title: String) {
        titleLable.text = title
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.myCustomInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.myCustomInit()
    }
    
    func myCustomInit() {
        
        let bgView = UIView()
        addSubview(bgView)
        bgView.whc_Top(1).whc_Left(0).whc_Bottom(1).whc_Right(1)
        setupNoPictureAlphaBgView(view: bgView)
        
        let verLine = UIView()
        addSubview(verLine)
        setThemeViewThemeColor(view: verLine)
        verLine.whc_Top(5).whc_Left(5).whc_Bottom(5).whc_Width(10)
        
        addSubview(titleLable)
        titleLable.font = UIFont.systemFont(ofSize: 17.0)
        titleLable.textColor = UIColor.black
        titleLable.whc_Top(0).whc_Left(5, toView: verLine).whc_Bottom(0)
    }
}
