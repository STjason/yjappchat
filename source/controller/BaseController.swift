//
//  BaseController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2017/12/3.
//  Copyright © 2017年 com.lvwenhan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class BaseController: UIViewController{
    
    var loadingDialog:MBProgressHUD?
    var keyBoardNeedLayout: Bool = true
    var popHeight:Int = 0
    var menuDelegate:MenuDelegate?
    var shouldFrosted = true
    var baseBgImageView = UIImageView()
    var baseBgImageViewCover = UIView()
    
    /** 标识进入前台次数 */
    var appCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        setupThemeView()
        if shouldFrosted {
            setupthemeBgView(view: self.view)
//            setupGeneralThemeView()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationBecomeActiv), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    // 监听后台进入前台
    @objc func applicationBecomeActiv()
    {
        if !switch_GesturePwdOn(){
            return
        }
        let passWord = UserDefaults.standard.value(forKey: "passWord")
        if YiboPreference.getGESTURE_LOCK() == "true" && passWord != nil && YiboPreference.getGestureLockNetSwitch() == "on" && !judgeIsMobileGuest() && YiboPreference.getAccountMode() != GUEST_TYPE && YiboPreference.getLoginStatus() == true {
            if appCount > 0 {
                return
            }else
            {
                appCount = appCount + 1
            }
            let now = NSDate()
            // 当前时间戳
            let nowStamp = Int(now.timeIntervalSince1970)
            // 最后一次退出前台进入后台时间戳
            let lastStamp = YiboPreference.getLAST_TIME()
            // 手势锁时间间隔
            let interval = YiboPreference.getGESTURE_LOCK_TIME()
            
            if interval < nowStamp - lastStamp {
                let gestureLockMainVc = GestureLockMainViewController()
                gestureLockMainVc.isModify = false
                gestureLockMainVc.unlockSuccessBlock = {(status) in
                    if status == .checkLoginOut {
                        /// 登出当前账户
                        YiboPreference.setToken(value: "" as AnyObject)
                        YiboPreference.saveLoginStatus(value: false as AnyObject)
                        YiboPreference.saveUserName(value: "" as AnyObject)
                        YiboPreference.savePwd(value: "" as AnyObject)
                        YiboPreference.setAccountMode(value: 0 as AnyObject)
                        YiboPreference.setCACHEAVATARdata(value: Data.init())
                    }
                }
                self.present(gestureLockMainVc, animated: true, completion: nil)
            }
        }
    }
    // 监听前台进入后台
    @objc func applicationEnterBackground()
    {
        let passWord = UserDefaults.standard.value(forKey: "passWord")
        if YiboPreference.getGESTURE_LOCK() == "true" && passWord != nil && YiboPreference.getGestureLockNetSwitch() == "on" && !judgeIsMobileGuest() && YiboPreference.getAccountMode() != GUEST_TYPE && YiboPreference.getLoginStatus() == true {
            appCount = 0
            let now = NSDate()
            let nowStamp = Int(now.timeIntervalSince1970)
            YiboPreference.setLAST_TIME(time: nowStamp)
        }
    }
    
    private func setupThemeView() {
        
        setViewBackgroundColorTransparent(view: self.view)
        
        baseBgImageView = UIImageView()
        self.view.addSubview(baseBgImageView)
        baseBgImageView.whc_Top(0).whc_Left(0).whc_Bottom(0).whc_Right(0)
        baseBgImageView.contentMode = .scaleAspectFill
        baseBgImageView.theme_image = "FrostedGlass.ColorImage"
        self.view.insertSubview(baseBgImageView, at: 0)
    
        baseBgImageViewCover = UIView()
        self.view.addSubview(baseBgImageView)
        baseBgImageViewCover = baseBgImageViewCover.whc_FrameEqual(baseBgImageView)
        baseBgImageViewCover.theme_backgroundColor = "FrostedGlass.ColorImageCoverColor"
        self.view.insertSubview(baseBgImageViewCover, at: 1)
    }
    
    func setupGeneralThemeView(bgImageViewCoverAlpha: CGFloat = 0.2) {
        if bgImageViewCoverAlpha == 0 {
            self.baseBgImageViewCover.theme_alpha = "FrostedGlass.ColorImageCoverAlphZeroForGlass"
        }else {
            self.baseBgImageViewCover.theme_alpha = "FrostedGlass.ColorImageCoverAlpha"
        }
    }
    
    func showDialog(view:UIView,loadText:String) -> Void {
//        if loadingDialog == nil {
            loadingDialog = showLoadingDialog(view: view, loadingTxt: loadText)
//        }
    }
    
    func hideDialog() -> Void {
        if loadingDialog != nil {
            hideLoadingDialog(hud: loadingDialog!)
        }
    }
    
    @objc func onBackClick(){
        if self.navigationController != nil{
            let count = self.navigationController?.viewControllers.count
            if count! > 1{
                self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true) {
                    let appdele:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appdele.isPresent = false
                }
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func request(baseUrl:String,frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                 callback:@escaping (_ resultJson:String,_ returnStatus:Bool)->()) -> Void {
        
        if !NetwordUtil.isNetworkValid(){
            showToast(view: self.view, txt: "网络连接不可用，请检测")
            return
        }
        
        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog {
                self.showDialog(view: self.view, loadText: loadTextStr)
            }
        }
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in
            self.hideDialog()
            
            print("url ====\(BASE_URL)\(url)")
            //网站维护时处理
            if self.isWebsiteCause(json: resultJson){
                return
            }
            callback(resultJson, returnStatus)
        }
        let baseUrl = baseUrl + PORT
        requestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
    }
    
    
    func request(frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                 callback:@escaping (_ resultJson:String,_ returnStatus:Bool)->()) -> Void {
        print(url)
        if !NetwordUtil.isNetworkValid(){
            showToast(view: self.view, txt: "网络连接不可用，请检测")
            return
        }
        
        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog {
                self.showDialog(view: UIApplication.shared.keyWindow!, loadText: loadTextStr)
            }
        }
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in
            self.hideDialog()
            //判断网站维护
            if self.isWebsiteCause(json: resultJson){
                return
            }
            callback(resultJson, returnStatus)
        }
        
        if url == URL_HEADER_DER {
            requestData(curl: url , cm: method, parameters:params,before: beforeClosure, after: afterClosure)
            return
        }else if url.contains("gateway?") { //支付
            requestData(curl:url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
            return
        }else if url == "https://logou8.com/domain/getServerIp" {
            requestData(curl:url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
            return
        }
        
        let baseUrl = BASE_URL + PORT
        requestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
    }
    
    private func isWebsiteCause(json:String) -> Bool{
        if isEmptyString(str: json){
            return false
        }
        if let result = MintenanceWraper.deserialize(from: json){
            if !result.success{
                if !isEmptyString(str: result.msg){
                    //                        showToast(view: self.view, txt: result.msg)
                    let preffix = "maintenance_exception:"
                    if result.msg.starts(with: preffix){
                        let subcause = (result.msg as NSString).substring(from: preffix.length)
                        showWhenWebsiteMintancce(controller: self,cause:subcause)
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func expandRequest(frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                       callback:@escaping (_ resultJson:String,_ returnStatus:Bool)->(),maxAge: Int = 15) -> Void {
        
        if !NetwordUtil.isNetworkValid(){
            showToast(view: self.view, txt: "网络连接不可用，请检测")
            return
        }
        
        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog {
                self.showDialog(view: self.view, loadText: loadTextStr)
            }
        }
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in
            self.hideDialog()
            callback(resultJson, returnStatus)
        }
        let baseUrl = BASE_URL + PORT
        
        expandRequestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure,maxAge: maxAge)
    }
    
    
    func print_error_msg(msg:String?) -> Void {
        if let errorMsg = msg{
            showToast(view: self.view, txt: errorMsg)
        }else{
            showToast(view: self.view, txt: convertString(string: "获取失败"))
        }
    }
    
    //键盘弹起响应
    @objc func keyboardWillShow(notification: NSNotification) {
        print("show")
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
//            if popHeight == 0{
//                deltaY = intersection.height-21-44
//            }else{
//                deltaY = CGFloat(popHeight)
//            }
            if keyBoardNeedLayout {
                UIView.animate(withDuration: duration, delay: 0.0,
                               options: UIView.AnimationOptions(rawValue: curve),
                               animations: {
                                self.view.frame = CGRect.init(x:0,y:-deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                                self.keyBoardNeedLayout = false
                                self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    //键盘隐藏响应
    @objc func keyboardWillHide(notification: NSNotification) {
        print("hide")
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
//            if popHeight == 0{
//                deltaY = intersection.height+21+44
//            }else{
//                deltaY = 21+44
//            }
            UIView.animate(withDuration: duration, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            self.view.frame = CGRect.init(x:0,y:deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
}

