//
//  CreateSpreadTBCell.swift
//  gameplay
//
//  Created by William on 2018/8/8.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class CreateSpreadTBCell: UITableViewCell {

    @IBOutlet weak var mylabel2BgView: UIView!
    @IBOutlet weak var mylabel: UILabel!
    @IBOutlet weak var mylabel2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mylabel.numberOfLines = 2
        setupNoPictureAlphaBgView(view: self)
        setupNoPictureAlphaBgView(view: self.mylabel2BgView, alpha: 0.4, bgViewColor: "FrostedGlass.creatNewSpreadCellGray")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
