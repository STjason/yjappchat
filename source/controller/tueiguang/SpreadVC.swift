//
//  SpreadVC.swift
//  gameplay
//
//  Created by William on 2018/8/8.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class SpreadVC: BaseController, UITableViewDelegate,UITableViewDataSource,DelegateSpreadTBCell {

    /** 当前需要识别的二维码cell */
    var qrCodeCell : SpreadTBCell?
    
    @IBOutlet weak var myTableView: UITableView!
    private var promp_link_mode = ""
    var result:SpreadModel = SpreadModel()
    
    var isFirst = true
    var tueiguangIndexs:[Int] = Array<Int>()//删除推广时，序号不马上刷新，重进页面再刷新数据
    
    override func viewWillAppear(_ animated: Bool) {
        tueiguangIndexs = Array<Int>()
        isFirst = true
        getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "推广链接"
        
        if !YiboPreference.getLoginStatus() {
            loginWhenSessionInvalid(controller: self)
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新建推广", style: UIBarButtonItem.Style.plain, target: self, action: #selector(createMethod))
        
        promp_link_mode = promp_link_mode_switch()
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.tableFooterView = UIView()
        setViewBackgroundColorTransparent(view: myTableView)
        let identify = "SwiftCell"
        myTableView.register(UINib.init(nibName: "SpreadTBCell", bundle: nil), forCellReuseIdentifier: identify)
    }
    
    @objc func createMethod(){
        let vc = CreateSpreadVC2()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getData() { //"/native/agent_reg_promotionList.do"
        request(frontDialog: true, method: .post, loadTextStr: "正在加载...", url: agent_reg_promotionList) { (resultJson:String, resultStatus:Bool) in
            
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "获取失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if let result = SpreadModel.deserialize(from: resultJson) {
                if result.success{
                    self.result = result
                    if self.isFirst {
                        self.isFirst = false
                        if let count:Int = self.result.content?.rows?.count {
                            for i in 0..<count{
                                self.tueiguangIndexs.append(count - i - 1)
                            }
                        }
                    }
                    self.myTableView.reloadData()
                }else{
                    if !isEmptyString(str: result.msg){
                        showToast(view: self.view, txt: result.msg)
                    }else{
                        showToast(view: self.view, txt: "获取失败")
                    }
                }
            }
        }
    }
    
    //只有一个分区
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    //返回表格行数
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.tableViewDisplayWithMessage(datasCount: (result.content?.rows?.count ?? 0), tableView: tableView)
        return (self.result.content?.rows?.count) ?? 0
    }
    
    //cell的高度
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height:CGFloat = promp_link_mode != "v1" ? getSwitch_lottery() ? 445 : 400 : getSwitch_lottery() ? 445 : 400
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identify = "SwiftCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identify, for: indexPath) as! SpreadTBCell
        cell.rateConstraintH.constant = getSwitch_lottery() ? 44 : 0
        cell.downOrSpreadVersion = promp_link_mode_switch()
        cell.backgroundColor = UIColor.brown
        
        if self.result.content?.rows?.count != nil {
            let model:DailiModel = (result.content?.rows![indexPath.row])!
            cell.addMst(model: model, num: self.tueiguangIndexs[indexPath.row])
        }
        
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(qrCodeLongPress(longPress:)))
        longPress.minimumPressDuration = 0.5
        cell.qrCodeImageView.isUserInteractionEnabled = true
        cell.qrCodeImageView.addGestureRecognizer(longPress)
        
        cell.selectionStyle = .none //选中状态的模式(无颜色变化)
        
        cell.delegate = self
        
        return cell
    }
    
    @objc func qrCodeLongPress(longPress: UILongPressGestureRecognizer) {
        if longPress.state == .began {
            self.becomeFirstResponder()
            
            let point = longPress.location(in: self.myTableView)
            let indexPath = self.myTableView.indexPathForRow(at: point)
            qrCodeCell = self.myTableView.cellForRow(at: indexPath!) as? SpreadTBCell
            
            let alertController = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
            
            let cacheImageAction = UIAlertAction(title: "保存图片", style: .default) { (alert: UIAlertAction) in
                //将图片保存到本地
                UIImageWriteToSavedPhotosAlbum((self.qrCodeCell?.qrCodeImageView.image)!, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
            }
            alertController.addAction(cacheImageAction)
            
            let qrCodeAction = UIAlertAction(title: "识别二维码", style: .default) { (alert: UIAlertAction) in
                //1.初始化扫描仪，设置设别类型和识别质量
                let options = ["IDetectorAccuracy" : CIDetectorAccuracyHigh]
                let detector: CIDetector = CIDetector.init(ofType: "CIDetectorTypeQRCode", context: nil, options: options)!
                //2.扫描获取的特征组
                let features = detector.features(in: CIImage.init(cgImage: (self.qrCodeCell?.qrCodeImageView.image?.cgImage)!))
                //3.获取扫描结果
                let feature = features[0] as! CIQRCodeFeature
                let scannedResult = feature.messageString
                // 弹窗提示扫描结果
                let alert = PublicAlertView.init(title: "扫描结果为", message: scannedResult, customView: nil, delegate: nil, cancelButtonTitle: "取消", otherButtonTitles: "前往", type: 0)
                alert?.setConfirmBlock({
                    openBrower(urlString: scannedResult!)
                })
                alert?.show()
            }
            alertController.addAction(qrCodeAction)
            
            let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
        
        if error != nil {
            showErrorHUD(errStr: "保存失败！")
            
        } else {
            showErrorHUD(errStr: "保存成功！")
        }
    }
    
    func queryDetail(datasoure: [String]) {
        if datasoure.isEmpty{
            return
        }
        let dialog = DetailListDialog(dataSource: datasoure, viewTitle: "推广详情")
        dialog.selectedIndex = 0
        self.view.window?.addSubview(dialog)
        dialog.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(dialog.kHeight)
        dialog.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            dialog.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func deleteUrl(index: Int, num:Int) { // "/native/agent_delete_prom_link.do"
        let alertController = UIAlertController(title:"温馨提示",message:"你确定要删除吗？",preferredStyle: .alert);
        let canceAction = UIAlertAction(title:"取消",style:.cancel,handler:nil);
        let okAciton = UIAlertAction(title:"确定",style:.default,handler: {
            action in
            var tueiguangIndex = 0
            for i in 0..<self.tueiguangIndexs.count{
                if self.tueiguangIndexs[i] == num{
                    tueiguangIndex = i
                }
            }
            self.tueiguangIndexs.remove(at: tueiguangIndex)
            self.sureDelete(index: index)
        })
        alertController.addAction(canceAction);
        alertController.addAction(okAciton);
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sureDelete(index: Int) {
        request(frontDialog: true, method: .post, loadTextStr: "正在删除...", url: agent_delete_prom_link, params: ["id":index]) { (resultJson:String, resultStatus:Bool) in
            if !resultStatus {
                return
            }
            self.getData()
        }
    }

}
