//
//  SpreadTBCell.swift
//  gameplay
//
//  Created by William on 2018/8/8.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

protocol DelegateSpreadTBCell:NSObjectProtocol {
    //设置协议方法
    func queryDetail(datasoure:[String]) // 查看详情
    func deleteUrl(index:Int, num:Int) //删除推广链接
}

class SpreadTBCell: UITableViewCell {
    
    weak var delegate:DelegateSpreadTBCell?
    
    private var mymodel:DailiModel!
    private var num:Int!
    var downOrSpreadVersion = "v1"
    
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var rateConstraintH: NSLayoutConstraint!
    @IBOutlet weak var tueiguangLb: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var typeLB: UILabel!
    @IBOutlet weak var maiLB: UILabel!
    @IBOutlet weak var rebackLB: UILabel!
    @IBOutlet weak var ordinaryUrl: UILabel!
    @IBOutlet weak var copyputongUrl: UIButton!
    @IBOutlet weak var queryBtn: UIButton!
    @IBOutlet weak var downOrSpreadUrlName: UILabel!
    @IBOutlet weak var encryptionConsH: NSLayoutConstraint!
    @IBOutlet weak var encryptUrl: UILabel!
    
    @IBOutlet weak var encryptCopyBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        copyputongUrl.addTarget(self, action: #selector(copyputongUrlAction(btn:)), for: UIControl.Event.touchUpInside)
        queryBtn.addTarget(self, action: #selector(queryDetail(btn:)), for: UIControl.Event.touchUpInside)
        encryptCopyBtn.addTarget(self, action: #selector(copyEncryption), for: .touchUpInside)
        deleteBtn.addTarget(self, action: #selector(deleteMethod), for: UIControl.Event.touchUpInside)
        
    }
    
    @objc func deleteMethod(){
        self.delegate?.deleteUrl(index: self.mymodel.id!, num: self.num!)
    }
    
    @IBAction func copyTueiguangMa(_ sender: UIButton) {
        UIPasteboard.general.string = maiLB.text
        showToast(view: self.contentView, txt: convertString(string: "复制成功"))
    }
    @objc func copyputongUrlAction(btn:UIButton){
        UIPasteboard.general.string = ordinaryUrl.text
        showToast(view: self.contentView, txt: convertString(string: "复制成功"))
    }
    
    @objc func copyEncryption() {
        UIPasteboard.general.string = encryptUrl.text
        showToast(view: self.contentView, txt: convertString(string: "复制成功"))
    }
    
    
    @objc func queryDetail(btn:UIButton){//点击查看详情
        var arr = Array<String>()
        if let model = self.mymodel{
            arr.append("生成时间: \(model.createTime!)")
            arr.append("总访问量: \(model.accessNum!)")
            arr.append("注册人数: \(model.registerNum!)")
            
            if getSwitch_lottery() {
                arr.append("彩票返点: \(model.cpRolling!)‰")
            }
            
            let system = getSystemConfigFromJson()
            let sport = system?.content.onoff_sport_switch
            let sbsport = system?.content.onoff_sb_switch
            let zhenren = system?.content.onoff_zhen_ren_yu_le
            let game = system?.content.onoff_dian_zi_you_yi
            let chess = system?.content.switch_chess
            
            if !isEmptyString(str: sport!) && sport == "on"{
                if model.sportRebate != nil {
                    arr.append("体育返点: \(model.sportRebate!)‰")
                }
            }
            if !isEmptyString(str: sbsport!) && sbsport == "on"{
                if model.sbSportRebate != nil {
                    arr.append("沙巴体育返点: \(model.sbSportRebate!)‰")
                }
            }
            if !isEmptyString(str: zhenren!) && zhenren == "on"{
                if model.realRebate != nil {
                    arr.append("真人返点: \(model.realRebate!)‰")
                }
            }
            if !isEmptyString(str: game!) && game == "on"{
                if model.egameRebate != nil {
                    arr.append("电子返点: \(model.egameRebate!)‰")
                }
            }
            if !isEmptyString(str: chess!) && chess == "on"{
                if model.chessRebate != nil {
                    arr.append("棋牌返点: \(model.chessRebate!)‰")
                }
            }
            delegate?.queryDetail(datasoure: arr)
        }
    }
    
    
    func addMst(model:DailiModel, num:Int) {
        self.mymodel = model
        self.num = num
        tueiguangLb.text = "推广\(num+1)" //model.registerNum!
        typeLB.text = getType(type: model.type!)
        maiLB.text = model.linkKey
        rebackLB.text = "\(model.cpRolling!)‰"
        
//        loginWhenSessionInvalid(controller: self)
        
        if downOrSpreadVersion == "v1" {
            if let config = getSystemConfigFromJson(){
                if config.content != nil{
                    let url = config.content.app_download_link_ios
                    ordinaryUrl.text = url
                }
            }
            downOrSpreadUrlName.text = "下载链接"
        }else {
            ordinaryUrl.text = model.linkUrl
            downOrSpreadUrlName.text = "推广链接"
            encryptUrl.text = model.linkUrlEn
        }
        setQRCodeToImageView(qrCodeImageView, model.linkUrl)
    }
    
    func getType(type:Int) -> String {
        switch type {
        case 1:
            return "会员"
        case 2:
            return "代理"
        default:
            print("类型错误 ========= ")
        }
        return "error"
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
