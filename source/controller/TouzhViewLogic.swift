//
//  TouzhViewLogic.swift
//  gameplay
//
//  Created by admin on 2018/10/2.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class TouzhViewLogic: UIView {

     func drawDashLineAction(lineView: UIView)
    {
        // 创建对象
        let shapeLayer = CAShapeLayer.init()
        // 设置路径
        shapeLayer.path = self.drawDashLine().cgPath
        // 设置路径的颜色
        shapeLayer.theme_strokeColor = "Global.themeColor"
        // 设置虚线的间隔
        shapeLayer.lineDashPattern = [3.0,1.5];
        // 设置路径的宽度
        shapeLayer.lineWidth = 1.5
        // 路径的渲染
        lineView.layer.addSublayer(shapeLayer)
    }
    
    // MARK: 虚线绘制
    func drawDashLine() -> UIBezierPath {
        let BzPath = UIBezierPath.init()
        BzPath.move(to: CGPoint.init(x: 0, y: 0))
        BzPath.addLine(to: CGPoint.init(x: kScreenWidth, y: 0))
        return BzPath
    }
}
