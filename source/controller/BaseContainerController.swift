//
//  BaseContainerController.swift
//  gameplay
//
//  Created by admin on 2019/11/25.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import MBProgressHUD
import SocketIO
import PKHUD

class BaseContainerController: BaseMainController {
    
    override func viewWillAppear(_ animated: Bool) {
        adjustRightBtn()
        
        kRequest(isContent: false, frontDialog: false, url: VALID_LOGIN_URL) { (content) in
            if let result = ValidLoginWraper.deserialize(from: content as? String ?? ""){
                if !result.success{
                    socket?.disconnect()
                    socket = nil
                    YiboPreference.saveLoginStatus(value: false as AnyObject)
//                    YiboPreference.saveAutoLoginStatus(value: false)
                    self.adjustRightBtn()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //loginAndAuthGetParameters()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(reConnectSocketAction), name: NSNotification.Name(rawValue: "reConnectSocketNoti"), object: nil)
    }
    
//    @objc func reConnectSocketAction() {
//        loginAndAuthGetParameters()
//    }
    
    ///获取授权的参数
    func loginAndAuthGetParameters() {
        
        CRNetManager.shareInstance().netAuthenticateLogin(paramters: [:], method: .get) {[weak self] (success, msg, model) in
            
            guard let weakSelf = self else {
                return
            }
            
            if !success {
//                showToast(view: weakSelf.view, txt: msg) //首页聊天室授权错误不要显示出来
                return
            }
            
            guard let m = model else {
                return
            }
            
            weakSelf.netAuthenticate(model: m)
        }
    }
    
    ///初始化socket
    func netAuthenticate(model:CRAuthenticateModel) {
        
        /* 同步中转系统返回的聊天室域名和文件系统域名*/
        var chatFileUrl = model.chatFileUrl
        let array = model.chatFileUrl.components(separatedBy: "/")
        if let components = array.last {
            if components == "" {
               chatFileUrl = chatFileUrl.subString(start: 0, length: (chatFileUrl.length - 1))
            }
        }

        if chatFileUrl.length > 0 {
            CRUrl.shareInstance().CHAT_FILE_BASE_URL = chatFileUrl
        }
        
        CRUrl.shareInstance().CRCHAT_URL = model.chatDomainUrl
        authenticateModel = model
        
        //初始化socket
//        setupNewSocket()
    }
    
    //发送消息也用socket的socket初始化
    func setupNewSocket() {
        socket = nil
        
        let params = ["key":"#!@$%&^*AEUBSJXK","token":CRCommon.generateMsgID()]
        guard let paramsString = jsonStringFrom(dictionary: params) else {
            return
        }
        
        let encryParams = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: paramsString)
        
        let chatURL = CRUrl.shareInstance().CRCHAT_URL
        guard let url = URL.init(string: chatURL) else {return}
        
        print("\n初始化SocketIOClient,参数:\(params)\nurl:\(url)")
        
        let optionParams = SocketIOClientOption.connectParams(["key":encryParams])
        let optionForceNew = SocketIOClientOption.forceNew(true)
        let optionLog = SocketIOClientOption.log(false)
        let optionCompress = SocketIOClientOption.compress
        let config = SocketIOClientConfiguration.init(arrayLiteral: optionParams,optionForceNew,optionLog,optionCompress)
        
        let manager = SocketManager(socketURL: url, config: config)
        manager.connect()
        socketHandler()
    }
    
    func socketHandler() {
        
        socket?.on("new message") { (data, ack) in
            print("socket==========2\nnew message dada:\(data),ack = \(ack)")
        }
        
        
        
        socket?.on(clientEvent: .connect) { (data, ack) in
            
            print("socket==========3\nsocket.on connect dada:\(data),ack = \(ack)")
            isSocketConnect = true
        }
        
        socket?.on(clientEvent: .disconnect) { (data, ack) in
            print("socket==========4\nsocket.on disconnect dada:\(data),ack = \(ack)")
            isSocketConnect = false
        }
        
        socket?.on(clientEvent: .error) { (data, ack) in

            isSocketConnect = false
            print("socket==========5\nsocket.on error dada:\(data),ack = \(ack)")
        }
        
        socket?.on(clientEvent: .reconnect) { (data, ack) in
            
            print("socket==========6\nsocket.on reconnect dada:\(data),ack = \(ack)")
        }
        
        socket?.on(clientEvent: .reconnectAttempt) { (data, ack) in
            print("socket==========7\nsocket.on reconnectAttempt dada:\(data),ack = \(ack)")
        }
        
        socket?.on(clientEvent: .statusChange) {[weak self] (data, ack) in
            print("socket==========8\nsocket.on statusChange dada:\(data),ack = \(ack)")
            guard let _ = self else {
                return
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}
