//
//  Tueijian02TableViewCell.swift
//  gameplay
//
//  Created by William on 2018/8/18.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class Tueijian02TableViewCell: UITableViewCell {
    
    @IBOutlet weak var mylabel1: UILabel!
    @IBOutlet weak var mylabel2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        setupNoPictureAlphaBgView(view: self)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: mylabel1)
        setThemeLabelTextColorGlassWhiteOtherBlack(label: mylabel2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
