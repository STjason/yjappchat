//
//  showCoverView.swift
//  gameplay
//
//  Created by Gallen on 6/2/2020.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit

class ShowCoverView: UIView {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var dismissView: UIButton!
    @IBOutlet weak var QRImageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.backgroundColor = UIColor.black.cgColor
        backView.layer.borderWidth = 2
    }
    @IBAction func dismissViewClick(_ sender: UIButton) {
        removeFromSuperview()
    }
    
}
extension ShowCoverView{
    class func loadFromNib() -> ShowCoverView {
        return Bundle.main.loadNibNamed("ShowCoverView", owner: nil, options: nil)?[0] as! ShowCoverView
    }
}
