//
//  Tueijian01TableViewCell.swift
//  gameplay
//
//  Created by William on 2018/8/18.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import SnapKit

class Tueijian01TableViewCell: UITableViewCell {
    
    @IBOutlet weak var tueijianAddress: UILabel!
    @IBOutlet weak var mytext: UILabel!
    
    @IBOutlet weak var showQRCodeButton: UIButton!
    @IBOutlet weak var showQRCodeWidthConstraint: NSLayoutConstraint!
    var QRCodeLink:String = ""
    var linkKey:String = ""
//
//    lazy var backView:UIView = {
//        let view = UIView()
//        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
//        view.frame = UIScreen.main.bounds
//        UIApplication.shared.keyWindow?.addSubview(view)
//
//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissView))
//        view.addGestureRecognizer(tap)
//        return view
//    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupNoPictureAlphaBgView(view: self)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: tueijianAddress)
        setThemeLabelTextColorGlassWhiteOtherBlack(label: mytext)
    }
    
    @IBAction func myCopy(_ sender: UIButton) {
        UIPasteboard.general.string = mytext.text
        showToast(view: self.contentView, txt: convertString(string: "复制成功"))
    }
    
    
    @IBAction func showQRCode(_ sender: UIButton) {
        //
        if !QRCodeLink.isEmpty{
            let link = BASE_URL + "/qr/getPromotionQrCode.do?linkKey=" + linkKey
            let view =  ShowCoverView.loadFromNib()
            view.frame = UIScreen.main.bounds
            downloadImage(url: URL.init(string: link)!, imageUI: view.QRImageV)
//            view.QRImageV.kf.setImage(with: URL(string: link), placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: nil)
            UIApplication.shared.keyWindow?.addSubview(view)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

