//
//  MemberPageController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/5.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

let offset_HeaderStop:CGFloat = 40.0 // At this offset the Header stops its transformations
let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label

class MemberPageController: BaseMainController{
    
    @IBOutlet weak var gridview:UICollectionView!
    var datas:[MemberBean] = []
    var bgImageView = UIImageView()
    
    /** 新版头部试图 带会员等级进度条 */
    var _userHeaderView : UserHeaderView?
    
    var messageNumber:Int = 0
    //MARK: - UI
    //MARK: 设置下拉时，headerView效果
    private func setupBGView() {
        bgImageView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 40)
        self.view.insertSubview(bgImageView, at: 0)
        updateHeaderBgLogo()
    }
    
    func updateHeaderBgLogo() -> Void {
        guard let sys = getSystemConfigFromJson() else{return}
        var logoImg = sys.content.member_page_bg_url
        if !isEmptyString(str: logoImg){
            //这里的logo地址有可能是相对地址
            if logoImg.contains("\t"){
                let strs = logoImg.components(separatedBy: "\t")
                if strs.count >= 2{
                    logoImg = strs[1]
                }
            }
            logoImg = logoImg.trimmingCharacters(in: .whitespaces)
            if !logoImg.hasPrefix("https://") && !logoImg.hasPrefix("http://"){
                logoImg = String.init(format: "%@/%@", BASE_URL,logoImg)
            }
            downloadImage(url: URL.init(string: logoImg)!, imageUI: self.bgImageView)
        }else{
            self.bgImageView.contentMode = .scaleAspectFill
            self.bgImageView.clipsToBounds = true
            self.bgImageView.theme_image = "General.personalHeaderBg"
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity

        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / bgImageView.bounds.height
            let headerSizevariation = ((bgImageView.bounds.height * (1.0 + headerScaleFactor)) - bgImageView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            bgImageView.layer.transform = headerTransform
        }else {
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
        }
    }
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        if glt_iphoneX {
            let themeView = UIView.init()
            self.view.addSubview(themeView)
            themeView.whc_Left(0).whc_Right(0).whc_Top(0).whc_Height(44)
            themeView.theme_backgroundColor = "Global.themeColor"
        }
        
        setupthemeBgView(view: self.view, alpha: 0)
        
        gridview.delegate = self
        gridview.dataSource = self
        gridview.register(MemberCell.self, forCellWithReuseIdentifier:"memberCell")
        gridview.showsVerticalScrollIndicator = false
        //添加头视图
        gridview.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadMemberDatas()
        
        showDragBtnHandler?(false,self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        showDragBtnHandler?(true,self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.isNavigationBarHidden = true
        var popDatas:[ReceiveMessageModelContentRow] = [ReceiveMessageModelContentRow]()
        LennyNetworkRequest.obtainMessageReceiveList { (model) in
            if model?.code == 0 && !(model?.success)!{
                loginWhenSessionInvalid(controller: self)
                return
            }
            for item in (model?.content?.rows) ?? []{
                //
                if item.popStatus == 2{
                    //有弹窗
                    popDatas.append(item)
                }
            }
            if popDatas.count > 0  {
                if YiboPreference.getInsideStation(){
                    let webListView = WebviewList.init(noticeResuls: nil, inSideDatas: popDatas, urlContent: false, dialogTitle: "站内信", type: 4)
                    webListView.show()
                }
            }
        }
        userMessage()
        /// 每次进入刷新会员数据
        self.accountWeb()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    //MARK:账户信息
    func accountWeb(){
        request(frontDialog: false, url:MEMINFO_URL,callback: {[weak self](resultJson:String,resultStatus:Bool)->Void in
            
            guard let weakSelf = self else{return}
            if !resultStatus {
                return
            }
            if let result = MemInfoWraper.deserialize(from: resultJson){
                if result.success{
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    if let memInfo = result.content{
                        //更新帐户名，余额等信息
                        weakSelf.userHeaderView().refreshUserData(userData: memInfo)
                    }
                }
            }
        })
        //会员等级显示开关
        if getSystemConfigFromJson()?.content.switch_level_show != "on"  {
            return
        }
        // 会员信息
        kRequest(isContent:true, frontDialog: false, url: GET_LEVEL_INFO) {[weak self] (content) in
            guard let weakSelf = self else{return}
            let model = VipLevelMoel().getCurrentJson(dic: content as? NSDictionary ?? NSDictionary())
            weakSelf.userHeaderView().refreshVipData(vipData: model)
        }
    }
    
    /** 获取未读消息数量 */
    func userMessage(){
        request(frontDialog: false, method: .get, loadTextStr: "", url: "/native/unread_msg_count_v3.do", params: [:]) { [weak self](resultJson:String, resultStatus:Bool) in
            guard let weakSelf = self else{return}
            if resultStatus{
                let model = CenterUserMessageNumber.deserialize(from: resultJson)
                if model!.success {
                    if model?.content?.count != nil {
                        weakSelf.messageNumber = (model?.content?.count)!
                        weakSelf.gridview.reloadData()
                    }
                }
            }
        }
    }
    
    private func shouldContainItems(txtName:String) -> Bool {
        let lotteryFitter = ["彩票注单","开奖结果","走势图","彩种信息","彩种限额","彩种信息"]
        if lotteryFitter.contains(txtName) {
            if !getSwitch_lottery() {
                return true
            }
        }
        return false
    }
    //获取本地json数据
    func loadMemberDatas() -> Void {
        DispatchQueue.global().async {
            do{
                let path = Bundle.main.path(forResource: "member_data", ofType: "json")
                if let pathValue = path{
                    //2 获取json文件里面的内容,NSData格式
                    let jsonData = NSData.init(contentsOfFile: pathValue)
                    //3 解析json内容
                    let json = try JSONSerialization.jsonObject(with: jsonData! as Data, options:[]) as! [AnyObject]
                    self.datas.removeAll()
                    let dailiFilter:[String] = ["团队报表","团队总览","代理管理","推广链接","我的推荐","注册管理"]
                    
                    for item in json{
                        let iconName = item["icon_name"] as! String
                        let txtName = item["txt_name"] as! String
                        let result = MemberBean()
                        result.iconName = iconName
                        result.txtName = txtName
                        
                        //会员或试玩账号时，不显示
                        if (YiboPreference.getAccountMode() == MEMBER_TYPE || YiboPreference.getAccountMode() == GUEST_TYPE) {
                            if dailiFilter.contains(txtName){
                                if !(txtName == "团队总览" && stationCode() != v023_ID) {
                                    continue
                                }
                            }
                        }
                        
                        if self.shouldContainItems(txtName: txtName) {continue}
                         
                        if let sys = getSystemConfigFromJson() {
                            if sys.content != nil{
                                
                                if txtName == "注册管理" {
                                    let fix_mode = sys.content.fixed_rate_model
                                    let cp_dynamic_switch = sys.content.lottery_dynamic_odds
                                    //固定模式，且下级返点一致时，不永旭设定返点
                                    if fix_mode == "on" && cp_dynamic_switch != "on"{
                                        continue
                                    }
                                }
                                
                                if txtName == "团队总览" && stationCode() != v023_ID {
                                    let fix_mode = sys.content.fixed_rate_model //此项开启
                                    let cp_dynamic_switch = sys.content.lottery_dynamic_odds //此项关闭
                                    if fix_mode == "on" && cp_dynamic_switch == "off"{
                                        continue
                                    }
                                }
                                
                                //额度转换开关没开时，不显示
                                if txtName == "额度转换" || txtName == "额度转换记录" {
                                    let switch_money_change = sys.content.switch_money_change
                                    if isEmptyString(str: switch_money_change) || switch_money_change == "off"{
                                        continue
                                    }
                                }
                                
                                //额度转换开关没开时，不显示
                                if txtName == "积分兑换" {
                                    let switch_money_change = sys.content.exchange_score
                                    if isEmptyString(str: switch_money_change) || switch_money_change == "off"{
                                        continue
                                    }
                                }
                                
                                //是否显示追号查询
                                if txtName == "追号查询" {
                                    let show_lottrack_menu = sys.content.show_lottrack_menu
                                    if (!isEmptyString(str: show_lottrack_menu) && show_lottrack_menu == "on") || !getSwitch_lottery() {
                                        continue
                                    }
                                }
                                
                                //我的推荐在动态赔率开关打开时，不显示
                                if txtName == "我的推荐" {
                                    let lottery_dynamic_odds = sys.content.lottery_dynamic_odds
                                    let same_rateback_when_dynamic = sys.content.fixed_rate_model
                                    
                                    if lottery_dynamic_odds == "on" || (lottery_dynamic_odds != "on" && same_rateback_when_dynamic != "on"){
                                        continue
                                    }
                                }
                                
                                if txtName == "推广链接" {
                                    let lottery_dynamic_odds = sys.content.lottery_dynamic_odds
                                    let same_rateback_when_dynamic = sys.content.fixed_rate_model
                                    if (lottery_dynamic_odds != "on" && same_rateback_when_dynamic == "on"){
                                        continue
                                    }
                                }
                                
                                if txtName == "真人注单" {
                                    let onoff_zhen_ren_yu_le = sys.content.onoff_zhen_ren_yu_le
                                    if isEmptyString(str: onoff_zhen_ren_yu_le) || onoff_zhen_ren_yu_le == "off"{
                                        continue
                                    }
                                }
                                
                                if txtName == "棋牌注单"{
                                    let onoff_chess = sys.content.switch_chess
                                    if isEmptyString(str: onoff_chess) || onoff_chess == "off"{
                                        continue
                                    }
                                }
                                
                                if txtName == "电子注单" {
                                    let onoff_dian_zi_you_yi = sys.content.onoff_dian_zi_you_yi
                                    if isEmptyString(str: onoff_dian_zi_you_yi) || onoff_dian_zi_you_yi == "off"{
                                        continue
                                    }
                                }
                                
                                if txtName == "PT电子注单" {
                                    let onoff_dian_zi_you_yi = sys.content.onoff_dian_zi_you_yi
                                    let switch_pt_egame = sys.content.switch_pt_egame
                                    if isEmptyString(str: onoff_dian_zi_you_yi) || onoff_dian_zi_you_yi == "off" || switch_pt_egame == "off"{
                                        continue
                                    }
                                }
                                if txtName == "体育注单"{
                                    if sys.content.onoff_sport_switch == "off"{
                                        continue
                                    }
                                }
                                if txtName == "周周转运"{
                                    let onoff_week_luckly = sys.content.week_deficit_onoff
                                    if isEmptyString(str: onoff_week_luckly) || onoff_week_luckly == "off"{
                                        continue
                                    }
                                }
                                if txtName == "每日加奖"{
                                    let onoff_bouns_day = sys.content.one_bonus_onoff
                                    if isEmptyString(str: onoff_bouns_day) || onoff_bouns_day == "off"{
                                        continue
                                    }
                                }
                                
                                if txtName == "充值卡" || txtName == "充值卡记录"{
                                    let onoff_topup_card = sys.content.recharge_card_onoff
                                    if isEmptyString(str: onoff_topup_card) || onoff_topup_card == "off"{
                                        //关闭充值卡和充值卡记录选项
                                        continue
                                    }
                                }
                                
                                if txtName == "代金券"{
                                    let coupon_onoff = sys.content.coupons_onoff
                                    if isEmptyString(str: coupon_onoff) || coupon_onoff == "off"{
                                        //代金券开关
                                        continue
                                    }
                                }
                                
                                if txtName == "优惠活动大厅"{
                                    let activeHall = sys.content.onoff_application_active
                                    if activeHall == "off"{
                                        continue
                                    }
                                }
                            }
                        }
                        self.datas.append(result)
                    }
                    
                }
            }catch let error as NSError{
                print(error.localizedDescription)
            }
            DispatchQueue.main.async {
                self.gridview.reloadData()
            }
        }
    }

    //each item click method when response cell click
    @objc private func stackViewButtonsClickHandle(name: String) {
        switch name {
        case "真人注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 0
            self.navigationController?.pushViewController(page, animated: true)
        case "电子注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 1
            self.navigationController?.pushViewController(page, animated: true)
        case "PT电子注单":
            let vc = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let page = vc as! BetHistoryController
            page.viewControllerType = 1
            page.singlePT = true
            page.singlePlatformCode = "PT"
            self.navigationController?.pushViewController(page, animated: true)
        case "体育注单":
            let loginVC = UIStoryboard(name: "touzhu_record", bundle: nil).instantiateViewController(withIdentifier: "touzhuRecord")
            let recordPage = loginVC as! TouzhuRecordController
            recordPage.titleStr = name
            recordPage.recordType = MenuType.SPORT_RECORD
            self.navigationController?.pushViewController(recordPage, animated: true)
        case "棋牌注单":
            let loginVC = UIStoryboard(name: "betHistory_page", bundle: nil).instantiateViewController(withIdentifier: "betHistoryController")
            let recordPage = loginVC as! BetHistoryController
            recordPage.viewControllerType = 2
            self.navigationController?.pushViewController(recordPage, animated: true)
        case "额度转换":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            openConvertMoneyPage(controller: self)
            break
        case "额度转换记录":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            
            let vc = FeeConvertRecordController()
            vc.title = name
            self.navigationController?.pushViewController(vc, animated: true)
            
        case "积分兑换":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "score_change_page", bundle: nil).instantiateViewController(withIdentifier: "scoreExchange")
            let page = vc as! ScoreExchangeController
            self.navigationController?.pushViewController(page, animated: true)
//            openScoreChange(controller: self)
            break
        case "彩票注单":
            let vc = GoucaiQueryController()
            vc.isAttachInTabBar = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "追号查询":
            self.navigationController?.pushViewController(ZuihaoQueryController(), animated: true)
            break
        case "个人报表":

            if judgeIsMobileGuest() {
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(GerenReportController(), animated: true)
            break
        case "团队报表":
            self.navigationController?.pushViewController(TeamReportsController(), animated: true)
        case "打码量变动记录":
            self.navigationController?.pushViewController(CodeAmount(), animated: true)
            break
        case "账变报表":
            self.navigationController?.pushViewController(AccountChangeController(), animated: true)
            break
        case "充提记录":
            
            if judgeIsMobileGuest() {
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(TopupAndWithdrawRecords(), animated: true)
            break
        case "优惠活动":
            let vc = UIStoryboard(name: "active_page",bundle:nil).instantiateViewController(withIdentifier: "activePage") as! ActiveController
            vc.isAttachInTabBar = false
            vc.showDragBtnHandlerIS=false
            self.navigationController?.pushViewController(vc, animated: true)
        case "优惠活动大厅":
             openEventHall(controller: self)
            break
        case "用户资料":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(UserInfoController(), animated: true)
            break
        case "银行卡":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(BankCardListController(), animated: true)
        case "个人总览":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "geren_overview",bundle:nil).instantiateViewController(withIdentifier: "geren_overview") as! GerenTeamProfileController
            vc.fromTeam = false
            
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "密码修改":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(PasswordModifyController(), animated: true)
            break
        case "建议反馈":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            openFeedback(controller: self)
            break
        case "彩种信息":
            let vc = UIStoryboard(name: "lot_info_list",bundle:nil).instantiateViewController(withIdentifier: "lotInfo") as! LotInfoListController
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "彩种限额":
            let vc = UIStoryboard(name: "lot_fee_limit_page",bundle:nil).instantiateViewController(withIdentifier: "lotInfo") as! LotFeeLimitController
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "开奖结果":
            let vc = LotteryResultsController()
            vc.isAttachInTabBar = false
//            vc.code = "CQSSC"
            vc.code = DefaultLotteryNumber()
            vc.whichPage = "notRootVC"
            vc.showDragBtnHandlerIS=false;
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "走势图":
            let vc = LotteryResultsController()
            vc.isAttachInTabBar = false
//            vc.code = "CQSSC"
            vc.code = DefaultLotteryNumber()
            vc.showDragBtnHandlerIS=false;
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "团队总览":
            if YiboPreference.getAccountMode() == GUEST_TYPE && stationCode() != v023_ID {
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "geren_overview",bundle:nil).instantiateViewController(withIdentifier: "geren_overview") as! GerenTeamProfileController
            vc.fromTeam = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "用户列表":
//            if YiboPreference.getAccountMode() == GUEST_TYPE{
//                showToast(view: self.view, txt: "试玩账号不能进行此操作")
//                return
//            }
            if YiboPreference.getAccountMode() == AGENT_TYPE || YiboPreference.getAccountMode() == TOP_AGENT_TYPE{
                let vc = UIStoryboard(name: "user_list_page",bundle:nil).instantiateViewController(withIdentifier: "userList") as! UserListViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showToast(view: self.view, txt: tip_shiwan)
            }
            break
        case "我的推荐":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = TueijianViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "注册管理":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = UIStoryboard(name: "register_manager_page",bundle:nil).instantiateViewController(withIdentifier: "register_manager") as! RegisterManagerContrller
            vc.title = "注册管理"
            self.navigationController?.pushViewController(vc, animated: true)
//            self.navigationController?.pushViewController(RegistrationManagementController(), animated: true)
            break
        case "站内短信":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            self.navigationController?.pushViewController(InsideMessageController(), animated: true)
            break
        case "周周转运":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let weekLuckyCtrl = ProfileWeekLuckCtrl.init(nibName: "ProfileWeekLuckCtrl", bundle: nil)
            weekLuckyCtrl.title = name
            weekLuckyCtrl.prizeTypes = .week
            weekLuckyCtrl.url = NATIVEDEFICIT_PAGE_DATA
            self.navigationController?.pushViewController(weekLuckyCtrl, animated: true)
            break
        case "每日加奖":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let weekLuckyCtrl = ProfileWeekLuckCtrl.init(nibName: "ProfileWeekLuckCtrl", bundle: nil)
            weekLuckyCtrl.title = name
            weekLuckyCtrl.prizeTypes = .day
            weekLuckyCtrl.url = BONUS_PAGE_DATA
            self.navigationController?.pushViewController(weekLuckyCtrl, animated: true)
            break
        case "充值卡":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            //充值卡
           let topupCtrl =  TopUpCardController()
           self.navigationController?.pushViewController(topupCtrl, animated: true)
           
            break
        case "充值卡记录":
            //充值记录
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let topUpRecordCtrl = TopUpCardRecordController()
            self.navigationController?.pushViewController(topUpRecordCtrl, animated: true)
           
            break
        case "代金券":
            //充值记录
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let couponCtrl = MemberCouponController()
            self.navigationController?.pushViewController(couponCtrl, animated: true)
            
            break
        case "网站公告":
            let vc = UIStoryboard(name: "notice_page",bundle:nil).instantiateViewController(withIdentifier: "notice_page") as! NoticesPageController
            self.navigationController?.pushViewController(vc, animated: true)
        case "帮助中心":
            let url = String.init(format: "%@%@%@", BASE_URL,PORT, HELP_CENTER_URL)
            openActiveDetail(controller: self, title: "帮助中心", content: "", foreighUrl: url)
            break;
        case "推广链接":
            if YiboPreference.getAccountMode() == GUEST_TYPE{
                showToast(view: self.view, txt: tip_shiwan)
                return
            }
            let vc = SpreadVC()
            self.navigationController?.pushViewController(vc, animated: true)
            break;
        default:
            break
        }
    }
    func userHeaderView() -> UserHeaderView {
        if _userHeaderView == nil {
            _userHeaderView = UserHeaderView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: 260))
        }
        return _userHeaderView!
    }

}
//MARK:UICollectionViewDelegate && UICollectionViewDataSource
extension MemberPageController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    //创建头视图
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader{
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
            //添加头部视图
            header.addSubview(self.userHeaderView())
            return header
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: kScreenWidth, height: 264)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (kScreenWidth - 2.5) / 3, height: 100)
    }
    
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datas.count
    }
    //返回自定义的cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberCell", for: indexPath) as! MemberCell
        cell.tag = indexPath.row
        if indexPath.row + 1 > self.datas.count {
            return cell
        }
        cell.setupData(data: self.datas[indexPath.row])
        cell.messageNumber.text = String(self.messageNumber)
        if cell.titleLabel?.text == "站内短信" {
            cell.messageNumber.isHidden = false
            cell.messageNumber.isHidden = self.messageNumber == 0
        }else{
            cell.messageNumber.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row + 1 > self.datas.count {
            collectionView.reloadData()
            return
        }
        let data = self.datas[indexPath.row]
        self.stackViewButtonsClickHandle(name: data.txtName)
    }
}
