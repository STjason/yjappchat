//
//  AppDelegate.swift
//  SwiftSideslipLikeQQ
//
//  Created by JohnLui on 15/4/10.
//  Copyright (c) 2015年 com.lvwenhan. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import SocketIO
import AudioToolbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var isPresent:Bool = false //是否已经跳转一个
    var currentNavCtrl:UINavigationController?
    
    //当前界面支持的方向（默认情况下只能竖屏，不能横屏显示）
    var interfaceOrientations:UIInterfaceOrientationMask = .portrait{
        didSet{
            //强制设置成竖屏
            if interfaceOrientations == .portrait{
                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue,
                                          forKey: "orientation")
            }
                //强制设置成横屏
            else if !interfaceOrientations.contains(.portrait){
                UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue,
                                          forKey: "orientation")
            }
        }
    }
    
    //返回当前界面支持的旋转方向
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor
        window: UIWindow?)-> UIInterfaceOrientationMask {
        return interfaceOrientations
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let passWord = UserDefaults.standard.value(forKey: "passWord")
        // 新增手势锁判断
        if YiboPreference.getGESTURE_LOCK() == "true" && passWord != nil && YiboPreference.getGestureLockNetSwitch() == "on" && !judgeIsMobileGuest() && YiboPreference.getAccountMode() != GUEST_TYPE && YiboPreference.getLoginStatus() == true
        {
            enterUnlock(application: application)
        }
        else
        {
            self.normalAccess(application: application)
        }
        
        defaultHeader["isSimulator"] = "ios/" + isSimulator()
        defaultHeader["User-Agent"] = String.init(format: "ios/%@", getVerionName())
        
        return true
    }
    
    func enterUnlock(application: UIApplication)
    {
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
        
        let gesturelockVc = GestureLockMainViewController()
        gesturelockVc.isModify = false
        gesturelockVc.unlockSuccessBlock = {[weak self](status) in
            if status == .setSuccess || status == .checkSuccess || status == .modifySuccess {
                
            }else if status == .checkLoginOut
            {
                /// 登出当前账户
                YiboPreference.setToken(value: "" as AnyObject)
                YiboPreference.saveLoginStatus(value: false as AnyObject)
                YiboPreference.saveUserName(value: "" as AnyObject)
                YiboPreference.savePwd(value: "" as AnyObject)
                YiboPreference.setAccountMode(value: 0 as AnyObject)
                YiboPreference.setCACHEAVATARdata(value: Data.init())
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "startup_page")
            self?.window?.rootViewController = storyboard
            self?.normalAccess(application: application)
        }
        self.window?.rootViewController = gesturelockVc
    }
    
    func normalAccess(application: UIApplication)
    {
        
//        setupCrashReporting()
        
        ASAlamofireManager().setAlamofireHttps()
        
        IQKeyboardManager.shared.enable = false
        
        if UserDefaults.standard.string(forKey: "FLOAT_OPEN_PUSH") == nil {//推送开关
            UserDefaults.standard.set("open", forKey: "FLOAT_OPEN_PUSH")
        }
        
        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
        if UserDefaults.standard.bool(forKey: "everLaunch") == false {
            UserDefaults.standard.set(true, forKey: "everLaunch")
            YiboPreference.setPlayTouzhuVolume(value: true as AnyObject)
            YiboPreference.saveShakeTouzhuStatus(value: true as AnyObject)
            YiboPreference.savePwdState(value: true as AnyObject)
            YiboPreference.saveAutoLoginStatus(value: true)
            YiboPreference.setShouldOpenFloatTool(value: false as AnyObject)
            YiboPreference.saveMallStyle(value: OLD_CLASSIC_STYLE as AnyObject)
            YiboPreference.setYJFMode(value: YUAN_MODE as AnyObject)
        }
        
        YiboPreference.setChATVIEWONdata(value: true)
        YiboPreference.setIsChatBet(value: "off" as AnyObject)
        
        //注册本地推送
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge, .carPlay], completionHandler: { (granted, error) in
                if granted {
                    print("允許")
                } else {
                    print("不允許")
                }
            })
            // 代理 UNUserNotificationCenterDelegate，這麼做可讓 App 在前景狀態下收到通知
            UNUserNotificationCenter.current().delegate = self
        } else {
            application.registerUserNotificationSettings(UIUserNotificationSettings.init(types: [UIUserNotificationType.alert,UIUserNotificationType.badge,UIUserNotificationType.sound]
                , categories: nil))
        }
        application.applicationIconBadgeNumber = 0
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // 后台进入前台
//        let app = UIApplication.shared
//        app.applicationIconBadgeNumber = 0;
        
//        app.cancelLocalNotification(application)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if currentIsChat {
            isChatActive = true
        }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    
    //当应用进入后台时其作用
    func applicationDidEnterBackground(_ application: UIApplication) {
        isChatActive = false
        //开启一个后台任务
        //下面是声明的任务ID
        let task:UIBackgroundTaskIdentifier?
        task = application.beginBackgroundTask {
            if  YiboPreference.getLoginStatus() {
                guard let socket = socket else{return}
            }else{
                socket?.disconnect()
            }
        }
    }
    
    /**
     iOS9推送处理
     */
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if application.applicationState.hashValue == 1{
            let userInfo = notification.userInfo
            if let code = userInfo!["code"]{
                pushWith(code: code as! Int)
            }
        }
    }
    
    // 在前景收到通知時所觸發的 function
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if isChatActive {
            return
        }
        
        print("在前景收到通知...")
        if  let userInfo:[AnyHashable : Any] =  notification.request.content.userInfo {
            if let code = userInfo["code"]{
                let codeInt:Int = code as! Int
                if codeInt ==  KMESSAGENOTI{
                    var currentCtrl:UIViewController?
                    let currCtrl = currentVc() as? SlideContainerController
                    
                    for ctrl in (currCtrl?.children) ?? []{
                        print(ctrl)
                        if ctrl.classForCoder == RootTabBarViewController.classForCoder(){
                            if let currentTabbarCtrl = ctrl as? RootTabBarViewController{
                                let nav = currentTabbarCtrl.selectedViewController as? UINavigationController
                                currentCtrl = nav?.topViewController
                                currentNavCtrl = nav
                            }
                        }
                    }
                    let ctrlName = userInfo["ctrl"] as? String ?? ""
                    let currCtrlName = currentCtrl?.classForCoder ??  UIViewController().classForCoder
                    let currCtrlString = NSStringFromClass(currCtrlName)
                    if currCtrlString == ctrlName{
                        return
                    }else{
                        if socket == nil || CRDefaults.getChatroomMessageNoti() == false{
                            return
                        }
                        
                    }
                }
            }
        }
        
        completionHandler([.badge,.sound,.alert])
    }
    
    //获取当前页面的最顶层控制器
    func currentVc() -> UIViewController? {
        let keywindow = UIApplication.shared.keyWindow
        let firstView: UIView = (keywindow?.subviews.first)!
        let secondView: UIView = firstView.subviews.first!
        let vc = viewForController(view: secondView)
        return vc
    }
    
    func viewForController(view:UIView)->UIViewController?{
        var next:UIView? = view
        repeat{
            if let nextResponder = next?.next, nextResponder is UIViewController {
                return (nextResponder as! UIViewController)
            }
            next = next?.superview
        }while next != nil
        return nil
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if isChatActive {
            return
        }
        
        let userInfo:[AnyHashable : Any] = response.notification.request.content.userInfo
//        let categoryId = response.notification.request.content.categoryIdentifier
        if let code = userInfo["code"]{
            let codeInt:Int = code as! Int
            if codeInt == KMESSAGENOTI{
                
                if CRDefaults.getChatroomMessageNoti() == false || socket == nil{
                    return
                }
                //获取当前控制器
                var currentCtrl:UIViewController?
                let currCtrl = currentVc() as? SlideContainerController
                for ctrl in (currCtrl?.children) ?? []{
                    print(ctrl)
                    if ctrl.classForCoder == RootTabBarViewController.classForCoder(){
                        if let currentTabbarCtrl = ctrl as? RootTabBarViewController{
                            let nav = currentTabbarCtrl.selectedViewController as? UINavigationController
                            currentCtrl = nav?.topViewController
                            currentNavCtrl = nav
                        }
                    }
                }
                let ctrlName = userInfo["ctrl"] as? String ?? ""
                let currCtrlName = currentCtrl?.classForCoder ??  UIViewController().classForCoder
                let currCtrlString = NSStringFromClass(currCtrlName)
                if currCtrlString == ctrlName{
                    return
                }else{
                    if socket == nil || CRDefaults.getChatroomMessageNoti() == false{
                        return
                    }
                    
                }
                
                let roomId = userInfo["roomId"] as? String ?? ""
                let userId = userInfo["userId"] as? String ?? ""
                let stationId = userInfo["stationId"] as? String ?? ""
                
                let chatRoomCtrl = CRChatController()
                //房间roomId
                chatRoomCtrl.chat_roomId = roomId
                chatRoomCtrl.msgModel.stationId = stationId
                chatRoomCtrl.msgModel.userId = userId
                
                //私聊相关
                let isPrivateChat = userInfo["isPrivateChat"] as? Bool ?? false
                
                if isPrivateChat {
                    let passiveUserId = userInfo["passiveUserId"] as? String ?? ""
                    let userType = userInfo["userType"] as? String ?? ""
                    let nickName = userInfo["nickName"] as? String ?? ""
                    let account = userInfo["account"] as? String ?? ""
                    
                    let passiveUser = CROnLineUserModel()
                    passiveUser.id = passiveUserId
                    passiveUser.userType = userType
                    passiveUser.nickName = nickName
                    passiveUser.account = account
                    passiveUser.privatePermission = true
                    
                    chatRoomCtrl.passiveUser = passiveUser
                    chatRoomCtrl.isPrivateChat = true
                }
                
                //从通知进入房间
                chatRoomCtrl.isNotiFrom = true
                //消息室

                if currentNavCtrl?.topViewController?.classForCoder == chatRoomCtrl.classForCoder{
                    return
                }
                
                if currentNavCtrl != nil{
                    //隐藏多功能入口按钮
                    if let view = getSemicycleButton() {
                        view.isHidden = true
                    }
                      currentNavCtrl?.pushViewController(chatRoomCtrl, animated: true)
                }else{
//                    pushWith(code: codeInt)
                }
               
            }else if codeInt != NEW_VERSION_CODE{
                pushWith(code: code as! Int)
            }else{
                let url:String = userInfo["url"] as! String
                openBrower(urlString: url)
            }
        }
        completionHandler()
        //移除通知
        center.removeAllDeliveredNotifications()
    }
    
    func pushWith(code:Int){
        if code == UNREAD_MESSAGE_CODE{
            UIApplication.shared.applicationIconBadgeNumber = 0
            let controller = InsideMessageController()
            pushMessagePresend(present: controller)
        } else if code  == NEW_NOTICE_CODE{ //最新公告
            let noticesVc = UIStoryboard(name: "notice_page",bundle:nil).instantiateViewController(withIdentifier: "notice_page") as! NoticesPageController
            pushMessagePresend(present: noticesVc)
        } else if code == NEW_ACTIVE_ACTIVITY_CODE {//优惠活动
            let activeVc = UIStoryboard(name: "active_page",bundle:nil).instantiateViewController(withIdentifier: "activePage") as! ActiveController
            activeVc.isAttachInTabBar = false
            pushMessagePresend(present: activeVc)
        }else if code == KMESSAGENOTI{
            //通知消息
            if let nav = self.window?.rootViewController as? UINavigationController {
                nav.pushViewController(CRChatController(), animated: true)
            }
        }
    }
    /** 点击推送消息跳转函数 */
    func pushMessagePresend(present:BaseController) {
        let pushVc:SlideContainerController = (UIApplication.shared.keyWindow?.rootViewController) as! SlideContainerController
        if isPresent{
            pushVc.dismiss(animated: true) {
                let nav = UINavigationController.init(rootViewController: present)
                self.window?.rootViewController?.present(nav, animated: true, completion: nil)
                self.isPresent = true
            }
        }else{
            let nav = UINavigationController.init(rootViewController: present)
            self.window?.rootViewController?.present(nav, animated: true, completion: nil)
            isPresent = true
        }
    }
}

extension Data{
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}


extension AppDelegate {
//    func setupCrashReporting() {
//
//        let config = PLCrashReporterConfig.init(signalHandlerType: PLCrashReporterSignalHandlerType.mach, symbolicationStrategy: PLCrashReporterSymbolicationStrategy.all)
//
//        guard let crashReporter = (PLCrashReporter.init(configuration: config)) else {
//            return
//        }
//
//        if crashReporter.hasPendingCrashReport() {
//            handleCrashReport(crashReporter)
//        }
//
//        #if DEBUG
//        #else
//        if !crashReporter.enable() {
//            print("Could not enable crash reporter")
//        }
//        #endif
//    }
//
//    func handleCrashReport(_ crashReporter: PLCrashReporter) {
//        guard let crashData = try? crashReporter.loadPendingCrashReportDataAndReturnError(), let report = try? PLCrashReport(data: crashData), !report.isKind(of: NSNull.classForCoder()) else {
//            crashReporter.purgePendingCrashReport()
//            return
//        }
//
//        if let crash = PLCrashReportTextFormatter.stringValue(for: report, with: PLCrashReportTextFormatiOS) {
//            saveCrash(exceptionInfo: crash)
//            readAndUpdateCrashFiles()
//        }
//
//        crashReporter.purgePendingCrashReport()
//    }
    
}

