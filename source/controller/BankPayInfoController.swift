//
//  BankPayInfoController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Kingfisher
//银行支付信息填写

class BankPayInfoController: BaseController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var introducePics = [String]()
    var isSelect = false
    var banks:[BankPay] = []
    
    var gameDatas = [FakeBankBean]()
    var currentChannelIndex:Int = 0//绑定的银行卡通道列表
    var selectedBankWayIndex:Int = 0;//选择的转账类型列表
    var selectedBankWay:Int = 1
    
    var inputMoney = ""
    var inputDeposiName = ""
    var meminfo:Meminfo?
    var bankWays:[BankWayBean] = []
    var bankWanyNames:[String] = []
    var qrCodeStr = ""
    @IBOutlet weak var moneyLimitTV:UILabel!
    @IBOutlet weak var tablview:UITableView!
    @IBOutlet weak var confirmBtn:UIButton!
    
    @IBOutlet weak var topTableViewConstraint: NSLayoutConstraint!
    //备注
    var propmtString:String = ""
    var propmtLabel:UILabel?
    var indicateView:UIView?
    var onoff_payment_show_info = false
    
    //备注提示
    lazy var promptView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        indicateView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        indicateView!.layer.cornerRadius = 5
        indicateView!.layer.masksToBounds = true
        indicateView!.backgroundColor = UIColor.red
        //备注文字
        propmtLabel = UILabel()
        propmtLabel!.textColor = UIColor.white
        propmtLabel!.numberOfLines = 0
        propmtLabel!.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(propmtLabel!)
        view.addSubview(indicateView!)
        
        return view
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if tablview != nil {
            currentChannelIndex = 0
            updateContentWhenChannelChange(index: self.currentChannelIndex)
            tablview.reloadData()
        }
    }
    
    func tableHeaderViewLable()->UIView {
        var text = ""
        if let sysConfig = getSystemConfigFromJson()
        {
            if sysConfig.content != nil
            {
                text = sysConfig.content.pay_tips_deposit_general_tt
            }
        }
        
        if text.isEmpty {
            return UIView.init()
        }
        
        let viewf = UIView.init(frame: CGRect.init(x: 0, y: 0, width:0, height: 100))
        let lable = UILabel.initWith(text:"温馨提示：" + text, textColor:self.moneyLimitTV.textColor, fontSize: 14.0, fontName: 0)
        viewf.addSubview(lable)
        
        lable.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(5)
            make.height.greaterThanOrEqualTo(40)
        }
        lable.lineBreakMode = NSLineBreakMode.byCharWrapping
        lable.numberOfLines = 0
        
        return viewf
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                if config.content.onoff_payment_show_info == "off" {
                    onoff_payment_show_info = true

                }
            }
        }
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        self.title = "银行转账"
        
        depoistGuidePictures()
        
        tablview.delegate = self
        tablview.dataSource = self
        tablview.showsVerticalScrollIndicator = false
        tablview.tableHeaderView = self.createCollectionView()
        self.tablview.tableFooterView = tableHeaderViewLable()
        confirmBtn.addTarget(self, action: #selector(onCommitBtn(ui:)), for: .touchUpInside)
        confirmBtn.layer.cornerRadius = 5
        confirmBtn.theme_backgroundColor = "Global.themeColor"
        prepareBankWays()
    
        updateContentWhenChannelChange(index: self.currentChannelIndex)
        

    }
    
    
    private func createCollectionView() -> UIView {
        let row = self.banks.count / 3
        var collectionViewHeight: CGFloat = 0
        
        //是否存在二维码图片
        let bankModel = self.banks[currentChannelIndex]
        var qrCodeHeight:CGFloat = 0
        qrCodeHeight = !isEmptyString(str: bankModel.qrCodeImg) ? 180 : 0
        
        if onoff_payment_show_info {
            qrCodeHeight = 0
        }
        
        
        if row == 0 {
            collectionViewHeight = CGFloat(15) * CGFloat(2) +  CGFloat(40) + qrCodeHeight
        }else if self.banks.count > row * 3 {
            collectionViewHeight = CGFloat(row) * CGFloat(15) + CGFloat(15) * CGFloat(2) + (CGFloat(row) + CGFloat(1)) * CGFloat(40) + qrCodeHeight
        }else {
            collectionViewHeight = (CGFloat(row) - CGFloat(1)) * CGFloat(15) + CGFloat(15) * CGFloat(2)  + CGFloat(row) * CGFloat(40) + qrCodeHeight
        }
        
        let header = UIView.init(frame: CGRect(x: 0, y: 200, width: kScreenWidth, height: collectionViewHeight))
        
        //////////////////////////////////////////
        //扫描二维码 View
        let qrCodeView = UIView()
        qrCodeView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: qrCodeHeight)
        header.addSubview(qrCodeView)
        
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 25)
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "请扫描二维码"
        label.isHidden = qrCodeHeight == 0 ? true : false
        qrCodeView.addSubview(label)
        
        
        let qrCodeImg = UIImageView()
        qrCodeImg.contentMode = .scaleAspectFit
        // 180 - 25 = 155 为qrcodeimg高度
        qrCodeImg.frame = CGRect.init(x: (screenWidth - 155) * 0.5, y: 25, width: 155, height: 155)
        if let qrCodeUrl = URL.init(string: bankModel.qrCodeImg) {
            qrCodeStr = bankModel.qrCodeImg
            qrCodeImg.kf.setImage(with: ImageResource(downloadURL: qrCodeUrl), placeholder: UIImage.init(named: "default_placeholder_picture"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        qrCodeView.backgroundColor = UIColor.clear
        qrCodeView.addSubview(qrCodeImg)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressClick))
        qrCodeImg.isUserInteractionEnabled = true
        qrCodeImg.addGestureRecognizer(longPress)
        
        if onoff_payment_show_info {
           qrCodeImg.isHidden = true
        }
    
        
        //////////////////////////////////////////
        
//        let tipView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 40))
////        tipView.backgroundColor = UIColor.white
//        setupNoPictureAlphaBgView(view: tipView,alpha: 0.2)
//        header.addSubview(tipView)
//
//        let necessaryImg = UIImageView.init(frame: CGRect(x: 10, y: 0, width: 10, height: 40))
//        tipView.addSubview(necessaryImg)
//
//        let tipLable = UILabel.init(frame: CGRect(x: 15, y: 0, width: kScreenWidth -  15 * 2, height: 40))
//        tipView.addSubview(tipLable)
//        tipLable.text = "银行入款"
//        tipLable.textAlignment = .center
//        tipLable.textColor = UIColor.red
//        tipLable.font = UIFont.systemFont(ofSize: 16)
//
//        let tipSeparateLinel = UIView.init()
//        tipView.addSubview(tipSeparateLinel)
//        tipSeparateLinel.backgroundColor = UIColor.red
//        tipSeparateLinel.frame = CGRect.init(x: 0, y: 39, width: screenWidth, height: 1)
        
        let layout = UICollectionViewFlowLayout()
        
        let width = (kScreenWidth - CGFloat(60)) / CGFloat(3)
        layout.itemSize = CGSize(width: width, height: 40)
        layout.sectionInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        layout.minimumLineSpacing = 15
        layout.minimumInteritemSpacing = 15
        
        let colltionView = UICollectionView(frame: CGRect(x: 0, y: qrCodeHeight, width: kScreenWidth, height: collectionViewHeight), collectionViewLayout: layout)
//        colltionView.backgroundColor = UIColor.groupTableViewBackground
        setupNoPictureAlphaBgView(view: colltionView,alpha: 0.2)
        
        let nib = UINib(nibName: "NormalButtonCollectionCell", bundle: nil)
        colltionView.register(nib, forCellWithReuseIdentifier: "normalButtonCollectionCell")
        
        colltionView.delegate = self
        colltionView.dataSource = self
        
        header.addSubview(colltionView)
        return header
    }
    
    @objc func longPressClick(gesture:UIGestureRecognizer){
        if let qrcodeImg:UIImageView = gesture.view as? UIImageView {
            let alert = UIAlertController(title: "请选择", message: nil, preferredStyle: .actionSheet)
            let action = UIAlertAction.init(title: "保存到相册", style: .default, handler: {(action:UIAlertAction) in
                if qrcodeImg.image == nil{
                    return
                }
                UIImageWriteToSavedPhotosAlbum(qrcodeImg.image!, self, #selector(self.save_image(image:didFinishSavingWithError:contextInfo:)), nil)
            })
            
            let action2 = UIAlertAction.init(title: "识别二维码图片", style: .default, handler: {(action:UIAlertAction) in
                if qrcodeImg.image == nil {
                    return
                }
                UIImageWriteToSavedPhotosAlbum(qrcodeImg.image!, self, #selector(self.readQRcode(image:didFinishSavingWithError:contextInfo:)), nil)
            })
            
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(action2)
            
            alert.addAction(cancel)
            //ipad使用，不加ipad上会崩溃
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect.init(x: kScreenWidth/4, y: kScreenHeight, width: kScreenWidth/2, height: 300)
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func readQRcode(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer){
        
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy : CIDetectorAccuracyHigh])
        let imageCI = CIImage.init(image: image)
        let features = detector?.features(in: imageCI!)
        guard (features?.count)! > 0 else {
            showToast(view: self.view, txt: "二维码不正确,请联系客服")
            return
        }
        let feature = features?.first as? CIQRCodeFeature
        let qrMessage = feature?.messageString

        guard var code = qrMessage else{
            showToast(view: self.view, txt: "二维码不正确,请联系客服")
            return
        }
        if !isEmptyString(str: code){
            var appname = ""
            code = code.lowercased()
            if code.contains("weixin"){
                appname = "微信"
            }else if code.contains("alipay"){
                appname = "支付宝"
            }else{
                if let url = URL.init(string: code) {
                    openUrlWithBrowser(url: url, view: self.view)
                }else {
                    showToast(view: self.view, txt: "二维码不正确,请联系客服")
                }
                
                return
            }
            
            if error == nil {
                let ac = UIAlertController.init(title: "保存成功",
                                                message: String.init(format: "您可以打开%@,从相册选取并识别此二维码", appname), preferredStyle: .alert)
                ac.addAction(UIAlertAction(title:"去扫码",style: .default,handler: {(action:UIAlertAction) in
                    // 跳转扫一扫
                    if appname == "微信"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "weixin://")!){
                            openBrower(urlString: "weixin://")
                        }else{
                            showToast(view: self.view, txt: "您未安装微信，无法打开扫描")
                        }
                    }else if appname == "支付宝" || appname == "QQ" || appname == "云闪付"{
                        if UIApplication.shared.canOpenURL(URL.init(string: "alipay://")!){
                            openBrower(urlString: "alipay://")
                        }else{

                            showToast(view: self.view, txt: "您未安装\(appname)，无法打开扫描")
                        }
                    }
                }))
                self.present(ac, animated: true, completion: nil)
            } else {
                let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
        }else{
            showToast(view: self.view, txt: "请确认二维码图片是否正确的收款二维码")
            return
        }
       
    }
    
    //保存二维码
    @objc func save_image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            showToast(view: self.view, txt: "保存图片成功")
        } else {
            let ac = UIAlertController(title: "保存失败", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "好的", style: .default, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    //MARK: - 入款指南
    private func depoistGuidePictures() {
        requestDepositeGuidePictures(controller: self, bannerType: "7", success: {[weak self] (pictures) in
            if let weakSelf = self {
                if pictures.count > 0 {
                    weakSelf.introducePics = pictures
                    weakSelf.setupRightNavTitle(title: "存款指南")
                }
            }
        }) { (errorMsg) in
            
        }
    }
    
    func setupRightNavTitle(title:String) -> Void {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onRightMenuClick))
    }
    
    @objc func onRightMenuClick() -> Void {
        let pop = PagePicturePop.init(frame: .zero, urls: self.introducePics)
        pop.show()
    }
    
    
    //MARK: - <UICollectionViewDataSource,UICollectionViewDelegate>
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "normalButtonCollectionCell", for: indexPath) as! NormalButtonCollectionCell
        
        let data = self.banks[indexPath.row]
//        var fast = data.payName
        let name = data.receiveName.length > 0 ? data.receiveName:data.payName
        
        var fast = name
//        if isEmptyString(str: fast) {
//            fast = "没有名称"
//        }
        
//        cell.normalButton.setTitle(fast, for: .normal)
//        cell.normaltext.text = fast
        
        
        cell.normaltext.text = isEmptyString(str: fast) ? "没有名称" : fast
        cell.backgroundColor = UIColor.clear
     
//        cell.normaltext.snp.updateConstraints { (make) in
//            make.left.right.equalTo(0)
//        }
        
        
        if kScreenHeight <= 568 { //iphone5
            cell.normaltext.font = UIFont.systemFont(ofSize: 12.0)
        }else{
          if fast.length > 6 {
            cell.normaltext.font = UIFont.systemFont(ofSize: 10.0)
          }
        }
        
        
//        if !isEmptyString(str: data.icon)
//        {
//            if let urlP = URL.init(string: data.icon)
//            {
//                cell.normalButton.kf.setImage(with: ImageResource.init(downloadURL: urlP), for: .normal)
//                cell.normalButton.kf.setImage(with: ImageResource.init(downloadURL: urlP), for: .normal, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
//                    if error == nil && image != nil
//                    {
//                        cell.normalButton.setTitle(nil, for: .normal)
//                    }
//                }
//            }
//        }
        
        
        
        if currentChannelIndex == indexPath.row {
//            cell.normalButton.theme_setBackgroundImage("MemberPage.Charge.payMethedBgSelectedImage", forState: .normal)
            cell.normalV.isHidden = false
            cell.normaltext.layer.borderColor = UIColor.colorWithRGB(r: 236, g: 40, b: 41, alpha: 1).cgColor
        }else {
//            cell.normalButton.theme_setBackgroundImage("MemberPage.Charge.payMethedBgNormalImage", forState: .normal)
            cell.normaltext.layer.borderColor = UIColor.lightGray.cgColor
            cell.normalV.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "normalButtonCollectionCell", for: indexPath) as! NormalButtonCollectionCell
        //        cell.normalButton.setBackgroundImage(UIImage(named: ""), for: .normal)
        currentChannelIndex = indexPath.row
        selectedBankWay = 1
        selectedBankWayIndex = 0
        updateContentWhenChannelChange(index: indexPath.row)
        tablview.tableHeaderView = self.createCollectionView()
        collectionView.reloadData()
        //        self.tablview.reloadData()
    }
    
    //返回多少个组
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //返回多少个cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.banks.count
    }
    
    //点击更换通道后，更新新支付信息到view
    private func updateContentWhenChannelChange(index:Int){
        if self.banks.isEmpty{
            return
        }
        
        if !propmtString.isEmpty {
            propmtLabel?.text = propmtString
            let labeSize = propmtLabel?.sizeThatFits(CGSize.init(width: kScreenWidth - 40 - 20, height: CGFloat(MAXFLOAT)))
            let promptViewHeight = (labeSize?.height ?? 0) > CGFloat(50) ? labeSize?.height : 50
            view.addSubview(promptView)
            promptView.isHidden = false
            promptView.snp.remakeConstraints { (make) in
                make.top.equalTo(view).offset(KNavHeight)
                make.left.right.equalTo(view)
                make.height.equalTo(promptViewHeight ?? 0)
            }
            indicateView?.snp.remakeConstraints({ (make) in
                make.left.equalTo(promptView).offset(20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.width.height.equalTo(10)
            })
            propmtLabel?.snp.remakeConstraints { (make) in
                make.left.equalTo(indicateView!.snp.right).offset(10)
                make.right.equalTo(promptView).offset(-20)
                make.centerY.equalTo(promptView.snp.centerY)
                make.top.bottom.equalTo(promptView)
            }
            
            topTableViewConstraint.constant = promptViewHeight ?? 0
            
            
        }else{
            promptView.isHidden = true
            topTableViewConstraint.constant = 0
        }
        self.gameDatas.removeAll()
        let bank = self.banks[index]
        var tip = String.init(format: "温馨提示: 最低充值金额%d元，最大金额%d元;", bank.minFee,bank.maxFee)
        if !isEmptyString(str: bank.remark){
            tip = tip + "\n\n备注：\(bank.remark)";
        }
        moneyLimitTV.text = tip
        getFakeModels(bank: bank)
        self.tablview.reloadData()
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        if !propmtString.isEmpty {
            promptView.snp.updateConstraints { (make) in
                make.top.equalTo(view).offset(view.safeAreaInsets.top)
            }
        }
    }
    
    func prepareBankWays(){
        let item1 = BankWayBean()
        item1.name = "网银转账"
        item1.type = 1
        bankWays.append(item1)
        bankWanyNames.append(item1.name)
        
        let item2 = BankWayBean()
        item2.name = "ATM入款"
        item2.type = 2
        bankWays.append(item2)
        bankWanyNames.append(item2.name)
        
        let item3 = BankWayBean()
        item3.name = "银行柜台"
        item3.type = 3
        bankWays.append(item3)
        bankWanyNames.append(item3.name)
        
        let item4 = BankWayBean()
        item4.name = "手机转账"
        item4.type = 4
        bankWays.append(item4)
        bankWanyNames.append(item4.name)
        
        let item5 = BankWayBean()
        item5.name = "支付宝"
        item5.type = 5
        bankWays.append(item5)
        bankWanyNames.append(item5.name)
        
        let item6 = BankWayBean()
        item6.name = "微信"
        item6.type = 6
        bankWays.append(item6)
        bankWanyNames.append(item6.name)
    }
    
    func getFakeModels(bank:BankPay){
        
        let item3 = FakeBankBean()
        item3.text = "收款人名"
        item3.value = bank.receiveName
        gameDatas.append(item3)
        
        
        let item1 = FakeBankBean()
        item1.text = "收款通道"
        item1.value = String.init(format: "%@", bank.payName)
        gameDatas.append(item1)
        
        let item2 = FakeBankBean()
        item2.text = "收款账号"
        item2.value = bank.bankCard
        gameDatas.append(item2)
        
      
        
        let item4 = FakeBankBean()
        item4.text = "收款银行"
        item4.value = bank.payName
        gameDatas.append(item4)
        
        let item21 = FakeBankBean()
        item21.text = "银行地址"
        item21.value = bank.bankAddress
        gameDatas.append(item21)
        
        let item5 = FakeBankBean()
        item5.text = "转账类型"
        item5.value = self.bankWanyNames[selectedBankWayIndex]
        gameDatas.append(item5)
        
        let item6 = FakeBankBean()
        item6.text = "汇款人名"
        item6.value = ""
        gameDatas.append(item6)
        
        let item7 = FakeBankBean()
        item7.text = "存入金额"
        item7.value = ""
        gameDatas.append(item7)
        
        if bank.aliQrcodeStatus == "2" && !isEmptyString(str: bank.aliQrcodeLink){
            let item8 = FakeBankBean()
            item8.text = "支付宝转卡"
            item8.value = ""
            gameDatas.append(item8)
        }
    }
    
    
    @objc func onCommitBtn(ui:UIButton){
        
        let fast = self.banks[currentChannelIndex]
        
        if isEmptyString(str: self.inputMoney){
            showToast(view: self.view, txt: "请输入转账金额")
            return
        }
//        if !isPurnInt(string: self.inputMoney){
//            showToast(view: self.view, txt: "请输入整数金额")
//            return
//        }
        
        let minFee = fast.minFee
        let maxFee = fast.maxFee
        
        guard let money = Float(self.inputMoney) else {
            showToast(view: self.view, txt: "金额格式不正确,请重新输入")
            return
        }
        if money == 0 {
            showToast(view: self.view, txt: "充值金额不能为0")
            return
        }
        if money < Float(minFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能小于%d元", minFee))
            return
        }
        
        if money > Float(maxFee){
            showToast(view: self.view, txt: String.init(format: "充值金额不能大于%d元", maxFee))
            return
        }
        
        if isEmptyString(str: inputDeposiName){
            showToast(view: self.view, txt: "请输入汇款人名")
            return
        }
        
        let payId = fast.id
        let payCode = "bank"
        let amount = self.inputMoney
//        if let dn = meminfo?.account.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
//            depositName = dn as String
//        }
        let parameter = ["payCode": payCode,"amount":amount,"payId":payId,"depositName":inputDeposiName,"bankWay":selectedBankWay,"belongsBank":""] as [String : Any]
        
        self.request(frontDialog: true, method:.post, loadTextStr:"提交中...", url:API_SAVE_OFFLINE_CHARGE,params: parameter,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                        
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = OfflineChargeResultWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                showToast(view: self.view, txt: "提交成功")
                                if let meminfo = self.meminfo{
                                    var orderno = ""
                                    let account = meminfo.account
                                    let amount = self.inputMoney
                                    let payName = fast.payName
                                    
                                    let fycode = ""
                                    let remark = ""
                                    
                                    if let contents = getFieldWithJsonString(jsonString: resultJson) {
                                        orderno = contents
                                    }
                                    
                                    self.openConfirmPayController(orderNo: orderno, accountName: account, chargeMoney: amount, payMethodName: payName, receiveName: fast.receiveName, receiveAccount: fast.bankCard, dipositor: self.inputDeposiName, dipositorAccount: "", qrcodeUrl:self.qrCodeStr, payType: PAY_METHOD_BANK, payJson: "",remark:remark,fycode: fycode)
                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                                }
                                if (result.code == 0 || result.code == -1) {
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
                        
        })
    }
    
    func openConfirmPayController(orderNo:String,accountName:String,chargeMoney:String,
                                  payMethodName:String, receiveName:String,receiveAccount:String,dipositor:String,dipositorAccount:String,qrcodeUrl:String,payType:Int,payJson:String,
                                  remark:String="",fycode:String="") -> Void {
        if self.navigationController != nil{
            openConfirmPay(controller: self, orderNo: orderNo, accountName: accountName, chargeMoney: chargeMoney, payMethodName: payMethodName, receiveName: receiveName, receiveAccount: receiveAccount, dipositor: dipositor, dipositorAccount: dipositorAccount, qrcodeUrl: qrcodeUrl, payType: payType, payJson: payJson,
                           remark: remark,fycode: fycode)
        }
    }
    
    //MARK: 跳转支付宝
    private func gotoAlipay(url:URL) {
        if UIApplication.shared.canOpenURL(URL.init(string: "alipay://")!) {
            openUrlWithBrowser(url: url, view: self.view)
        }else {
            showToast(view: self.view, txt: "您未安装'支付宝'")
        }
    }
}

extension BankPayInfoController :UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
        }
        let model = self.gameDatas[indexPath.row]
        
        cell.configModel(model: model)
        
        cell.transferHandler = {() -> Void in
            let bank = self.banks[self.currentChannelIndex]
            if let url = URL.init(string: bank.aliQrcodeLink) {
                //跳转支付宝
                self.gotoAlipay(url: url)
            }else {
                showToast(view: self.view, txt: "支付信息不正确,请联系客服")
            }
        }
        
        cell.inputTV.delegate = self
        cell.inputTV.tag = indexPath.row
        if model.text.contains("金额") || model.text.contains("汇款人"){
            cell.inputTV.isHidden = false
            cell.valueTV.isHidden = true
            cell.inputTV.text = model.value
            cell.inputTV.placeholder = String.init(format: "请输入%@", model.text)
        }else{
            cell.inputTV.isHidden = true
            cell.valueTV.isHidden = false
            cell.valueTV.text = model.value
        }
        if model.text == "收款账号" || model.text == "收款人名" || model.text == "收款银行"{
            cell.copyBtn.isHidden = false
            cell.copyBtn.tag = indexPath.row
            cell.copyBtn.addTarget(self, action: #selector(onCopyBtn(ui:)), for: .touchUpInside)
        }else{
            cell.copyBtn.isHidden = true
        }
        
        if model.text.contains("金额"){
            cell.inputTV.keyboardType = .decimalPad
        }
//        if indexPath.row == 0 || indexPath.row == 5{
//            cell.accessoryType = .disclosureIndicator
//        }else{
//            cell.accessoryType = .none
//        }
        
        if model.text == "收款通道" || model.text == "转账类型"{
            cell.accessoryType = .disclosureIndicator
        }else{
            cell.accessoryType = .none
        }
        cell.textTV.text = model.text
        cell.inputTV.addTarget(self, action: #selector(onInput(ui:)), for: .editingChanged)
        return cell
    }
    
    @objc private func onCopyBtn(ui:UIButton){
        if self.gameDatas.isEmpty{
            return
        }
        let data = self.gameDatas[ui.tag]
        if !isEmptyString(str: data.value){
            UIPasteboard.general.string = data.value
            showToast(view: self.view, txt: "复制成功")
        }else{
            showToast(view: self.view, txt: "没有内容,无法复制")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.gameDatas[indexPath.row]
        if model.text == "收款通道"{
            return 0
        }else{
            return 44.0
        }
//        if indexPath.row == 0 {
//            return 0
//        }else {
//            return 44
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.gameDatas[indexPath.row]
        if (isSelect == false) {
            isSelect = true
            if model.text == "收款通道"{
                self.showChannelDialog()
            }else if model.text == "转账类型"{
                self.showBankWayDialog()
            }
//            if indexPath.row == 0{
//                self.showChannelDialog()
//            }else if indexPath.row == 5{
//                self.showBankWayDialog()
//            }
//            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.isSelect = false
            }
        }

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    @objc func onInput(ui:UITextField){
        let text = ui.text!
        self.gameDatas[ui.tag].value = text
        if ui.tag == 6{
            self.inputDeposiName = text
        }else if ui.tag == 7{
            self.inputMoney = text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    private func showChannelDialog(){
        
        var bankNames:[String] = []
        for bank in self.banks{
            bankNames.append(bank.payName)
        }
        
        let selectedView = LennySelectView(dataSource: bankNames, viewTitle: "请选择通道")
        selectedView.selectedIndex = self.currentChannelIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.currentChannelIndex = index
            self?.updateContentWhenChannelChange(index: index)
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
            //            self.setSelected(false, animated: true)
        }
    }
    
    private func showBankWayDialog(){
        let selectedView = LennySelectView(dataSource: self.bankWanyNames, viewTitle: "请选择转账类型")
        selectedView.selectedIndex = self.selectedBankWayIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.selectedBankWayIndex = index
            self?.selectedBankWay = (self?.bankWays[index].type)!
            self?.gameDatas[5].value = (self?.bankWanyNames[index])!
            self?.tablview.reloadData()
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
            //            self.setSelected(false, animated: true)
        }
    }
    
}
