//
//  LotteryPathView.swift
//  gameplay
//
//  Created by admin on 2018/11/2.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LotteryPathView: UIView {
    @IBOutlet weak var topLeftConstraintW: NSLayoutConstraint!
    @IBOutlet weak var topLeftView: UIView!
    @IBOutlet weak var topRightView: UIView!
    @IBOutlet weak var bottomView: UIView!
    var version_top_menu:SwiftPopMenu!
    var type_switch_datasources = [(icon:"",title:"第一球"),(icon:"",title:"第二球")]
    @IBAction func closeAction() {
        self.removeFromSuperview()
    }
    
    
    @IBAction func changeFatherTypeAction(_ sender: UIButton) {
        //frame 为整个popview相对整个屏幕的位置 arrowMargin ：指定箭头距离右边距离
        version_top_menu = SwiftPopMenu(frame:  CGRect(x: 5, y: (screenHeight - 240 - 60), width: 130, height: 100), arrowMargin: 105)
        version_top_menu.popData = type_switch_datasources
        //点击菜单事件
        version_top_menu.didSelectMenuBlock = { [weak self](index:Int)->Void in
            self?.version_top_menu.dismiss()
            self?.click_version_menu(index: index)
        }
        version_top_menu.show()
    }
    
    //MARK: 切换官方下注、信用下注
    func click_version_menu(index:Int) {
        
    }
    
    
    private var datas = [BcLotteryData]()
    
    lazy var xibView:UIView = {
        return Bundle.main.loadNibNamed("LotteryPathView", owner: self, options: nil)?.first as! UIView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibView.frame = bounds
        addSubview(xibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(self.xibView)
        xibView.frame = self.bounds
    }
    
    private func setupModelAndLogic(cpBianHao: String,index:Int,lotType:String)
    {
        let logic = LotteryResultsLogic.init(from: self)
        logic.recentLotteryResults(cpBianHao: cpBianHao, lastLotteryExcpHandler: { (tips) in
            
        }) { (datas) in
            self.datas = datas
        }
    }
    
    func configWithTitles(titles:[String],cpBianHao:String,lotType:String)
    {
        
    }
    
    private func getViewController() -> ASFormCollectionScrollController
    {
        let page = ASFormCollectionScrollController()
        
        return page
    }

}

