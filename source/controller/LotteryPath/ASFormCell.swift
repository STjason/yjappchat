//
//  ASFormCell.swift
//  ToutiaoPageViewDemo_shaw
//
//  Created by admin on 2018/10/26.
//

import UIKit

class ASFormCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderWidth = 0.25
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
    }

    
    func configTitle(title:String)
    {
        titleLabel.text = title
    }
}
