//
//  ASFormCollectionScrollController.swift
//  ToutiaoPageViewDemo_shaw
//
//  Created by admin on 2018/10/26.
//

// 监控 offset，滚动到临界位置，加载更多数据

import UIKit

class ASFormCollectionScrollController: BaseController {

    private var scrollView:UIScrollView!
    private var dataSourceItems = [FormatPathDataItem]()
    private var maxRowX = 0
    private var minRowX = 0
    private var tdHasValueNotes = [String]()
    private var hasChangeDirection = false
    private var pageIndex = 0
    private var lotType = ""
    private var pathLogic:LotteryPathLogic!
    
    private var datas = [BcLotteryData]() {
        didSet
        {
//            reloadBallNums(datas: datas, index: pageIndex,lotType:lotType)
            reloadBallParity(datas: datas, index: pageIndex,lotType:lotType)
            reloadItmesView()
        }
    }
    
    //MARK: -init
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor.clear
        setupScrollView()
    }
    
    //MARK: - life cycle
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
    }
    
    //MARK: public
    func setDatas(lotteryDatas:[BcLotteryData],index:Int,lotType:String)
    {
        resetDataAndView()
        pageIndex = index
        datas = lotteryDatas
        self.lotType = lotType
        
    }
    
    //MARK: -UI
    private func setupScrollView()
    {
        scrollView = UIScrollView.init()
        scrollView.backgroundColor = UIColor.clear
        scrollView.bounces = false
        scrollView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight - 44 * 2)
        self.view.addSubview(scrollView)
    }
    
    
    //MARK: - 路子逻辑
    private func resetDataAndView()
    {
        for subView in scrollView.subviews
        {
            subView.removeFromSuperview()
        }
        
        dataSourceItems = [FormatPathDataItem]()
        maxRowX = 0
        minRowX = 0
        tdHasValueNotes = [String]()
        pageIndex = 0
        hasChangeDirection = false
    }
    
    //MARK: 号码
    private func reloadBallNums(datas:[BcLotteryData],index:Int,lotType:String)
    {
        let lotteryAllNums = getNumsWithIndex(datas: datas,index: index)
        dataSourceItems = formatTilesWithModel(nums: lotteryAllNums)
    }
    
    //MARK: 单双
    private func reloadBallParity(datas:[BcLotteryData],index:Int,lotType:String)
    {
        let lotteryAllParities = getParityWithNums(datas: datas, index: index)
//        let lotteryAllParities = ["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2","2"]
        dataSourceItems = formatTilesWithModel(nums: lotteryAllParities)
    }
    
    //MARK: 获得单双数据
    private func getParityWithNums(datas:[BcLotteryData],index:Int) -> [String]
    {
        let lotteryAllNums = getNumsWithIndex(datas: datas,index: pageIndex)
        var nums = [String]()
        for (_,value) in lotteryAllNums.enumerated()
        {
            if let num = Int(value)
            {
                nums.append(num % 2 == 0 ? "双" : "单")
            }
        }
        
        return nums
    }
    
    /// 获取开奖结果中的，所有 index上的号码 的数组
    ///
    /// - Parameter index: 第index 个开奖号码的数组
    private func getNumsWithIndex(datas:[BcLotteryData],index:Int) -> [String]
    {
        var lotteryAllNums = [String]()
        for (_,value) in datas.enumerated()
        {
            let numStr = value.haoMa
            
            let Nums = numStr.components(separatedBy: ",")
            lotteryAllNums.append(Nums[index])
        }
        
        return lotteryAllNums
    }
    
    //路子显示的模型逻辑
    private func formatTilesWithModel(nums:[String]) -> [FormatPathDataItem]
    {
        var models = [FormatPathDataItem]()
        for (index,value) in nums.enumerated()
        {
            let model = FormatPathDataItem()
            model.title = value
            
            if index == 0
            {
                model.tdArray = [0,0]
                models.append(model)
                
            }else
            {
                let lastModel = models[index - 1]
                let td = lastModel.tdArray
                
                if td[1] == 0
                {
                    minRowX =  td[0]
                }
                
                if value == lastModel.title
                {
                    if td[1] == 5
                    {
                        model.tdArray = [td[0] + 1,5]
                    }else
                    {
                        if tdHasValueNotes.contains("\(td[0])\(td[1] + 1)")
                        {
                            hasChangeDirection = true
                            model.tdArray = [td[0] + 1,td[1]]
                        }else
                        {
                            if hasChangeDirection
                            {
                               model.tdArray = [td[0] + 1,td[1]]
                            }else
                            {
                               model.tdArray = [td[0],td[1] + 1]
                            }
                        }
                    }
                }else
                {
                    hasChangeDirection = false
                    model.tdArray = [minRowX + 1,0]
                }
                
                maxRowX = model.tdArray[0] > maxRowX ? model.tdArray[0] : maxRowX
                models.append(model)
                tdHasValueNotes.append("\(model.tdArray[0])\(model.tdArray[1])")
            }
        }
        
        return models
    }
    
    private func reloadItmesView()
    {
        scrollView.contentSize = CGSize.init(width: CGFloat(maxRowX) * 40, height: screenHeight - 44 * 2)
        
        let gap:CGFloat = 40
        let horiLinePointStart = CGPoint(x: 0, y: 0)
        let horiLinePointEnd = CGPoint(x: CGFloat(maxRowX) * gap, y: 0)
        let vertiLinePointEnd = CGPoint(x: 0, y: 6 * gap)
        
        ASDrawTool.drawHoriLines(count: 7, gap: gap, originalPoints: (horiLinePointStart,horiLinePointEnd), view: scrollView,lineColor: UIColor.lightGray)
        ASDrawTool.drawVerticalLines(count: maxRowX + 1, gap: gap, originalPoints: (horiLinePointStart,vertiLinePointEnd), view: scrollView,lineColor: UIColor.lightGray)

        setupItemsView(gap:gap)
    }
    
    private func setupItemsView(gap:CGFloat)
    {
        for (_,value) in dataSourceItems.enumerated()
        {
            let td = value.tdArray
            let x = 1 + CGFloat(td[0]) * gap
            let y = 1 + CGFloat(td[1]) * gap
            let label = UILabel.init(frame: CGRect.init(x: x, y: y, width: gap - 1, height: gap - 1))
            scrollView.addSubview(label)
            label.text = value.title
            label.textColor = UIColor.black
            label.font = UIFont.systemFont(ofSize: 14.0)
            label.textAlignment = .center
        }
    }
    
    //MARK: - others
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

class FormatPathDataItem: NSObject {
    var title = ""
    var tdArray = [-1,-1]
}





