//
//  LotteryResultsLogic.swift
//  gameplay
//
//  Created by admin on 2018/11/2.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class LotteryResultsLogic: NSObject {
    
    private var fromView:UIView = UIView()
    private var datas = [BcLotteryData]()
    
    convenience init(from:UIView) {
        self.init()
        fromView = from
    }
    
    func getDatas() -> [BcLotteryData] {
        return datas
    }
    
}

extension LotteryResultsLogic
{
    //MARK: 获取开奖结果
    func recentLotteryResults(cpBianHao:String,lastLotteryExcpHandler:@escaping (_ contents:String) -> (),updateLotteryHandler:@escaping (_ results:[BcLotteryData]) -> ()) -> Void {
        request(frontDialog: false, url:LOTTERY_LAST_RESULT_URL,params: ["lotCode":cpBianHao,"pageSize":50],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.fromView, txt: convertString(string: "获取最近开奖结果失败"))
                        }else{
                            showToast(view: self.fromView, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = LastResultWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            //更新开奖结果
                            if let results = result.content{
                                if results.count > 0
                                {
                                    //正在开发
                                    //正式时，要改为拼接
                                    self.datas = results
                                }
                            }
                            
                            updateLotteryHandler(self.datas)
                        }
                    }
        })
    }
    
    func request(frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                 callback:@escaping (_ resultJson:String,_ returnStatus:Bool)->()) -> Void {
        
        if !NetwordUtil.isNetworkValid(){
            showToast(view: self.fromView, txt: "网络连接不可用，请检测")
            return
        }
        
        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog {
                showToast(view: self.fromView, txt: loadTextStr)
            }
        }
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in
            
            callback(resultJson, returnStatus)
        }
        let baseUrl = BASE_URL + PORT
        
        requestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
    }
    
}
