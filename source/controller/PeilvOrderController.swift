//
//  PeilvOrderController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/16.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
import MBProgressHUD

class PeilvOrderController: BaseController,UITextFieldDelegate {
    
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var againBtn:UIButton!
    @IBOutlet weak var randomBtn:UIButton!
    
    @IBOutlet weak var tableviewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var clearBtn:UIButton!
    @IBOutlet weak var touzhuBtn:UIButton!
    @IBOutlet weak var fastMoneyInput:CustomFeildText!
    @IBOutlet weak var bottomViewV1: UIView!
    @IBOutlet weak var bottomViewV2: UIView!
    @IBOutlet weak var BetButton: UIButton!
    
    var loading:MBProgressHUD?
    var dealBetFlag = true //防止短时间内多次点击,true可点击，false不可点击
    var accountBanlance = 0.0
    var meminfo:Meminfo?
    var moneyTotal = 0
    var datas:[OrderDataInfo] = []
    var order:[OrderDataInfo]?
    var peilvs:[BcLotteryPlay]?
    var lhcLogic:LHCLogic2?
    
    var subPlayName = ""
    var subPlayCode = ""
    var cpTypeCode = ""
    var cpBianHao = ""
    var current_rate:Float = 0
    var cpName = ""
    var cpVersion = ""
    var lotteryicon = ""//icon
    var sectionModels = [[OrderDataInfo]]()
    var fromHistory = false //是否是历史注单页跳转来的
    
//    var actionButton: ActionButton!//悬浮按钮
    
    /** 注单数据分组处理 */
    private func handlesectionModels() {
        var sectionsModelDic = [String:[OrderDataInfo]]()

        for (_,data) in self.datas.enumerated() {
            if sectionsModelDic.keys.contains(data.subPlayName) {
                var newModels = sectionsModelDic["\(data.subPlayName)"]
                newModels?.append(data)
                sectionsModelDic["\(data.subPlayName)"] = newModels
            }else {
                sectionsModelDic["\(data.subPlayName)"] = [data]
            }
        }
        
        self.sectionModels.removeAll()
        
        for (_,value) in sectionsModelDic {
            sectionModels.append(value)
        }
    }
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
   
        topViewHeight.constant = fromHistory ? 0 : 60
        setupthemeBgView(view: self.view, alpha: 0)
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        self.navigationItem.title = String.init(format: "购彩列表(%@)", cpName)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.tableFooterView = UIView.init()
        clearBtn.layer.cornerRadius = 5

        touzhuBtn.layer.cornerRadius = 5
        BetButton.layer.cornerRadius = 5
        clearBtn.addTarget(self, action: #selector(onClearClick), for: .touchUpInside)
        touzhuBtn.addTarget(self, action: #selector(onTouzhuClick), for: .touchUpInside)
        BetButton.addTarget(self, action: #selector(onTouzhuClick), for: .touchUpInside)
        fastMoneyInput.addTarget(self, action: #selector(onFastMoneyInput(ui:)), for: .editingChanged)
        fastMoneyInput.delegate = self
//        actionButton = createCommonFAB(controller:self)
        
        againBtn.theme_setTitleColor("Global.themeColor", forState: .normal)
        randomBtn.theme_setTitleColor("Global.themeColor", forState: .normal)
        setupNoPictureAlphaBgView(view: againBtn)
        setupNoPictureAlphaBgView(view: randomBtn)
        againBtn.addTarget(self, action: #selector(onAgainBtn), for: .touchUpInside)
        randomBtn.addTarget(self, action: #selector(onRandomBtn), for: .touchUpInside)
        
        //当键盘弹起的时候会向系统发出一个通知，
        //这个时候需要注册一个监听器响应该通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        //当键盘收起的时候会向系统发出一个通知，
        //这个时候需要注册另外一个监听器响应该通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        let orderJson = YiboPreference.getTouzhuOrderJson()
        if !isEmptyString(str: orderJson){
            if let data = order{
                self.datas = self.datas + data
                let result:[OrderDataInfo] = JSONDeserializer<OrderDataInfo>.deserializeModelArrayFrom(json: orderJson)! as! [OrderDataInfo]
                for info in result{
//                    if !(info.numbers == info.numbers && info.subPlayCode == info.subPlayCode){
                        self.datas.append(info)
//                    }
                }
            }
        }else{
            if let data = order{
                self.datas = self.datas + data
            }
        }
        
        let tuples = filterRepeatData(datas: datas)
        let newDatas = tuples.0
        let filteredDatas = tuples.1
        if filteredDatas.count > 0 {
            showToast(view: self.view, txt: "已过滤 \(filteredDatas.count) 重复注单")
        }
        
        datas = newDatas
        
        handlesectionModels()
        tableview.reloadData()
    }
    
    /** 输入框代理方法 限制输入内容 */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location > 6{
            showToast(view: self.view, txt: "当前允许输入最大长度为7位")
            return false
        }
        return true
    }
    @objc override func onBackClick() {
        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
        self.sectionModels.removeAll()
        changeSectionModelsToModels()
        self.navigationController?.popViewController(animated: true)
    }
    
    //将二级model数组，转换为一级数组
    private func changeSectionModelsToModels() {
        var localModels:[OrderDataInfo] = []
        for (_,models) in self.sectionModels.enumerated() {
            for (_,model) in models.enumerated() {
                localModels.append(model)
            }
        }
        
        self.datas = localModels
    }
    
    @objc func onAgainBtn(){
//        onAgainBtnClick?(false)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onRandomBtn(){
        var isExist = false
        var betVC : BetPageController?
        for vc in navigationController?.children ?? []{
            if vc.isMember(of: BetPageController.self){
                betVC = vc as? BetPageController
                isExist = true
            }

        }
        if isExist{
            let bet = betVC!.getBetController()
            bet.calc_bet_orders_for_honest(datasAfterSelected: self.peilvs!) {
                self.addNewOrder()
            }
        }else{
            self.addNewOrder()
        }

    }
    
    //添加新注单
    func addNewOrder(){
        if let handle = self.lhcLogic{
            if let peilvs = self.peilvs{
                let orders = handle.randomBet(choosePlays: peilvs, orderCount: 1)
                if orders.isEmpty{
                    showToast(view: (self.view)!, txt: "没有机选出注单，请重试")
                    return
                }
                let data = fromPeilvBetOrder(peilv_orders: orders, subPlayName: self.subPlayName, subPlayCode: self.subPlayCode, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: self.current_rate)
                for d in data{
                    if let fastMoney = self.fastMoneyInput.text{
                        if !isEmptyString(str: fastMoney) && isPurnInt(string: fastMoney){
                            d.money = Double(fastMoney)!
                        }
                    }
                }
                self.datas = data + self.datas
                sectionModels.removeAll()
                self.handlesectionModels()
                self.tableview.reloadData()
            }
        }
    }
    
    //金额输入框内容变化回调
    @objc func onFastMoneyInput(ui:UITextField) -> Void {
        let moneyValue = ui.text!
        if isEmptyString(str: moneyValue){//当删完输入框全部数据后，页面显示为0
            for (_,models) in self.sectionModels.enumerated() {
                for (_,data) in models.enumerated() {
                    data.money = 0.001
                }
            }
            
            self.tableview.reloadData()
            return
        }
        if moneyValue.starts(with: "0") && moneyValue.count > 0 {
//            showToast(view: self.view, txt: "请输入正确格式的金额")
            fastMoneyInput.text = ""
            return
        }
        if moneyValue.length > 10 {
            showToast(view: (self.view)!, txt: "输入金额过大")
            fastMoneyInput.text = "99999999"
            return
        }
        
        for (_,models) in self.sectionModels.enumerated() {
            for (_,data) in models.enumerated() {
                data.money = Double(moneyValue)!
            }
        }
        
        self.tableview.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
//    func createCommonFAB(controller:UIViewController) -> ActionButton {
//        var items:[(title:String,imgname:String,action: (ActionButtonItem) -> Void)] = []
//        let manualItem = (title:"手选一注",imgname:"manual_bet_icon",action:{(item:ActionButtonItem)->Void in
//            self.navigationController?.popViewController(animated: true)
//        })
//        let randomItem = (title:"机选一注",imgname:"random_bet_icon",action:{(item:ActionButtonItem)->Void in
//
//
//        })
//        items.append(manualItem)
//        items.append(randomItem)
//        return showFAB(attachView: controller.view, items: items)
//    }
    
    @objc func onExitClick() -> Void {
        YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
        self.sectionModels.removeAll()
        changeSectionModelsToModels()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClearClick() -> Void {
        for (_,models) in self.sectionModels.enumerated() {
            for (_,data) in models.enumerated() {
                data.money = 0
            }
        }
        self.tableview.reloadData()
        self.navigationItem.rightBarButtonItems?.removeAll()
        self.fastMoneyInput.text = ""
    }
    
    @objc func onTouzhuClick() -> Void {
        if self.dealBetFlag {
            self.dealBetFlag = false
            
            self.accountWeb()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.dealBetFlag = true
            }
        }
    }
    
    private func betAction() {
        for (_,models) in self.sectionModels.enumerated() {
            for (_,data) in models.enumerated() {
                moneyTotal += Int(data.money)
            }
        }
        
        let accoundMode = YiboPreference.getAccountMode()
        if accountBanlance < Double(moneyTotal) && accoundMode != 4{
            openChargeMoney(controller: self, meminfo: self.meminfo)
            showToast(view: self.view, txt: "余额不足，请充值")
            return
        }
        
        if !judgeOrderMoney() {
            return
        }
        
        let data:(data:[PeilvOrder],rateback:Float) = formatData()
        
        if data.data.count == 0 {return}
        self.do_peilv_bet(models: data.data, rateback: data.rateback)
    }
    
    func accountWeb() -> Void {
        
        self.showDialog(view: self.view, loadText: "正在获取余额...")
        //帐户相关信息
        request(frontDialog: false, url:MEMINFO_URL,
                callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                    
                    if let weakSelf = self {
                        if !resultStatus {
                            showToast(view: weakSelf.view, txt: resultJson)
                            return
                        }
                        
                        if let result = MemInfoWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                if let memInfo = result.content{
                                    //更新余额等信息
                                    weakSelf.meminfo = memInfo
                                    
                                    if !isEmptyString(str: memInfo.balance){
                                        let leftMoneyName = "\(memInfo.balance)"
                                        if let value = Double(leftMoneyName) {
                                            weakSelf.accountBanlance = value
                                            weakSelf.betAction()
                                            return
                                        }
                                    }
                                }
                            }
                        }
                        
                        showToast(view: weakSelf.view, txt: "账户余额,获取失败")
                        weakSelf.hideDialog()
                    }
        })
    }
    
    
    /// 检查是否存在没有金额的号码
    ///
    /// - Returns: true都有金额，false存在没金额的号码
    private func judgeOrderMoney() -> Bool {
        for (_,models) in self.sectionModels.enumerated() {
            for (_,data) in models.enumerated() {
                if data.money == 0{
                    showToast(view: self.view, txt: "号码没有下注金额，请先输入金额")
                    return false
                }
            }
        }
        
        return true
    }
    
    private func formatData() -> (data:[PeilvOrder],rateback:Float) {
        
        self.datas = getDatasFromSectionDatas()
        
        let tuples = filterRepeatData(datas: self.datas)
        let newDatas = tuples.0
        let filteredDatas = tuples.1
        if filteredDatas.count > 0 {
            showToast(view: self.view, txt: "已过滤 \(filteredDatas.count) 重复注单")
        }
        
        datas = newDatas
        
        handlesectionModels()
        
        var peilvOrders:[PeilvOrder] = []
        
        var rateback:Float = 0
        for (_,models) in self.sectionModels.enumerated() {
            for (_,data) in models.enumerated() {
                if data.money == 0{
                    showToast(view: self.view, txt: "号码没有下注金额，请先输入金额")
                    return (data:peilvOrders,rateback:0)
                }
                
                let order = PeilvOrder()
                order.i = data.oddsCode
                order.c = data.numbers
                order.d = data.cpCode
                order.a = Float(data.money)
                
                rateback = Float(data.rate)
                peilvOrders.append(order)
            }
        }
        
        return (data:peilvOrders,rateback:rateback)
    }
    
    private func getDatasFromSectionDatas() -> [OrderDataInfo] {
        var datas = [OrderDataInfo]()
        
        for (_,models) in sectionModels.enumerated() {
            for (_,inModel) in models.enumerated() {
                datas.append(inModel)
            }
        }
        
        return datas
    }
    
    //MARK: 构造下注post数据
    private func buildPostData(models: [PeilvOrder]) -> [Dictionary<String,AnyObject>] {
        var bets = [Dictionary<String,AnyObject>]()
        for order in models{
            var bet = Dictionary<String,AnyObject>()
            bet["i"] = order.i as AnyObject
            bet["c"] = order.c as AnyObject
            bet["d"] = order.d as AnyObject
            bet["a"] = order.a as AnyObject
            bets.append(bet)
        }
        
        return bets
    }
    
    /*
     真正开始赔率下注
     @param order 下注注单
     @param rateback 用户选择的返水
     */
    func do_peilv_bet(models:[PeilvOrder],rateback:Float){
        if sectionModels.isEmpty{
            showToast(view: self.view, txt: "没有需要提交的订单，请先投注!")
            return
        }
        
        let bets = buildPostData(models: models)
        
        let postData = ["lotCode":self.cpBianHao,"data":bets,"kickback":rateback] as [String : Any]
        if (JSONSerialization.isValidJSONObject(postData)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: postData, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            //do bet
            request(frontDialog: true, method: .post, loadTextStr: "正在下注...", url:DO_PEILVBETS_URL,params: ["data":str!],
                    callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "下注失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = DoBetWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                
                                //把投注成功的结果记录在常用玩法的数据库
                                let record:VisitRecords = VisitRecords()
                                record.cpName = self.cpName //名字
                                record.czCode = self.cpTypeCode //类型
                                record.ago = "0" // 时间差 传默认值
                                record.cpBianHao = self.cpBianHao //编号
                                record.lotType = self.cpTypeCode //类型
                                record.lotVersion = self.cpVersion //版本
                                record.icon = self.lotteryicon //icon
                                CommonRecords.updateRecordInDB(record: record);
                                
                                self.showBetSuccessDialog()
                                YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
                                self.sectionModels.removeAll()
                                self.changeSectionModelsToModels()
                                self.tableview.reloadData()
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "下注失败"))
                                }
                                //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                                //所以此接口当code == 0时表示帐号被踢，或登录超时
                                if (result.code == 0) {
                                    loginWhenSessionInvalid(controller: self)
                                    return
                                }
                            }
                        }
            })
        }
        
    }
    
    func viewLotRecord(){
        let page = GoucaiQueryController()
//        page.filterLotCode = self.cpBianHao
        page.isAttachInTabBar = false
        self.navigationController?.pushViewController(page, animated: true)
    }
    
    func showBetSuccessDialog() -> Void {
        let alertController = UIAlertController(title: "温馨提示",
                                                message: "下注成功!", preferredStyle: .alert)
        let viewAction = UIAlertAction(title: "查看记录", style: .cancel, handler: {
            action in
            self.viewLotRecord()
        })
        let okAction = UIAlertAction(title: "继续下注", style: .default, handler: {
            action in
            self.navigationController?.popViewController(animated: false)
        })
        alertController.addAction(viewAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.sectionModels.isEmpty{
            YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
        }else{
            handlesectionModels()
            YiboPreference.saveTouzhuOrderJson(value: datas.toJSONString() as AnyObject)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        bottomViewV1.isHidden = !honest_betorder_page_fastmoney_switch()
        bottomViewV2.isHidden = !bottomViewV1.isHidden
    }
    
    //键盘弹起响应
    @objc override func keyboardWillShow(notification: NSNotification) {
        
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
  
            if keyBoardNeedLayout {
                tableviewTopConstraint.constant = deltaY
                UIView.animate(withDuration: duration, delay: 0.0,
                               options: UIView.AnimationOptions(rawValue: curve),
                               animations: {
                                self.view.frame = CGRect.init(x:0,y:-deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                                self.keyBoardNeedLayout = false
                                self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    //键盘隐藏响应
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
            
            tableviewTopConstraint.constant = 0
            UIView.animate(withDuration: duration, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            self.view.frame = CGRect.init(x:0,y:deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }

    override func showDialog(view:UIView,loadText:String) -> Void {
        loading = showLoadingDialog(view: view, loadingTxt: loadText)
    }
    
    override func hideDialog() -> Void {
        if loading != nil {
            hideLoadingDialog(hud: loading!)
        }
    }

}

extension PeilvOrderController : UITableViewDelegate,UITableViewDataSource,PeilvOrderDelegate{
    
    func onMoneyChange(money: Float, indexPath: IndexPath) {
        let data = self.sectionModels[indexPath.section][indexPath.row]
        data.money = Double(money)
        //
//        self.tableview.reloadRows(at: [IndexPath.init(row: row, section: 0)], with: .none)
    }
    
    func updateTableAction() {
//        self.tableview.reloadRows(at: [IndexPath.init(row: row, section: 0)], with: .none)
        self.tableview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = sectionModels[section][0].subPlayName
        return title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionModels[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "peilv_order_cell") as? PeilvOrderCell  else {
            fatalError("The dequeued cell is not an instance of PeilvOrderCell.")
        }
        cell.delegate = self
        
        if indexPath.row + 1 > self.datas.count {
            return cell
        }

        let data = sectionModels[indexPath.section][indexPath.row]
        cell.fromHistory = self.fromHistory
        cell.setData(data:data,indexPath: indexPath)
        cell.deleteUI.tag = indexPath.row
        cell.deleteUI.layer.cornerRadius = 15
//        cell.deleteUI.addTarget(self, action: #selector(onDelete(ui:)), for: .touchUpInside)
        
        cell.onDeleteHandler = {(indexpathP) in
            
            var models = self.sectionModels[indexpathP.section]
            models.remove(at: indexpathP.row)
            
            self.sectionModels.remove(at: indexpathP.section)
            if models.count > 0 {
                self.sectionModels.insert(models, at: indexpathP.section)
            }
            
            self.changeSectionModelsToModels()
            
            self.tableview.reloadData()
            if self.sectionModels.isEmpty{
                self.navigationItem.rightBarButtonItems?.removeAll()
                YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
