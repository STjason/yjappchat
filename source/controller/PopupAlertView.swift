//
//  PopupAlertView.swift
//  YiboGameIos
//
//  Created by ken on 2019/3/7.
//  Copyright © 2019 com.lvwenhan. All rights reserved.
//

import UIKit
extension String {
    /** 根据 正则表达式 截取字符串 - parameter regex: 正则表达式 - returns: 字符串数组 */
    public func matchesForRegex(regex: String) -> [String]? {
        do {
            let regularExpression = try NSRegularExpression(pattern: regex, options: [])
            let range = NSMakeRange(0,regex.count)
            let results = regularExpression.matches(in: self, options: [], range:range)
            let string = self as NSString
            return results.map { string.substring(with: $0.range)}
        } catch {
            return nil
        }
    }
}



class PopupAlertView: UIView,UIWebViewDelegate{
    var dataArray = [NoticeResult]()
    var dataModel:NoticeResult?
    var dataDic=[String:String]()
    var webis=true
    var messis = false
    //更新回调
    var updateCallClose: ((_ url:String) -> ())?
    //取消更新的回调
    var cancelUpdateCallColse:(() ->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        autoLayoutSubView()
        self.layer.shadowRadius = 10
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//         self.removeFromSuperview()
        clickBottomButton()
        
    }
  
    //#MARK: 赋值事件
    var assignmentDataArray:[NoticeResult]{
        get{
            return self.dataArray
        }
        set{
            self.cancelBtn.addTarget(self, action: #selector(clickBottomButton), for: .touchUpInside)
            self.dataArray = newValue
            if self.dataArray.count != 0 {
                self.dataModel = self.dataArray[0]
                self.titleLabel.text = self.dataModel?.title
                if self.dataModel?.content != nil {
                    let myAppend = "<style>"
                    let myAppend3 = "body{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}"
                    let myAppendend = "</style>"
                    let msgNew = (self.dataModel?.content)! + myAppend + myAppend3 + myAppendend
                    let loadStr = String(format: "<head><style>img{width:%f !important;height:auto}</style></head>%@", kScreenWidth*0.75,msgNew)
                    self.imageView.loadHTMLString(loadStr, baseURL: URL.init(string: BASE_URL))
                }
            }
        }
    }

    var assignmentData:[String:String]{
        get{
            return self.dataDic
        }
        set{
            self.dataDic = newValue
            let version = self.dataDic["version"] as! String
            let content = "版本号:\(version);\(self.dataDic["content"] as! String)"
            self.messis = true
            self.titleLabel.text="发现新版本"
            self.imageView.isHidden=true
            self.backgroundColor = UIColor.colorWithRGB(r: 0, g: 0, b: 0, alpha: 0.3)
            self.cancelBtn .setTitle("去更新", for: UIControl.State.normal)
            self.cancelUdBtn.isHidden=false
            self.lableView.isHidden=false
            let contentRep=content.replacingOccurrences(of: ";", with: "\n")
            self.lableView.text=contentRep
            self.cancelBtn.addTarget(self, action: #selector(upAction), for: .touchUpInside)
            //自适应高
            let separatedByCount = content.components(separatedBy: ";")
            var Count = 0
            for element in separatedByCount {
                print(element.lengthOfBytes(using: String.Encoding.utf8))
                let ttt = element.lengthOfBytes(using: String.Encoding.utf8) / 40
                if ttt == 0 {
                    Count += 1
                }else{
                    Count += ttt
                }
            }
            self.bgView.snp.updateConstraints{ (make) in
                let heights = 150+(25*Count) //15
                if kScreenHeight-150 >= CGFloat.init(heights){
                    make.height.equalTo(heights)
                }else{
                    make.height.equalTo(kScreenHeight*0.75)
                }
            }
        }
    }
    
    @objc func upAction() {
        if let url = self.dataDic["url"] {
              updateCallClose?(url)
//            if let ulll = URL.init(string: url){
//                updateCallClose?(ulll)
//                UIApplication.shared.openURL(ulll)
//            }
        }
        
        self.removeFromSuperview()
    }


    //#MARK: 业务事件
    
    //#MARK: 点击事件
    /**
     点击按钮调用代理方法
     1 不再弹出按钮  2 取消按钮
     */
    @objc func clickBottomButton(){
          cancelUpdateCallColse?()
        self.removeFromSuperview()
      
    }
    

    //#MARK: 约束
    func autoLayoutSubView (){
      
        self.bgView.snp.makeConstraints { (make) in
            make.center.equalTo(self.snp.center).offset(0)
            make.height.equalTo(0) //2
            make.width.equalTo(kScreenWidth-40) //20
        }
        
        self.titleLabel.snp.makeConstraints {(make) in
            make.left.right.top.equalTo(0)
            make.height.equalTo(60)
        }
        
        self.cancelBtn.snp.makeConstraints { (make) in
            make.width.equalTo(60)
            make.right.equalTo(self.bgView).offset(-10)
            make.bottom.equalTo(self.bgView).offset(-10)
            make.height.equalTo(30)
        }

        self.cancelUdBtn.snp.makeConstraints { (make) in
//            make.width.equalTo(60)
//            make.left.equalTo(self.bgView).offset(10)
//            make.bottom.equalTo(self.bgView).offset(-10)
//            make.height.equalTo(30)
            
            make.width.equalTo(60)
            make.right.equalTo(self.bgView).offset(-70)
            make.bottom.equalTo(self.bgView).offset(-10)
            make.height.equalTo(30)
        }
        
        self.imageView.snp.updateConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
            make.left.equalTo(10)
            make.right.equalTo(self.bgView).offset(-10)
            make.bottom.equalTo(self.cancelBtn.snp.top).offset(-10)
        }

       self.lableView.snp.updateConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
            make.left.equalTo(10)
            make.right.equalTo(self.bgView).offset(-10)
            make.bottom.equalTo(self.cancelBtn.snp.top).offset(-10)
        }
    }
    
  
    /** 背景视图 */
    lazy var bgView : UIView = {
        let view = UIView.init(frame: .zero)
        view.backgroundColor = UIColor.white
        view.clipsToBounds = true
        view.layer.cornerRadius = 5
        self.addSubview(view)
        return view
    }()
    
    /** 标题 */
    lazy var titleLabel : UILabel = {
        let title = UILabel.initWith(text: "平台公告", textColor: UIColor.black, fontSize: 18, fontName: 1)
        title.textAlignment = .center
//        title.theme_backgroundColor = "Global.themeColor"
//        title.textColor=UIColor.white
        title.theme_textColor = "Global.themeColor"
        self.bgView.addSubview(title)
        return title
    }()
    
    /** 内容webView */
    lazy var imageView : UIWebView = {
        let web = UIWebView.init(frame: .zero)
        web.delegate = self;
        web.scrollView.bounces = false
        web.sizeThatFits(web.frame.size)
        self.bgView.addSubview(web)
        return web
    }()
    
    /** 更新view内容视图 */
    lazy var lableView : UITextView = {
        let lable = UITextView.init(frame: .zero)
        lable.font=UIFont.systemFont(ofSize: 14)
        lable.isEditable=false
        lable.isHidden=true
        self.bgView.addSubview(lable)
        return lable
    }()
  
    /** 取消按钮 */
    lazy var cancelBtn : UIButton = {
//        let btn = UIButton.initWith(title: "确定", titleColot: UIColor.white, fontSize: 14, self, action: #selector(clickBottomButton));
        let btn = UIButton.init()
        btn.setTitle("确定", for: UIControl.State.normal)
        btn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        btn.titleLabel?.font=UIFont.boldSystemFont(ofSize: 14)
        btn.setTitleColor(UIColor.colorWithRGB(r: 0, g: 121, b: 107, alpha: 1), for: UIControl.State.normal)
        self.bgView.addSubview(btn)
        return btn
    }()

    /** 更新的取消按钮 */
    lazy var cancelUdBtn : UIButton = {
        let btn = UIButton.initWith(title: "取消", titleColot: UIColor.white, fontSize: 14, self, action: #selector(clickBottomButton));
        btn.isHidden=true
        btn.setTitleColor(UIColor.colorWithRGB(r: 0, g: 121, b: 107, alpha: 1), for: UIControl.State.normal)
        self.bgView.addSubview(btn)
        return btn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
 
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webis {
            let webHeightStr = webView.stringByEvaluatingJavaScript(from: "document.body.offsetHeight")
            let webHeight = CGFloat((webHeightStr! as NSString).floatValue)
            print(webHeight)
            print(kScreenHeight)
            webis=false
            self.bgView.snp.updateConstraints{ (make) in
                if webHeight >= 300 {
                    make.height.equalTo(kScreenHeight*0.75) 
                }else{
                    make.height.equalTo(webHeight+120)
                }
                make.width.equalTo(kScreenWidth*0.75)
            }
            self.imageView.snp.updateConstraints { (make) in
                make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
                make.left.equalTo(10)
                make.right.equalTo(self.bgView).offset(-10)
                make.bottom.equalTo(self.cancelBtn.snp.top).offset(-10)
            }
            self.backgroundColor = UIColor.colorWithRGB(r: 0, g: 0, b: 0, alpha: 0.3)
            return ;
        }
        
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        let Request:String = request.url!.absoluteString
        let replacing1 = Request.replacingOccurrences(of: "/", with: "")
        let replacing2 = BASE_URL.replacingOccurrences(of: "/", with: "")
        if replacing1 != replacing2 {
            NotificationCenter.default.post(name: NSNotification.Name("Request_String"), object:self, userInfo:["Request_String":Request])
            return false
        }
        return true
    }
}
