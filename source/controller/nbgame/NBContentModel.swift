//
//  NBContentModel.swift
//  gameplay
//
//  Created by William on 2018/9/16.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class NBContentModel: HandyJSON {
    var buttonImagePath:String!
    var displayName:String!
    var displayEnName:String!
    var img:String!
    var lapisId:String!
    var name:String!
    var url:String!
    var forwardUrl:String = ""
    
    required init(){}
}
