//
//  NBModel.swift
//  gameplay
//
//  Created by William on 2018/9/16.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class NBModel: HandyJSON {
    var success = false
    var accessToken:String!
    var content:[gameDetailItem]?
    var msg = ""
    var code:Int = 0
    
    required init(){}
}

class gameDetailItem:HandyJSON{
    //棋牌会有
    var forwardUrl:String = ""
    var img:String = ""
    var lapisId:String = ""
    var name:String = ""
    var single:String = ""
    //电子游戏
    var buttonImagePath:String = ""
    var displayName:String = ""
 
    required init(){}
}
