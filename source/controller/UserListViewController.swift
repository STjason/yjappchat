//
//  UserListViewController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import MJRefresh
//用户列表

class UserListViewController: BaseController {
    
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var content:UITableView!
    @IBOutlet weak var titleViewTopConstraint: NSLayoutConstraint!
    
    //////////////////////
//    var confirmHandler:((_ userName:String,_ minimumBalace: String,_ maxBalance:String,_ startTime: String,_ endTime: String,_ showAllLevel:Bool) -> Void)?
    var filterUsername = ""
    var include = false
    var filterMinBalance = ""
    var maxBalance = ""
    var startTime = ""
    var endTime = ""
    
//    isLoadMoreData: Bool,showDialog:Bool = true,include: Bool = false,pageSize: Int = 60,filterUsername:String = "",filterMinBalance:String = "",maxBalance:String = "",startTime:String = "",endTime:String = "")
    //////////////////////
    
    var datePickerView: NewCustomDatePicker!
    
    let filterViewHeight: CGFloat = 220
    var datas:[UserListSmallBean] = []
    var pageNumber = 1
    var filterView: UserlistFilterView!
    
    var dataRows = [UserListSmallBean]()
    
    let refreshHeader = MJRefreshNormalHeader()
    let refreshFooter = MJRefreshBackNormalFooter()
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        setupRightNavBarItem()
        
        setupthemeBgView(view: self.view, alpha: 0)
        setupNoPictureAlphaBgView(view: self.titleView,alpha: 0.4,bgViewColor: "FrostedGlass.subColorImageCoverDarkColorForNotGlass")
        
        self.title = "用户列表"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        content.delegate = self
        content.dataSource = self
        content.tableFooterView = UIView()
        content.separatorStyle = .none
        setupRefreshView()
        self.loadAllDatas(isLoadMoreData: false)
    }
    
    private func setupRightNavBarItem() {
        let button = UIButton(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        
        button.setTitle("筛选", for: .normal)
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(rightBarButtonItemAction(button:)), for: .touchUpInside)
        button.theme_setTitleColor("Global.barTextColor", forState: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
        
        self.setupFilterView()
        self.setUpDatePickerView()
    }
    
    //MARK: -   设置条件选择视图 、 日期选择视图
    private func setUpDatePickerView() {
        
        self.datePickerView = Bundle.main.loadNibNamed("NewCustomDatePicker", owner: self, options: nil)?.last as! NewCustomDatePicker
        self.datePickerView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.datePickerView.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.view.addSubview(self.datePickerView)
        self.view.bringSubviewToFront( self.datePickerView)
        
        self.datePickerView.cancelHandler = {() -> Void in
            self.showDatePickerView(show: false)
        }
        
        self.datePickerView.confirmHandler = {(timeString) -> Void in
            
            self.showDatePickerView(show: false)
            if self.datePickerView.isStartDate {
                self.filterView.loginStartTimeButton.setTitle("\(timeString)", for: .normal)
            }else {
                self.filterView.loginEndTimeButton.setTitle("\(timeString)", for: .normal)
            }
        }
    }
    
    private func setupFilterView() {
        self.filterView = Bundle.main.loadNibNamed("UserlistFilterView", owner: self, options: nil)?.last as! UserlistFilterView
        
        self.filterView.cancelHandler = {() -> Void in
            self.hideAndRemoveFilterView()
        }
        
        self.filterView.confirmHandler = {(userName, minimumBalanceContents, maxBalanceContents, loginStartTime, loginEndTime, showAllLevel) -> Void in

            print("(userName, minimumBalanceContents, maxBalanceContents, loginStartTime, loginEndTime, showAllLevel) = \(userName), \(minimumBalanceContents), \(maxBalanceContents), \(loginStartTime), \(loginEndTime), \(showAllLevel) ")
            
            self.include = showAllLevel
            self.filterUsername = userName
            self.filterMinBalance = minimumBalanceContents
            self.maxBalance = maxBalanceContents
            self.startTime = loginStartTime
            self.endTime = loginEndTime
            
            self.loadAllDatas(isLoadMoreData: false, showDialog: true, include: showAllLevel, filterUsername: userName, filterMinBalance: minimumBalanceContents, maxBalance: maxBalanceContents, startTime: loginStartTime, endTime: loginEndTime)
            
            self.hideAndRemoveFilterView()
        }
        
        self.filterView.allLevelHandler = {() -> Void in
            
        }
        
        self.filterView.loginEndHandler = {() -> Void in
            self.datePickerView.isStartDate = false
            self.showDatePickerView(show: true)
        }
        
        self.filterView.loginStartHandler = {() -> Void in
            self.datePickerView.isStartDate = true
            self.showDatePickerView(show: true)
        }
    }
    
    @objc private func rightBarButtonItemAction(button: UIButton) {
        
        if self.filterView.isShowing
        {
            hideAndRemoveFilterView()
        }else
        {
            showFilterView()
        }
    }
    
    //MARK: 显示、隐藏 条件视图、日期选择器
    private func showDatePickerView(show: Bool) {
        self.datePickerView.isHidden = !show
    }
    
    private func showFilterView() {
        self.filterView.isShowing = true
        self.view.addSubview(filterView)
        self.filterView.frame = CGRect.init(x: 0, y: glt_iphoneX ?  24 : -60 + 64, width: screenWidth, height: self.filterViewHeight)
        
        UIView.animate(withDuration: 0.3) {
            self.titleViewTopConstraint.constant = self.filterViewHeight
            self.filterView.alpha = 1.0
            self.filterView.frame = CGRect.init(x: 0, y: glt_iphoneX ? 88 : 64, width: screenWidth, height: self.filterViewHeight)
        }
    }

    private func hideAndRemoveFilterView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.titleViewTopConstraint.constant = 0
            self.filterView.alpha = 0.2
            self.filterView.frame = CGRect.init(x: 0, y: glt_iphoneX ? 24 : -60 + 64, width: screenWidth, height: self.filterViewHeight)
        }) { (_) in
//            self.filterView.clearData()
            self.filterView.isShowing = false
            self.filterView.removeFromSuperview()
        }
    }
}

extension UserListViewController {
    
    func loadAllDatas(isLoadMoreData: Bool,showDialog:Bool = true,include: Bool = false,pageSize: Int = 60,filterUsername:String = "",filterMinBalance:String = "",maxBalance:String = "",startTime:String = "",endTime:String = "") -> Void {
        
        var pageNumber = 1
        if isLoadMoreData {
            if self.dataRows.count % pageSize == 0 {
                pageNumber = self.dataRows.count / pageSize + 1
            }else {
                noMoreDataStatusRefresh()
                return
            }
        }
        
        let params = ["include":include,"username":filterUsername,"minBalance":filterMinBalance,"maxBalance":maxBalance,
                      "start":startTime,"end":endTime,"pageNumber":pageNumber,"pageSize":pageSize] as [String : Any]
        
        request(frontDialog: showDialog, method:.get, loadTextStr:"获取中...", url:API_USERLISTDATA,params:params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    self.endRefresh()
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = UserListBeanWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if result.content != nil{
                                self.datas = (result.content?.rows)!
                                
                                guard let contentP = result.content else {return}
                                guard let rowsP = contentP.rows else {return}
                                if !isLoadMoreData {
                                    self.dataRows.removeAll()
                                }
                                self.dataRows += rowsP
                                self.noMoreDataStatusRefresh(noMoreData: rowsP.count % pageSize != 0 || rowsP.count == 0)
                                self.content.reloadData()
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
                    
        })
    }
    
    //MARK: - 刷新
    private func setupRefreshView() {
        refreshHeader.setRefreshingTarget(self, refreshingAction: #selector(headerRefresh))
        refreshFooter.setRefreshingTarget(self, refreshingAction: #selector(footerRefresh))
        self.content.mj_header = refreshHeader
        self.content.mj_footer = refreshFooter
    }
    
    @objc fileprivate func headerRefresh() {
//        self.loadAllDatas(isLoadMoreData: false)
        self.loadAllDatas(isLoadMoreData: false, showDialog: true, include: self.include, filterUsername: self.filterUsername, filterMinBalance: self.filterMinBalance, maxBalance: self.maxBalance, startTime: self.startTime, endTime: self.endTime)
    }
    
    @objc fileprivate func footerRefresh() {
        self.loadAllDatas(isLoadMoreData: true)
    }
    
    private func endRefresh() {
        self.content.mj_header?.endRefreshing()
        self.content.mj_footer?.endRefreshing()
    }
    
    private func noMoreDataStatusRefresh(noMoreData: Bool = true) {
        if noMoreData {
            self.content.mj_footer?.endRefreshingWithNoMoreData()
        }else {
            self.content.mj_footer?.resetNoMoreData()
        }
    }
    
}

extension UserListViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.datas.count
        return self.dataRows.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getSwitch_lottery() ? 243 : 243 - 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "newUserListCell") as? NewUserListCell  else {
            fatalError("The dequeued cell is not an instance of newUserListCell.")
        }
        
        cell.rateBackConsH.constant = getSwitch_lottery() ? 45 : 0
        
        cell.selectionStyle = .none
        cell.index = indexPath.row
        
        let modelP = self.dataRows[indexPath.row]
        cell.setModel(model: modelP)
        
        cell.teamOVerView = {(index) in
            let vc = UIStoryboard(name: "geren_overview",bundle:nil).instantiateViewController(withIdentifier: "geren_overview") as! GerenTeamProfileController
            vc.fromTeam = true
            let data = self.datas[index]
            vc.accountId = data.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.accountChange = {(index) in
            let data = self.datas[index]
            let vc = AccountChangeController()
            vc.filterUserName = data.username
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.memberTransformProxy = {(index) in
            let vc = UIStoryboard(name: "register_manager_page",bundle:nil).instantiateViewController(withIdentifier: "register_manager") as! RegisterManagerContrller
            vc.userName = self.dataRows[index].username
            vc.accountId = "\(self.dataRows[index].id)"
            vc.title = "会员转成代理"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.rebateSetup = {(index) in
            
            if let config = getSystemConfigFromJson(){
                if config.content != nil{
                    let fix_mode = config.content.fixed_rate_model
                    let cp_dynamic_switch = config.content.lottery_dynamic_odds
                    //固定模式，且下级返点一致时，不永旭设定返点
                    if fix_mode == "on" && cp_dynamic_switch != "on"{
                        showToast(view: self.view, txt: "固定返点模式时，不可以再设置返点")
                        return
                    }
                }
            }
            
            let vc = UIStoryboard.init(name: "RatebackViewController", bundle: nil).instantiateViewController(withIdentifier: "ratebackViewController") as! RatebackViewController
            
            let modelP = self.dataRows[indexPath.row]
            if modelP.type == AGENT_TYPE{
                vc.isProxy = true
            }else if modelP.type == MEMBER_TYPE{
                vc.isProxy = false
            }else if modelP.type == TOP_AGENT_TYPE{
                vc.isProxy = true
            }else if modelP.type == GUEST_TYPE{
                vc.isProxy = false
            }
            
            vc.accountId = modelP.id
            vc.userName = modelP.username
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.userDetail = {(index) in
            self.onItemClick(datasoure: self.formSource(row: index))
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.separatorInset = UIEdgeInsets.zero
//        cell.layoutMargins = UIEdgeInsets.zero
//    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        onItemClick(datasoure: self.formSource(row: indexPath.row))
//    }
    
    private func formSource(row:Int) -> [String]{

        let content = self.dataRows[row]
        var sources = [String]()
        let t1 = String.init(format: "用户名称: %@", content.username)
        let t2 = String.init(format: "余额: %@", String.init(format: "%.2f元", content.money))
        let t3 = String.init(format: "返点级别: %@%@", String.init(format: "%.2f", content.kickback),"‰")
        let t4 = String.init(format: "用户等级: %@", getUserType(t: content.type))
        let t5 = String.init(format: "注册时间: %@", content.createDatetime)
        let t6 = String.init(format: "最后登录时间: %@", content.lastLoginDatetime)
        sources.append(t1)
        sources.append(t2)
        sources.append(t3)
        

        if content.type == AGENT_TYPE || content.type == TOP_AGENT_TYPE {
            let t7 = String(format: "电子返点: %@‰", content.egameScale)
            let t8 = String(format: "体育返点: %@‰", content.sportScale)
            let t9 = String(format: "沙巴体育返点: %@‰", content.shabaSportScale)
            let t10 = String(format: "真人返点: %@‰", content.realScale)
            let t11 = String(format: "棋牌返点: %@‰", content.chessScale)
            let t12 = String(format: "电竞返点:%@‰",content.esportScale)
            let t13 = String(format: "捕鱼返点:%@‰",content.fishingScale)
            
            sources.append(t7)
            sources.append(t8)
            sources.append(t9)
            sources.append(t10)
            sources.append(t11)
            sources.append(t12)
            sources.append(t13)
            
        }
        
        sources.append(t4)
        sources.append(t5)
        sources.append(t6)
        return sources
    }
    
    @objc func onItemClick(datasoure:[String]){
        if datasoure.isEmpty{
            return
        }
        
        let dialog = DetailListDialog(dataSource: datasoure, viewTitle: "用户详情")
        dialog.selectedIndex = 0
        //        randomView.didSelected = {
        //        }
        self.view.window?.addSubview(dialog)
        dialog.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(dialog.kHeight)
        dialog.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            dialog.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    @objc func clickTeamBtn(ui:UIButton){
        let vc = UIStoryboard(name: "geren_overview",bundle:nil).instantiateViewController(withIdentifier: "geren_overview") as! GerenTeamProfileController
        vc.fromTeam = true
        let data = self.datas[ui.tag]
        vc.accountId = data.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickAccountCBtn(ui:UIButton){
        let data = self.datas[ui.tag]
        let vc = AccountChangeController()
        vc.filterUserName = data.username
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
