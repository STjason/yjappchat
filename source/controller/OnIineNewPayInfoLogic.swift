//
//  OnIineNewPayInfoLogic.swift
//  gameplay
//
//  Created by admin on 2018/10/6.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class OnIineNewPayInfoLogic {
    var payMethodExpandIsUp = true
    var payPathExpandIsUp = true
    let payMethodCellH:CGFloat = 35
    let payPathCellW:CGFloat = 70
    var payMethodSelectedIndex = 0 {
        didSet {
            payPathSelectedIdnex = 0
            fixedAmmoutSelectedIndex = 0
            payPathExpandIsUp = true
            
            fixedAmmouts = getFixedAmmounts(index: payMethodSelectedIndex)
            seletedFixedAmount = fixedAmmouts.count > 0 ? fixedAmmouts[0] : ""
        }
    }
    
    var seletedFixedAmount = ""
    var payPathSelectedIdnex = 0
    var fixedAmmoutSelectedIndex = 0 {
        didSet {
            fixedAmmouts = getFixedAmmounts(index: payMethodSelectedIndex)
            seletedFixedAmount = fixedAmmouts.count > 0 ? fixedAmmouts[fixedAmmoutSelectedIndex] : ""
        }
    }
    
    /** mini height for payMethodCollectionView */
    static let payMethodMiniH:CGFloat = 35 + 10 + 10 //cell高度，section的上间距,cell的间距
    static let fixedAmountMiniH:CGFloat = 0
    
    var onlines:[OnlinePay] = []
    var onlinesFuncFirst:[OnlineFunFirstPay] = []
    var payPathDatas:[String] = []
    var fixedAmmouts:[String] = []
    
    /** insetForSectionAt for  payMethodCollectionView */
    static let cellSectionpadding = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
    
    /** layout for payMethodCollectionView  */
    static func getPayMethodLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        return layout
    }
    
    /**
     * funcFirst:true 先支付方法再支付通道，false 则反之
     * itemSize for payMethodCollectionView */
    func sizeForPayMethodCollection(index: IndexPath,funcFirst:Bool = false) -> CGSize {
        var title = ""
        if !funcFirst {
            let model = onlines[index.row]
            title = !isEmptyString(str: model.payAlias) ? model.payAlias : model.payName
        }else {
            let model = onlinesFuncFirst[index.row]
            title = !isEmptyString(str: model.payAlias) ? model.payAlias : model.payName
        }
        
        let width = String.getStringWidth(str: title, strFont: 14.0, h: payMethodCellH)
        return CGSize.init(width: width + 10, height: payMethodCellH)
    }
    
    /** itemSize for payPathCollectionView */
    func sizeForPayPathCollection(index: IndexPath) -> CGSize {
        return CGSize.init(width: payPathCellW, height: payMethodCellH)
    }
    
    func sizeForFixedAmountColletion(index: IndexPath) -> CGSize {
        let title = fixedAmmouts[index.row]
        var width = String.getStringWidth(str: title, strFont: 14.0, h: payMethodCellH) + 10
        width = width > 50 ? width : 50
        return CGSize.init(width: width, height: payMethodCellH)
    }
    
    /** datas of fixed ammount */
    func getFixedAmmounts(index:Int,funcFirst:Bool = false) -> [String] {
        var moneys = ""
        if !funcFirst {
            moneys = onlines[index].fixedAmount
        }else {
            moneys = onlinesFuncFirst[index].fixedAmount
        }
        if !isEmptyString(str: moneys){
            let moneyArr = moneys.components(separatedBy: ",")
            return moneyArr
        }
        
        return [String]()
    }
    
    /** get onLine pay tips */
    static func getPayTips() -> String {
        if let sysConfig = getSystemConfigFromJson()
        {
            if sysConfig.content != nil
            {
                return sysConfig.content.pay_tips_deposit_third
            }
        }
        return ""
    }
    
    /** 返回温馨提示 */
    func getWarmTips(index:Int,funcFirst:Bool = false) -> String {
        if !funcFirst {
            if onlines.count > 0 {
                return String.init(format: "最低充值金额%d元，最大金额%d元", onlines[index].minFee,onlines[index].maxFee)
            }
        }else {
            if onlinesFuncFirst.count > 0 {
                return String.init(format: "最低充值金额%d元，最大金额%d元", onlinesFuncFirst[index].min,onlinesFuncFirst[index].max)
            }
        }
        
        return ""
    }

    
    /** 返回浏览器提示 */
    static func getBrowerTips() -> String? {
        if shouldShowBrowsersChooseView()
        {
            var tips = "由于不同第三方支付的浏览器限制造成无法正常支付，请尝试切换其他浏览器来进行支付；您也可在设置中清除您选择的默认的支付浏览器，若有其他疑问，请联系客服。"
            if let config = getSystemConfigFromJson(){
                if config.content != nil{
                    let tipsContents = config.content.tip_for_multi_browser_pay
                    if !isEmptyString(str: tipsContents) {
//                        tips = tipsContents
                        tips = "温馨提示：" + tipsContents
                    }
                }
            }
            return tips
        }
        return nil
    }
    
    /** get onLine pay tips */
    static func getOnlinePayTips() -> String {
        if let sysConfig = getSystemConfigFromJson()
        {
            if sysConfig.content != nil
            {
                return sysConfig.content.pay_tips_deposit_general_tt
            }
        }
        return ""
    }
    
    
    static func getEmptyTipsView(tips:String) -> UILabel {
        let tipsLabel = UILabel()
        tipsLabel.text = tips
        tipsLabel.textAlignment = .center
        tipsLabel.textColor = UIColor.black
        tipsLabel.font = UIFont.systemFont(ofSize: 17.0)
        tipsLabel.backgroundColor = UIColor.clear
        return tipsLabel
    }
}
