//
//  SemicircleMenuController.swift
//
//  Created by Moayad on 5/29/16.
//  Copyright © 2016 Moayad. All rights reserved.
//  环形菜单，没有使用 数据库，数据管理很混乱，这一块有时间必须要改

import UIKit

class SemicircleMenuController: UIView,UICollectionViewDataSource, UICollectionViewDelegate {
    
    // 1:左边，2右边
    let CellTypeLeft = 1
    let CellTypeRight = 2
    var cellType = 0
    
    var hideSemicircleHandler:(() -> Void)? // 隐藏环形按钮触发
    
    var thumbnailCache = [String: UIImage]()
    var dialLayout:SemicircleMenuLayout!
    var cell_height:CGFloat!
    var collectionView:UICollectionView!
    var items: [(String,String)] = []
    var visualView: UIVisualEffectView!
    weak var holdVC:RootTabBarViewController!
    var allDatas = [LotteryData]()
    var realPerson:[LotteryData] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    
    private func setupView() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(clickSpaceView))
        tapGesture.cancelsTouchesInView = false
        self.addGestureRecognizer(tapGesture)
        
        setupBlurView()
        setupCollectionView()
    }
    
    @objc func clickSpaceView(sender: UITapGestureRecognizer){
        
        if let indexPath = self.collectionView?.indexPathForItem(at: sender.location(in: self.collectionView)) {
//            let cell = self.collectionView?.cellForItem(at: indexPath)
            
            let name = self.items[indexPath.row].0
            openVCwithName(name: name)
            
        }
        
        self.isHidden = true
        
        hideSemicircleHandler?()
    }
    
    private func setupBlurView() {
        let blurEffectView = UIBlurEffect.init(style: .dark)
        visualView = UIVisualEffectView.init(effect: blurEffectView)
        let frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        visualView.frame = frame
        self.addSubview(visualView)
    }
    
    func setupCollectionView(){
        
        dialLayout = SemicircleMenuLayout()
        dialLayout.cellSize = CGSize(width: 180, height: 50)
        dialLayout.shouldFlip = true
        dialLayout.dialRadius = 130
        dialLayout.angularSpacing = 23
        dialLayout.xOffset = 130
        dialLayout.itemHeight = 120
        
        let collectoinFrame = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
        collectionView = UICollectionView.init(frame: collectoinFrame, collectionViewLayout: dialLayout)
        collectionView.backgroundColor = UIColor.white.withAlphaComponent(0)
        self.addSubview(collectionView)
        
        let cellNib = UINib.init(nibName: "SemicircleMenuCell", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "semicircleMenuCell")
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.hideItemTitle()
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SemicircleMenuCell!
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "semicircleMenuCell", for: indexPath) as! SemicircleMenuCell
        
        cell.rightIcon.isHidden = cellType == CellTypeLeft
        cell.rightLabel.isHidden = cellType == CellTypeLeft
        cell.icon.isHidden = cellType == CellTypeRight
        cell.label.isHidden = cellType == CellTypeRight
        
        let item = self.items[indexPath.item]
        cell.configWithItem(titleAndImage: item)
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hideItemTitle()
    }
    
     func hideItemTitle() {
        let cells = self.collectionView.visibleCells
        for (_, cell) in cells.enumerated()
        {
            guard let cellP = cell as? SemicircleMenuCell else
            {
                fatalError("fatalError log 'dequeueReusableCell as? CircularMenuCell failure'")
            }
            
            cellP.label.isHidden = (cellP.frame.origin.x < -(cellP.width * 0.15) || cellType == CellTypeRight)
            cellP.rightLabel.isHidden = (cellP.frame.origin.x > (screenWidth - cellP.width * 0.85) || cellType == CellTypeLeft)
        }
    }
    //浮窗工具
    private func openVCwithName(name: String) {
        
       guard let vc = UIApplication.topViewController(controller: self.holdVC)
        else {
            showToast(view: self, txt: "当前页面不支持跳转")
            return
        }
        let isLogined = YiboPreference.getLoginStatus()
        let notFilterArray = getSwitch_lottery() ? ["扫码下注","优惠活动","App下载","网站公告","在线客服","开奖结果","游戏大厅"] : ["优惠活动","App下载","网站公告","在线客服","游戏大厅"]
        
        if !notFilterArray.contains(name) && !isLogined
        {
            showToast(view: self, txt: "请登录，以使用该功能")
            return
        }
        
        if name == "聊天室"{
            /// 规避侧边栏被展示
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showCenterNotiActionName"), object: nil)
            if switch_chatRoomOn()
            {
                if switch_nativeOrWapChat() {
                    if !YiboPreference.getLoginStatus(){
                        showToast(view: self, txt: "请先登录再使用")
                        return
                    }
                    kCheckLoginStatus(vc: vc)
                }else {
                    vc.navigationController?.pushViewController(ChatViewController(), animated: true)
                }
            }
            
        }
        else if name == "扫码下注"{
            if switch_scanBetOn()
            {
                openScanPage(controller: vc)
            }else{
                showToast(view: vc.view, txt: "扫码下注开关没有打开，请联系客服")
            }
        }
        else if name == "充值"{
            self.getChargeInfo = {(meminfo:Meminfo) -> Void in
                openChargeMoney(controller: vc, meminfo: meminfo)
            }
            
            accountWeb(from: "charge")
        }else if name == "App下载"{
            openAPPDownloadController(controller: vc)
        }else if name == "投注"{
            self.gotoBet = {(lotCode: String,isOffical:Int) -> Void in
                self.clickBet(lotCode: lotCode, isOffical: isOffical,fromVC: vc)
            }
            
            handleBettingData()
        }else if name == "在线客服"{
            openContactUs(controller: vc)
        }else if name == "优惠活动"{
            openActiveController(controller: vc)
        }else if name == "提款"{
            self.getWithdrawInfo = {(meminfo:Meminfo) -> Void in
                openPickMoney(controller: vc, meminfo: meminfo)
            }
            
            accountWeb(from: "withdraw")
        }else if name == "开奖结果"{
            let vcP = LotteryResultsController()
            vcP.isAttachInTabBar = false
//          vcP.code = "CQSSC"
            vcP.code = DefaultLotteryNumber()
            vcP.whichPage = "notRootVC"
            vcP.showDragBtnHandlerIS=false;
            vc.navigationController?.pushViewController(vcP, animated: true)
            
        }else if name == "购彩记录"{
            let vcP = GoucaiQueryController()
            vcP.isAttachInTabBar = false
            vc.navigationController?.pushViewController(vcP, animated: true)
        }else if name == "皇冠体育"{
            let vcP = UIStoryboard(name: "new_sport_page", bundle:nil).instantiateViewController(withIdentifier: "sport")
            let sport = vcP as! NewSportController
            vc.navigationController?.pushViewController(sport, animated: true)
        }else if name == "YG棋牌(NB)"{
            let vcP = UIStoryboard(name: "innner_game_list", bundle: nil).instantiateViewController(withIdentifier: "gameList") as! GameListController
            vcP.gameCode = "nb"
            vc.navigationController?.pushViewController(vcP, animated: true)
        }else if name == "抢红包"{
            if YiboPreference.getLoginStatus() == false {//未登陆则跳到登陆页面
                loginWhenSessionInvalid(controller: vc)
                return
            }
            
            if let sys = getSystemConfigFromJson() {
                if sys.content.rob_redpacket_version == "v1" {
                    let vcP = RedPackageViewController()
                    vc.navigationController?.pushViewController(vcP, animated: true)
                    return
                }
            }
            let vcp = gunRedEnvelopeViewController.init(nibName: "gunRedEnvelopeViewController", bundle: nil);
            vc.present(vcp, animated: true, completion: nil)
            
        }else if name == "大转盘"{
            openBigPanPage(controller: vc)
        }else if name == "积分兑换"{
            openScoreExchangePage(controller: vc)
        }else if name == "站内信"{
            let vcP = InsideMessageController()
            vc.navigationController?.pushViewController(vcP, animated: true)
        }else if name == "网站公告"{
            let vcP = UIStoryboard(name: "notice_page",bundle:nil).instantiateViewController(withIdentifier: "notice_page") as! NoticesPageController
            vc.navigationController?.pushViewController(vcP, animated: true)
        }else if name == "签到"{
            let nav = UINavigationController.init(rootViewController: CalenderController2())
            vc.present(nav, animated: true, completion: nil)
        }
        else if name == "添加快捷方式"{
            let vcP = UIStoryboard.init(name: "AddFastEntryMenu", bundle: nil).instantiateViewController(withIdentifier: "addFastEntryViewController") as? AddFastEntryViewController
            vc.navigationController?.pushViewController(vcP!, animated: true)
            
        }else if name == "游戏大厅"{
            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                NotificationCenter.default.post(name: NSNotification.Name( rawValue: "signOutToFirstTabNoti"), object: nil)
//            }
            YiboPreference.setChATVIEWONdata(value: false) //禁止首页再次初始化聊天
            let moduleStyle = YiboPreference.getMallStyle()
            let (xibname,_) = selectMainStyleByModuleID(styleID: moduleStyle)
            
            let vc0 = UIStoryboard(name: xibname,bundle:nil).instantiateViewController(withIdentifier: "new_home")
            vc.navigationController?.pushViewController(vc0, animated: true)
        }else if name == "AG真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "ag")
        }else if name == "BBIN真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "bbin")
        }else if name == "DS真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "ds")
        }else if name == "OG真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "og")
        }else if name == "MG真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "mg")
        }else if name == "AB真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "ab")
        }else if name == "BG真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "bgzr")
        }else if name == "DG真人"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "dg")
        }else if name == "性感赌场(NT)"{
            forwardReal(controller: BaseController(), requestCode: 0, playCode: "ntzr")
        }else if name == "皇家真人"{
           forwardReal(controller: BaseController(), requestCode: 0, playCode: "rgzr")
        }else if name == "EBET真人"{
           forwardReal(controller: BaseController(), requestCode: 0, playCode: "ebet")
        }else {
            showToast(view: holdVC.view, txt: "点击了\(name)")
        }
    }

    //MARK: - 跳转数据处理回调
    // 充值
    var getChargeInfo:((Meminfo) -> Void)?
    // 提款
    var getWithdrawInfo:((Meminfo) -> Void)?
    // 投注,Int为1：官方，2信用
    var gotoBet:((String,Int) -> Void)?
}

//MARK: 跳转数据处理 && 点击事件
extension SemicircleMenuController {
    
    
    func selectMainStyleByModuleID(styleID:String) -> (xibname:String,identifier:String){
        var id = styleID
        if isEmptyString(str: styleID){
            id = "1"
        }
        return (String.init(format: "custom%@", id),String.init(format: "main_controller_%@", id))
    }
    
    //MARK: 充值、个人信息获取
    private func accountWeb(from:String) {
        //帐户相关信息
        let baseVC = BaseController()
        baseVC.request(frontDialog: false, url:MEMINFO_URL,
                       callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            return
                        }
                        if let result = MemInfoWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                if let memInfo = result.content{
                                    if from == "charge"
                                    {
                                        self.getChargeInfo?(memInfo)
                                    }else if from == "withdraw"
                                    {
                                        self.getWithdrawInfo?(memInfo)
                                    }
                                    
                                }
                            }
                        }
        })
    }
    
    //MARK: 投注
    private func clickBet(lotCode:String,isOffical:Int,fromVC:UIViewController) {
        let lotData = self.allDatas
        for dataP in lotData
        {
            if dataP.subData.count > 0
            {
                for inDataP in dataP.subData
                {
                    if inDataP.code == lotCode && inDataP.lotVersion == isOffical
                    {
                        chooseControllerWithController(controller: fromVC, lottery: inDataP)
                        break
                    }
                }
                
            }else {
                if dataP.code == lotCode && dataP.lotVersion == isOffical
                {
                    chooseControllerWithController(controller: fromVC, lottery: dataP)
                    break
                }
            }
        }
    }
    
    private func handleBettingData() {
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                let lotCode = config.content.default_lot_code
                let version = config.content.lottery_version
                let isOffical = (version == VERSION_V1) || (version == VERSION_V1V2 && config.content.switch_xfwf != "on") ? 1 : 2
                self.gotoBet?(lotCode,isOffical)
            }
        }
    }
}




