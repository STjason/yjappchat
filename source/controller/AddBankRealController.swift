//
//  AddBankRealController.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/20.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class AddBankRealController: BaseController{
    
//    @IBOutlet weak var lastBankViewHeightConstrait:NSLayoutConstraint!
    
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewBG: UIImageView!
    @IBOutlet weak var lastBankView:UIView!
    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var bankNameTV:UILabel!
    @IBOutlet weak var bankAddressTV:UILabel!
    @IBOutlet weak var bankCardNoTV:UILabel!
    @IBOutlet weak var tablview:UITableView!

    var clickPointY:CGFloat = 0
    var isSelect = false
    var lastBankInfo:LastBankContent!
    var allBanks:[AllBankContent]!
    var allBankNames:[String] = []
    var gameDatas = [FakeBankBean]()
    var selectedIndex:Int = -1//银行列表选择的行索引
    
    let OTHER_BANK_CODE = "999"//其他银行时的银行代号
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !YiboPreference.getLoginStatus() {
            loginWhenSessionInvalid(controller: self)
        }
        
        self.title = "添加银行卡"
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowIn(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideOut(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        tablview.delegate = self
        tablview.dataSource = self
        tablview.showsVerticalScrollIndicator = false
//        tablview.tableFooterView = self.footView()
//        self.tablview.reloadData()
        tablview.tableFooterView = UIView.init()
        
        setupNoPictureAlphaBgView(view: imageViewBG, bgViewColor: "MemberPage.bankCardBgViewColor")
        icon.theme_image = "MemberPage.bankCardTag"
        self.loadLastBankDatas(showDialog: true)
        
        self.view.addSubview(self.footView())
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func getFakeModels(bank:AllBankContent){
        let item1 = FakeBankBean()
        item1.text = "开户银行"
        item1.value = bank.payName
        gameDatas.append(item1)
        
        if bank.iconCode == OTHER_BANK_CODE{
            let item10 = FakeBankBean()
            item10.text = "银行名称"
            item10.value = ""
            item10.placeholder = "请输入银行名称"
            gameDatas.append(item10)
        }
        
        let item2 = FakeBankBean()
        item2.text = "开户姓名"
        item2.value = ""
        gameDatas.append(item2)
        let item3 = FakeBankBean()
        item3.text = "银行地址"
        item3.value = ""
        gameDatas.append(item3)
        let item4 = FakeBankBean()
        item4.text = "银行卡号"
        item4.value = ""
        gameDatas.append(item4)
        let item5 = FakeBankBean()
        item5.text = "确认卡号"
        item5.value = ""
        gameDatas.append(item5)
    }
    
    //
    func loadLastBankDatas(showDialog:Bool) -> Void {
        
        request(frontDialog: showDialog, method:.get, loadTextStr:"获取记录中", url:API_LAST_BANK_INFO,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = LastBankWrapper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if result.content != nil{
                                self.lastBankInfo = result.content
                                self.updateLastBankView(info: result.content)
                            }else{
//                                self.lastBankViewHeightConstrait.constant = -120
//                                self.lastBankView.isHidden = true
                            }
                            if self.allBanks == nil{
                                self.loadAllBankDatas(showDialog: true)
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    func updateLastBankView(info:LastBankContent?){
        if info == nil{
//            lastBankViewHeightConstrait.constant = -120
//            lastBankView.isHidden = true
            return
        }
//        lastBankViewHeightConstrait.constant = 0
//        lastBankView.isHidden = false
//        icon.image = UIImage.init(named: "topup_icbcicon")
        
        bankNameTV.text = String.init(format: "%@", !isEmptyString(str: (info?.bankName)!) ? (info?.bankName)! : "暂无银行")
        bankAddressTV.text = String.init(format: "%@", !isEmptyString(str: (info?.bankAddress)!) ? (info?.bankAddress)! : "暂无地址")
        bankCardNoTV.text = String.init(format: "%@", !isEmptyString(str: (info?.cardNo)!) ? (info?.cardNo)! : "暂无卡号")
    }
    
    func loadAllBankDatas(showDialog:Bool) -> Void {
        
        request(frontDialog: showDialog, method:.get, loadTextStr:"获取银行列表中", url:API_GET_BANKS,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = AllBankWrapper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if result.content != nil{
                                self.allBanks = result.content
                                if !self.allBanks.isEmpty{
                                    for bank in self.allBanks{
                                        self.allBankNames.append(bank.payName)
                                    }

                                    self.getFakeModels(bank: AllBankContent())
                                    self.tablview.reloadData()
                                }
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取银行列表失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
                
        })
    }
    
    private func checkUerContact() {
        var switchP = true
        if let sysconfig = getSystemConfigFromJson()
        {
            if sysconfig.content != nil
            {
                switchP = sysconfig.content.add_bank_perfect_contact == "on"
            }
        }

        if switchP
        {
            checkUserHasContact()
        }else
        {
            commitAddCard()
        }
    }
    
    private func showAddUserContact() {
        let alert = UIAlertController.init(title: "请绑定手机号", message: nil, preferredStyle: .alert)
        let commitAction = UIAlertAction.init(title: "确认", style: .default) { (action) in
            if (alert.textFields![0].text != nil) {
                self.addUserContactRequest(phoneNum: alert.textFields![0].text!)
            }
        }
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel) { (action) in}
        alert.addTextField { (textField) in
            textField.placeholder = "请绑定手机号"
            textField.keyboardType = UIKeyboardType.phonePad
        }
        alert.addAction(commitAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {}
    }
    
    
    @objc func onCommitBtn(ui:UIButton){
        checkUerContact()
    }
    
    //MARK: 绑定手机号
    private func addUserContactRequest(phoneNum:String) {
        self.request(frontDialog: true, method:.post,url: UPDATE_USER_CONTACT,params:["phone":phoneNum]) { (resultJson:String,resultStatus:Bool) in
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "绑定手机号失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            
            if let result = BaseWrapper.deserialize(from: resultJson){
                if result.success{
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    if result.content
                    {
                        showToast(view: self.view, txt: "添加成功，请继续完成银行卡绑定")
                    }else
                    {
                        showToast(view: self.view, txt: "绑定手机号失败")
                    }
                }else{
                    if !isEmptyString(str: result.msg){
                        self.print_error_msg(msg: result.msg)
                    }else{
                        showToast(view: self.view, txt: convertString(string: "绑定手机号失败"))
                    }
                    if (result.code == 0) {
                        loginWhenSessionInvalid(controller: self)
                    }
                }
            }
        }
    }
    
    //MARK: - 接口
    //MARK: 检查手机号是否绑定
    private func checkUserHasContact() {
        self.request(frontDialog: true, method:.get,url: CHECK_USER_CONTACT) { (resultJson:String,resultStatus:Bool) in
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "检查绑定信息失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if let result = BaseWrapper.deserialize(from: resultJson){
                if result.success{
                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                    if result.content
                    {
                        self.commitAddCard()
                    }else
                    {
                        self.showAddUserContact()
                    }
                }else{
                    if !isEmptyString(str: result.msg){
                        self.print_error_msg(msg: result.msg)
                    }else{
                        showToast(view: self.view, txt: convertString(string: "检查绑定信息失败"))
                    }
                    if (result.code == 0) {
                        loginWhenSessionInvalid(controller: self)
                    }
                }
            }
        }
    }
    
    
    private func commitAddCard() {
        
        let lastBankCardNo = self.lastBankInfo != nil ? self.lastBankInfo.cardNo : ""
        let lastRealName = self.lastBankInfo != nil ? self.lastBankInfo.realName : ""
        //定位crash 12-11
        if self.selectedIndex == -1 {
            showToast(view: self.view, txt: "请选择开户银行")
            return
        }
        
        let bankCode = self.allBanks[self.selectedIndex].iconCode
        var bankName = ""
        var bankUserName = ""
        var bankAddress = ""
        var bankCardNo = ""
        var bankAgainCardNo = ""
        
        if bankCode != OTHER_BANK_CODE {
            bankName = self.allBanks[self.selectedIndex].payName
            bankUserName = self.gameDatas[1].value
            bankAddress = self.gameDatas[2].value
            bankCardNo = self.gameDatas[3].value
            bankAgainCardNo = self.gameDatas[4].value
        }else{
            bankName = self.gameDatas[1].value
            bankUserName = self.gameDatas[2].value
            bankAddress = self.gameDatas[3].value
            bankCardNo = self.gameDatas[4].value
            bankAgainCardNo = self.gameDatas[5].value
        }
        
        if isEmptyString(str: bankUserName){
            showToast(view: self.view, txt: "请输入开户姓名")
            return
        }
        if isEmptyString(str: bankAddress){
            showToast(view: self.view, txt: "请输入开户银行地址")
            return
        }
        if isEmptyString(str: bankCardNo){
            showToast(view: self.view, txt: "请输入银行卡号")
            return
        }
        if isEmptyString(str: bankAgainCardNo){
            showToast(view: self.view, txt: "请输入确认银行卡号")
            return
        }
        
        if bankCardNo != bankAgainCardNo{
            showToast(view: self.view, txt: "两次输入银行卡号不一致")
            return
        }
        
//        realName、bankName、bankAddress、cardNo、bankCode、lastCardNo、lastRealName
        let parameter = ["realName": bankUserName,"bankName":bankName,"bankAddress":bankAddress,"cardNo":bankCardNo,
                         "bankCode":bankCode,"lastCardNo":lastBankCardNo,"lastRealName":lastRealName]
        self.request(frontDialog: true, method:.post, loadTextStr:"添加中...", url:API_ADD_BANKCARD,params: parameter,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                        
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "添加失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = AddBankWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                showToast(view: self.view, txt: "添加成功")
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "添加失败"))
                                }
                                if (result.code == 0) {
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
                        
        })
    }
    
}

extension AddBankRealController{
    func footView() -> UIView{
        let footer = UIView.init(frame: CGRect.init(x: 0, y:kScreenHeight-50, width: kScreenWidth, height: 45))
        let confirmbtn = UIButton.init(frame: CGRect.init(x: 20, y:0, width: kScreenWidth - 40, height: 45))
        //confirmbtn.backgroundColor = UIColor.red
        confirmbtn.theme_backgroundColor = "Global.themeColor"
        confirmbtn.layer.cornerRadius = 5
        confirmbtn.setTitle("确认", for: .normal)
        confirmbtn.setTitleColor(UIColor.white, for: .normal)
        confirmbtn.addTarget(self, action: #selector(onCommitBtn(ui:)), for: .touchUpInside)
        footer.addSubview(confirmbtn)
        return footer

    }
}

extension AddBankRealController :UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
        }
        let model = self.gameDatas[indexPath.row]
        cell.inputTV.delegate = self
        cell.inputTV.tag = indexPath.row
        cell.setModel(model: model,row: indexPath.row,isOtherBank: gameDatas.count == 6)
        cell.inputTV.addTarget(self, action: #selector(onInput(ui:)), for: .editingChanged)
        cell.inputTV.addTarget(self, action: #selector(onInputStart(ui:)), for: .editingDidBegin)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            if (isSelect == false) {
                isSelect = true
                self.showBankListDialog()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.isSelect = false
                }
            }
        }
    }
    
    @objc func onInput(ui:UITextField){
        let text = ui.text!
        if ui.tag == 1  && text.length > 49
        {
            showToast(view: self.view, txt: "姓名不能超过50个字符")
            self.view.endEditing(true)
            return
        }else if ui.tag == 2  && text.length > 99
        {
            showToast(view: self.view, txt: "银行地址不能超过100个字符")
            self.view.endEditing(true)
            return
        }
        self.gameDatas[ui.tag].value = text
//        self.tablview.reloadData()
    }
    
    @objc func onInputStart(ui:UITextField) {
        let rectP = ui.convert(ui.bounds, to: view.window)
        clickPointY = rectP.origin.y + rectP.height
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    private func showBankListDialog(){
        let selectedView = LennySelectView(dataSource: self.allBankNames, viewTitle: "请选择开户银行")
        selectedView.selectedIndex = self.selectedIndex
        
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.gameDatas[0].value = selectedView.kLenny_InsideDataSource[index]
            self?.selectedIndex = index
            
            self?.gameDatas.removeAll()
            self?.getFakeModels(bank:(self?.allBanks[index])!)
            self?.tablview.reloadData()
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
//            self.setSelected(false, animated: true)
        }
    }

    //MARK: - 键盘响应
    @objc func keyboardWillShowIn(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
            
            if keyBoardNeedLayout {
//                playPaneTopConstraint.constant = abs(deltaY - 160)
//                if clickPointY
                
                var topConstaint:CGFloat = 0
                if (screenHeight - clickPointY) < deltaY
                {
                    topConstaint = (screenHeight - clickPointY) - deltaY
                }
                UIView.animate(withDuration: duration, delay: 0.0,
                               options: UIView.AnimationOptions(rawValue: curve),
                               animations: {
                                self.view.frame = CGRect.init(x:0,y:topConstaint,width:self.view.bounds.width,height:self.view.bounds.height)
                                self.keyBoardNeedLayout = false
                                self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    //键盘隐藏响应
    @objc  func keyboardWillHideOut(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            let frame = value.cgRectValue
            let intersection = frame.intersection(self.view.frame)
            let deltaY = intersection.height
            
//            playPaneTopConstraint.constant = 1
//            headerTopConstraint.constant = 0
            UIView.animate(withDuration: duration, delay: 0.0,
                           options: UIView.AnimationOptions(rawValue: curve),
                           animations: {
                            self.view.frame = CGRect.init(x:0,y:deltaY,width:self.view.bounds.width,height:self.view.bounds.height)
                            self.keyBoardNeedLayout = true
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    

    
}

