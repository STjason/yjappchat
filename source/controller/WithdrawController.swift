//
//  WithdrawController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/31.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

protocol BankDelegate {
    func onBankSetting()
}

import UIKit
//取款界面
class WithdrawController: BaseController,UITextFieldDelegate,BankDelegate {
   
    
    @IBOutlet weak var bottomLayout: NSLayoutConstraint!
    @IBOutlet weak var Feecalculation: CustomFeildText! //手续费
    @IBOutlet weak var FeecalLabale: UILabel!
    
    @IBAction func editPersonalInfo() {
        self.navigationController?.pushViewController(UserInfoController(), animated: true)
    }
    @IBOutlet weak var remainWithdrawTimes: UILabel!
    @IBOutlet weak var lowerLimit: UILabel!
    @IBOutlet weak var upperLimit: UILabel!
    @IBOutlet weak var needBettingAmount: UILabel!
    @IBOutlet weak var currentCodeAmount: UILabel!
    @ IBOutlet weak var headerImg:UIImageView!
    @IBOutlet weak var headerBgImage: UIImageView!
    @ IBOutlet weak var accountNameUI:UILabel!
    @ IBOutlet weak var accountBalanceUI:UILabel!
    @ IBOutlet weak var bankUI:UILabel!
    @ IBOutlet weak var unUseTipUI:UILabel!
//    @ IBOutlet weak var unUseTipHeightConstaint:NSLayoutConstraint!
    @IBOutlet weak var bankNameTitle: UILabel!
    
    @ IBOutlet weak var inputArea:UIView!
    @ IBOutlet weak var inputMoneyUI:CustomFeildText! //金额
    @ IBOutlet weak var inputPwdUI:CustomFeildText!
    @ IBOutlet weak var commitBtn:UIButton!

    @IBOutlet weak var withdrawTimeLable: UILabel!
    @IBOutlet weak var FeedescriptionLable: UILabel!
    @IBOutlet weak var FFFF: UILabel!
    @IBOutlet weak var bankNameTopContant: NSLayoutConstraint!
    @IBOutlet weak var bottomHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var topHeightConstant: NSLayoutConstraint!
    var selectedIndex: Int = 0{
        didSet{
            if selectedIndex == 0{
                self.withdrawType = "1"
                bankNameTitle.text = "银行卡:"
                self.USDTNumberCell.isHidden = true
                self.USDTRateCell.isHidden = true
                bottomHeightConstant.constant = 55
            }else if selectedIndex == 1{
                self.withdrawType = "2"
                bankNameTitle.text = "USDT:"
                self.USDTNumberCell.isHidden = false
                self.USDTRateCell.isHidden = false
                bottomHeightConstant.constant = 90
                self.USDTNumberCell.frame = CGRect.init(x: 0, y: 30*5, width: kScreenWidth, height: 30)
                self.USDTRateCell.frame = CGRect.init(x: 0, y: 30*6, width: kScreenWidth, height: 30)
            }
            if oldValue != selectedIndex{
                self.checkAccountFromWeb()
            }
        }
    }
    var withdrawType = "1"
    //    @IBOutlet weak var pickConfigLabel: UITextView!
    lazy var pickTypeCell: UITableViewCell = {
        let cell = UITableViewCell()
        let titleLabel = UILabel()
        cell.contentView.addSubview(titleLabel)
        titleLabel.text = "提款类型:"
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.leading.equalTo(cell.snp.leading).offset(8)
            make.width.equalTo(66)
            make.height.equalTo(cell.contentView)
        }
        let image: UIImage = UIImage.init(named: "more") ?? UIImage()
        let accessoryView = UIImageView.init(image: image)
        accessoryView.frame = CGRect.init(x: 0, y: 0, width: 20, height: 23)
        cell.accessoryView = accessoryView
        cell.contentView.addSubview(typeLabel)
        typeLabel.text = "银行卡"
        typeLabel.font = UIFont.systemFont(ofSize: 17)
        typeLabel.textColor = .gray
        typeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.height.equalTo(cell.contentView)
            make.trailing.equalTo(cell.contentView)
        }
        self.inputArea.addSubview(cell)
        cell.isUserInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapCellForselectingType))
        cell.addGestureRecognizer(tap)
        return cell
    }()
    
    lazy var USDTRateCell: UITableViewCell = {
        let cell = UITableViewCell()
        let titleLabel = UILabel()
        cell.contentView.addSubview(titleLabel)
        titleLabel.text = "提款汇率:"
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.leading.equalTo(cell.snp.leading).offset(8)
            make.width.equalTo(66)
            make.height.equalTo(cell.contentView)
        }
        let typeLabel = UILabel()
        typeLabel.textColor = .gray
        cell.contentView.addSubview(typeLabel)
        typeLabel.text = getSystemConfigFromJson()?.content.pay_tips_deposit_usdt_rate
        typeLabel.font = UIFont.systemFont(ofSize: 17)
        typeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.height.equalTo(cell.contentView)
            make.trailing.equalTo(cell.contentView)
        }
        self.inputArea.addSubview(cell)
        return cell
    }()
    var typeLabel: UILabel = UILabel()
    var numberLabel: UILabel = UILabel()
    
    lazy var USDTNumberCell: UITableViewCell = {
        let cell = UITableViewCell()
        let titleLabel = UILabel()
        titleLabel.adjustsFontSizeToFitWidth = true
        cell.contentView.addSubview(titleLabel)
        titleLabel.text = "USDT数量:"
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.leading.equalTo(cell.snp.leading).offset(8)
            make.width.equalTo(66)
            make.height.equalTo(cell.contentView)
        }
        cell.contentView.addSubview(numberLabel)
        numberLabel.text = "0"
        numberLabel.textColor = .gray
        numberLabel.font = UIFont.systemFont(ofSize: 17)
        numberLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.height.equalTo(cell.contentView)
            make.trailing.equalTo(cell.contentView)
        }
        self.inputArea.addSubview(cell)
        return cell
    }()
    
    @objc func tapCellForselectingType(){
        showBankListDialog()
    }
    
    //提款类型选择弹窗
    private func showBankListDialog(){
        let selectedView = LennySelectView(dataSource: ["银行卡", "USDT"], viewTitle: "请选择支付类型")
        selectedView.selectedIndex = self.selectedIndex
        selectedView.didSelected = { (index, content) in
            self.typeLabel.text = content
            self.selectedIndex = index
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        })
    }
    
    lazy var pickConfigLabel: UITextView = {
        var textView = UITextView()
        view.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.leading.equalTo(view).offset(5)
            make.trailing.equalTo(view).offset(-5)
            make.top.equalTo(FeedescriptionLable.snp.bottom).offset(10)
            make.bottom.equalTo(commitBtn.snp.top).offset(-18)
        }
        return textView
    }()
    var fee_types = 0 , drawNums = 0 , fee_values = 0, dayTimess = 0  //手续费计算
    
    
//    pickConfigLabel 禁止编辑
    private func setupThemeView() {
        setThemeLabelTextColorGlassWhiteOtherRed(label: self.needBettingAmount)
        setThemeLabelTextColorGlassWhiteOtherRed(label: self.currentCodeAmount)
        setThemeLabelTextColorGlassWhiteOtherRed(label: self.upperLimit)
        setThemeLabelTextColorGlassWhiteOtherRed(label: self.lowerLimit)
        setThemeLabelTextColorGlassWhiteOtherRed(label: self.withdrawTimeLable)
        setThemeLabelTextColorGlassWhiteOtherRed(label: self.FeedescriptionLable)
    }
    
//    @ IBOutlet weak var drawTipUI:UITextView!
    
    var meminfo:Meminfo?
    var firstBankInfo:BankList?
    var pickBankAccount:PickBankAccount?
    var currentBankCardID = ""

    @IBAction func viewAcountChange() { self.navigationController?.pushViewController(AccountChangeController(), animated: true)
    }
    
    @objc private func getBankListModelNoti(noti: Notification) {
        if let model = noti.object as? BankCardListContent
        {
            let cardID = (model.type == "2") ? "USDT" : model.bankName
            currentBankCardID = "\(model.id)"
            bankUI.text = "\(model.cardNoSc)" + "(" + "\(cardID)" + ")"
            bankNameTitle.text = (model.type == "2") ? "USDT:" : "银行卡:"
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountNameUI.text = "没有名称"
        if let meminfo = self.meminfo{
            accountBalanceUI.text = "帐户余额:\(meminfo.balance)"
        }
        
        setupThemeView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getBankListModelNoti), name: NSNotification.Name(rawValue: "getBankListModelNoti"), object: nil)
        headerBgImage.theme_image = "General.personalHeaderBg"
       
        
        if YiboPreference.getCACHEAVATARdata().count >= 1 {
             headerImg.image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
        }else{
             headerImg.theme_image = "General.placeHeader"
        }
        
        commitBtn.addTarget(self, action: #selector(onCommitClick), for: UIControl.Event.touchUpInside)
        inputMoneyUI.delegate = self
        inputPwdUI.delegate = self
        inputMoneyUI.addTarget(self, action: #selector(didChangeTextFieldValue(textField:)), for: .editingChanged)
        commitBtn.layer.cornerRadius = 3.0
        commitBtn.theme_backgroundColor = "Global.themeColor"
        commitBtn.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapBankLabel))
        bankUI.isUserInteractionEnabled = true
        bankUI.addGestureRecognizer(tapGesture)
        
        self.navigationItem.title = "提款"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "提款记录", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onWithdrawRecordClick))
        
        checkAccountFromWeb()
        updatePickConfigLabel()
        self.pickConfigLabel.isEditable=false //禁止编辑
        self.Feecalculation.isSecureTextEntry = false
        self.Feecalculation.delegate=self;
        
        self.Feecalculation.placeholder = "今日剩余免费提款次数0次"
        
        headerImg.layer.cornerRadius = 30.0
        headerImg.layer.masksToBounds = true
        selectedIndex = 0
    }
    
    @objc func didChangeTextFieldValue(textField: UITextField){
        guard let rate = getSystemConfigFromJson()?.content.pay_tips_deposit_usdt_rate else { return }
        
        let decimalRate: Decimal = Decimal.init(string: rate) ?? Decimal.zero
        let decimalMoney: Decimal = Decimal.init(string: textField.text ?? "0") ?? Decimal.zero
        let decimalResult: Decimal = decimalMoney / decimalRate
        numberLabel.text = "\(decimalResult)"
    }
    
    private func updatePickConfigLabel(){
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                self.pickConfigLabel.text = config.content.withdraw_page_introduce
            }
        }
    }
    
    private func getTimesToWithDraw() -> Int{
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                if let timesToWithDraw = config.content.withdraw_time_one_day {
                    print(timesToWithDraw)
                    return timesToWithDraw
                }
            }
        }
        
        return 1
    }
    
   @objc private func tapBankLabel() {
        showSelectedView()
    }
    
    private func showSelectedView() {
        if let model  = pickBankAccount {
            if !setupByBankRequest(content: model) {
                return
            }
        }
        
        let vc = BankCardListController()
        vc.fromWithDrawVC = true
        vc.withdrawType = withdrawType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func checkAccountFromWeb() -> Void {
        request(frontDialog: true,method: .get,loadTextStr: "正在检查帐号...",url:CHECK_PICK_MONEY_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if !isEmptyString(str: resultJson){
                            showToast(view: self.view, txt: resultJson)
                        }else{
                            showToast(view: self.view, txt: convertString(string: "检查提款帐户安全失败"))
                        }
                        return
                    }
//                    print(resultJson)
                    if let result = CheckPickAccountWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            let onoff_usdt_withdraw = getSystemConfigFromJson()?.content.onoff_usdt_withdraw
                            if onoff_usdt_withdraw == "on"{
                                self.bankNameTopContant.constant = 30
                                self.pickTypeCell.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 30)
                            }
                            //根据提款类型筛选
                            result.content?.bankList = result.content?.bankList?.compactMap({
                                if $0.type ?? "1" == self.withdrawType{
                                    return $0
                                }
                                return nil
                            })
                            if let content = result.content{
                                if self.setupByBankRequest(content: content) {}
                            }
                            
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "检查提款帐户安全失败"))
                            }
                            if result.code == 0{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    
    private func setupByBankRequest(content: PickBankAccount) -> Bool{
        self.pickBankAccount = content
        self.updateValues(data: content)
        //银行帐户等信息检查通过后获取提款配置信息
        if let pwd = content.receiptPwd{
            if isEmptyString(str: pwd){
                //跳到设置提款密码界面
                self.actionBankPwdSetting(delegate: self)
                return false
            }
        }
        if let banklist = content.bankList{
            if banklist.isEmpty{
                let json = content.toJSONString()
                if let jsonValue = json{
                    self.actionBankSetting(delegate: self,json:jsonValue)
                    return false
                }
                return false
            }else{
                //没有银行卡
                self.firstBankInfo = banklist[0]
                
            }
        }
        
        return true
    }
    
    @objc private func buttonAddHandle() {
        let vc: AddUSDTRealController = AddUSDTRealController()
        vc.addingType = withdrawType
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onHeaderEmptyViewClick(){
        inputMoneyUI.resignFirstResponder()
        inputPwdUI.resignFirstResponder()
    }
    
    func updateValues(data:PickBankAccount) -> Void {
        if let meminfo = self.meminfo{
            accountBalanceUI.text = String.init(format: "帐户余额:%@元", meminfo.balance)
        }
        
        if let bank = data.bankList{
            if !bank.isEmpty{
                let bankName = bank[0].type == "2" ? "USDT" : bank[0].bankName
                currentBankCardID = bank[0].id
                bankUI.text = String.init(format: "%@(%@)", bank[0].cardNoSc ,bankName)
                accountNameUI.text = String.init(format: "%@",bank[0].realName)
            }else{
                switch withdrawType {
                case "1":
                    bankUI.text = "暂无银行卡,请先绑定银行卡!"
                default:
                    bankUI.text = "暂无USDT,请先绑定USDT!"
                }
            }
        }
        
        if let everydayDrawTimes = Int((data.everydayDrawTimes)!) {
            self.dayTimess = data.dayTimes
            if getTimesToWithDraw() <= 0 {
                remainWithdrawTimes.text = "每日无限次提现次数"
            }else {
                let remainTimes = everydayDrawTimes - data.dayTimes
                //data.dayTimes 今日提现次数
                remainWithdrawTimes.text = "每日提现次数剩余:\(remainTimes)次"
            }
        }else {
            remainWithdrawTimes.text = "每日提现次数剩余: 次"
        }

        if let maxDrawMoney = data.maxDrawMoney {
            upperLimit.text = maxDrawMoney
        }
        if let minDrawMoney = data.minDrawMoney {
            lowerLimit.text = minDrawMoney
        }
        if let needBettingAmountFloat = data.bet?.drawNeed {
//            needBettingAmount.text = "\(needBettingAmountStr)"
            needBettingAmount.text = String.init(format: "%.2f", needBettingAmountFloat)
        }
        if let currentBettingNum = data.bet?.betNum {
            currentCodeAmount.text = "\(currentBettingNum)"
        }
        
        var withdrawTime = ""
        if let withdrawMinTime  = data.minDrawTime {
            withdrawTime += withdrawMinTime
            if let withdrawMaxTime = data.maxDrawTime {
                withdrawTime += "--" + withdrawMaxTime
            }
        }
        
        withdrawTimeLable.text = withdrawTime
        
        //新功能未提测
     
        
   
        if let strategyData = data.strategy{
            self.strategyUI(fee_type:strategyData.feeType,fee_value:strategyData.feeValue,drawNum:strategyData.drawNum)
        }else{
            FeecalLabale.isHidden = true
            Feecalculation.isHidden = true
            bottomLayout.constant = 10
            FeedescriptionLable.isHidden=true
            FFFF.isHidden=true
        }
    }
    
    func strategyUI(fee_type:NSInteger,fee_value:NSInteger,drawNum:NSInteger)->Void{
        let FeedescriptionStr:String
        fee_types = fee_type // ==1
        drawNums = drawNum //>1 , 免费
        fee_values = fee_value //固定扣x元
        
        if self.dayTimess > drawNums {
            self.Feecalculation.placeholder = "今日剩余免费提款次数0次"
        }else{
            self.Feecalculation.placeholder = String.init(format:"今日剩余免费提款次数%i次",drawNums-self.dayTimess)
        }
        
        
        //否则 fee_types == 2 && drawNum==0 fee_value 百分比收
        
        if fee_type == 1 {
            FeedescriptionStr="免费提款次数\(drawNum)次,超过次数按每次提款\(fee_value)元收手续费"
            FeedescriptionLable.lineBreakMode = NSLineBreakMode.byWordWrapping
            FeedescriptionLable.numberOfLines = 0
        }else{
            if drawNum == 0 {
                FeedescriptionLable.snp.updateConstraints { (make) in
                    make.centerY.equalTo(FFFF)
                    make.height.equalTo(21)
                }
                FeedescriptionStr="每次提款金额的\(fee_value)%收取手续费"
            }else{
                FeedescriptionStr="免费提款次数\(drawNum)次,超过次数按每次提款\(fee_value)%收手续费"
                FeedescriptionLable.lineBreakMode = NSLineBreakMode.byWordWrapping
                FeedescriptionLable.numberOfLines = 0
            }
        }
         FeedescriptionLable.text=FeedescriptionStr
    }
    
    //打开银行取款密码设置
    func actionBankPwdSetting(delegate:BankDelegate){
        openBankPwdSetting(controller: self,delegate:delegate)
    }
    
    //打开银行信息设置
    func actionBankSetting(delegate:BankDelegate,json:String){
//        openBankSetting(controller: self,delegate:delegate,json:json)
        buttonAddHandle()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.inputMoneyUI {
            
            
//            fee_types = fee_type // ==1
//            drawNums = drawNum //>1 , 免费
//            fee_values = fee_value //固定扣x元
//            //否则 fee_types == 2 && drawNum==0 fee_value 百分比收
            
        
            
            if (textField.text?.length)! >= 1{
                    var feestr = "0"
                    if fee_types == 1 {
                        if  dayTimess >= drawNums  {
                            feestr = String.init(format:"%.2f",CGFloat.init(fee_values))
                        }
                    }else{
                        if  dayTimess >= drawNums  {
                            let money = Float(self.inputMoneyUI.text!)
                            let fee_valuesinit = CGFloat.init(fee_values) / CGFloat.init(100)
                            feestr = String.init(format:"%.2f",fee_valuesinit*CGFloat.init(money!))
                        }
                    }
                
                    self.Feecalculation.text = feestr //手续费计算
            }else if (textField.text?.length)! == 0 {
                 self.Feecalculation.text = ""
            }
        }
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.Feecalculation {
             return false
        }
        return true
    }
    
    @objc func onWithdrawRecordClick() -> Void {
//        openAccountMingxi(controller: self, whichPage: 1)
        let vc = TopupAndWithdrawRecords()
        vc.current_index = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onCommitClick() -> Void {
        
        if self.firstBankInfo == nil{
            showToast(view: self.view, txt: "未获取到银行卡信息,请先绑定银行卡")
            return
        }
        
        
        if let data = pickBankAccount {
            if let everydayDrawTimes = Int((data.everydayDrawTimes)!) {

                let remainTimes = everydayDrawTimes - data.dayTimes
                if remainTimes <= 0 && getTimesToWithDraw() > 0{
                    showToast(view: self.view, txt: "剩余提现次数为 0")
                    return
                }
            }
            
//            let timeMin = data.minDrawTime
//            let timeMax = data.maxDrawTime
//            let timeNow = getTimeWithDate(date: Date(),dateFormat: "HH:mm")
            
//            if timeNow.compare(timeMin!).rawValue < 0 || timeNow.compare(timeMax!).rawValue > 0 {
//                showToast(view: self.view, txt: "不在提款时间段内，无法提款！")
//                return
//            }

            let moneyStr = inputMoneyUI.text!
            
            if let moneyNum = Float(moneyStr) {
                if let maxDrawMoney = Float((pickBankAccount?.maxDrawMoney)!) {
                    if moneyNum > maxDrawMoney {
                        showToast(view: self.view, txt: "提现金额不得大于: \(maxDrawMoney)")
                        return
                    }
                }
                
                if let minDrawMoney = Float((pickBankAccount?.minDrawMoney)!) {
                    if moneyNum < minDrawMoney {
                        showToast(view: self.view, txt: "提现金额不得小于: \(minDrawMoney)")
                        return
                    }
                }
            }
            
            let pwdStr = inputPwdUI.text!
            if isEmptyString(str: moneyStr){
                showToast(view: self.view, txt: "请输入取款金额")
                return
            }
            if isEmptyString(str: pwdStr){
                showToast(view: self.view, txt: "请输入取款密码")
                return
            }
            onPostPickMoney(money: moneyStr, bankid: self.currentBankCardID, pwd: pwdStr)
        }
    }
    
    
    func onPostPickMoney(money:String,bankid:String,pwd:String) -> Void {
        var params: [String: String] = ["amount":money,"bankId":bankid,"pwd":pwd, "drawType": withdrawType]
        if withdrawType == "2"{
            params["rate"] = getSystemConfigFromJson()?.content.pay_tips_deposit_usdt_rate
            params["num"] = numberLabel.text
        }
        request(frontDialog: true,method: .post, loadTextStr:"正在提交...",url: POST_PICK_MONEY_URL,params: params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "提交失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = PostPickMoneyWraper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }
                            showToast(view: self.view, txt: "提交成功，请等待订单处理完成")
                            self.inputMoneyUI.text = ""
                            self.inputPwdUI.text = ""
                            self.Feecalculation.text = ""
                            //提现请求成功后，关闭界面
//                            self.onBackClick()
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }
                            //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                            //所以此接口当code == 0时表示帐号被踢，或登录超时
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    //银行取款密码设置，或出款银行设置完后
    func onBankSetting() {
        checkAccountFromWeb()
    }
    

}
