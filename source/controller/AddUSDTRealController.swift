//
//  AddUSDTRealController.swift
//  gameplay
//
//  Created by JK on 2020/10/10.
//  Copyright © 2020 yibo. All rights reserved.
//

import UIKit
import MBProgressHUD
import IQKeyboardManagerSwift

var USDTtitleFontSize: CGFloat = 14

class AddUSDTRealController: LennyBasicViewController{
    lazy var tableView:UITableView = {
        let tableView = UITableView()
        tableView.frame = self.view.bounds
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(USDTCell.self, forCellReuseIdentifier: "cell")
        tableView.register(USDTHeader.self, forHeaderFooterViewReuseIdentifier: "headerFooter")
        tableView.tableFooterView = UIView()
        self.view.addSubview(tableView)
        return tableView
    }()
    //确认按钮
    lazy var footView: UIView = {
        let footer = UIView.init(frame: CGRect.init(x: 0, y:kScreenHeight-50, width: kScreenWidth, height: 45))
        let confirmbtn = UIButton.init(frame: CGRect.init(x: 20, y:0, width: kScreenWidth - 40, height: 45))
        confirmbtn.theme_backgroundColor = "Global.themeColor"
        confirmbtn.layer.cornerRadius = 5
        confirmbtn.setTitle("确认", for: .normal)
        confirmbtn.setTitleColor(UIColor.white, for: .normal)
        confirmbtn.addTarget(self, action: #selector(commitAddCard), for: .touchUpInside)
        footer.addSubview(confirmbtn)
        return footer
    }()
    //银行选择弹窗
    private func showBankListDialog(indexPath: IndexPath){
        let selectedView = LennySelectView(dataSource: self.allBankNames, viewTitle: "请选择开户银行")
        selectedView.selectedIndex = self.selectedIndex
        
        selectedView.didSelected = { (index, content) in
            self.resultList[indexPath.section][indexPath.row] = content
            if content == "其他银行"{
                //插入新的cell
                self.insertABankCell()
            }else {
                //恢复原来设置
                self.resetAllBankCell()
            }
            self.selectedIndex = index
            self.tableView.reloadData()
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        })
    }
    //用于判断ui和相关数据的布局和显示
    var existedUSDTAccount: Bool?{
        didSet{
            if existedUSDTAccount == false{
                //如果set新值后仍然是false，则删除首个section预设的所有相关数据
                self.identify.remove(at: 0)
                self.titles.remove(at: 0)
                self.rowsOfsection.remove(at: 0)
                self.placeholders.remove(at: 0)
                self.emptyPrompts.remove(at: 0)
                self.sectionTitles.remove(at: 0)
                self.resultList.remove(at: 0)
                self.cellEditting.remove(at: 0)
                self.otherPrompts.remove(at: 0)
            }
        }
    }
    var selectedIndex:Int = -1
    var allBankNames:[String] = []
    var allBanks:[AllBankContent]!
    var delegate:BankDelegate?
    
    //每个section里有几个row
    lazy var rowsOfsection: [Int] = {
        switch addingType {
        case "1":
            return [2, 5]
        default:
            return [2, 3]
        }
    }()
    //cell是否能点击
    lazy var cellEditting: [[Bool]] = {
        switch addingType  {
        case "1":
            return [[true, true], [false, true, true, true, true]]
        default:
            return [[true, true], [true, true, true]]
        }
    }()
    //section的标题
    lazy var sectionTitles: [String] = {
        switch addingType {
        case "1":
            return ["增加绑定银行卡需提供最近一次绑定的卡号信息！", "银行卡绑定后即不能修改，请确认好个人信息再确定绑定！"]
        default:
            return ["增加绑定USDT地址需提供最近一次绑定的使用中的USDT地址信息！", "*为必填信息，如需要删除请联系客服提供地址信息和充值截图进行处理！"]
        }
    }()
    //cell标题
    lazy var titles: [[String]] = {
        switch addingType {
        case "1":
            return [["开户姓名", "银行卡号"], ["开户银行", "开户姓名", "银行地址", "银行卡号", "确认卡号"]]
        default:
            return [["姓名", "USDT地址"], ["*姓名", "*USDT地址", "*确认地址"]]
        }
    }()
    //占位符
    lazy var placeholders: [[String]] = {
        switch addingType {
        case "1":
            return [["最近一次绑定的银行卡开户姓名", "最近一次绑定的银行卡号"], ["请选择开户银行", "请输入开户预留姓名", "请输入开户银行地址", "银行卡号必须是7至25个数字或字母组成", "请再次输入银行卡卡号"]]
        default:
            return [["最近一次绑定的姓名", "最近一次绑定的USDT地址"], ["请填写真实姓名", "只能输入30-50个数字和字母组合", "再次输入USDT地址"]]
        }
        
    }()
    //判空提示
    lazy var emptyPrompts: [[String]] = {
        switch addingType {
        case "1":
            return [["最近一次绑定的银行卡开户姓名不能为空", "最近一次绑定的银行卡号不能为空"], ["开户银行不能为空", "请输入符合要求的开户姓名", "银行地址不能为空", "请输入符合要求的银行卡号", "确认卡号不能为空"]]
        default:
            return [["最近一次绑定的姓名不能为空", "最近一次绑定的USDT地址不能为空"], ["请输入真实姓名", "请输入符合要求的USDT地址", "确认地址不能为空"]]
        }
    }()
    //其他提示
    lazy var otherPrompts: [[String]] = {
        switch addingType {
        case "1":
            return [["", ""], ["", "请输入符合要求的开户姓名", "", "请输入符合要求的银行卡号", "确认卡号与银行卡号不一致"]]
        default:
            return [["", "请输入符合要求的USDT地址"], ["", "请输入符合要求的USDT地址", "确认地址和USDT地址不一致"]]
        }
    }()
    //标识
    lazy var identify: [[String]] = {
        switch addingType {
        case "1":
            return [["lastBankName", "lastBankAddress"], ["bankName", "realBankName", "bankAddress", "bankCardNo", "confirmedBankCardNo"]]
        default:
            return [["lastUSDTName", "lastNo"], ["realUSDTName", "USDTAddress", "confirmedUSDTAddress"]]
        }
    }()
    //需要提交的结果
    lazy var resultList: [[String]] = {
        switch addingType {
        case "1":
            return [["", ""], ["", "", "", "", ""]]
        default:
            return [["", ""], ["", "", ""]]
        }
    }()
    //添加的类型，1是银行卡，2是USDT
    var addingType = "1"
    override func viewDidLoad() {
        super.viewDidLoad()
        switch addingType {
        case "1":
            title = "添加银行卡"
        default:
            title = "添加USDT"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadNetRequestWithViewSkeleton(animate: true)
    }
    
    //判断有没有绑定过usdt
    override func loadNetRequestWithViewSkeleton(animate: Bool) {
        super.loadNetRequestWithViewSkeleton(animate: false)
        LennyNetworkRequest.obtainBankList(){
            (model) in
            if model?.code == 0 && !(model?.success)!{
                loginWhenSessionInvalid(controller: self)
                return
            }
            if let content = model?.content, content.count > 0{
                //上次绑定的usdt
                if let first = content.first(where: {$0.type ?? "1" == self.addingType}){
                    self.existedUSDTAccount = true
                    print("上次绑定的姓名和卡号：" + "\n" + "\(first.realName ?? "")" + "\n" + "\(first.cardNo)")
                }else{
                    self.existedUSDTAccount = false
                }
            }else { self.existedUSDTAccount = false }
            self.tableView.reloadData()
            self.view.addSubview(self.footView)
            self.loadAllBankDatas(showDialog: true)
        }
    }
    
    func insertABankCell(){
        for (section, element) in identify.enumerated(){
            for row in 0..<element.count {
                let iden = element[row]
                if iden == "bankName"{
                    rowsOfsection[section] += 1
                    titles[section].insert("银行名称", at: row+1)
                    placeholders[section].insert("请输入银行名称", at: row+1)
                    emptyPrompts[section].insert("开户银行不能为空", at: row+1)
                    resultList[section].insert("", at: row+1)
                    identify[section].insert("otherBank", at: row+1)
                    cellEditting[section].insert(true, at: row+1)
                    otherPrompts[section].insert("", at: row+1)
                }
            }
        }
    }
    
    func resetAllBankCell(){
        for (section, element) in identify.enumerated(){
            for row in 0..<element.count {
                let iden = element[row]
                if iden == "otherBank"{
                    rowsOfsection[section] -= 1
                    titles[section].remove(at: row)
                    placeholders[section].remove(at: row)
                    emptyPrompts[section].remove(at: row)
                    identify[section].remove(at: row)
                    resultList[section].remove(at: row)
                    cellEditting[section].remove(at: row)
                    otherPrompts[section].remove(at: row)
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        IQKeyboardManager.shared.enable = true
    }
}

//tableViewDelegateCallback
extension AddUSDTRealController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowsOfSection = rowsOfsection[section]
        return rowsOfSection
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return rowsOfsection.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height: CGFloat = sectionTitles[section].height(width: screenWidth, font: UIFont.systemFont(ofSize: USDTtitleFontSize))
        //不能太矮
        if height <= 50{
            height = 50
        }else {
            height += 20
        }
        return height
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: USDTHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerFooter") as! USDTHeader
        header.titleLabel.text = sectionTitles[section]
        return header
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: USDTCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! USDTCell
        cell.selectionStyle = .none
        cell.titleLabel.text = titles[indexPath.section][indexPath.row]
        cell.textField.placeholder = placeholders[indexPath.section][indexPath.row]
        cell.textField.text = resultList[indexPath.section][indexPath.row]
        cell.textDidChangeCallBack = {(textField: UITextField) in
            print("现在在" + "\(cell.titleLabel.text ?? "")" + "输入:" + "\(textField.text ?? "")")
            let text = "\(textField.text ?? "")"
            self.resultList[indexPath.section][indexPath.row] = text
        }
        cell.textField.isUserInteractionEnabled = cellEditting[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let iden = identify[indexPath.section][indexPath.row]
        
        if iden == "bankName"{
            print("点击")
            self.showBankListDialog(indexPath: indexPath)
        }
    }
}

//network
extension AddUSDTRealController{
    @objc private func commitAddCard() {
        //通用值
        var lastName: String = ""
        var lastNo: String = ""
        //银行的值
        var bankName: String = ""
        var bankRealName: String = ""
        var bankAddress: String = ""
        var bankCardNo: String = ""
        var bankCode: String = ""
        //USDT的值
        var USDTRealName: String = ""
        var USDTCardNo: String = ""
        
        for (index, texts) in resultList.enumerated(){
            for (subIndex, text) in texts.enumerated(){
                if text.isEmpty{
                    //判空
                    showToast(view: self.view, txt: self.emptyPrompts[index][subIndex])
                    return
                }else {
                    //每个数值单独根据情况判断
                    let identify = self.identify[index][subIndex]
                    let prompt = self.otherPrompts[index][subIndex]
                    switch identify {
                    case "lastBankName",
                         "lastUSDTName":
                        lastName = text
                    case "lastNo",
                         "lastBankAddress":
                        lastNo = text
                    case "realUSDTName":
                        USDTRealName = text
                    case "realBankName":
                        bankRealName = text
                    case "USDTAddress":
                        let pwd =  "^[A-Za-z0-9]{30,50}$"
                        let regextestpwd = NSPredicate(format: "SELF MATCHES %@",pwd)
                        if !(regextestpwd.evaluate(with: text) == true) {
                            showToast(view: self.view, txt: convertString(string: prompt))
                            return
                        }
                        USDTCardNo = text
                    case "bankAddress":
                        bankAddress = text
                    case "bankCardNo":
                        let pwd =  "^[A-Za-z0-9]{7,25}$"
                        let regextestpwd = NSPredicate(format: "SELF MATCHES %@",pwd)
                        if !(regextestpwd.evaluate(with: text) == true) {
                            showToast(view: self.view, txt: convertString(string: prompt))
                            return
                        }
                        bankCardNo = text
                    case "confirmedUSDTAddress":
                        if USDTCardNo != text{
                            showToast(view: self.view, txt: convertString(string: prompt))
                            return
                        }
                    case "confirmedBankCardNo":
                        if bankCardNo != text{
                            showToast(view: self.view, txt: convertString(string: prompt))
                            return
                        }
                    case "bankName":
                        bankName = text
                        self.allBanks.forEach { (AllBankContent) in
                            if AllBankContent.payName == text{
                                bankCode = AllBankContent.iconCode
                            }
                        }
                    case "otherBank":
                        bankName = text
                    default:
                        break
                    }
                }
            }
        }
        
        //提交
        var parameter: [String: Any] = [:]
        switch addingType {
        case "1":
            parameter = ["realName": bankRealName,"bankName":bankName,"bankAddress":bankAddress,"cardNo":bankCardNo,
                             "bankCode":bankCode,"lastCardNo":lastNo,"lastRealName":lastName,"type":addingType]
        default:
            parameter = ["realName": USDTRealName,"cardNo":USDTCardNo,"lastCardNo":lastNo,"lastRealName":lastName,"type":addingType]
        }
            
        
        self.request(frontDialog: true, method:.post, loadTextStr:"添加中...", url:API_ADD_BANKCARD,params: parameter,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                        
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "添加失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = AddBankWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                showToast(view: self.view, txt: "添加成功")
                                self.navigationController?.popViewController(animated: true)
                                if let delegate = self.delegate{
                                    delegate.onBankSetting()
                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    self.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "添加失败"))
                                }
                                if (result.code == 0) {
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
                        
                     })
    }
    
    func loadAllBankDatas(showDialog:Bool) -> Void {
        request(frontDialog: showDialog, method:.get, loadTextStr:"获取银行列表中", url:API_GET_BANKS,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = AllBankWrapper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if result.content != nil{
                                self.allBanks = result.content
                                if !self.allBanks.isEmpty{
                                    for bank in self.allBanks{
                                        self.allBankNames.append(bank.payName)
                                    }
                                }
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取银行列表失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
                
        })
    }
}

class USDTHeader: UITableViewHeaderFooterView{
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: USDTtitleFontSize)
        label.textColor = .gray
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(label)
        return label
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.sizeToFit()
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(self.contentView)
        }
    }
}

class USDTCell: UITableViewCell{
    
    var textDidChangeCallBack: ((_ textField: UITextField) ->())?
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 1
        self.contentView.addSubview(titleLabel)
        return titleLabel
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.clearButtonMode = .whileEditing
        self.contentView.addSubview(textField)
        textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        return textField
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.sizeToFit()
        titleLabel.snp.remakeConstraints { (make) in
            make.leading.equalTo(self.contentView).offset(10)
            make.height.equalTo(self.contentView)
            make.width.equalTo(titleLabel.width)
        }
        
        textField.snp.remakeConstraints { (make) in
            make.leading.equalTo(titleLabel.snp.trailing).offset(10)
            make.trailing.equalTo(self.contentView).offset(-10)
            make.height.equalTo(self.contentView)
        }
    }
    
    @objc func textDidChange(textField: UITextField){
        guard let callback = textDidChangeCallBack else { return }
        callback(textField)
    }
}
