//
//  RegisterManagerContrller.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/28.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//注册管理
class RegisterManagerContrller: BaseController {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var confirmBtn:UIButton!
    
    var isSelect = false
    var gameDatas = [[FakeBankBean]]()
    var textFields = [CustomFeildText]()
    var options : [RegisterConfigContentModel]?
    var userTypes:[String] = ["代理","会员"]
    var selectedUserTypeIndex = 0
    
    var lotteryRebate = "";//选中的返点
    var sportRebate = "";//选中的体育返点
    var shabaRebate = "";//选中的沙巴体育返点
    var realRebate = "";//选中的真人返点
    var eqameRebate = "";//选中的电子返点
    var chessRebate = "";//
    /** 电竞游戏返点 */
    var gamingRebate = ""
    /** 捕鱼游戏返点 */
    var gameFishingRebate = ""
    
    
    
    var userName = ""
    var passWord = ""
    var againPassword = ""

    var phone = ""
    var realName = ""
    
    var ratebackData:AllRateBack!
    
    var isProxy = true
    var accountId = "" //’会员转代理‘时使用
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !YiboPreference.getLoginStatus() {
            loginWhenSessionInvalid(controller: self)
            return
        }
        
        confirmBtn.setTitle(isMemberTransformProxy() ? "确认" : "注册", for: .normal)
        
        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(backPageAction))
        tableView.delegate = self
        tableView.dataSource = self
        confirmBtn.layer.cornerRadius = 5
        confirmBtn.addTarget(self, action: #selector(actionRegister), for: .touchUpInside)
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        loadRatebackDatas(showDialog: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        IQKeyboardManager.shared.enable = true
    }
    
    private func addRebateParameters(parameters:Dictionary<String, String>) -> Dictionary<String,String> {
        var params = parameters
        if !isEmptyString(str: lotteryRebate){
            params["kickback"] = lotteryRebate
        }else
        {
            params["kickback"] = "0"
        }
        
        if !isEmptyString(str: realRebate){
            params["realRebate"] = realRebate
        }else
        {
            params["realRebate"] = "0"
        }
        
        if !isEmptyString(str: sportRebate){
            params["sportRebate"] = sportRebate
        }else
        {
            params["sportRebate"] = "0"
        }
        
        if !isEmptyString(str: eqameRebate){
            params["egameRebate"] = eqameRebate
        }else
        {
            params["egameRebate"] = "0"
        }
        
        if !isEmptyString(str: chessRebate){
            params["chessRebate"] = chessRebate
        }else
        {
            params["chessRebate"] = "0"
        }
        
        if !isEmptyString(str: shabaRebate){
            params["shabaRebate"] = shabaRebate
        }else{
            params["shabaRebate"] = "0"
        }
        
        params["esportRebate"] =  !isEmptyString(str: gamingRebate) ?  gamingRebate : "0"
        params["fishingRebate"] =  !isEmptyString(str: gameFishingRebate) ?  gameFishingRebate : "0"
        
        return params
    }
    
    private func registerAction() {

        if passWord != againPassword{
            showToast(view: self.view, txt: "两次密码不一致")
            return
        }
        
        let userType = selectedUserTypeIndex == 0 ? "2" : "1"
        var params = [String : String]()
        for section in gameDatas{
            for item in section{
                if item.show == 2{
                    //检查必输配置
                    if (isEmptyString(str: item.value) && item.required == 2) {
                        showToast(view: self.view, txt: String.init(format: "请输入%@", item.name))
                        return
                    }
                    //校验输入
                    if !isEmptyString(str: item.regex){
                        if (item.validate == 2 && !isMatchRegex(text: item.value, regex: item.regex)
                            && !isEmptyString(str: item.value)) {
                            showToast(view: self.view, txt: String.init(format: "请输入正确格式的%@",   item.name))
                            return;
                        }
                    }
                }
                
                //填充入参
                if item.eleName == "passwordRepeat"{continue} //重复密码不用提交
                params[item.eleName] = item.value
                
            }
        }
        params["accountType"] = userType

        params = addRebateParameters(parameters: params)
        
        print("提交注册数据>>>>>\(params)")
        
        request(frontDialog: true, method:.post, loadTextStr:"提交中...", url:API_REGISTER_SAVE,params: params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "提交失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = RatebackWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            showToast(view: self.view, txt: "注册成功")
                            self.onBackClick()
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
                    
        })
    }
     
    private func memberTransformToProxy() {
        var params = addRebateParameters(parameters: [:])
        params["accountId"] = self.accountId
        request(frontDialog: true, method: .post, url: API_MEMBER_TO_AGENT, params:params) { (resultJson:String, resultStatus:Bool) in
            if !resultStatus {
                if resultJson.isEmpty {
                    showToast(view: self.view, txt: convertString(string: "提交失败"))
                }else{
                    showToast(view: self.view, txt: resultJson)
                }
                return
            }
            
            if let jsonData = resultJson.data(using: .utf8)  {
                do {
                    if let reponse = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
                        if let msg = reponse["msg"] as? String {
                            showToast(view: self.view, txt: msg)
                            return
                        }
                        
                        if let success = reponse["success"] as? Bool {
                            if success {
                                showToast(view: self.view, txt: "操作成功，数据同步需要一点时间")
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }catch let paraseError as NSError {
                    print("数据解析错误:\(paraseError.localizedDescription)")
                }
            }
            
        }
    }

    @objc func actionRegister(){
        if isMemberTransformProxy() {
            memberTransformToProxy()
        }else {
            registerAction()
        }
    }
    
     @objc func backPageAction(){
        if self.navigationController != nil{
            let count = self.navigationController?.viewControllers.count
            if count! > 1{
                self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }

    func getFakeModels(bank:/*AllRateBack?*/[RegisterConfigContentModel]){
        
        var data1:[FakeBankBean] = []
        
        let item1 = FakeBankBean()
        item1.text = isMemberTransformProxy() ? "用户账号" : "用户类型"
       
        if !isMemberTransformProxy() {
            item1.value = self.isProxy ? userTypes[0] : userTypes[1]
        }else {
            item1.value = self.userName
        }
        
        data1.append(item1)
        if isMemberTransformProxy() {
            self.gameDatas.append(data1)
        }
        
        if !isMemberTransformProxy() {
            
            var values = [String : String]()
            for textField in textFields{
                if !isEmptyString(str: textField.accessibilityIdentifier ?? ""){
                    values[textField.accessibilityIdentifier ?? ""] = textField.text
                }
            }
            
            let item2 = FakeBankBean()
            item2.text = "登录账户"
            item2.placeholder = "账号只能是数字和字母"
            item2.eleName = "username"
            item2.value = values[item2.eleName] ?? ""
            item2.required = 2
            item2.show = 2
            item2.validate = 2
            item2.name = item2.text
            data1.append(item2)
            
            let item3 = FakeBankBean()
            item3.text = "登录密码"
            item3.placeholder = "请输入密码"
            item3.eleName = "password"
            item3.value = values[item3.eleName] ?? ""
            item3.required = 2
            item3.show = 2
            item3.validate = 2
            item3.name = item3.text
            data1.append(item3)
            
            let item4 = FakeBankBean()
            item4.text = "重复密码"
            item4.placeholder = "请再次输入密码"
            item4.eleName = "passwordRepeat"
            item4.value = values[item4.eleName] ?? ""
            item4.required = 2
            item4.show = 2
            item4.validate = 2
            item4.name = item4.text
            data1.append(item4)
            
            
//            let item5 = FakeBankBean()
//            item5.text = "姓 名"
//            item5.value = ""
//            item5.placeholder = "请输入姓名"
//            data1.append(item5)
//
//            let item6 = FakeBankBean()
//            item6.text = "手机号"
//            item6.value = ""
//            item6.placeholder = "请输入手机号码"
//            data1.append(item6)
            
            for subject in bank{
                let item = FakeBankBean()
                item.text = subject.name
                item.value = ""
                item.placeholder = "请输入\(item.text)"
                item.eleName = subject.eleName //判断依据
                item.value = values[item.eleName] ?? ""
                item.show = subject.show
                item.validate = subject.validate
                item.required = subject.required
                item.regex = subject.regex
                item.name = subject.name
                data1.append(item)
            }
            
            self.gameDatas.append(data1)
        }
        
        //...........................
        var data2:[FakeBankBean] = []
        
        if getSwitch_lottery() {
            let item21 = FakeBankBean()
            item21.text = "设置奖金:"
            item21.value = "选择其他奖金组"
            item21.code = "cpfs"
            data2.append(item21)
        }
        
        let system = getSystemConfigFromJson()
        let sport = system?.content.onoff_sport_switch
        let sbsport = system?.content.onoff_sb_switch
        let zhenren = system?.content.onoff_zhen_ren_yu_le
        let game = system?.content.onoff_dian_zi_you_yi
        let chess = system?.content.switch_chess
        let gaming = system?.content.switch_esport
        let fishing = system?.content.switch_fishing
        
        if !isEmptyString(str: sport!) && sport == "on" && self.isProxy{
            let item22 = FakeBankBean()
            item22.text = "设置体育返点:"
            item22.value = "选择其他返点比例"
            item22.code = "tyfs"
            data2.append(item22)
        }
        
        if !isEmptyString(str: sbsport!) && sbsport == "on" && self.isProxy{
            let item23 = FakeBankBean()
            item23.text = "设置第三方体育返点:"
            item23.value = "选择其他返点比例"
            item23.code = "sbfs"
            data2.append(item23)
        }
        
        if !isEmptyString(str: zhenren!) && zhenren == "on" && self.isProxy{
            let item24 = FakeBankBean()
            item24.text = "设置真人返点:"
            item24.value = "选择其他返点比例"
            item24.code = "zrfs"
            data2.append(item24)
        }
        
        if !isEmptyString(str: game!) && game == "on" && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置电子返点:"
            item25.value = "选择其他返点比例"
            item25.code = "dzfs"
            data2.append(item25)
        }
        if !isEmptyString(str: chess!) && chess == "on" && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置棋牌返点:"
            item25.value = "选择其他返点比例"
            item25.code = "qpfs"
            data2.append(item25)
        }
        if !isEmptyString(str: gaming!) && gaming == "on" && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置电竞返点:"
            item25.value = "选择其他返点比例"
            item25.code = "djfs"
            data2.append(item25)
        }
        if !isEmptyString(str: fishing!) && fishing == "on" && self.isProxy{
            let item25 = FakeBankBean()
            item25.text = "设置捕鱼返点:"
            item25.value = "选择其他返点比例"
            item25.code = "pyfs"
            data2.append(item25)
        }
        self.gameDatas.append(data2)
        
    }
    
    
    func loadRatebackDatas(showDialog:Bool) -> Void {
        
        gameDatas = [[FakeBankBean]]()
        
        request(frontDialog: showDialog, method:.get, loadTextStr:"获取数据中...", url:API_REGISTER_RATEBACK,
                callback: {(resultJson:String,resultStatus:Bool)->Void in

                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = RatebackWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            self.ratebackData = result.content
                            //更新需要显示的条目
                            //下标0为代理，1为会员 flag 2为代理，1为会员
                            self.loadDatas(flag: self.selectedUserTypeIndex == 0 ? 2 : 1)
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
                    
        })
    }
    
    //获取注册管理配置
    func loadDatas(flag : Int) -> Void {
        request(frontDialog: true, loadTextStr: "获取注册配置中...", url:REG_CONFIG_URL_V2, params: ["flag" : flag],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = RegisterConfigModel.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            if let configValues = result.content{
                                self.options = configValues
                                if self.options != nil{
                                    //排序
                                    let tempOptions = self.options?.sorted(by: { (RegisterConfigContentModel1, RegisterConfigContentModel2) -> Bool in
                                        return RegisterConfigContentModel1.sortNo > RegisterConfigContentModel2.sortNo
                                    })
                                    //把不显示的去掉
                                    let tempOptions2 = tempOptions?.compactMap({ (RegisterConfigContentModel1) -> RegisterConfigContentModel? in
                                        if RegisterConfigContentModel1.show == 2{
                                            return RegisterConfigContentModel1
                                        }
                                        return nil
                                    })
                                    self.getFakeModels(bank: tempOptions2 ?? [RegisterConfigContentModel]())
                                }
                                self.tableView.reloadData()
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                self.print_error_msg(msg: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
}

extension RegisterManagerContrller :UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameDatas[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if !isProxy && self.gameDatas.count == 2 && !getSwitch_lottery() && !isMemberTransformProxy() {
            return 1
        }
        
        return self.gameDatas.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isMemberTransformProxy() {
            return section != 0 ? 40 : 0
        }else {
            return 40
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.ratebackData == nil{
            return 0
        }
        let data = self.gameDatas[indexPath.section][indexPath.row]
//        if data.code == "cpfs"{
//            let array = self.ratebackData.lotteryArray
//        }else
        
        var showWhenZero = true
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                showWhenZero = config.content.show_rateback_when_zero == "on"
            }
           
        }
        
        if data.code == "tyfs"{
            let array = self.ratebackData.sportArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }else if data.code == "sbfs"{
            let array = self.ratebackData.shabaArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }else if data.code == "zrfs"{
            let array = self.ratebackData.realArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }else if data.code == "dzfs"{
            let array = self.ratebackData.egameArray
            if array.count == 0 && !showWhenZero {return 0} else
            if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }else if data.code == "qpfs"{
            let array = self.ratebackData.chessArray
            if array.count == 0 && !showWhenZero {return 0} else
                if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }
        else if data.code == "djfs"{
            let array = self.ratebackData.esportArray
            if array.count == 0 && !showWhenZero {return 0} else
                if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }
        else if data.code == "pyfs"{
            let array = self.ratebackData.fishingArray
            if array.count == 0 && !showWhenZero {return 0} else
                if array.count == 1 && array[0].value == "0" && !showWhenZero {return 0}
        }
        
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        
        setThemeViewNoTransparentDefaultGlassBlackOtherGray(view: header)
        
        let label = UILabel.init(frame: CGRect.init(x: 20, y: 0, width: tableView.bounds.width - 40, height: 40))
        setThemeLabelTextColorGlassWhiteOtherBlack(label: label)
        if section == 0{
            label.text = "信息填写"
        }else{
            label.text = isMemberTransformProxy() ? "返点设定" : "奖金组设定"
        }
        label.font = UIFont.systemFont(ofSize: 14)
        header.addSubview(label)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "add_bank_cell") as? AddBankTableCell  else {
            fatalError("The dequeued cell is not an instance of AddBankTableCell.")
        }
        let model = self.gameDatas[indexPath.section][indexPath.row]
        if indexPath.section == 0{
            if indexPath.row == 0{
                cell.accessoryType = isMemberTransformProxy() ? .none : .disclosureIndicator
                cell.inputTV.isHidden = true
                cell.valueTV.isHidden = false
            }else{
                if model.eleName == "password" || model.eleName == "passwordRepeat" || model.eleName == "receiptPwd"{
                    cell.inputTV.isSecureTextEntry = true
                }
                cell.accessoryType = .none
                if model.eleName == "phone" {
                    cell.inputTV.keyboardType = .numberPad
                }
                cell.inputTV.isHidden = false
                cell.valueTV.isHidden = true
                cell.inputTV.delegate = self
                cell.inputTV.tag = indexPath.row
                cell.inputTV.accessibilityIdentifier = model.eleName
                cell.inputTV.addTarget(self, action: #selector(onInput(ui:)), for: .editingChanged)
            }
        }else{
            let model = self.gameDatas[indexPath.section][indexPath.row]
//            cell.setModel(model: model,row: indexPath.row)
            cell.inputTV.isHidden = true
            cell.valueTV.isHidden = false
            cell.accessoryType = .disclosureIndicator
        }
        cell.inputTV.placeholder = !isEmptyString(str: model.placeholder) ? model.placeholder : String.init(format: "请输入%@", model.value)
        cell.valueTV.text = model.value
        cell.textTV.text = model.text
        //是否添加小红星
        if model.required == 2{
            let attributeString = NSMutableAttributedString.init(string: cell.textTV.text ?? "")
            let attributeString2 = NSMutableAttributedString.init(string: "*", attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
            attributeString.append(attributeString2)
            cell.textTV.attributedText = attributeString
        }
        if !textFields.contains(cell.inputTV){
            textFields.append(cell.inputTV)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 0{
            if indexPath.row == 0{
                if !isMemberTransformProxy() {
                    if (isSelect == false) {
                        isSelect = true
                        self.showUserTypeListDialog()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            self.isSelect = false
                        }
                    }

                }
            }
        }else{
            
            if (isSelect == false) {
                isSelect = true
                
                if self.ratebackData == nil{
                    return
                }
                let data = self.gameDatas[indexPath.section][indexPath.row]
                if data.code == "cpfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: true, rebatBeans: self.ratebackData.lotteryArray)
                    showRateBackListDialog(row: indexPath.row, code: data.code, sources: array)
                }else if data.code == "tyfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.sportArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }else if data.code == "sbfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.shabaArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }else if data.code == "zrfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.realArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }else if data.code == "dzfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.egameArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }else if data.code == "qpfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.chessArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }else if data.code == "djfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.esportArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }else if data.code == "pyfs"{
                    let array = createFakeRatebackDataForEmpty(isLottery: false, rebatBeans: self.ratebackData.fishingArray)
                    showRateBackListDialog(row: indexPath.row,code: data.code, sources: array)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.isSelect = false
                }
            }
            
        }
    }
    
    private func createFakeRatebackDataForEmpty(isLottery: Bool,rebatBeans: [RebateBean]) -> [RebateBean] {
        
        if rebatBeans.count > 0
        {
            return rebatBeans
        }else
        {
            let model = RebateBean()
            model.value = "0"
            model.label = isLottery ? "0%" : "0‰"
            let array = [model]
            
            return array
        }
    }
    
    @objc func onInput(ui:UITextField){
        let text = ui.text ?? ""
        let eleName = ui.accessibilityIdentifier

        //单独装一下，用来判断两者是否相同
        if eleName == "password"{
            passWord = text
        }else if eleName == "passwordRepeat"{
            againPassword = text
        }
        
        for item in gameDatas.first ?? [FakeBankBean](){
            if eleName == item.eleName{
                item.value = text
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    private func showUserTypeListDialog(){
        let selectedView = LennySelectView(dataSource: self.userTypes, viewTitle: "请选择用户类型")
        selectedView.selectedIndex = self.selectedUserTypeIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            self?.gameDatas[0][0].value = selectedView.kLenny_InsideDataSource[index]
            self?.isProxy =  self?.gameDatas[0][0].value == "代理"
            self?.selectedUserTypeIndex = index
            self?.loadRatebackDatas(showDialog: true)
//            self?.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .middle)

            // 选择会员或代理后，刷新奖金组设定的section
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
            //            self.setSelected(false, animated: true)
        }
    }
    
    private func showRateBackListDialog(row:Int,code:String,sources:[RebateBean]){
        if sources.isEmpty{
            showToast(view: self.view, txt: "没有返点数据，无法选择，请联系客服")
            return
        }
        var dataNames:[String] = []
        var selectedIndex = 0
        for index in 0...sources.count-1{
            let item = sources[index]
            if code == "cpfs"{
                if item.value == lotteryRebate{
                    selectedIndex = index
                }
            }else if code == "tyfs"{
                if item.value == sportRebate{
                    selectedIndex = index
                }
            }else if code == "sbfs"{
                if item.value == shabaRebate{
                    selectedIndex = index
                }
            }else if code == "zrfs"{
                if item.value == realRebate{
                    selectedIndex = index
                }
            }else if code == "dzfs"{
                if item.value == eqameRebate{
                    selectedIndex = index
                }
            }else if code == "qpfs"{
                if item.value == chessRebate{
                    selectedIndex = index
                }
            }
            else if code == "djfs"{
                if item.value == gamingRebate{
                    selectedIndex = index
                }
            }
            else if code == "pyfs"{
                if item.value == gameFishingRebate{
                    selectedIndex = index
                }
            }
            dataNames.append(String.init(format: "%@", item.label))
        }
        let selectedView = LennySelectView(dataSource: dataNames, viewTitle: "请选择奖金组")
        selectedView.selectedIndex = selectedIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            guard let weakSelf = self else{return}
            weakSelf.gameDatas[1][row].value = selectedView.kLenny_InsideDataSource[index]
            weakSelf.tableView.reloadData()
            if code == "cpfs"{
                weakSelf.lotteryRebate = sources[index].value
            }else if code == "tyfs"{
                weakSelf.sportRebate = sources[index].value
            }else if code == "sbfs"{
                weakSelf.shabaRebate = sources[index].value
            }else if code == "zrfs"{
                weakSelf.realRebate = sources[index].value
            }else if code == "dzfs"{
                weakSelf.eqameRebate = sources[index].value
            }else if code == "qpfs"{
               weakSelf.chessRebate = sources[index].value
            }else if code == "djfs"{
                weakSelf.gamingRebate = sources[index].value
            }else if code == "pyfs"{
               weakSelf.gameFishingRebate = sources[index].value
            }
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in
            //            self.setSelected(false, animated: true)
        }
    }
    
    private func isMemberTransformProxy() -> Bool {
        return self.title == "会员转成代理"
    }
    
}

