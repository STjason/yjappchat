//
//  gunRedEnvelopeViewController.swift
//  hb
//
//  Created by ken on 2019/4/14.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit


extension String {
    func mySubString(to index: Int) -> String {
        return String(self[..<self.index(self.startIndex, offsetBy: index)])
    }
    
    func mySubString(from index: Int) -> String {
        return String(self[self.index(self.startIndex, offsetBy: index)...])
    }
}

class gunRedEnvelopeViewController: BaseController,CAAnimationDelegate{
    @IBOutlet weak var lababag: UIImageView!
    @IBOutlet weak var laba: UIImageView!
    static var timeCount = 1
    static var timeMiaoCount = 1
    @IBOutlet weak var dayslable: UILabel!
    @IBOutlet weak var fenView: UIView!
    @IBOutlet weak var tianView: UIView!
    @IBOutlet weak var shitianView: UIView!
    @IBOutlet weak var shiifenView: UIView!
    @IBOutlet weak var haomiaoView: UIView!
    @IBOutlet weak var miaoView: UIView!
    @IBOutlet weak var open: UIButton!
    @IBOutlet weak var shitian: UILabel!
    @IBOutlet weak var tian: UILabel!
    @IBOutlet weak var shifen: UILabel!
    @IBOutlet weak var fen: UILabel!
    @IBOutlet weak var miao: UILabel!
    @IBOutlet weak var haomiao: UILabel!
    var timer:DispatchSource?
    private let noticeUI = JXMarqueeView()
    private let noticeUI_label = UILabel()
//    var openkey = true
//    var timer:Timer?
    var imageV:UIImageView?
    var redPacketId = 0
    var redPacDic = Dictionary<String, NSObject>()
    //红包规则
    var rule: ActiveResult = ActiveResult(){
        didSet{
            openActiveDetail(controller: self, title: "红包规则",content:rule.content,foreighUrl:"")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewNoticeUI()
        viewUI()
        RenEnvelope()
        loadWebData()
    }
    
    func viewNoticeUI(){
        noticeUI.backgroundColor = UIColor.clear
        noticeUI.contentMargin = 50
        noticeUI.marqueeType = .left
        lababag.addSubview(noticeUI)
        
        noticeUI.snp.makeConstraints { (make) in
            make.centerY.equalTo(lababag)
            make.height.equalTo(lababag.snp.height)
            make.left.equalTo(laba.snp.right).offset(10)
            make.right.equalTo(lababag.snp.right).offset(-10)
        }


        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
        timer?.suspend()
        guard let t = timer else {
            return
        }
        t.cancel()
//        timer? = nil
    }

    func viewUI(){
        
        viewLayer(view: fenView)
        viewLayer(view: tianView)
        viewLayer(view: shitianView)
        viewLayer(view: shiifenView)
        viewLayer(view: haomiaoView)
        viewLayer(view: miaoView)
    }
    func viewLayer(view:UIView){
        view.layer.masksToBounds = true;
        view.layer.cornerRadius = 5
    }
    @IBAction func openRenEnvelope(_ sender: UIButton) {
        RenEnvelopeOpen()
    }
    @IBAction func ruleClick(_ sender: Any) {
        loadRedEnvelopeRule()
    }

    @IBAction func exit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
   
    func RenEnvelope(){
        imageV = UIImageView.init(image: UIImage.init(named: "hongbao"))
        self.view.addSubview(imageV!)
        
        imageV!.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.height.equalTo(kScreenHeight*0.5)
            make.width.equalTo(kScreenWidth-100)
        }
        imageV!.isHidden=true
      
    } //RenEnvelope
    
    
    func RenEnvelopeOpen(){
        self.imageV!.isHidden=false
        let base = CABasicAnimation.init(keyPath: "transform.rotation.z")
        base.fromValue = NSNumber.init(value:-0.05)
        base.toValue = NSNumber.init(value:0.05)
        base.duration = 0.05;
        base.repeatCount = 10
        base.delegate = self;
        base.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        base.autoreverses = true
        self.imageV?.layer .add(base, forKey: "centerLayer")
    }

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.imageV!.isHidden=true
        openAction()
    }
    
//    resultJson    String    "{\"success\":true,\"accessToken\":\"6578e2d6-9eef-4fa3-87eb-f961e1bd4cbc\",\"content\":{\"beginDatetime\":1552634370000,\"betRate\":1,\"endDatetime\":1556349564000,\"id\":518,\"ipNumber\":0,\"minMoney\":0.01,\"remainMoney\":1.0,\"remainNumber\":7,\"status\":2,\"title\":\"\",\"totalMoney\":1.0,\"totalNumber\":7}}"
    
    
    //获取可用红包信息
    func loadWebData() -> Void {
        request(frontDialog: true,method: .get, loadTextStr:"获取红包数据中...", url:VALID_RED_PACKET_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = RedPacketWraper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            guard let redPacket = result.content else {return}
                            self.redPacketId = redPacket.id
                            self.formatTimeOfRedPackage(startTimestamp: redPacket.beginDatetime, endTimestamp: redPacket.endDatetime)
                            
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: self.view, txt: errorMsg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取失败"))
                            }
                            if (result.code == 0 || result.code == -1) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                        self.getData(withRedPacketId: true)//获取抢红包数据
                    }
        })
    }
    
    //获取红包规则
    func loadRedEnvelopeRule() -> Void {
        request(frontDialog: true,method: .get, loadTextStr:"获取中...",url:VALID_RED_RULE_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败,请重试"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    let result = ActivesResultWraper.deserialize(from: resultJson)
                    self.rule = result?.content?.first ?? ActiveResult()
        })
    }

    func openAction(){
        request(frontDialog: true,method: .post, loadTextStr:"抢红包中...", url:GRAB_RED_PACKET_URL,params:["redPacketId":self.redPacketId],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "抢红包失败"))
                            
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = GrabPacketWraper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            guard let money = result.content else {return}
                            if money > 0{
                                self.showGrabDialog(money: money)
                            }
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: self.view, txt: errorMsg)
                                
                            }else{
                                showToast(view: self.view, txt: convertString(string: "抢红包失败"))
                            }
                            if (result.code == 0) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    
    func showGrabDialog(money:Float) -> Void {
        let message = String.init(format: "恭喜您抢到了%.2f元", money)
        showToast(view:self.view, txt: message)
    }
        
    
    
    func formatTimeOfRedPackage(startTimestamp: Int64,endTimestamp: Int64) {
        dispatchTimer(timeInterval: 1) { (timer) in
            let timeTips = self.formatTime(startTimestamp: startTimestamp, endTimestamp: endTimestamp)
            var timeTips64 = 0
            timeTips64 = Int(timeTips["dictionaryType"] as! Int64)
            if timeTips64 == 0 {
                self.shitian.text=String(format:"%02d",timeTips["hours"]!).mySubString(to: 1)
                self.tian.text=String(format:"%02d",timeTips["hours"]!).mySubString(from: 1)
                self.shifen.text=String(format:"%02d",timeTips["minutes"]!).mySubString(to: 1)
                self.fen.text=String(format:"%02d",timeTips["minutes"]!).mySubString(from: 1)
                self.miao.text=String(format:"%02d",timeTips["seconds"]!).mySubString(to: 1)
                self.haomiao.text=String(format:"%02d",timeTips["seconds"]!).mySubString(from: 1)
                let day = "距离运气红包结束日期有:\(String(format:"%02d",timeTips["days"]!))天"
                self.dayslable.text=day
                
                //                    if self.openkey {
                //                    self.openkey = false
                //                    let day = "距离运气红包结束日期有:\(String(format:"%02d",timeTips["days"]!))天"
                //                    self.open .setTitle(day, for: UIControl.State.normal)
                //                    }
                
            }else{
                self.dayslable.text = "活动已结束"
                timer?.suspend()
                guard let t = timer else {
                    return
                }
                t.cancel()
            }
        }
    }
        
    
    func dispatchTimer(timeInterval: Double, handler:@escaping (DispatchSourceTimer?)->()){
        timer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main) as? DispatchSource
        timer?.schedule(deadline: .now(), repeating: timeInterval)
        timer?.setEventHandler {
            DispatchQueue.main.async {
                handler(self.timer)
            }
        }
        timer?.resume()
    }
    
    
    // 该方法存冗余代码，需要抽象
    func formatTime(startTimestamp: Int64,endTimestamp: Int64) -> Dictionary<String, Int64>{
        
        var mDict = Dictionary<String, Int64>()
    
        let nowTimestamp = Int64(NSDate().timeIntervalSince1970) * 1000 //- (8 * 3600 * 1000)
        
        let startToNow = startTimestamp - nowTimestamp
        let endToNow = endTimestamp - nowTimestamp
        
        let allToStartSeconds =  abs(startToNow / 1000)
        let allToEndSeconds = abs(endToNow / 1000)
        if startToNow >= 0 // 活动未开始
        {
            let days = allToStartSeconds / (24 * 3600)
            let hours = ( allToStartSeconds % (24 * 3600) ) / 3600
            let minutes = (allToStartSeconds % 3600) / 60
            let seconds = allToStartSeconds % 60
            mDict["days"] = 0
            mDict["hours"] = 0
            mDict["minutes"] = 0
            mDict["seconds"] = 0

            if days > 0
            {
                mDict["days"] = days
            }
            
            if hours > 0 || days > 0
            {
                 mDict["hours"] = hours
            }
            
            if minutes > 0 || days > 0 || hours > 0
            {
                 mDict["minutes"] = minutes
            }
            
            mDict["seconds"] = seconds
            mDict["dictionaryType"] = 0
            return mDict
            
        }else // 活动已经开始，或者结束
        {
            if endToNow >= 0 // 活动已经开始，且没有结束
            {
                let days = allToEndSeconds / (24 * 3600)
                let hours = ( allToEndSeconds % (24 * 3600) ) / 3600
                let minutes = (allToEndSeconds % 3600) / 60
                let seconds = allToEndSeconds % 60
                mDict["days"] = 0
                mDict["hours"] = 0
                mDict["minutes"] = 0
                mDict["seconds"] = 0
                if days > 0
                {
                      mDict["days"] = days
                }
                
                if hours > 0 || days > 0
                {
                      mDict["hours"] = hours
                }
                
                if minutes > 0 || days > 0 || hours > 0
                {
                     mDict["minutes"] = minutes
                }
                
                  mDict["seconds"] = seconds
                  mDict["dictionaryType"] = 0
                
                return mDict
            }else // 活动已经结束
            {
                 mDict["dictionaryType"] = 1
                return mDict
            }
        }
    } //formatTime
    
    
    func getData(withRedPacketId:Bool=false) {
        var params:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        params["code"] = 1 as AnyObject
        if withRedPacketId{
            params["redPacketId"] = self.redPacketId as AnyObject
        }
        request(frontDialog: false,method: .post, loadTextStr:"获取记录中...", url:FAKE_DATA_URL_V2,params:params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    
                    if let result = FakeDataWraper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                YiboPreference.setToken(value: token as AnyObject)
                            }
                            if let redPacket = result.content {
                                self.updateNotice(str: redPacket)
                            }
                        }else{
                            if let errorMsg = result.msg{
                                Tool.confirm(title: "温馨提示", message: errorMsg, controller: self)
                            }
                            if (result.code == 0 || result.code == 10000) {
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    func updateNotice(str:[NewFakeData] = []) -> Void {
        var notices = ""
        for item in str{
            notices = notices + "     \(item.account)喜中:\(item.money)元"
        }
        notices = notices.replacingOccurrences(of: "\n", with: "")
        noticeUI_label.text = notices
        noticeUI_label.textColor = UIColor.red
        noticeUI_label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        noticeUI.contentView = noticeUI_label
    }

}
