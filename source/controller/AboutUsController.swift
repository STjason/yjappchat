//
//  AboutUsController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/19.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

class AboutUsController: UIViewController {

    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var appName:UILabel!
    @IBOutlet weak var appVersion:UILabel!
    
    var shouldOpenTheDoor = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupthemeBgView(view: self.view)
        appName.text = getAppName()
        appVersion.text = getVerionName()
        icon.image = UIImage.init(named: "AppIcon")
        
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: appName)
        setThemeLabelTextColorGlassBlackOtherDarkGray(label: appVersion)
        
        #if DEBUG
           self.setupBack_door()
        #else
        #endif
    }

    private func setupBack_door() {
        let leftTopButton = UIButton()
        self.view.addSubview(leftTopButton)
        leftTopButton.whc_Top(64).whc_Left(0).whc_Width(60).whc_Height(60)
        leftTopButton.backgroundColor = UIColor.clear
        leftTopButton.addTarget(self, action: #selector(leftTopButtonAction), for: .touchUpInside)
        
        let rightBottomButton = UIButton()
        self.view.addSubview(rightBottomButton)
        rightBottomButton.whc_Bottom(0).whc_Right(0).whc_Width(60).whc_Height(60)
        rightBottomButton.backgroundColor = UIColor.clear
        rightBottomButton.addTarget(self, action: #selector(rightBottomButtonAction), for: .touchUpInside)
    }

    @objc private func leftTopButtonAction() {
        shouldOpenTheDoor += 1
    }


    @objc private func rightBottomButtonAction() {
        if shouldOpenTheDoor == 1 {
            self.navigationController?.pushViewController(ConfigDomainURLViewController(), animated: true)
        }
    }
}
