//
//  StartupController.swift
//  
//
//  Created by yibo-johnson on 2017/12/13.
//

import UIKit
import Kingfisher
import HandyJSON

class StartupController: BaseController {
    
    @IBOutlet  weak var jumpBtn:UIButton!
    @IBOutlet  weak var viewDomainBtn:UIButton!
    @IBOutlet  weak var imageUI:UIImageView!
    private var openDoorValue = 0
    
    @IBAction func openDoorFirstAction() {
        openDoorValue += 1
    }
    
    @IBAction func openDoorSecondAction() {
        if openDoorValue == 1
        {
            #if DEBUG
                YiboPreference.setOpenConfigURLPage(value: "on")
            #else
            #endif
        }
    }
    
    var downTimer:Timer?//动态密码倒计时器
    var COUNT_SECONDS = 3
    var main_page_style = OLD_CLASSIC_STYLE
    
    var gotRealDomain = false//是否获取到了真实域名
    var count_acquire_seconds = 10
    var acquireDomainTimer:Timer?//获取域名倒计时器
    var acquire_domain_time = 0;//第几次获取域名
    var fix_urls:[String] = []
    lazy var popAlertView:PopupAlertView = {
        let popView = PopupAlertView.init(frame:view.bounds)
        self.view.addSubview(popView)
        return popView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        viewDomainBtn.isHidden = false;
        
        YiboPreference.setOpenConfigURLPage(value: "off")
        
        jumpBtn.addTarget(self,action:#selector(startAcquireDomainCountDown),for:.touchUpInside)
        viewDomainBtn.addTarget(self,action:#selector(onDomainClick),for:.touchUpInside)
        jumpBtn.layer.cornerRadius = 5
        //开始计时
        startCountDown()
        
        fix_urls = [FIX_NATIVE_BASE_URL_1,
                    FIX_NATIVE_BASE_URL_2,
                    FIX_NATIVE_BASE_URL_3,
                    FIX_NATIVE_BASE_URL_4,
                    FIX_NATIVE_BASE_URL_5,
                    FIX_NATIVE_BASE_URL_6];
        //refresh launcher img
        if !isEmptyString(str: YiboPreference.getLauncherUrl()){
            let imageURL = URL(string: YiboPreference.getLauncherUrl())
            if let url = imageURL{
                imageUI.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "startup"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
    }
    
    @objc func onJumpController() -> Void {
        let hasShow = UserDefaults.standard.bool(forKey: "showedPicture")
        
        guard let sys = getSystemConfigFromJson() else{return}
        let show_welcome_pages = sys.content.show_welcome_pages
        
        if !hasShow && show_welcome_pages == "on"{
            let guideViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "guideViewController")
            let pictureVC = guideViewController
            pictureVC.modalPresentationStyle = .fullScreen
            self.present(pictureVC, animated: true, completion: nil)
        }else{
            if YiboPreference.getAutoLoginStatus(){
                actionLogin(username: YiboPreference.getUserName(), pwd: YiboPreference.getPwd())
            }else{
                YiboPreference.saveLoginStatus(value: false as AnyObject)
                openMain()
            }
        }
    }
    
    func openMain() -> Void {
        goMainScreen(controller: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let themePlistName = YiboPreference.getCurrentThmeByName()
        setupTheme(immediately:isEmptyString(str: themePlistName) ? true : false)
        
        if let timer = self.downTimer{
            timer.invalidate()
        }
    }
    
    override func  viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    // 隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    func actionLogin(username:String,pwd:String) -> Void {
        if isEmptyString(str: username){
            self.navigationController?.popViewController(animated: true)
            openMain()
            return
        }
        if isEmptyString(str: pwd){
            self.navigationController?.popViewController(animated: true)
            openMain()
            return
        }
        
        let staionID = stationID()
        let versionName = getVerionName()
        let iphoneModel = iphoneType()
        
        let preffix = staionID + ":" + "ios/\(versionName)" + ":" + "iPhone" + "-" + iphoneModel
        let source = preffix + "---\(username)" + "---\(pwd)" + "---"
        
        let keyEncrypt = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: preffix)
        let sourceEncrypt = Encryption.Endcode_AES_ECB(key: aesKey, iv: aesIV, strToEncode: source)
        
        let params = ["key":keyEncrypt,
                      "data":sourceEncrypt,
                      "apiVersion":1] as [String : Any]
        
//        print("---------> 登录参数")
//                print("preffix: \(preffix)")
        //        print("source: \(source)")
        //        print("keyEncrypt: \(preffix)")
        //        print("sourceEncrypt: \(source)")
                
//        print("---------> 请求头参数")
//        let headerString = defaultHeader["User-agent"] ?? ""
//        print("defaultHeader[\"User-agent\"]: \(headerString)")
        
        request(frontDialog: true, method: .post, loadTextStr: "正在登录中...", url:LOGIN_NEW_URL,params:params,
                callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                    
                    guard let weakSelf = self else {return}
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: weakSelf.view, txt: convertString(string: "自动登录失败"))
                        }else{
                            showToast(view: weakSelf.view, txt: resultJson)
                        }
                        YiboPreference.saveLoginStatus(value: false as AnyObject)
                        YiboPreference.savePwd(value: "" as AnyObject)
                        YiboPreference.saveUserName(value: "" as AnyObject)
                        weakSelf.openMain()
                        return
                    }
                    
                    if let result = LoginResultWraper.deserialize(from: resultJson){
                        if result.success{
//                            showToast(view: self.view, txt: "登录成功")
                            YiboPreference.saveLoginStatus(value: true as AnyObject)
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            
                            weakSelf.isSixMarkRequest()
                            
                            //记住密码
                            if (YiboPreference.getPwdState()) {
                                YiboPreference.savePwd(value: pwd as AnyObject)
                            }else{
                                YiboPreference.savePwd(value: "" as AnyObject)
                            }
                            if let content = result.content{
                                if let accountType = content.accountType{
                                    YiboPreference.setAccountMode(value:accountType as AnyObject)
                                }
                                let isUpdatePwd = content.updatePwd
//                                isUpdatePwd = true
                                UserDefaults.standard.set(isUpdatePwd, forKey:"isUpdatePwd")
                            }
                            
                            weakSelf.navigationController?.popViewController(animated: true)
                            
                         
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: weakSelf.view, txt: errorMsg)
                            }else{
                                showToast(view: weakSelf.view, txt: convertString(string: "自动登录失败"))
                            }

                            YiboPreference.saveLoginStatus(value: false as AnyObject)
                        }
                    }
                    weakSelf.openMain()
        })
    }
    
    //是否是六合彩接口判断,返回数组
    func isSixMarkRequest() {
        request(frontDialog: false, method: .get, url: IS_SIXMARK, callback: { (resultJson:String,resultStatus:Bool) -> Void in
            if !resultStatus {
                return
            }
            
            class SixMarkModel:HandyJSON {
                required init() {}
                var success = false
                var accessToken = ""
                var content = [String]()
            }
            
            if let result = SixMarkModel.deserialize(from: resultJson){
                if result.success{
                    sixMarkArray = result.content
                    if !isEmptyString(str: result.accessToken){
                        YiboPreference.setToken(value: result.accessToken as AnyObject)
                    }
                    
                }else {}
            }
        })
    }
 
    
    @objc func updateTime(timer:Timer) {
        COUNT_SECONDS -= 1
        if COUNT_SECONDS > 0{
            updatejumpTxt(second:COUNT_SECONDS)
        }
        if COUNT_SECONDS == 0 {
            jumpBtn.setTitle(String.init(describing: String.init(describing: "跳过")), for:.normal)
            startAcquireDomainCountDown()
        }
    }
    
    func updatejumpTxt(second:Int) ->  Void{
        jumpBtn.setTitle(String.init(format: "跳过 %d", second), for:.normal)
    }
    
    func startCountDown() -> Void {
        //延时1秒执行
        let time: TimeInterval = 1
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
            if self.downTimer == nil{
                self.downTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime(timer:)), userInfo: nil, repeats: true)
            }
        }
    }
    
  
    
    deinit {
        downTimer?.invalidate()
        downTimer = nil;
        
        acquireDomainTimer?.invalidate()
        acquireDomainTimer = nil
    }
  
    
    
    func syncLauncher(baseurl:String) -> Void {
        request(baseUrl:baseurl,frontDialog: false,loadTextStr:"launcher..", url:GET_LAUNCHER_IMG_URL,params:["pid":getAPPID()],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        print("sync launcher fail")
                        return
                    }
                    if let result = LauncherWraper.deserialize(from: resultJson){
                        if result.success{
                            let url = result.imgUrl
                            print("domain launcher imgurl ==",url)
                            if !isEmptyString(str: url){
                                YiboPreference.setLauncherUrl(value: url as AnyObject)
                            }else{
                                YiboPreference.setLauncherUrl(value: "" as AnyObject)
                            }
                        }else{
                            if let errorMsg = result.msg{
                                print("sync launcher fail",errorMsg)
                            }else{
                                print("sync launcher fail")
                            }
                        }
                    }
        })
    }
    
    /**
     请求网站真正的域名
     **/
    func syncRealWebsiteDomain(baseUrl:String) -> Void {
        //获取启动图图片地址
        syncLauncher(baseurl: baseUrl)
        request(baseUrl:baseUrl,frontDialog: true,loadTextStr:"正在同步最优线路...", url:GET_REAL_DOMAIN_URL,params:["pid":getAPPID()],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if self.gotRealDomain{
                        return
                    }
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "同步失败,请重试"))
                            //                            print("sync real domain error = ","同步失败,请重试")
                        }else{
                            showToast(view: self.view, txt: resultJson)
                            //                            print("sync real domain error = ",resultJson)
                        }
                        //跳转路由器测试
                        self.gojumpTouterTest()
                        return
                    }
                    if let result = RealDomainWraper.deserialize(from: resultJson){
                    
                        if result.success{
                            
                            // 聊天室配置
                            if !self.gotRealDomain {
                                if let url = result.domainUrl{
                                    if !isEmptyString(str: url){
                                        BASE_URL = url
                                    }
                                }
                                self.gotRealDomain = true
                                self.acquireDomainTimer?.invalidate()
                                self.acquireDomainTimer = nil
                            }
                            // 加载配置
                            self.loadSysConfigData()
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: self.view, txt: errorMsg)
                                print("sync real domain error = ",errorMsg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "同步失败,请重试"))
                                print("sync real domain error = ","同步失败,请重试")
                            }
                            //跳转路由器测试
                            self.gojumpTouterTest()
                        }
                    }
        })
    }
    
    func doAfterDomain()
    {
        self.checkVersion(signType: YiboPreference.getUpdatePayment())
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        downTimer?.invalidate()
        downTimer = nil;
        
        acquireDomainTimer?.invalidate()
        acquireDomainTimer = nil
    }
    
    func viewDomainDialog(domain:String) {
        
        let message = "域名:"+domain
        let alertController = UIAlertController(title: convertString(string: "App域名"),message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "复制", style: .default, handler: {
            action in
            UIPasteboard.general.string = domain
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showUpdateDialog(version:String,content:String,url:String) ->Void{
        
        let Dict:[String:String] = ["version":version,"content":content,"url":url]
        self.popAlertView.assignmentData = Dict
        self.popAlertView.updateCallClose = { (url) in
            let alertController = UIAlertController(title: "若您使用的是超级签名APP请点击超级签名进行版本更新，以免出现安装失败的情况", message: "", preferredStyle:.alert)
            let enterpriseAction = UIAlertAction(title: "企业签名", style: .default) { (action) in
                openBrower(urlString: url)
                YiboPreference.saveUpdatePayment(value: "0" as AnyObject)
            }
            let superSignAction = UIAlertAction(title: "超级签名", style: .default) { (action) in
                openBrower(urlString: url)
                YiboPreference.saveUpdatePayment(value: "1" as AnyObject)
            }
            alertController.addAction(enterpriseAction)
            alertController.addAction(superSignAction)
            self.present(alertController, animated: true, completion: {
                alertController.tapAlert();
            })
        }
        //取消更新的回调
        self.popAlertView.cancelUpdateCallColse = {[weak self] () in
            if getSystemConfigFromJson()?.content.force_install_when_new_version == "on" {
                exit(0)
            }else{
                self?.onJumpController()
            }
        }
    }
    
    
    @objc func loadSysConfigData() {
        request(frontDialog: true,method: .get,loadTextStr: "加载配置中...", url:SYS_CONFIG_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取配置失败,请重试"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        self.gojumpTouterTest()
                        return
                    }
                    print(resultJson)
                    if let result = SystemWrapper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                UserDefaults.standard.setValue(token, forKey: "token")
                            }
                            //save lottery version
                            if let config = result.content{
                                if !isEmptyString(str: config.yjf){
                                    YiboPreference.setYJFMode(value: config.yjf as AnyObject)
                                }
                                YiboPreference.saveMallStyle(value: config.native_style_code as AnyObject)
                                YiboPreference.setMallImageTextTabStyle(value: config.switch_optimize_mainpage_tabs as AnyObject)
                            }
                            //save system config to user default
                            YiboPreference.saveConfig(value: resultJson as AnyObject)
                            setupTheme()
                            self.doAfterDomain()
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: self.view, txt: errorMsg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取配置失败,请重试"))
                            }
                            self.gojumpTouterTest()
                        }
                    }
        })
    }
}

//MARK:路由器检测跳转
extension StartupController{
    
    private func gojumpTouterTest(){
        let alertVC = UIAlertController(title: "温馨提示",
                                        message: "当前网络环境较差，请检测路由器状况",
                                        preferredStyle:UIAlertController.Style.alert)
        
        let sureAction = UIAlertAction(title: "去路由检测",
                                       style: UIAlertAction.Style.default, handler: { (action) in
            //去路由检测
            let routerTextVC = TracerouteController()
            
            let navgationNav = MainNavigationController(rootViewController: routerTextVC)
            //从启动页跳转过去
            routerTextVC.isFromStartUp = true
                                        
                                        self.acquireDomainTimer?.invalidate()
                                        self.acquireDomainTimer = nil;
                                        
                                        self.acquire_domain_time = 0
            
            self.present(navgationNav, animated: true, completion: nil)
        })
        
        let  cancelAction = UIAlertAction(title: "取消",
                                          style: UIAlertAction.Style.default,
                                          handler: nil)
        
        alertVC.addAction(sureAction)
        
        alertVC.addAction(cancelAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
}

//MARK:-- 事件响应
extension StartupController{
    /**
     开始获取真实域名定时器，10秒内没有返回域名则换下一条固定域名来获取
     **/
    @objc private func startAcquireDomainCountDown(){
        //先开始第一次获取
        if acquire_domain_time < fix_urls.count {
            
            syncRealWebsiteDomain(baseUrl: fix_urls[acquire_domain_time])
        }else{
            
            acquire_domain_time = 0
            
        }
        
        //延时1秒执行
        let time: TimeInterval = 0.1
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
            if self.acquireDomainTimer == nil{
                self.acquireDomainTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.tickDomain(timer:)), userInfo: nil, repeats: true)
            }
        }
    }
    
    
    @objc private func tickDomain(timer:Timer) {
        
        if self.gotRealDomain{
            print("domain have got it")
            return
        }
        print("domain tick 1111")
        count_acquire_seconds -= 1
        if count_acquire_seconds > 0{
            //.....
        }
        if count_acquire_seconds == 0 {
            print("domain tick finish one more time now")
            gotRealDomain = false
            acquire_domain_time += 1
            count_acquire_seconds = 8
            if acquire_domain_time < fix_urls.count{
                print("domain tick finish one more time now,url = ",fix_urls[acquire_domain_time])
                syncRealWebsiteDomain(baseUrl: fix_urls[acquire_domain_time])
            }
        }
    }
    
    @objc private func onDomainClick() {
        
        let domainValue = Bundle.main.infoDictionary!["domain_url"] as! String
        
        viewDomainDialog(domain:domainValue)
    }
}

extension StartupController{
    
    @objc func checkVersion(signType: String){
        
        let updateUrl = getDownloadPageURL()
        
        request(frontDialog: true, method: .post, loadTextStr: "版本检测中...", url:updateUrl,params:["curVersion":getVerionName(),"appID":getAPPID(),"platform":"ios","superSign":signType],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    var findNewVersion = false
                    if resultStatus{
                        if let result = CheckUpdateWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                if let content = result.content{
                                    if !isEmptyString(str: content.url){
                                        let url = content.url
                                        
                                        self.showUpdateDialog(version: content.version, content: content.content, url: url)
                                        findNewVersion = true
                                        return
                                    }
                                }
                            }
                        }
                    }else {
                        
                        let alert = UIAlertController.init(title: "提示", message: resultJson, preferredStyle: .alert)
                        let action = UIAlertAction.init(title: "确定", style: .cancel, handler: nil)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                        
                        return
                    }
                    if !findNewVersion{
                        //没有找到新版本时，直接跳转主页
                        self.onJumpController();
                    }
        })
    }
}
