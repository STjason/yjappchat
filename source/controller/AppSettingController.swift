//
//  AppSettingController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/4.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher

class AppSettingController: BaseController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var isSelect = false
    var from = 0;//从哪个模块进入的
    
    var datas = [["自动登录","摇一摇投注","下注声音","不再弹出公告","悬浮工具","消息推送","显示悬浮窗", "站内信弹窗"],
                 ["安全中心"],
                 ["切换主题","路由检测"],
                 ["清理缓存","清除支付浏览器"],
                 ["版本检测","关于我们"],
                 ["退出登录"]]
    
    func loadSysConfigData() {
        request(frontDialog: false,method: .get,loadTextStr: "加载配置中...", url:SYS_CONFIG_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取配置失败,请重试"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = SystemWrapper.deserialize(from: resultJson){
                        if result.success{
                            if let token = result.accessToken{
                                UserDefaults.standard.setValue(token, forKey: "token")
                            }
                            //save lottery version
                            if let config = result.content{
                                if !isEmptyString(str: config.yjf){
                                    YiboPreference.setYJFMode(value: config.yjf as AnyObject)
                                }
                                YiboPreference.saveMallStyle(value: config.native_style_code as AnyObject)
                            }
                            //save system config to user default
                            YiboPreference.saveConfig(value: resultJson as AnyObject)
                            
                            setupTheme()
                        }else{
                            if let errorMsg = result.msg{
                                showToast(view: self.view, txt: errorMsg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取配置失败,请重试"))
                            }
                        }
                    }
        })
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        let themePlistName = YiboPreference.getCurrentThmeByName()
        if isEmptyString(str: themePlistName) {
            setupTheme(immediately:true)
        }
    }

    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        if #available(iOS 11, *) {} else {
            if self.responds(to: #selector(setter: edgesForExtendedLayout)) {
                self.edgesForExtendedLayout = UIRectEdge.bottom
            }
        }
        
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if shouldShowNoticeWIndow() {}
        }
        if YiboPreference.isShouldAlert_isAll() == "off"{
            if shouldShowNoticeWIndow() {}
        }

        setupthemeBgView(view: self.view, alpha: 0)
        
        self.navigationItem.title = "设置"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                // 拦截试玩账号
                if judgeIsMobileGuest() || YiboPreference.getAccountMode() == GUEST_TYPE {
                    showErrorHUD(errStr: "操作权限不足，请联系客服！")
                    return
                }
                let safetyCenterVc = SafetyCenterViewController()
                self.navigationController?.pushViewController(safetyCenterVc, animated: true)
            }
        }
       else if indexPath.section == 2 {
            let row = indexPath.row
            if row == 0 {
                switchTheme()
            }else if row == 1{
                traceroute()
            }
        }
        else if indexPath.section == 3 {
            if indexPath.row == 0{
                ////图片异步加载框架清理内存缓存
                KingfisherManager.shared.cache.clearMemoryCache()
                // 清理硬盘缓存，这是一个异步的操作
                KingfisherManager.shared.cache.clearDiskCache()
                // 清理过期或大小超过磁盘限制缓存。这是一个异步的操作
                KingfisherManager.shared.cache.cleanExpiredDiskCache()
                URLCache.shared.removeAllCachedResponses()
                YiboPreference.setCurrentTheme(value: 0 as AnyObject)
                YiboPreference.setCurrentThemeByName(value: "" as AnyObject)
                YiboPreference.setLauncherUrl(value: "" as AnyObject)
                CRDefaults.setLastRoom(value: "") //清除聊天室，最后进入的房间id
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    showToast(view: self.view, txt: "清理成功")
                }
                
                loadSysConfigData()
            }else if indexPath.row == 1 {
                YiboPreference.setDefault_brower_type(value: "-1")
                tableView.reloadRows(at: [IndexPath.init(row: 2, section: 2)], with: .fade)
                showToast(view: self.view, txt: "清除默认支付浏览器成功")
            }
            
        }else if indexPath.section == 4{
            if indexPath.row == 0{
                let alertController = UIAlertController(title: "若您使用的是超级签名APP请点击超级签名进行版本更新，以免出现安装失败的情况", message: "", preferredStyle:.alert)
                let enterpriseAction = UIAlertAction(title: "企业签名", style: .default) { (action) in
                    
                    checkVersion(controller: self, showDialog: true, showText: "正在检测新版本...",showError:true, isUpdate: true, isSuperSign: false)
                    YiboPreference.saveUpdatePayment(value: "0" as AnyObject)
                }
                let superSignAction = UIAlertAction(title: "超级签名", style: .default) { (action) in
                   
                    checkVersion(controller: self, showDialog: true, showText: "正在检测新版本...",showError:true, isUpdate: true, isSuperSign: true)
                    YiboPreference.saveUpdatePayment(value: "1" as AnyObject)
                }
                alertController.addAction(enterpriseAction)
                alertController.addAction(superSignAction)
                self.present(alertController, animated: true) {
                    alertController.tapAlert()
                }
            }else if indexPath.row == 1{
                openAboutus(controller: self)
            }
        }else if indexPath.section == 5{
            showConfirmExitDialog()
        }
    }
    
    func showConfirmExitDialog() -> Void {
        let message = "确定要退出吗？"
        let alertController = UIAlertController(title: "温馨提示",
                                                message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "好的", style: .default, handler: {
            action in
            self.postExitLogin()
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func postExitLogin() -> Void {
        request(frontDialog: true, loadTextStr:"退出登录中...",url:LOGIN_OUT_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "退出失败,请重试"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = LoginOutWrapper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }
                            YiboPreference.saveLoginStatus(value: false as AnyObject)
                            CRDefaults.setLastRoom(value: "")
                            socket?.disconnect()
                            socket = nil
                            
                            //试玩站号推出时清除账号
                            if YiboPreference.getAccountMode() == GUEST_TYPE{
                                YiboPreference.saveUserName(value: "" as AnyObject)
                                YiboPreference.savePwd(value: "" as AnyObject)
                                YiboPreference.setAccountMode(value: 0 as AnyObject)
                            }
                            
                            YiboPreference.setCACHEAVATARdata(value: Data.init())
                            if self.from == 1{
                                loginWhenSessionInvalid(controller: self)
                            }else{
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "signOutToFirstTabNoti"), object: nil)
                                self.navigationController?.popViewController(animated: false)
                            }
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "退出失败,请重试"))
                            }
                        }
                    }
        })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if shouldShowBrowsersChooseView() {
            return datas[section].count
        }else {
            if section == 3 {
                return datas[section].count - 1
            }else {
                return datas[section].count
            }
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "appSetting") as? AppSettingCell  else {
            fatalError("The dequeued cell is not an instance of JianjinPaneCell.")
        }
        cell.txt.text = datas[indexPath.section][indexPath.row]
        cell.discriptionLabel.isHidden = true
        if indexPath.section == 0{
            cell.toggle.isHidden = false
            cell.moreImg.isHidden = true
            if indexPath.row == 0{
                cell.toggle.tag = 0
                cell.toggle.isOn = YiboPreference.getAutoLoginStatus()
            }else if indexPath.row == 1{
                cell.toggle.tag = 1
                cell.toggle.isOn = YiboPreference.getShakeTouzhuStatus()
            }else if indexPath.row == 2{
                cell.toggle.tag = 2
                cell.toggle.isOn = YiboPreference.isPlayTouzhuVolume()
            }else if indexPath.row == 3{
                cell.toggle.tag = 3
                cell.toggle.isOn = YiboPreference.isShouldAlert_isAll() == "off"
            }else if indexPath.row == 4{
                cell.toggle.tag = 4
                cell.toggle.isOn = YiboPreference.isShouldOpenFloatTool() == "on"
            }else if indexPath.row == 5{
                cell.toggle.tag = 5
                cell.toggle.isOn = UserDefaults.standard.string(forKey: "FLOAT_OPEN_PUSH") == "open"
            }else if indexPath.row == 6{
                cell.toggle.tag = 6
                cell.toggle.isOn = YiboPreference.getFloatingWindowONdata()
            }else if indexPath.row == 7{
                //站内信开关
                cell.toggle.tag = 8
                cell.toggle.isOn = YiboPreference.getInsideStation()
            }

            cell.toggle.addTarget(cell, action: #selector(cell.onSwitchAction(view:)), for: UIControl.Event.valueChanged)
        }else{
            cell.toggle.isHidden = true
            cell.moreImg.isHidden = false
        }
        
        if indexPath.section == 5 {
            cell.exitlogin.isHidden = false
            cell.toggle.isHidden = true
            cell.moreImg.isHidden = true
            cell.txt.isHidden = true
        }else if indexPath.section == 3 && indexPath.row == 2{
            let tipsLable = UILabel()
            cell.contentView.addSubview(tipsLable)
            tipsLable.text = getDefaultName()
            tipsLable.textColor = UIColor.darkGray
            tipsLable.font = UIFont.systemFont(ofSize: 14.0)
            tipsLable.whc_Right(40).whc_CenterY(0)
        }
        
        
        if indexPath.section != 5 {
            cell.exitlogin.isHidden = true
            cell.txt.isHidden = false
        }
        
        return cell
    }
    
    private func getDefaultName() -> String {
        let nameType = YiboPreference.getDefault_brower_type()
        var name = ""
        if nameType == -1{
            return name
        }
        if nameType == BROWER_TYPE_SAFARI {
            name = "Safari浏览器"
        }else if nameType == BROWER_TYPE_QQ{
            name = "QQ浏览器"
        }else if nameType == BROWER_TYPE_GOOGLE{
            name = "谷歌浏览器"
        }else if nameType == BROWER_TYPE_UC{
            name = "UC浏览器"
        }else if nameType == BROWER_TYPE_FIREFOX{
            name = "火狐浏览器"
        }
        return name
    }
    
    private func switchTheme() {
        if (isSelect == false) {
            isSelect = true
            onItemClick(dataSource: themes,viewTitle: "主题风格切换")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.isSelect = false
            }
        }
    }
    
    /// 路由检测
    private func traceroute() {
        let vc = TracerouteController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func onItemClick(dataSource: [String], viewTitle: String){
        
        let currentThmeIndex = YiboPreference.getCurrentThme()
        
        let selectedView = LennySelectView(dataSource: dataSource, viewTitle: viewTitle)
        selectedView.selectedIndex = currentThmeIndex
        selectedView.didSelected = { [weak self, selectedView] (index, content) in
            
            let themeStr = getCurrentThemeListNameWithName(name:content)
            
            ThemeManager.setTheme(plistName: themeStr, path: .mainBundle)
            YiboPreference.setCurrentThemeByName(value: themeStr as AnyObject)
            YiboPreference.setCurrentTheme(value: index as AnyObject)
        }
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }) { (_) in

        }
    }
}
