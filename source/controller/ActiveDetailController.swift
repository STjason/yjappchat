//
//  ActiveDetailController.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/2/2.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
import WebKit
//优惠活动详情
class ActiveDetailController: BaseController,UIWebViewDelegate,URLSessionDelegate, URLSessionDataDelegate {
   
    
    /** 是否是聊天室进入 */
    var isChat: Bool = false
    
    var NotificationCenteris = false
    var htmlContent = ""
    var titleStr = "优惠活动详情"
    var navChangeToBlack = true
    var progressViewLayer:CALayer!
//    @IBOutlet weak var webView:UIWebView!
    
    lazy var webView:WKWebView = {
        let configuration = WKWebViewConfiguration()
        
        // 自适应屏幕宽度js
        //        let jsString = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        //        let jsString = ""
        //        let wkUserScript = WKUserScript(source: jsString, injectionTime: WKUserScriptInjectionTime.atDocumentEnd, forMainFrameOnly: true)
        let jsString = "document.cookie = 'SESSION=\(YiboPreference.getToken());path=/';"
        let wkUserScript = WKUserScript(source: jsString, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
        
        let wkUController = WKUserContentController()
        wkUController.addUserScript(wkUserScript)
        configuration.userContentController = wkUController
        
        let webView = WKWebView(frame: CGRect(x: 0.0, y: 0.0, width: Double(view.width), height: Double(Int(view.height)/* - (KNavHeight)*/)), configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.sizeToFit()
        view.addSubview(webView)
        return webView;
    }()
    
    var foreignUrl = ""
    var outsideOpen = false
    var CookieIs = false //不影响其他类 需要cookie 就yes
    
    override func viewDidDisappear(_ animated: Bool) {
        if NotificationCenteris{
            NotificationCenter.default.post(name: NSNotification.Name("ActiveDetailController"), object:self,userInfo: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
        if isChat {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
        }else {
            UINavigationBar.appearance().tintColor = navChangeToBlack ? UIColor.black : UIColor.white
            self.navigationController?.navigationBar.tintColor = navChangeToBlack ? UIColor.black : UIColor.white
        }
    }
 
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        webView.y = view.safeAreaInsets.top
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
        if let view = getSemicycleButton() {
            view.isHidden = true
        }
        
        if isChat {
            let navBackgroudImage = UIImage(named: "chatNavBar")
            self.navigationController?.navigationBar.setBackgroundImage(navBackgroudImage?.getNavImageWithImage(image: navBackgroudImage!), for: .default)
        }else {
            UINavigationBar.appearance().tintColor = UIColor.white
            self.navigationController?.navigationBar.tintColor = UIColor.white
        }
    }
    
    func wrapHtml(html:String) -> String{
        var htmlContent = "<html>";
        htmlContent += "<meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"/>"
        htmlContent += "<body>"
        htmlContent += html
        htmlContent += "<script>\n"
        htmlContent += "              function ResizeImages(){\n"
        htmlContent += "                var myimg;\n"
        htmlContent += "                for(var i=0;i <document.images.length;i++){\n"
        htmlContent += "                  myimg = document.images[i];\n"
        htmlContent += "                  myimg.width = " + String.init(format: "%f", kScreenWidth) + ";\n"
        htmlContent += "                }\n"
        htmlContent += "              }\n"
        htmlContent += "              window.onload=function(){ \n"
        htmlContent += "                ResizeImages()\n"
        htmlContent += "                window.location.hash = '#' + document.body.clientHeight;\n"
        htmlContent += "                document.title = document.body.clientHeight;\n"
        htmlContent += "              }\n"
        htmlContent += "              </script>"
        htmlContent += "</body>"
        htmlContent += "</html>"
        return htmlContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //监听网页开始加载
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: [.new,.old], context: nil)
        let progress = UIView(frame: CGRect(x: 0, y: 0, width: Int(webView.frame.width), height: 3))
        webView.addSubview(progress)
        progressViewLayer = CALayer()
        progressViewLayer.backgroundColor = UIColor.colorWithHexString("#52e20b").cgColor
        progress.layer.addSublayer(progressViewLayer!)
        
        progressViewLayer.frame = CGRect(x: 0, y: 0, width: webView.frame.width * 0.1, height: 3)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
//        if #available(iOS 11, *){} else {self.automaticallyAdjustsScrollViewInsets = false}
        
//        webView.scalesPageToFit = true
//        webView.delegate = self
        webView.scrollView.bounces = false
        
        if isEmptyString(str: foreignUrl){
            webView.loadHTMLString(wrapHtml(html: htmlContent), baseURL: URL.init(string: BASE_URL))
        }else{
            
            if isEmptyString(str: foreignUrl) {
                return
            }
            
            if CookieIs {
                let DomainUrl = getDomainUrl()
                let localhostComponents = DomainUrl.components(separatedBy: "//")
                var localhost = "localhost"
                if localhostComponents.count >= 2 {
                    localhost = localhostComponents[1]
                }
                
                var cookieProperties0 =  [HTTPCookiePropertyKey : Any]()
                cookieProperties0[HTTPCookiePropertyKey.name] = "SESSION"
                cookieProperties0[HTTPCookiePropertyKey.value] = YiboPreference.getToken()
                cookieProperties0[HTTPCookiePropertyKey.domain] = localhost
                cookieProperties0[HTTPCookiePropertyKey.originURL] = localhost
                cookieProperties0[HTTPCookiePropertyKey.path] = "/"
                cookieProperties0[HTTPCookiePropertyKey.version] = "0"
                cookieProperties0[HTTPCookiePropertyKey.expires] = Date(timeIntervalSinceNow: 60 * 60)
                cookieProperties0[HTTPCookiePropertyKey.discard] = 0
                
                let cookie0 = HTTPCookie(properties: cookieProperties0)
                HTTPCookieStorage.shared.setCookie(cookie0!)
                //                    loadRequest前获取token
                if let cookies = HTTPCookieStorage.shared.cookies {
                    for cookie in cookies {
                        print("cookies: \(cookie)\n")
                    }
                }
            }
            if let foreignUrl = URL.init(string: foreignUrl){
                var request = URLRequest(url: foreignUrl)
                //                request.addValue("SESSION=\(YiboPreference.getToken())", forHTTPHeaderField: "Cookie")
                let headFields = request.allHTTPHeaderFields
                let cookie = headFields?["SESSION"]
                if cookie == nil {
                    request.addValue("SESSION=\(YiboPreference.getToken())", forHTTPHeaderField: "Cookie")
                }
                webView.load(request)
            }else{
                showToast(view:view, txt: "该链接无效")
            }
            //            }
            
        }
        self.title = titleStr
    }
    
    //移除监听
    deinit {
        
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        
    }
}

//extension ActiveDetailController{
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        showDialog(view: self.view, loadText: "正在载入...")
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        hideDialog()
//        if self.outsideOpen{
//            if let url = webView.request?.url?.absoluteString{
//                if !isEmptyString(str: url) && !url.contains(BASE_URL){
//                    openBrower(urlString: url)
//                    return
//                }
//            }
//        }
//
//
//        if CookieIs {
//            //去掉余额生息网页返回标签
//            //如果多个使用web需传Cookie就根据weburl来执行相应的js交互
////            let js = "javascript:(function() {" +
////                "document.getElementsByClassName(\"header\")[0].style.display=\'none\';" + "" +
////                "document.getElementsByClassName(\"containter\")[0].style.marginTop=\'0\';" + "" +
////            "})()";
////            let  js = "javascript:(function() {" +
////                "document.getElementsByClassName(\"header\")[0].style.display=\'none\';" + "" +
////                "document.getElementById(\"main\").style.marginTop=\'-50px\';" + "" +
////            "})()";
////            webView.stringByEvaluatingJavaScript(from: js)
//        }
//
//        let  js = "javascript:(function() {" +
//            "document.getElementsByClassName(\"header\")[0].style.display=\'none\';" + "" +
//            "document.getElementById(\"main\").style.marginTop=\'-50px\';" + "" +
//        "})()";
//        webView.stringByEvaluatingJavaScript(from: js)
//
//
//
//    }
//
//}

extension ActiveDetailController:WKUIDelegate,WKNavigationDelegate{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showDialog(view: self.view, loadText: "正在载入...")
    }

//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
////        if CookieIs {
//            //去掉余额生息网页返回标签
//            //如果多个使用web需传Cookie就根据weburl来执行相应的js交互
//            let js = "javascript:(function() {" +
//                "document.getElementsByClassName(\"header\")[0].style.display=\'none\';" + "" +
//                "document.getElementsByClassName(\"containter\")[0].style.marginTop=\'0\';" + "" +
//            "})()";
//
//            webView.evaluateJavaScript(js, completionHandler: nil)
////        }
//        hideDialog()
//    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame?.isMainFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {

        if let response = navigationResponse.response as? HTTPURLResponse{
            
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: response.allHeaderFields as! [String : String], for: response.url!)
            
            for cookie in cookies {
                
                HTTPCookieStorage.shared.setCookie(cookie)
                
            }
        }
        decisionHandler(.allow)
        
    }
    
    // 处理拨打电话、发短信、发邮件以及Url跳转等等
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        if navigationAction.request.url?.scheme == "tel" {
            //            DispatchQueue.main.async {
            UIApplication.shared.openURL(navigationAction.request.url!);
            decisionHandler(WKNavigationActionPolicy.cancel)
            //            }
        }
        else if navigationAction.request.url?.scheme == "sms"{
            //短信的处理
            UIApplication.shared.openURL(navigationAction.request.url!);
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else if navigationAction.request.url?.scheme == "mailto"{
            //邮件的处理
            UIApplication.shared.openURL(navigationAction.request.url!);
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        else{
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }
 
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        let cookieStorage = HTTPCookieStorage.shared
        
        let JSFuncString = "function setCookie(name,value,expires){var oDate=new Date();oDate.setDate(oDate.getDate()+expires);document.cookie=name+'='+value+';expires='+oDate;}"
        
        var JSCookieString = JSFuncString
        
        if let cookieArr = cookieStorage.cookies {
            
            for cookie in cookieArr {
                
                let excuteJSString = "setCookie('\(cookie.name)', '\(cookie.value)', 1);"
                
                JSCookieString.append(excuteJSString)
                
            }
            
        }
        
        webView.evaluateJavaScript(JSCookieString, completionHandler: nil)
        
    }
}

//MARK:--监听进度条的值
extension ActiveDetailController{
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress" {
            
            guard let changes = change else { return }
            let newProgress = changes[NSKeyValueChangeKey.newKey] as? Double ?? 0
            let oldProgress = changes[NSKeyValueChangeKey.oldKey] as? Double ?? 0
            //只有在进度大于0.1后再进行变化
            if newProgress > oldProgress && newProgress > 0.1 {
                progressViewLayer?.frame = CGRect(x: 0, y: 0, width: Double(Double(webView.frame.width) * newProgress), height: 3)
            }
            
            // 当进度为100%时，隐藏progressLayer并将其初始值改为0
            if newProgress == 1.0 {
                let time1 = DispatchTime.now() + 0.4
                let time2 = time1 + 0.1
                DispatchQueue.main.asyncAfter(deadline: time1) {
                    self.progressViewLayer.opacity = 0
                }
                DispatchQueue.main.asyncAfter(deadline: time2) {
                    self.progressViewLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 3)
                }
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}
