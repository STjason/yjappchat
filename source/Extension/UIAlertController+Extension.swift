//
//  UIAlertController+Extension.swift
//  gameplay
//
//  Created by Gallen on 2019/9/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

extension UIAlertController{
    func tapAlert(){
        let arrayViews = UIApplication.shared.keyWindow?.subviews
        let viewClass = NSClassFromString("UITransitionView") as! AnyClass
        var backView = arrayViews?[1]
        if (arrayViews?.count)! > 0{
            for (index,view) in (arrayViews?.enumerated())!{
                if view.isMember(of: viewClass){
                    backView = arrayViews?[index]
                }
            }
            backView?.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapClick))
            backView?.addGestureRecognizer(tap)
        }
    }
    
    @objc func tapClick(){
        dismiss(animated: true, completion: nil)
    }
}

