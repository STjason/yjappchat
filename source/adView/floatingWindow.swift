//
//  floatingWindow.swift
//
//
//  Created by ken on 2019/3/4.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit
import SnapKit
class floatingWindow:UIView,UITableViewDelegate,UITableViewDataSource,WRCycleScrollViewDelegate{
    var array:NSMutableArray? //数据是否进来?
    var dic:NSMutableDictionary?
    var floatHeight = 0 as Float
    var floatWidth = 0 as Float
    var type = false  //默认显示轮播 , true 是盖楼
    var lunboImgs:[String] = [];//轮播图片数据集
    var lunboUrl:[String] = [];//轮播图片url数据集
    var cycleScrollView:WRCycleScrollView?
    var Controller : UIViewController?
    static var iiiiiii : Int = 0
    override init(frame:CGRect){
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        array = NSMutableArray.init()
        dic = NSMutableDictionary.init()
//      NotificationCenter.default.addObserver(self, selector: #selector(test), name: NSNotification.Name(rawValue:"imageSize"), object: nil) //动态二次修改坐标
        NotificationCenter.default.addObserver(self, selector: #selector(changeTableViewFrame), name: NSNotification.Name(rawValue: "changeTableViewWidth"), object: nil)
    }
    
    
    func sizeLimit(type:Bool,size:CGFloat)->Float{
        if kScreenHeight >= size {
            
        }
        return 30.00
    }
    
    //动态计算二次高宽通知
    @objc func test(nofi : Notification){
        if floatingWindow.iiiiiii == 1{
            let jsonResult = nofi.userInfo as! Dictionary<String, AnyObject>
            //适配高宽
            floatHeight = jsonResult["height"] as! Float
            floatWidth = jsonResult["width"] as! Float
            let Position = jsonResult["Position"] as! Int
            if type {
                var YYwidths = floatWidth
                if CGFloat.init(floatWidth) >= kScreenWidth {
                    YYwidths = Float(kScreenWidth)
                    floatWidth=YYwidths
                }
                
                
                let heights = floatHeight * Float(array!.count);
                var YYheights = heights
                if CGFloat.init(heights) >= kScreenHeight{
                    YYheights = Float(kScreenHeight)
                    //                    floatHeight = YYheights/Float(array!.count); 盖楼不控制图片高
                }
                
                
                //更新盖楼
                self.snp.updateConstraints { (make) in
                    
                    if kScreenHeight/2 >= CGFloat.init(YYheights) { //高小于半屏幕
                        if Position==9 || Position==11 || Position==13{
                            make.top.equalTo(YYheights/2)
                        }else if Position == 10 || Position==12 || Position==14{
                            make.bottom.equalTo(-YYheights/2)
                        }
                    }
                    make.width.equalTo(YYwidths) //YYwidths
                    make.height.equalTo(YYheights) //YYheights=高*count floatHeight = 单张图片高 YYheights
                }
                gailouViewTabble.reloadData()
                
            }else{
                //                500 375
                var YYwidths = floatWidth
                //                if CGFloat.init(floatWidth) >= kScreenWidth {
                //                    YYwidths = Float(kScreenWidth/3*2) //kScreenWidth
                //                    floatWidth=YYwidths
                //                }
                YYwidths = Float(kScreenWidth/3*2) //kScreenWidth
                floatWidth=YYwidths
                
                let heights = floatHeight
                var YYheights = heights
                //                if CGFloat.init(heights) >= kScreenHeight{
                //                    YYheights = Float(kScreenHeight)
                //                    floatHeight = YYheights
                //                }
                YYheights = Float(floatWidth/2)
                floatHeight = YYheights
                
                self.snp.updateConstraints { (make) in
                    //适配左中上，左中下...
                    if Position==9 || Position==11 || Position==13{
                        make.top.equalTo(YYheights/2)
                    }else if Position == 10 || Position==12 || Position==14{
                        make.bottom.equalTo(-YYheights/2)
                    }
                    make.width.equalTo(YYwidths)
                    make.height.equalTo(YYheights)
                }
                
                self.lunboui(lunboImgs: lunboImgs, Width: floatWidth, Heights: floatHeight)
            }
        }
        
        if floatingWindow.iiiiiii >= 2 {
            floatingWindow.iiiiiii=0
        }else{
            floatingWindow.iiiiiii+=1
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var assignmentController:UIViewController {
        get{
            return UIViewController()
        }
        set {
            Controller = newValue
        }
    }
    
    //#MARK: 赋值事件
    var assignmentData:NSMutableDictionary{
        get{
            return dic!
        }
        set{
            dic = newValue
            floatHeight = dic?.object(forKey: "height") as! Float
            type = dic?.object(forKey: "type") as! Bool
            let arr = dic?.object(forKey: "array") as? NSArray
            let Position = dic?.object(forKey: "Position") as! Int
            for i in 0..<arr!.count {
                let arrs = arr![i] as! WechatQRCodeModel
                array!.add(arrs.imgUrl)
                //用户添加轮播或其它的链接的时候容易加上空格之类的多余 
                let bannerUrl = arrs.imgUrl.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                let linkUrl = arrs.linkUrl.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                lunboImgs.append(bannerUrl)
                lunboUrl.append(linkUrl)

//                lunboImgs.append(arr![i] as! String)
//                array!.add(arr![i])
            }
            if type {
                self.diytabble(tabble:gailouViewTabble,views:self)
            }else{
//              坐标
                let www = Float(kScreenWidth/3*2)
                let hhh = Float(www/2)
                self.lunboui(lunboImgs: lunboImgs, Width:www, Heights:hhh)
            }
        }
    }
    
    
    func lunboui(lunboImgs:[String],Width:Float,Heights:Float)->Void{
        let frame = CGRect.init(x: 0, y: 0, width: CGFloat.init(floatWidth), height: CGFloat.init(Heights))
        
       
        
        //        cycleScrollView = WRCycleScrollView.init(frame: frame, type: .SERVER, imgs: lunboImgs, descs: nil, defaultDotImage:UIImage.init(named: "center_header"), currentDotImage: nil, shouldScroll: false)
        //        cycleScrollView!.delegate = self
        //        cycleScrollView?.descLabelFont = UIFont.boldSystemFont(ofSize: 15)
        //        cycleScrollView?.descLabelHeight = 50
        //        cycleScrollView?.isAutoScroll=false
        ////        cycleScrollView?.isbuttonHidden=false //button隐藏
        //        cycleScrollView?.autoScrollInterval=5.0
        //        cycleScrollView?.pageControlAliment = .CenterBottom
        //        self.addSubview(cycleScrollView!)
        
        
        cycleScrollView = WRCycleScrollView.init(frame:frame, type: .SERVER, imgs:lunboImgs, descs: nil, defaultDotImage:nil, currentDotImage:nil,shouldScroll:false)
        cycleScrollView!.delegate = self
        cycleScrollView?.backgroundColor = UIColor.lightGray
        cycleScrollView?.clipsToBounds = true
        cycleScrollView?.descLabelHeight = 40
        cycleScrollView?.isAutoScroll=false
        cycleScrollView?.descLabelFont = UIFont.systemFont(ofSize: 13)
        cycleScrollView?.pageControlAliment = .CenterBottom
        cycleScrollView?.bottomViewBackgroundColor = UIColor.lightGray
        cycleScrollView?.removeFromSuperview()
        self.addSubview(cycleScrollView!)
        
        
    }
    
    func diytabble(tabble:UITableView,views:UIView)->Void{

        views.addSubview(tabble)
        tabble.snp.updateConstraints({ (make) in
            make.left.width.top.height.equalTo(views)
        })
    
        
    }
    
    func exitView(){
        //         NotificationCenter.default.removeObserver(self)
        self.removeFromSuperview()
    }
    
//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }
    
  
    
    func imagesize(Name:String) -> CGSize {
        var image:UIImage?
        let urls = URL.init(string: Name)
        getDataFromUrl(url:urls!) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                image = UIImage(data: data)
            }
        }
        return image!.size
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
//        return CGFloat.init(floatHeight)
        return UITableView.automaticDimension
    }
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if lunboUrl.count > 0 {
            let url = lunboUrl[indexPath.row]
            if url.length > 0 {
                let loginVC = UIStoryboard(name: "bbin_page", bundle: nil).instantiateViewController(withIdentifier: "bbin")
                let recordPage = loginVC as! BBinWebContrllerViewController
                recordPage.htmlContent = url
                recordPage.httpis=true
                Controller?.navigationController?.pushViewController(recordPage, animated: true)
            }
        }
    }
    
    func downloadImage(url: URL,imageUI:UIImageView) {
        
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() { () -> Void in
                imageUI.image = UIImage(data: data)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array!.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cells = tableView.dequeueReusableCell(withIdentifier: "gailoucell", for: indexPath) as! gailoucell
      

//        let urls=URL.init(string: array![indexPath.row] as! String)
//        downloadImage(url:urls!, imageUI:cells.images)
        
         cells.assignmentDataModel =  array![indexPath.row] as! String
//        关闭按钮，可以单张删除广告
//        cells.BUTTON.tag=indexPath.row
//        cells.BUTTON.addTarget(self, action:#selector(btnClickFun(sender:)), for: .touchUpInside)
        cells.BUTTON.isHidden = true
        return cells
    }
    @objc func btnClickFun(sender:UIButton?){
        array!.removeObject(at: sender!.tag)
        gailouViewTabble.reloadData()
        if array!.count == 0 {
            self.exitView()
        }
    }
    
    /// 点击图片回调
    @objc func cycleScrollViewDidSelect(at index:Int, cycleScrollView:WRCycleScrollView){
        //数据源会有点击数据
        if lunboUrl.count > 0 {
            let url = lunboUrl[index]
            if url.length > 0 {
                let loginVC = UIStoryboard(name: "bbin_page", bundle: nil).instantiateViewController(withIdentifier: "bbin")
                let recordPage = loginVC as! BBinWebContrllerViewController
                recordPage.htmlContent = url
                recordPage.httpis=true
                Controller?.navigationController?.pushViewController(recordPage, animated: true)
            }
        }
    }
    /// 图片滚动回调
    @objc func cycleScrollViewDidScroll(to index:Int, cycleScrollView:WRCycleScrollView){
        
    }
    
    @objc func cycleScrollViewButtonDid(to index: Int, button: UIButton) {
        lunboImgs.remove(at: index)
        cycleScrollView?.serverImgArray = lunboImgs
        if lunboImgs.endIndex == 0 {
            self.exitView()
        }
    }
    
    lazy var gailouViewTabble : UITableView = {
        let tabble = UITableView.init(frame:CGRect.init(x: 0, y: 0, width: 0, height: 0), style: UITableView.Style.plain)
        tabble.dataSource = self
        tabble.backgroundColor = UIColor.clear
        tabble.separatorStyle = .none
        tabble.delegate = self
        tabble.register(UINib(nibName: "gailoucell", bundle: nil), forCellReuseIdentifier: "gailoucell")
        self.addSubview(tabble)
        return tabble
    }()
    
    deinit {
        //记得移除通知监听
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension floatingWindow{
    //改变tableViewFrame
    @objc func changeTableViewFrame(noti:Notification){
        let userInfo = noti.userInfo as! [String: AnyObject]
        let size = userInfo["size"] as! CGSize
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.width.equalTo(size.width)
            make.height.equalTo(size.height)
        })
        

    }
}



