//
//  gailoucelltest.swift
//  swift test
//
//  Created by ken on 2019/3/2.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit
import Kingfisher
class gailoucell: UITableViewCell{
    @IBOutlet weak var images: UIImageView!
    @IBOutlet weak var BUTTON: UIButton!
    var dic:String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }

    //gif
    var assignmentDataModel:String{
        get{
            return self.dic;
        }
        set{
            self.dic = newValue;
            var logoImg = self.dic
            if !logoImg.isEmpty{
                logoImg = logoImg.trimmingCharacters(in: .whitespaces)
                let imageURL = URL.init(string: logoImg)
                if let url = imageURL{
                    self.images.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage.init(named: "activeDiscount_default"), options: nil, progressBlock: nil, completionHandler: {
                        image, error, cacheType, imageURL in
                        
                        self.size = image?.size ?? CGSize(width: 50, height: 50)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTableViewWidth"), object: self,
                                                        userInfo: ["size":self.size])

                    })
                }
            }
        }
    }
}
