//
//  floatingWindowManagement.swift
//
//
//  Created by ken on 2019/3/5.
//  Copyright © 2019 ken. All rights reserved.
//

import UIKit
import Kingfisher

struct floatingWindowManagement {
    static var popView:floatingWindow?
}
//exit view
func popViewRemoveFromSuperview(){
    floatingWindowManagement.popView?.removeFromSuperview()
}
//type=yes 盖楼
func popViewinitialization(positionStr:String ,view:UIView,array:NSArray,type:Bool,Controller:UIViewController)->Void{
    let position = positionConversion(positionStr: positionStr)
    var width = Float(kScreenWidth / 3 * 2)
    var height = Float(width / 2)
    if type {
        width = Float(kScreenWidth / 5 * 1)
        height = width

    }

//    let height=popViewImageCalculation(array: array, type: type,view: view,Position:position).height 自动适配第一张图片高
//    let width=popViewImageCalculation(array: array, type: type,view: view,Position:position).width 自动适配第一张图片宽
    popViewFormat(view:view, array: array, height: Float(height),width: Float(width), type: type,Controller: Controller , Position:position)
    popViewPosition(position: position, cout: array.count, imageHeight: Float(height), imageWidth: Float(width), type: type, view: view)
}
//坐标转换
// 例子 left_middle 左中
func positionConversion(positionStr:String)->NSInteger{
    
    if positionStr == "left_top" {
        return 0
    }else if positionStr == "left_middle"{
        return 1
    }else if positionStr == "left_bottom"{
        return 2
    }else if positionStr == "middle_top"{
        return 12 //3
    }else if positionStr == "middle_middle"{
        return 4
    }else if positionStr == "middle_bottom"{
        return 11 //5
    }else if positionStr == "right_top"{
        return 6
    }else if positionStr == "right_middle"{
        return 7
    }else if positionStr == "right_bottom"{
        return 8
    }else if positionStr == "left_middle_top"{
        return 9
    }else if positionStr == "left_middle_bottom"{
        return 10
    }else if positionStr == "middle_middle_top"{ //middle_middle_top
        return 11
    }else if positionStr == "middle_middle_bottom"{
        return 12
    }else if positionStr == "right_middle_top"{
        return 13
    }else if positionStr == "right_middle_bottom"{
        return 14
    }else{
        return 0
    }


}

//根据vc类名来管理showPage 
func VCshowPageManagement(controller:UIViewController)->NSInteger{
    let className = "\(controller.classForCoder)"
    
    if className == "SegmentMallContainerController" || className == "GroupMainContainerController" {  //首页
        return 1
    }else if className == "MemberPageTwoController" || className == "MemberPageController"{  //个人中心
        return 2 //2
    }else if className == "LoginController"{ //登录
        return 3 //3
    }else if className == "NewRegisterController"{ //注册
        return 4 //4
    }else if className == "TouzhPlainController" || className == "TouzhNewController" { //投注
        return 5 //5
    }
//  1=网站首页，2=个人中心，3=登录页面，4=注册页面，5=彩票投注页面
    return 0
//  个人中心
}

func popViewFormat(view:UIView,array:NSArray,height:Float,width:Float,type:Bool,Controller:UIViewController,Position:NSInteger)->Void{
    floatingWindowManagement.popView = floatingWindow.init(frame: UIScreen.main.bounds)
    let dicc = NSMutableDictionary.init()
    dicc .setValue(width, forKey: "width")
    dicc .setValue(array, forKey: "array")
    dicc .setValue(height, forKey: "height")
    dicc .setValue(type, forKey: "type")
    dicc .setValue(Position, forKey: "Position")
    floatingWindowManagement.popView?.assignmentController = Controller
    floatingWindowManagement.popView?.assignmentData = dicc
    floatingWindowManagement.popView?.backgroundColor = UIColor.clear
    view.addSubview(floatingWindowManagement.popView!)
}

func imageSize(named:WechatQRCodeModel,view:UIView,Position:NSInteger) -> CGSize {
//    var image:UIImage?
//    if let result = WechatQRCodeModel.deserialize(from: named){
//        let urls = URL.init(string:named.imgUrl)
//        getDataFromUrl(url:urls!) { (data, response, error)  in
//            guard let data = data, error == nil else { return }
//            DispatchQueue.main.async() { () -> Void in
//                image = UIImage(data: data)
//                //第一张图片下载成功，通知修改view坐标
//                NotificationCenter.default.post(name: NSNotification.Name("imageSize"), object:view, userInfo: ["width":image?.size.width as Any,"height":image?.size.height as Any,"Position":Position])
//            }
//        }
//    }
    return CGSize.init(width: 0, height: 0)
}


//计算第一张图长宽
func popViewImageCalculation(array:NSArray,type:Bool,view:UIView,Position:NSInteger)->CGSize{
    let sysHeight =  UIScreen.main.bounds.height //XTgggg
    let sysWidth =  UIScreen.main.bounds.width //xTkkkk

    var height = Float(imageSize(named: array.object(at: 0) as! WechatQRCodeModel, view: view,Position: Position).height)
    var width =  Float(imageSize(named: array.object(at: 0) as! WechatQRCodeModel, view: view,Position: Position).width)
    if Float(sysWidth) < width{
        width = Float(sysWidth)
    }
    let size:CGSize
    if type {
        var lunboheight = height
        if Float(sysHeight) < lunboheight{
            lunboheight = Float(sysHeight)
        }
        size = CGSize(width: Int(width), height: Int(lunboheight))
    }else{
        let screen = Float(array.count) * height
        if Float(sysHeight) < screen{
            height = Float(sysHeight)/Float(array.count)
        }
        size = CGSize(width: Int(width), height: Int(height))
    }

    return size
}
/*
 0,1,2 = 左
 3,4,5 = 中
 6,7,8 = 右
 */
func popViewPosition(position:NSInteger,cout:NSInteger,imageHeight:Float,imageWidth:Float,type:Bool,view:UIView)->Void{
//    let cout = cout + 1
    
    var imageHeight = imageHeight
    if type {  //盖楼高判断
        var hhh = imageHeight * Float(cout)
        if CGFloat.init(hhh) > kScreenHeight {
            imageHeight = Float(kScreenHeight)
        }else{
            if position == 0 || position == 3 || position == 6 {
                 hhh = imageHeight * Float(cout+1)
            }else{
                hhh = imageHeight * Float(cout)
            }
            imageHeight = hhh
        }
    }
    
    switch position {
    case 0:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.left.equalTo(0)
            make.top.equalTo(KNavHeight)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 1:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.left.equalTo(0)
            make.centerY.equalTo(view)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 2:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.left.equalTo(0)
            make.bottom.equalTo(-KTabBarHeight)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 3: //
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.top.equalTo(KNavHeight)
            make.centerX.equalTo(view)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 4:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.center.equalTo(view)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 5:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.bottom.equalTo(0)
            make.centerX.equalTo(view)
            make.width.equalTo(imageWidth)
           make.height.equalTo(imageHeight)
        })
        
    case 6:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.right.equalTo(0)
            make.top.equalTo(KNavHeight)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 7:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.right.equalTo(0)
            make.centerY.equalTo(view).offset(-imageWidth/2)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
        })
    case 8:
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.right.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.bottom.equalTo(-KTabBarHeight)
        })
    case 9: //左中上
        floatingWindowManagement.popView?.snp.remakeConstraints({ (make) in
            make.left.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.top.equalTo((kScreenHeight - CGFloat(KNavHeight) - CGFloat(imageHeight) ) * 0.25)
        })
    case 10: //左中下
        let bottomDistance = -((kScreenHeight - KTabBarHeight - CGFloat(imageHeight)) * 0.25)
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.left.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.bottom.equalTo(view).offset(bottomDistance)
        })
    case 11: //中中上
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.top.equalTo((kScreenHeight - CGFloat(KNavHeight) - CGFloat(imageHeight) ) * 0.25)
        })
    case 12: //中中下
        let bottomDistance = -((kScreenHeight - KTabBarHeight - CGFloat(imageHeight)) * 0.25)
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.bottom.equalTo(view).offset(bottomDistance)
        })
    case 13: //右中上
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.right.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.top.equalTo((kScreenHeight - CGFloat(KNavHeight) - CGFloat(imageHeight) ) * 0.25)
        })
    case 14: //右中下
        let bottomDistance = -((kScreenHeight - KTabBarHeight - CGFloat(imageHeight)) * 0.25)
        floatingWindowManagement.popView?.snp.updateConstraints({ (make) in
            make.right.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageHeight)
            make.bottom.equalTo(bottomDistance)
        })
    default:
        break
    }
    

}
////计算高度
//func calculateFirstImageHeight(imageUrl:String)->CGFloat{
//    var imgUrl = imageUrl
//    var width:CGFloat = 0
//    if !imgUrl.isEmpty{
//        imgUrl = imgUrl.trimmingCharacters(in: .whitespaces)
//        let imageURL = URL.init(string: imgUrl)
//        if let url = imageURL{
//            UIImageView().kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage.init(named: "activeDiscount_default"), options: nil, progressBlock: nil, completionHandler: {
//                image, error, cacheType, imageURL in
//                width = image?.size.width ?? 75
//            })
//        }
//    }
//    return 0
//}


