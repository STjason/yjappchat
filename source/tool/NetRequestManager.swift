//
//  NetRequestManager.swift
//  gameplay
//
//  Created by admin on 2018/11/9.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import Alamofire

class NetRequestManager: NSObject {

    func recentLotteryResults(cpBianHao:String,lastLotteryExcpHandler:@escaping (_ contents:String) -> (),updateLotteryHandler:@escaping (_ results:[BcLotteryData]) -> ()) -> Void {
        request(frontDialog: false, url:LOTTERY_LAST_RESULT_URL,params: ["lotCode":cpBianHao,"pageSize":50],
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {

                        }else{

                        }
                        return
                    }

                    if let result = LastResultWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            //更新开奖结果
                            if let results = result.content{
                                if results.count > 0
                                {
                                    //正在开发
                                    //正式时，要改为拼接

                                }
                            }

                            
                        }
                    }
        })
    }

    func request(frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                 callback:@escaping (_ resultJson:String,_ returnStatus:Bool)->()) -> Void {

        if !NetwordUtil.isNetworkValid(){
            
            return
        }

        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog {
            
            }
        }
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in

            callback(resultJson, returnStatus)
        }
        let baseUrl = BASE_URL + PORT

        requestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
    }
}
