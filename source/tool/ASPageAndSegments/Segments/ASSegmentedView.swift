//
//  ASSegmentedView.swift
//  PagesViewWithSegs
//
//  Created by admin on 2018/12/1.
//  Copyright © 2018年 Adam. All rights reserved.
//

import UIKit

class ASSegmentedView: UIScrollView {

    private var itemSize = CGSize.zero
    private var latestIndex:Int = 0
    private var titles: [String] = []
    private var items:[UIView] = []
    private var segmentViewSize: CGSize = CGSize.zero
    
    var segmentedIndexChanged:((Int) -> Void)?
    
    convenience init(frame:CGRect, segmentTitles:[String], indexSelected:Int = 0, segmentItemSize:CGSize) {
        self.init(frame: frame)
        self.backgroundColor = UIColor.orange
        self.showsHorizontalScrollIndicator = false
        
        itemSize = segmentItemSize
        titles = segmentTitles
        latestIndex = indexSelected
        segmentViewSize = frame.size
        setupSelf()
    }
    
    //MARK: - API
    func segmentUpdateSegmentWith(index: Int) {
        updateSegment(index: index, isInit: false)
    }
    
    //MARK: - FUNCTION
    private func setupSelf() {
        self.contentSize = CGSize.init(width: itemSize.width * CGFloat(titles.count), height: self.itemSize.height)
        setupItems(titles: titles)
        updateSegment(index: latestIndex,isInit: true)
    }
    
    //MARK: get segment with index
    private func getSegmentItem(index: Int) -> UIView{
        let item = subviews[index]
        return item
    }
    
    private func setupItemText(item: ASLabelSegment,fontSize: CGFloat, color: UIColor) {
        item.titleFont = UIFont.systemFont(ofSize: fontSize)
        item.titleColor = color
    }
    
    //MARK: offset
    private func itemScrollToIndex(index: Int)  {
        let nextItemX = CGFloat(index) * itemSize.width
        let offsetToMiddleX = nextItemX - segmentViewSize.width * 0.5
        if  offsetToMiddleX > 0 {
            self.setContentOffset(CGPoint.init(x: overMaxOffset(offset: offsetToMiddleX) ? getMaxOffset() : offsetToMiddleX, y: 0), animated: true)
        }else {
            self.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
        }
    }
    
    private func updateSegment(index: Int,isInit: Bool = false) {
        if let item = getSegmentItem(index: index) as? ASLabelSegment {
            
            if latestIndex != index {
                if let latestItem = getSegmentItem(index: latestIndex) as? ASLabelSegment {
                    latestItem.hideBottomLine()
                    setupItemText(item: latestItem, fontSize: 14.0, color: UIColor.black)
                }
                
                setupItemText(item: item, fontSize: 17.0, color: UIColor.orange)
                item.showBottomLine()
            }else if isInit {
                setupItemText(item: item, fontSize: 17.0, color: UIColor.orange)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    item.showBottomLine()
                }
            }
            
            itemScrollToIndex(index: index)
            latestIndex = index
        }
    }
    
    private func overMaxOffset(offset: CGFloat) -> Bool {
        return offset - getMaxOffset() > 0
    }
    
    private func getMaxOffset() -> CGFloat{
        return CGFloat(items.count) * itemSize.width - self.frame.size.width
    }
    
    //MARK: - UI
    private func setupItems(titles:[String]) {
        for (index, title) in titles.enumerated() {
            if let item = Bundle.main.loadNibNamed("ASLabelSegment", owner: self, options: nil)?.last as? ASLabelSegment {
                
                setListener(view: item, index: index)
                item.config(title: title)
                items.append(item)
                addSubview(item)
            }
        }
    }
    
    private func setListener(view: UIView, index: Int) {
        view.tag = index
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tapSegmentAcction))
        view.addGestureRecognizer(gesture)
    }
    
    //MARK: - EVENTS
    //MARK: tap segement action
    @objc private func tapSegmentAcction(gesture: UIGestureRecognizer) {
        let index = gesture.view!.tag
        updateSegment(index: index)
        segmentedIndexChanged?(index)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for (index, item) in subviews.enumerated() {
            item.frame = CGRect.init(x: CGFloat(index) * itemSize.width, y: 0, width: itemSize.width, height: itemSize.height)
        }
    }

}
