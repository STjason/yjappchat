//
//  ASLabelSegment.swift
//  PagesViewWithSegs
//
//  Created by admin on 2018/12/1.
//  Copyright © 2018年 Adam. All rights reserved.
//

import UIKit

class ASLabelSegment: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    private var bottomLine = UIView()
    private var bottomLineH:CGFloat = 4
    
    var titleFont: UIFont = UIFont.systemFont(ofSize: 14.0) {
        didSet {
            titleLabel.font = titleFont
        }
    }
    
    var titleColor: UIColor = UIColor.black {
        didSet {
            titleLabel.textColor = titleColor
        }
    } 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bottomLine.backgroundColor = UIColor.orange

        addSubview(bottomLine)
    }
    
    //MARK: - API
    func config(title: String) {
        titleLabel.text = title
    }
    
    func showBottomLine() {
        UIView.animate(withDuration: 0.5) {
            self.resetShowBottomLine()
        }
    }
    
    func hideBottomLine() {
        UIView.animate(withDuration: 0.5) {
            self.resetHideBottomLine()
        }
    }
    
    //MARK: - Private
    private func resetHideBottomLine() {
        bottomLine.frame.size = CGSize.zero
        bottomLine.center.x = self.frame.size.width * 0.5
        bottomLine.center.y = self.frame.size.height - self.bottomLineH * 0.5
        bottomLine.alpha = 0
    }
    
    private func resetShowBottomLine() {
        let width = self.frame.size.width
        bottomLine.frame.size = CGSize.init(width: width * 0.75, height:self.bottomLineH)
        bottomLine.center.x = self.frame.size.width * 0.5
        bottomLine.center.y = self.frame.size.height - self.bottomLineH * 0.5
        bottomLine.alpha = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        resetHideBottomLine()
    }
    
}
