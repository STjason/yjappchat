//
//  ASPageView.swift
//  PagesViewWithSegs
//
//  Created by admin on 2018/12/3.
//  Copyright © 2018年 Adam. All rights reserved.
//

import UIKit

class ASPageView: UIView {
    private var pageController: UIPageViewController!
    private var isPageEnabled: Bool = true
    private var latestIndex:Int = 0
    private var subControllers:[UIViewController]!
    
    var pageViewFinishedAnimateHandler:((Int) -> Void)?
    
    convenience init(controllers:[UIViewController], indexSelected:Int, pageEnabled:Bool = true) {
        self.init()
        subControllers = controllers
        latestIndex = indexSelected
        isPageEnabled = pageEnabled
        
        configSelf()
    }
    
    //MARK: -API
    func pageViewUpdateWith(index:Int) {
        updatePageShowView(index: index)
    }
    
    //MARK: - Private
    private func configSelf() {
        pageController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController.delegate = self
        pageController.dataSource = self
        setupPageControllerScrollEnabled(scrollEnabled: isPageEnabled)
        addSubview(pageController.view)
        updatePageShowView(index: latestIndex)
    }
    
    private func updatePageShowView(index:Int) {
        pageController.setViewControllers([subControllers[index]], direction: index > latestIndex ? .forward : .reverse, animated: true) { (complected) in
            self.latestIndex = index
        }
    }
    
    private func setupPageControllerScrollEnabled(scrollEnabled: Bool) {
        if scrollEnabled {
            if let scrollView = getLowestLevelScrollFrom(page: pageController) {
                scrollView.isScrollEnabled = scrollEnabled
            }
        }
    }
    
    //MARK: - Functions
    private func getLowestLevelScrollFrom(page:UIPageViewController) -> UIScrollView? {
        for (_, subView) in page.view.subviews.enumerated() {
            if subView is UIScrollView {
                return subView as? UIScrollView
            }
        }
        return nil
    }
    
}

extension ASPageView:UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        /*
         UIPageViewController has two property,like 'viewControllers' and 'childViewControllers',the difference is
         
         Apple: childViewControllers: "An array of view controllers that are child of the current view controller"
         
         by adam: all of the pageController childControllers
         ============
         Apple: viewControllers: "The view controllers displayed by the pageViewController"
         
         by adam: when didFinishAnimating, the controllers which ones will be displaying in the screen
         ============
         */
        
        if let controllers = pageViewController.viewControllers {
            if let index = subControllers.index(of: controllers[0]) {
                latestIndex = index
                pageViewFinishedAnimateHandler?(latestIndex)
            }
        }
    }
    
}

extension ASPageView:UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = subControllers.index(of: viewController) {
            return index != 0 ? subControllers[index - 1] : nil
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = subControllers.index(of: viewController) {
            return index != subControllers.count - 1 ? subControllers[index + 1] : nil
        }
        return nil
    }
    
}
