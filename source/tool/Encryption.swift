//
//  Encryption.swift
//  encryption
//
//  Created by 大姚 on 2017/8/30.
//  Copyright © 2017年 YY. All rights reserved.
//

import CryptoSwift

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}

let aesKey = "5Po)(%G&v3#M.{:;"
let aesIV = "0>2^4*6(~9!B#D$F"
var newAESKey = ""

class Encryption: NSObject {
    
    //MARK: AES-CBC 128加密
    public static func Endcode_AES_ECB(key:String = newAESKey,iv:String = aesIV,strToEncode:String)->String {
        let input:[UInt8] = Array(strToEncode.utf8)
        let keyArray:[UInt8] = Array(key.utf8)
        let ivArray:[UInt8] = Array(iv.utf8)
        
        do {
            let encrypted:[UInt8] = try AES(key: keyArray, blockMode: CBC(iv: ivArray), padding: .pkcs7).encrypt(input)
            
            let encoded =  Data(encrypted)
            let stringEncoded = encoded.hexEncodedString()
            return stringEncoded
        } catch {
            print(error)
        }
        
        return ""
    }
    
    //  MARK:  AES-ECB128解密
    public static func Decode_AES_ECB(key:String = newAESKey,strToDecode:String)->String? {
        let input = Array<UInt8>(hex: strToDecode)
        let keyArray:[UInt8] = Array(key.utf8)
        let ivArray:[UInt8] = Array(aesIV.utf8)
        
        do {
            let decrypted = try AES(key: keyArray, blockMode: CBC(iv: ivArray), padding: .pkcs7).decrypt(input)
            
            // byte 转换成NSData
            let encoded = Data(decrypted)
            //解密结果从data转成string
            if let str = String(bytes: encoded.bytes, encoding: .utf8) {
                return str
            }
            
        } catch {
            print(error)
        }
        
        return ""
    }
    
    //  MARK:  AES-ECB128解密
    public static func Decode_AES_ECB(key:String = newAESKey,iv:String,strToDecode:String)->String? {
        let input = Array<UInt8>(hex: strToDecode)
        let keyArray:[UInt8] = Array(key.utf8)
        let ivArray:[UInt8] = Array(iv.utf8)
        
        do {
            let decrypted = try AES(key: keyArray, blockMode: CBC(iv: ivArray), padding: .pkcs7).decrypt(input)
            
            // byte 转换成NSData
            let encoded = Data(decrypted)
            //解密结果从data转成string
            if let str = String(bytes: encoded.bytes, encoding: .utf8) {
                return str
            }
            
        } catch {
            print(error)
        }
        
        return ""
    }
}
