//
//  UITableView+Extension.swift
//  gameplay
//
//  Created by admin on 2018/9/10.
//  Copyright © 2018 yibo. All rights reserved.
//

import Foundation

extension UITableView {
    ///注意不要冲突 10000+1/2的tag
    func tableViewDisplayWithLabel(datasCount:Int,tableView:UITableView, size:CGSize  = CGSize.init(width: screenWidth, height: 166),message:String = "暂无数据",textColor:UIColor,fontSize:CGFloat = 14) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            if datasCount == 0
            {
                self.removeView(superView: tableView)
                
                let tipLabel = UILabel()
                tipLabel.text = message
                tipLabel.tag = 10000+102
                tipLabel.frame = CGRect.init(x: 0, y: 0, width: size.width, height: 30)
                tipLabel.centerY = tableView.centerY
                tipLabel.centerX = tableView.centerX
                tipLabel.textAlignment = .center
                setThemeLabelTextColorGlassWhiteOtherBlack(label: tipLabel)
                tipLabel.textColor = textColor
                tipLabel.font = UIFont.systemFont(ofSize: fontSize)
                tableView.addSubview(tipLabel)
            }else
            {
                self.removeView(superView: tableView)
            }
        }
    }
    
    ///注意不要冲突 10000+1/2的tag
    func tableViewDisplayWithMessage(datasCount:Int,tableView:UITableView, size:CGSize  = CGSize.init(width: 200, height: 166),message:String = "暂无数据",fontSize:CGFloat = 14) {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
//        {
            if datasCount == 0
            {
                self.removeView(superView: tableView)
                
                let tipImage = UIImageView()
                tipImage.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
                tipImage.contentMode = .scaleAspectFill
                tipImage.center = tableView.center
                tipImage.centerY = tableView.centerY
                tipImage.backgroundColor = UIColor.clear
                tipImage.clipsToBounds = true
                tipImage.theme_image = "General.noDatasTipImage"
                tipImage.tag = 10000+101
                tableView.addSubview(tipImage)
                
                tipImage.snp.makeConstraints({ (make) in
                    make.centerY.equalTo(tableView.snp.centerY).offset(-20)
                    make.centerX.equalTo(tableView.snp.centerX)
                })
                
                let tipLabel = UILabel()
                tipLabel.text = message
                tipLabel.tag = 10000+102
                tipLabel.frame = CGRect.init(x: 0, y: 0, width: size.width, height: 30)
                tipLabel.centerY = tableView.centerY + tipImage.height * 0.5 + 10
                tipLabel.centerX = tipImage.centerX
                tipLabel.textAlignment = .center
                tipLabel.font = UIFont.systemFont(ofSize: fontSize)
                setThemeLabelTextColorGlassWhiteOtherBlack(label: tipLabel)
                tableView.addSubview(tipLabel)
                
                tipLabel.snp.makeConstraints({ (make) in
                    make.centerX.equalTo(tipImage.snp.centerX)
                    make.top.equalTo(tipImage.snp.bottom).offset(15)
                })
            }else{
                self.removeView(superView: tableView)
            }
//        }
    }
    
    private func removeView(superView: UIView) {
        if let tipView = superView.viewWithTag(10000+101)
        {
            tipView.removeFromSuperview()
        }
        
        if let tipLabelP = superView.viewWithTag(10000+102)
        {
            tipLabelP.removeFromSuperview()
        }
    }
}
