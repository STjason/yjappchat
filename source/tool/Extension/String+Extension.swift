//
//  String+Extension.swift
//  Colliers-CFIM
//
//  Created by  ywf on 16/12/19.
//  Copyright © 2016年 yingwf. All rights reserved.
//

import Foundation


extension Float {
   /// 小数点后如果只是0，显示整数，如果不是，显示原来的值
   var cleanZero : String {

       var value = self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    
        value = formatPureIntIfIsInt(value: value)
        return value
   }
}

extension Double {
   /// 小数点后如果只是0，显示整数，如果不是，显示原来的值
   var cleanZero : String {

        var value = self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
        value = formatPureIntIfIsInt(value: value)
        return value
   }
}

extension String {
    
    var length: Int {
        return self.count
    }
    
    func getDateByFormatString(_ str: String) -> Date? {
        
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = str
        dateFormat.locale = Locale.current
        
        return dateFormat.date(from: self)
    }
    
    
    //截取字符串
    func substrfromBegin(length:Int)->String{
        
        let index = self.index(self.startIndex, offsetBy: length)
        
        return self.substring(to: index)
        
    }
    
    //根据开始位置和长度截取字符串
    func subString(start:Int, length:Int = -1) -> String {
        var len = length
        if len == -1 {
            len = self.count - start
        }
        let st = self.index(startIndex, offsetBy:start)
        let en = self.index(st, offsetBy:len)
        return String(self[st ..< en])
    }
    
    func getCurrentTimeString() -> String {
        let currentDate = Date.init()
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDateString = dateFormatter.string(from: currentDate)
        return currentDateString
    }
    
    func getCommonDateByFormatString() -> Date? {
        return getDateByFormatString("yyyy-MM-dd HH:mm:ss")
    }
    
    func getStringForTimeStamp(time:TimeInterval) ->String{
        let timeStamp = Int32(time/1000)
        let stampDate = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: stampDate)
    }
    
    
    static func getStringSize(str: String? = nil, attriStr: NSMutableAttributedString? = nil, font: CGFloat, w: CGFloat, h: CGFloat) -> CGSize {
        if str != nil {
            var strSize = (str! as NSString).boundingRect(with: CGSize(width: w, height: h), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: font)], context: nil).size
            //控制按钮长度为屏幕总长的三分之一
            if strSize.width > (UIScreen.main.bounds.width/3 - OnIineNewPayInfoLogic.cellSectionpadding.left - OnIineNewPayInfoLogic.cellSectionpadding.right){
                strSize.width = UIScreen.main.bounds.width/3 - OnIineNewPayInfoLogic.cellSectionpadding.left - OnIineNewPayInfoLogic.cellSectionpadding.right
            }
            return strSize
        }
        
        if attriStr != nil {
            var strSize = attriStr!.boundingRect(with: CGSize(width: w, height: h), options: .usesLineFragmentOrigin, context: nil).size
            return strSize
        }
        return CGSize.zero
    }
    
    static func getStringWidth(str: String, strFont: CGFloat, h: CGFloat) -> CGFloat {
        return getStringSize(str: str, font: strFont, w: CGFloat.greatestFiniteMagnitude, h: h).width
    }
    
    static func getStringHeight(str: String, strFont: CGFloat, w: CGFloat) -> CGFloat {
        return getStringSize(str: str, font: strFont, w: w, h: CGFloat.greatestFiniteMagnitude).height
    }
    
    func positionOf(sub:String, backwards:Bool = false)->Int {
        // 如果没有找到就返回-1
        var pos = -1
        if let range = range(of:sub, options: backwards ? .backwards : .literal ) {
            if !range.isEmpty {
                pos = self.distance(from:startIndex, to:range.lowerBound)
            }
        }
        return pos
    }
  

}
