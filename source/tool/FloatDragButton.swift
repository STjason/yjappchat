//
//  FloatDragButton.swift
//  gameplay
//
//  Created by admin on 2018/9/19.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class FloatDragButton: UIButton {

    var clickClosure:((FloatDragButton) -> Void)?
    var autoDockEndClosure:((FloatDragButton) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panAction))
        self.addGestureRecognizer(panGesture)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapAction))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc private func tapAction() {
        clickClosure?(self)
    }
    
    @objc private func panAction(gesture: UIPanGestureRecognizer) {
        guard let superViewP = self.superview else {return}
        
        if self.isSelected {return}
        
        if gesture.state == UIGestureRecognizer.State.began || gesture.state == UIGestureRecognizer.State.changed
        {
            var translationPoint = gesture.translation(in: self.superview)
            
            if (self.y + translationPoint.y) > superViewP.height - self.height
            {
                translationPoint.y = superViewP.height - self.height - self.y
            }else if self.y + translationPoint.y < 64
            {
                translationPoint.y = 64 - self.y
            }
            
            if (self.x + translationPoint.x) > superViewP.width
            {
                translationPoint.x = superViewP.width - self.x - self.width
            }else if (self.x + translationPoint.x) < 0
            {
                translationPoint.x = -self.x
            }
            
            self.center = CGPoint.init(x: self.centerX + translationPoint.x, y: self.centerY + translationPoint.y)
        }else if gesture.state == UIGestureRecognizer.State.ended
        {
            if self.centerX > superViewP.centerX
            {
                self.center = CGPoint.init(x: superViewP.width - self.width * 0.5, y: self.centerY)
            }else
            {
                self.center = CGPoint.init(x: self.width * 0.5, y: self.centerY)
            }
            
            autoDockEndClosure?(self)
        }
        
        gesture.setTranslation(CGPoint.zero, in: superViewP)
    }
    
    

}
