//
//  SafetyCenterViewController.swift
//  gameplay
//
//  Created by JK on 2019/9/27.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class SafetyCenterViewController: BaseController, UITableViewDelegate, UITableViewDataSource {
    
    /** 开启手势锁 数据源 */
    var openGestureLockData =  [["手势锁开关", "修改手势密码", "手势密码锁屏时间"]]
    /** 关闭手势锁 数据源 */
    var closeGestureLockData = [["手势锁开关"]]
    /** 列表数据源 */
    var listData = [[]]
    /** 列表 */
    var _listView : UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        self.title = "安全中心"

        self.getGestureData()
        view.addSubview(self.listView())
    }
    
    func getGestureData() {
        // 后台开关权重高于本地开关 优先判断后台开关
        if YiboPreference.getGestureLockNetSwitch() == "off" {
            listData = []
            return
        }
        if YiboPreference.getGESTURE_LOCK() == "true" {
            listData = openGestureLockData
            self.listView().reloadSections([0], with: .automatic)
        }else {
            listData = closeGestureLockData
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "safetyCenterCell") as! SafetyCenterCell
        
        cell.titleLabel().text = listData[indexPath.section][indexPath.row] as? String
        cell.selectionStyle = .none
        
        if indexPath.row == 0 {
            cell.accessoryType = .none
            cell.contentLabel().isHidden = true
            cell.rightSwitch().isHidden = false
            let passWord = UserDefaults.standard.value(forKey: "passWord")
            cell.rightSwitch().isOn = YiboPreference.getGESTURE_LOCK() == "true" && passWord != nil ? true : false
            
            cell.rightSwitch().addTarget(self, action: #selector(changeGestureLockStatus(gestureLockSwitch:)), for: .touchUpInside)
        }else if indexPath.row == 1 {
            cell.rightSwitch().isHidden = true
            cell.contentLabel().isHidden = true
            if YiboPreference.getGESTURE_LOCK() == "true" {
                cell.accessoryType = .disclosureIndicator
            }
        }else if indexPath.row == 2 {
            cell.rightSwitch().isHidden = true
            cell.accessoryType = .disclosureIndicator
            cell.contentLabel().isHidden = false
            if YiboPreference.getGESTURE_LOCK() == "true" {
                cell.contentLabel().text = String(format: "%ds", YiboPreference.getGESTURE_LOCK_TIME())
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let sectionView = UIView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: kCurrentScreen(x: 75)))
            let getureLockLabel = UILabel(frame: kCGRect(x: kCurrentScreen(x: 50), y: 0, width: kScreenWidth - kCurrentScreen(x: 100), height: kCurrentScreen(x: 75)))
            getureLockLabel.text = "手势锁"
            getureLockLabel.textColor = UIColor.gray
            getureLockLabel.textAlignment = .left
            getureLockLabel.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 30))
            sectionView.addSubview(getureLockLabel)
            
            if YiboPreference.getGestureLockNetSwitch() == "on" {
                return sectionView
            }
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kCurrentScreen(x:75)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1
        {
            let gestureLockVc = GestureLockMainViewController()
            gestureLockVc.isModify = true
            let nav = UINavigationController(rootViewController: gestureLockVc)
            gestureLockVc.unlockSuccessBlock = {(status) in
                if status == .modifyFail {
                    showErrorHUD(errStr: "修改手势密码失败!")
                }else if status != .modifyFail && status != .setFail && status != .checkLoginOut {
                    gestureLockVc.dismiss(animated: true, completion: nil)
                    showErrorHUD(errStr: "修改手势密码成功!")
                }else if status == .checkLoginOut
                {
                    /// 登出当前账户
                    YiboPreference.setToken(value: "" as AnyObject)
                    YiboPreference.saveLoginStatus(value: false as AnyObject)
                    YiboPreference.saveUserName(value: "" as AnyObject)
                    YiboPreference.savePwd(value: "" as AnyObject)
                    YiboPreference.setAccountMode(value: 0 as AnyObject)
                    YiboPreference.setCACHEAVATARdata(value: Data.init())
                    
                    self.navigationController?.popToViewController(self.kViewControler(), animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                        showErrorHUD(errStr: "错误次数过多，你已被踢出登录！")
                    })
                }
            }
            self.present(nav, animated: true, completion: nil)
        }
        else if indexPath.row == 2
        {
            self.present(self.bottomListAlert(), animated: true, completion: nil)
        }
    }
    
    @objc func changeGestureLockStatus(gestureLockSwitch: UISwitch) {
        if gestureLockSwitch.isOn == true {
            // 开启手势密码 首先判断是否已经设置
            let passWord = UserDefaults.standard.value(forKey: "passWord")
            if passWord == nil {
                let gestureLockVc = GestureLockMainViewController()
                gestureLockVc.unlockSuccessBlock = {[weak self](status) in
                    if let weakSelf = self {
                        if status == .setSuccess || status == .modifySuccess {
                            showErrorHUD(errStr: "设定手势密码成功!")
                            weakSelf.listData = weakSelf.openGestureLockData
                            YiboPreference.setGESTURE_LOCK(isOpen: "true")
                            weakSelf.listView().reloadSections([0], with: .automatic)
                        }else  if status == .setFail || status == .modifyFail {
                            showErrorHUD(errStr: "设定手势密码失败!")
                            weakSelf.listData = weakSelf.closeGestureLockData
                            YiboPreference.setGESTURE_LOCK(isOpen: "false")
                            weakSelf.listView().reloadSections([0], with: .automatic)
                        }else if status == .checkLoginOut
                        {
                            /// 登出当前账户
                            YiboPreference.setToken(value: "" as AnyObject)
                            YiboPreference.saveLoginStatus(value: false as AnyObject)
                            YiboPreference.saveUserName(value: "" as AnyObject)
                            YiboPreference.savePwd(value: "" as AnyObject)
                            YiboPreference.setAccountMode(value: 0 as AnyObject)
                            YiboPreference.setCACHEAVATARdata(value: Data.init())
                            
                            weakSelf.navigationController?.popToViewController(weakSelf.kViewControler(), animated: true)
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                                showErrorHUD(errStr: "错误次数过多，你已被踢出登录！")
                            })
                        }
                    }
                }
                let nav = UINavigationController(rootViewController: gestureLockVc)
                self.present(nav, animated: true, completion: nil)
            }else
            {
                self.listData = self.openGestureLockData
                YiboPreference.setGESTURE_LOCK(isOpen: "true")
                self.listView().reloadSections([0], with: .automatic)
            }
        }else
        {
            /// 关闭手势密码  需先验证
            let gestureLockVc = GestureLockMainViewController()
            let nav = UINavigationController(rootViewController: gestureLockVc)
            gestureLockVc.unlockSuccessBlock = {[weak self](status) in
                if let weakSelf = self {
                    if status == .checkSuccess {
                        weakSelf.listData = weakSelf.closeGestureLockData
                        YiboPreference.setGESTURE_LOCK(isOpen: "false")
                        weakSelf.listView().reloadSections([0], with: .automatic)
                    }
                    else
                    {
                        weakSelf.listData = weakSelf.openGestureLockData
                        YiboPreference.setGESTURE_LOCK(isOpen: "true")
                        weakSelf.listView().reloadSections([0], with: .automatic)
                    }
                }
            }
            self.present(nav, animated: true, completion: nil)
        }
    }
    /** 从试图层遍历获取控制器 */
    func kViewControler() -> UIViewController
    {
        let arr = self.navigationController?.viewControllers
        for vc in arr! {
            if (vc.isKind(of: MemberPageTwoController.self)) || (vc.isKind(of: MemberPageController.self)) {
                return vc
            }
        }
        return MenuController()
    }
    
    func listView() -> UITableView {
        if _listView == nil {
            _listView = UITableView(frame: self.view.bounds, style: .grouped)
            _listView?.delegate = self
            _listView?.dataSource = self
            _listView?.tableFooterView = UIView()
            
            _listView?.register(SafetyCenterCell.self, forCellReuseIdentifier: "safetyCenterCell")
            
        }
        return _listView!
    }
    
    func bottomListAlert() -> UIAlertController {
        let list = ["10s", "30s", "60s", "90s", "120s"]
        let alertController = UIAlertController(title: "设置手势密码间隔", message: nil, preferredStyle: .actionSheet)
        let currentTitle = String(format: "%ds", YiboPreference.getGESTURE_LOCK_TIME())
        for title in list {
            var style = UIAlertAction.Style.default
            if title == currentTitle {
                style = .destructive
            }
            
            let listAction = UIAlertAction(title: title, style: style, handler: {(alert: UIAlertAction) in
                YiboPreference.setGESTURE_LOCK_TIME(time: Int(title.replacingOccurrences(of: "s", with: ""))!)
                if alert.title != currentTitle {
                    let indexPath = NSIndexPath(row: 2, section: 0)
                    self.listView().reloadRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.automatic)
                }
            })
            alertController.addAction(listAction)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
}
