
//
//  UserHeaderView.swift
//  gameplay
//
//  Created by JK on 2019/10/3.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import Kingfisher
import SnapKit
import MBProgressHUD

class UserHeaderView: UIView, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    /** 设置按钮 */
    var _setupBtn : UIButton?
    /** 用户信息 */
    var userInfo : Meminfo?
    /** 背景试图 */
    var _bgImgView : UIImageView?
    /** 头像 */
    var _iconImageView : UIImageView?
    /** 会员名称 */
    var _userNameLabel : UILabel?
    /** 会员等级 */
    var _vipLevelLabel : UILabel?
    /** 当前已存金额 */
    var _currentTotalLabel : UILabel?
    /** 当前积分等级状态描述 */
    var _discriptionLabel : UILabel?
    /** 当前积分等级进度条 */
    var _progressView : UIProgressView?
    /** 当前积分等级进度条 目前不需要 */
    var _stepView : StepView?
    /** 当前等级 */
    var _currentLevelLabel : UILabel?
    /** 下一等级及距离下一等级 需求 */
    var _nextLevelLabel : UILabel?
    /** 余额 */
    var _balanceLabel : UILabel?
    /** 底部工具栏 */
    var _bottomBar : UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.bgImageView())
        
        // 新增判断条件  前台注册试玩账号、后台注册试玩账号都没有显示会员等级的权限
        if getSystemConfigFromJson()?.content.switch_level_show == "on" && !judgeIsMobileGuest() && YiboPreference.getAccountMode() != GUEST_TYPE && YiboPreference.getLoginStatus() == true {
            self.addSubview(self.iconIamgeView())
            self.addSubview(self.userNameLabel())
            
            self.addSubview(self.vipLevelLabel())
            self.addSubview(self.currentTotalLabel())
            self.addSubview(self.discriptionLabel())
            self.addSubview(self.progressView())
            self.addSubview(self.currentLevelLabel())
            self.addSubview(self.nextLevelLabel())
            
            self.addSubview(self.balanceLabel())
        }else
        {
            self.balanceLabel().frame = kCGRect(x: 0, y: self.userNameLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth, height: kCurrentScreen(x: 50))
            
            self.addSubview(self.iconIamgeView())
            self.addSubview(self.userNameLabel())
            self.addSubview(self.balanceLabel())
        }
        
        self.addSubview(self.setupBtn())
        self.addSubview(self.bottomBar())
        // 更新背景
        self.updateHeaderBgLogo()
        // 更新头像
        self.updateHeader()
        NotificationCenter.default.addObserver(self, selector: #selector(themeHadChange), name: Notification.Name(rawValue: ThemeUpdateNotification), object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //#MARK: ------------------------- 业务 ---------------------------------
    /// 刷新余额 等信息
    func refreshUserData(userData: Meminfo)
    {
        self.userInfo = userData
        userNameLabel().text = isEmptyString(str: userData.account) ? "暂无名称" : userData.account
        let balanceStr = isEmptyString(str: userData.balance) ? "0.0" : userData.balance
        balanceLabel().text = "余额:\(balanceStr)元"
        
    }
    /// 刷新会员等级信息
    func refreshVipData(vipData: VipLevelMoel)
    {
        // 赋值会员等级信息
        self.vipLevelLabel().text = vipData.curLevelName
        // 填充当前存款总额数据
        self.currentTotalLabel().text = "当前已存款\(vipData.accDepositTotal ?? "")元"
        // 计算距离下级需存款多少
        let nextNeedDeposit = Float("\(vipData.newLevelDepositMoney ?? "0")")! - Float("\(vipData.accDepositTotal ?? "0")")!
       
        // 计算进度条
        let progress = Float(Float("\(vipData.accDepositTotal ?? "0")")!/Float("\(vipData.newLevelDepositMoney ?? "0")")!)
        // 赋值进度条信息
        self.progressView().progress = Float(progress) > 1 ? 1 : progress
        // 填充当前等级及需存金额信息
        if YiboPreference.getAccountMode() != GUEST_TYPE{
            // 赋值当前等级距离下级信息
            self.discriptionLabel().text = nextNeedDeposit > 0 ? NSString(format: "升级到下一级还需%@元", NSString(format: "%.3f", nextNeedDeposit).hc_keepDotStr()) as String : "当前已为最高等级" as String
            //不是试玩账号
            self.currentLevelLabel().text = "\(vipData.curLevelName ?? "")(需存款\(vipData.curLevelDepositMoney ?? "")元)"
            // 填充下一等级及需存金额信息 作最高等级判断
            if kStrIsEmpty(value: vipData.newLevelName as AnyObject) {
                self.nextLevelLabel().text = ""
            }else{
                self.nextLevelLabel().text = "\(vipData.newLevelName ?? "")(需存款\(vipData.newLevelDepositMoney ?? ""))"
            }
            if !(nextNeedDeposit > 0){
                self.currentLevelLabel().isHidden = true
                self.nextLevelLabel().isHidden = true
            }else{
                self.currentLevelLabel().isHidden = false
                self.nextLevelLabel().isHidden = false
            }
            self.discriptionLabel().isHidden = false
        }else{
            //是试玩账号就把当前等级和最高等级显示隐藏
            self.currentLevelLabel().isHidden = true
            self.nextLevelLabel().isHidden = true
            self.discriptionLabel().isHidden = true
        }
       
    }
    // 跳转设置
    @objc func changeSettingVc() {
        openSetting(controller: kViewControler(view: self))
    }
    
    /// 我的充值、我的提款、余额生金 事件
    @objc func click(sender: UIButton) {
        switch sender.tag {
        case 1000:
            // 拦截试玩账号
            if judgeIsMobileGuest() {
                showToast(view: self, txt: tip_shiwan)
                return
            }
            if kViewControler(view: self).navigationController != nil{
                openChargeMoney(controller: kViewControler(view: self), meminfo:self.userInfo)
            }else{
                chooseGoChargeController(meminfo: self.userInfo, controller: kViewControler(view: self))
            }
            break
        case 1001:
            // 拦截试玩账号
            if judgeIsMobileGuest() {
                showToast(view: self, txt: tip_shiwan)
                return
            }
            if kViewControler(view: self).navigationController != nil{
                openPickMoney(controller: kViewControler(view: self), meminfo:self.userInfo)
            }else{
                let loginVC = UIStoryboard(name: "withdraw_page", bundle: nil).instantiateViewController(withIdentifier: "withDraw")
                let recordPage = loginVC as! WithdrawController
                recordPage.meminfo = self.userInfo
                kViewControler(view: self).navigationController?.pushViewController(recordPage, animated: true)
                let nav = UINavigationController.init(rootViewController: recordPage)
                kViewControler(view: self).present(nav, animated: true, completion: nil)
            }
            break
        case 1002:
            if viewController()?.navigationController != nil{
                openBalance(controller: kViewControler(view: self),meminfo:self.userInfo)
            }else{
                let root = fundViewController()
                kViewControler(view: self).present(root, animated: true, completion: nil)
            }
            break
        default:
            break
        }
    }
    //MARK: 设置头像、头像背景
    @objc private func themeHadChange() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.updateHeaderBgLogo()
            self.updateHeader()
        }
    }
    // 更新背景
    func updateHeaderBgLogo() -> Void {
        guard let sys = getSystemConfigFromJson() else{return}
        let logoImg = sys.content.member_page_bg_url
        if !isEmptyString(str: logoImg){
            
            let urlString = handleImageURL(urlString: logoImg)
            
            let themeName = YiboPreference.getCurrentThmeByName()
            var bgImageName = ""
            if themeName == "Red" {
                bgImageName = "personalHeaderBg_red"
            }else if themeName == "Blue" || themeName == "FrostedPlain"{
                bgImageName = "personalHeaderBg_blue"
            }else if themeName == "Green" {
                bgImageName = "personalHeaderBg_green"
            }else if themeName == "FrostedOrange" {
                bgImageName = "personalHeaderBg_glassOrange"
            }else {
                bgImageName = "personalHeaderBg_glassOrange"
            }
            
            if let url = URL.init(string: urlString) {
                self.bgImageView().kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: bgImageName), options: nil, progressBlock: nil, completionHandler: nil)
            }else {
                self.bgImageView().theme_image = "General.personalHeaderBg"
            }
            
        }else{
            self.bgImageView().theme_image = "General.personalHeaderBg"
        }
    }
    //更新头像
    private func updateHeader(){
        if let sysconfig = getSystemConfigFromJson(){
            if sysconfig.content != nil{
                let logoImg = sysconfig.content.member_page_logo_url
                if isEmptyString(str: logoImg){
                    if YiboPreference.getCACHEAVATARdata().count >= 1 {
                        self.iconIamgeView().image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
                    }else{
                        print(logoImg)
                        self.iconIamgeView().theme_image = "General.placeHeader"
                    }
                }else{
                    updateAppLogo(icon: self.iconIamgeView())
                }
            }
        }
    }
    
    @objc func iconClick() {
        let sexActionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        weak var weakSelf = self
        let sexNanAction = UIAlertAction(title: "从相册中选择", style: UIAlertAction.Style.default){ (action:UIAlertAction)in
            weakSelf?.initPhotoPicker()
        }
        
        let sexNvAction = UIAlertAction(title: "拍照", style: UIAlertAction.Style.default){ (action:UIAlertAction)in
            weakSelf?.initCameraPicker()
        }
        
        let sexSaceAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        sexActionSheet.addAction(sexNanAction)
        sexActionSheet.addAction(sexNvAction)
        sexActionSheet.addAction(sexSaceAction)
        
        kViewControler(view: self).present(sexActionSheet, animated: true, completion: nil)
    }
    
    //MARK: - 相机
    //从相册中选择
    func initPhotoPicker(){
        let photoPicker =  UIImagePickerController()
        photoPicker.delegate = self
        photoPicker.allowsEditing = true
        photoPicker.sourceType = .photoLibrary
        //在需要的地方present出来
        kViewControler(view: self).present(photoPicker, animated: true, completion: nil)
    }
    
    //拍照
    func initCameraPicker(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let  cameraPicker = UIImagePickerController()
            cameraPicker.delegate = self
            cameraPicker.allowsEditing = true
            cameraPicker.sourceType = .camera
            //在需要的地方present出来
            kViewControler(view: self).present(cameraPicker, animated: true, completion: nil)
        } else {
            print("不支持拍照")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //获得照片
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        // 拍照
        if picker.sourceType == .camera {
            //保存相册
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
        }
        
        var dialog:MBProgressHUD!
        dialog = MBProgressHUD.showAdded(to: self, animated: true)
        dialog.detailsLabel.text = "正在上传中..."
        dialog.isSquare = false
        dialog.mode = .text
        dialog.label.textColor = UIColor.white
        dialog.detailsLabel.textColor = UIColor.white
        dialog.bezelView.color = UIColor.black
        
        if  let data = image.jpegData(compressionQuality: 0.5) { //压缩质量50%
            UploadAvatar(imageData: data) { (json, jsonstata) in
                if jsonstata {
                    DispatchQueue.main.sync {
                        dialog.hide(animated: true)
                        self.iconIamgeView().image = image
                        YiboPreference.setCACHEAVATARdata(value: data) //更新缓存
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:"DownloadAvatar"), object: self, userInfo:nil)
                        showToast(view: self, txt: "头像上传成功")
                    }
                }else{
                    DispatchQueue.main.sync {
                        dialog.hide(animated: true)
                        showToast(view: self, txt: "头像上传失败,请重试")
                    }
                }
            }
        }
        kViewControler(view: self).dismiss(animated: true, completion: nil)
    }
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
        
        if error != nil {
            print("保存失败")
        } else {
            print("保存成功")
        }
    }
    
    //#MARK: ------------------------- 实例化 ---------------------------------
    func setupBtn() -> UIButton {
        if _setupBtn == nil {
            _setupBtn = UIButton(type: .custom)
            _setupBtn?.frame = kCGRect(x: kScreenWidth - kCurrentScreen(x: 150), y: kCurrentScreen(x: 35), width: kCurrentScreen(x: 125), height: kCurrentScreen(x: 75))
            _setupBtn?.setTitle("设 置", for: .normal)
            _setupBtn?.setTitleColor(.white, for: .normal)
            _setupBtn?.titleLabel?.font = UIFont.boldSystemFont(ofSize: kCurrentScreen(x: 50))
            
            _setupBtn?.addTarget(self, action: #selector(changeSettingVc), for: .touchUpInside)
        }
        return _setupBtn!
    }
    
    func iconIamgeView() -> UIImageView {
        if _iconImageView == nil {
            _iconImageView = UIImageView(frame: kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 200), height: kCurrentScreen(x: 200)))
            _iconImageView?.center = CGPoint(x: kScreenWidth/2, y: kCurrentScreen(x: 150))
            _iconImageView?.layer.cornerRadius = kCurrentScreen(x: 100)
            _iconImageView?.clipsToBounds = true
            _iconImageView?.isUserInteractionEnabled = true
            
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(iconClick))
            _iconImageView?.addGestureRecognizer(tap)
        }
        return _iconImageView!
    }
    
    func userNameLabel() -> UILabel {
        if _userNameLabel == nil {
            _userNameLabel = UILabel(frame: kCGRect(x: 0, y: self.iconIamgeView().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth, height: kCurrentScreen(x: 50)))
            _userNameLabel?.textColor = .white
            _userNameLabel?.textAlignment = .center
            _userNameLabel?.font = UIFont.boldSystemFont(ofSize: kCurrentScreen(x: 45))
            
        }
        return _userNameLabel!
    }
    
    func vipLevelLabel() -> UILabel {
        if _vipLevelLabel == nil {
            _vipLevelLabel = UILabel(frame: kCGRect(x: 0, y: self.userNameLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth, height: kCurrentScreen(x: 50)))
            _vipLevelLabel?.textColor = .white
            _vipLevelLabel?.textAlignment = .center
            _vipLevelLabel?.font = UIFont.boldSystemFont(ofSize: kCurrentScreen(x: 45))
            
        }
        return _vipLevelLabel!
    }
    
    func currentTotalLabel() -> UILabel {
        if _currentTotalLabel == nil {
            _currentTotalLabel = UILabel(frame: kCGRect(x: 0, y: self.vipLevelLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth/2, height: kCurrentScreen(x: 50)))
            _currentTotalLabel?.textColor = .white
            _currentTotalLabel?.textAlignment = .center
            _currentTotalLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
            _currentTotalLabel?.text = "当前已存款0元"
            
        }
        return _currentTotalLabel!
    }
    
    func discriptionLabel() -> UILabel {
        if _discriptionLabel == nil {
            _discriptionLabel = UILabel(frame: kCGRect(x: kScreenWidth/2, y: self.vipLevelLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth/2, height: kCurrentScreen(x: 50)))
            _discriptionLabel?.textColor = .white
            _discriptionLabel?.textAlignment = .center
            _discriptionLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
            _discriptionLabel?.text = "升级到下级还需0元"
        }
        return _discriptionLabel!
    }
    
    func progressView() -> UIProgressView {
        if _progressView == nil {
            _progressView = UIProgressView(frame: kCGRect(x: kCurrentScreen(x: 200), y: self.discriptionLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth - kCurrentScreen(x: 400), height: kCurrentScreen(x: 50)))
            _progressView?.progressTintColor = .red
            _progressView?.trackTintColor = .gray
            _progressView?.progress = 0.0
            _progressView?.transform = CGAffineTransform.init(scaleX: 1.0, y: 3.0)
            
        }
        return _progressView!
    }
    
    func currentLevelLabel() -> UILabel {
        if _currentLevelLabel == nil {
            _currentLevelLabel = UILabel(frame: kCGRect(x: 0, y: self.progressView().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth/2, height: kCurrentScreen(x: 50)))
            _currentLevelLabel?.textColor = .white
            _currentLevelLabel?.textAlignment = .center
            _currentLevelLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
        }
        return _currentLevelLabel!
    }
    
    func nextLevelLabel() -> UILabel {
        if _nextLevelLabel == nil {
            _nextLevelLabel = UILabel(frame: kCGRect(x: kScreenWidth/2, y: self.progressView().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth/2, height: kCurrentScreen(x: 50)))
            _nextLevelLabel?.textColor = .white
            _nextLevelLabel?.textAlignment = .center
            _nextLevelLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
            
        }
        return _nextLevelLabel!
    }
    
    func stepView() -> StepView {
        if _stepView == nil {
            _stepView = StepView.init(frame: kCGRect(x: kCurrentScreen(x: 50), y: self.discriptionLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth - kCurrentScreen(x: 100), height: kCurrentScreen(x: 35)), titles: ["v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7"])
            
        }
        return _stepView!
    }
    
    func balanceLabel() -> UILabel {
        if _balanceLabel == nil {
            _balanceLabel = UILabel(frame: kCGRect(x: 0, y: self.nextLevelLabel().frame.maxY + kCurrentScreen(x: 20), width: kScreenWidth, height: kCurrentScreen(x: 50)))
            _balanceLabel?.textColor = .white
            _balanceLabel?.textAlignment = .center
            _balanceLabel?.font = UIFont.boldSystemFont(ofSize: kCurrentScreen(x: 45))
            
        }
        return _balanceLabel!
    }
    
    func bottomBar() -> UIView {
        if _bottomBar == nil {
            _bottomBar = UIView(frame: kCGRect(x: 0, y: self.height - kCurrentScreen(x: 100), width: kScreenWidth, height: kCurrentScreen(x: 100)))
            _bottomBar?.backgroundColor = .white
            
            var data = [(icon:String,title:String)]()
            if getSystemConfigFromJson()?.content.on_off_money_income == "on" {
                data = [(icon:"HomePage.homeDeposit",title:"我的充值"), (icon:"HomePage.withdrawal",title:"我的提现"), (icon:"HomePage.Balanceinterest",title:"余额生金")]
            }else {
                data = [(icon:"HomePage.homeDeposit",title:"我的充值"), (icon:"HomePage.withdrawal",title:"我的提现")]
            }
            for i in 0..<data.count {
                let btn = UIButton(type: .custom)
                btn.tag = 1000+i
                btn.frame = kCGRect(x: (Int(kScreenWidth)/data.count)*i, y: 0, width: Int(kScreenWidth)/data.count, height: kCurrentScreen(x: 100))
                let result = data[i]
                btn.theme_setImage(ThemeImagePicker.init(keyPath: result.icon), forState: .normal)
                btn.setTitle(result.title, for: .normal)
                btn.setTitleColor(.black, for: .normal)
                btn.imageView?.contentMode = .scaleAspectFit
                btn.imageEdgeInsets = UIEdgeInsets(top: kCurrentScreen(x: 15), left: -kCurrentScreen(x: 20), bottom: kCurrentScreen(x: 15), right: 0)
                btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: kCurrentScreen(x: 20), bottom: 0, right: 0)
                btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: kCurrentScreen(x: 50))
                btn.addTarget(self, action: #selector(click(sender:)), for: .touchUpInside)
                
                _bottomBar?.addSubview(btn)
            }
            
        }
        return _bottomBar!
    }
    
    func bgImageView() -> UIImageView {
        if _bgImgView == nil {
            _bgImgView = UIImageView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: self.height - kCurrentScreen(x: 100)))
            
        }
        return _bgImgView!
    }
}
