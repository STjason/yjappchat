//
//  LongDragonModel.swift
//  gameplay
//
//  Created by JK on 2019/9/19.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class LongDragonModel: NSObject {
    
    
    
}


class LongDragonListModel: NSObject
{
    /** 期数 */
    var count : String?
    
    /** 玩法名称 */
    var playName : String?
    
    /** 玩法项名称 */
    var itemName : String?
    
    func getCurrentJson(dic: NSDictionary) -> LongDragonListModel
    {
        let model = LongDragonListModel()
        
        model.count         = String(format: "%@", dic.value(forKey: "count") as! CVarArg)
        model.playName      = String(format: "%@", dic.value(forKey: "playName") as! CVarArg)
        model.itemName      = String(format: "%@", dic.value(forKey: "itemName") as! CVarArg)
        
        return model
    }
}
