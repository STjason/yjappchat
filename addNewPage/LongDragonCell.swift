//
//  LongDragonCell.swift
//  gameplay
//
//  Created by JK on 2019/9/19.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class LongDragonCell: UITableViewCell {
    
    /** 内容 */
    var _contentLabel : UILabel?
    
    /** 期数 */
    var _periodLabel : UILabel?
    
    /** 数据模型 */
    var model : LongDragonListModel? {
        didSet {
            self.contentLabel().text = String(format: "%@@%@", model?.playName ?? "", model?.itemName ?? "")
            self.periodLabel().text = String(format: "%@期", model?.count ?? "0")
            
            let attriContentText = NSMutableAttributedString.init(string: self.contentLabel().text ?? "")
            let itemRange = NSMakeRange(attriContentText.length - (model?.itemName?.length ?? 0) - 1, 1 + (model?.itemName?.length ?? 0))
            attriContentText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: itemRange)
            self.contentLabel().attributedText = attriContentText
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .orange
        
        self.contentView.addSubview(self.contentLabel())
        self.contentView.addSubview(self.periodLabel())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //#MARK: ------------------------- 实例化 ---------------------------------
    func contentLabel() -> UILabel {
        if _contentLabel == nil {
            _contentLabel = UILabel(frame: kCGRect(x: 1, y: 0, width: kCurrentScreen(x: 900)-1, height: self.height - 0.5))
            _contentLabel?.backgroundColor = .white
            _contentLabel?.textColor = .black
            _contentLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _contentLabel?.textAlignment = .center
        }
        return _contentLabel!
    }
    
    func periodLabel() -> UILabel {
        if _periodLabel == nil {
            _periodLabel = UILabel(frame: kCGRect(x: self.contentLabel().frame.maxX+1, y: 0, width: kScreenWidth - kCurrentScreen(x: 900) - 2, height: self.height - 0.5))
            _periodLabel?.backgroundColor = .white
            _periodLabel?.textColor = .red
            _periodLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _periodLabel?.textAlignment = .center
            
        }
        return _periodLabel!
    }
}
