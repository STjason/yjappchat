//
//  SafetyCenterCell.swift
//  gameplay
//
//  Created by JK on 2019/9/27.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class SafetyCenterCell: UITableViewCell {
    
    /** 标识 title */
    var _titleLabel : UILabel?
    
    /** 开关 */
    var _rightSwitch : UISwitch?
    
    /** 值 content */
    var _contentLabel : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.titleLabel())
        self.contentView.addSubview(self.rightSwitch())
        self.contentView.addSubview(self.contentLabel())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func titleLabel() -> UILabel {
        if _titleLabel == nil {
            _titleLabel = UILabel(frame: kCGRect(x: kCurrentScreen(x: 50), y: 0, width: kScreenWidth/2, height: self.height))
            _titleLabel?.textColor = .black
            _titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _titleLabel?.textAlignment = .left
            
        }
        return _titleLabel!
    }
    
    func rightSwitch() -> UISwitch {
        if _rightSwitch == nil {
            _rightSwitch = UISwitch()
            _rightSwitch?.center = CGPoint(x: screenWidth - kCurrentScreen(x: 100), y: self.height/2)
            _rightSwitch?.isHidden = true
        }
        return _rightSwitch!
    }
    
    func contentLabel() -> UILabel {
        if _contentLabel == nil {
            _contentLabel = UILabel(frame: kCGRect(x: kScreenWidth/2, y: 0, width: kScreenWidth/2 - kCurrentScreen(x: 100), height: self.height))
            _contentLabel?.textColor = .black
            _contentLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _contentLabel?.textAlignment = .right
            
        }
        return _contentLabel!
    }
}
