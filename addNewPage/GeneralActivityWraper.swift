//
//  LotterysWraper.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2017/12/13.
//  Copyright © 2017年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class GeneralActivityWraper: HandyJSON {
    
    var content:GeneralActivity?
    var success:Bool!
    var msg:String?
    var code:Int = 0
    var accessToken:String?
    
    required init() {}
}

class GeneralActivity: HandyJSON {
    
    var imageUrl:String?
    var name:String?
    var url:String?
    
    required init() {}
}
