//
//  LongDragonViewController.swift
//  gameplay
//
//  Created by JK on 2019/9/19.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class LongDragonViewController: BaseController, UITableViewDelegate, UITableViewDataSource, PopMenuViewDelegate {
    
    
    /** 头部切换器 */
    var _titleSegment : UISegmentedControl?
    /** 彩种集合名称 */
    var groupName : String?
    /** 游戏标识 */
    var gameCode : String?
    /** 冷热所有数据 */
    var codeAndHotAllListData : [LongDragonListModel]?
    /** 冷热选择列表展示数据集合 */
    var codeAndHotDisplayData : Array<Int>?
    /** 选项选择列表展示数据集合 */
    var omitCodeDisplayData : Array<Any>?
    /** 选项列表传参集合 */
    var omitCodeParamData : Array<Any>?
    /** 默认选项index */
    var selectIndex : Int = 0
    /** 长龙列表 */
    var _longDragonList : UITableView?
    /** 列表数据源 */
    var listData : Array<Any>?
    /** 右上角选择列表按钮 */
    var _rightBarItem : UIButton?
    /** 右上角选择列表 */
    var _rightPopMenu : HCPopListMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 配置展示及参数集合
        handColorVariety()
        // 请求冷热数据
        requestCodeAndHotData()
        // 添加头部切换器
        self.navigationItem.titleView = self.titleSegment()
        // 添加右上角选择列表按钮
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: self.rightBarItem())
        // 加载长龙列表
        self.view.addSubview(self.longDragonList())
        // 赋值默认选项
        self.rightBarItem().setTitle(omitCodeDisplayData![selectIndex] as? String, for: .normal)
    }
    
    //#MARK: ------------------------- tab Delegate ---------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "longDragonCell") as! LongDragonCell
        
        cell.model = self.listData?[indexPath.row] as? LongDragonListModel
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    //#MARK: ------------------------- 业务 ---------------------------------
    func requestCodeAndHotData() {
        kRequest(isContent:false, frontDialog: true, method: .post, loadTextStr: "获取冷热数据...", url: GET_OUT_CODE, params: ["code": gameCode ?? ""] as [String: Any]) { (content) in
            let listData = getArrayFromJSONString(jsonString: content as? String ?? "")
            
            self.codeAndHotAllListData = []
            for obj in listData {
                let model = LongDragonListModel().getCurrentJson(dic: (obj as? NSDictionary)!)
                self.codeAndHotAllListData?.append(model)
            }
            if self.codeAndHotAllListData?.count == 0 {
                
                return
            }
            self.codeAndHotDisplayData = []
            var duplicateData: Set<String> = []
            for model in self.codeAndHotAllListData! {
                let count = model.count
                duplicateData.insert(count ?? "")
            }
            // 去重
            let notDuplicateData = Set(duplicateData)
            for title in notDuplicateData {
                self.codeAndHotDisplayData?.append(Int(title)!)
            }
            // 排序
            self.codeAndHotDisplayData?.sort(){$0 < $1}
            
            self.refreshCodeAndHotList()
        }
    }
    
    func requestOmissionData() {
        kRequest(isContent:false, frontDialog: true, method: .post, loadTextStr: "获取遗漏数据...", url: GET_OMIT_CODE, params: ["code": gameCode ?? "", "type": omitCodeParamData![selectIndex]] as [String: Any]) { (content) in
            let listData = getArrayFromJSONString(jsonString: content as? String ?? "")
            self.listData = []
            for obj in listData {
                let model = LongDragonListModel().getCurrentJson(dic: (obj as? NSDictionary)!)
                self.listData?.append(model)
            }
            self.longDragonList().reloadData()
        }
    }
    // 刷新冷热数据集合
    func refreshCodeAndHotList() {
        
        let title = codeAndHotDisplayData?[selectIndex]
        let rightBarTitle = String(format: "%d期", title ?? 0)
        self.rightBarItem().setTitle(rightBarTitle, for: .normal)
        
        self.listData = []
        for obj in codeAndHotAllListData! {
            if Int(obj.count!) == title {
                self.listData?.append(obj)
            }
        }
        self.longDragonList().reloadData()
    }
    
    func handColorVariety() {
        omitCodeParamData = ["bothSide"]
        omitCodeDisplayData = ["两面"]
        switch groupName {
        case "时时彩":
            omitCodeParamData = ["bothSide", "code1", "code2", "code3", "code4", "code5", "draTig", "all5Win1"]
            omitCodeDisplayData = ["两面", "第一球", "第二球", "第三球", "第四球", "第五球", "龙虎斗", "全五中一"]
            break
        case "PK10":
            omitCodeParamData = ["bothSide", "code1", "code2", "code3", "code4", "code5", "code6", "code7", "code8", "code9", "code10", "draTig", "firSecSum", "firSecCom"]
            omitCodeDisplayData = ["两面", "第一名", "第二名", "第三名", "第四名", "第五名", "第六名", "第七名", "第八名", "第九名", "第十名", "龙虎斗", "冠亚和值", "冠亚组合"]
            break
        case "11选5":
            omitCodeParamData = ["bothSide", "code1", "code2", "code3", "code4", "code5", "draTig", "all5Win1"]
            omitCodeDisplayData = ["两面", "第一球", "第二球", "第三球", "第四球", "第五球", "龙虎斗", "全五中一"]
            break
        case "分分彩":
            omitCodeParamData = ["bothSide", "code1", "code2", "code3", "code4", "code5", "draTig", "all5Win1"]
            omitCodeDisplayData = ["两面", "第一球", "第二球", "第三球", "第四球", "第五球", "龙虎斗", "全五中一"]
            break
        case "北京赛车":
            omitCodeParamData = ["bothSide", "code1", "code2", "code3", "code4", "code5", "draTig", "all5Win1"]
            omitCodeDisplayData = ["两面", "第一球", "第二球", "第三球", "第四球", "第五球", "龙虎斗", "全五中一"]
            break
        case "快三":
            omitCodeParamData = ["all3Win1", "dice", "longBrand", "shortBrand", "point"]
            omitCodeDisplayData = ["三军", "全骰围骰", "长牌", "短牌", "点数"]
        case "PC蛋蛋":
            omitCodeParamData = ["bothSide", "balls", "color", "leopard"]
            omitCodeDisplayData = ["两面", "球号", "波色", "豹子"]
            break
        case "低频彩":
            omitCodeParamData = ["bothSide", "code1", "code2", "code3", "leopard", "draTig", "span", "all3Win1"]
            omitCodeDisplayData = ["两面", "第一位", "第二位", "第三位", "豹子", "龙虎斗", "跨度", "独胆"]
            break
        default:
            break
        }
    }
    // 显示右上角选项列表
    @objc func showSelectList() {
        if self.titleSegment().selectedSegmentIndex == 0 {
            _rightPopMenu = HCPopListMenu.init(titles: codeAndHotDisplayData)
        }else {
            _rightPopMenu = HCPopListMenu.init(titles: omitCodeDisplayData)
        }
        
        _rightPopMenu?.delegate = self
        _rightPopMenu?.show()
    }
    
    func menu(_ menu: HCPopListMenu!, didSelectRowAt index: Int) {
        
        selectIndex = index
        if self.titleSegment().selectedSegmentIndex == 0
        {
            self.refreshCodeAndHotList()
        }
        else
        {
            self.requestOmissionData()
        }
    }
    
    // 切换 冷热遗漏
    @objc func changeCodeAndHot_Omission(segment: UISegmentedControl) {
        selectIndex = 0
        if segment.selectedSegmentIndex == 0 {
            self.requestCodeAndHotData()
        }else {
            self.requestOmissionData()
            self.rightBarItem().setTitle(omitCodeDisplayData?[0] as? String ?? "", for: .normal)
        }
    }
    
    //#MARK: ------------------------- 实例化 ---------------------------------
    func longDragonList() -> UITableView {
        if _longDragonList == nil {
            _longDragonList = UITableView(frame: self.view.bounds, style: .plain)
            _longDragonList?.delegate = self
            _longDragonList?.dataSource = self
            
            _longDragonList?.separatorStyle = .none;
            
            _longDragonList?.register(LongDragonCell.self, forCellReuseIdentifier: "longDragonCell")
            
        }
        return _longDragonList!
    }
    func titleSegment() -> UISegmentedControl {
        if _titleSegment == nil {
            _titleSegment = UISegmentedControl(items: ["出码排行", "遗漏排行"])
            _titleSegment?.frame = kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 500), height: 30)
            _titleSegment?.center = CGPoint(x: kScreenWidth/2, y: 22)
            _titleSegment?.selectedSegmentIndex = 0
            _titleSegment?.layer.cornerRadius = kCurrentScreen(x: 20)
            _titleSegment?.layer.borderColor = UIColor.white.cgColor
            _titleSegment?.layer.borderWidth = 1
            _titleSegment?.layer.masksToBounds = true
            let attriSelect = [kCTForegroundColorAttributeName : UIColor.white, kCTFontAttributeName : UIFont.systemFont(ofSize: kCurrentScreen(x: 45))] as [CFString : Any]
            _titleSegment?.setTitleTextAttributes(attriSelect as [NSAttributedString.Key : Any], for: .selected)
            let attriNormal = [kCTForegroundColorAttributeName : UIColor.red, kCTFontAttributeName : UIFont.systemFont(ofSize: kCurrentScreen(x: 40))]
            _titleSegment?.setTitleTextAttributes(attriNormal as [NSAttributedString.Key : Any], for: .normal)
            
            _titleSegment?.addTarget(self, action: #selector(changeCodeAndHot_Omission(segment:)), for: .valueChanged)
        }
        return _titleSegment!
    }
    func rightBarItem() -> UIButton {
        if _rightBarItem == nil {
            _rightBarItem = UIButton(type: .custom)
            _rightBarItem?.frame = kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 200), height: 30)
            _rightBarItem?.center = CGPoint(x: kScreenWidth - kCurrentScreen(x: 120), y: 22)
            _rightBarItem?.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 42))
            _rightBarItem?.setTitleColor(.white, for: .normal)
            _rightBarItem?.addTarget(self, action: #selector(showSelectList), for: .touchUpInside)
        }
        return _rightBarItem!
    }
    
}
