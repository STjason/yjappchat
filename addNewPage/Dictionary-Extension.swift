//
//  Dictionary-Extension.swift
//  YiboGameIos
//
//  Created by JK on 2019/10/7.
//  Copyright © 2019 com.lvwenhan. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    open func kValue(forKey key: String) -> String {
        if kStrIsEmpty(value: self.value(forKey: key) as AnyObject) {
            return ""
        }
        return "\(self.value(forKey: key) ?? "")"
    }
    
}
