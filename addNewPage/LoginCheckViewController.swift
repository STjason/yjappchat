//
//  LoginCheckViewController.swift
//  gameplay
//
//  Created by JK on 2019/9/26.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class LoginCheckViewController: UIViewController {
    
    var loginCheckSuccess:(()-> Void)?
    /** 账号描述 Label */
    var _userNameLabel : UILabel?
    /** 密码描述 Label */
    var _passwordLabel : UILabel?
    /** 账号输入框 */
    var _userNameTextFd : UITextField?
    /** 密码输入框 */
    var _passwordTextFd : UITextField?
    /** 确认按钮 */
    var _confirmBtn : UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        self.title = "登录校验"
        
        view.addSubview(self.userNameLabel())
        view.addSubview(self.passwordLabel())
        view.addSubview(self.userNameTextFd())
        view.addSubview(self.passwordTextFd())
        view.addSubview(self.confirmBtn())
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
    }
    
    //#MARK: ------------------------- 业务 ---------------------------------
    
    @objc func onBackClick() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func loginClick() {
        if self.userNameTextFd().text == "" {
            showErrorHUD(errStr: "请输入用户名")
            return
        }
        if self.passwordTextFd().text == "" {
            showErrorHUD(errStr: "请输入密码")
            return
        }
        
        // 提取缓存用户名
        let userAccount = YiboPreference.getUserName()
        // 提取缓存密码
        let passWord = YiboPreference.getPwd()
        
        if self.userNameTextFd().text != userAccount {
            showErrorHUD(errStr: "用户名不正确!")
            return
        }
        if self.passwordTextFd().text != passWord {
            showErrorHUD(errStr: "密码不正确!")
            return
        }
        
        self.loginCheckSuccess!()
        self.dismiss(animated: true, completion: nil)
    }

    //#MARK: ------------------------- 实例化 ---------------------------------
    func userNameLabel() -> UILabel {
        if _userNameLabel == nil {
            _userNameLabel = UILabel(frame: kCGRect(x: kCurrentScreen(x: 50), y: kCurrentScreen(x: 350), width: kCurrentScreen(x: 200), height: kCurrentScreen(x: 100)))
            _userNameLabel?.text = "账号:"
            _userNameLabel?.textColor = .black
            _userNameLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _userNameLabel?.textAlignment = .left
        }
        return _userNameLabel!
    }
    func passwordLabel() -> UILabel {
        if _passwordLabel == nil {
            _passwordLabel = UILabel(frame: kCGRect(x: kCurrentScreen(x: 50), y: self.userNameLabel().frame.maxY + kCurrentScreen(x: 50), width: kCurrentScreen(x: 200), height: kCurrentScreen(x: 100)))
            _passwordLabel?.text = "密码:"
            _passwordLabel?.textColor = .black
            _passwordLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _passwordLabel?.textAlignment = .left
            
        }
        return _passwordLabel!
    }
    func userNameTextFd() -> UITextField {
        if _userNameTextFd == nil {
            _userNameTextFd = UITextField(frame: kCGRect(x: self.userNameLabel().frame.maxX + kCurrentScreen(x: 20), y: kCurrentScreen(x: 350), width: kCurrentScreen(x: 800), height: kCurrentScreen(x: 100)))
            _userNameTextFd?.placeholder = "请输入账号"
            _userNameTextFd?.textColor = .black
            _userNameTextFd?.layer.cornerRadius = kCurrentScreen(x: 10)
            _userNameTextFd?.layer.borderWidth = 0.75
            _userNameTextFd?.layer.borderColor = UIColor.gray.cgColor
            _userNameTextFd?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        }
        return _userNameTextFd!
    }
    func passwordTextFd() -> UITextField {
        if _passwordTextFd == nil {
            _passwordTextFd = UITextField(frame: kCGRect(x: self.passwordLabel().frame.maxX + kCurrentScreen(x: 20), y: self.userNameTextFd().frame.maxY + kCurrentScreen(x: 50), width: kCurrentScreen(x: 800), height: kCurrentScreen(x: 100)))
            _passwordTextFd?.placeholder = "请输入密码"
            _passwordTextFd?.textColor = .black
            _passwordTextFd?.layer.cornerRadius = kCurrentScreen(x: 10)
            _passwordTextFd?.layer.borderWidth = 0.75
            _passwordTextFd?.layer.borderColor = UIColor.gray.cgColor
            _passwordTextFd?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
            _passwordTextFd?.isSecureTextEntry = true
        }
        return _passwordTextFd!
    }
    func confirmBtn() -> UIButton {
        if _confirmBtn == nil {
            _confirmBtn = UIButton(type: .custom)
            _confirmBtn?.frame = kCGRect(x: (kScreenWidth - kCurrentScreen(x: 500))/2, y: self.passwordTextFd().frame.maxY + kCurrentScreen(x: 50), width: kCurrentScreen(x: 500), height: kCurrentScreen(x: 100))
            _confirmBtn?.backgroundColor = .red
            _confirmBtn?.setTitle("验 证", for: .normal)
            _confirmBtn?.setTitleColor(.white, for: .normal)
            _confirmBtn?.layer.cornerRadius = kCurrentScreen(x: 20)
            _confirmBtn?.addTarget(self, action: #selector(loginClick), for: .touchUpInside)
        }
        return _confirmBtn!
    }
}
