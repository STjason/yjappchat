//
//  GestureView.swift
//  FQCard
//
//  Created by 冯倩 on 2017/5/8.
//  Copyright © 2017年 冯倩. All rights reserved.
//

import UIKit

class GestureView: UIView
{
    // 解锁成功回调
    var unlockSuccesBlock:((_ status: GestureLockBlockStatus) -> Void)?
    //路径
    var path:UIBezierPath = UIBezierPath()
    //存储已经路过的点
    var pointsArray = [CGPoint]()
    //当前手指所在点
    var fingurePoint:CGPoint!
    //密码存储
    var passwordArray = [Int]()
    //初次登陆时候的最多输入次数
    var inputCount:Int = 0
    //控制是否初次登陆
    var isFirst:Bool = true
    //控制是否修改密码
    var isChangeGestures:Bool = false
    /** 是否校验成功 */
    var isCheckSuccess = false
    /** 接收初次密码 */
    var firstPassword : String?
    
    /** 用户头像 */
    var _userIcon : UIImageView?
    /** 提示 label */
    var _messageLabel : UILabel?
    
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        //手势密码
        gesturePasswordLayoutUI()        
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect,changeGestures:Bool)
    {
        super.init(frame: frame)
        //手势密码
        isChangeGestures = changeGestures
        gesturePasswordLayoutUI()
    }
    
    //MARK: - UI
    func gesturePasswordLayoutUI()
    {
        self.backgroundColor = UIColor.white
        
        //添加用户头像
        addSubview(userIcon())
        //添加提示框
        addSubview(messageLabel())
        
        //添加手势密码
        gesturePasswordUI()
        //设置path
        path.lineWidth = 2
        
        //是否修改密码
        if isChangeGestures
        {
            UserDefaults.standard.removeObject(forKey: "newPassWord")
            messageLabel().text = "请先验证原有密码"
        }
        else
        {
            //本地密码
            let passWord = UserDefaults.standard.value(forKey: "passWord")
            if(passWord != nil)
            {
                isFirst = false
                messageLabel().text = "确认手势密码"
            }
            else
            {
                messageLabel().text = "请创建手势密码"
            }
        }
    }
    
    func gesturePasswordUI()
    {
        self.backgroundColor = UIColor.white
        
        //画密码
        let width:CGFloat = kCurrentScreen(x: 200)
        let height:CGFloat = width
        var x:CGFloat = 0
        var y:CGFloat = 0
        //计算空隙
        let spaceWidth = (kScreenWidth - 3 * width) / 4
        let spaceHeight = (kScreenWidth - 3 * height) / 4
        for index in 0..<9
        {
            //计算当前所在行
            let row = index % 3
            let line = index / 3
            //计算坐标
            x = CGFloat(row) * width + CGFloat(row + 1) * spaceWidth
            y = CGFloat(line) * width  + CGFloat(line + 1) * spaceHeight + messageLabel().frame.maxY
            let button = NumberButton(frame: kCGRect(x: x, y: y, width: width, height: height))
            button.tag = index
            addSubview(button)
        }
    }

    //MARK: 绘制
    override func draw(_ rect: CGRect)
    {
        self.path.removeAllPoints()
        for (index,point) in pointsArray.enumerated()
        {
            if index == 0
            {
                path.move(to: point)
            }
            else
            {
                path.addLine(to: point)
            }
            
        }
        //让画线跟随手指
        if self.fingurePoint != CGPoint.zero && self.pointsArray.count > 0
        {
            path.addLine(to: self.fingurePoint)
        }
        
        //设置线的颜色
        let color = UIColor.hexStringToColor(hexString: ColorLineBlueColor)
        color.set()
        path.stroke()
        
    }
    
    //MARK: - Other Functions
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        //每次点击移除所有存储过的点，重新统计
        pointsArray.removeAll()
        touchChanged(touch: touches.first!)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        touchChanged(touch: touches.first!)
    }
    
    func touchChanged(touch:UITouch)
    {
        let point = touch.location(in: self)
        fingurePoint = point
        for button in subviews
        {
            if button.isKind(of: NumberButton.self) && !pointsArray.contains(button.center) && button.frame.contains(point)
            {
                //记录已经走过的点
                passwordArray.append(button.tag)
                //记录密码
                pointsArray.append(button.center)
                //设置按钮的背景色
                button.backgroundColor = UIColor.hexStringToColor(hexString: ColorOfWaveBlueColor)
            }
        }
        //会调用draw 方法
        setNeedsDisplay()
    }
    
    
    //松手的时候调用
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if(passwordArray as NSArray).count == 0
        {
            return
        }
        inputCount += 1
        
        //本地存储
        let passWord = UserDefaults.standard.value(forKey: "passWord")
        let newPassWord = UserDefaults.standard.value(forKey: "newPassWord")
        //修改密码界面
        if isChangeGestures
        {
            // 判断是否通过校验
            if isCheckSuccess
            {
                if(newPassWord != nil )
                {
                    if Tools().passwordString(array: passwordArray as NSArray) == Tools().passwordString(array: newPassWord as! NSArray)
                    {
                        UserDefaults.standard.set(passwordArray, forKey: "passWord")
                        self.unlockSuccesBlock!(.modifySuccess)
                    }
                    else
                    {
                        if inputCount < 4
                        {
                            messageLabel().text = "输入错误,还可以输入\(4 - inputCount)次"
                            messageLabel().textColor = .red
                        }
                        else
                        {
                            // 输错超过4次 踢出登录
                            self.unlockSuccesBlock!(.checkLoginOut)
                        }
                    }
                }
                else//初次储存新密码
                {
                    UserDefaults.standard.set(passwordArray, forKey: "newPassWord")
                    messageLabel().text = "请验证新密码"
                    messageLabel().textColor = .black
                }
            }
            else
            {
                if Tools().passwordString(array: passwordArray as NSArray) == Tools().passwordString(array: passWord as! NSArray)
                {
                    showErrorHUD(errStr: "验证成功")
                    messageLabel().text = "请设置新密码"
                    messageLabel().textColor = .black
                    isCheckSuccess = true
                }
                else
                {
                    if inputCount < 4
                    {
                        messageLabel().text = "输入错误,还可以输入\(4 - inputCount)次"
                        messageLabel().textColor = .red
                    }
                    else
                    {
                        // 输错超过4次 踢出登录
                        self.unlockSuccesBlock!(.checkLoginOut)
                    }
                }
            }
        }
        //非修改密码界面
        else
        {
            //初次登陆,五次设置密码的机会
            if isFirst
            {
                if(firstPassword != nil)
                {
                    if Tools().passwordString(array: passwordArray as NSArray) == firstPassword
                    {
                        UserDefaults.standard.set(passwordArray, forKey: "passWord")
                        self.unlockSuccesBlock!(.setSuccess)
                    }
                    else
                    {
                        if inputCount < 4
                        {
                            messageLabel().text = "输入错误,还可以输入\(4 - inputCount)次"
                            messageLabel().textColor = .red
                        }
                        else
                        {
                            // 输错超过4次 踢出登录
                            self.unlockSuccesBlock!(.checkLoginOut)
                        }
                    }
                }
                else    //初次存储
                {
                    messageLabel().text = "请验证手势密码"
                    messageLabel().textColor = .black
                    firstPassword = Tools().passwordString(array: passwordArray as NSArray)
                }
            }
            else//非初次登陆,不可重置密码,只能一直输入
            {
                if Tools().passwordString(array: passwordArray as NSArray) == Tools().passwordString(array: passWord as! NSArray)
                {
                    self.unlockSuccesBlock!(.checkSuccess)
                }
                else
                {
                    if inputCount < 4
                    {
                        messageLabel().text = "输入错误,还可以输入\(4 - inputCount)次"
                        messageLabel().textColor = .red
                    }
                    else
                    {
                        // 输错超过4次 踢出登录
                        self.unlockSuccesBlock!(.checkLoginOut)
                    }
                }
            }
        }
        
        //----------------------------
        //移除所有的记录
        pointsArray.removeAll()
        passwordArray.removeAll()
        path.removeAllPoints()
        setNeedsDisplay()
        fingurePoint = CGPoint.zero
        
        //清除所有按钮的选中状态
        for button in subviews
        {
            if button.isKind(of: NumberButton.self)
            {
                button.backgroundColor  =  UIColor.clear
            }
        }
    }
    func userIcon() -> UIImageView {
        if _userIcon == nil {
            _userIcon = UIImageView(frame: kCGRect(x: (kScreenWidth-kCurrentScreen(x: 200))/2, y: kCurrentScreen(x: 350), width: kCurrentScreen(x: 200), height: kCurrentScreen(x: 200)))
            _userIcon?.layer.cornerRadius = kCurrentScreen(x: 200)/2
            _userIcon?.layer.borderWidth = 0.25
            _userIcon?.layer.borderColor = UIColor.gray.cgColor
            _userIcon?.clipsToBounds = true
            if YiboPreference.getCACHEAVATARdata().count >= 1 {
                _userIcon?.image = UIImage.init(data: YiboPreference.getCACHEAVATARdata())
            }else{
                _userIcon?.theme_image = "General.placeHeader"
            }
        }
        return _userIcon!
    }
    
    func messageLabel() -> UILabel {
        if _messageLabel == nil {
            _messageLabel = UILabel(frame: kCGRect(x: 0, y: self.userIcon().frame.maxY + kCurrentScreen(x: 100), width: kScreenWidth, height: kCurrentScreen(x: 75)))
            _messageLabel?.textColor = .black
            _messageLabel?.textAlignment = .center
            _messageLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 50))
        }
        return _messageLabel!
    }
}
