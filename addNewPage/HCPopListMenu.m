//
//  PopMenuView.m
//  AITUI
//
//  Created by CoderTan on 2017/9/4.
//  Copyright © 2017年 LF. All rights reserved.
//

#import "HCPopListMenu.h"

//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x
// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define MAX_ROWS        6
#define MIN_WIDTH       kCurrentScreen(350)
#define CELL_HEIGHT     kCurrentScreen(100)
#define TOP_MARGIN      ([[UIApplication sharedApplication] statusBarFrame].size.height+44) //导航栏高度

@interface HCPopListMenu()<UITableViewDelegate, UITableViewDataSource>
{
    int tableHeight;
    int tableWidth;
    int styleX;
}
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) UIView  *mainView;
@property (nonatomic, strong) UIView  *backView;
@property (nonatomic, strong) UITableView *mTableView;
@end

@implementation HCPopListMenu

#pragma mark - init
- (instancetype)initWithTitles:(NSArray *)titles {
    
    if (self = [super init]) {
        self.frame = [UIApplication sharedApplication].keyWindow.frame;
        styleX  = 0;
        _titles = titles;
        if (titles) {
            _titles = titles;
        }else {
            _titles = [NSArray array];
        }
        
        for (int i = 0; i < _titles.count; i++) {
            NSString *title = [NSString stringWithFormat:@"%@",[_titles objectAtIndex:i]];
            CGSize defultSize = CGSizeMake(SCREEN_WIDTH, CELL_HEIGHT);
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
            
            CGRect rect = [title boundingRectWithSize:defultSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
            int width = rect.size.width + 50 + 20;
            tableWidth = width < tableWidth ? tableWidth : width;
        }
        
        _mainView = [[UIView alloc] initWithFrame:self.frame];
        _mainView.backgroundColor = [UIColor clearColor];
        // 手势
        UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        _mainView.userInteractionEnabled = YES;
        [_mainView addGestureRecognizer:tgr];
        [self addSubview:_mainView];
        
        tableHeight = _titles.count * CELL_HEIGHT;
        _backView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_HEIGHT-tableWidth, TOP_MARGIN, tableWidth, tableHeight)];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.shadowColor = [UIColor blackColor].CGColor;
        _backView.layer.shadowOpacity = 0.8f;
        _backView.layer.shadowRadius = 4.f;
        _backView.layer.shadowOffset = CGSizeMake(4,4); //阴影偏移量
        [self addSubview:_backView];
        
        // 列表
        _mTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, tableWidth, tableHeight) style:UITableViewStylePlain];
        _mTableView.bounces = NO;
        _mTableView.delegate = self;
        _mTableView.dataSource = self;
        _mTableView.scrollEnabled = _titles.count > MAX_ROWS;
        _mTableView.showsVerticalScrollIndicator = NO;
        _mTableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10);
        _mTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _mTableView.backgroundColor = [UIColor whiteColor];
        _mTableView.layer.shadowOpacity = 1.0;
        _mTableView.layer.shadowRadius = 4.f;
        _mTableView.layer.shadowColor = [UIColor blackColor].CGColor;
        _mTableView.layer.shadowOffset = CGSizeMake(0, 0); //四周阴影
        _mTableView.tableFooterView = [[UIView alloc] init];
        [_backView addSubview:_mTableView];
    }
    return self;
}

- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    _mainView.alpha = 1;
    _backView.alpha = 1;
    //整个背景View缩放到0.1
    CGAffineTransform newTransform = CGAffineTransformScale(_backView.transform, 0.1, 0.1);
    [_backView setTransform:newTransform];
    _backView.center = CGPointMake(SCREEN_WIDTH - styleX - (tableWidth - tableWidth*.1)/2,
                                   TOP_MARGIN+(tableHeight-tableHeight*.1)/2);
    
    [UIView animateWithDuration:0.25 animations:^{
        _mainView.alpha = 1;
        _backView.alpha = 1;
        // 整个背景View缩放到1
        CGAffineTransform newTransform = CGAffineTransformScale(_backView.transform, 10, 10);
        [_backView setTransform:newTransform];
        _backView.center = CGPointMake(SCREEN_WIDTH - styleX  - tableWidth/2, TOP_MARGIN+tableHeight/2);
    }];
}


- (void)dismiss
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _mainView.alpha = 0;
        _backView.alpha = 0;
        //整个背景View缩放到0.1
        CGAffineTransform newTransform = CGAffineTransformScale(_backView.transform, 0.1, 0.1);
        [_backView setTransform:newTransform];
        _backView.center = CGPointMake(SCREEN_WIDTH - styleX - (tableWidth - tableWidth*.1)/2,
                                       TOP_MARGIN+(tableHeight-tableHeight*.1)/2);
    } completion:^(BOOL finished) {
        if (finished) {
            // 整个View缩放到1
            CGAffineTransform newTransform = CGAffineTransformScale(_backView.transform, 10, 10);
            [_backView setTransform:newTransform];
            _backView.center = CGPointMake(SCREEN_WIDTH - styleX  - tableWidth/2, TOP_MARGIN+tableHeight/2);
            [self removeFromSuperview];
        }
    }];
}

#pragma mark - data && delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *titleCellIdentifier = @"titleCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:titleCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:titleCellIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont systemFontOfSize:kCurrentScreen(45)];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.numberOfLines = 0;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[_titles objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_titles.count > indexPath.row) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(menu:didSelectRowAtIndex:)]) {
            
            [self.delegate menu:self didSelectRowAtIndex:indexPath.row];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(menu:didSelectRowAtItem:)]) {
            
            [self.delegate menu:self didSelectRowAtItem:_titles[indexPath.row]];
        }
        
        if (self.DidRowBlock) {
            [self dismiss];
            self.DidRowBlock(indexPath.row, _titles[indexPath.row]);
        }
        
        [self dismiss];
    }
}

@end
