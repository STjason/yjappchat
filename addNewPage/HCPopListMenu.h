//
//  PopMenuView.h
//  AITUI
//
//  Created by CoderTan on 2017/9/4.
//  Copyright © 2017年 LF. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HCPopListMenu;
@protocol PopMenuViewDelegate <NSObject>

@optional
/// 获取当前选择的行数
- (void)menu:(HCPopListMenu *)menu didSelectRowAtIndex:(NSInteger)index;
/// 获取当前选择行数的内容
- (void)menu:(HCPopListMenu *)menu didSelectRowAtItem:(NSString *)item;

@end

@interface HCPopListMenu : UIView

- (instancetype)initWithTitles:(NSArray *)titles;

- (void)show;

- (void)dismiss;

/** 获取当前所以选信息 */
@property (nonatomic, copy) void(^DidRowBlock)(NSInteger idx, NSString *item);

@property (nonatomic, weak) id <PopMenuViewDelegate>delegate;

@end
