//
//  GestureLockMainViewController.swift
//  gameplay
//
//  Created by JK on 2019/9/25.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import LocalAuthentication

class GestureLockMainViewController: UIViewController, UITextViewDelegate {
    
    /** 底部重置 */
    var _bottomPromptBoxTextView : UITextView?
    
    /** 解锁成功 闭包回调 */
    var unlockSuccessBlock:((_ status: GestureLockBlockStatus) -> Void)?
    
    /** 手势锁试图 */
    var gesturelockView : GestureView?
    
    /** 是否是修改进入 */
    var isModify : Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        createContent()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
    }
    
    @objc func onBackClick() {
        var status = GestureLockBlockStatus.setFail
        if isModify == true {
            status = .modifyFail
        }
        unlockSuccessBlock!(status)
        self.dismiss(animated: true, completion: nil)
    }
    
    func createContent() {
        //手势密码
        gesturelockView = GestureView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - 50), changeGestures: isModify ?? false)
        gesturelockView?.unlockSuccesBlock = {[weak self](status) in
            if let weakSelf = self {
                weakSelf.unlockSuccessBlock!(status)
                weakSelf.dismiss(animated: true, completion: nil)
            }
        }
        view.addSubview(gesturelockView!)
        
        view.addSubview(self.bottomPromptBoxTextView())
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let urlStr = URL.absoluteString
        if urlStr.hasPrefix("resetPassword") {
            // 跳转登录页面
            let loginVc = LoginCheckViewController()
            loginVc.loginCheckSuccess = {[weak self]() in
                showErrorHUD(errStr: "手势密码已重置")
                if let weakSelf = self {
                    weakSelf.gesturelockView?.removeFromSuperview()
                    weakSelf.bottomPromptBoxTextView().removeFromSuperview()
                    UserDefaults.standard.removeObject(forKey: "passWord")
                    weakSelf.isModify = false
                    weakSelf.createContent()
                }
            }
            let nav = UINavigationController(rootViewController: loginVc)
            self.present(nav, animated: true, completion: nil)
            
            return false
        }
        return true
    }
    
    func bottomPromptBoxTextView() -> UITextView {
        if _bottomPromptBoxTextView == nil {
            _bottomPromptBoxTextView = UITextView()
            // 提取配置文本
            let contentStr = "忘记密码?重置密码"
            let contentAttriStr = NSMutableAttributedString(string: contentStr as String)
            //添加样式 (行间距和对其方式)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 6
            paragraphStyle.alignment = .right
            contentAttriStr.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, contentAttriStr.length))
            // 给文本添加事件
            let range = NSRange(location: 5, length: 4)
            // 注册事件路径
            contentAttriStr.addAttribute(.link, value: "resetPassword://", range: range)
            
            var y = kScreenHeight - 50
            if IS_IPHONE_X {
                y = kScreenHeight - 50 - 34
            }
            _bottomPromptBoxTextView?.frame = kCGRect(x: 10, y: y, width: kScreenWidth - 20, height:50)
            _bottomPromptBoxTextView?.attributedText = contentAttriStr
            _bottomPromptBoxTextView?.isEditable = false
            _bottomPromptBoxTextView?.delegate = self
            _bottomPromptBoxTextView?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        }
        return _bottomPromptBoxTextView!
    }
}
