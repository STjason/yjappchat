//
//  CRDefaults.swift
//  Chatroom
//
//  Created by admin on 2019/7/4.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRDefaults: NSObject {
    
    static let TOKEN = "token"
    static let USER_ID = "userId"
    static let PLAN_USER = "planUser"
    static let USER_TYPE = "userType"
    /**不在聊天界面是否还能接收消息通知*/
    static let CHATROOM_RECEIVE_MESSAGE_NOTI = "chatroom_receive_message_noti"
    /**消息接收提示音*/
    static let CHATROOM_RECEIVE_MESSAGE_VOICE = "chatroom_receive_message_voice"
    /**进入房间通知*/
    static let CHATROOM_ENTER_ROOM_NOTI = "chatroom_enter_room_noti"
    /**消息发送提示音*/
    static let CHATROOM_SEND_MESSAGE_VOICE = "chatroom_send_message_voice"
    
    
    /**发送投注*/
    static let CHATROOM_SEND_BETTING = "chatroom_send_betting"
    /**发送emoji*/
    static let CHATROOM_SEND_EXPRESSION = "chatroom_send_expression"
    /**进入房间*/
    static let CHATROOM_ENTER_ROOM = "chatroom_enter_room"
    /**发送文本*/
    static let CHATROOM_SEND_TEXT = "chatroom_send_text"
    /**抢红包*/
    static let CHATROOM_RECEIVE_RED_PACKET = "chatroom_receive_red_packet"
    /**发送红包*/
    static let CHATROOM_SEND_RED_PACKET = "chatroom_send_red_packet"
    /**发送图片*/
    static let CHATROOM_SEND_IMAGE = "chatroom_send_image"
    /**发送音频*/
    static let CHATROOM_SEND_VOICE = "chatroom_send_voice"
    /**快捷发言*/
     static let CHATROOM_FAST_TALK = "chatroom_fast_talk"
    ///记录最后进入的房间,存储房间id
    static let CHATROOM_LAST_ROOM = "chatroom_last_room"
    
    static let LevelBackGroudColor = "level_backgroud_color"
    
    static let levelTitleColor = "chatroom_level_title_color"
    //保存密码房
    static let roomKeyConfig = "chatroom_room_password"
    //投注胜率
    static let betWinRate = "chatroom_win_rate"
    
    static func setLastRoom(value:String) {
        UserDefaults.standard.set(value, forKey: CHATROOM_LAST_ROOM)
    }
    
    static func getLastRoom() -> String {
        let value = UserDefaults.standard.value(forKey: CHATROOM_LAST_ROOM)
        if let value = value as? String {
            return value
        }
        
        return ""
    }
    
    //2，4为管理员
    static func setUserType(value:Int) {
        UserDefaults.standard.setValue(value, forKey: USER_TYPE)
    }
    
    static func getUserType() -> Int {
        let value = UserDefaults.standard.value(forKey: USER_TYPE)
        if let value = value as? Int {
            return value
        }
        
        return 0
    }
    
    //0非计划员，1计划员
    static func setPlanUser(value:Int) {
        UserDefaults.standard.setValue(value, forKey: PLAN_USER)
    }
    
    static func getPlanUser() -> Int {
        let value = UserDefaults.standard.value(forKey: PLAN_USER)
        if let value = value as? Int {
            return value
        }
        
        return 0
    }
    
    static func setUserID(value:String) {
        UserDefaults.standard.setValue(value, forKey: USER_ID)
    }
    
    static func getUserID() -> String {
        let value = UserDefaults.standard.value(forKey: USER_ID)
        if let value = value as? String {
            return value
        }
        
        return ""
    }
    
    static func setToken(value:String) {
        UserDefaults.standard.set(value, forKey: TOKEN)
    }
    
    static func getToken() -> String {
        let value = UserDefaults.standard.value(forKey: TOKEN)
        if let value = value as? String {
            return value
        }
        
        return ""
    }
    /**设置是否有通知配置*/
    static func setChatroomMessageNoti(value:Bool){
        
        UserDefaults.standard.set(value, forKey: CHATROOM_RECEIVE_MESSAGE_NOTI)
    }
    /**获取是否有通知配置*/
    static func getChatroomMessageNoti() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_RECEIVE_MESSAGE_NOTI)
        if let values = value as? Bool {
            return values
        }
        
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return false
        }
        if source.switch_new_msg_notification == "1"{
             return true
        }else{
            return false
        }
       
    }
    /**设置消息接收是否有提示音配置*/
    static func setChatroomRecerveMessageVoice(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_RECEIVE_MESSAGE_VOICE)
    }
    /**获取消息接收是否有提示音配置*/
    static func getChatroomRecerveMessageVoicei() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_RECEIVE_MESSAGE_VOICE)
        if let values = value as? Bool {
            return values
        }
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return false
        }
        if source.switch_room_voice == "1"{
            return true
        }else{
            return false
        }
    }
    /**设置进入房间是否有通知*/
    static func setChatroomEnterRoomNoti(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_ENTER_ROOM_NOTI)
    }
    /**获取进入房间是否有通知配置*/
    static func getChatroomEnterRoomNoti() -> Bool{
        var localShowNoti = false
        var remoteShowNoti = false
        
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return false
        }
        
        let value = UserDefaults.standard.value(forKey: CHATROOM_ENTER_ROOM_NOTI)
        if let values = value as? Bool {
            localShowNoti = values
        }
        
        if source.switch_room_tips_show == "1"{
            remoteShowNoti = true
        }else{
            remoteShowNoti = false
        }
        
        return localShowNoti && remoteShowNoti
    }
    /**设置消息发送是否有提示音*/
    static func setChatroomSendMessageVoice(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_MESSAGE_VOICE)
    }
    /**获取消息发送是否有提示音配置*/
    static func getChatroomSendMessageVoice() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_MESSAGE_VOICE)
        if let values = value as? Bool {
            return values
        }
        //如果用户没有设置取后台默认的开关状态
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return false
        }
        if source.switch_msg_receive_notify == "1"{
            return true
        }else{
            return false
        }
    }
    /**设置发送投注*/
    static func setChatroomSendBetting(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_BETTING)
    }
    /**获取发送投注*/
    static func getChatroomSendBetting() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_BETTING)
        if let values = value as? Bool {
            return values
        }
        return true
    }

    /**设置发送emoji*/
    static func setChatroomSendExpression(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_EXPRESSION)
    }
    /**获取设置发送emoji*/
    static func getChatroomSendExpression() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_EXPRESSION)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    
    /**设置进入房间*/
    static func setChatroomEnterRoom(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_ENTER_ROOM)
    }
    /**获取设置进入房间*/
    static func getChatroomEnterRoom() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_ENTER_ROOM)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    
    /**设置是否可以发送文本*/
    static func setChatroomSendText(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_TEXT)
    }
    /**获取发送文本的配置*/
    static func getChatroomSendText() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_TEXT)
        if let values = value as? Bool {
            return values
        }
        return true
    }

    /**设置是否可以抢红包*/
    static func setChatroomReceiveRedPacket(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_RECEIVE_RED_PACKET)
    }
    /**获取是否可以抢红包*/
    static func getChatroomReceiveRedPacket() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_RECEIVE_RED_PACKET)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    
    /**设置是否可以发送红包*/
    static func setChatroomSendRedPacket(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_RED_PACKET)
    }
    /**获取发送红包*/
    static func getChatroomSendRedPacket() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_RED_PACKET)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    
    /**设置是否可以发送图片*/
    static func setChatroomSendImage(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_IMAGE)
    }
    /**获取发送图片的配置*/
    static func getChatroomSendRedImage() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_IMAGE)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    
    /**设置是否可以发送音频*/
    static func setChatroomSendVoice(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_SEND_VOICE)
    }
    /**获取发送音频的配置*/
    static func getChatroomSendVoice() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_SEND_VOICE)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    /**设置是否可以快捷发言*/
    static func setChatroomFastTalk(value:Bool){
        UserDefaults.standard.set(value, forKey: CHATROOM_FAST_TALK)
    }
    /**获取否可以快捷发言配置*/
    static func getChatroomFastTalk() -> Bool{
        let value = UserDefaults.standard.value(forKey: CHATROOM_FAST_TALK)
        if let values = value as? Bool {
            return values
        }
        return true
    }
    /**获取等级背景相关的颜色*/
    static func setLevelColor(colorDict:[String:Any]){
        UserDefaults.standard.set(colorDict, forKey: LevelBackGroudColor)
    }
    /**获取等级背景相关颜色配置*/
    static func getLevelColor() -> [String:Any]{
        let value = UserDefaults.standard.value(forKey: LevelBackGroudColor) as? [String:Any]
        if let values = value  {
            return values
        }
        return [:]
    }
    
    /**获取等级字体相关的颜色*/
    static func setLevelTitleColor(colorDict:[String:Any]){
        UserDefaults.standard.set(colorDict, forKey: levelTitleColor)
    }
    /**获取等级字体颜色配置*/
    static func getLevelTitleColor() -> [String:Any]{
        let value = UserDefaults.standard.value(forKey: levelTitleColor) as? [String:Any]
        if let values = value  {
            return values
        }
        return [:]
    }
    
    static func setBetWinRate(rate:Float){
        UserDefaults.standard.set(rate, forKey: betWinRate)
    }
    
    static func getBetWinRate() -> Float{
        let value = UserDefaults.standard.value(forKey: betWinRate) as? Float ?? 0
        
        return value
    }
    
    //保存密码
    static func setSaveRoomPassword(passwordDict:[String:[[String:String]]]){
        UserDefaults.standard.set(passwordDict, forKey: roomKeyConfig)
    }
    //获取房间密码
    static func getRoomPassword() ->[String:[[String:String]]]{
        let value = UserDefaults.standard.value(forKey: roomKeyConfig) as? [String:[[String:String]]] ?? [:]
        
        return value
    }
    
    
    
    
}
