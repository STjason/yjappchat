//
//  CRLoginModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/5.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import HandyJSON

class CRNoticeModel: CRBaseModel {
    var source:[CRNoticeResult]?
    required init() {}
}

class CRNoticeResult: HandyJSON {
    var id:String = ""
    var title:String = ""
    var body:String = ""
    var enable:Int?
    var stationId:String = ""
    required init(){}
}
