//
//  CRWelcomeMsgItem.swift
//  gameplay
//
//  Created by admin on 2019/11/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRWelcomeMsgItem: HandyJSON {
//"{\"code\":\"Welcome\",\"nickName\":\"co***31\",\"roomId\":\"4bc576891e1c478c89699ce50e615034_yjtest1_3\"}"
    var code = ""
    var nickName = ""
    var roomId = ""
    var nativeAccount = ""
    
    var msgUUID = "" //自己添加以区别消息
    required init() {}
}
