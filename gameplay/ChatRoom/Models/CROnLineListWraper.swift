//
//  CROnLineListWraper.swift
//  gameplay
//
//  Created by admin on 2019/12/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CROnLineListWraper: CRBaseModel {
    var source:[CROnLineUserModel]?
}

class CROnLineUserModel: HandyJSON {
    var avatarUrl = ""
    var level = ""
    var nickName = ""
    var privatePermission = false
    var id = ""
    var avatar = ""
    var userType = ""
    var speakingFlag = ""
    var account = ""
    var levelIcon = ""
    
//    "avatarUrl": "nullhttps://yj8.me/img/h3eM/i5WRcWKRy.jpg",
//    "level": "渣渣会员",
//    "nickName": "小宝贝",
//    "privatePermission": false,
//    "id": "eee69972b71441939bf1e2882c704c37",
//    "avatar": "https://yj8.me/img/h3eM/i5WRcWKRy.jpg",
//    "userType": 1,
//    "speakingFlag": 0,
//    "account": "abc1234",
//    "levelIcon": ""
    required init() {}
}
