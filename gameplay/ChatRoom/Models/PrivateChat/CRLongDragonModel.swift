//
//  CRLongDragonModel.swift
//  YiboGameIos
//
//  Created by JK on 2020/4/30.
//  Copyright © 2020 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class CRLongDragonBaseWraper: CRBaseModel {
    var source:CRLongDragonModel?
}

class CRLongDragonModel:HandyJSON {
    var longDragon: [CRLongDragonList]?
    required init() {}
}

class CRLongDragonList:HandyJSON {
    var continueStage = ""
    var lotteryCode = ""
    var lotteryName = ""
    var lotteryNum = ""
    var lotteryResult = ""
    var playName = ""
    required init() {}
}
