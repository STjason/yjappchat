//
//  CRPrivateFromUser.swift
//  gameplay
//
//  Created by admin on 2020/2/1.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRPrivateFromUser: HandyJSON {
    var levelName = ""
    var userType = ""
    var avatar = ""
    var id = ""
    var account = ""
    var levelIcon = ""
    
    required init() {}
}
