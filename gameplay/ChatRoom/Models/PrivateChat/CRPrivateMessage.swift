//
//  CRPrivateMessage.swift
//  gameplay
//
//  Created by admin on 2020/2/4.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRPrivateMessage: HandyJSON {
    var msgType = ""
    var code = ""
    var fromUser:CRPrivateFromUser?
    var user:CRPrivateFromUser?
    var accountType = ""
    var inputUserId = ""
    var msgUUID = ""
    var userMessageId = ""
    var userId = ""
    var roomId = ""
    var record = ""
    var outputUserId = ""
    var userType = ""
    var time = ""
    var payId = "" //红包
    required init() {}
}
