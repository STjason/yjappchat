//
//  CRHistoryRecordItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/7.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRHistoryRecordItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:[CRMessage]?
    var status:String = ""
    required init(){}
}

class CRPrivateHistoryRecordItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRPrivateHistoryRecordSourcce?
    var status:String = ""
    required init(){}
}

class CRPrivateHistoryRecordSourcce: HandyJSON {
    var data:[CRMessage]?
    required init() {}
}




