//
//  CRConstants.swift
//  Chatroom
//
//  Created by admin on 2019/7/20.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SocketIO

enum messageType:Int { //消息类型
    /**文本消息*/
    case TEXT_MSG_TYPE = 1
    /**图片消息*/
    case IMAGE_MSG_TYPE = 2
    /**红包消息*/
    case REDPACKET_MSG_TYPE = 3
    /**领红包*/
    case PICK_REDPACKET_MSG_TYPE = 4
    /** 分享注单 */
    case SHARE_BET_MSG_TYPE = 5
}

enum ChatTapAvtarType { //点击用户头后，像选择的事件类型
    /// @ 某人
    case informSomeOne
    ///撤回消息
    case retractMsg
    ///清屏
    case clearScreen
    ///禁言
    case prohibitTalk
    ///取消禁言
    case cancelProhibitTalk
    ///全体禁言
    case prohibitTalkAll
    ///解除全体禁言
    case cancelProhibitTalkAll
    ///查看个人信息
    case personInfo
    ///私聊
    case privateChat
}

var socket:SocketIOClient?
var socketManager:SocketManager?
var isSocketConnect = false
var authenticateModel:CRAuthenticateModel? //授权信息
var currentIsChat = false //聊天室是否正在显示
var isChatActive = false //聊天室是否正在显示
var privatePermission = false //用户是否可以被私聊
var agentRoomHostSelf = false //用户是否是当前房间的代理
var last_chat_roomId = "" {
    didSet {
        CRDefaults.setLastRoom(value: last_chat_roomId)
    }
}

class CRConstants: NSObject {
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
}
