//
//  CRToolPermissonWraper.swift
//  gameplay
//
//  Created by admin on 2020/1/22.
//  Copyright © 2020年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRToolPermissonWraper: CRBaseModel {
    var source:CRToolPermissonSource?
}

class CRToolPermissonSource: HandyJSON {
    var toolPermission:[CRToolPermission]?
    required init() {}
}

class CRToolPermission: HandyJSON {
    var createTime = ""
    var enable = ""
    var id = ""
    var stationId = ""
    var toolCode = ""
    var toolName = ""
    var updateTime = ""
    var userId = ""
    required init() {}
}
