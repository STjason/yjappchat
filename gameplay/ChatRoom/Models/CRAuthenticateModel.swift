//
//  CRAuthenticateModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/9.
//  Copyright © 2019年 yun. All rights reserved.
// 获取验证聊天室信息接口所需参数

import UIKit
import HandyJSON

class CRAuthenticateWrapper: CRBaseModel {
    var content:CRAuthenticateModel?
}

class CRAuthenticateModel: HandyJSON {
    var chatFileUrl = ""
    var encrypted = ""
    var sign = ""
    var clusterId = ""
    var chatDomainUrl = ""
    required init() {}
}
