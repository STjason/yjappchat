//
//  CRPorhibitMsgItem.swift
//  gameplay
//
//  Created by admin on 2020/1/17.
//  Copyright © 2020年 yibo. All rights reserved.
//  禁言 ,socket事件 ‘push_p’ 返回

import UIKit
import HandyJSON

class CRPorhibitMsgItem: CRMessage {
    var prohibitModel:CRProhibitModel?
}

class CRProhibitModel:HandyJSON {
    var code = ""
    var speakingClose = ""
    var source = ""
    var msgUUID = ""
    var operatorAccount = ""
    var speakingCloseId = ""
    var userId = ""
    var channelId = ""
    var roomId = ""
    
    var isBanSpeak = "" //用于房间全体禁言
    required init() {}
}


