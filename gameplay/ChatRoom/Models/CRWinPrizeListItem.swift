//
//  CRWinPrizeListItem.swift
//  gameplay
//
//  Created by Gallen on 2019/12/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRWinPrizeListItem: HandyJSON {
    var msg = ""
    var code = ""
    var success = false
    var source:CRWinningSource?
    var status = ""
    required init(){}
}

class CRWinningSource:HandyJSON{
    var winningList:[CRWinningListItem]?
    required init(){}
}

class CRWinningListItem: HandyJSON {
    var userLevelName = ""
    var prizeDate = ""
    var prizeType = ""
    var prizeMoney = ""
    var id = ""
    var userName = ""
    var prizeProject = ""
    var stationId = ""
    var imageIconUrl = ""
    required init(){}
}


