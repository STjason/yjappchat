//
//  CROnLineListWraper.swift
//  gameplay
//
//  Created by admin on 2019/12/6.
//  Copyright © 2019年 yibo. All rights reserved.
//  管理员列表，私聊列表，在线用户 Model

import UIKit
import HandyJSON

class CROnLineListWraper: CRBaseModel {
    var source:[CROnLineUserModel]?
}

class CROnLineUserModel: HandyJSON {
    var avatarUrl = ""
    var avatar = ""
    var level = ""
    var nickName = ""
    var privatePermission = false
    var id = ""
    var userType = ""
    var speakingFlag = ""
    var account = ""
    var levelIcon = ""
    var levelName = ""
    
    var isRead = "" //本地添加变量，标志未读数

    required init() {}
}
