//
//  CRPrivateChatInitialWraper.swift
//  gameplay
//
//  Created by admin on 2019/12/26.
//  Copyright © 2019年 yibo. All rights reserved.
//  初始化私聊

import UIKit
import HandyJSON

class CRPrivateChatInitialWraper: CRBaseModel {
    var source:CRPrivateChatInitialModel?
}

class CRPrivateChatInitialModel: HandyJSON {
    var passiveUserName = ""
    var createTime = ""
    var id = ""
    var type = ""
    var userId = ""
    var roomId = ""
    required init() {}
}
