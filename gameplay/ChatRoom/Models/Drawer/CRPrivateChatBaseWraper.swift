//
//  CRPrivateChatBaseWraper.swift
//  gameplay
//
//  Created by admin on 2019/12/26.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRPrivateChatBaseWraper: CRBaseModel {
    var source:CRPrivateChatBaseModel?
}

class CRPrivateChatBaseModel:HandyJSON {
    var type = ""
    var roomId = ""
    required init() {}
}
