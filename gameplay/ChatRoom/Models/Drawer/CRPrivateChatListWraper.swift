//
//  CRPrivateChatListWraper.swift
//  gameplay
//
//  Created by admin on 2019/12/25.
//  Copyright © 2019年 yibo. All rights reserved.
//  私聊列表

import UIKit
import HandyJSON

class CRPrivateChatListWraper: CRBaseModel {
    var source:CRPrivateChatModel?
}

class CRPrivateChatModel:HandyJSON {
    var type = ""
    var roomList:[CRPrivateChatRoomList]?
    required init() {}
}

class CRPrivateChatRoomList:HandyJSON {
    var lastRecordTime = ""
    var lastRecordType = ""
    var lastRecord = ""
    var isRead = ""
    var roomId = ""
    var remarks = ""
    var fromUser: CROnLineUserModel?
    required init() {}
}
