//
//  CRUrls.swift
//  Chatroom
//
//  Created by admin on 2019/7/4.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

enum sendMessageType:String {
     case appealBand = "R70026" //申请解除禁言
    /**聊天室个人中心今日分享*/
    case shareToday = "R0001"
    /**文本消息*/
    case text = "R7001"
    /**图片消息*/
    case image = "R7024"
    /**红包消息*/
    case redPacket = "R7009"
    /**查看后台红包详情*/
    case lookPacketDetail = "R7015"
    /**系统公告*/
    case systemNotice = "R7025"
    /**抢红包*/
    case receiveRedPacket = "R7027"
    /**抢红包*/
    case receiveRedPacketNew = "R0013"
    /**拉取所有彩种*/
    case pullLotteryTicket = "R7028"
    /**历史记录*/
    case historyRecord = "R7029"
    /**违禁词*/
    case contrabandLanguage = "R7030"
    /**聊天室系统配置*/
    case chatroomSystemConfiguration = "R7031"
    /**获取领取红包详情*/
    case receiveDetail = "R7032"
    /** 聊天室历史投注页面 */
    case historyBetShare = "R7033"
    /**获取所有系统的图像列表*/
    case userIcon = "R7035"
    /**用户今天的输赢明细*/
    case userTodayWinLos = "R7036"
    /**用户的个人信息*/
    case userInfo = "R7038"
    /**中奖榜单,盈利榜单数据*/
    case winPrizeList = "R7039"
    /**分享注单*/
    case shareOrder = "R7008"
    /**跟单*/
    case followOrder = "R7010"
    /**更新聊天室头像*/
    case updatePersonnalAvatar = "R7020"
    ///提交修改用户的个人信息
    case submitUserInfo = "R7034"
    ///在线用户列表
    case roomOnLineList = "R0010"
    //管理员列表
    case managerList = "R0041"
    ///获取当前余额
    case currentBalance = "R0034"
    ///导师计划
    case tutorPlan = "R0040"
    ///签到
    case signIn = "R7037"
    ///私聊 初始化相关
    case privateChatInit = "R0500"
    ///私聊 发消息相关
    case privateChatSend = "R7051"
    case privateHistory = "R0501"
    ///禁言，取消禁言
    case prohibit = "R0009"
    ///禁言，取消禁言 所有
    case prohibitAll = "R0029"
    ///撤回
    case retract = "R0102"
    ///工具箱权限
    case toolPermisson = "R0008"
    ///长龙
    case longDragon = "R70043"
}

var requestMessageType:sendMessageType = .text

/// 推送-接收
let CR_PUSH_R = "push_p"
/// 登录-发送
let CR_LOGIN_S = "LOGIN"
/// 登录-接收
let CR_LOGIN_R = "login"
/// 发消息-发送
let CR_USER_S = "USER_R"
/// 发消息-接收
let CR_USER_R = "user_r"
/// 换房间-发送
let CR_USER_JOIN_ROOM_S = "USER_JOIN_ROOM"
/// 换房间-接收
let CR_USER_JOIN_ROOM_R = "user_join_room"
/// 加入组-发送
let CR_USER_JOIN_GROUP_S = "USER_JOIN_GROUP"
/// 加入组-接收
let CR_USER_JOIN_GROUP_R = "user_join_group"
/// 重连-发送
let CR_REONNNECT_S = "REONNNECT"
/// 重连-接收
let CR_REONNNECT_R = "reconnect_r"

class CRUrl {
    static let instance = CRUrl()
    
    static func shareInstance() -> CRUrl {
        return instance
    }
    
    //http://a07.com:9999/play_core_war
    var CRCHAT_URL:String = "https://testapi.yibochat.com/"
//    var CRCHAT_URL:String = "http://192.168.9.37:6700"
    
    //url
//    var CHAT_FILE_BASE_URL = "";//文件系统域名 图片
    var CHAT_FILE_BASE_URL = "http://testfile.yibochat.com"
    
//        var BASE_URL_CHAT = "https://testapp.yibochat.com/app"//APP聊天室域名
    //本地
//    var BASE_URL_CHAT = "http://192.168.9.37:8070/app"//APP聊天室域名

    /**发送文本消息*/
//        let URL_CHAT_SEND_MESSAGE = "https://testapp.yibochat.com/app/nativeTestApi"
    
    //本地
//    let URL_CHAT_SEND_MESSAGE = "http://192.168.9.37:8070/app/nativeTestApi"
    
    //code
    var LOGIN_AUTHORITY = "R70000";//登录并授权
    var CHAT_ROOM_LIST = "R7023";//房间列表
    var JOIN_CHAT_ROOM = "R7022";//进入房间
    var SEND_MSG_TXT = "R7001"//发送文本消息---使用socket
    var CODE_SHARE_BET_MSG = "R7008" //分享注单
    var GET_NOTICE_LIST = "R7025" //获取公告列表
    var CODE_FOLLOW_BET = "R7010" //跟单
    var CODE_HISTORY_SHARE = "R7033" //聊天室注单历史列表
    var CODE_ROOM_CONFIG = "R7031" //拉取房间配置
    var CODE_BAND_WORDS = "R7030" //拉去违禁词
    var CODE_GAME_LIST = "R0028"// 彩票列表
    var CODE_SPEAKQUICKLY_LIST = "R0011" // 拉取快速发言列表
    var CODE_ONLINE_LIST = "R0010"// 拉取房间在线用户
    var CODE_IMAGECOLLECT = "R0038" // 图片收藏
    
    
    let URL_LOGIN = "/native/login.do"//登录
//    let URL_AUTHENTICATE = "/native/native_chatroom_url.do" //聊天室授权
    let URL_AUTHENTICATE = "/native/nativeChatAddr.do" //聊天室授权
    let URL_AUTH = "/nativeTestApi" //聊天室
    /**上传文件*/
    let URL_UPLOAD_FILE = "/native_add_file"
    /**上传头像*/
    let URL_UPLOAD_IMAGE = "/add_file"
    /**读取文件*/
    let URL_READ_FILE = "/native_read_file"
    let URL_WEB_READ_FILE = "/read_file"
    
    /// 根据url最后的路径获取完整的URL
    ///
    /// - Parameters:
    ///   - URLString: url最后的拼接部分
    ///   - uniqueForChat: 非聊天室专有接口，如聊天室中的和聊天无关的投注功能，直接使用原app中的 base_url
    func getcompleteURLWith(URLString:String,uniqueForChat:Bool = true,useOriginalUrl:Bool = false) -> String {
   
        let port = ""

        //选择 BASE_URL_CHAT
        let old_URLs = [URL_AUTHENTICATE]
        var finalURL = ""
        
        if useOriginalUrl {
            finalURL = URLString
        }else if !uniqueForChat {
            finalURL = BASE_URL + port + URLString
        }else if old_URLs.contains(URLString) {
            finalURL = BASE_URL + port + URLString
        }else {
            finalURL = CRCHAT_URL + "/app" + port + URLString
        }
        
        return finalURL
    }
    
    func getChatSendMessage() -> String {
//        return "http://192.168.9.37:18081/app" + "/app/nativeTestApi"
        return CRCHAT_URL + "/app/nativeTestApi"
    }
    
}

