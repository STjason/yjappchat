//
//  CRBaseModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/5.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import HandyJSON

class CRBaseModel: HandyJSON {
    var success = false
    var msg = ""
    var accessToken = ""
    var code = ""
    var status = ""
    required init(){}
}
