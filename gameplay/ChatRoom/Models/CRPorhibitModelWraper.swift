//
//  CRPorhibitModelWraper.swift
//  gameplay
//
//  Created by admin on 2020/1/21.
//  Copyright © 2020年 yibo. All rights reserved.
//  禁言，解除禁言

import UIKit
import HandyJSON

class CRPorhibitModelWraper: CRBaseModel {
    var source: CRPorhibitSource?
}

class CRPorhibitSource:HandyJSON {
    //禁言个人----
    var speakingClose = ""
    var userId = ""
    
    //房间禁言----
    var msg = ""
    var code = ""
    var roomId = ""
    var isBanSpeak = ""
    
    required init() {}
}
