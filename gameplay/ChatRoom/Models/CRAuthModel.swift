//
//  CRAuthModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/10.
//  Copyright © 2019年 yun. All rights reserved.
//  授权聊天室

import UIKit
import HandyJSON

class CRAuthWrapper: HandyJSON {
    var msg = ""
    var code = ""
    var success = false
    var dataKey = "" //加密的key
    var status = ""
    var source:CRAuthMsgModel?
    required init() {}
}

class CRAuthMsgModel: HandyJSON {
    var selfUserId = ""
    var userType:Int = 1 //1普通玩家，2后台管理员，3机器人，4后台管理员 5前台普通用户
    var securityKey = ""
    var stationId = ""
    /**业务系统的类型：1会员，2代理，3总代理，4普通试玩账号*/
    var accountType:Int = 0
    var userConfigRoom:CRAgentRoomItem? //为用户设置的默认房间，不需要密码可直接进入
    var agentRoom:CRAgentRoomItem? //代理房
    //    var userTmp:CRAuthUserTmp?
    //    var channelId = ""
    var roomEntify:CRRoomListModel?
    required init() {}
}

class CRAgentRoomItem:HandyJSON{
    var agentUserCode:String = ""
    var roomImg:String = ""
    var name:String = ""
    var id:String = ""
    var switchAgentEnterPublicRoom = ""
    var needRoomKey = "" //房间密码
    
    required init() {}
}

//class CRAuthWrapper: HandyJSON {
//    var code = ""
//    var success = false
//    var status = ""
//    var msg:CRAuthMsgModel?
//    required init() {}
//}
//
//class CRAuthMsgModel: HandyJSON {
//    var selfUserId = ""
//    var userType:Int?
//    var securityKey = ""
//    var stationId = ""
//
////    var userTmp:CRAuthUserTmp?
////    var channelId = ""
//    required init() {}
//}


//class CRAuthUserTmp: HandyJSON {
//    var account = ""
//    var accountType:Int?
//    var createTime:Int64 = 0
//    var id = ""
//    var levelName = ""
//    var stationId = ""
//    var updateTime:Int64 = 0
//    var userClust = ""
//    var userCode = ""
//    var userDetailEntity:UserDetailEntity?
//    var userType:Int?
//    required init() {}
//}
//
//class UserDetailEntity:HandyJSON {
//    var avatarCode = ""
//    var id = ""
//    var userId = ""
//    required init() {}
//}
