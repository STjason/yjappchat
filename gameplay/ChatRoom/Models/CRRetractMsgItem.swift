//
//  CRRetractMsgItem.swift
//  gameplay
//
//  Created by admin on 2020/1/17.
//  Copyright © 2020年 yibo. All rights reserved.
//  撤回消息 ,socket事件 ‘push_p’ 返回

import UIKit
import HandyJSON

class CRRetractMsgItem: CRMessage {
    var rectractModel:CRRetractModel?
}

class CRRetractModel: HandyJSON {
    var code = ""
    var nickName = ""
    var msgId = "" //被撤回的消息的 id
    var userType = ""
    var msgUUID = ""
    var roomId = ""
    required init() {}
}

