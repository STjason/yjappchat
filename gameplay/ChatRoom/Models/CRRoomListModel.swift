//
//  CRRoomListModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/11.
//  Copyright © 2019年 yun. All rights reserved.
//  获取房间 list

import UIKit
import HandyJSON

class CRRoomListWrapper: HandyJSON {
    var msg = ""
    var code = ""
    var success = false
    var status = ""
    var source:CRRoomListModels?
    var msgUUID = ""
    required init() {}
}

class CRRoomListModels: HandyJSON {
    var data:[CRRoomListModel]?
    var success = false
    required init() {}
}

//房间model
class CRRoomListModel: HandyJSON {
    var remark = "" //房间描述
    var roomUserCount = 0 // 房间人数
    var createTime:Int64 = 0
    var enable:Int?
    var id = ""
    var index:Int?
    var isBanspeak:Int?
    var speakingFlag:Int = 0
    var joinCondition = ""
    var name = ""
    var roomImg = ""
    var roomKey = ""
    var stationId = ""
    var type:Int?
    var updateTime:Int64 = 0
    
    var agentUserCode = ""//如果是代理房，从R7000接口返回中会有该字段，赋值给该对象，登录房间时取用
    required init() {}
}
