//
//  CRPlanMsgItem.swift
//  gameplay
//
//  Created by admin on 2019/11/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRPlanMsgItem: CRMessage {
    var planItem:CRPlanMessage?
    required init() {}
}

class CRPlanMessage: HandyJSON {
    var lotteryCode = ""
    var text = ""
    var lotteryName = ""
    var msgUUID = ""
    var msgId = ""
    var nickName = ""
    var avatar = ""
    var account = ""
    required init() {}
}


