//
//  CRLoginModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/5.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import HandyJSON

class CRLoginWraper: CRBaseModel {
    var content:CRLoginResult?
}

class CRLoginResult: HandyJSON {
    var accountType:Int?//用户模式 1-会员 6-游客
    var account:String = ""//当前帐号
    var money:Float = 0;//account balance
    required init(){}
}
