//
//  CRShareBetInfo.swift
//  gameplay
//
//  Created by admin on 2019/9/21.
//  Copyright © 2019年 yibo. All rights reserved.

import UIKit
import HandyJSON

//  用于分享注单后显示注单信息使用,由app传给后台，后台会原样返回
//对象示例：
//"betInfo": {
//"lottery_play": "万位",
//"showZhuShu": true,
//"lottery_amount": "2.0",
//"lottery_content": "2",
//"orderId": "C19111900133",
//"playName": "万位",
//"ago": 0,
//"buyMoney": 2.0,
//"buyZhuShu": 1,
//"haoMa": "2",
//"version": "1",
//"lotName": "老重庆时时彩",
//"lotCode": "LCQSSC",
//"qiHao": "20191119094",
//"model": 1,
//"lottery_qihao": "20191119094",
//"lottery_type": "老重庆时时彩",
//"lottery_zhushu": "1"
//}
class CRShareBetInfo: HandyJSON {
    var icon = ""
    var lotIcon = ""
    var lottery_amount = ""
    var lottery_content = ""
    var lottery_play = ""
    var lottery_qihao = ""
    var lottery_type = ""
    var lottery_zhushu = ""
    var lotCode = "" //彩票code
    var version = "" //1官方 2信用
    var model = 1 //1元，10角，100分
    var ago:Int64 = 0
    var orderId = ""
    var showZhuShu = ""
    var playName = ""
    var buyMoney = ""
    var buyZhuShu = ""
    var haoMa = ""
    var lotName = ""
    var qiHao = ""
    
    var betMoney = ""
    
    required init() {}
}
