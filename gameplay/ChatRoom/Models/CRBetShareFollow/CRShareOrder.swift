//
//  CRShareOrder.swift
//  gameplay
//
//  Created by admin on 2019/9/23.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

//对象示例
//"orders": [{
//    "betMoney": "52.0",
//    "orderId": "C19092100025"
//    }],
class CRShareOrder: HandyJSON {
    var betMoney = ""
    var orderId = ""
    required init() {}
}

