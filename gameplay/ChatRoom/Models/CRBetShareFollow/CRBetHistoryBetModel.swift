//
//  CRBetHistoryBetModel.swift
//  gameplay
//
//  Created by admin on 2019/10/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRBetHistoryWraper: HandyJSON {
    var msg = ""
    var code = ""
    var success = false
    var status = ""
    var source:CRBetHistorySource?
    required init() {}
}

class CRBetHistoryMsgModel: HandyJSON {
    var accountId = ""
    var buyIp = ""
    var buyMoney : Double = 0
    var buyOdds = ""
    var buyZhuShu = 0
    var canUndo = false
    var cheat = false
    var closedTime = ""
    var createTime = ""
    var currentRebate : Float = 0
    var haoMa = ""
    var icon = ""
    var id = ""
    var kickback : Float = 0
    var lotCode = ""
    var lotType = 0
    var lotVersion = 0
    var model = 0
    var multiple = 0
    var oddsCode = ""
    var orderId = ""
    var playCode = ""
    var playName = ""
    var playType = ""
    var proxyRollback = 0
    var qiHao = ""
    var rollBackStatus = 0
    var sellingTime = ""
    var signKey = ""
    var stationId = 0
    var status = 0
    var terminalBetType = 0
    var updateTime = ""
    var username = ""
    var zhuiHao = 0
    required init() {}
}

class CRBetHistorySource: HandyJSON {
    var showZhuShu = false
    var sourceClient = ""
    var isPrivate = false
    var msg:[CRBetHistoryMsgModel]?
    required init() {}
}
