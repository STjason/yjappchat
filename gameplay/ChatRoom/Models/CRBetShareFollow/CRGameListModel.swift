//
//  CRGameListModel.swift
//  gameplay
//
//  Created by JK on 2019/12/2.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CRGameListModel: NSObject {
    
    /** 站点Id */
    var stationId : String?
    /** 操作选项 1--初始列表展示数据 2--切换彩种 3--切换玩法 */
    var option : String?
    /** 彩种编码 */
    var lotteryCode : String?
    /** 玩法名称 */
    var playName : String?
    /** 所有站点Id */
    var stationList : Array<String>?
    /** 开奖的结果 */
    var lotteryResult : String?
    /** 期号 */
    var stage : String?
    /** 账号 */
    var sysUserAccount : String?
    
    func getCurrentJson() -> CRGameListModel {
        let model = CRGameListModel()
        
        
        return model
    }
    
}
