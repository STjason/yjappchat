//
//  CRShareBetModel.swift
//  gameplay
//
//  Created by admin on 2019/9/21.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRShareBetModel: HandyJSON {
    var roomId = ""
    var agentRoomHost = ""
    var stationId = ""
    var code = ""
    var msgUUID = ""
    var msgId = ""
    var source = ""
    var userId = ""
    /**发送消息的类型 1.快捷发言 2.自由发言 3.发送表情 4.投注消息 5.@其它用户的消息 6.快捷图片*/
    var speakType:String = ""
    var userType:String = ""// 传1
    var betInfo:CRShareBetInfo?
    var betInfos:[CRShareBetInfo]?
    var orders:[CRShareOrder]?
    var winOrLost:CRWinRateItem?
    var multiBet:String = "1"
    required init() {}
    
//    func copy() -> Copyable {
//        let p = CRShareBetModel()
//        p.roomId = roomId
//        p.agentRoomHost = agentRoomHost
//        p.stationId = stationId
//        p.code = code
//        p.msgUUID = msgUUID
//        p.source = source
//        p.userId = userId
//        p.speakType = speakType
//        p.userType = userType
//        p.betInfo = betInfo
//        p.betInfos = betInfos
//        p.orders = orders
//        p.winOrLost = winOrLost
//        p.multiBet = multiBet
//        return p
//    }

}
