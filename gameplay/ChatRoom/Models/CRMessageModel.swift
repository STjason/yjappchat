//
//  CRMessageModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/11.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import HandyJSON

class CRMessageModel: HandyJSON {
    var code = ""
    var source = ""
    var userId = ""
    var content:ContentBean?
    var roomId = ""
    var stationId = ""
    var msgResult:MsgResultBean?
    var msgType:Int?
  
    required init() {}
}


class ContentBean: HandyJSON {
    var msgType:Int?
    var fromUser:FromUserBean?
    var record = ""
    var userType:Int?
    var time = ""
    var userMessageId = ""
    var userId = ""
    var roomId = ""
    var stationId = ""
    var imageString:String = ""
    required init() {}
}

class FromUserBean: HandyJSON {
    var avatarCode:Int?
    var levelName = ""
    var id = ""
    var account = ""
    var levelIcon = ""
    required init() {}
}

class MsgResultBean: HandyJSON {
    var userEntity:UserEntityBean?
    var userMsgEntity:UserMsgEntityBean?
    required init() {}
}

class UserEntityBean: HandyJSON {
    var account = ""
    var accountType:Int?
    var createTime:Int64 = 0
    var id = ""
    var levelName = ""
    var stationId = ""
    var updateTime:Int64 = 0
    var userClust = ""
    var userCode = ""
    var userDetailEntity:UserDetailEntityBean?
    var userType:Int?
    required init() {}
}

class UserMsgEntityBean: HandyJSON {
    var createTime:Int64 = 0
    var enable:Int?
    var id = ""
    var roomId = ""
    var stationId = ""
    var tableName = ""
    var textRecord = ""
    var type:Int?
    var userId = ""
    var userType:Int?
    required init() {}
}

class UserDetailEntityBean: HandyJSON {
    var avatarCode = ""
    var id = ""
    var userId = ""
    required init() {}
}




