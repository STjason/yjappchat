//
//  CRProhibitLanguageItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/11.
//  Copyright © 2019年 yibo. All rights reserved.
// 违禁词model

import UIKit
import HandyJSON

class CRProhibitLanguageItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:[CRProhibitLanguageSourceItem]?
    var status:String = ""
    required init(){}
}

class CRProhibitLanguageSourceItem:HandyJSON{
    var id:String = ""
    var word:String = ""
    var statId:String = ""
    var enable:String = ""
    var updateTime:String = ""
    var createTime:String = ""
    required init(){}
}

