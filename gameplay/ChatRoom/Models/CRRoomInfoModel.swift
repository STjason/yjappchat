//
//  CRRoomInfoModel.swift
//  Chatroom
//
//  Created by admin on 2019/7/11.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import HandyJSON

class CRRoomInfoWrapper: HandyJSON {
    var msg = ""
    var code = ""
    var success = false
    var status = ""
    var source:CRRoomInfoModel?
    required init() {}
}

class CRRoomInfoModel: HandyJSON {
    var privatePermission:Bool = false
    var type:String = ""
    var userId = ""
    var depositMobileUrl = ""
    var lotteryImageUrl = ""
    var title = ""
    var agentRoomHost:String = ""
    /***/
    var speakingFlag:Int?
    var drawUrl = ""
    var roomId = ""
    var score:Int?
    var toolPermission = [String]() //权限，用户是否可以禁言，撤回等
    var mobileUrl = ""
    var levelIcon = ""
    var roomPassWord = ""
    var hasSpeak = false
    var level = ""
    var accountType:Int?
    var signDay:Int?
    var signed = ""
    var avatar:String = ""
    /**整个房间禁言 1 全体禁言*/
    var isBanSpeak:Int = 0
    var depositUrl = ""
    var drawMobileUrl = ""
    var roomKey = ""
    var userType:Int = 1
    var account = ""
    var planUser = 0// 为1则为计划员
    var nickName:String = ""
    var nativeAccount = ""
    var backGround = ""//房间背景图
    var dailyMoney:CRRoomDailyMoneyModel?
    var permissionObj:CRPermissionObjItem?
    var winOrLost:CRWinRateItem?
    required init() {}
}

class CRRoomDailyMoneyModel: HandyJSON {
    var depositAmount:Double = 0
    var sbSportsWinAmount:Double = 0
    var realRebateAmount:Double = 0
    var sportsRebateAmount:Double = 0
    var realWinAmount:Double = 0
    var sbSportsRebateAmount:Double = 0
    var realBetAmount:Double = 0
    var chessWinAmount:Double = 0
    var chessRebateAmount:Double = 0
    var egameRebateAmount:Double = 0
    var lotteryWinAmount:Double = 0
    var sbSportsBetAmount:Double = 0
    var chessBetAmount:Double = 0
    var egameWinAmount:Double = 0
    var sportsWinAmount:Double = 0
    var sportsBetAmount:Double = 0
    var lotteryRebateAmount:Double = 0
    var depositArtificial:Double = 0
    var egameBetAmount:Double = 0
    var lotteryBetAmount:Double = 0
    var withdrawAmount:Double = 0
    var proxyRebateAmount:Double = 0
    required init() {}
}


class CRImgEntity:HandyJSON{
    var code:String = ""
    var localPath:String = ""
    var roomId:String = ""
    var size:String = ""
    var permissionObj:String = ""
    var source:String = ""
    var thumbPath:String = ""
    var type:String = ""
    var userId:String = ""
    var record:String = ""
    required init(){}
}

class CRPermissionObjItem:HandyJSON{
    var SEND_BETTING:String = ""
    var SEND_EXPRESSION:String = ""
    var ENTER_ROOM:String = ""
    var SEND_TEXT:String = ""
    var RECEIVE_RED_PACKET:String = ""
    var SEND_RED_PACKET:String = ""
    var SEND_IMAGE:String = ""
    var FAST_TALK:String = ""
    var SEND_AUDIO  = ""
   required init(){}
}

//胜率
class CRWinRateItem:HandyJSON{
    var allBetZhuShu = ""
    var sumDepost = ""
    var sumDepostToday = ""
    var allWinZhuShu = ""
    var allWinAmount = ""
    var yingkuiAmount = ""
    var betNum = ""
    var success = ""
    //胜率
    var winPer:Float = 0
    var allBetAmount = ""
    required init(){}
}

