//
//  CRUploadImageItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/20.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class CRUploadImageItem: HandyJSON {
    var fileCode:String = ""
    var fileString:String = ""
    var success:Bool = false
    var msgUUID = ""
    required init() {}
}
