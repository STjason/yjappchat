//
//  CRUserInfoCenterItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRUserInfoCenterItem: HandyJSON,Copying {
    required init(original: CRUserInfoCenterItem) {
        msg = original.msg
        code = original.code
        success = original.success
        source = original.source
        status = original.status
    }
    
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRUserInfoCenterSource?
    var status:String = ""
    required init(){}
}

class CRUserInfoCenterSource:HandyJSON{
    var depositMobileUrl:String = ""
    var lotteryImageUrl:String = ""
    var winLost:CRUserInfoCenterSourceWinLostItem?
    var level:String = ""
    var nickName:String = ""
    var nativeAccount = ""
    var accountType:String = ""
    var signDay:String = ""
    var signed:String = ""
    var avatar:String = ""
    var drawUrl:String = ""
    var score:String = ""
    var depositUrl:String = ""
    var drawMobileUrl:String = ""
    var mobileUrl:String = ""
    var account:String = ""
    var levelIcon:String = ""
    required init(){}
}

class CRUserInfoCenterSourceWinLostItem:HandyJSON{
    var allBetZhuShu:Float = 0
    var sumDepost:Float = 0
    var sumDepostToday:Float = 0
    var allWinZhuShu:Float = 0
    var allWinAmount:Float = 0
    var yingkuiAmount:Float = 0
    var betNum:Float = 0
    var success:Float = 0
    var winPer:Float = 0
    var allBetAmount:Float = 0

    required init(){}
}
