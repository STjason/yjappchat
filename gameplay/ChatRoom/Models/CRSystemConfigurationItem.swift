//
//  CRSystemConfigurationItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/10.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class CRSystemConfigurationItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRSystemConfigurationSource?
    var status:String = ""
    required  init() {}
}
class CRSystemConfigurationSource:HandyJSON{
    var switch_currency_unit_show = ""
//    ("注单货币单位选择", "select", "yuan:1,jiao:0,fen:0","系统配置", 1033,"元.yuan:1,jiao:0,fen:0;元角.yuan:1,jiao:1,fen:0;元角分.yuan:1,jiao:1,fen:1;元分.yuan:1,jiao:0,fen:1;角分.yuan:0,jiao:1,fen:1;角.yuan:0,jiao:1,fen:0;分.yuan:0,jiao:0,fen:1"),
    var name_win_lottery_animation_interval =  "" //开奖结果间隔
    var file_url:String = ""
    /**在线客服*/
    var name_station_online_service_link:String = ""
    var name_red_bag_remark_info:String = ""
    var switch_front_bet_show:String = ""
    var name_vip_nick_name_update_num:String = ""
    var switch_lottery_result_default_type_show:String = ""
    ///房间列表显示开关 0不显示，1显示 
    var switch_room_show:String = ""
    var switch_room_voice:String = ""
    /***/
    var name_word_color_info:String = ""
    var switch_check_in_show:String = ""
    /**追加在线人数*/
    var name_room_people_num:Int = 0
    var switch_open_simple_chat_room_show:String = ""
    /**自定义用户名开关*/
    var switch_user_name_show:String = ""
    /**字体颜色*/
    var switch_chat_word_size_show:Int = 16
    var switch_send_image_show:String = ""
    /**中奖榜单*/
    var switch_winning_list_show:String = ""
    ///盈利榜单开关
    var switch_today_earnings_list_info = "1"
    /**新成员的会员图像*/
    var name_new_members_default_photo:String = ""
    var switch_front_admin_ban_send:String = ""
    /**等级图标显示开关*/
    var switch_level_ico_show:String = ""
    /**投注命中率 用户的胜率高于该胜率显示*/
    var name_user_win_tips_per:Float = 0
    var switch_level_show:String = ""
    /**在线人数只有管理员可见*/
    var switch_room_people_admin_show:String = ""
    /**聊天背景颜色选择*/
    var native_name_backGround_color_info :[CRLevelBackgroudColorItem] = []
    
    var switch_room_tips_show:String = ""
    /**开奖结果开关*/
    var switch_lottery_result_show:String = ""
    /**红包是否显示备注文字开关*/
    var switch_red_bag_remark_show:String = ""
    /**消息提示音*/
    var switch_msg_tips_show:String = ""
    /**盈亏开关*/
    var switch_yingkui_show:String = ""
    /**发送红包开关*/
    var switch_red_bag_send:String = ""
    /**红包领取详情开关*/
    var switch_red_info:String = ""
    /**等级文字颜色选择*/
    var native_name_level_title_color_info:[CRLevelBackgroudColorItem] = []
    /**打码量开关*/
    var switch_bet_num_show:String = "1"

    var switch_plan_user_show = "1" //导师计划开关
    /**彩票计划开关*/
    var switch_plan_list_show = "1"
    var switch_long_dragon_show = "1" //长龙开关
    ///禁言同时是否删除消息
    var switch_ban_speak_del_msg = "0" //0不删，1 删
    /** 聊天室消息通知开关 */
    var switch_new_msg_notification = "0"
    /** 消息接收提示音开关 */
    var switch_msg_receive_notify = "0"
    /** 中奖榜单轮播开关 */
    var switch_winning_banner_show = ""
    ///聊天记录拉取历史记录数目
    var switch_history_perpage_count = ""
    ///达到投注金额自动分享注单(元)
    var name_auto_share_bet_arrive_amount = ""
    var switch_sum_recharge_show = "1";//会员总充值显示开关
    var switch_today_recharge_show = "1";//会员今日充值显示开关
    var switch_local_ava = "0" //聊天室开启自定义头像，0 关闭，1 开启
    
    required  init() {}
}


/**等级级别颜色*/
class CRLevelBackgroudColorItem:HandyJSON{
    /**颜色*/
    var color:String = ""
    /**会员等名称*/
    var levelName:String = ""
    
    required init(){}
}
