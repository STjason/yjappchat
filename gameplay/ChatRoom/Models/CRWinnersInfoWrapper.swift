//
//  CRWinnersInfoWrapper.swift
//  gameplay
//
//  Created by admin on 2019/10/21.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRWinnersInfoWrapper: CRBaseModel {
    var source:CRWinnerSource?
    required init() {}
}
    
class CRWinnerSource:HandyJSON {
    var winningList:[CRWinnersInfo]?
    var msgUUID = ""
    var status = ""
    required init() {}
}

class CRWinnersInfo:HandyJSON {
    var id = ""
    var prizeDate = ""
    var prizeMoney = ""
    var prizeProject = ""
    var prizeType = ""
    var stationId = ""
    var userLevelName = ""
    var userName = ""
    required init() {}
}
