//
//  CRProgressView.swift
//  gameplay
//
//  Created by Gallen on 2019/9/27.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRProgressView: UIView {
    //进度条
    var progress:Double = 0{
        didSet{
            setNeedsDisplay()
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        //路径
        let center = CGPoint(x: rect.width * 0.5, y: rect.height * 0.5)
        let radius = rect.width * 0.5 - 3
        let startAngle = CGFloat(-Double.pi / 2)
        let endAngle = CGFloat((2 * Double.pi) * progress) + startAngle
        // 创建贝塞尔曲线
        let path = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        UIColor(white: 1.0, alpha: 0.4).setFill()
        // 绘制一条中心点的线
        path.addLine(to: center)
        path.close()
        path.fill()
    
    }

}
