//
//  NSFileManager+Extension.swift
//  gameplay
//
//  Created by Gallen on 2019/9/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

extension FileManager{
    
   class func pathUserChatImage(imageName:String) -> String {
        let path = String(format:"%@/User/%@/Chat/Images/",documentsPath(),CRDefaults.getUserID())
    
        if !FileManager.default.fileExists(atPath: path){
            //            let error:Error? = nil
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
       }
    
        return path + imageName
    }
   class func documentsPath() -> String{
        let pathArray = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = pathArray[0]
        return documentsDirectory
    }
    
    class func pathExpressionForGroupID(groudId:String) -> String{
        let path = String.init(format:"%@/Expression/%@/",FileManager.documentsPath(),groudId)
        if !FileManager.default.fileExists(atPath: path) {
            let error:Error?
            try?  FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        
        return path
    }
    /**音频路径*/
    class func pathUserChatVoice(voiceName:String) -> String{
        let path = String(format:"%@/User/Chat/Voices/",FileManager.documentsPath())
        if !FileManager.default.fileExists(atPath: path) {
           try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        return path + "\(voiceName)"
    }
    
    /**本地转amr路径 上传给服务器*/
    class func pathUploadVoicePath(amrPath:String) -> String{
        let path = String(format:"%@/User/Chat/UploadAmrPath/",FileManager.documentsPath())
        if !FileManager.default.fileExists(atPath: path) {
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        return path + "\(amrPath)"
    }
    
    /**下载的音频路径*/
    class func pathUserDonwloadVoice(voiceName:String) -> String{
        let path = String(format:"%@/User/Chat/DonwLoadVoices/",FileManager.documentsPath())
        if !FileManager.default.fileExists(atPath: path) {
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        return path + "\(voiceName)"
    }
    
    /**判断是否是文件夹的方法*/
    class func  directoryIsExists (path: String) -> Bool {
        
        var directoryExists = ObjCBool.init(false)
        
        let fileExists = FileManager.default.fileExists(atPath: path, isDirectory: &directoryExists)
        
        return fileExists && directoryExists.boolValue
    }
}
