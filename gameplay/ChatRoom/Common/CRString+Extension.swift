//
//  String+Extension.swift
//  Chatroom
//
//  Created by admin on 2019/7/10.
//  Copyright © 2019年 yun. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func getUTF8String() -> String {
        return String(describing: self.cString(using: String.Encoding.utf8))
    }
    
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    func toMessageString()->NSAttributedString{
        //1、创建一个可变的属性字符串
        let attributeString = NSMutableAttributedString.init(string: self)
        let font = getChatRoomSystemConfigFromJson()?.source?.switch_chat_word_size_show ?? 0
        var textFont = CGFloat(font / 2)
        if textFont == 0  {
            textFont = 16
        }else{
            if textFont < 16{
                textFont = 16
            }
        }
        attributeString.addAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: textFont)], range: NSRange(location: 0, length: self.utf16.count))
        //2、通过正则表达式来匹配字符串
//        let regex_emoji = "\\[[a-zA-Z0-9\\/\\u4e00-\\u9fa5]+\\]"; //匹配表情
        return attributeString;
    }
}
extension String {
    var unicodeStr:String {
        let tempStr1 = self.replacingOccurrences(of: "\\u", with: "\\U")
        let tempStr2 = tempStr1.replacingOccurrences(of: "\"", with: "\\\"")
        let tempStr3 = "\"".appending(tempStr2).appending("\"")
        let tempData = tempStr3.data(using: String.Encoding.utf8)
        var returnStr:String = ""
        do {
            returnStr = try PropertyListSerialization.propertyList(from: tempData!, options: [.mutableContainers], format: nil) as! String
        } catch {
            print(error)
        }
        return returnStr.replacingOccurrences(of: "\\r\\n", with: "\n")
    }
}
