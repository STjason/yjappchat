//
//  UIScrollView + Extension.swift
//  gameplay
//
//  Created by Gallen on 2019/9/15.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

var key :Void? = nil
extension UIScrollView{
    var contentWidth : CGFloat?{
        get{
            return objc_getAssociatedObject(self, &key) as? CGFloat
        }
        set{
            objc_setAssociatedObject(self, &key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            var size = self.contentSize
            size.width = newValue ?? 0
            self.contentSize = size
        }
    }
    var contentHeight : CGFloat?{
        get{
            return objc_getAssociatedObject(self, &key) as? CGFloat ?? 0
        }
        set{
            objc_setAssociatedObject(self, &key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            var size = self.contentSize
            size.height = newValue ?? 0
            self.contentSize = size
        }
    }
    var offsetX:CGFloat?{
        get{
            return objc_getAssociatedObject(self, &key) as? CGFloat
        }
        set{
            objc_setAssociatedObject(self, &key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var offsetY:CGFloat?{
        get{
            return objc_getAssociatedObject(self, &key) as? CGFloat
        }
        set{
            objc_setAssociatedObject(self, &key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func setOffsetX(offsetX : CGFloat ,animated:Bool){
        var point = self.contentOffset
        point.x = offsetX
        self.setContentOffset(point, animated: animated)
    }
    
    func setOffsetY(offsetY : CGFloat ,animated:Bool){
        var point = self.contentOffset
        point.y = offsetY
        self.setContentOffset(point, animated: animated)
    }
    /**
     *  滚动到最顶部
     */
    func scrollToTopWidthAnimation(animation:Bool){
        self.setOffsetY(offsetY: 0, animated: animation)
    }
    /**
     *  滚动到最底部
     */
    func scrollToBottomAnimation(animation:Bool){
        let viewHeight = frame.size.height
        if self.contentHeight! > viewHeight {
            let offsetY = self.contentHeight! - viewHeight
            self.setOffsetY(offsetY: offsetY, animated: animation)
        }
    }
    /**
     *  滚动到最左端
     */
    func scrollToLeftAnimation(animation:Bool){
        setOffsetX(offsetX: 0, animated: animation)
    }
    /**
     *  滚动到最右端
     */
    func scrollToRightAnimation(animation:Bool){
        let viewWidth = frame.size.width
        if self.contentWidth! > viewWidth {
            let offsetX = self.contentWidth! - viewWidth
            self.setOffsetX(offsetX: offsetX, animated: animation)
        }
    }
}
