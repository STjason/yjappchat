//
//  Extension.swift
//  gameplay
//
//  Created by Gallen on 2019/9/25.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

extension UITextView{
    
    func textRangeFromNSRange(range:NSRange) -> UITextRange?
    {
        let beginning = self.beginningOfDocument
        guard let start = self.position(from: beginning, offset: range.location) else {return nil}
        guard let end = self.position(from: start, offset: range.length) else { return nil}
        return  self.textRange(from: start, to: end)
    }
}

