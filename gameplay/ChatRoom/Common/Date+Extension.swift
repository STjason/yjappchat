//
//  Date+Extension.swift
//  gameplay
//
//  Created by Gallen on 2019/9/27.
//  Copyright © 2019年 yibo. All rights reserved.
//

import Foundation
extension Date{
    
    static func creatDateString(creatAtString : String) -> String {
        // 设置日期格式对象
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        fmt.locale = Locale(identifier: "zh_hans_CN")
    
        // 将Date创时间转成string对象
        let creatAtDate = fmt.date(from: creatAtString) ?? Date()
        
        // 创建当前时间
        let nowDate = Date()
        
        // 计算当前时间和创建时间的时间差
        let interval = Int(nowDate.timeIntervalSince(creatAtDate))
        
        if isToday(date: creatAtDate) {
            //  1天内展示xx小时前
//            if interval < 60 * 60 * 24 {
                fmt.dateFormat = "今天 HH:mm"
                let timeStr = fmt.string(from: creatAtDate)
                return timeStr
//            }
        }
//
        
        //  创建日历对象
        let calender = Calendar.current
        
        
        // 处理昨天数据 昨天 12：33
        if calender.isDateInYesterday(creatAtDate) {
            fmt.dateFormat = "昨天 HH:mm"
            let timeStr = fmt.string(from: creatAtDate)
            return timeStr
        }else{
            if isSameWeek(date: creatAtDate){
                fmt.dateFormat = "MM-dd EEEE HH:mm"
                let timeStr = fmt.string(from: creatAtDate)
                return timeStr
            }else{
                let timeStr = fmt.string(from: creatAtDate)
                return timeStr
            }
        }
        
        
        // 处理一年内数据 02-22 12：33
        let cmps = calender.component(.year, from: creatAtDate)
        
        if cmps < 1 {
            if isSameWeek(date: creatAtDate){
                fmt.dateFormat = "MM-dd HH:mm EEEE"
            }else{
                fmt.dateFormat = "MM-dd HH:mm"
            }
           
            let timeStr = fmt.string(from: creatAtDate)
            return timeStr
        }
        
        // 处理超过一年数据 2019-10-03  12:22
        if cmps >= 1 {
            fmt.dateFormat = "yyyy-MM-dd HH:mm"
            let timeStr = fmt.string(from: creatAtDate)
            return timeStr
        }
        
        
        return "1970-01-01 00：00"
    }
    
    
    /**
     *  是否为同一天
     */
    
    static func isSameDay(firstDateString:String,secondDateString:String) -> Bool{
        
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    let calendar = Calendar.current
        if let firstDate = fmt.date(from: firstDateString), let secondDate = fmt.date(from: secondDateString) {
            return Calendar.current.isDate(firstDate, inSameDayAs: secondDate)
        }else {
            return true
        }
    }
    /**是否在同一周*/
    static func isSameWeek(date:Date) -> Bool{
//        let fmt = DateFormatter()
//        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    let calendar = Calendar.current
//        let firstDate = fmt.date(from: dateString)
        let calan = Calendar.current
        let now = Date()
        if  calan.compare(date, to: now, toGranularity: .yearForWeekOfYear) == .orderedSame{
            return true
        }
        return false
    }
    //是否是今天
    static func isToday(date:Date) -> Bool{
        let calendar = Calendar.current
        let unit: Set<Calendar.Component> = [.day,.month,.year]
        let nowComps = calendar.dateComponents(unit, from: Date())
        let selfCmps = calendar.dateComponents(unit, from: date)
        
        return (selfCmps.year == nowComps.year) &&
            (selfCmps.month == nowComps.month) &&
            (selfCmps.day == nowComps.day)
        
    }
    
    
    
}


