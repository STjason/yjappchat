//
//  CRNetBase.swift
//  Chatroom
//
//  Created by admin on 2019/7/5.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import Alamofire

class CRNetBase: NSObject {
    
    static let instance = CRNetBase()
    
    static func shareInstance() -> CRNetBase {
        return instance
    }

    typealias responseClosure = (_ success:Bool,_ response:String) -> () //网络请求结束闭包
    /**文件进度条闭包*/
    typealias fileProgressClosure = (_ progress:Double) ->()
    
    ///传入URLRequest的网络请求
    func netWithRequest(urlString:String, method:HTTPMethod, parameters:Parameters,useOriginalUrl:Bool = false, handler:@escaping responseClosure) {
        
        defaultHeader["Cookie"] = "SESSION=" + CRDefaults.getToken()
        defaultHeader["Content-Type"] = "application/json"
        
        let urlString = CRUrl.shareInstance().getcompleteURLWith(URLString: urlString,useOriginalUrl: useOriginalUrl)
        guard let url = URL.init(string: urlString) else {
            print("url错误")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = defaultHeader
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("参数错误")
            return
        }
        
        guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            return
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) else {
            return
        }
        
        request.httpBody = data
        
        Alamofire.request(request).responseString { (data) in
            switch data.result {
            case .success(let json):
                handler(true, json)
                break
            case .failure(let error):
                print("error: \(error)")
                handler(false, error.localizedDescription)
                break
            }
        }
    }
    
    ///传入url，参数的网络请求
    func netRequest(urlString:String, method:HTTPMethod, parameters:Parameters, handler:@escaping responseClosure) {
        defaultHeader["Cookie"] = "SESSION=" + CRDefaults.getToken()
        
        let urlString = CRUrl.shareInstance().getcompleteURLWith(URLString: urlString,uniqueForChat: false)
        guard let url = URL.init(string: urlString) else {
            print("url错误: \(urlString)")
            return
        }
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: defaultHeader).responseString {data in
            
            switch data.result {
            case .success(let json):
                handler(true, json)
                break
            case .failure(let error):
                print("==================errorStart")
                print("url: \(url)")
                print("error: \(error)")
                print("==================errorEnd")
                handler(false, error.localizedDescription)
                break
            }
        }
    }
    
    func sendMessageRuqest(urlString:String, method:HTTPMethod, parameters:Parameters, handler:@escaping responseClosure){
        
        defaultHeader["Cookie"] = "SESSION=" + CRDefaults.getToken()
        defaultHeader["Content-Type"] = "application/json"
        
//        let urlString = CRUrl.shareInstance().getcompleteURLWith(URLString: urlString)
        guard let url = URL.init(string: urlString) else {
            print("url错误")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = defaultHeader
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("参数错误")
            return
        }
        
        guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            return
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) else {
            return
        }
        
        request.httpBody = data
        
        Alamofire.request(request).responseString { (data) in
            switch data.result {
            case .success(let json):
                handler(true, json)
                break
            case .failure(let error):
                print("error: \(error)")
                handler(false, error.localizedDescription)
                break
            }
        }
    }
    
    func loadImageRuqest(urlString:String, method:HTTPMethod, parameters:Parameters, handler:@escaping responseClosure){
        
        defaultHeader["Cookie"] = "SESSION=" + CRDefaults.getToken()
        defaultHeader["Content-Type"] = "application/json"
        
        guard let url = URL.init(string: urlString) else {
            print("url错误")
            return
        }
        
        var request = URLRequest(url: url)
        //读取图片默认get
        request.httpMethod = HTTPMethod.get.rawValue
        request.allHTTPHeaderFields = defaultHeader
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("参数错误")
            return
        }
        
        guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            return
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) else {
            return
        }
        
        request.httpBody = data
        
        Alamofire.request(request).responseString { (data) in
            switch data.result {
            case .success(let json):
                handler(true, json)
                break
            case .failure(let error):
                print("error: \(error)")
                handler(false, error.localizedDescription)
                break
            }
        }
    }
    
    func uploadFile(urlString:String,
                    method:HTTPMethod,
                    parameters:Parameters,
                    fileData:Data,
                    filePath:String,
                    handler:@escaping responseClosure,progressCloser:@escaping fileProgressClosure){
        defaultHeader["Cookie"] = "SESSION=" + CRDefaults.getToken()
//        defaultHeader["Content-Type"] = "multipart/form-data"
        guard let url = URL.init(string: urlString) else {
            print("url错误")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = defaultHeader
        let mimeType = parameters["fileType"] as? String ?? ""
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            print("参数错误")
            return
        }

        guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            return
        }

        guard let data = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) else {
            return
        }
        request.httpBody = data
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(fileData, withName: "file", fileName: filePath, mimeType: mimeType)
            
        }, with: request) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    progressCloser(progress.fractionCompleted)
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    let dataString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)! as String
                    
                    handler(true, dataString)
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    //创建队列
    func serialQueueSync(urlString:String,method:HTTPMethod,parameters:Parameters,imageData:Data,imagePath:String,handler:@escaping responseClosure,progressCloser:@escaping fileProgressClosure) {
        
        print("+++start+++")
        let concurrentQueue = DispatchQueue(label: "上传图片队列", qos: .default, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency:.inherit, target: nil)
        
        concurrentQueue.async {
            //执行任务
            self.uploadFile(urlString: urlString, method: method, parameters: parameters, fileData: imageData, filePath: imagePath, handler: handler, progressCloser: progressCloser)
        }
        
        print("+++end+++")
    }
 
 
    ///data -> String
    private func getStringWithData(data:Data) -> String {
        return (String(data: data, encoding: String.Encoding.utf8) as String?)!
    }

}
