//
//  CRSocketManager.swift
//  gameplay
//
//  Created by admin on 2020/1/7.
//  Copyright © 2020年 yibo. All rights reserved.
//  管理socket

//需要处理

import UIKit
import SocketIO
import HandyJSON

class CRSocketManager: NSObject {
//    static let instance = CRSocketManager()
//
//    static func shared() -> CRSocketManager {
//        return instance
//    }
//
//    ///用以限定短时间内不多次重复重连
//    var canReconnect = true
//    ///之前的的房间infoModel
//    var preRoomInfo:CRRoomInfoModel?
//    ///聊天消息对象数组
//    var msgItems = [CRMessageModel]()
//    ///进入房间的ID
//    var chat_roomId:String = ""
//    ///进入房间的userId
//    var chat_userId:String = ""
//    ///用户icon
//    var userIconUrl:String = ""
//    ///房间名称
//    var roomName:String = ""
//    ///记录上一次推送过来的消息
//    var recordMsgUUIDContentItem:CRRedPacketContentItem = CRRedPacketContentItem()
//    ///历史记录的图片数据源
//    var imageUrls:[String] = []
//
//    //MARK: - socket初始化 和事件监听
//    //MARK: socket初始化
//    func setupNewSocket() {
//        socket = nil
//
//        let params = ["key":"#!@$%&^*AEUBSJXK","token":CRCommon.generateMsgID()]
//        guard let paramsString = jsonStringFrom(dictionary: params) else {
//            return
//        }
//
//        let encryParams = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: paramsString)
//
//        let chatURL = CRUrl.shareInstance().CRCHAT_URL
//        guard let url = URL.init(string: chatURL) else {return}
//
//        print("\n初始化SocketIOClient,参数:\(params)\nurl:\(url)")
//
//        let optionParams = SocketIOClientOption.connectParams(["key":encryParams])
//        let optionForceNew = SocketIOClientOption.forceNew(true)
//        let optionLog = SocketIOClientOption.log(false)
//        let optionCompress = SocketIOClientOption.compress
//        let config = SocketIOClientConfiguration.init(arrayLiteral: optionParams,optionForceNew,optionLog,optionCompress)
//
//        socket = SocketIOClient.init(socketURL: url, config: config)
//        socket?.connect()
//        socketHandler()
//    }
//
//    //MARK: socket库的时间监听
//    func socketHandler() {
//        socket?.off("new message")
//        socket?.off(clientEvent: .connect)
//        socket?.off(clientEvent: .disconnect)
//        socket?.off(clientEvent: .error)
//        socket?.off(clientEvent: .reconnect)
//        socket?.off(clientEvent: .reconnectAttempt)
//        socket?.off(clientEvent: .statusChange)
//
//        //需要处理
////        HUDView.hide(animated: true)
////        HUDView = getHUD(text: "聊天室连接中...")
//
//        socket?.on("new message") { (data, ack) in
//            print("socket==========2\nnew message dada:\(data),ack = \(ack)")
//        }
//
//        socket?.on(clientEvent: .connect) {[weak self] (data, ack) in
//
//            print("socket==========3\nsocket.on connect dada:\(data),ack = \(ack)")
//            guard let weakSelf = self else {
//                return
//            }
//
//            //需要处理
////            weakSelf.HUDView.hide(animated: true)
//
//            isSocketConnect = true
//            weakSelf.canReconnect = true
//            weakSelf.observeCustomSocket()
//            weakSelf.sendMessageloginRoom(model: authenticateModel)
//        }
//
//        socket?.on(clientEvent: .disconnect) {[weak self] (data, ack) in
//            print("socket==========4\nsocket.on disconnect dada:\(data),ack = \(ack)")
//            isSocketConnect = false
//            guard let weakSelf = self else {return}
//            //需要处理
////            weakSelf.HUDView.hide(animated: true)
//        }
//
//        socket?.on(clientEvent: .error) {[weak self] (data, ack) in
//
//            isSocketConnect = false
//            print("socket==========5\nsocket.on error dada:\(data),ack = \(ack)")
//            guard let weakSelf = self else {return}
//
//            if !NetwordUtil.isNetworkValid()
//            {
//                //需要处理
////                weakSelf.HUDView.hide(animated: true)
////                showToast(view: weakSelf.view, txt: "网络连接不可用，请检测")
//            }else {
//
//                weakSelf.canReconnect = false
//                DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
//
//                    weakSelf.canReconnect = true
//                    //                    weakSelf.HUDView.hide(animated: true)
//
//                    if !isSocketConnect {
//                        //                        showErrorHUD(errStr: "聊天室即将重连...")
//                        socket?.reconnect()
//                    }
//                })
//            }
//        }
//
//        socket?.on(clientEvent: .reconnect) {(data, ack) in
//            print("socket==========6\nsocket.on reconnect dada:\(data),ack = \(ack)")
//        }
//
//        socket?.on(clientEvent: .reconnectAttempt) {[weak self] (data, ack) in
//            print("socket==========7\nsocket.on reconnectAttempt dada:\(data),ack = \(ack)")
//
//            //需要处理
////            guard let weakSelf = self else {return}
////            weakSelf.HUDView.hide(animated: true)
////            weakSelf.HUDView = weakSelf.getHUD(text: "聊天室连接中...")
//        }
//
//        socket?.on(clientEvent: .statusChange) {[weak self] (data, ack) in
//            print("socket==========8\nsocket.on statusChange dada:\(data),ack = \(ack)")
//            guard let _ = self else {
//                return
//            }
//        }
//    }
//
//    //MARK: 监听自定义的socket事件
//    func observeCustomSocket() {
//
//        socket?.off(CR_PUSH_R)
//        socket?.off(CR_LOGIN_R)
//        socket?.off(CR_USER_R)
//        socket?.off(CR_USER_JOIN_ROOM_R)
//        socket?.off(CR_USER_JOIN_GROUP_R)
//        socket?.off(CR_REONNNECT_R)
//
//        socket?.on(CR_PUSH_R) {[weak self] (data, ack) in //推送
//
//            guard let weakSelf = self else {return}
//            if let messages = weakSelf.formatMsgModelsWithStrings(datas: data) {
//                for model in messages{
//                    weakSelf.didReceivedMessage(message: model)
//                }
//            }
//        }
//
//        socket?.on(CR_USER_JOIN_GROUP_R) {[weak self] (data,ack) in //加入组
//            guard let weakSelf = self else {return}
//
//            weakSelf.receiveUserMsg(msgType:CR_USER_JOIN_GROUP_R, datas: data, handler: { (success, msg, model,type,_) in
//                print("")
//            })
//        }
//
//        socket?.on(CR_REONNNECT_R) {[weak self] (data,ack) in //重连
//            guard let weakSelf = self else {return}
//
//        }
//
//        //MARK:发消息回调
//        socket?.on(CR_USER_R) { [weak self] (data,ack) in //发消息回调
//            guard let weakSelf = self else {return}
//            weakSelf.handleReceiveUserMsgWith(data: data)
//        }
//
//        socket?.on(CR_LOGIN_R) {[weak self] (data,ack) in //登录
//            guard let weakSelf = self else {return}
//
//            weakSelf.receiveUserMsg(msgType:CR_LOGIN_R, datas: data, handler: { (success, msg, model,type,_) in
//                if type == CRUrl.shareInstance().LOGIN_AUTHORITY {
//                    if !success {
//                        //需要处理
////                        weakSelf.kickOutRoom()
//                        return
//                    }
//
//                    if let m = model as? CRAuthMsgModel {
//                        //需要处理
////                        weakSelf.handleLogin(model: m)
//                    }
//                }
//            })
//        }
//
//        socket?.on(CR_USER_JOIN_ROOM_R) {[weak self] (data,ack) in //换房间
//            guard let weakSelf = self else {return}
//
//            weakSelf.receiveUserMsg(msgType:CR_USER_JOIN_ROOM_R, datas: data, handler: { (success, msg, model,type,_)   in
//                if !success {
//                    //需要处理
////                    showToast(view: weakSelf.view, txt: msg)
//                    return
//                }
//
//                guard let roomInfo = weakSelf.preRoomInfo else {
//                    //需要处理
////                    showToast(view: weakSelf.view, txt: "切换房间失败，请重试")
//                    return
//                }
//
//                //需要处理
////                weakSelf.handleInsideRoom(roomInfo: roomInfo)
//            })
//        }
//    }
//
//    //MARK: - 发送消息
//    //MARK: socket方式登录
//    func sendMessageloginRoom(model:CRAuthenticateModel?) {
//
//        guard let modelWraper = model else {
//            //需要处理
////            showToast(view: self.view, txt: "授权失败")
//            return
//        }
//
//        var p = [String:Any]()
//        let token =  "clusterId=" + modelWraper.clusterId + "&encrypted=" + modelWraper.encrypted + "&sign=" + modelWraper.sign
//        p["token"] = token
//        p["code"] = CRUrl.shareInstance().LOGIN_AUTHORITY
//        p["source"] = "app"
//
//        sendSysMessage(msgType: CR_LOGIN_S, code:CRUrl.shareInstance().LOGIN_AUTHORITY,parameters: p)
//    }
//
//
//    //MARK: - 工具方法
//    //从json字符串格式化 [CRMessageModel]
//    func formatMsgModelsWithStrings(datas:[Any]) -> [CRMessage]? {
//        if let stringArray = datas as? [String] {
//            var models = [CRMessage]()
//            let isFirstLoadMessages = self.msgItems.count == 0
//            for (_,jsonString) in stringArray.enumerated(){
//                let deJsonString = Encryption.Decode_AES_ECB(key: aesKey, strToDecode: jsonString)
//
//                //                print("deJsonString = \(deJsonString)")
//
//                guard let deJsonUnWrap = deJsonString,let wraper = CRMessageWraper.deserialize(from: deJsonUnWrap) else {
//                    return nil
//                }
//
//                if let model = CRMessage.deserialize(from: wraper.nativeMsg) {
//                    //用户ID
//                    let userId = CRDefaults.getUserID()
//
//                    if model.userId == userId && model.msgType == "16" { //踢出
//                        //需要处理
////                        kickOutRoom()
//                    }else if model.userId != userId || isFirstLoadMessages {
//                        models.append(model)
//                    }
//                }
//            }
//
//            return models
//        }
//        return nil
//    }
//
//    //Base64转码为 UTF8 String
//    func base64ToNormalString(base64Msg:String) -> String {
//        guard let baseData = Data(base64Encoded: base64Msg, options: Data.Base64DecodingOptions(rawValue: NSData.Base64DecodingOptions.RawValue(0))) else {
//            return ""
//        }
//
//        return String.init(data:baseData, encoding: .utf8) ?? ""
//    }
//
//    func newMsgModel(msgModel:CRMessageModel) -> CRMessageModel {
//        let model = CRMessageModel()
//        model.source = msgModel.source + ""
//        model.code = msgModel.code + ""
//        model.stationId = msgModel.stationId + ""
//        model.userId = msgModel.userId + ""
//        model.roomId = msgModel.roomId + ""
//        return model
//    }
//
//    ///将加密字符串数组，格式化为解码后的字符串数组 CR_LOGIN_R
//    func formatDecodedStringArray(msgType:String = "",datas:[Any]) -> (jsonArray:[String],type:String)? {
//        if let encodeStrings = datas as? [String] {
//            var deStringArray = [String]()
//            var code = ""
//            for jsonString in encodeStrings {
//                var key =  newAESKey
//                if [CR_LOGIN_R,CR_USER_JOIN_ROOM_R].contains(msgType) {
//                    key = aesKey
//                }
//
//                var deJsonString:String?
//                if [CR_USER_JOIN_GROUP_R].contains(msgType) {
//                    deJsonString = Encryption.Decode_AES_ECB(key: aesKey, iv:aesIV , strToDecode: jsonString)
//                }else {
//                    deJsonString = Encryption.Decode_AES_ECB(key: key,strToDecode: jsonString)
//                }
//
//                let model = CRSysSocketBaseModel.deserialize(from: deJsonString)
//                code = model?.code ?? ""
//
//                deStringArray.append(deJsonString ?? "")
//            }
//
//            return (deStringArray,code)
//        }
//
//        return (nil)
//    }
//
//    ///根据msgUUID从 消息数组中查找对应 CRMessage
//    private func getMsgFromMessageItemsWith(msgUUID:String?) -> CRMessage? {
//
//        //需要处理
////        for i in (0..<contentView.messageArea.msgItems.count).reversed() {
////            let item = contentView.messageArea.msgItems[i]
////            if item.msgUUID == msgUUID {
////                return item
////            }
////        }
//        return nil
//    }
//
//    ///根据payid获取对应的红包消息
//    private func getRedpacketWith(payId:String) -> CRRedPacketMessage? {
//        //需要处理
////        for i in (0..<contentView.messageArea.msgItems.count).reversed() {
////            if let item = contentView.messageArea.msgItems[i] as? CRRedPacketMessage {
////                if item.payId == payId {
////                    return item
////                }
////            }
////        }
//        return nil
//    }
//
//
//    //MARK: - 消息的数据回调处理 && 数据统一处理
//    //MARK: 接收socket 推过来的消息 入口
//    public func didReceivedMessage(message:CRMessage) {
//        if message.sentTime.isEmpty {
//            let currentTime = String().getCurrentTimeString()
//
//            message.sentTime = currentTime.subString(start: 10)
//        }
//
//        let chatRoomConfig = getChatRoomSystemConfigFromJson()
//
//        //这里需判断下用户ID
//        //不是自己发送的
//        if message.msgType == "\(CRMessageType.WinnerInfo.rawValue)" {
//            //中奖公告
//            guard let config = chatRoomConfig?.source else {
//                //需要处理
////                self.receiveWinnersInfo(message: message)
//                return
//            }
//            if config.switch_winning_list_show == "1"{
//                //需要处理
////                self.receiveWinnersInfo(message: message)
//            }
//
//        }else if message.msgType == "\(CRMessageType.InsideRoom.rawValue)" {
//            //进入房间通知
//            if CRDefaults.getChatroomEnterRoomNoti(){
//                //需要处理
////                self.receiveInsideRoom(message: message)
//            }
//
//        }else if message.msgType == "\(CRMessageType.Band_speak_OrNot.rawValue)" {
//            if message.userId == CRDefaults.getUserID(){
//                //用户ID 是自己就处理
//                //后台对用户禁言
//                let speakingItem =  CRSpeakingCloseItem.deserialize(from: message.msgStr)
//                if speakingItem?.speakingClose == "0"{
//                    //针对在线用户的禁言解禁
//                    //1.解禁言的时候要判断之前房间的状态
//
//                    //需要处理
////                    bannedStatus(prohibitInput: false, promptTest: "")
//                }else if speakingItem?.speakingClose == "1"{
//                    //需要处理
////                    //对在线用户禁言
////                    bannedStatus(prohibitInput: true, promptTest: "禁言中 请联系管理员")
//                }
//            }
//
//        }else{
//            if message.userId != CRDefaults.getUserID() {
//
//                //非登录状态不接收聊天室通知
//                if !YiboPreference.getLoginStatus(){
//                    return
//                }
//
//                if let msgResult = message.msgResult,let userEntity = msgResult.userEntity {
//                    message.nickName = userEntity.nickName
//                    message.account = userEntity.account
//                    message.nativeAccount = userEntity.nativeAccount
//                }
//
//                let localNotification = UILocalNotification()
//                localNotification.alertBody = "您有一条通知，请注意查看"
//                if CRDefaults.getChatroomRecerveMessageVoicei(){
//                    localNotification.soundName = "notify_msg.caf"
//                }else{
//                    localNotification.soundName = ""
//                }
//
//                localNotification.hasAction = true
//                localNotification.alertAction = "解锁查看"
//                //发送消息通知
//
//                var infoDic:[String:Any] = [:]
//                infoDic["code"] = KMESSAGENOTI
//                infoDic["roomId"] = chat_roomId
//                infoDic["userId"] = CRDefaults.getUserID()
//                infoDic["stationId"] = message.stationId
//                //用户图像
//                infoDic["headIcon"] = self.userIconUrl
//                //房间名
//                infoDic["roomName"] = ""
//                //房间图标
//                infoDic["roomIcon"] = ""
//                //            let curCtrl = self as? AnyClass.
//                //            infoDic["ctrl"] = NSStringFromClass(self)
//
//                let className = NSStringFromClass(self.classForCoder)
//                infoDic["ctrl"] = className
//
//                localNotification.userInfo = infoDic;
//                //文本消息
//                if message.msgType == "\(CRMessageType.Text.rawValue)" {
//                    let textMessageItem = CRTextMessageItem()
//                    textMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    textMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    textMessageItem.ownerType = .Others
//                    textMessageItem.messageType = .Text
//                    let decodeString = message.msgStr
//
//                    textMessageItem.msgResult?.userEntity = (message.msgResult?.userEntity ?? nil)
//                    textMessageItem.msgResult?.userEntity?.userDetailEntity = (message.msgResult?.userEntity?.userDetailEntity ?? nil)
//                    //用户图像
//                    textMessageItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//                    textMessageItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    textMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//                    textMessageItem.nickName = message.nickName
//                    textMessageItem.account = message.account
//                    textMessageItem.nativeAccount = message.nativeAccount
//
//                    if let messageEntityItem = CRMessageEntity.deserialize(from: decodeString){
//                        textMessageItem.text = messageEntityItem.record
//                        textMessageItem.msgUUID = messageEntityItem.msgUUID
//                        //标题 用户称昵
//                        localNotification.alertTitle = "房间: " + roomName
//                        localNotification.alertBody = textMessageItem.nickName + ": " +  messageEntityItem.record
//                        //                    localNotification.alertLaunchImage
//                    }
//
//                    textMessageItem.sentTime  = message.sentTime
//
//                    if recordMsgUUIDContentItem.msgUUID == textMessageItem.msgUUID {
//                        return
//                    }
//
//                    recordMsgUUIDContentItem.msgUUID = textMessageItem.msgUUID
//
//                    addToShowMessage(message:textMessageItem)
//
//                }else if message.msgType == "\(CRMessageType.Image.rawValue)"{
//                    //图片消息
//                    //调用读图片的接口
//                    let imageMessageItem = CRImageMessage()
//                    imageMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    imageMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    imageMessageItem.ownerType = .Others
//                    imageMessageItem.messageType = .Image
//                    let decodeString = message.msgStr
//                    imageMessageItem.nickName = message.nickName
//                    imageMessageItem.account = message.account
//                    imageMessageItem.nativeAccount = message.nativeAccount
//
//                    imageMessageItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//
//                    guard let imageResource = CRImageResource.deserialize(from: decodeString) else {
//                        return
//                    }
//
//
//                    let imageUrlCode = imageResource.fileString.isEmpty ? imageResource.record : imageResource.fileString
//                    //                    imageMessageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imageUrlCode
//
//                    imageMessageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imageUrlCode
//
//                    imageMessageItem.msgUUID = imageResource.msgUUID
//                    //把图片添加到图片浏览器中
//                    imageUrls.append(imageMessageItem.imageUrl!)
//
//                    let imageV = UIImageView()
//                    imageV.kf.setImage(with: URL(string: imageMessageItem.imageUrl!), placeholder: nil, options: nil, progressBlock: { (curProgress, totalProgress) in
//
//                    }) { (image, error, cache, url) in
//                        //
//                        imageMessageItem.imageSize = (image?.size) ?? CGSize.zero
//                        print("imageSize = \(imageMessageItem.imageSize)")
//                        imageMessageItem.kMessageFrame = nil
//                        imageMessageItem.sendState = .Success
//                    }
//
//                    imageMessageItem.sentTime  = message.sentTime
//                    imageMessageItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    imageMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//                    if self.recordMsgUUIDContentItem.msgUUID == imageMessageItem.msgUUID {
//                        return
//                    }
//                    self.addToShowMessage(message:imageMessageItem)
//                    self.recordMsgUUIDContentItem.msgUUID = imageMessageItem.msgUUID
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody = imageMessageItem.nickName + ": " + "图片消息"
//                }else if message.msgType == "\(CRMessageType.RedPacket.rawValue)" {
//                    //接收消息类型是红包
//                    let redPacketItem = CRRedPacketMessage()
//                    redPacketItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    redPacketItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    redPacketItem.ownerType = .Others
//                    redPacketItem.messageType = .RedPacket
//
//                    redPacketItem.nickName = message.nickName
//                    redPacketItem.account = message.account
//                    redPacketItem.nativeAccount = message.nativeAccount
//                    redPacketItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//
//                    let decodeString = message.msgStr
//                    if let redPacketContentItem = CRRedPacketContentItem.deserialize(from: decodeString) {
//                        redPacketItem.redPacketContentItem = redPacketContentItem
//                        redPacketItem.msgUUID = redPacketContentItem.msgUUID
//                    }
//                    redPacketItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    redPacketItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//                    redPacketItem.sentTime = message.sentTime
//
//                    if self.recordMsgUUIDContentItem.msgUUID == redPacketItem.msgUUID {
//                        return
//                    }
//                    addToShowMessage(message: redPacketItem)
//                    //标题 用户称昵
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody = redPacketItem.nickName + ": " + "红包消息"
//
//                    self.recordMsgUUIDContentItem.msgUUID = redPacketItem.msgUUID
//                }else if message.msgType == "\(CRMessageType.ShareBet.rawValue)" {
//                    let shareBetMessageItem = CRShareBetMessageItem()
//                    shareBetMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    shareBetMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    shareBetMessageItem.ownerType = .Others
//                    shareBetMessageItem.messageType = .ShareBet
//                    shareBetMessageItem.nickName = message.nickName
//                    shareBetMessageItem.account = message.account
//                    shareBetMessageItem.nativeAccount = message.nativeAccount
//                    shareBetMessageItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//
//                    let decodeString = message.msgStr + ""
//
//                    guard let betModel = CRShareBetModel.deserialize(from: decodeString), let betInfos = betModel.betInfos else {
//                        return
//                    }
//
//                    var height:CGFloat = 0
//                    if betInfos.count == 1 {
//                        height = CRShareBetAllView.singleH
//                    }else if betInfos.count >= 2 {
//                        height = CGFloat(2) *  CRShareBetAllView.singleH + CRShareBetAllView.followAllH
//                    }
//
//                    let messageFrame  = CRMessageFrame()
//                    messageFrame.contentSize = CGSize.init(width: 300, height: height)
//                    messageFrame.height = Float(height) + Float(20 + 20)
//                    shareBetMessageItem.messageFrame = messageFrame
//                    shareBetMessageItem.shareBetModel = betModel
//                    shareBetMessageItem.winRate = betModel.winOrLost?.winPer ?? 0
//
//                    shareBetMessageItem.winRate =  shareBetMessageItem.shareBetModel?.winOrLost?.winPer ?? 0
//                    shareBetMessageItem.msgUUID = shareBetMessageItem.shareBetModel?.msgUUID ?? ""
//                    shareBetMessageItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    shareBetMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//                    if self.recordMsgUUIDContentItem.msgUUID == shareBetMessageItem.msgUUID {
//                        return
//                    }
//                    addToShowMessage(message:shareBetMessageItem)
//                    self.recordMsgUUIDContentItem.msgUUID = shareBetMessageItem.msgUUID
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody =  shareBetMessageItem.nickName + ": " +  "注单消息"
//                }else if message.msgType == "\(CRMessageType.PlanMsg.rawValue)" {
//                    let planMsgItem = CRPlanMsgItem()
//                    planMsgItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    planMsgItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    planMsgItem.ownerType = .Others
//                    planMsgItem.messageType = .PlanMsg
//                    planMsgItem.msgResult?.userEntity?.nickName = "计划消息"
//                    planMsgItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//                    planMsgItem.planItem = CRPlanMessage.deserialize(from: message.msgStr)
//                    planMsgItem.sentTime = message.time
//
//                    planMsgItem.msgUUID = planMsgItem.planItem?.msgUUID ?? ""
//                    planMsgItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    planMsgItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//
//                    if self.recordMsgUUIDContentItem.msgUUID == planMsgItem.msgUUID {
//                        return
//                    }
//
//                    addToShowMessage(message: planMsgItem)
//                    self.recordMsgUUIDContentItem.msgUUID = planMsgItem.msgUUID
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody =  planMsgItem.nickName + ": " +  "计划消息"
//                }else if message.msgType == "\(CRMessageType.ShareGainsLosses.rawValue)"{
//                    //今日分享
//                    //                    var shareGainsLossesItem = CRShareGainsLossesItem()
//                    guard let msgStrItem =  CRMessage.deserialize(from: message.msgStr) else{return}
//                    guard let shareGainsLossesItem =  CRShareGainsLossesItem.deserialize(from: msgStrItem.record) else{return}
//                    shareGainsLossesItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    shareGainsLossesItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    shareGainsLossesItem.ownerType = .Others
//                    shareGainsLossesItem.messageType = .ShareGainsLosses
//                    shareGainsLossesItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//                    shareGainsLossesItem.sentTime = message.sentTime
//                    shareGainsLossesItem.msgUUID = msgStrItem.msgUUID
//                    shareGainsLossesItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    shareGainsLossesItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//                    shareGainsLossesItem.nickName = message.nickName
//                    shareGainsLossesItem.account = message.account
//                    shareGainsLossesItem.nativeAccount = message.nativeAccount
//                    if self.recordMsgUUIDContentItem.msgUUID == shareGainsLossesItem.msgUUID {
//                        return
//                    }
//                    addToShowMessage(message: shareGainsLossesItem)
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody =  shareGainsLossesItem.nickName + ": " +  "今日盈亏消息"
//                }
//                else if message.msgType == "\(CRMessageType.robotMsg.rawValue)" {
//                    let robotMsgItem = CRRobotMsgItem()
//                    robotMsgItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    robotMsgItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    robotMsgItem.ownerType = .Others
//                    robotMsgItem.messageType = .robotMsg
//                    if message.msgResult?.userEntity?.nickName != ""{
//                        robotMsgItem.nickName = (message.msgResult?.userEntity?.nickName)!
//                    }else{
//                        robotMsgItem.nickName = (message.msgResult?.userEntity?.account) ?? ""
//                    }
//
//                    robotMsgItem.robotMessage = CRRobotMessage.deserialize(from: message.msgStr)
//
//                    robotMsgItem.sentTime = message.sentTime
//                    robotMsgItem.time = message.time
//                    robotMsgItem.avatar = robotMsgItem.robotMessage?.fromUser?.robotImage
//
//                    robotMsgItem.msgUUID = robotMsgItem.robotMessage?.msgUUID ?? ""
//                    robotMsgItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    robotMsgItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//
//                    if self.recordMsgUUIDContentItem.msgUUID == robotMsgItem.msgUUID {
//                        return
//                    }
//
//                    addToShowMessage(message: robotMsgItem)
//                    self.recordMsgUUIDContentItem.msgUUID = robotMsgItem.msgUUID
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody =  robotMsgItem.nickName + ": " +  "机器人消息"
//                }
//                else if message.msgType == "\(CRMessageType.Voice.rawValue)" {
//                    //音频消息
//                    let decodeString = message.msgStr
//                    let voiceMsgItem = CRVoiceMessageItem()
//                    voiceMsgItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
//                    voiceMsgItem.userType = message.msgResult?.userEntity?.userType ?? "1"
//                    guard let msgItem = CRAudioMsgStrItem.deserialize(from: decodeString) else{return}
//                    guard let audioMsgItem = msgItem.audioMsg else{return}
//                    voiceMsgItem.durationTime = String.init(format:"%ld",audioMsgItem.duration)
//                    voiceMsgItem.ownerType = .Others
//                    voiceMsgItem.messageType = .Voice
//                    voiceMsgItem.sentTime = message.time
//                    //                    voiceMsgItem.path = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE + "?fileId=" + "\(audioMsgItem.record)" + "&contentType=audio/amr"
//
//                    //需要处理
//                    //下载该音频文件
////                    downloadFileFromServer(fileCode: audioMsgItem.record, voiceMsgItem: voiceMsgItem)
//
//                    //音频消息详情
//                    voiceMsgItem.nickName = message.nickName
//                    voiceMsgItem.account = message.account
//                    voiceMsgItem.nativeAccount = message.nativeAccount
//
//                    voiceMsgItem.sentTime = message.time
//                    voiceMsgItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
//                    voiceMsgItem.msgUUID = message.msgUUID
//                    voiceMsgItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
//                    voiceMsgItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
//
//                    addToShowMessage(message: voiceMsgItem)
//                    self.recordMsgUUIDContentItem.msgUUID = voiceMsgItem.msgUUID
//                    localNotification.alertTitle = "房间: " + self.roomName
//                    localNotification.alertBody =  voiceMsgItem.nickName + ": " +  "语音消息"
//                }
//
//                //推送通知
//                if  CRDefaults.getChatroomMessageNoti(){
//                    UIApplication.shared.presentLocalNotificationNow(localNotification)
//                }
//
//            }
//        }
//    }
//
//    //MARK: 添加消息到数据源 并显示
//    func addToShowMessage(message:CRMessage){
//        //需要处理
////        contentView.messageArea.addMessage(message: message)
////        DispatchQueue.main.async{
////            if message.ownerType == .TypeSelf {
////                self.scrollTableViewToBottom(animated: true)
////            }
////        }
//    }
//
//    //MARK: socket发送消息,socket请求调用
//    func sendSysMessage(msgType:String,code:String = "",parameters:[String:Any]) {
//        var parameters = parameters
//        if isSocketConnect {
//
//            var funcs = ""
//            if msgType == CR_USER_JOIN_ROOM_S {
//                funcs = "切换房间"
//            }else {
//                switch code {
//                    //                case CRUrl.shareInstance().CODE_GAME_LIST:
//                    //                    funcs = "获取彩票计划列表数据"
//                //                    break
//                case CRUrl.shareInstance().LOGIN_AUTHORITY:
//                    funcs = "聊天室登录"
//                    break
//                case CRUrl.shareInstance().CHAT_ROOM_LIST:
//                    funcs = "获取房间列表"
//                    break
//                case CRUrl.shareInstance().JOIN_CHAT_ROOM:
//                    funcs = "进入房间"
//                    break
//                case CRUrl.shareInstance().CODE_ROOM_CONFIG:
//                    funcs = "拉取房间配置"
//                    break
//                case "\(sendMessageType.redPacket.rawValue)":
//                    funcs = "发送红包"
//                    break
//                case "\(sendMessageType.image.rawValue)":
//                    funcs = "发送图片"
//                    break
//                case "\(sendMessageType.shareOrder.rawValue)":
//                    parameters["multiBet"] = "1"
//                    funcs = "分享注单"
//                    break
//                case "\(sendMessageType.followOrder.rawValue)":
//                    funcs = "跟单"
//                    break
//                case "\(sendMessageType.historyRecord.rawValue)":
//                    funcs = "拉取聊天记录"
//                    break
//                case "\(sendMessageType.pullLotteryTicket.rawValue)":
//                    funcs = "拉取开奖结果"
//                    break
//                case "\(sendMessageType.historyBetShare.rawValue)":
//                    funcs = "拉取投注历史"
//                    break
//                case "\(sendMessageType.userInfo.rawValue)":
//                    funcs = "拉取用户信息"
//                    break
//                case "\(sendMessageType.roomOnLineList.rawValue)":
//                    funcs = "拉取在线用户"
//                    break
//                case "\(sendMessageType.signIn.rawValue)":
//                    funcs = "签到"
//                case "\(sendMessageType.signIn.rawValue)":
//                    funcs = "拉取导师计划"
//                case "\(sendMessageType.privateChatInit.rawValue)":
//
//                    if let type = parameters["type"] as? String {
//                        if type == "1" {
//                            funcs = "初始化私聊"
//                        }else if type == "2" {
//                            funcs = "拉取私聊列表"
//                        }else if type == "join" {
//                            funcs = "切换私聊房间"
//                        }
//                    }
//                case "\(sendMessageType.privateChatSend.rawValue)":
//                    if let type = parameters["type"] as? String {
//                        if type == "2" {
//                            funcs = "拉取私聊历史消息"
//                        }
//                    }
//                default:
//                    break
//                }
//            }
//
//            if !funcs.isEmpty {
//                //需要处理
////                HUDView.label.text = funcs
//            }
//
//            let jsonString = jsonStringFrom(dictionary: parameters)
//            guard let msgJson = jsonString else {return}
//
//            var encryKey = newAESKey
//            if [CR_LOGIN_S,CR_USER_JOIN_ROOM_S].contains(msgType) {
//                encryKey = aesKey
//            }
//
//            if encryKey.isEmpty {
//                return
//            }
//
//            var encryMsg = ""
//
//            if [CR_USER_JOIN_GROUP_S].contains(msgType) {
//                encryMsg = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: msgJson)
//            }else {
//                encryMsg = Encryption.Endcode_AES_ECB(key: encryKey, strToEncode: msgJson)
//            }
//
//            let timeOut:Double = 15 //超时
//
//            socket?.emitWithAck(msgType, encryMsg).timingOut(after: timeOut, callback: {[weak self] (data) in
//                guard let weakSelf = self else {return}
//
//                if code == "\(sendMessageType.image.rawValue)"{
//                    //音频也是这个方法
//                    guard let imgMessageItem = CRImageResource.deserialize(from: jsonString) else{return}
//                    if imgMessageItem.record.isEmpty{return}
//
//                    //                    let imageUrl =  CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imgMessageItem.record
//
//                    let imageUrl =  CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imgMessageItem.record
//
//                    weakSelf.imageUrls.append(imageUrl)
//                }
//
//                if let datas = data as? [String] {
//                    if datas.count > 0 {
//                        if datas[0] == "NO ACK" {
//                            if !funcs.isEmpty {
//                                // 失败状态
//                                let msgUUId = parameters["msgUUID"] as? String
//                                let  originalMsg = weakSelf.getMsgFromMessageItemsWith(msgUUID: msgUUId)
//                                originalMsg?.sendState = .Fail
//
//                                //需要处理
////                                weakSelf.HUDView.hide(animated: true)
////
////                                if !(code == CRUrl.shareInstance().LOGIN_AUTHORITY && isSocketConnect) {
////                                    showToast(view: weakSelf.view, txt: "\(funcs)超时")
////                                }
//                            }
//                        }
//                    }
//                }
//            })
//
//        }else {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                socket?.reconnect()
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                    self.sendSysMessage(msgType: msgType, parameters: parameters)
//                }
//            }
//        }
//    }
//
//    //MARK: 处理历史记录返回的数据
//    private func handleHistoryRecord(historyItems:CRHistoryRecordItem) {
//        let historys = historyItems.source ?? []
//        var firstDateString = ""
//
//        var localMsgs: [CRMessage] = [] //历史记录
//
//        for (index ,messageItem) in historys.enumerated() {
//            if index == 0{
//                //记录第一条消息的日期
//                firstDateString = messageItem.time
//                messageItem.isShowTime = true
//            }else{
//                if Date.isSameDay(firstDateString: firstDateString, secondDateString: messageItem.time){
//                    messageItem.isShowTime = false
//                }else{
//                    firstDateString = messageItem.time
//                    messageItem.isShowTime = true
//                }
//
//            }
//            if messageItem.sender == CRDefaults.getUserID(){
//                messageItem.ownerType = .TypeSelf
//            }else{
//                messageItem.ownerType = .Others
//            }
//            //去除年的显示
//            //            if messageItem.time.length > 5{
//            //                messageItem.time = messageItem.time.subString(start: 10)
//            //            }
//
//            if messageItem.msgType == "\(CRMessageType.Text.hashValue)"{
//                //文本
//                let msgTextItem = CRTextMessageItem()
//                msgTextItem.isPlanUser = messageItem.isPlanUser
//                msgTextItem.userType = messageItem.userType
//                //                msgTextItem.time = messageItem
//                msgTextItem.messageType = .Text
//                msgTextItem.isShowTime = messageItem.isShowTime
//                msgTextItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
//                msgTextItem.text = messageItem.context
//                msgTextItem.avatar = messageItem.avatar
//                msgTextItem.userId = messageItem.sender
//                if messageItem.time.length > 5{
//                    msgTextItem.sentTime = messageItem.time.subString(start: 10)
//                }
//                msgTextItem.levelIcon = messageItem.levelIcon
//                msgTextItem.levelName = messageItem.levelName
//                msgTextItem.time = messageItem.time
//                msgTextItem.nickName = messageItem.nickName
//                msgTextItem.account = messageItem.account
//                msgTextItem.nativeAccount = messageItem.nativeAccount
//
//                localMsgs.append(msgTextItem)
//
//            }else if messageItem.msgType == "\(CRMessageType.Image.hashValue)" {
//                //图片
//                let msgImgItem = CRImageMessage()
//                msgImgItem.isPlanUser = messageItem.isPlanUser
//                msgImgItem.userType = messageItem.userType
//                msgImgItem.messageType = .Image
//                msgImgItem.isShowTime = messageItem.isShowTime
//                msgImgItem.userId = messageItem.sender
//                msgImgItem.time = messageItem.time
//                //发送者和接收者区分
//                msgImgItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
//
//                var imageCode = messageItem.url ?? ""
//                if imageCode.contains("/chatFile/read_file/") {
//                    imageCode = imageCode.replacingOccurrences(of: "/chatFile/read_file/", with: "")
//                }
//
//                //                msgImgItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imageCode
//
//                msgImgItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imageCode
//
//                //                let imageV = UIImageView()
//                //
//                //                imageV.kf.setImage(with: URL(string: msgImgItem.imageUrl!), placeholder: UIImage(named: "image_placeholder"), options: nil, progressBlock: { (curProgress,totalProgress ) in
//                //
//                //                }, completionHandler: { (image, error, cache, url) in
//                //                    msgImgItem.imageSize = image?.size ?? CGSize.zero
//                //                    msgImgItem.kMessageFrame = nil
//                //                    msgImgItem.sendState = .Success
//                //                })
//                msgImgItem.avatar = messageItem.avatar
//                msgImgItem.levelIcon = messageItem.levelIcon
//                msgImgItem.levelName = messageItem.levelName
//
//                //                                msgImgItem.sendState = .progress
//                msgImgItem.nickName = messageItem.nickName
//                msgImgItem.account = messageItem.account
//                msgImgItem.nativeAccount = messageItem.nativeAccount
//                //把历史记录的图片加入图片浏览器数据源
//                imageUrls.append(msgImgItem.imageUrl!)
//                msgImgItem.sentTime = messageItem.time
//                localMsgs.append(msgImgItem)
//            }else if messageItem.msgType == "3"{
//                //红包
//                let redPacketItem = CRRedPacketMessage()
//                redPacketItem.isPlanUser = messageItem.isPlanUser
//                redPacketItem.userType = messageItem.userType
//                redPacketItem.messageType = .RedPacket
//                redPacketItem.sendState = .Success
//                redPacketItem.isShowTime = messageItem.isShowTime
//                redPacketItem.userId = messageItem.sender
//                redPacketItem.time = messageItem.time
//                //发送者和接收者区分
//                redPacketItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
//                redPacketItem.avatar = messageItem.avatar
//                //金额
//                //                                redPacketItem.amount  = messag。eItem.money
//                redPacketItem.remark = messageItem.remark
//                redPacketItem.payId = messageItem.payId
//                redPacketItem.levelIcon = messageItem.levelIcon
//                redPacketItem.levelName = messageItem.levelName
//                //用户昵称
//                redPacketItem.nickName = messageItem.nickName
//                redPacketItem.account = messageItem.account
//                redPacketItem.nativeAccount = messageItem.nativeAccount
//                redPacketItem.sentTime = messageItem.time
//                //                                redPacketItem.payId = messageItem.payId
//                localMsgs.append(redPacketItem)
//
//            }else if ["5","8"].contains(messageItem.msgType) {
//                //分享注单
//                //"\(CRMessageType.ShareBet.hashValue)"
//                let shareMessageItem = CRShareBetMessageItem()
//                shareMessageItem.isPlanUser = messageItem.isPlanUser
//                shareMessageItem.userType = messageItem.userType
//                shareMessageItem.messageType = .ShareBet
//                shareMessageItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
//                shareMessageItem.sentTime = messageItem.time
//                shareMessageItem.avatar = messageItem.avatar
//                shareMessageItem.levelIcon = messageItem.levelIcon
//                shareMessageItem.levelName = messageItem.levelName
//                shareMessageItem.time = messageItem.time
//                shareMessageItem.isShowTime = messageItem.isShowTime
//                shareMessageItem.nickName = messageItem.nickName
//                shareMessageItem.account = messageItem.account
//                shareMessageItem.nativeAccount = messageItem.nativeAccount
//
//                var jsonString = ""
//                var shareBetModel = CRShareBetModel()
//
//                if messageItem.msgType == "5" {
//                    jsonString = messageItem.context
//
//                    //暂时没有一单中，有多种注单消息的，因为多注单都被拆分成一个注单发出去的，所以这里可以取数组中最后一个数据（因为数组中最多有一个元素）
//                    if let betInfos = [CRShareBetInfo].deserialize(from: jsonString),let betInfo = betInfos.first {
//                        shareBetModel.betInfo = betInfo
//
//                        let shareOrder = CRShareOrder()
//                        shareOrder.betMoney = betInfo?.betMoney ?? ""
//                        shareOrder.orderId = betInfo?.orderId ?? ""
//                        shareBetModel.orders = [shareOrder]
//                    }
//                }else if messageItem.msgType == "8" {
//                    jsonString = messageItem.nativeContent
//
//                    guard let betModel = CRShareBetModel.deserialize(from: jsonString), let betInfos = betModel.betInfos else {
//                        //过滤错误信息
//                        continue
//                    }
//
//                    var height:CGFloat = 0
//                    if betInfos.count == 1 {
//                        height = CRShareBetAllView.singleH
//                    }else if betInfos.count >= 2 {
//                        height = CGFloat(2) *  CRShareBetAllView.singleH + CRShareBetAllView.followAllH
//                    }
//
//                    let messageFrame  = CRMessageFrame()
//                    messageFrame.contentSize = CGSize.init(width: 300, height: height)
//                    messageFrame.height = Float(height) + Float(20 + 20)
//
//                    shareMessageItem.messageFrame = messageFrame
//                    shareBetModel = betModel
//                }
//
//                shareMessageItem.shareBetModel = shareBetModel
//                //胜率
//                shareMessageItem.winRate = messageItem.winOrLost?.winPer ?? 0
//                localMsgs.append(shareMessageItem)
//
//            }else if messageItem.msgType == "6" { //计划消息
//                let planMsgItem = CRPlanMsgItem()
//                planMsgItem.isPlanUser = messageItem.isPlanUser
//                planMsgItem.userType = messageItem.userType
//                planMsgItem.ownerType = .Others
//                planMsgItem.time = messageItem.time
//                planMsgItem.isShowTime = messageItem.isShowTime
//                planMsgItem.messageType = .PlanMsg
//                planMsgItem.msgResult?.userEntity?.nickName = "计划消息"
//                planMsgItem.avatar = messageItem.msgResult?.userEntity?.userDetailEntity?.avatarCode
//                planMsgItem.text = messageItem.text ?? ""
//                planMsgItem.lotteryName = messageItem.lotteryName
//
//                //                let jsonString = messageItem.context
//
//                let planItem = CRPlanMessage()
//                planItem.text = planMsgItem.text ?? ""
//                planItem.lotteryName = planMsgItem.lotteryName
//                planMsgItem.planItem = planItem
//                //                planMsgItem.planItem = CRPlanMessage.deserialize(from: jsonString)
//
//                planMsgItem.sentTime = messageItem.time
//
//                planMsgItem.msgUUID = planMsgItem.planItem?.msgUUID ?? ""
//                planMsgItem.levelIcon = messageItem.msgResult?.userEntity?.levelIcon ?? ""
//                planMsgItem.levelName = messageItem.msgResult?.userEntity?.levelName ?? ""
//
//                localMsgs.append(planMsgItem)
//            }else if messageItem.msgType == "7"{
//                //机器人消息
//                let robotItem = CRRobotMsgItem()
//                robotItem.isPlanUser = messageItem.isPlanUser
//                robotItem.userType = messageItem.userType
//                //                robotItem.robotMessage?.record = messageItem.context
//                robotItem.text = messageItem.text
//                robotItem.time = messageItem.time
//                robotItem.sentTime = messageItem.time
//                robotItem.isShowTime = messageItem.isShowTime
//                let robotMessageItem = CRRobotMessage()
//                robotMessageItem.fromUser?.robotImage = messageItem.avatar ?? ""
//                robotMessageItem.record = messageItem.context
//
//                robotItem.robotMessage = robotMessageItem
//
//                robotItem.ownerType = .Others
//                robotItem.messageType = .robotMsg
//
//                robotItem.avatar = messageItem.avatar
//                robotItem.nickName = messageItem.nickName
//                robotItem.account = messageItem.account
//                robotItem.nativeAccount = messageItem.nativeAccount
//
//                robotItem.levelIcon = messageItem.levelIcon
//                robotItem.levelName = messageItem.levelName
//                localMsgs.append(robotItem)
//
//            }else if messageItem.msgType == "9"{
//                //今日分享
//                guard let shareTodayGainsLossesItem = CRShareGainsLossesItem.deserialize(from: messageItem.context) else{return}
//                shareTodayGainsLossesItem.isPlanUser = messageItem.isPlanUser
//                shareTodayGainsLossesItem.userType = messageItem.userType
//                shareTodayGainsLossesItem.messageType = .ShareGainsLosses
//                shareTodayGainsLossesItem.isShowTime = messageItem.isShowTime
//                shareTodayGainsLossesItem.userId = messageItem.sender
//                shareTodayGainsLossesItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
//                shareTodayGainsLossesItem.avatar = messageItem.avatar
//                shareTodayGainsLossesItem.levelIcon = messageItem.levelIcon
//                shareTodayGainsLossesItem.levelName = messageItem.levelName
//                shareTodayGainsLossesItem.sentTime = messageItem.time
//                shareTodayGainsLossesItem.time = messageItem.time
//                shareTodayGainsLossesItem.nickName = messageItem.nickName
//                shareTodayGainsLossesItem.account = messageItem.account
//                shareTodayGainsLossesItem.nativeAccount = messageItem.nativeAccount
//                //                  let context = messageItem.context
//
//                localMsgs.append(shareTodayGainsLossesItem)
//            }else if messageItem.msgType == "11"{
//                //音频消息
//                let voiceMessageItem = CRVoiceMessageItem()
//                voiceMessageItem.isPlanUser = messageItem.isPlanUser
//                voiceMessageItem.userType = messageItem.userType
//                voiceMessageItem.messageType = .Voice
//                //在语音历史消息中后台返回的字段remark代表语音时长
//                voiceMessageItem.durationTime = messageItem.remark
//                voiceMessageItem.userId = messageItem.sender
//                voiceMessageItem.time = messageItem.time
//                voiceMessageItem.isShowTime = messageItem.isShowTime
//                //发送者和接收者区分
//                voiceMessageItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
//                voiceMessageItem.avatar = messageItem.avatar
//                voiceMessageItem.levelIcon = messageItem.levelIcon
//                voiceMessageItem.levelName = messageItem.levelName
//                voiceMessageItem.msgUUID = messageItem.msgUUID
//                voiceMessageItem.nickName = messageItem.nickName
//                voiceMessageItem.account = messageItem.account
//                voiceMessageItem.nativeAccount = messageItem.nativeAccount
//                voiceMessageItem.sentTime = messageItem.time
//                let fillCodeText = messageItem.context
//                if fillCodeText.contains("/"){
//                    let array = fillCodeText.components(separatedBy: "/")
//                    voiceMessageItem.fileCode = array.last!
//                }
//
//                localMsgs.append(voiceMessageItem)
//
//                //需要处理
//                //下载历史记录的语音消息
////                downloadFileFromServer(fileCode: voiceMessageItem.fileCode, voiceMsgItem: voiceMessageItem)
//            }
//        }
//
//        //需要处理
////        contentView.messageArea.msgItems.insert(contentsOf: localMsgs, at: 0)
////
////        contentView.messageArea.msgTable.reloadData()
////
////        if historyStart == 0 {
////            scrollTableViewToBottom(animated: false)
////        }
////
////        if historys.count == historyCountPerPage { //当拉取的历史记录条数 == 拉取历史传的每页应有条数的参数时，表示可能还有更多数据
////            historyStart += 1
////        }
//
//
//        //        contentView.messageArea.msgTable.mj_header?.endRefreshing()
//    }
//
//    //MARK: "user_r" 消息回调Response处理
//    private func receiveUserMsg(msgType:String = "",datas:[Any],handler:@escaping (_ success:Bool,_ mzsg:String, _ model:Any?,_ type:String?,_ msgUUID:String?) -> ()) {
//
//        //需要处理
////        HUDView.hide(animated: true)
//
//        if let jsonAndCodeTuples = formatDecodedStringArray(msgType:msgType,datas: datas) {
//
//            let code = jsonAndCodeTuples.type
//            let jsonArray = jsonAndCodeTuples.jsonArray
//            var response = ""
//            if jsonArray.count > 0 {
//                response = jsonArray[0]
//            }
//
//            if msgType == CR_USER_JOIN_GROUP_R { //私聊切换房间
//                sendMsgPrivateChatHistory(type: "2")
//            }else if msgType == CR_USER_JOIN_ROOM_R { //群聊换房间
//
//                class CRChangeRoomModel: HandyJSON {
//                    var success = false
//                    var roomId = ""
//                    var msg = ""
//                    var code = ""
//                    var status = ""
//                    var msgUUID = ""
//                    required init() {}
//                }
//
//                let funcDes = "切换房间"
//                guard let wrapper = CRChangeRoomModel.deserialize(from: response) else {
//                    handler(false,"\(funcDes)失败",nil, "", "")
//                    return
//                }
//
//                if wrapper.success  {
//                    handler(true,"",wrapper.roomId,code,wrapper.msgUUID)
//                }else {
//                    handler(false,wrapper.msg,nil,code,wrapper.msgUUID)
//                }
//
//            }else {
//
//                if code == "\(sendMessageType.privateChatInit.rawValue)" { //私聊初始化相关
//                    //需要处理
////                    self.handlePrivateChatInitalHandleWith(code: code, response: response) { (success, msg, model, code, msgUUid) in
////                        handler(success,msg,model,code, msgUUid)
////                    }
//                }else if code == "\(sendMessageType.privateChatSend.rawValue)" { //私聊发送消息相关
//                    //需要处理
////                    self.handlePrivateChatSendHandleWith(code: code, response: response) { (success, msg, model, code, msgUUid) in
////                        handler(success,msg,model,code, msgUUid)
////                    }
//
//                }else if code == CRUrl.shareInstance().CODE_IMAGECOLLECT { // 新增图片收藏列表数据回调
//                    let funcDes = "收藏图片"
//                    guard let wrapper = CRImageCollectListModel.deserialize(from: response) else {
//                        handler(false,"获取\(funcDes)失败",nil,code,"")
//                        return
//                    }
//                    if wrapper.success == false {
//                        showErrorHUD(errStr: wrapper.msg.isEmpty ? "请求失败" : wrapper.msg)
//                        return
//                    }
//                    if wrapper.source?.option == "2" {
//                        showErrorHUD(errStr: "收藏图片成功")
//                        return
//                    }
//                    self.imageCollectModelHandle?(wrapper)
//                }
//                    /// 新增拉取快速发言列表
//                else if code == CRUrl.shareInstance().CODE_SPEAKQUICKLY_LIST {
//                    let funcDes = "获取快速发言列表数据"
//                    guard let warpper = CRSpeakQuicklyListModel.deserialize(from: response) else {
//                        handler(false,"\(funcDes)失败",nil,code,"")
//                        return
//                    }
//                    if warpper.source.isEmpty {
//                        showErrorHUD(errStr: "暂无快速发言数据")
//                        return
//                    }
//                    showSpeakQuicklyList(list: warpper.source)
//                }
//                    /// 新增彩票计划列表
//                else if code == CRUrl.shareInstance().CODE_GAME_LIST {
//                    let funcDes = "获取彩票计划列表数据"
//                    guard let wrapper = CRLotterySchemeModel.deserialize(from: response) else {
//                        self.lotterySchemeModelHandle?(CRLotterySchemeModel())
//                        handler(false,"\(funcDes)失败",nil,code,"")
//                        return
//                    }
//                    self.lotterySchemeModelHandle?(wrapper)
//                }else if code == CRUrl.shareInstance().CHAT_ROOM_LIST {
//                    let funcDes = "获取聊天室列表"
//                    guard let wrapper = CRRoomListWrapper.deserialize(from: response) else {
//                        handler(false,"\(funcDes)失败",nil,code,"")
//                        return
//                    }
//
//                    if wrapper.success  {
//                        handler(true,"",wrapper.source,code,wrapper.msgUUID)
//                    }else {
//                        handler(false,wrapper.msg,nil,code,wrapper.msgUUID)
//                    }
//                }else if code == CRUrl.shareInstance().LOGIN_AUTHORITY {
//
//                    let funcDes = "聊天室登录"
//                    guard let wrapper = CRAuthWrapper.deserialize(from: response) else {
//                        handler(false,"\(funcDes)失败",nil,code, "")
//                        return
//                    }
//
//                    if wrapper.success  {
//                        handler(true,"",wrapper.source,code, nil)
//                    }else {
//                        handler(false,wrapper.msg,nil,code, nil)
//                    }
//                }else if code == CRUrl.shareInstance().JOIN_CHAT_ROOM {
//                    let funcDes = "进入房间"
//
//                    guard let wrapper = CRRoomInfoWrapper.deserialize(from: response) else {
//                        handler(false,"\(funcDes)失败",nil,code, nil)
//                        return
//                    }
//                    if wrapper.success  {
//                        handler(true,"",wrapper.source,code, nil)
//
//
//                    }else {
//                        handler(false,wrapper.msg,nil,code, nil)
//                    }
//                }else if code == CRUrl.shareInstance().CODE_ROOM_CONFIG {
//                    let funcDes = "拉取房间配置"
//                    guard let wrapper = CRSystemConfigurationItem.deserialize(from: response) else {
//                        handler(false,"\(funcDes)失败",nil,code, nil)
//                        return
//                    }
//                    if wrapper.success  {
//                        handler(true,"",response,code, nil)
//                    }else {
//                        handler(false,wrapper.msg,nil,code, nil)
//                    }
//                }else if code == CRUrl.shareInstance().SEND_MSG_TXT {
//                    guard let jsonObjctItem = CRMessage.deserialize(from: response) else{
//                        handler(false,"发送消息失败",nil,code, "")
//                        return
//                    }
//                    if  jsonObjctItem.success {
//                        handler(true, "发送消息成功", jsonObjctItem, code, jsonObjctItem.msgUUID)
//                    }else{
//                        handler(false,jsonObjctItem.msg,jsonObjctItem,code, jsonObjctItem.msgUUID)
//                    }
//                }else if code == CRUrl.shareInstance().CODE_SHARE_BET_MSG {
//                    //                    let funcDes = "分享注单"
//                    print("\(response)")
//                    guard let shareOrderItem = CRShareBetMessageItem.deserialize(from: response) else{
//                        handler(false,"发送分享注单失败",nil,code, nil)
//                        return
//                    }
//                    if shareOrderItem.success{
//                        handler(true,"发送分享注单成功",shareOrderItem,code, shareOrderItem.msgUUID)
//                    }else{
//                        handler(false,shareOrderItem.msg,shareOrderItem,code, shareOrderItem.msgUUID)
//                    }
//
//                }else if code == "\(sendMessageType.redPacket.rawValue)" {
//
//                    guard let jsonObjctItem = CRRedPacketDataItem.deserialize(from: response) else{
//                        handler(false,"发送红包失败",nil,code, nil)
//                        return
//                    }
//
//                    if jsonObjctItem.success{
//                        handler(true, "发送红包成功", jsonObjctItem,code, jsonObjctItem.msgUUID)
//                    }else{
//                        handler(false,jsonObjctItem.msg,jsonObjctItem,code, jsonObjctItem.msgUUID)
//                    }
//                }else if code == "\(sendMessageType.image.rawValue)" {
//                    guard let msg = CRImageMessage.deserialize(from: response) else {
//                        handler(false,"发送图片失败",nil,code, nil)
//                        return
//                    }
//
//                    if msg.success {
//                        handler(true,"发送图片成功",msg,code, msg.msgUUID)
//                    }else {
//                        handler(false,msg.msg,msg,code,msg.msgUUID)
//                    }
//                }else if code == "\(sendMessageType.systemNotice.rawValue)" { //系统公告
//                    guard let jsonObjcItem = CRSystemNoticeItem.deserialize(from: response) else{
//                        handler(false,"获取系统公告失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success{
//                        handler(true,"获取系统公告成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.contrabandLanguage.rawValue)" { //违禁词
//                    guard let jsonObjcItem = CRProhibitLanguageItem.deserialize(from: response) else{
//                        handler(false,"获取违禁词失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success{
//
//                        self.prohibitLanguageArray = jsonObjcItem.source ?? []
//
//                        handler(true,"获取违禁词成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.followOrder.rawValue)" { //跟单
//                    guard let jsonObjctItem = CRFollowBetWrapper.deserialize(from: response) else{
//                        handler(false,"跟单失败",nil,code, nil)
//                        return
//                    }
//
//                    guard let modelWraper = jsonObjctItem.source else {
//                        handler(false,"\(jsonObjctItem.msg)",nil,code, nil)
//                        return
//                    }
//                    if modelWraper.success {
//                        handler(true,"跟单成功",modelWraper,code, nil)
//                    }else {
//                        handler(false,modelWraper.msg,nil,code, nil)
//                    }
//                }else if code == "\(sendMessageType.pullLotteryTicket.rawValue)" { //拉取开奖结果
//                    guard let jsonObjcItem = CRLotterDataItem.deserialize(from: response) else{
//                        handler(false,"获取彩种失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success{
//                        handler(true,"获取彩种成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.historyRecord.rawValue)" { //获取历史记录
//                    guard let jsonObjcItem = CRHistoryRecordItem.deserialize(from: response) else{
//                        handler(false,"获取聊天记录失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success{
//                        handler(true,"获取聊天记录成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.historyBetShare.rawValue)" { //投注历史
//                    guard let wrapper = CRBetHistoryWraper.deserialize(from: response) else {
//                        handler(false,"获取历史注单消息失败",nil,code, nil)
//                        return
//                    }
//                    if wrapper.success{
//                        handler(true,"获取历史注单消息成功",wrapper.source?.msg,code, nil)
//                    }else{
//                        handler(false,"获取历史注单消息失败",wrapper.source?.msg,code, nil)
//                    }
//                }else if code == "\(sendMessageType.tutorPlan.rawValue)" {
//                    guard let jsonObjcItem = CRHistoryRecordItem.deserialize(from: response) else{
//                        handler(false,"获取导师计划失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success{
//                        handler(true,"获取导师计划成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.userInfo.rawValue)" { //个人信息
//                    guard let jsonObjcItem = CRUserInfoCenterItem.deserialize(from: response) else{
//                        handler(false,"获取个人信息失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success {
//                        handler(true,"获取个人信息成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if ["\(sendMessageType.userIcon.rawValue)","\(sendMessageType.submitUserInfo.rawValue)"].contains(code) { //默认图片列表 //提交修改的用户个人信息
//                    guard let jsonObjcItem = CRSystemIconItem.deserialize(from: response) else{
//                        handler(false,"获取系统默认用户图像失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success{
//                        handler(true,"获取系统默认用户图像成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.receiveRedPacket.rawValue)" {
//                    guard let jsonObjctItem = CRReceiveRedPacketItem.deserialize(from: response) else{
//                        handler(false,"发送抢红包消息失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjctItem.success{
//                        handler(true,"发送抢红包消息成功",jsonObjctItem,code, nil)
//                    }else{
//                        handler(false,jsonObjctItem.msg,jsonObjctItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.receiveDetail.rawValue)" {
//                    guard let jsonObjcItem = CRReceiveDetailItem.deserialize(from: response) else{
//                        handler(false,"获取领取红包详情失败",nil,code, nil)
//                        return
//                    }
//                    if jsonObjcItem.success {
//                        handler(true,"获取领取红包详情成功",jsonObjcItem,code, nil)
//                    }else{
//                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil)
//                    }
//                }else if code == "\(sendMessageType.shareToday.rawValue)" {
//                    print(response)
//                }else if ["\(sendMessageType.roomOnLineList.rawValue)","\(sendMessageType.managerList.rawValue)"].contains(code) {
//
//                    var funcDes = ""
//
//                    if code == "\(sendMessageType.roomOnLineList.rawValue)" {
//                        funcDes = "获取在线用户"
//                    }else if code == "\(sendMessageType.managerList.rawValue)" {
//                        funcDes = "获取管理员列表"
//                    }
//
//                    guard let wraper = CROnLineListWraper.deserialize(from: response) else {
//                        handler(false,"\(funcDes)失败",nil,code, nil)
//                        return
//                    }
//
//                    if wraper.success {
//                        handler(true,"",wraper,code, nil)
//                    }else {
//                        handler(false,wraper.msg,wraper,code, nil)
//                    }
//                }else if code == "\(sendMessageType.currentBalance.rawValue)" {
//                    guard let wraper = CRBalanceWraper.deserialize(from: response) else {
//                        handler(false,"获取余额失败",nil,code, nil)
//                        return
//                    }
//
//                    if wraper.success {
//                        handler(true,"",wraper.source,code, nil)
//                    }else {
//                        handler(false,wraper.msg,wraper,code, nil)
//                    }
//                }else if code == "\(sendMessageType.winPrizeList.rawValue)"{
//                    guard let wraper = CRWinPrizeListItem.deserialize(from: response) else{
//                        handler(false,"获取榜单失败",nil,code, nil)
//                        return
//                    }
//
//                    if wraper.success {
//                        handler(true,"",wraper.source,code, nil)
//                    }else {
//                        handler(false,wraper.msg,wraper,code, nil)
//                    }
//                }else if code == "\(sendMessageType.signIn.rawValue)" {
//
//                    guard let wraper = CRSignInWraper.deserialize(from: response) else{
//                        handler(false,"签到失败",nil,code, nil)
//                        return
//                    }
//
//                    if wraper.success {
//                        guard let model = wraper.source else {
//                            handler(false,"签到失败",nil,code, nil)
//                            return
//                        }
//
//                        if model.success {
//                            handler(true,"",model,code, nil)
//                        }else {
//                            handler(false,model.erroMsg,model,code, nil)
//                        }
//                    }else {
//                        handler(false,wraper.msg,nil,code, nil)
//                    }
//                }
//            }
//        }
//    }
//
//    //MARK: "user_r" 消息回调Model 处理
//    func handleReceiveUserMsgWith(data:[Any]) {
//
//        receiveUserMsg(datas: data, handler: {[weak self] (success, msg, model,type,msgUUID) in
//
//            guard let weakSelf = self else {return}
//
//            var messageItem:CRMessage?
//            if type == sendMessageType.text.rawValue ||
//                type == sendMessageType.image.rawValue ||
//                type == sendMessageType.redPacket.rawValue ||
//                type == sendMessageType.shareOrder.rawValue{
//                messageItem = weakSelf.getMsgFromMessageItemsWith(msgUUID: msgUUID)
//            }
//
//            if !success {
//                messageItem?.sendState = .Fail
//                if type == sendMessageType.text.rawValue {
//                    weakSelf.scrollTableViewToBottom(animated: false)
//                }else if type == sendMessageType.image.rawValue ||
//                    type == sendMessageType.shareOrder.rawValue ||
//                    type == sendMessageType.redPacket.rawValue {
//                    weakSelf.contentView.messageArea.msgTable.reloadData()
//                    weakSelf.scrollTableViewToBottom(animated: false)
//                }else if type == sendMessageType.submitUserInfo.rawValue{
//                    showToast(view: (weakSelf.navigationController?.topViewController?.view)!, txt: msg)
//                }
//                showToast(view: weakSelf.view, txt: msg)
//                return
//            }else{
//
//                if type == sendMessageType.text.rawValue ||
//                    type == sendMessageType.image.rawValue ||
//                    type == sendMessageType.redPacket.rawValue ||
//                    type == sendMessageType.shareOrder.rawValue{
//                    messageItem?.sendState = .Success
//                    weakSelf.playSendMessageSuccessVoice()
//                    //                            weakSelf.contentView.messageArea.msgTable.reloadData()
//                }
//
//            }
//            if type == CRUrl.shareInstance().CHAT_ROOM_LIST { //获取房间列表
//                guard let m = model as? CRRoomListModels else {return}
//
//                weakSelf.handleGetRoomList(rooms: m)
//            }else if type == CRUrl.shareInstance().JOIN_CHAT_ROOM { //进入房间
//                guard let infoModel = model as? CRRoomInfoModel else {return}
//                let winrateItem = infoModel.winOrLost
//                //投注命中率
//                weakSelf.userWinRate = winrateItem?.winPer ?? 0
//                //投注命中率
//                CRDefaults.setBetWinRate(rate: winrateItem?.winPer ?? 0)
//
//                //socket发送换房间消息
//                weakSelf.sendMsgChangeRoom(roomInfo: infoModel)
//            }else if type == CRUrl.shareInstance().CODE_ROOM_CONFIG { //拉取房间配置
//                guard let jsonString = model as? String else {return}
//                //name_new_members_default_photo
//                CRPreference.saveChatRoomConfig(value: jsonString as AnyObject)
//                weakSelf.configWithChatConfig() //获取到开关后进行的配置
//
//                weakSelf.sendMsgGetUserAvatarList(userId: CRDefaults.getUserID())
//            }else if type == CRUrl.shareInstance().SEND_MSG_TXT { //发送文本
//                guard let textMsgItem = model as? CRMessage else{return}
//                guard let originalMsg = weakSelf.getMsgFromMessageItemsWith(msgUUID: textMsgItem.msgUUID) as? CRTextMessageItem else {
//                    return
//                }
//                originalMsg.sendState = .Success
//
//            }else if type == CRUrl.shareInstance().CODE_SHARE_BET_MSG { //分享注单
//
//                print("")
//            }else if type == "\(sendMessageType.image.rawValue)" { //发送图片
//                guard let imgMsgItem = model as? CRImageMessage else{return}
//                guard let originalMsg = weakSelf.getMsgFromMessageItemsWith(msgUUID: imgMsgItem.msgUUID) as? CRImageMessage else{return}
//                originalMsg.sendState = .Success
//
//            }else if type == "\(sendMessageType.systemNotice.rawValue)" { //系统公告 stringByDecodingHTMLEntities
//                if let noticeItem = model as? CRSystemNoticeItem{
//                    let sourceString = noticeItem.source?.toJSONString()?.htmlDecoded()
//                    if let sourceArray = [CRSystemNoticesourceItem].deserialize(from: sourceString){
//                        if sourceArray.count != 0{
//                            for item in sourceArray{
//                                if let noticeSourceItem = item {
//                                    weakSelf.contentView.announceView.configWith(title:noticeSourceItem.body)
//                                }
//                            }
//                        }else{
//                            weakSelf.contentView.announceView.configWith(title: "")
//                        }
//
//                    }else{
//                        weakSelf.contentView.announceView.configWith(title: "")
//                    }
//                }else {
//                    showToast(view: weakSelf.view, txt: "获取系统失败")
//                }
//
//            }else if type == "\(sendMessageType.contrabandLanguage.rawValue)" { //拉取违禁词
//                guard let languageItem = model as? CRProhibitLanguageItem, let languageSourceArray = languageItem.source else {
//                    return
//                }
//
//                weakSelf.prohibitLanguageArray = languageSourceArray
//            }else if type == "\(sendMessageType.followOrder.rawValue)" {
//                showToast(view: weakSelf.view, txt: success ? "跟单成功" : "\(msg)")
//            }else if type == "\(sendMessageType.pullLotteryTicket.rawValue)" { //拉取开奖结果
//                guard let lotterItem = model as? CRLotterDataItem else {
//                    showToast(view: weakSelf.view, txt: "拉取开奖结果失败")
//                    return
//                }
//                DispatchQueue.main.async {
//                    weakSelf.contentView.lotteryBanner.lotterData =  lotterItem.source?.lotteryData
//                }
//
//            }else if type == "\(sendMessageType.historyRecord.rawValue)" { //聊天记录
//
//                if weakSelf.historyStart == 0 { //切换房间的时候，即使拉取聊天记录失败，也要清除上次房间的数据
//                    //清空之前的数据源
//                    weakSelf.contentView.messageArea.msgItems.removeAll()
//                    weakSelf.imageUrls.removeAll()
//                }
//
//                guard let historyItems = model as? CRHistoryRecordItem else {
//                    return
//                }
//
//                weakSelf.handleHistoryRecord(historyItems: historyItems)
//            }else if type == "\(sendMessageType.tutorPlan.rawValue)" { //导师计划
//                guard let historyItems = model as? CRHistoryRecordItem else {
//                    return
//                }
//
//                if let topControlelr = weakSelf.navigationController?.topViewController as? CRMasterPlanController {
//                    topControlelr.updateMsgs(msgs: historyItems)
//                }
//
//            }else if type == "\(sendMessageType.historyBetShare.rawValue)" { //投注历史
//                guard let historyModels = model as? [CRBetHistoryMsgModel] else {
//                    return
//                }
//
//                weakSelf.lotteryHistoryController?.historyModels = historyModels
//            }else if type == "\(sendMessageType.userInfo.rawValue)" {
//                guard let userInfo = model as? CRUserInfoCenterItem,let sourceItem = userInfo.source,let _ = sourceItem.winLost else {
//                    return
//                }
//
//                //保存上次的输赢信息，个人信息结果
//                weakSelf.userInfoCenter = userInfo
//
//                if let topControlelr = weakSelf.navigationController?.topViewController as? CRUserProfileInfoCtrl {
//                    topControlelr.updateUserInfo(userItem: userInfo)
//                }else {
//                    guard let drawerHeader = weakSelf.drawer?.subviewsBGView.header else {return}
//                    drawerHeader.updateUserInfo(userItem: userInfo)
//                }
//
//            }else if type == "\(sendMessageType.userIcon.rawValue)" {
//                guard let userIcon = model as? CRSystemIconItem else {
//                    return
//                }
//
//                weakSelf.userIconLists = (userIcon.source?.items) ?? []
//
//                //设置个人信息页面
//                if let topControlelr = weakSelf.navigationController?.topViewController as? CRUserProfileInfoCtrl {
//                    topControlelr.chooseUserIconHandle(userListsItem: userIcon)
//                }
//
//                //打开抽屉时
//                if let drawer = weakSelf.drawer {
//                    drawer.subviewsBGView.userIconLists = weakSelf.userIconLists + [String]()
//                }
//
//            }else if type == "\(sendMessageType.submitUserInfo.rawValue)" { //提交个人信息修改
//                guard let userIcon = model as? CRSystemIconItem else {
//                    return
//                }
//
//                if let topControlelr = weakSelf.navigationController?.topViewController as? CRUserProfileInfoCtrl {
//                    topControlelr.submitInfoHandle(userListsItem: userIcon)
//                }
//
//            }else if type == "\(sendMessageType.redPacket.rawValue)" { //红包
//
//                guard let redPacgetItem = model as? CRRedPacketDataItem,let redPacketDataItem = redPacgetItem.source else {
//                    return
//                }
//                // 获取当前msgUUID对应的模型
//                guard let originalMsg = weakSelf.getMsgFromMessageItemsWith(msgUUID: redPacketDataItem.msgUUID) as? CRRedPacketMessage else {
//                    return
//                }
//
//                originalMsg.payId = redPacketDataItem.payId
//                originalMsg.remark = redPacketDataItem.remark
//                print("payId = \(originalMsg.payId)")
//                print()
//            }else if type == "\(sendMessageType.receiveRedPacket.rawValue)" {
//                // 处理抢红包 返回结果，更新红包状态
//                guard let m = model as? CRReceiveRedPacketItem else {
//                    return
//                }
//
//                guard let source = m.source else {
//                    return
//                }
//                if success {
//                    if let receiverRedpacketItem = model as? CRReceiveRedPacketItem{
//                        if  receiverRedpacketItem.source?.pickData == nil{
//                            //红包已被领取
//                            guard let message = weakSelf.getRedpacketWith(payId: source.payId) else {
//                                return
//                            }
//                            message.status = (m.source?.status)!
//                            weakSelf.redPacketDetailView.redPacketStatus = message.status
//                            //MARK：房间配置
//                            let chatRoomConfig = getChatRoomSystemConfigFromJson()
//                            if chatRoomConfig?.source?.switch_red_info == "1"{
//                                //显示详情
//                                weakSelf.redPacketDetailView.isShowDetail = true
//                            }else{
//                                weakSelf.redPacketDetailView.isShowDetail = false
//                            }
//                            weakSelf.sendMsgReceiverDataRequest(message: message)
//                        }else{
//                            guard let message = weakSelf.getRedpacketWith(payId: source.pickData?.payId ?? "") else {
//                                return
//                            }
//                            message.status = (m.source?.status)!
//                            weakSelf.redPacketDetailView.redPacketStatus = message.status
//                            //MARK：房间配置
//                            let chatRoomConfig = getChatRoomSystemConfigFromJson()
//                            if chatRoomConfig?.source?.switch_red_info == "1"{
//                                //显示详情
//                                weakSelf.redPacketDetailView.isShowDetail = true
//                            }else{
//                                weakSelf.redPacketDetailView.isShowDetail = false
//                            }
//                            weakSelf.sendMsgReceiverDataRequest(message: message)
//                        }
//                    }
//                    //红包详情
//
//                }else {
//                    if let receiverRedpacketItem = model as? CRReceiveRedPacketItem{
//                        //红包过期
//                        var payId = source.pickData?.payId
//                        if payId?.count == 0{
//                            payId = source.payId
//                        }
//                        guard let message = weakSelf.getRedpacketWith(payId: source.pickData?.payId ?? "") else {
//                            return
//                        }
//                        message.status = receiverRedpacketItem.status
//                        weakSelf.redPacketDetailView.redPacketStatus = message.status
//                        //MARK：房间配置
//                        let chatRoomConfig = getChatRoomSystemConfigFromJson()
//                        if chatRoomConfig?.source?.switch_red_info == "1"{
//                            //显示详情
//                            weakSelf.redPacketDetailView.isShowDetail = true
//                        }else{
//                            weakSelf.redPacketDetailView.isShowDetail = false
//                        }
//                    }
//
//                    showToast(view: weakSelf.view, txt: msg)
//                }
//            }else if type == "\(sendMessageType.receiveDetail.rawValue)" {
//                //领取红包详情
//                guard let receiveDetailItem = model as? CRReceiveDetailItem else {
//                    return
//                }
//                let receiveDetailString = receiveDetailItem.source?.parent?.toJSONString()
//                //领取的红包的当前用户信息
//                let receiveDetailCustomString = receiveDetailItem.source?.c?.toJSONString()
//                let receiveDetailCustomItem = CRReceiveDetailCustom.deserialize(from: receiveDetailCustomString)
//
//                let totalPeopleDetails = receiveDetailItem.source?.other
//                if let redPacketParentItem = CRReceiveDetailParent.deserialize(from: receiveDetailString){
//
//                    weakSelf.redPacketDetailView.receiveRedPacketDetailItem = redPacketParentItem
//                    weakSelf.redPacketDetailView.receiveRedPacketDetailCustomItem = receiveDetailCustomItem
//                    weakSelf.redPacketDetailView.totalPeopleDetails = totalPeopleDetails
//                    //MARK：房间配置
//                    let chatRoomConfig = getChatRoomSystemConfigFromJson()
//                    if chatRoomConfig?.source?.switch_red_info == "1"{
//                        //显示详情
//                        weakSelf.redPacketDetailView.isShowDetail = true
//                    }else{
//                        weakSelf.redPacketDetailView.isShowDetail = false
//                    }
//                    if !weakSelf.redPacketDetailView.isShow{
//                        UIApplication.shared.keyWindow?.addSubview(weakSelf.redPacketDetailView)
//                    }
//                    weakSelf.redPacketDetailView.isShow = true
//                }
//            }else if type == "\(sendMessageType.roomOnLineList.rawValue)" { //在线用户
//                guard let wraper = model as? CROnLineListWraper,let list = wraper.source else {
//                    return
//                }
//
//                weakSelf.drawer?.subviewsBGView.updateInfo(list: list, type: .DrawerSectionOnlineUser)
//
//            }else if type == "\(sendMessageType.managerList.rawValue)" { //管理员列表
//                guard let wraper = model as? CROnLineListWraper,let list = wraper.source else {
//                    return
//                }
//                weakSelf.drawer?.subviewsBGView.updateInfo(list: list,type: .DrawerSectionManager)
//            }else if type == "\(sendMessageType.privateChatInit.rawValue)" { //私聊列表，和初始化私聊
//                weakSelf.handlePrivateChatInitWith(model: model)
//            }else if type == "\(sendMessageType.privateChatSend.rawValue)" { //私聊发送消息相关
//                weakSelf.handlePrivateChatSend(model:model)
//            }else if type == "\(sendMessageType.currentBalance.rawValue)" { //用户余额
//                guard let balanceModel = model as? CRBalanceModel else {
//                    return
//                }
//
//                weakSelf.drawer?.subviewsBGView.header.updateBalance(value: balanceModel.money)
//            }else if type == "\(sendMessageType.winPrizeList.rawValue)"{
//
//                guard let prizeSource = model as? CRWinningSource,let winPrizeLists = prizeSource.winningList else{
//                    return
//                }
//                //获取系统默认图像前十
//                weakSelf.sendMsgGetUserAvatarList(userId: winPrizeLists[0].id)
//
//                for (index,item) in winPrizeLists.enumerated(){
//                    if weakSelf.userIconLists.count > winPrizeLists.count{
//                        item.imageIconUrl = weakSelf.userIconLists[index]
//                    }
//                }
//                if weakSelf.userIconLists.count > 0{
//                    let prizeListView =  CRWinPrizeListView.init(frame: UIScreen.main.bounds, prizeLists: winPrizeLists)
//                    UIApplication.shared.keyWindow?.addSubview(prizeListView)
//                }
//
//            }else if type == "\(sendMessageType.signIn.rawValue)" {
//                guard let signModel = model as? CRSignModel else {
//                    return
//                }
//
//                weakSelf.handleSignin(model: signModel)
//            }
//        })
//    }
//
//
//
}
