//
//  CRNetManager.swift
//  Chatroom
//
//  Created by admin on 2019/7/5.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

func netStartLog(functionDes:String,urlString:String,paramters:Parameters) {
    print("\n请求【开始】URL:\(urlString) --- \(functionDes) \nparameter:\(paramters)")
}

func netResponseLog(functionDes:String,urlString:String,response:String) {
    print("\n请求【结束】URL:\(urlString) --- \(functionDes) \n返回数据:\nresponse:\(response)")
    if let reuslt = CRAuthWrapper.deserialize(from: response) {
        if reuslt.success{
            if let selfUserId = reuslt.source?.selfUserId{
                if !selfUserId.isEmpty{
                    
                }
               
            }
          
        }
    }
    
}

class CRNetManager: NSObject {
    typealias Parameters = [String: Any]
    typealias responseErrorClosure = (_ success:Bool,_ msg:String) -> ()
    static let instance = CRNetManager()
    
    class func shareInstance() -> CRNetManager {
        return instance
    }
    //取消指定请求
    func cancelRequest(){
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler {
            (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach {
                //只取消指定url的请求
                // 推出页面，不需要再请求的接口
                let urls = [LOTTERY_LAST_RESULT_URL,CRUrl.shareInstance().URL_LOGIN,CRUrl.shareInstance().URL_AUTHENTICATE,CRUrl.shareInstance().URL_UPLOAD_FILE,CRUrl.shareInstance().URL_READ_FILE]
                
                for url in urls {
                    if (($0.originalRequest?.url?.absoluteString ?? "").contains(url) ) {
                        $0.cancel()
                    }
                }
                
            }
        }
    }
    
    ///////////////////
    
    ///查询购彩记录请求方法
    func netObtainLotteryQuery(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String,_ code:Int?, _ models:CountDown?) -> ()) {
        let urlString = CRUrl.shareInstance().getChatSendMessage()
        let funcDes = "查询购彩记录"
        
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().sendMessageRuqest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            
        }
    }
    
    //////////////////
    
    /**
     * 获取当前期号离结束投注倒计时
     * @param bianHao 彩种编号
     * @param lotVersion 彩票版本
     */
    func netGetCountDownByCpcode(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String,_ code:Int?, _ models:CountDown?) -> ()) {
        let urlString = LOTTERY_COUNTDOWN_URL
        let funcDes = "获取倒计时"
        
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil,nil)
            }else {
                guard let wrapper = LocCountDownWraper.deserialize(from: response) else {
                    handler(false,"",nil,nil)
                    return
                }
                
                if wrapper.success {
                    CRDefaults.setToken(value: wrapper.accessToken)
                    handler(true,"",nil,wrapper.content)
                }else {
                    handler(false,wrapper.msg,wrapper.code,nil)
                }
            }
        }
    }
    
    ///获取账户信息
    func netAccountWeb(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String, _ models:Meminfo?) -> ()) {
        let urlString = MEMINFO_URL
        let funcDes = "获取账户信息"
        
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = MemInfoWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取开奖结果
    func netLastOpenResult(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String, _ models:[BcLotteryData]?) -> ()) {
        let urlString = LOTTERY_LAST_RESULT_URL
        let funcDes = "获取开奖结果"
        
//        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
//            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = LastResultWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取官方反水
    func netSync_official_peilvs_after_playrule_click(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String, _ model:[PeilvWebResult]?) -> ()) {
        let urlString = GET_JIANJIN_ODDS_URL
        let funcDes = "获取官方反水"
        
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = PeilvWebResultWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success {
//                    let rightResult = CRBetLogic.update_slide_when_peilvs_obtain(rateBack: wrapper.content)
                    let odds = wrapper.content
                    handler(true,"",odds)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///赔率获取
    func netSync_honest_plays_odds_obtain(fakeParentCode:String = "",paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String, _ models:[HonestResult]?) -> ()) {
        let urlString = GET_HONEST_ODDS_URL
        let funcDes = "获取赔率数据"
        
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = PeilvHonestResultWrapper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                let content = wrapper.content
                //时时彩，分分彩，1-5球 本地数据处理;过滤掉大小单双
                guard let playCodes = paramters["playCodes"] as? String else {return}
                
                if fakeParentCode == "ffc_ssc_1To5" {
                    for (_,resultModel) in content.enumerated() {
                        var newOdds = [PeilvWebResult]()

                        for (_,webResult) in resultModel.odds.enumerated() {
                            if !["大","小","单","双"].contains(webResult.numName) {
                                newOdds.append(webResult)
                            }
                        }

                        resultModel.odds = newOdds
                    }
                }

                if fakeParentCode == "ffc_ssc_lm" { //两面，过滤掉 0-9
                    for (_,resultModel) in content.enumerated() {
                        var newOdds = [PeilvWebResult]()

                        for (_,webResult) in resultModel.odds.enumerated() {
                            if !["0","1","2","3","4","5","6","7","8","9"].contains(webResult.numName) {
                                newOdds.append(webResult)
                            }
                        }

                        resultModel.odds = newOdds
                    }
                }
                
                if wrapper.success {
                    handler(true,"",content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取冷热遗漏数据
    func netColdHoltCodeRank(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String, _ models:[CodeRankModel]?) -> ()) {
        let urlString = API_COLD_HOT
        let funcDes = "获取冷热遗漏数据"
        
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CodeRankModelWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取彩种对应的玩法
    func netAllPlayRules(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String?, _ model:LotteryData?) -> ()) {
        let urlString = GAME_PLAYS_URL
        
        let funcDes = "获取玩法"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = LotPlayWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }

                if wrapper.success {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取所有彩种
    func netAllLotteries(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String?, _ models:[LotteryData]?) -> ()) {
        let urlString = ALL_GAME_DATA_URL
        
        let funcDes = "获取所有彩种"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .get, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = LotterysWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    //MARK: - 聊天室
    ///分享注单
    func netShareBetMsg(paramters: Parameters,handler:@escaping (_ success:Bool,_ msg:String?, _ models:[LotteryData]?) -> ()) {
        let urlString = CRUrl.shareInstance().getChatSendMessage()
        
        let funcDes = "分享注单"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: .post, parameters: paramters) { (success, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = LotterysWraper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///进入房间
    func netInsideChatRoom(paramters: Parameters,method: HTTPMethod,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRRoomInfoModel?) -> ()) {
        let urlString = CRUrl.shareInstance().URL_AUTH
        var p = paramters
        
        p["code"] = CRUrl.shareInstance().JOIN_CHAT_ROOM
        p["source"] = "app"

        
        let funcDes = "进入房间"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: p)
        
        CRNetBase.shareInstance().netWithRequest(urlString: urlString, method: method, parameters: p) { (success, response) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CRRoomInfoWrapper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                if wrapper.success  {
                    handler(true,"",wrapper.source)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取聊天室列表
    func netGetChatRoomList(paramters: Parameters,method: HTTPMethod,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRRoomListModels?) -> ()) {
        let urlString = CRUrl.shareInstance().URL_AUTH
        var p = paramters
        
        p["code"] = CRUrl.shareInstance().CHAT_ROOM_LIST
        p["source"] = "app"
        
        let funcDes = "获取聊天室列表"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: p)
        
        CRNetBase.shareInstance().netWithRequest(urlString: urlString, method: method, parameters: p) { (success, response) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CRRoomListWrapper.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                
                if wrapper.success  {
                    handler(true,"",wrapper.source)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///聊天室登录
    func netAuthenticate(paramters: Parameters,method: HTTPMethod,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRAuthMsgModel?) -> ()) {
        let urlString = CRUrl.shareInstance().URL_AUTH
        var p = [String:Any]()
        
        var token = ""
        if let encrypted = paramters["encrypted"]  as? String,let sign = paramters["sign"] as? String,let clusterId = paramters["clusterId"] as? String {
            token =  "clusterId=" + clusterId + "&encrypted=" + encrypted + "&sign=" + sign
            p["token"] = token
            p["code"] = CRUrl.shareInstance().LOGIN_AUTHORITY
            p["source"] = "app"
        }
        
        let funcDes = "聊天室登录"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: p)
        
        CRNetBase.shareInstance().netWithRequest(urlString: urlString, method: method, parameters: p) { (success, response) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CRAuthWrapper.deserialize(from: response) else {
                    handler(false,"\(funcDes)失败",nil)
                    return
                }

                if wrapper.success  {
                    handler(true,"",wrapper.source)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取公告列表
    func getRoomNotice(paramters: Parameters,method: HTTPMethod,handler:@escaping (_ success:Bool,_ msg:String, _ model:[CRNoticeResult]?) -> ()) {
        let urlString = CRUrl.shareInstance().URL_AUTH
        let funcDes = "获取公告列表"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netWithRequest(urlString: urlString, method: method, parameters: paramters) { (success, response) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CRNoticeModel.deserialize(from: response) else {
                    handler(false,"",nil)
                    return
                }
                if wrapper.success  {
                    handler(true,"",wrapper.source)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///获取聊天室授权信息
    func netAuthenticateLogin(paramters: Parameters,method: HTTPMethod,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRAuthenticateModel?) -> ()) {
        let urlString = CRUrl.shareInstance().URL_AUTHENTICATE
        
        let funcDes = "聊天室授权"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: method, parameters: paramters) { (success, response) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CRAuthenticateWrapper.deserialize(from: response) else {
                    handler(false,"\(funcDes)失败",nil)
                    return
                }
                
                if !wrapper.accessToken.isEmpty {
                    CRDefaults.setToken(value: wrapper.accessToken)
                }
                
                if wrapper.success  {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
    
    ///聊天室发送文本消息/Users/gallen/trunk/gameplay/ChatRoom
    func sendMessage(paramters:Parameters,method:HTTPMethod,requestType:sendMessageType,handler:@escaping (_ success:Bool,_ msg:String, _ model:Any?) -> ()){
        /*
        let urlString = CRUrl.shareInstance().getChatSendMessage()
        let funcDes = "发送消息"
        CRNetBase.shareInstance().sendMessageRuqest(urlString: urlString, method: method, parameters: paramters) { (isSuccess, response) in
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            if !isSuccess{
                handler(false,response,nil)
            }else{
                switch requestType{
                case .redPacket:
                    guard let jsonObjctItem = CRRedPacketDataItem.deserialize(from: response) else{
                        handler(false,"发送消息失败",nil)
                        return
                    }
                    if jsonObjctItem.success{
                        handler(true,"发送消息成功",jsonObjctItem)
                    }else{
                        handler(false,jsonObjctItem.msg,jsonObjctItem)
                    }
                    break
                case .text:
                    guard let jsonObjctItem = CRMessage.deserialize(from: response) else{
                        handler(false,"发送消息失败",nil)
                        return
                    }
                    if  jsonObjctItem.success {
                        handler(true,"发送消息成功",jsonObjctItem)
                    }else{
                        handler(false,jsonObjctItem.msg,jsonObjctItem)
                    }
                    break
                case .image:
                    guard let jsonObjctItem = CRMessage.deserialize(from: response) else{
                        handler(false,"发送消息失败",nil)
                        return
                    }
                    if jsonObjctItem.success{
                        handler(true,"发送消息成功",jsonObjctItem)
                    }else{
                        handler(false,jsonObjctItem.msg,jsonObjctItem)
                    }
                    
                     break
                case .receiveRedPacket:
                    guard let jsonObjctItem = CRReceiveRedPacketItem.deserialize(from: response) else{
                        handler(false,"发送消息失败",nil)
                        return
                    }
                    if jsonObjctItem.success{
                        handler(true,"发送消息成功",jsonObjctItem)
                    }else{
                        handler(false,jsonObjctItem.msg,jsonObjctItem)
                    }
                    break
                case .shareOrder:
                    guard let jsonObjctItem = CRMessage.deserialize(from: response) else{
                        handler(false,"发送消息失败",nil)
                        return
                    }
                    if jsonObjctItem.success{
                         handler(true,"发送消息成功",jsonObjctItem)
                    }else{
                         handler(false,jsonObjctItem.msg,jsonObjctItem)
                    }
                   
                    break
                case .followOrder:
                    guard let jsonObjctItem = CRFollowBetWrapper.deserialize(from: response) else{
                        handler(false,"跟单失败",nil)
                        return
                    }
                    
                    if jsonObjctItem.success {
                        guard let model = jsonObjctItem.source else {
                            handler(false,"跟单成功",nil)
                            return
                        }
                        if model.success {
                            handler(true,"跟单成功",model)
                        }else {
                            handler(false,model.msg,nil)
                        }
                        
                    }else {
                        handler(false,jsonObjctItem.msg,nil)
                    }
                    
                     break
                case .systemNotice:
                    //系统公告
                    guard let jsonObjcItem = CRSystemNoticeItem.deserialize(from: response) else{
                        handler(false,"获取系统消息失败",nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取系统消息成功",jsonObjcItem)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    
                    break
                case .pullLotteryTicket:
                    guard let jsonObjcItem = CRLotterDataItem.deserialize(from: response) else{
                        handler(false,"获取彩种失败",nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取彩种成功",jsonObjcItem)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    break
                case .receiveDetail:
                    guard let jsonObjcItem = CRReceiveDetailItem.deserialize(from: response) else{
                        handler(false,"获取领取红包详情失败",nil)
                        return
                    }
                    if jsonObjcItem.success {
                         handler(true,"获取领取红包详情成功",jsonObjcItem)
                    }else{
                         handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    break
                case .userIcon,.submitUserInfo:
                    guard let jsonObjcItem = CRSystemIconItem.deserialize(from: response) else{
                        handler(false,"获取系统默认用户图像失败",nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取系统默认用户图像成功",jsonObjcItem)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    break
                case .userTodayWinLos:
                    
                    break
                case .historyBetShare:
                    guard let wrapper = CRBetHistoryWraper.deserialize(from: response) else {
                        handler(false,"获取历史注单消息失败",nil)
                        return
                    }
                    if wrapper.success{
                        handler(true,"获取历史注单消息成功",wrapper.source?.msg)
                    }else{
                        handler(false,"获取历史注单消息失败",wrapper.source?.msg)
                    }
                break
                case .userInfo:
                    //个人信息
                    guard let jsonObjcItem = CRUserInfoCenterItem.deserialize(from: response) else{
                        handler(false,"获取用户个人信息失败",nil)
                        return
                    }
                    if jsonObjcItem.success {
                        handler(true,"获取用户个人信息成功",jsonObjcItem)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    break
                case .historyRecord:
                    //获取历史记录
                    guard let jsonObjcItem = CRHistoryRecordItem.deserialize(from: response) else{
                        handler(false,"获取历史记录失败",nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取历史记录成功",jsonObjcItem)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    break
                case .chatroomSystemConfiguration:
                    //聊天室系统配置
                    guard let jsonObjcItem = CRSystemConfigurationItem.deserialize(from: response) else{
                        handler(false,"获取聊天室系统酌情置失败",nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,response,jsonObjcItem)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                
                    break
                case .contrabandLanguage:
                    //违禁词
                    guard let jsonObjcItem = CRProhibitLanguageItem.deserialize(from: response) else{
                        handler(false,"获取违禁词失败",nil)
                        return
                    }
                    if jsonObjcItem.success{
                         handler(true,"获取违禁词成功",jsonObjcItem)
                    }else{
                         handler(false,jsonObjcItem.msg,jsonObjcItem)
                    }
                    break
                case .lookPacketDetail:
                    break
                case .shareToday:
                    break
                case .roomOnLineList:
                    break
                case .managerList:
                    break
                case .currentBalance:
                    break
                }
            }
        }
 */
    }
    
    //MARK:上传图片到服务器
    func uploadFile(paramters:Parameters,method:HTTPMethod,imageData:Data,imagePath:String,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRUploadImageItem?) -> (),progressClouser:@escaping (_ progress:Double) -> ()){
        let urlString = CRUrl.shareInstance().CHAT_FILE_BASE_URL +  CRUrl.shareInstance().URL_UPLOAD_FILE
        let funsDes = "上传图片到服务器"
        CRNetBase.shareInstance().uploadFile(urlString: urlString, method: .post, parameters: paramters, fileData: imageData, filePath: imagePath, handler: { (isSuccess, json) in
            netResponseLog(functionDes: funsDes, urlString: urlString, response: json)
            if !isSuccess{
                handler(false,json,nil)
            }else{
                guard let jsonObjctItem = CRUploadImageItem.deserialize(from: json) else{
                    handler(false,"上传图片失败",nil)
                    return
                }
                if jsonObjctItem.success{
                     handler(true,"上传图片成功",jsonObjctItem)
                }else{
                    handler(false,"上传图片失败",jsonObjctItem)
                }
            }
        }) { (progress) in
            progressClouser(progress)
        }
    }
    
    //MARK: 上传头像图片到服务器
        func uploadAvatarFile(paramters:Parameters,method:HTTPMethod,imageData:Data,imagePath:String,handler:@escaping (_ success:Bool,_ msg:String, _ fileCode:String?) -> (),progressClouser:@escaping (_ progress:Double) -> ()){
            
            let urlString = CRUrl.shareInstance().CHAT_FILE_BASE_URL +  CRUrl.shareInstance().URL_UPLOAD_IMAGE
            let funcDes = "上传头像"
            netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
            
            UploadAvatarLocal(url:urlString, dic: paramters, imageData: imageData) { (response, success) in
                if success {
                    if let fileCode = response["fileCode"] as? String {
                        handler(true,"",fileCode)
                    }else {
                        handler(false,"上传头像出错",nil)
                    }
                }else {
                    handler(false,"上传头像出错",nil)
                }
            }
        }
    
    //MARK:上传音频到服务器
    func uploadFile(paramters:Parameters,method:HTTPMethod,fileData:Data,filePath:String,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRUploadImageItem?) -> (),progressClouser:@escaping (_ progress:Double) -> ()){
        let urlString = CRUrl.shareInstance().CHAT_FILE_BASE_URL +  CRUrl.shareInstance().URL_UPLOAD_FILE
        let funsDes = "上传音频文件到服务器"
        CRNetBase.shareInstance().uploadFile(urlString: urlString, method: .post, parameters: paramters, fileData: fileData, filePath: filePath, handler: { (isSuccess, json) in
            netResponseLog(functionDes: funsDes, urlString: urlString, response: json)
            if !isSuccess{
                handler(false,json,nil)
            }else{
                guard let jsonObjctItem = CRUploadImageItem.deserialize(from: json) else{
                    handler(false,"上传音频文件失败",nil)
                    return
                }
                if jsonObjctItem.success{
                    handler(true,"上传音频文件成功",jsonObjctItem)
                }else{
                    handler(false,"上传音频文件失败",jsonObjctItem)
                }
            }
        }) { (progress) in
            progressClouser(progress)
        }
    }
    //MARK：读取图片
    func readFile(parameters:Parameters ,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRUploadImageItem?) -> ()){
        let urlString = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE
        let funcDes = "读取图片消息"
        CRNetBase.shareInstance().loadImageRuqest(urlString: urlString, method: .get, parameters: parameters) { (isSuccess, jsonString) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: jsonString)
            
        }
    }
    
    
    ///login
    func netLogin(paramters: Parameters,method: HTTPMethod,handler:@escaping (_ success:Bool,_ msg:String, _ model:CRLoginResult?) -> ()) {
        let urlString = CRUrl.shareInstance().URL_LOGIN
        
        let funcDes = "进入房间"
        netStartLog(functionDes: funcDes, urlString: urlString, paramters: paramters)
        
        CRNetBase.shareInstance().netRequest(urlString: urlString, method: method, parameters: paramters) { (success, response) in
            
            netResponseLog(functionDes: funcDes, urlString: urlString, response: response)
            
            if !success {
                handler(false,response,nil)
            }else {
                guard let wrapper = CRLoginWraper.deserialize(from: response) else {
                    handler(false,"登录失败",nil)
                    return
                }
                if !wrapper.accessToken.isEmpty {
                    CRDefaults.setToken(value: wrapper.accessToken)
                }
                if wrapper.success  {
                    handler(true,"",wrapper.content)
                }else {
                    handler(false,wrapper.msg,nil)
                }
            }
        }
    }
}

