//
//  CRInsetsLabel.swift
//  gameplay
//
//  Created by Gallen on 2019/12/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRInsetsLabel: UILabel {

    public var textInsets: UIEdgeInsets = .zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    internal init(frame: CGRect, textInsets: UIEdgeInsets) {
        self.textInsets = textInsets
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        var rects = rect
//        let lableWidth = rect.width + 4
//        let lableRect = CGRect(x:2, y: 0, width: lableWidth, height: rects.height)
        super.drawText(in: rects.inset(by: textInsets))
    }

}
