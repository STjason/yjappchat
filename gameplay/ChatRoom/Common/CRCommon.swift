//
//  CRCommon.swift
//  Chatroom
//
//  Created by admin on 2019/7/5.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
///是否可以自动分享注单
func shouldAutoShareBet(askMoney:String,realMoney:String) -> Bool {
    if let askMoneyValue = Double(askMoney),let realMoneyValue = Double(realMoney) {
        return realMoneyValue >= askMoneyValue
    }
    
    return false
}

/// Dictionary -> JsonString
func jsonStringFrom(dictionary:[String: Any]) -> String? {
    
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .init(rawValue: 0))
        
        if  let jsonString = String.init(data: jsonData, encoding: .utf8) {
            return jsonString
        }
        
    } catch {
        print(error.localizedDescription)
    }
    
    return nil
}

func getChatController() -> CRChatController? {
//    if isSocketConnect {
//        let vc = CRChatController()
//        return vc
//    }else {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reConnectSocketNoti"), object: nil)
//        return nil
//    }
    
    let vc = CRChatController()
    return vc
}

class CRCommon {
    
    class func generateMsgID() -> String {
        let userid = CRDefaults.getUserID()
        let timeStamp = Date.timeIntervalSinceReferenceDate
        let timeStapString = String(format:"%f",timeStamp)
        let timeString = timeStapString.replacingOccurrences(of: ".", with: "")
        return userid + timeString
    }
}

