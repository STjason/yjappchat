//
//  CRSpark.m
//  gameplay
//
//  Created by admin on 2019/10/22.
//  Copyright © 2019年 yibo. All rights reserved.
//

#import "CRSpark.h"

@implementation CRSpark

//+ (void)setupSpark:(UIView *)view imageOne:(NSString *)imageOne imageTwo:(NSString *)imageTwo {
+ (void)setupSpark:(UIView *)view images:(NSArray *)images {
    CAEmitterLayer *caELayer;
    caELayer                   = [CAEmitterLayer layer];
    // 发射源
    caELayer.emitterPosition   = CGPointMake(view.frame.size.width / 2, view.frame.size.height - 50);
    // 发射源尺寸大小
    caELayer.emitterSize       = CGSizeMake(50, 0);
    // 发射源模式
    caELayer.emitterMode       = kCAEmitterLayerOutline;
    // 发射源的形状
    caELayer.emitterShape      = kCAEmitterLayerLine;
    // 渲染模式
    caELayer.renderMode        = kCAEmitterLayerAdditive;
    
    
    // 发射方向
    caELayer.velocity          = 1;
    // 随机产生粒子
    caELayer.seed              = (arc4random() % 100) + 1;
    // 不超出范围
    caELayer.frame = CGRectMake(0, 80, view.frame.size.width, view.frame.size.height - 80 - 50);
    caELayer.masksToBounds = YES;
    // cell
    CAEmitterCell *cell             = [CAEmitterCell emitterCell];
    // 速率
    cell.birthRate                  = 1.0;
    // 发射的角度
    cell.emissionRange              = 0.11 * M_PI;
    // 速度
    cell.velocity                   = 300;
    // 范围
    cell.velocityRange              = 150;
    
    /////////
    
    // Y轴 加速度分量
    cell.yAcceleration              = 75;
    // 声明周期
    cell.lifetime                   = 2.04;
    //是个CGImageRef的对象,既粒子要展现的图片
    cell.contents                   = (id)
    
    [[UIImage imageNamed:images[0]] CGImage];
    [[UIImage imageNamed:images[1]] CGImage];
    [[UIImage imageNamed:images[2]] CGImage];
    
    // 缩放比例
    cell.scale                      = 0.15;
    // 粒子的颜色
    cell.color                      = [[UIColor colorWithRed:0.6
                                                       green:0.6
                                                        blue:0.6
                                                       alpha:1.0] CGColor];
    
    // 一个粒子的颜色green 能改变的范围
    cell.greenRange                 = 1.0;
    // 一个粒子的颜色red 能改变的范围
    cell.redRange                   = 1.0;
    // 一个粒子的颜色blue 能改变的范围
    cell.blueRange                  = 1.0;
    // 子旋转角度范围
    cell.spinRange                  = M_PI;
    
    
    /////////////
    
    // 爆炸
    CAEmitterCell *burst            = [CAEmitterCell emitterCell];
    // 粒子产生系数
    burst.birthRate                 = 1.0;
    // 速度
    burst.velocity                  = 0;
    // 缩放比例
    burst.scale                     = 2.5;
    // shifting粒子red在生命周期内的改变速度
    burst.redSpeed                  = -1.5;
    // shifting粒子blue在生命周期内的改变速度
    burst.blueSpeed                 = +1.5;
    // shifting粒子green在生命周期内的改变速度
    burst.greenSpeed                = +1.0;
    //生命周期
    burst.lifetime                  = 0.08;
    
    ///========
    
    
    // 火花 and finally, the sparks
    CAEmitterCell *spark            = [CAEmitterCell emitterCell];
    //粒子产生系数，默认为1.0
    spark.birthRate                 = 400;
    //速度
    spark.velocity                  = 125;
    // 360 deg//周围发射角度
    spark.emissionRange             = 2 * M_PI;
    // gravity//y方向上的加速度分量
    spark.yAcceleration             = 75;
    //粒子生命周期
    spark.lifetime                  = 3;
    //是个CGImageRef的对象,既粒子要展现的图片
    spark.contents                  = (id)
    
    [[UIImage imageNamed:images[3]] CGImage];
    [[UIImage imageNamed:images[4]] CGImage];
    [[UIImage imageNamed:images[5]] CGImage];
    
    //缩放比例速度
    spark.scaleSpeed                = -0.2;
    //粒子green在生命周期内的改变速度
    spark.greenSpeed                = -0.1;
    //粒子red在生命周期内的改变速度
    spark.redSpeed                  = 0.4;
    //粒子blue在生命周期内的改变速度
    spark.blueSpeed                 = -0.1;
    //粒子透明度在生命周期内的改变速度
    spark.alphaSpeed                = -0.25;
    //子旋转角度
    spark.spin                      = 2* M_PI;
    //子旋转角度范围
    spark.spinRange                 = 2* M_PI;
    
    
    caELayer.emitterCells = [NSArray arrayWithObject:cell];
    cell.emitterCells = [NSArray arrayWithObjects:burst, nil];
    burst.emitterCells = [NSArray arrayWithObject:spark];
    [view.layer addSublayer:caELayer];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [caELayer removeAllAnimations];
        [caELayer removeFromSuperlayer];
    });
}

@end
