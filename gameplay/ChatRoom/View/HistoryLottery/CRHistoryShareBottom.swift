//
//  CRHistoryShareBottom.swift
//  gameplay
//
//  Created by admin on 2019/10/7.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRHistoryShareBottom: UIView {
    
    var sharebetHandler:(() -> Void)? //点击分享注单按钮
    var allSelectHandler:((_ selectAllButton:UIButton,_ selectCheckButton:UIButton) -> Void)? //全选
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var selectAllCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectAllButton.setTitle("全选", for: .normal)
        self.selectAllButton.setTitle("取消", for: .selected)
        self.shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        self.selectAllButton.addTarget(self, action: #selector(allCheckAction), for: .touchUpInside)
        self.selectAllCheck.addTarget(self, action: #selector(allCheckAction), for: .touchUpInside)
    }
    
    @objc func shareAction() {
        self.sharebetHandler?()
    }
    
    @objc func allCheckAction() {
        self.allSelectHandler?(self.selectAllButton,selectAllCheck)
    }
    
}
