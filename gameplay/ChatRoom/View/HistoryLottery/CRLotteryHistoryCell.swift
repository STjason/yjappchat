//
//  CRLotteryHistoryCell.swift
//  gameplay
//
//  Created by admin on 2019/10/5.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRLotteryHistoryCell: UITableViewCell {
    @IBOutlet weak var lotteryRule: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var numsLabel: UILabel!
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    var status = ""
    var colour: UIColor = UIColor.orange

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.checkBox.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configWith(isSelected:Bool,model:CRBetHistoryMsgModel) {
        self.checkBox.isSelected = isSelected
        let mode = model.lotVersion == 1 ? "官" : "信"
        self.lotteryRule.text = "[\(mode)]-\(model.playType) - \(model.playName)"
        self.periodLabel.text = "\(trimQihao(currentQihao: model.qiHao))期"
        self.moneyLabel.text = String.init(format: "%.2f元", model.buyMoney)
        self.numsLabel.text = model.haoMa
        if model.status == 1{
            status = "等待开奖"
            colour = .orange
        }
        else if model.status == 2{
            status = "已中奖"
            colour = .red
        }else if model.status == 3{
            status = "未中奖"
            colour = .white
        }
        self.statusLabel.text = status
        self.statusLabel.textColor = colour

    }
    
}
