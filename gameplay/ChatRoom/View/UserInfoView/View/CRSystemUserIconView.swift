//
//  CRSystemUserIconView.swift
//  gameplay
//
//  Created by Gallen on 2019/10/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRSystemUserIconView: UIView {
    /**是否显示*/
    var isShow:Bool = false
    
    var systemUserIcons:[String] = [] {
        didSet{
            userIconCollectionView.reloadData()
        }
    }
    /**回调闭包*/
    var userIconUrl:((_ iconUrl:String)->(Void))?
    
    lazy var userIconCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenWidth), collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
      
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:--设置UI
extension CRSystemUserIconView{
    func setupUI(){
        
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        addSubview(userIconCollectionView)
        userIconCollectionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(self.frame.size.width)
            
        }
        //注册Cell
        userIconCollectionView.register(CRUserIconCell.self, forCellWithReuseIdentifier: "CRUserIconCell")
    }
}

extension CRSystemUserIconView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfItem = self.systemUserIcons.count 
        return switchLocalAvar() ? numberOfItem + 1 : numberOfItem
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let userIconCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CRUserIconCell", for: indexPath) as? CRUserIconCell
        if systemUserIcons.count > indexPath.row{
            userIconCell?.urlString = self.systemUserIcons[indexPath.row]
        }else {
            userIconCell?.localImageString = "uploadAvatarIcon"
        }
        return userIconCell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (self.frame.size.width - 50) / 4
        return CGSize(width: cellWidth, height: cellWidth)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //点击的图片
        var urlString: String?
        if systemUserIcons.count > indexPath.row{
            urlString = self.systemUserIcons[indexPath.row]
        }
        userIconUrl?(urlString ?? "")
            
        let selectedCell = collectionView.cellForItem(at: indexPath)
        selectedCell?.layer.borderWidth = 1
        selectedCell?.layer.cornerRadius = ((self.frame.size.width - 50) / 4) * 0.5
        selectedCell?.layer.borderColor = UIColor.gray.cgColor
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let didDeselectCell = collectionView.cellForItem(at: indexPath)
        didDeselectCell?.layer.borderWidth = 0
        didDeselectCell?.layer.borderColor = UIColor.clear.cgColor
    }
}
extension CRSystemUserIconView{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.removeFromSuperview()
    }
}
