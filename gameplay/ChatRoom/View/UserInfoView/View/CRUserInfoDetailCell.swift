//
//  CRUserInfoCell.swift
//  gameplay
//
//  Created by Gallen on 2019/10/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
protocol CRUserInfoDetailCellDelegate {
    /**刷新数据代理*/
    func userInfoDetailCellRefreshData()
    /**开关起用或关闭*/
    func switchFunctionStartAndClose(on:Bool,cell:CRUserInfoDetailCell)
    /**分享今天输赢*/
    func shareGainsAndLossesForToday()
}
class CRUserInfoDetailCell: UITableViewCell {
    
    var userInfoDetailCellDelegate:CRUserInfoDetailCellDelegate?

    var userInfoItem:CRUserInfoItem?{
        didSet{
            guard let infoItem = userInfoItem else {
                return
            }
            titleNameLabel.text = infoItem.titleName
            moneyLabel.text = String(format:"%.2f",infoItem.money)
            swicthButton.isHidden = !infoItem.isSwitchOn
            moneyLabel.isHidden = infoItem.isSwitchOn
            if infoItem.titleName == "今日中奖:" {
                refreshButotn.isHidden = false
                refreshButotn.setTitle("分享", for: .normal)
            }else if infoItem.titleName == "今日盈亏:"{
                refreshButotn.isHidden = false
                refreshButotn.setTitle("刷新", for: .normal)
            }else{
                refreshButotn.isHidden = true
            }
            if infoItem.titleName == "消息推送通知"{
                swicthButton.isOn = CRDefaults.getChatroomMessageNoti()
            }else if infoItem.titleName == "消息发送提示音"{
                swicthButton.isOn = CRDefaults.getChatroomSendMessageVoice()
            }else if infoItem.titleName == "消息接收提示音"{
                swicthButton.isOn = CRDefaults.getChatroomRecerveMessageVoicei()
            }else if infoItem.titleName == "进房通知"{
                swicthButton.isOn = CRDefaults.getChatroomEnterRoomNoti()
            }else{
                swicthButton.isOn = false
            }
        }
    }

    lazy var titleNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var moneyLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithHexString("#ffb802")
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    /**下划线*/
    lazy var underLineView:UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.colorWithRGB(r: 230, g: 230, b: 230, alpha: 1.0)
        return lineView
    }()
    
    lazy var refreshButotn:UIButton = {
        //刷新按钮
        let button = UIButton()
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.backgroundColor = UIColor.colorWithHexString("#ffcc3b")
        button.setTitle("刷新", for: .normal)
        button.addTarget(self, action: #selector(refreshData(button:)), for: .touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    //切换开关
    lazy var swicthButton:UISwitch = {
        let button = UISwitch()
        button.isOn = CRDefaults.getChatroomMessageNoti()
        button.addTarget(self, action:#selector(switchFunction(switchButton:)), for: UIControl.Event.valueChanged)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:-设置UI
extension CRUserInfoDetailCell{
    func setupUI(){
        
        contentView.addSubview(titleNameLabel)
        contentView.addSubview(moneyLabel)
        contentView.addSubview(underLineView)
        contentView.addSubview(refreshButotn)
        contentView.addSubview(swicthButton)
        selectionStyle = .none
        
        
        //用户信息标题
        titleNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(15)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        //刷新按钮
        refreshButotn.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-15)
            make.centerY.equalTo(titleNameLabel.snp.centerY)
            make.width.equalTo(50)
            make.height.equalTo(28)
        }
        
        swicthButton.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-15)
            make.centerY.equalTo(titleNameLabel.snp.centerY)
            make.width.equalTo(50)
            make.height.equalTo(28)
        }
        //金额
        moneyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleNameLabel.snp.right).offset(20)
            make.centerY.equalTo(titleNameLabel.snp.centerY)
//            make.right.equalTo(refreshButotn.snp.left).offset(-10)
        }
        
        
        underLineView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(contentView)
            make.height.equalTo(0.5)
        }
      
    }
}
//MARK:- 响应事件
extension CRUserInfoDetailCell{
    //刷新数
    @objc private func refreshData(button:UIButton){
        if button.currentTitle == "刷新" {
            self.userInfoDetailCellDelegate?.userInfoDetailCellRefreshData()
        }else if button.currentTitle == "分享"{
            //
            self.userInfoDetailCellDelegate?.shareGainsAndLossesForToday()
        }
       
    }
    @objc private func switchFunction(switchButton:UISwitch){
        //开关
        userInfoDetailCellDelegate?.switchFunctionStartAndClose(on: switchButton.isOn, cell: self)
    }
}
