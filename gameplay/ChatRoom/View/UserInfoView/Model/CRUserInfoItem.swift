//
//  CRUserInfoItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRUserInfoItem: NSObject {
    
    var titleName:String = ""
    var money:Float = 0
    /**是否有开关*/
    var isSwitchOn:Bool = false
}
