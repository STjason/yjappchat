//
//  CRImageCollectBottomToolBarView.swift
//  gameplay
//
//  Created by JK on 2019/12/9.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CRImageCollectBottomToolBarView: UIView {
    
    /** 删除 按钮 */
    var deleteBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.frame = kCGRect(x: 10, y: 5, width: 60, height: 40)
        btn.backgroundColor = UIColor.colorWithHexString("ec2829")
        
        btn.setTitleColor(.white, for: .normal)
        btn.setTitle("删除", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        
        btn.layer.cornerRadius = 5
        
        return btn
    }()
    
    /** 确认&删除 按钮 */
    var sendBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.frame = kCGRect(x: kScreenWidth - 65, y: 5, width: 60, height: 40)
        btn.backgroundColor = UIColor.colorWithHexString("FF4081")
        
        btn.setTitle("发送", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        
        btn.layer.cornerRadius = 5
        
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .black
        
        addSubview(deleteBtn)
        addSubview(sendBtn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
