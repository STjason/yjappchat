//
//  CRWinPrizeListCell.swift
//  gameplay
//
//  Created by Gallen on 2019/12/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRWinPrizeListCell: UITableViewCell {
    /**用户图像*/
    lazy var userIconImageV:UIImageView = {
        let imageV = UIImageView()
        return imageV
    }()
    /**用户昵称*/
    lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithRGB(r: 51, g: 136, b: 255, alpha: 1.0)
        label.font = UIFont.systemFont(ofSize:14)
        return label
    }()
    /**在那个彩种里种的奖项*/
    lazy var projectLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    /**标题*/
    lazy var prizeTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "喜中"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    /**中奖金额*/
    lazy var prizeMoneyLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithRGB(r: 220, g: 59, b: 64, alpha: 1.0)
        return label
    }()
    
    lazy var underLineView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.colorWithHexString("#e9e9e9")
        return view
    }()
    //指示
    lazy var indicatImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.isHidden = true
        imageV.image = UIImage(named: "chatroom_champion")
        return imageV
    }()
    
    var prizeDetailItem:CRWinningListItem?{
        didSet{
            userNameLabel.text = prizeDetailItem?.userName
            prizeMoneyLabel.text = String.init(format:"¥%@",prizeDetailItem?.prizeMoney ?? "")
            projectLabel.text = prizeDetailItem?.prizeProject
//            let imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE + "?fileId=" + (prizeDetailItem?.id)!
            let imageUrl = prizeDetailItem?.imageIconUrl ?? ""
            userIconImageV.kf.setImage(with: URL(string: imageUrl), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        contentView.addSubview(userIconImageV)
        contentView.addSubview(userNameLabel)
        contentView.addSubview(projectLabel)
        contentView.addSubview(prizeTitleLabel)
        contentView.addSubview(prizeMoneyLabel)
        contentView.addSubview(underLineView)
        contentView.addSubview(indicatImageV)
        
        userIconImageV.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(50)
            make.height.equalTo(50)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        userNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(userIconImageV.snp.right).offset(5)
            make.top.equalTo(userIconImageV.snp.top).offset(2)
        }
        
        projectLabel.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabel.snp.right).offset(5)
            make.centerY.equalTo(userNameLabel.snp.centerY)
        }
        
        prizeTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabel.snp.left)
            make.bottom.equalTo(userIconImageV.snp.bottom).offset(-2)
        }
        
        prizeMoneyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(prizeTitleLabel.snp.right).offset(5)
            make.centerY.equalTo(prizeTitleLabel.snp.centerY)
        }
        
        underLineView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(1)
            
        }
        
        indicatImageV.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-10)
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
