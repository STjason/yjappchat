//
//  CRLeftImgButton.swift
//  Chatroom
//
//  Created by admin on 2019/7/16.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRLeftImgButton: UIButton {
    
    let imageWidth:CGFloat = 15
    
    override var isHighlighted: Bool {
        didSet {
            super.isHighlighted = false
        }
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        titleLabel?.textAlignment = NSTextAlignment.left
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let newX = CGFloat(contentRect.size.width) - imageWidth
        let newWidth = imageWidth
        let newHeight = CGFloat(contentRect.size.height)
        return CGRect(x: newX, y: 0, width: newWidth, height: newHeight)
    }
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let newWidth = contentRect.size.width - imageWidth
        let newHeight = (contentRect.size.height)
        return CGRect(x: 0, y: 0, width: newWidth, height: newHeight)
    }
    
}
