//
//  CRWinPrizeListView.swift
//  gameplay
//
//  Created by Gallen on 2019/12/7.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRWinPrizeListView: UIView {
    
    var winningShow = false //中奖榜单开关
    var profitShow = false //盈利榜单开关
    var selectIndex = 0 //当前选择的seg索引
    
    lazy var backView:UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var prizeListTableView:UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CRWinPrizeListCell.self, forCellReuseIdentifier: "CRWinPrizeListCell")
        
        return tableView
    }()
    
    lazy var segment:UISegmentedControl = {
        let view = UISegmentedControl.init(frame: .zero)
        view.backgroundColor = UIColor.white
        if winningShow &&  profitShow {
            view.insertSegment(withTitle: "中奖榜单", at: 0, animated: true)
            view.insertSegment(withTitle: "盈利榜单", at: 1, animated: true)
            selectIndex = 0
        }
        
        view.selectedSegmentIndex = 0
        view.addTarget(self, action: #selector(segmentAction), for: .valueChanged)
        return view
    }()
    
    //中奖标题View
    lazy var prizeTitleView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.red
        return view
    }()
    
    /**中奖标题*/
    lazy var prizeTitle:UILabel = {
        let title = UILabel()
        title.text = "近期开奖榜单"
        title.textColor = UIColor.white
        title.font = UIFont.systemFont(ofSize: 15)
        return title
    }()
    /**隐藏视图*/
    lazy var closeButton:UIButton = {
        let button = UIButton()
        button.setTitle("X", for: .normal)
        button.addTarget(self, action: #selector(closeWinPrizeListView), for: .touchUpInside)
        button.titleLabel?.textColor = UIColor.white
        return button
    }()
    
    var winPrizeLists:[CRWinningListItem]?{
        didSet{
            prizeListTableView.reloadData()
        }
    }
    
    var profitPrizeLists:[CRWinningListItem]? {
        didSet{
            prizeListTableView.reloadData()
        }
    }

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        //标题
//
//    }
    init(frame: CGRect,winningShow:Bool,profitShow:Bool) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(backView)
        
        self.winningShow = winningShow
        self.profitShow = profitShow
        
        prizeTitleView.addSubview(prizeTitle)
        //关闭
        prizeTitleView.addSubview(closeButton)
        
        backView.addSubview(prizeTitleView)
        backView.addSubview(segment)
        backView.addSubview(prizeListTableView)
        
        backView.snp.makeConstraints { (make) in
//            make.centerY.equalTo(self.snp.centerY)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(360)
            make.height.equalTo(280)
            make.centerY.equalTo(self.snp.centerY)
        }
        
        
        prizeTitleView.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(backView)
            make.height.equalTo(44)
        }
        
        prizeTitle.snp.makeConstraints { (make) in
            make.centerX.equalTo(prizeTitleView.snp.centerX)
            make.centerY.equalTo(prizeTitleView.snp.centerY)
        }
        
        segment.snp.makeConstraints { (make) in
            make.centerX.equalTo(prizeTitleView.snp.centerX)
            make.top.equalTo(prizeTitleView.snp.bottom).offset(0)
            make.height.equalTo((winningShow &&  profitShow) ? 40 : 0)
            make.left.right.equalTo(prizeTitleView)
        }
        
        prizeListTableView.snp.makeConstraints { (make) in
            make.top.equalTo(segment.snp.bottom)
            make.left.bottom.right.equalTo(backView)
        }
        
        closeButton.snp.makeConstraints { (make) in
            make.right.equalTo(prizeTitleView).offset(-10)
            make.centerY.equalTo(prizeTitleView.snp.centerY)
        }
    }
    
    //根据数据刷新视图 2 中奖榜单，4 盈利榜单
    func updateDatas(type:Int,datas:[CRWinningListItem]?) {
        if type == 2 {
            winPrizeLists = datas
        }else if type == 4 {
            profitPrizeLists = datas
        }
    }
    
    @objc func segmentAction(sender:UISegmentedControl) {
        selectIndex = sender.selectedSegmentIndex
    }
    
    ///0 中奖榜单，1 盈利榜单
    func getTypeOfData(index:Int) -> Int {
        if index == 1 {
            return 1
        }else if winningShow && profitShow && index == 0 {
            return 0
        }else if winningShow && !profitShow {
            return 0
        }else if !winningShow && profitShow {
            return 1
        }else {
            return -1 //不存在该情况
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CRWinPrizeListView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let type = getTypeOfData(index: selectIndex)
        if type == 0 {
            print("winPrizeLists.count = \(winPrizeLists?.count)")
            return winPrizeLists?.count ?? 0
        }else if type == 1 {
            print("profitPrizeLists.count = \(profitPrizeLists?.count)")
            return profitPrizeLists?.count ?? 0
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let prizeListCell = tableView.dequeueReusableCell(withIdentifier: "CRWinPrizeListCell") as? CRWinPrizeListCell
        var winPrizeItem = CRWinningListItem()
        
        let type = getTypeOfData(index: selectIndex)
        if type == 0 {
            if let array = winPrizeLists {
                winPrizeItem = array[indexPath.row]
            }
        }else if type == 1 {
            if let array = profitPrizeLists {
                winPrizeItem = array[indexPath.row]
            }
        }
        
        prizeListCell?.prizeDetailItem = winPrizeItem
        if indexPath.row == 0 {
            prizeListCell?.indicatImageV.image = UIImage(named: "chatroom_champion")
            prizeListCell?.indicatImageV.isHidden = false
        }else if indexPath.row == 1{
            prizeListCell?.indicatImageV.image = UIImage(named: "chatroom_silver")
            prizeListCell?.indicatImageV.isHidden = false
        }else if indexPath.row == 2{
            prizeListCell?.indicatImageV.image = UIImage(named: "chatroom_copper")
            prizeListCell?.indicatImageV.isHidden = false
        }else{
            prizeListCell?.indicatImageV.image = UIImage(named: "")
            prizeListCell?.indicatImageV.isHidden = true
        }
        return prizeListCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension CRWinPrizeListView{
    @objc private func closeWinPrizeListView(){
        self.removeFromSuperview()
        
    }
}
