//
//  CRVerticalButton.swift
//  Chatroom
//
//  Created by admin on 2019/7/12.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRVerticalButton: UIButton {
    var imageheightScale:CGFloat = 0.55

//    override var isHighlighted: Bool {
//        didSet {
//            super.isHighlighted = false
//        }
//    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        titleLabel?.textAlignment = NSTextAlignment.center
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let newX:CGFloat = 0.0
        let newY:CGFloat = 5.0
        let newWidth = CGFloat(contentRect.size.width)
        let newHeight = CGFloat(contentRect.size.height) * imageheightScale - newY
        return CGRect(x: newX, y: newY, width: newWidth, height: newHeight)
    }

    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let newX: CGFloat = 0
        let newY = contentRect.size.height * imageheightScale
        let newWidth = contentRect.size.width
        let newHeight = contentRect.size.height - contentRect.size.height * imageheightScale
        return CGRect(x: newX, y: newY, width: newWidth, height: newHeight)
    }

    override func setImage(_ image: UIImage?, for state: UIControl.State) {
        if image == nil {
            imageheightScale = 0
        }else{
            imageheightScale = 0.55
            super.setImage(image, for: state)
        }
    }

}
