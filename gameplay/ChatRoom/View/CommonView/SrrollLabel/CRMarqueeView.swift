//
//  CRMarqueeView.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/2/3.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit

protocol CRMarqueeDelegate {
    func onDelegate()
}

class CRMarqueeView: UIView {
    private var marqueeTitle:String?
    
    var marqueeType: CRJXMarqueeType?
    private let marqueeView = CRJXMarqueeView()
    var delegate:CRMarqueeDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    func setupView(title:String,htmlTxt:Bool = false,txtColor:UIColor = UIColor.darkGray) -> Void {
        var notices = ""

        notices = title
        notices = notices.replacingOccurrences(of: "\n", with: "")
        notices = notices.pregReplace(pattern: "<[^>]*>", with:"")
        notices = notices.pregReplace(pattern: "&nbsp;", with:"")

//        if notices.isEmpty {
//            return
//        }
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = notices
        marqueeView.contentView = label
        marqueeView.backgroundColor = UIColor.clear
        marqueeView.contentMargin = 50
        marqueeView.marqueeType = .left
        
        self.addSubview(marqueeView)

        marqueeView.snp.updateConstraints { (make) in

            make.left.right.top.bottom.equalTo(0)
        }

        let tap = UITapGestureRecognizer.init(target: self, action: #selector(onMarClick))
        self.addGestureRecognizer(tap)
        
    }
    
    @objc func onMarClick() -> Void {
        if let delegate = self.delegate{
            delegate.onDelegate()
        }
    }
}
