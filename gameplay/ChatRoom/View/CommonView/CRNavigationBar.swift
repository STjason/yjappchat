//
//  CRNavigationBar.swift
//  Chatroom
//
//  Created by admin on 2019/7/15.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import PKHUD
import SnapKit

class CRNavigationBar: UIView {
    let newMsgTipIconWH:CGFloat = 10 //右侧 BTN，未读消息的提示红点
    
    var leftTapHandler:(() -> Void)?
    var titleTapHandler:(() -> Void)?
    var rightTapHander:(()->(Void))?
    
    var leftView = UIView()
    var titleView = UIView()
    var rightView = UIView()
    
    lazy var leftButton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "whiteBackArrow"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(leftTapAction), for: .touchUpInside)
        return button
    }()
    
    lazy var titleButton:CRLeftImgButton = {
        let button = CRLeftImgButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        button.addTarget(self, action: #selector(titleTapAction), for: .touchUpInside)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    lazy var titleWithOutImg:UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        button.addTarget(self, action: #selector(titleTapAction), for: .touchUpInside)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    //MARK:-个人中心信息设置
    lazy var rightButton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "menu_white"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(rightTapAction), for: .touchUpInside)
        return button
    }()
    
    lazy var newMsgTipIcon:UIView = {
        let view = UIView()
        view.layer.cornerRadius = newMsgTipIconWH * 0.5
        view.backgroundColor = UIColor.red
        view.isHidden = true
        view.isUserInteractionEnabled = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    func setTitleButtonCanClick(can:Bool,title:String = "") {
        titleWithOutImg.removeFromSuperview()
        titleButton.removeFromSuperview()
        
        let button = can ? titleButton : titleWithOutImg
        button.isUserInteractionEnabled = can
        button.setTitle(title.isEmpty ? "聊天室" : title, for: .normal)
        titleView.addSubview(button)
        
        button.snp.makeConstraints ({[weak self] (make) in
            guard let weakSelf = self else {
                return
            }
            make.center.equalTo(weakSelf.titleView.snp.center)
            make.height.equalTo(30)
            make.width.equalTo(0)
        })
        
        self.titleView.layoutSubviews()
        
        let label = UILabel()
        label.text = button.titleLabel?.text
        label.font = UIFont.systemFont(ofSize: 18)
        let size = label.sizeThatFits(CGSize.init(width: 150, height: CGFloat(MAXFLOAT)))
        
        if can {
            button.setImage(UIImage.init(named: "downWhite"), for: .normal)
        }
        
        button.snp.remakeConstraints ({[weak self] (make) in
            guard let weakSelf = self else {
                return
            }
            
            make.center.equalTo(weakSelf.titleView.snp.center)
            make.height.equalTo(30)
            make.width.equalTo(size.width + CGFloat(can ? 15 : 0))
        })
        
        UIView.animate(withDuration: 0.1) {
            self.titleView.layoutSubviews()
        }
    }
    
    /// 新增右侧按钮更改为玩法选择
    func setRightTitle(title: String) {
        self.rightButton.setTitle(title, for: .normal)
        self.rightButton.setImage(UIImage.init(named: "downWhite"), for: .normal)
    }
    /// 更新彩种名称
    func setLotteryTitle(title: String) {
        self.titleButton.frame = kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 350), height: 30)
        self.titleButton.center = titleView.center
        self.titleView.addSubview(self.titleButton)
        
        self.titleButton.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 35))
        self.titleButton.setImage(UIImage.init(named: "downWhite"), for: .normal)
        
        self.titleButton.layer.cornerRadius = 5
    }
    
    private func setupUI() {
        
        let bgImg = UIImageView.init(image: UIImage.init(named: "chatNavBar"))
        addSubview(bgImg)
        bgImg.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        addSubview(leftView)
        addSubview(titleView)
        addSubview(rightView)
        
        leftView.addSubview(leftButton)
        rightView.addSubview(rightButton)
        rightView.addSubview(newMsgTipIcon)
        
        self.layout()
    }
    
    private func layout() {
        self.leftView.snp.makeConstraints { (make) in
            make.leading.equalTo(20)
            make.width.equalTo(50)
            make.top.equalTo(kStatusHeight)
            if IS_IPHONE_X{
                 make.centerY.equalTo(KNavHeight - Int(kStatusHeight) + 20)
            }else{
                make.centerY.equalTo(KNavHeight - Int(kStatusHeight))
            }
        }
        self.titleView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.leftView.snp.top)
                make.leading.equalTo(weakSelf.leftView.snp.trailing)
                make.bottom.equalTo(weakSelf.leftView.snp.bottom)
            }
        }
        
        self.leftButton.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.centerY.equalTo(weakSelf.leftView.snp.centerY)
                make.leading.equalTo(0)
                make.height.equalTo(35)
                make.width.equalTo(30)
            }
        }
        
        self.rightView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.titleView.snp.top)
                make.leading.equalTo(weakSelf.titleView.snp.trailing)
                make.bottom.equalTo(weakSelf.titleView.snp.bottom)
                make.width.equalTo(50)
            
                make.trailing.equalTo(-20)
            }
        }
        
        self.rightButton.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self{
                make.centerY.equalTo(weakSelf.rightView.snp.centerY)
                make.trailing.equalTo(0)
                make.height.equalTo(40)
                make.width.equalTo(40)
            }
        }
        
        newMsgTipIcon.snp.makeConstraints { (make) in
            make.center.equalTo(rightButton.snp.center)
            make.size.equalTo(CGSize.init(width: newMsgTipIconWH, height: newMsgTipIconWH))
        }
    }
    
    //MARK: - EVENTS
    @objc private func titleTapAction() {
        self.titleTapHandler?()
    }
    
    @objc private func leftTapAction() {
        self.leftTapHandler?()
    }
    @objc private func rightTapAction(){
        self.rightTapHander?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

