//
//  CRReverseButton.swift
//  Chatroom
//
//  Created by admin on 2019/7/16.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRReverseButton: UIButton {

    let imageWidthScale:CGFloat = 0.3
    
    override var isHighlighted: Bool {
        didSet {
            super.isHighlighted = false
        }
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        titleLabel?.textAlignment = NSTextAlignment.left
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let newX = CGFloat(contentRect.size.width) * (1 - imageWidthScale)
        let newWidth = CGFloat(contentRect.size.width) * imageWidthScale
        let newHeight = CGFloat(contentRect.size.height)
        return CGRect(x: newX, y: 0, width: newWidth, height: newHeight)
    }
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let newWidth = contentRect.size.width * (1 - imageWidthScale)
        let newHeight = (contentRect.size.height)
        return CGRect(x: 0, y: 0, width: newWidth, height: newHeight)
    }
   
}
