//
//  CRCustomSpark.swift
//  gameplay
//
//  Created by admin on 2019/10/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRCustomSpark: NSObject {
    
    let sparkSize = CGSize.init(width: 6, height: 6)
    let radius:CGFloat = 180 //烟花爆炸的最大半径
    let minRadius:CGFloat = 20 //烟花爆炸的最小半径
    let sparkCount = 1 //同时出现的烟花的个数
    let sparkAnimateLife:TimeInterval = 1//一个烟花的运动的生命周期
    let sparkShowLife:TimeInterval = 0.7 //一个烟花的显示的生命周期
    let imagesCount:UInt32 = 21 //烟花效果的图片数量
    let sparkFragmentCount = 120
    
    func createSpark(view:UIView) {
        for _ in 0..<sparkCount {
            //获得烟花中心随机点
            let point = getSparkCenterRandomPoint()
            
            explosionView(count: sparkFragmentCount, center: point, radius: radius,minRadius:minRadius, view: view)
            
//            //模拟烟花多次爆炸
//            DispatchQueue.main.asyncAfter(deadline: .now() + sparkAnimateLife) {
//                if index == (self.sparkCount - 1) {
//                    self.createSpark()
//                }
//            }
        }
    }
    
    //MARK: 获得烟花中心的随机点
    private func getSparkCenterRandomPoint() -> CGPoint {
        
        //x > radius && x < screenWidth - radius
        //y > 100 + radius && y < screenHight - tabbarHeight - radius
        
        //烟花中心点出现的范围的，宽和高
//        let sparkRangeWidth = kScreenWidth - CGFloat(2) * radius
//        let sparkRangeHeight = kScreenHeight - CGFloat(2) * radius - CGFloat(KNavHeight) - KTabBarHeight
        
        let sparkRangeWidth = kScreenWidth - CGFloat(1.65) * radius
        let sparkRangeHeight = kScreenHeight - CGFloat(1.65) * radius - KTabBarHeight
        
        //烟花中心点，在屏幕上的坐标范围
//        let sparkOriginX = CGFloat(arc4random() % UInt32(sparkRangeWidth)) + radius
//        let sparkOriginY = CGFloat(arc4random() % UInt32(sparkRangeHeight)) + radius + CGFloat(KNavHeight)
        
        let sparkOriginX = CGFloat(arc4random() % UInt32(sparkRangeWidth)) + radius * CGFloat(0.5)
        let sparkOriginY = CGFloat(arc4random() % UInt32(sparkRangeHeight)) + radius * CGFloat(0.5) + CGFloat(KNavHeight)
        
        let sparkPoint = CGPoint.init(x: sparkOriginX, y: sparkOriginY)
        return sparkPoint
    }
    
    //根据给定的爆炸点的数量，初始化对应个数的视图
    
    /// 初始化烟花爆炸碎片
    ///
    /// - Parameters:
    ///   - count: 碎片个数
    ///   - center: 烟花爆炸中心点
    ///   - radius: 爆炸半径
    private func explosionView(count:Int,center:CGPoint,radius:CGFloat,minRadius:CGFloat,view:UIView) {
        for _ in 0..<count {
            let point = explosionPoint(center: center, radius: radius,minRadius:minRadius)
            let spark = UIImageView()
            spark.frame = CGRect.init(x: center.x, y: center.y, width: sparkSize.width, height: sparkSize.height)
            view.addSubview(spark)
            view.bringSubviewToFront( spark)
            
            //获得随机透明度
            let alphaOriginal = CGFloat(arc4random() % 1 + 9) / CGFloat(10)
            let alphaFinal = CGFloat(arc4random() % 2 + 8) / CGFloat(10)
            
            //获得随机图片
            let imgName = getSparkImage(imgsCount: imagesCount)
            spark.image = UIImage.init(named: imgName)
            spark.alpha = alphaOriginal
            
            UIView.animate(withDuration: sparkAnimateLife) {
                spark.frame = CGRect.init(x: point.x, y: point.y, width: self.sparkSize.width, height: self.sparkSize.height)
                spark.alpha = alphaFinal
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + sparkShowLife) {
                spark.removeFromSuperview()
            }
        }
    }
    
    //获得随机烟花图片
    private func getSparkImage(imgsCount:UInt32) -> String {
        let imageIndex = arc4random() % imgsCount
        let imageName = "spark_\(imageIndex)"
        return imageName
    }
    
    //根据给定的点，获得距离其半径距离的 N个点
    
    /// 爆炸的烟花点
    ///
    /// - Parameters:
    ///   - count: 烟花点数量
    ///   - center: 烟花点zhongxin
    ///   - radius: 爆炸半径
    private func explosionPoint(center:CGPoint,radius:CGFloat,minRadius:CGFloat) -> CGPoint {
        //随机在X或者Y的正半轴还是负半轴
        let PositiveOrNegativeX:CGFloat = arc4random() % 2 == 0 ? -1 : 1
        let PositiveOrNegativeY:CGFloat = arc4random() % 2 == 0 ? -1 : 1
        
        let thisRadius:CGFloat = CGFloat(arc4random() % UInt32(radius - minRadius))  + minRadius
        
        //以烟花中心为 圆心，随机获得 x正负半轴的 x点，再计算对应的 y点
        //烟花点随机范围，- radius --> + radius
        let randomRange = CGFloat(arc4random() % UInt32(thisRadius)) * PositiveOrNegativeX
        
        //距离中心点的随机点的 X轴的坐标
        let pointX = center.x + randomRange
        
        //根据勾股定理，求出爆炸点的Y相对的烟花中心的Y值
        let sqrth = CGFloat(sqrtf(Float(thisRadius*thisRadius - randomRange*randomRange)))
        let pointY = center.y + CGFloat(sqrth) * PositiveOrNegativeY
        let point = CGPoint.init(x: pointX, y: pointY)
        return point
    }
}
