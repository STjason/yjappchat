//
//  CRMoreRoomCell.swift
//  gameplay
//
//  Created by admin on 2019/10/13.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMoreRoomCell: UITableViewCell {
    
    
    @IBOutlet weak var roomBgImg: UIImageView!
    @IBOutlet weak var infoBgView: UIView!
    @IBOutlet weak var lockImg: UIImageView!
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var roomDesLabel: UILabel!
    @IBOutlet weak var personCountLabel: UILabel!
    
    @IBOutlet weak var peopleIndicateImageV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.infoBgView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.infoBgView.layer.cornerRadius = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    ///colorType 判断房间cell背景图片
    func configWith(model:CRRoomListModel,colorType:Int,userType:Int) {
        self.roomNameLabel.text = model.name
        self.roomDesLabel.text = model.remark
        
        let  chatRoomConfig = getChatRoomSystemConfigFromJson()
        if chatRoomConfig?.source?.switch_room_people_admin_show == "1"{
            //在线管理员可看到在线人数开关 1.开 0关闭
            if [2,4].contains(userType) {
                //只有管理员账户可以看到在线人数
                self.peopleIndicateImageV.isHidden = false
                self.personCountLabel.text = "\(model.roomUserCount + (chatRoomConfig?.source?.name_room_people_num ?? 0))人"
            }else{
//                self.peopleIndicateImageV.isHidden = true
                self.personCountLabel.text = ""
            }
        }else{
            self.peopleIndicateImageV.isHidden = false
            var onLines = model.roomUserCount + (chatRoomConfig?.source?.name_room_people_num ?? 0)
            if onLines < 10{
                onLines = 10
            }
            self.personCountLabel.text = "\(onLines)人"
        }
        //先隐藏房间在线人数
//        self.peopleIndicateImageV.isHidden = true
//        self.personCountLabel.text = ""
        
        self.lockImg.isHidden = model.roomKey.length == 0
        
        var imgName = model.roomImg
        if imgName.isEmpty{
            if  colorType == 0 { //紫
                imgName = "chatEntryPurple"
            }else if colorType == 1 { // 蓝
                imgName = "chatEntryBlue"
            }else if colorType == 2 { //红
                imgName = "chatEntryPink"
            }
            roomBgImg.image = UIImage(named: imgName)
        }else{
            if imgName.starts(with: "http") || imgName.starts(with: "https://"){
                roomBgImg.kf.setImage(with: URL(string: imgName), placeholder: UIImage(named: imgName), options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                imgName = "chatEntryPurple"
                roomBgImg.image = UIImage(named: imgName)
            }
        }
    }
}
