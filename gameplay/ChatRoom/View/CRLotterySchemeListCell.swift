//
//  CRLotterySchemeListCell.swift
//  gameplay
//
//  Created by JK on 2019/12/3.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CRLotterySchemeListCell: UITableViewCell {
    
    /// 期数Label
    lazy var periodLabel: UILabel = {
        let label = UILabel(frame: kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 385), height: kCurrentScreen(x: 125)))
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        
        return label
    }()
    /// 开奖结果Label
    lazy var resultLabel: UIButton = {
        let label = UIButton(type: .custom)
        label.frame = kCGRect(x:periodLabel.frame.maxX, y: 0, width: kCurrentScreen(x: 280), height: kCurrentScreen(x: 125))
        label.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        
        return label
    }()
    /// 购彩计划Label
    lazy var lotterySchemeLabel: UIButton = {
        let label = UIButton(type: .custom)
        label.frame = kCGRect(x:resultLabel.frame.maxX, y: 0, width: kCurrentScreen(x: 280), height: kCurrentScreen(x: 125))
        label.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        
        return label
    }()
    /// 开奖时间Label
    lazy var lotteryTimeLabel: UILabel = {
        let label = UILabel(frame: kCGRect(x: lotterySchemeLabel.frame.maxX, y: 0, width: kCurrentScreen(x: 285), height: kCurrentScreen(x: 125)))
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        
        return label
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(periodLabel)
        addSubview(resultLabel)
        addSubview(lotterySchemeLabel)
        addSubview(lotteryTimeLabel)
        
        self.createLine()
    }
    
    func createLine() {
        for index in 0..<3 {
            let line = UIView()
            line.frame = kCGRect(x: kCurrentScreen(x: 385) + CGFloat(index) * kCurrentScreen(x: 280), y: 0, width: 0.5, height: kCurrentScreen(x: 125))
            line.backgroundColor = UIColor.lightGray
            
            addSubview(line)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getCurrentModel(model: CRResultListModel) {
        periodLabel.text            = model.lotteryNum.subString(start: 4, length: model.lotteryNum.count - 4)
        
        resultLabel.setTitleColor(model.lotteryResult.isEmpty ? UIColor.red : UIColor.black, for: .normal)
        resultLabel.setTitle(model.lotteryResult.isEmpty ? "待开" : model.lotteryResult, for: .normal)
        resultLabel.setBackgroundImage(model.lotteryResult == model.forecast ? UIImage(named: "activityResource.bundle/lotteryScheme_winPrize_bg") : UIImage(named: ""), for: .normal)
        
        lotterySchemeLabel.setTitleColor(UIColor.black, for: .normal)
        lotterySchemeLabel.setTitle(model.forecast, for: .normal)
        lotterySchemeLabel.setBackgroundImage(model.lotteryResult == model.forecast ? UIImage(named: "activityResource.bundle/lotteryScheme_winPrize_bg") : UIImage(named: ""), for: .normal)
        
        lotteryTimeLabel.text       = model.resultDate
    }
    
}
