//
//  CRExpressionItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/15.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRExpressionItem: HandyJSON {

    var type:CREmojiType = CREmojiType.Face
    /**表情ID*/
    var eid:String = ""
    //表情包ID
    var gid :String = ""
    /**表情名字*/
    var name:String = ""
    /**表情大小*/
    var size:CGSize = CGSize.zero
    
    var count:Int = 0
    
    required init() {}
    
}
