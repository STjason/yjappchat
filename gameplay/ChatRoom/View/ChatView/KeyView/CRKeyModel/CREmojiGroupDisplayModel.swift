//
//  CREmojiGroupDisplayModel.swift
//  gameplay
//
//  Created by Gallen on 2019/9/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CREmojiGroupDisplayModel: NSObject {

    var emojiType:CREmojiType = CREmojiType.Emoji
    var groudId:String = ""
    var groupName:String = ""
    var count:Int = 0
    var data:[Any] = []
    var emojiGroupIndex:Int = 0
    var pageIndex:Int = 0
    /**每页个数*/
    var pageItemCount:Int = 0
    /**页数*/
    var pageNumber:Int = 0
    /**行数*/
    var rowNumeber:Int = 0
    /**列数*/
    var colNumber:Int = 0
    var cellSize:CGSize = CGSize.zero
    var selectionInset:UIEdgeInsets = UIEdgeInsets.zero
   
     init(emojiGroup:CRExpressionGroupModel,pageNumber:Int,count:Int) {
        super.init()
        self.groudId = emojiGroup.gId
        self.groupName = emojiGroup.name
        self.emojiType = emojiGroup.emojiType
        
        self.rowNumeber = emojiGroup.rowNumber
        self.colNumber = emojiGroup.colNumber
        self.pageItemCount = emojiGroup.pageItemCount
        
        let  startIndex = pageNumber * count;
        let dataCount = emojiGroup.data?.count ?? 0
        if (dataCount > startIndex) {
            let length = min(count, dataCount - startIndex)
            for (index, value) in (emojiGroup.data?.enumerated())!{
                if index >= startIndex && index < (startIndex+length){
                    self.data.append(value)
                }
                if index >= (startIndex + length){
                    break
                }
            }
        }
    }
  
    func insetAtIndex(index:Int) -> Any{
        
        return index < self.data.count ? self.data[index] : nil
    }
    func addEmoji(emojiItem:CRExpressionItem){
        var emojis = self.data
        emojis.append(emojiItem)
        self.data = emojis;
    }
}
