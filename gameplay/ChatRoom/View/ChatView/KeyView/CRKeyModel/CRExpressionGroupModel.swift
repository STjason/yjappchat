//
//  CRExpressionGroupModel.swift
//  gameplay
//
//  Created by Gallen on 2019/9/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRExpressionGroupModel: NSObject {
    
    var gId:String = ""
    var name:String = ""
    var detail:String = ""
    private var _iconPath:String?
    var iconPath:String{
        get{
            return String.init(format:"%icon_%@",path!,gId)
        }
        set{
            _iconPath = newValue
        }
    }
    
    var path:String?{
        get{
            return FileManager.pathExpressionForGroupID(groudId: self.gId)
        }
    }
    var bannerId:String = ""
    /**表情总数*/
    var picCount:Int = 0
    var data:[Any]?{
        didSet{
            self.picCount = data?.count ?? 0
        }
    }
    var emojiType:CREmojiType = CREmojiType.Emoji
    
    var rowNumber:Int{
        get{
            if emojiType == CREmojiType.Face || emojiType == CREmojiType.Emoji {
                return 3
            }
            return 2
        }
    }
    
    var colNumber:Int{
        get{
            if (emojiType == CREmojiType.Face || emojiType == CREmojiType.Emoji) {
                return 7;
            }
            return 4;
        }
    }
    
    var pageItemCount:Int{
        get{
            if (emojiType == CREmojiType.Face || emojiType == CREmojiType.Emoji) {
                
                return rowNumber * colNumber - 1;
            }
            return rowNumber * colNumber;
        }
    }
    
    var pageNumber:Int{
        get{
           return picCount / pageItemCount + (picCount % pageItemCount == 0 ? 0 : 1);
        }
    }

}
extension CRExpressionGroupModel{
    
    func updateExpressItemGid(){
        for (index,vaule) in (self.data?.enumerated())!{
            (vaule as! CRExpressionItem).gid = self.gId
        }
    }
    
    
}
