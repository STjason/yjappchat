//
//  CRMoreKeyboardItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

enum CRMoreKeyboardItemType {
    case Image
    case Camera
    case Video
    case RedPacket
}
class CRMoreKeyboardItem: NSObject {
    var type:CRMoreKeyboardItemType = .Image
    var title:String = ""
    var imagePath: String = ""
    
    class func createByType(type:CRMoreKeyboardItemType,title:String,imagePath:String) -> CRMoreKeyboardItem{
        let item =  CRMoreKeyboardItem()
        item.type = type
        item.title = title
        item.imagePath = imagePath
        return item
    }
}
