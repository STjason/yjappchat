//
//  CREmojiKeyboard.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CREmojiKeyboard: CRBaseKeyboard {
    
    weak var emojiKeybaordDelegate:CREmojiKeyboardDelegate?
    
    var curGroup:CRExpressionGroupModel?
    
    static let emojiKeyboard:CREmojiKeyboard = CREmojiKeyboard()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.colorWithRGB(r: 245.0, g: 245.0, b: 247.0, alpha: 1.0)
        addSubview(displayView)
        addSubview(pageControl)
        addLoyoutConstraint()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var displayView:CREmojiGroupDisplayView = {
        let view = CREmojiGroupDisplayView()
        view.emojiDisplayDelegate = self
        return view
    }()
    
    lazy var pageControl:UIPageControl = {
        let pageCtrl = UIPageControl()
        pageCtrl.centerX = self.centerX
        pageCtrl.currentPage = 0
        pageCtrl.pageIndicatorTintColor = UIColor(white: 0.5, alpha: 0.3)
        pageCtrl.currentPageIndicatorTintColor = UIColor.gray
        pageCtrl.addTarget(self, action: #selector(pageControlChanged(page:)), for: UIControl.Event.valueChanged)
        return pageCtrl
    }()
    
    var emojiGroupData:[Any]?{
        didSet{
            self.displayView.data = emojiGroupData ?? []
        }
    }
    //添加约束
    func addLoyoutConstraint(){

        self.pageControl.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.height.equalTo(20)
            make.bottom.equalTo(self)
        }
        self.displayView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self)
            make.bottom.equalTo(self.pageControl.snp.top).offset(-5)
        }
    }
    @objc func pageControlChanged(page:UIPageControl){
        //page 宽
        let pageScrollWidth = displayView.emojiCollectionView.size.width
        let pageScrollHeight = displayView.emojiCollectionView.size.height
        let pageCurrentX = pageScrollWidth * CGFloat(pageControl.currentPage)
        displayView.emojiCollectionView.scrollRectToVisible(CGRect(x: pageCurrentX, y: 0, width: pageScrollWidth, height: pageScrollHeight), animated: true)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        // 顶部直线
        let context = UIGraphicsGetCurrentContext();
        context?.setLineWidth(0.5);
        context?.setStrokeColor(UIColor.gray.cgColor);
        context?.beginPath();
        context?.move(to: CGPoint(x: 0, y: 0))
        context?.addLine(to: CGPoint(x: kScreenWidth, y: 0))
        context?.strokePath();
    }
}
extension CREmojiKeyboard:CREmojiGroupDisplayViewDelegate{
    //删除
    func emojiGroupDisplayViewDeleteButtonPressed(displayView: CREmojiGroupDisplayView) {
        self.emojiKeybaordDelegate?.emjiKeyboardDeleteButtonDonw()
    }
    //选中
    func emojiGroupDisplayViewDidClickedEmoji(displayView: CREmojiGroupDisplayView, emojiItem: CRExpressionItem) {
        if let delegate  = self.emojiKeybaordDelegate {
            delegate.emojiKeyboardDidSelectedEmojiItem(emojiKeyboard: self, emojiItem: emojiItem)
        }
    }
    
    func emojiGroupDisplayViewDidScrollToPageIndexForGroupIndex(displayView: CREmojiGroupDisplayView, pageIndex: Int, groudIndex: Int) {
        let groupItem = self.emojiGroupData?[groudIndex] as? CRExpressionGroupModel;
        if (curGroup != groupItem) {
            curGroup = groupItem;
            pageControl.isHidden = (groupItem?.pageNumber) ?? 0 <= 1
            pageControl.numberOfPages = (groupItem?.pageNumber) ?? 0
            emojiKeybaordDelegate?.emojiKeyboardSelectedEmojiGroupType(emojiKeyboard: self, type: ((groupItem?.emojiType) ?? CREmojiType(rawValue: 0))!)
        }
        pageControl.currentPage = pageIndex

    }
    
    func emojiGroupDisplayViewDidLongPressEmojiAtRect(displayView: CREmojiGroupDisplayView, emojiItem: CRExpressionItem, rect: CGRect) {
        
    }
    
    func emojiGroupDisplayViewDidStopLongPressEmoji(displayView: CREmojiGroupDisplayView) {
        
    }
    
    
}
