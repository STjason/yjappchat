//
//  CRMoreKeyboardHelper.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMoreKeyboardHelper: NSObject {
    
    var chatMoreKeyboardData:[Any]?
    override init() {
        super.init()
        chatMoreKeyboardData = Array()
        getData()
        
    }
    
    func getData(){
      let  imageItem = CRMoreKeyboardItem.createByType(type: CRMoreKeyboardItemType.Image, title: "照片", imagePath: "moreKB_image")
        let cameraItem = CRMoreKeyboardItem.createByType(type: .Video, title: "视频", imagePath: "moreKB_sight")
        let redPacketItem = CRMoreKeyboardItem.createByType(type: .RedPacket, title: "红包", imagePath: "moreKB_wallet")
        
        self.chatMoreKeyboardData?.append(imageItem)
        self.chatMoreKeyboardData?.append(cameraItem)
        self.chatMoreKeyboardData?.append(redPacketItem)
    }
}
