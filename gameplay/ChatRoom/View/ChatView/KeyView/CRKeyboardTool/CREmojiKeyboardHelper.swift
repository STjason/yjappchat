//
//  CREmojiKeyboardHelper.swift
//  gameplay
//
//  Created by Gallen on 2019/9/13.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CREmojiKeyboardHelper: NSObject {
    
    static let sharedKBHelper:CREmojiKeyboardHelper = CREmojiKeyboardHelper()
    
    var complete:(([Any])->())?
    
    var userId:String = ""
    
    func emojiGroupDataByUserID(userID:String,complete:(([Any])->())?){
        userId = userID
        self.complete = complete
        var emojiGroupData:[Any] = []
//        emojiGroupData.append(CRExpressionHelper.sharedHelper.defaultSystemEmojiGroup)
        emojiGroupData.append(CRExpressionHelper.sharedHelper.defaultFaceGroup)
        complete!(emojiGroupData)
       
    }
}
