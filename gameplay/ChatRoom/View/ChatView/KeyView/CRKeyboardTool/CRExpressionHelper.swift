//
//  CRExpressionHelper.swift
//  gameplay
//
//  Created by Gallen on 2019/9/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRExpressionHelper: NSObject {
    
    static let sharedHelper:CRExpressionHelper = CRExpressionHelper()
    
    lazy var defaultFaceGroup:CRExpressionGroupModel = {
        let defalutFace = CRExpressionGroupModel()
        defalutFace.emojiType = CREmojiType.Face
        defalutFace.iconPath = "emojiKB_group_face"
        let path = Bundle.main.path(forResource:"Emoji", ofType: "json")
        let data =  NSData(contentsOfFile: path!)
        let str = String.init(data: data! as Data, encoding: String.Encoding.utf8)
        let models:[CRExpressionItem] = JSONDeserializer<CRExpressionItem>.deserializeModelArrayFrom(json: str)! as! [CRExpressionItem]
        defalutFace.data = models
        for emojiItem in defalutFace.data! {
            (emojiItem as? CRExpressionItem)?.type = CREmojiType.Face
        }
        return defalutFace
    }()
    
    lazy var defaultSystemEmojiGroup:CRExpressionGroupModel = {
        let emojiItem = CRExpressionGroupModel()
        emojiItem.emojiType = CREmojiType.Emoji
        emojiItem.iconPath = "emojiKB_group_face"
        let path = Bundle.main.path(forResource: "SystemEmoji", ofType: "json")
        let data =  NSData(contentsOfFile: path!)
         let str = String.init(data: data! as Data, encoding: String.Encoding.utf8)
        let models:[CRExpressionItem] = JSONDeserializer<CRExpressionItem>.deserializeModelArrayFrom(json: str)! as! [CRExpressionItem]
        emojiItem.data = models
        return emojiItem
    }()
        

    
}
