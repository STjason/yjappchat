//
//  CRMoreKeyboard.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMoreKeyboard: CRBaseKeyboard,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    weak var delegate : CRMoreKeyboardDelegate?
    
    static let shareMoreKeyboard:CRMoreKeyboard = CRMoreKeyboard()
    
    var pageItemCount:Int{
        get{
            return 3
        }
    }
    
    var chatMoreKeyboardData:[Any]?
    
    var collectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectView.backgroundColor = UIColor.clear
        collectView.isPagingEnabled = true
        collectView.showsHorizontalScrollIndicator = false
        collectView.showsVerticalScrollIndicator = false
        collectView.scrollsToTop = false
        return collectView
    }()

    
    var pageControl:UIPageControl = {
        let pageControl = UIPageControl()
       
        return pageControl
    }()
    
    static let keyboard:CRMoreKeyboard = CRMoreKeyboard()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.colorWithRGB(r: 245.0, g: 245.0, b: 247.0, alpha: 1.0)
        addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CRMoreKeyboardCell.self, forCellWithReuseIdentifier: "CRMoreKeyboardCell")
    }
    
    func setChatMoreKeyboardData(chatMoreKeyboardData:[Any]){
        collectionView.reloadData()
        let pageNumber = chatMoreKeyboardData.count / self.pageItemCount + (chatMoreKeyboardData.count % self.pageItemCount == 0 ? 0 : 1);
    }
    
    override func reset() {
        collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: collectionView.width, height: collectionView.height), animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.chatMoreKeyboardData?.count ?? 0
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.pageItemCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let moreKeyboardCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CRMoreKeyboardCell", for: indexPath) as? CRMoreKeyboardCell
        let index = indexPath.section * self.pageItemCount + indexPath.row
        let tempIndex = p_transformIndex(index: index)
        if tempIndex > (self.chatMoreKeyboardData?.count ?? 0) {
            moreKeyboardCell?.item = nil
        }else {
            moreKeyboardCell?.item = self.chatMoreKeyboardData?[tempIndex] as? CRMoreKeyboardItem
        }
        
        weak var weakSelf = self
        moreKeyboardCell?.clickClosed = { item in
            
            weakSelf?.delegate?.moreKeyboardDidSelectedFunctionItem!(keyboard: weakSelf as Any, funcItem: item)
        }
        return moreKeyboardCell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return  CGSize(width: 60, height: (collectionView.frame.size.height - 15) / 2 * 0.93)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return CGFloat((Double(collectionView.frame.size.width) - Double(60) * Double(self.pageItemCount) * 0.5)) / CGFloat(Double(self.pageItemCount) * 0.5 + 1)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return (collectionView.height - 15) * 0.5 * 0.07;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let space = (Double(collectionView.width) - Double(60) * Double(self.pageItemCount)  * 0.5) / (Double(self.pageItemCount) * 0.5 + 1);
        return UIEdgeInsets(top: 15, left: CGFloat(space), bottom: 0, right: CGFloat(space));
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func p_transformIndex(index:Int) -> Int{
        var indexs = index
        let page = indexs / self.pageItemCount
        indexs = index % pageItemCount
        let x = indexs / 2
        let y = indexs % 2
        return self.pageItemCount / 2 * y + x + page * self.pageItemCount
    }

}

