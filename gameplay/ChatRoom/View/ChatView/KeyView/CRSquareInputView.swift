//
//  CRSquareInputView.swift
//  Chatroom
//
//  Created by admin on 2019/7/18.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRSquareInputView: UIControl,UIKeyInput {
    var textField:UITextField = UITextField()
    var inputs = "" //输入的文本
    var maxLength = 0
    let lines = 2
    var gap:CGFloat = 0 //格子的宽
    var squareHeight:CGFloat = 40 //格子的高
    var lineColor = UIColor.lightGray
    var bgColor = UIColor.white
    
    var textChangeHandler:((_ input:String) -> Void)?
    
    var hasText: Bool {
        return inputs.count > 0
    }
    
    func getSize() -> CGSize {
        return CGSize.init(width: gap * CGFloat(self.maxLength), height: squareHeight)
    }
    
    convenience init(contents:String,gap:CGFloat = 40,width:CGFloat) {
        self.init()
        self.backgroundColor = self.bgColor
        self.maxLength = contents.count
        
        //输入框距离视图左右的间距
        let horSpacing:CGFloat = 20
        self.gap = (width - CGFloat(2) * horSpacing) / CGFloat(self.maxLength)
        
        self.frame = CGRect.init(x: 0, y: 10, width: gap * CGFloat(self.maxLength), height: squareHeight)
        
        textField.frame = CGRect.init(x: 0, y: 10, width: 0.1, height: 0.1)
        textField.font = UIFont.systemFont(ofSize: 1)
        textField.textColor = UIColor.clear
        textField.backgroundColor = UIColor.clear
        textField.delegate = self
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        
        let bgView = UIView()
        bgView.backgroundColor = UIColor.white
        bgView.frame = textField.bounds
        textField.addSubview(bgView)
        
        self.addSubview(textField)
        
        self.addTarget(self, action: #selector(tapSelf), for: .touchUpInside)
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        let width = CGFloat(self.gap) //每一个小格子的宽度
        context.setStrokeColor(lineColor.cgColor)
        context.setLineWidth(1)
        //外边框
        context.stroke(rect)
        let path = UIBezierPath()
        //画中间分隔的竖线
        (1..<maxLength).forEach { (index) in
            path.move(to: CGPoint(x: rect.origin.x + CGFloat(index) * self.gap, y: rect.origin.y))
            path.addLine(to: CGPoint(x: rect.origin.x + CGFloat(index) * self.gap, y: rect.origin.y + self.squareHeight))
        }
        context.addPath(path.cgPath)
        context.strokePath()
        //画圓点
        let pointSize = CGSize(width: squareHeight * 0.3, height: squareHeight * 0.3)
        (0..<self.inputs.count).forEach { (index) in
            let origin = CGPoint(x: rect.origin.x + CGFloat(index) * width + (width - pointSize.width) / 2, y: rect.origin.y + (rect.height - pointSize.height) / 2)
            let pointRect = CGRect(origin: origin, size: pointSize)
            context.fillEllipse(in: pointRect)
        }
    }
    
    func insertText(_ text: String) {
        if self.inputs.count < maxLength {
            self.inputs += text
            self.textChangeHandler?(self.inputs)
            setNeedsDisplay()
        }
    }
    
    func deleteBackward() {
        if self.inputs.count > 0 {
            self.inputs = String(self.inputs.dropLast())
            self.textChangeHandler?(self.inputs)
            setNeedsDisplay()
        }
    }
    

    //键盘的样式 (UITextInputTraits中的属性)
    var keyboardType: UIKeyboardType {
        get{
            return .default
        } set{
            
        }
    }
    
    @objc func tapSelf() {
        if !textField.isFirstResponder {
            textField.becomeFirstResponder()
        }
    }
}

extension CRSquareInputView:UITextFieldDelegate {
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let shouldReturn = textField.text?.count ?? 0 == maxLength ? false : true
        
        print("shouldChangeCharactersIn string:\(string)")
        print("shouldChangeCharactersIn field.text: \(textField.text ?? "")")
        
        if range.length == 1 { // 删
            deleteBackward()
        }else if range.length == 0 && shouldReturn { //增
            insertText(string)
        }
        
        return textField.text?.count ?? 0 > maxLength ? false : true
    }
}





