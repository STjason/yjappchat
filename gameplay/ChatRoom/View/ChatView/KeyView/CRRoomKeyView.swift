//
//  CRRoomKeyView.swift
//  Chatroom
//
//  Created by admin on 2019/7/18.
//  Copyright © 2019年 yun. All rights reserved.
//  输入房间密码View

import UIKit
import SnapKit

class CRRoomKeyView: UIView {
   
    @IBOutlet weak var inputArea: UIView!
    @IBOutlet weak var commitButton: UIButton!
    @IBOutlet weak var keyTipsLabel: UILabel!
    
    var isShow = false
    var inputSqure:CRSquareInputView?
    var password = ""
    var inputs = ""
    var confirmHandler:((_ inputs:String) -> Void)?
    var closeHandler:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5.0
        self.commitButton.layer.cornerRadius = 3.0
    }
        
    func configWith(contents:String,width:CGFloat) {
        if contents.count > 0 {
            self.password = contents
            
            let inputSqure = CRSquareInputView.init(contents: contents,width:width)
            self.inputArea.addSubview(inputSqure)
            
            let inputSqureSize = inputSqure.getSize()
            
            inputSqure.snp.makeConstraints {[weak self] (make) in
                if let weakSelf = self {
                    make.width.equalTo(inputSqureSize.width)
                    make.height.equalTo(inputSqureSize.height)
                    make.center.equalTo(weakSelf.inputArea.snp.center)
                }
            }
            
            self.inputSqure = inputSqure
            
            self.inputSqure?.textChangeHandler = {[weak self] (contents) in
                if let weakSelf = self {
                    weakSelf.inputs = contents
                    
                    if contents.length == weakSelf.password.length {
                        weakSelf.confirmHandler?(contents)
                    }
                }
            }
        }
    }
    
    func updateRoomkeyTips(roomName:String) {
        keyTipsLabel.text = String(format:"请输入\"%@\"密码",roomName)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.closeHandler?()
    }
    
    @IBAction func commitAction(_ sender: Any) {
        self.confirmHandler?(self.inputs)
    }
    
}
