//
//  CRBaseKeyboard.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBaseKeyboard: UIView ,CRKeyboardProtocol {
    func keyboardHeight() -> CGFloat {
        return 215
    }
    /**是否正在展示*/
    var isShow:Bool = false
    /**键盘事件回调*/
    weak var keyboardDelegate:CRKeyboardDelegate?
    /**是否显示动画*/
    func showWithAnimation(animation:Bool){
        
        showInView(view: UIApplication.shared.keyWindow!, animation: animation)
    }
    //父视图
    func showInView(view:UIView,animation:Bool){
        if (isShow) {
            return;
        }
        isShow = true
        if let delegate = self.keyboardDelegate {
            delegate.chatKeyboardWillShow?(keyboard: self, animation: animation)
        }
        
        view.addSubview(self)
        let keyboardHeight = self.keyboardHeight()
        self.snp.remakeConstraints { (make) in
            make.left.right.equalTo(view)
            make.height.equalTo(keyboardHeight)
            make.bottom.equalTo(view).offset(keyboardHeight)
        }
        view.layoutIfNeeded()

        if (animation) {
            UIView.animate(withDuration: 0.3, animations: {
                self.snp.makeConstraints({ (make) in
                    make.bottom.equalTo(view)
                })
                self.superview?.layoutIfNeeded()
                let keyboardY = self.y
                let height = (self.superview?.height ?? 0) - keyboardY
                self.keyboardDelegate?.chatKeyboardDidChangeHeight?(keyboard: self, height: height)
            }) { (finished:Bool) in
                
                self.keyboardDelegate?.chatKeyboardDidShow?(keyboard: self, animation: animation)
            }
        }
        else {
            self.snp.makeConstraints { (make) in
                make.bottom.equalTo(view)
            }
            view.layoutIfNeeded()
            self.keyboardDelegate?.chatKeyboardDidShow?(keyboard: self, animation: animation)
        }
    }
    /**是否显示消失动画*/
    func dismissWithAnimation(animation:Bool){
        if !isShow {
            if !animation{
                self.removeFromSuperview()
            }
            return
        }
        isShow = false
        if let delegate = self.keyboardDelegate {
            delegate.chatKeyboardWillDismiss?(keyboard: self, animation: animation)
        }

        if (animation) {
            
            let keyboardHeight = self.keyboardHeight();
            UIView.animate(withDuration: 0.3, animations: {
                self.snp.makeConstraints({ (make) in
//                    make.bottom.equalTo((self.superview)!).offset(keyboardHeight)
                    make.height.equalTo(KTabBarHeight)
                    
                })
            
                self.superview?.layoutIfNeeded()
                let keyboardY = self.y
                let height = (self.superview?.height ?? 0) - keyboardY
                self.keyboardDelegate?.chatKeyboardDidChangeHeight!(keyboard: self, height: height)
            }) { (finished:Bool) in
                self.removeFromSuperview()
                self.keyboardDelegate?.chatKeyboardDidDismiss!(keyboard: self, animation: animation)
            }
        }
        else {
            self.removeFromSuperview()
            self.keyboardDelegate?.chatKeyboardDidDismiss!(keyboard: self, animation: animation)
        }
    }
    /**
     *  重置键盘
     */
    func reset(){
        
    }

}
