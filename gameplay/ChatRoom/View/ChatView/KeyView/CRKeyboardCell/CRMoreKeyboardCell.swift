//
//  CRMoreKeyboardCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMoreKeyboardCell: UICollectionViewCell {
    
    var item :CRMoreKeyboardItem?{
        didSet{
            if item == nil {
                titleLabel.isHidden = false
                iconButton.isHidden = false
                isUserInteractionEnabled = false
                return
            }else{
                isUserInteractionEnabled = true
                titleLabel.isHidden = true
                iconButton.isHidden = true
                titleLabel.text = item?.title
                iconButton.setImage(UIImage(named: item?.imagePath ?? ""), for: .normal)
            }
        }
    }
    
    var clickClosed : ((_ item:CRMoreKeyboardItem)->(Void))?
    
    lazy var iconButton : UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
//        button.setBackgroundImage(<#T##image: UIImage?##UIImage?#>, for: <#T##UIControlState#>)
        button.addTarget(self, action: #selector(chooseItem), for: .touchUpInside)
        return button
    }()
    
    lazy var titleLabel:UILabel = {
        let lable = UILabel()
        lable.font = UIFont.systemFont(ofSize: 12)
        lable.textColor = UIColor.gray
        return lable
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(iconButton)
        contentView.addSubview(titleLabel)
        
        iconButton.snp.makeConstraints { (make) in
            make.top.equalTo(contentView)
            make.centerX.equalTo(contentView)
            make.width.equalTo(contentView)
            make.height.equalTo(iconButton.snp.width)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func chooseItem(){
        self.clickClosed?(self.item!)
    }
}
