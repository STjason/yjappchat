//
//  CREmojiGroupDisplayViewDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol  CREmojiGroupDisplayViewDelegate: NSObjectProtocol {
    /**
     *  发送按钮点击事件
     */
    func emojiGroupDisplayViewDeleteButtonPressed(displayView:CREmojiGroupDisplayView)
    /**
     *  选中表情
     */
    func emojiGroupDisplayViewDidClickedEmoji(displayView:CREmojiGroupDisplayView,emojiItem:CRExpressionItem)
    /**
     *  翻页
     */
    func emojiGroupDisplayViewDidScrollToPageIndexForGroupIndex(displayView:CREmojiGroupDisplayView,pageIndex:Int,groudIndex:Int)
    
    /**
     *  表情长按
     */
    func emojiGroupDisplayViewDidLongPressEmojiAtRect(displayView:CREmojiGroupDisplayView,emojiItem:CRExpressionItem,rect:CGRect)
    
    /**
     *  停止表情长按
     */
    func emojiGroupDisplayViewDidStopLongPressEmoji(displayView:CREmojiGroupDisplayView)
}
