//
//  CREmojiKeyboardDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/15.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

 protocol CREmojiKeyboardDelegate : NSObjectProtocol{
    
     func chatInputViewHasText() -> Bool
    /**
     *  长按某个表情（展示）
     */
      func emojiKeyboardDidTouchEmojiItemAtRect(emojiKeyboard:CREmojiKeyboard,emojiItem:CRExpressionItem,rect:CGRect)
    /**
     *  取消长按某个表情（停止展示）
     */
      func emojiKeyboardCancelTouchEmojiItem(emojikeybarod:CREmojiKeyboard)
    /**
     *  点击事件 —— 选中某个表情
     */
      func emojiKeyboardDidSelectedEmojiItem(emojiKeyboard:CREmojiKeyboard,emojiItem:CRExpressionItem)
    /**
     *  点击事件 —— 表情发送按钮
     */
      func emojiKeyboardSendButtonDown()
    /**
     *  点击事件 —— 删除按钮
     */
      func emjiKeyboardDeleteButtonDonw()
    /**
     *  点击事件 —— 表情编辑按钮
     */
      func emojiKeyboardEmojiEditButtonDown()
    /**
     *  点击事件 —— 我的表情按钮
     */
      func emojiKeyboardMyEmojiEditButtonDown()
    /**
     *  选中不同类型的表情组回调
     *  用于改变chatBar状态（个性表情组展示时textView不可用）
     */
      func emojiKeyboardSelectedEmojiGroupType(emojiKeyboard:CREmojiKeyboard , type : CREmojiType)
}
