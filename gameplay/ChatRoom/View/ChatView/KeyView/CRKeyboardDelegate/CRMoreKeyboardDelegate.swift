//
//  CRMoreKeyboardDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

@objc protocol CRMoreKeyboardDelegate : NSObjectProtocol {
    
   @objc optional func moreKeyboardDidSelectedFunctionItem(keyboard : Any,funcItem:CRMoreKeyboardItem)
}
