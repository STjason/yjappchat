//
//  CRKeyboardProtocol.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol CRKeyboardProtocol : NSObjectProtocol{
    
   func keyboardHeight() ->CGFloat
}
