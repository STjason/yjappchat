//
//  CRKeyboardDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

@objc protocol CRKeyboardDelegate : NSObjectProtocol {
   
    @objc optional func chatKeyboardWillShow(keyboard : Any,animation : Bool)
    
    @objc optional func chatKeyboardDidShow(keyboard : Any ,animation : Bool)
    
    @objc optional func chatKeyboardWillDismiss(keyboard : Any ,animation : Bool)
    
    @objc optional func chatKeyboardDidDismiss(keyboard : Any ,animation :Bool)
    
    @objc optional func chatKeyboardDidChangeHeight(keyboard : Any ,height:CGFloat)
    
}
