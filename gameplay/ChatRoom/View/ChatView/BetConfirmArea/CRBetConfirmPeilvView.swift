//
//  CRBetConfirmPeilvView.swift
//  gameplay
//
//  Created by admin on 2019/9/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetConfirmPeilvView: UIView {
    var onKickbackRaiseSubtractHandler:((_ tag:Int) -> Void)?
    var onFastMoneyClickHandler:((_ moneyItems:[String],_ button:UIButton) -> Void)?
    var peilvMoneyHandler:( (_ moeny:String?) -> Void)?
    @IBOutlet weak var creditBottomHistoryLabel: UILabel!
    @IBOutlet weak var creditBottomHistoryImg: UIImageView!
    @IBOutlet weak var creditBottomHistoryButton: UIButton!
    @IBOutlet weak var creditbottomTopSlider: CustomSlider!
    @IBOutlet weak var creditbottomSlideLeftLabel: UILabel!
    @IBOutlet weak var bottomtopTipsLabel: UILabel!
    @IBOutlet weak var creditbottomTopField: CustomFeildText!
    @IBOutlet weak var creditBottomTopFastImgBtn: UIButton!
    @IBOutlet weak var bet_kick_minus: UIImageView!
    @IBOutlet weak var bet_kick_add: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bet_kick_minus.isUserInteractionEnabled = true
        bet_kick_add.isUserInteractionEnabled = true
        bet_kick_minus.tag = 100
        bet_kick_add.tag = 101
        bet_kick_minus.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        bet_kick_add.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        
        self.creditBottomTopFastImgBtn.addTarget(self, action: #selector(onFastMoneyClick(sender:)), for: .touchUpInside)
        
        self.creditbottomTopSlider.addTarget(self, action: #selector(sliderChange), for: UIControl.Event.valueChanged)
        
        self.creditbottomTopField.delegate = self
        creditbottomTopField.addTarget(self, action: #selector(fieldValueChanged(field:)), for: .editingChanged)
    }
    
    @objc private func fieldValueChanged(field:CustomFeildText) {
        peilvMoneyHandler?(field.text)
    }
    
    //拖动条滑动事件
    @objc func sliderChange(slider:UISlider) -> Void{
        self.creditbottomTopSlider.changeSeekbar(currentProgress: slider.value)
    }
    
    @objc func onKickbackRaiseSubtract(ui:UITapGestureRecognizer) {
        if let view = ui.view {
            self.onKickbackRaiseSubtractHandler?(view.tag)
        }
    }

    //快选金额点击事件
    @objc func onFastMoneyClick(sender:UIButton){
        
        var str = ""
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                str = config.content.fast_money_setting
            }
        }
        if isEmptyString(str: str){
            str = "10,20,50,100,200,500,1000,2000,5000"
        }
        
        str = str.trimmingCharacters(in: .whitespaces)
        let moneys = str.components(separatedBy: ",")
        if moneys.isEmpty{
            showToast(view: self, txt: "没有快选金额，请联系客服")
            return
        }
        
        for m in moneys{
            if !isPurnInt(string: m){
                showToast(view: self, txt: "配置的快选金额格式不正确")
                return
            }
        }
        
        self.onFastMoneyClickHandler?(moneys,sender)
    }
    
}

extension CRBetConfirmPeilvView:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location >= 7 {
            showToast(view: self, txt: "当前允许输入最大长度为7位")
            return false
        }
        return true
    }
}
