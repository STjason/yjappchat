//
//  CRBetConfirmArea.swift
//  gameplay
//
//  Created by admin on 2019/9/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetConfirmArea: UIView {
    let cellID = "CRBetConfirmCell"
    var datas:[OrderDataInfo] = []  { //信用版选中的彩种数据
        didSet {
            self.tableView.reloadData()
        }
    }
    
    private var isPeilv = true {
        didSet {
            self.peilvBottom.isHidden = !isPeilv
            self.officalBottom.isHidden = isPeilv
        }
    }
    //信用版
    lazy var tableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .plain)
        view.separatorInset = UIEdgeInsets.zero
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.register(CRBetConfirmCell.self, forCellReuseIdentifier: cellID)
        view.tableFooterView = UIView.init()
        return view
    }()
    
    lazy var peilvBottom:CRBetConfirmPeilvView = {
        let view = Bundle.main.loadNibNamed("CRBetConfirmPeilvView", owner: self, options: nil)?.last as! CRBetConfirmPeilvView
        view.isHidden = true
        
        return view
    }()
    
    /**官方*/
    lazy var officalBottom:CRBetConfirmOfficalView = {
        let view = Bundle.main.loadNibNamed("CRBetConfirmOfficalView", owner: self, options: nil)?.last as! CRBetConfirmOfficalView
        view.isHidden = true
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.addSubview(self.peilvBottom)
        self.addSubview(self.officalBottom)
        self.addSubview(self.tableView)
        
        self.tableView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(0)
            make.bottom.equalTo(-65)
        }
        
        self.officalBottom.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(0)
            make.height.equalTo(65)
        }
        
        self.peilvBottom.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(0)
            make.height.equalTo(65)
        }
    }
    
    func updateWith(isPeilv:Bool) {
        self.isPeilv = isPeilv
    }
}


extension CRBetConfirmArea:UITableViewDelegate {
    
}

extension CRBetConfirmArea:UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as? CRBetConfirmCell else {
            fatalError("dequeueReusableCell CRBetConfirmCell failure")
        }
        
        let data = self.datas[indexPath.row]
        
        let rule = data.subPlayName + "- \(data.oddsName)"
        let num = data.numbers
        cell.configWith(rule: rule, num: num)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datas.count
    }
}
















