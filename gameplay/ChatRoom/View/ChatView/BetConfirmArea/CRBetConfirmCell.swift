//
//  CRBetConfirmCell.swift
//  gameplay
//
//  Created by admin on 2019/9/19.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetConfirmCell: UITableViewCell {
    let ruleNameLabel = UILabel()
    let selectNumLabel = UILabel()

    //MARK: - UI
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = UIColor.clear
        self.ruleNameLabel.textAlignment = .left
        self.ruleNameLabel.font = UIFont.systemFont(ofSize: 16)
        self.ruleNameLabel.textColor = UIColor.white
        self.ruleNameLabel.numberOfLines = 0
        
        self.selectNumLabel.textAlignment = .center
        self.selectNumLabel.font = UIFont.systemFont(ofSize: 16)
        self.selectNumLabel.textColor = UIColor.orange
        self.selectNumLabel.numberOfLines = 0
        
        self.addSubview(self.ruleNameLabel)
        self.addSubview(self.selectNumLabel)
        
        self.ruleNameLabel.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.left.equalTo(5)
                make.right.equalTo(weakSelf.selectNumLabel.snp.left).offset(-5)
                make.width.equalTo(weakSelf.snp.width).dividedBy(3)
                make.height.greaterThanOrEqualTo(44)
            }
        }
        
        self.selectNumLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(0)
            make.right.equalTo(-5)
        }
    }
    
    //MARK: - API
    
    /// 配置cell的显示
    ///
    /// - Parameters:
    ///   - rule: 当前玩法名称
    ///   - num: 选中的号码
    func configWith(rule:String,num:String) {
        self.ruleNameLabel.text = rule
        self.selectNumLabel.text = num
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
