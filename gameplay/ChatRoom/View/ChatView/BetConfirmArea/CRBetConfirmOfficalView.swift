//
//  CRBetConfirmOfficalView.swift
//  gameplay
//
//  Created by admin on 2019/9/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetConfirmOfficalView: UIView {
    var show_mode_switch_dialogHandler:((_ button:UIButton) -> Void)?
    var plusMultipleActionHandler:((_ beishu_money_input:CustomFeildText) -> Void)?
    var minusMultipleActionHandler:((_ beishu_money_input:CustomFeildText) -> Void)?
    var onKickbackRaiseSubtractHandler:((_ tag:Int) -> Void)?
    var officalInputMoneyStringHandler:((_ moneyString:String)->())?
    /**官方版本输入框事件*/
    var officialInputMoneyHander:((_ keyboardHeight:CGFloat,_ durationTime:Double)->())?
    var officiaHiddenKeyboardHander:((_ durationTime:Double)->())?
    
    
    var keyboardHeight:CGFloat = 0
    var keyBoardAnimateDuratio:Double = 0
    
    @IBOutlet weak var modeBtn: UIButton!
    @IBOutlet weak var beishu_money_input: CustomFeildText!
    @IBOutlet weak var ratebackTV: UILabel!
    @IBOutlet weak var currentOddTV: UILabel!
    @IBOutlet weak var official_bet_kick_minus: UIImageView!
    @IBOutlet weak var official_bet_kick_add: UIImageView!
    @IBOutlet weak var oddSlider: CustomSlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.official_bet_kick_minus.isUserInteractionEnabled = true
        self.official_bet_kick_add.isUserInteractionEnabled = true
        official_bet_kick_minus.tag = 200
        official_bet_kick_add.tag = 201
        beishu_money_input.delegate = self
        self.official_bet_kick_minus.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        self.official_bet_kick_add.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(onKickbackRaiseSubtract(ui:))))
        beishu_money_input.addTarget(self, action: #selector(inputMoney(textField:)), for: .editingChanged)
        
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.oddSlider.addTarget(self, action: #selector(sliderChange), for: UIControl.Event.valueChanged)
    }
    
    //拖动条滑动事件
    @objc func sliderChange(slider:UISlider) -> Void{
        oddSlider.changeSeekbar(currentProgress: slider.value)
    }
    
    @IBAction func minusMultipleAction(_ sender: Any) {
        self.minusMultipleActionHandler?(self.beishu_money_input)
    }
    
    @IBAction func plusMultipleAction(_ sender: Any) {
        self.plusMultipleActionHandler?(self.beishu_money_input)
    }
    
    @IBAction func onModeSwitch(_ sender: Any) {
        self.show_mode_switch_dialogHandler?(sender as! UIButton)
    }
    
    @objc func onKickbackRaiseSubtract(ui:UITapGestureRecognizer) {
        if let view = ui.view {
            self.onKickbackRaiseSubtractHandler?(view.tag)
        }
    }
    @objc func inputMoney(textField:UITextField){
        if let moneyString = textField.text{
            officalInputMoneyStringHandler?(moneyString)
        }
        
    }
    
}
extension CRBetConfirmOfficalView:UITextFieldDelegate{
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        officialInputMoneyHander?()
//    }
}
extension CRBetConfirmOfficalView{
    
    @objc func keyboardWillShow(notification:Notification){
        let userInfo = notification.userInfo! as Dictionary
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        keyBoardAnimateDuratio = duration.doubleValue
        let keyboradFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey]as! NSValue).cgRectValue
        keyboardHeight = keyboradFrame.size.height
        officialInputMoneyHander?(keyboardHeight,keyBoardAnimateDuratio)
    }
    @objc func keyboardWillHide(notification:Notification){
        let userInfo = notification.userInfo! as Dictionary
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let anmationDuration = duration.doubleValue
        let keyboradFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey]as! NSValue).cgRectValue
        keyboardHeight = keyboradFrame.size.height
        officiaHiddenKeyboardHander?(anmationDuration)
    }
}





