//
//  CRRoomsTopEntryCell.swift
//  Chatroom
//
//  Created by admin on 2019/7/16.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit

class CRRoomsTopEntryCell: UICollectionViewCell {
    var itemModel = CRRoomListModel()
    
    lazy var titleButton:UIButton = {
        var view = UIButton()
        view.titleLabel?.numberOfLines = 2
        view.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        view.isUserInteractionEnabled = false
        view.setTitleColor(UIColor.white, for: .normal)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        
        titleButton.layer.cornerRadius = 3
        addSubview(titleButton)
        
        titleButton.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    func configWith(model:CRRoomListModel,colorType:Int,isSelectIndex:Bool) {
        titleButton.setTitle(model.name, for: .normal)
        titleButton.setImage(UIImage.init(named: "entryRoomDiamond"), for: .normal)
        
        var imgName = ""
        if  colorType == 0 { //紫--紫
            imgName = "chatentry_purple"
        }else if colorType == 1 { // 蓝--红
            imgName = "chatentry_red"
        }else if colorType == 2 { //红--粉
            imgName = "chatentry_pink"
        }else if colorType == 3 { //蓝
            imgName = "chatentry_blue"
        }else if colorType == -1 { //橘色
            imgName = "chatentry_orange"
            titleButton.setImage(UIImage.init(named: "entryRoomMore"), for: .normal)
        }
        
        if isSelectIndex {
            imgName = "chatentry_gray"
        }
        
        titleButton.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
    }
    
}






