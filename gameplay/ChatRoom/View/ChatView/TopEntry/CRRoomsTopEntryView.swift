//
//  CRRoomsTopEntryView.swift
//  Chatroom
//
//  Created by admin on 2019/7/16.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRRoomsTopEntryView: UIView {
    var currentSelectIndex = 0
    let IDENTIFIER = "CRRoomsTopEntryCell"
    var isShow = false
    var hiddenFrame = CGRect.zero //视图隐藏时的frame
    var showFrame = CGRect.zero //视图显示时的frame
    //collection相关
    var numsInHorizontal = 2 //每行个数
    var minimumLine:CGFloat = 10 //垂直间距
    var sectionMargin:CGFloat = 15
    var itemHeight:CGFloat = 44
    
    var itemModels = [CRRoomListModel]()
    
    var selectRoomHandler:((_ index:Int,_ isMore:Bool) -> Void)?
    
    lazy var collection:UICollectionView = {
        var minimumInteritem:CGFloat = 10 //水平间距
        
        var itemWidth = (self.showFrame.size.width - 2 * sectionMargin - CGFloat(numsInHorizontal - 1) * minimumInteritem) / CGFloat(numsInHorizontal) //每个item的宽
        
        var layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: sectionMargin, left: sectionMargin, bottom: sectionMargin, right: sectionMargin)
        layout.minimumLineSpacing = minimumLine
        layout.minimumInteritemSpacing = minimumInteritem
        layout.itemSize = CGSize.init(width: itemWidth, height: itemHeight)
        
        var view = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        view.register(CRRoomsTopEntryCell.self, forCellWithReuseIdentifier: IDENTIFIER)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = UIColor.clear
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //初始化frame
    func configWith(hiddenFrame:CGRect) {
        self.hiddenFrame = hiddenFrame
    }
    
    ///显示或者隐藏视图
    func show() {
        let frame = self.isShow ? self.hiddenFrame : self.showFrame
        self.isShow = !self.isShow
       
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = frame
        }) { (complete) in
        }
    }
    
    func hideView() {
        let frame = self.hiddenFrame
        self.isShow = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = frame
        }) { (complete) in
        }
    }
    
    //MARK: - 外部调用API
    func configWith(models:[CRRoomListModel]) {
        self.itemModels = models
        self.calculateViewHeight(itemsCount: self.itemModels.count)
        
        self.collection.removeFromSuperview()
        self.addSubview(self.collection)
        self.collection.reloadData()
    }
    
    //计算视图最大高度
    private func calculateViewHeight(itemsCount:Int) {
        let itemsCount = getCellCount()
        let rows = itemsCount % 2 + itemsCount / 2 //行数
        let mumLinesHeight =  CGFloat(rows - 1) * minimumLine //所有行间距的和
        let sectionMargin = self.sectionMargin * 2 //上下间距
        let itemsHeight = CGFloat(rows) * self.itemHeight
        let heightInTotal = mumLinesHeight + sectionMargin + itemsHeight
        
        self.showFrame = CGRect.init(origin: self.hiddenFrame.origin, size: CGSize.init(width: self.hiddenFrame.size.width, height: heightInTotal))
        
        self.frame = CGRect.init(x: showFrame.origin.x, y: showFrame.origin.y, width: showFrame.size.width, height: 0)
        self.collection.frame = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: showFrame.size)
    }
    
    //MARK: 显示的房间入口个数,包括更多
    func getCellCount() -> Int{
        if itemModels.count >= 7 {
            return 8
        }else {
            return itemModels.count >= 1 ? itemModels.count + 1  : 0
        }
    }
    
}

//MARK: - UICollectionViewDelegate
extension CRRoomsTopEntryView:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectRoomHandler?(indexPath.row, indexPath.row == getCellCount() - 1)
    }
}


//MARK: - UICollectionViewDataSource
extension CRRoomsTopEntryView:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getCellCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IDENTIFIER, for: indexPath) as? CRRoomsTopEntryCell else {
            fatalError("dequeueReusableCell")
        }
        
        let remainder = (indexPath.row + 1) % 4
        
        let isSelectIndex = currentSelectIndex == indexPath.row
        
        //更多入口
        if indexPath.row == getCellCount() - 1 {
            let model = CRRoomListModel()
            model.name = "更多房间"
            cell.configWith(model: model,colorType:-1,isSelectIndex: false)
        }else {
            let mdoel = self.itemModels[indexPath.row]
            cell.configWith(model: mdoel,colorType: remainder,isSelectIndex: isSelectIndex)
        }
        
        return cell
    }
    
}


