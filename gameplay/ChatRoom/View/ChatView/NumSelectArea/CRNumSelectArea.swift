//
//  CRNumSelectArea.swift
//  gameplay
//
//  Created by admin on 2019/8/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRNumSelectArea: UIView {
    let topViewH:CGFloat = 100
    let bottomViewH:CGFloat = IS_IPHONE_X ? 44 + 34 : 44
    let animateTime:TimeInterval = 0.3
//    let totalH:CGFloat = 0
    
    lazy var topView:CRBetTopView = {
        let view = Bundle.main.loadNibNamed("CRBetTopView", owner: self, options: nil)!.last as! CRBetTopView
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    lazy var numSelectBackView:UIView = {
        let view = UIView()
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        return view
    }()
    
    //玩法选择view
    lazy var betRuleView:CRBetRulesView = {
        let view = CRBetRulesView.init(size: CGSize.init(width: screenWidth, height: 0), models: [])
        return view
    }()
    
    //号码选择view
    lazy var numSelectView:CRNumSelectView = {
        let view = CRNumSelectView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    //下注确认页
    lazy var confirmBetArea:CRBetConfirmArea = {
        let view = CRBetConfirmArea.init(frame: .zero)
        return view
    }()
    
    //投注底部view
    lazy var bottomView:CRBetBottomView = {
        let view = Bundle.main.loadNibNamed("CRBetBottomView", owner: self, options: nil)!.last as! CRBetBottomView
        view.backgroundColor = UIColor.black.withAlphaComponent(0.45)
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.addSubview(self.topView)
        self.addSubview(self.numSelectBackView)
        self.addSubview(self.bottomView)
        self.numSelectBackView.addSubview(self.betRuleView)
        self.numSelectBackView.addSubview(self.numSelectView)
        self.numSelectBackView.addSubview(self.confirmBetArea)
        
        self.layout()
    }
    
    private func layout() {
        self.topView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.left.right.equalTo(0)
                make.height.equalTo(weakSelf.topViewH)
            }
        }
        
        self.numSelectBackView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.topView.snp.bottom)
                make.left.right.equalTo(0)
                make.bottom.equalTo(weakSelf.bottomView.snp.top)
            }
        }
        
        self.betRuleView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.width.equalTo(weakSelf.numSelectBackView.snp.width)
                make.right.equalTo(weakSelf.numSelectView.snp.left)
            }
        }
        
        self.numSelectView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.width.equalTo(weakSelf.numSelectBackView.snp.width)
                make.right.equalTo(weakSelf.confirmBetArea.snp.left)
                make.right.equalTo(weakSelf.numSelectBackView.snp.right)
            }
        }
        
        self.confirmBetArea.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.width.equalTo(weakSelf.numSelectBackView.snp.width)
            }
        }

        self.bottomView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.left.right.equalTo(0)
                make.bottom.equalTo(0)
                make.height.equalTo(weakSelf.bottomViewH)
            }
        }
        
        
    }
    
    //MARK: - API
    ///显示投注确认页面
    func showConfirmArea() {
        self.numSelectView.snp.remakeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.width.equalTo(weakSelf.numSelectBackView.snp.width)
                make.right.equalTo(weakSelf.confirmBetArea.snp.left)
                make.right.equalTo(weakSelf.numSelectBackView.snp.left)
            }
        }
        
        UIView.animate(withDuration: animateTime) {
            self.numSelectBackView.layoutIfNeeded()
        }
    }
    
    ///显示玩法选择视图
    func showRulesView() {
        self.numSelectView.snp.remakeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.width.equalTo(weakSelf.numSelectBackView.snp.width)
                make.right.equalTo(weakSelf.confirmBetArea.snp.left)
                make.left.equalTo(weakSelf.numSelectBackView.snp.right)
            }
        }
        
        UIView.animate(withDuration: animateTime) {
            self.numSelectBackView.layoutIfNeeded()
        }
    }
    
    ///显示号码选择视图
    func showNumSelectView() {
        self.numSelectView.snp.remakeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.width.equalTo(weakSelf.numSelectBackView.snp.width)
                make.right.equalTo(weakSelf.confirmBetArea.snp.left)
                make.right.equalTo(weakSelf.numSelectBackView.snp.right)
            }
        }
        
        UIView.animate(withDuration: animateTime) {
            self.numSelectBackView.layoutIfNeeded()
        }
    }
}

