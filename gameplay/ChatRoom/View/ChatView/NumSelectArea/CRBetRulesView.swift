//
//  CRBetRulesView.swift
//  gameplay
//
//  Created by admin on 2019/8/14.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetRulesView: UIView {
    //collection相关
    var ruleSelectHandler:((_ model:BcLotteryPlay,_ index:Int) -> Void)?
    ///配置显示选择的玩法 index,isSelected,cell
    var showCellHandler:((_ index:Int,_ isSelected:Bool,_ cell:CRBetRuleCell) -> Void)?
    var selected_rule_position = 0;//用户在侧边玩法栏选择的玩法位置
    let IDENTIFIER = "CRBetRuleCell"
    var numsInHorizontal = 3 //每行个数
    var minimumLine:CGFloat = 30 //水平间距
    var sectionHorMargin:CGFloat = 15 //水水平间距
    var sectionTopMargin:CGFloat = 8 //竖直间距
    var sectionBottomMargin:CGFloat = 8  //竖直间距
    var itemSize:CGSize = .zero
    let animateTime:TimeInterval = 0.3
    let minimumInteritem:CGFloat = 8 //垂直间距
    private let lotteryNameH:CGFloat = 40 //lotteryName 高度
    
//    var collectionH:CGFloat = 0 //collection's height
    var collectionW:CGFloat = 0
    var playRules:[BcLotteryPlay] = [BcLotteryPlay]() { //玩法数组
        didSet {
            self.updateRulesWith()
        }
    }
    
    lazy var collection:UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: sectionTopMargin, left: sectionHorMargin, bottom: sectionBottomMargin,  right: sectionHorMargin)
        layout.minimumLineSpacing = minimumLine
        layout.minimumInteritemSpacing = minimumInteritem
        layout.itemSize = self.itemSize
        
        var view = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        view.register(CRBetRuleCell.self, forCellWithReuseIdentifier: IDENTIFIER)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = UIColor.clear
        
        view.isPagingEnabled = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    convenience init(size:CGSize,models:[BcLotteryPlay]) {
        self.init()
        self.collectionW = size.width
//        self.collectionH = size.height
        
        // 计算设置 item的 size
        let itemWidth = (self.collectionW - 2 * sectionHorMargin - CGFloat(numsInHorizontal - 1) * minimumLine) / CGFloat(numsInHorizontal) //每个item的宽
        self.itemSize = CGSize.init(width: itemWidth, height: 44)
        
        self.setupUI()
        self.playRules = models
    }
    
    private func setupUI() {
        self.addSubview(self.collection)
        self.layout()
    }
    
    private func layout() {
        self.collection.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
    }
    
    /// 根据数据刷新彩种视图
    private func updateRulesWith() {
        self.collection.reloadData()
    }
    
    func updateRulesWith(models:[BcLotteryPlay]) {
        self.playRules = models
    }
}

extension CRBetRulesView:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.playRules[indexPath.row]
        self.selected_rule_position = indexPath.row
        self.ruleSelectHandler?(model,indexPath.row)
        self.collection.reloadData()
    }
}


extension CRBetRulesView:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.playRules.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IDENTIFIER, for: indexPath) as? CRBetRuleCell else {fatalError("dequeueReusableCell CRBetRuleCell failure")}
        
        let row = indexPath.row
        
        let model = self.playRules[row]
        let name = isEmptyString(str: model.fakeParentName) ? model.name : model.fakeParentName
        
        cell.configWith(title: name)
        
        self.showCellHandler?(row,self.selected_rule_position == indexPath.row,cell)
        return cell
    }
    
}
