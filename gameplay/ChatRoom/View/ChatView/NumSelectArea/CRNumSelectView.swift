//
//  CRNumSelectView.swift
//  gameplay
//
//  Created by admin on 2019/8/14.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRNumSelectView: UIView {
    var update_honest_seekbarHandler:((_ maxOddResult:PeilvWebResult) -> Void)?
    var playClickBallSoundHandler:(() -> Void)?
    //注数，金额变化
    var betInfoHandler:((_ zhuShu:Int,_ money:Float,_ info:String) -> Void)?
    var gotoBetNewPeilvHandler:(([OrderDataInfo],[BcLotteryPlay],LHCLogic2,String,String,String,String,Float,String,String,String) -> Void)? //信用下注
    var gotoBetNewOfficalHandler:((_ data:[OrderDataInfo],_ lotCode:String,_ lotName:String,_ subPlayCode:String,_ subPlayName:String,
    _ cpTypeCode:String,_ cpVersion:String,_ officail_odds:[PeilvWebResult],_ icon:String,_ meminfo:Meminfo?,_ officalBonus:Double?) -> Void)? //官方下注
    ///信用版 快捷 一般隐藏显示
    var shownormalFastButtons:((_ show:Bool) -> Void)?
    
    lazy var lhcLogic:LHCLogic2 = { //特殊六合彩玩法的处理类
        let logic = LHCLogic2()
        logic.initAllDatas()
        logic.fromCRChatRoom = true
        logic.playButtonDelegate = self
        return logic
    }()
    
    var fakeSubPlayCode = "" // 标注从后台返回的玩法数据中自己分离出来的玩法
    var fakeSubPlayName = "" // 标注从后台返回的玩法数据中自己分离出来的玩法
    let OFFICIAL_IDENTIFIER = "CRJianjinPaneCell"
    let PEILV_IDENTIFIER = "CRPeilvTouzhuCell"
    var multiplyValue:Float = 1.0
    var selectedBeishu = 1;//选择的倍数
    var winExample = "";
    var detailDesc = "";
    var playMethod = "";
    var selectedPlayRulesModel = SelectedPlayRulesModel() //多玩法投注，记录选择的玩法
    var peilvListDatasSuperSet:[[BcLotteryPlay]] = [] //多玩法投注多个玩法的赔率列表数据集合
    var headerCode = "" //有header选择的彩种的code，如连肖/连码等
    var headerName = "" //有header选择的彩种的name，如连肖/连码等
    var moreHeadersSubCode = "" //合肖等有多个子header(如一肖中，一肖不中)对应的code，以标识是‘中’或‘不中’
    var betShouldPayMoney = 0.0
    var is_fast_bet_mode = true//是否快捷下注模式
    var honest_orders:[PeilvOrder] = []////信用版所有下注主单列表
    var sortWhenSelectPeilvNumber = 0 //用户选择号码的顺序
    var betMoneyWhenPeilv = ""//每注下注金额
    var cpName:String = ""
    var selectNumList:[PeilvWebResult] = []//选择好的赔率项数据
    var official_orders:[OfficialOrder] = []//官方版所有下注主单列表
    var officail_odds = [PeilvWebResult]()//官方版所有赔率
    var lotData:LotteryData?
    var showCodeRank = true//是否显示冷热遗漏数据
    var isColdHotNormalType = 0 // 0 冷热 1 遗漏  2 快捷 3 一般
    var codeRank:[CodeRankModel]?
    var cpVersion:String = VERSION_1;//当前彩票版本
    var lhcSelect = false;//是否在奖金版本中选中了六合彩，十分六合彩
    var cpBianHao:String = "";//彩票编码
    let BALL_COUNT_PER_LINE_WHEN_EXPAND = 5
    var cpTypeCode:String = "" //彩票类型代号
    var subPlayCode:String = ""
    var selectedSubPlayCode:String = ""//赔率版左侧玩法栏玩法code
    var subPlayName = ""
    var ballonDatas:[BallonRules] = []//官方版彩票列表数据源
    var peilvListDatas:[BcLotteryPlay] = []//赔率版彩票列表数据源
    var honest_odds:[HonestResult] = []//信用版用户选择的侧边打玩法对应的所有小玩法对应的所有赔率列表数据
    var selected_rule_position = 0;//用户在侧边玩法栏选择的玩法位置
    var selectPlay:BcLotteryPlay?//当前选择的侧边玩法
    var choosedPlay:BcLotteryPlay?
    var playRules:[BcLotteryPlay] = []//所有叶子玩法列表数据
    var betLogic = CRBetLogic.shareInstance()
    
    lazy var numSelectTableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .plain)
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.separatorStyle = .none
        
        let officalNib = UINib.init(nibName: "CRJianjinPaneCell", bundle: nil)
        let peilvNib = UINib.init(nibName: "CRPeilvTouzhuCell", bundle: nil)
        
        view.register(officalNib, forCellReuseIdentifier: OFFICIAL_IDENTIFIER)
        view.register(peilvNib, forCellReuseIdentifier: PEILV_IDENTIFIER)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.addSubview(self.numSelectTableView)
        self.layout()
    }
    
    private func layout() {
        self.numSelectTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
    }
    
    //侧边玩法栏列表项点击事件
    func handlePlayRuleClick(playData:BcLotteryPlay,position:Int,fromPlayCellSelect:Bool = false,handler:@escaping (_ rightResult:PeilvWebResult?) -> ()) {
        self.selected_rule_position = position
        self.selectPlay = playData
        
        fakeSubPlayCode = playData.fakeParentCode
        fakeSubPlayName = playData.fakeParentName
        
        if playData.status != 2{
            showToast(view: self, txt: "该玩法已被关闭使用，开关此玩法请至平台配置")
            return
        }

        let isPeilvVersion = self.isPeilvVersion()
        
        if switchCanMutiRulesBet(isPeilv: isPeilvVersion) {

        }else {
            //当切换到其他玩法时，将保存的下注注单清空
            if playData.code != self.subPlayCode{
                YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
            }
        }

        self.subPlayCode = playData.code
        self.subPlayName = playData.name
        self.winExample = playData.winExample
        self.detailDesc = playData.detailDesc
        self.playMethod = playData.playMethod

        if (self.subPlayName == "自选不中" || self.subPlayName == "连码" || self.subPlayName == "连肖") && lotData?.lotType == 6
        {
            self.shownormalFastButtons?(false)
        }else
        {
            self.shownormalFastButtons?(true)
        }

//        //重新刷新投注面板
        if !isPeilvVersion {
            //根据彩票版本和玩法确定下注球列表数据
            let ballonRules = form_jianjing_pane_datasources(lotType: self.cpTypeCode, subCode: self.subPlayCode)
            self.ballonDatas.removeAll()
            self.ballonDatas = self.ballonDatas + ballonRules
            
            let params:Dictionary<String,AnyObject> = ["playCode":self.subPlayCode as AnyObject,"lotType":self.cpTypeCode as AnyObject,"version":VERSION_1 as AnyObject]
            
            CRNetManager.shareInstance().netSync_official_peilvs_after_playrule_click(paramters: params) {[weak self] (success, msg, odds) in
                if let weakSelf = self,let oddsArray = odds {
                    weakSelf.officail_odds = oddsArray
                    if let result = CRBetLogic.update_slide_when_peilvs_obtain(rateBack: oddsArray) {
                        handler(result)
                    }
                }
            }
            
        }else{
//            //获取到玩法栏数据后，再根据选择玩法栏的所有子玩法code，获取子玩法下所有投注号码的赔率列表数据
//            //开始网络获取
            self.getOddsFromAllPlayCodes(playData:playData, showDialog: true,fromPlayCellSelect:fromPlayCellSelect)
            handler(nil)
        }
        
        self.numSelectTableView.reloadData()
    }
    
    ///切换彩种后更新
    func updateLocalConstants(lotData:LotteryData?,handler:@escaping (_ name: String) -> ()) -> Void {
        self.lotData = lotData
        
        
        if let lotName = lotData?.name{
            self.cpName = lotName
        }
//        if let lotCode = lotData?.czCode{
//            self.czCode = lotCode
//        }
//        if let lotAgo = lotData?.ago{
//            self.ago = lotAgo
//            self.disableBetTime = lotAgo
//        }
        if let lotCpCode = lotData?.code{
            self.cpBianHao = lotCpCode
//            lotCpCodeChangedHandler?(lotCpCode)
        }
//        if let lotteryICON = lotData?.lotteryIcon {
//            self.lotteryICON = lotteryICON
//        }
        if let lotCodeType = lotData?.lotType{
            self.cpTypeCode = String.init(describing: lotCodeType)
        }
        if let lotVersion = lotData?.lotVersion{
            self.cpVersion = String.init(describing: lotVersion)
        }
//        self.tickTime = self.ago + Int64(self.offset)
        if isSixMark(lotCode: self.cpBianHao){
            lhcSelect = true
            self.cpVersion = VERSION_2
        }else {
            lhcSelect = false
        }
        
        handler(self.cpName)
    }
    
    ///根据所有玩法获取赔率列表
    func getOddsFromAllPlayCodes(playData:BcLotteryPlay,showDialog:Bool,fromPlayCellSelect:Bool = false) -> Void{
        var playCodes = ""
        
        //根据每一行的玩法code，从赔率表中获取对应的号码球赔率数据集
        let play = playData
        let children:[BcLotteryPlay] = play.children
        if !children.isEmpty{
            for p in children{
                playCodes += p.code
                playCodes.append(",")
            }
        }
        if playCodes == ""{
            showToast(view: self, txt: "没有开启该玩法，请联系客服")
            self.selectPlay = nil;
            self.refresh_page_after_honest_odds_obtain(selected_play: &self.selectPlay, odds: [])
            return
        }
        if playCodes.hasSuffix(","){
            playCodes = (playCodes as NSString).substring(to: playCodes.count - 1)
        }
        
        var cpTypeCode = ""
        if let model = self.lotData {
            cpTypeCode = "\(model.lotType)"
            cpBianHao = model.code ?? ""
        }
        
        var p = [String:Any]()
        p["playCodes"] = playCodes
        p["lotType"] = cpTypeCode
        p["version"] = self.cpVersion
        CRNetManager.shareInstance().netSync_honest_plays_odds_obtain(fakeParentCode: selectPlay?.fakeParentCode ?? "",paramters: p) {[weak self] (status, msg, models) in
            //更新赔率面板号码区域赔率等数据
            if let weakSelf = self,let models = models {
                weakSelf.honest_odds = models
                
                weakSelf.update_honest_seekbar(handler: {(maxOddResult) in
                    weakSelf.update_honest_seekbarHandler?(maxOddResult)
                })
                
                ///将当前玩法赔率插入当前玩法对象
                var hadSelected = false
                if weakSelf.peilvListDatasSuperSet.count == 0 {
                    hadSelected = false
                }else {
                    //判断peilvListDatas SuperSet 是否存在当前选择的 peilvListDatas
                    
                    for (_,peilvs) in weakSelf.peilvListDatasSuperSet.enumerated() {
                        //表示已经选择过该左侧边栏玩法
                        print("")
                        if peilvs[0].code == models[0].code {
                            hadSelected = true
                            break
                        }else {
                            hadSelected = false
                        }
                    }
                }
                
                if !hadSelected {
                    PeilvLogic.insertOddsToPlayDatas(selectPlay: &weakSelf.selectPlay, odds: models)
                }
                
                weakSelf.refresh_page_after_honest_odds_obtain(selected_play: &weakSelf.selectPlay, odds: weakSelf.honest_odds)
            }
        }
    }
    
    /**
     * 信用版：根据玩法栏选择好的玩法，刷新列表数据  
     * @param selected_play 用户选择的侧边玩法数据
     * @param odds  选择的侧边玩法对应的所有子玩法对应的所有赔率项数据
     */
    func refresh_page_after_honest_odds_obtain(selected_play: inout BcLotteryPlay?,odds:[HonestResult],fromPlayCellSelect:Bool = false) {
        
        self.numSelectTableView.tableHeaderView = nil
        
        if selected_play == nil{
            peilvListDatas.removeAll()
            self.numSelectTableView.reloadData()
            return
        }
        
        if isEmptyString(str: cpBianHao) || isEmptyString(str: subPlayCode){
            return
        }
        
        var hadSelected = false
        if peilvListDatasSuperSet.count == 0 {
            hadSelected = false
        }else {
            //判断peilvListDatas SuperSet 是否存在当前选择的 peilvListDatas
            
            for (index,peilvs) in peilvListDatasSuperSet.enumerated() {
//                //表示已经选择过该左侧边栏玩法
//                //                || peilvs[0].name == selected_play?.name
//                if peilvs[0].parentCode == selected_play?.code  {
//                    hadSelected = true
//                    break
//                }else if index == peilvListDatasSuperSet.count - 1 {
//                    // 未选择过该左侧边栏玩法
//                    hadSelected = false
//                }
                
                if !isEmptyString(str: selected_play?.fakeParentName ?? "") && peilvs[0].fakeParentName == selected_play?.fakeParentName {
                    hadSelected = true
                    break
                }else if peilvs[0].parentCode == selected_play?.code  && isEmptyString(str: selected_play?.fakeParentName ?? "") {
                    hadSelected = true
                    break
                }else if index == peilvListDatasSuperSet.count - 1 {
                    // 未选择过该左侧边栏玩法
                    hadSelected = false
                }
            }
        }
        
        if !hadSelected {
            PeilvLogic.insertOddsToPlayDatas(selectPlay: &selected_play, odds: odds)
        }
        
        selectedSubPlayCode = (selected_play?.code)!
        choosedPlay = selected_play
        
        // 通过禁用子玩法项没有赔率及玩法项下所有子玩法项被禁用，移除该玩法
        if let data = self.choosedPlay?.children {
            
            var lotteryPlays = [BcLotteryPlay]()
            var lotterOdds = [HonestResult]()
            
            for index in 0..<data.count {
                let model = data[index]
                if model.peilvs.count != 0 {
                    honest_odds.forEach { (HonestResult) in
                        if HonestResult.code == model.code{
                            lotteryPlays.append(model)
                            lotterOdds.append(/*honest_odds[index]*/HonestResult)
                        }
                    }

                }
            }
            self.choosedPlay?.children = lotteryPlays
            self.honest_odds = lotterOdds
        }
        //若是连码等特殊玩法时，展示附加条件选择条
        lhcLogic.initializeIndexTitle()

        guard let play = selected_play else{return}
        if play.children.isEmpty{
            return
        }
        
        if isSixMark(lotCode: lotData?.code ?? "") || self.isPeilvVersion() {
            if let headerView = lhcLogic.createHeaderView(controller: UIViewController(),view:self, playCode: selectedSubPlayCode, odds: self.honest_odds) {
                
                var headerFrame = headerView.frame
                headerFrame.size =  CGSize(width:screenWidth, height: headerFrame.size.height)
                self.numSelectTableView.tableHeaderView = headerView
                
                if let selectedPlay = self.choosedPlay{
                    let map = lhcLogic.getListWhenSpecialClick(play: selectedPlay)
                    if map != nil {
                        lhcLogic.setFixFirstNumCountWhenLHC(fixCount: (map?.keys.contains("fixFirstCount"))! ? map!["fixFirstCount"] as! Int : 0)
                        let list = (map?.keys.contains("datas"))! ? map!["datas"] : ([] as AnyObject)
                        
                        if let datas = list as? [BcLotteryPlay] {
                            for (_,data) in datas.enumerated() {
                                data.parentCode = subPlayCode
                            }
                            
                            self.prapare_peilv_datas_for_tableview(peilvWebResults:datas)
                        }
                    }
                    
                    self.numSelectTableView.reloadData()
                }
            }else {
                lhcLogic.playName = (selected_play?.name) ?? ""
                self.numSelectTableView.tableHeaderView = nil
                
                self.prapare_peilv_datas_for_tableview(peilvWebResults:(selected_play?.children)!)
                self.numSelectTableView.reloadData()
            }
        }
    }
    
    /**
     * 计算出对应玩法下的号码及赔率面板区域显示数据
     *
     * @param peilvWebResults 赔率数据
     */
    func prapare_peilv_datas_for_tableview(peilvWebResults:[BcLotteryPlay]){
        
        self.peilvListDatas.removeAll()
        self.peilvListDatas = peilvWebResults
        
        if peilvWebResults.count == 0 {
            return
        }else {
            self.headerName = peilvWebResults[0].name
            self.headerCode = peilvWebResults[0].code
        }
        
        if peilvListDatasSuperSet.count == 0 {
            peilvListDatasSuperSet = [peilvWebResults]
            self.peilvListDatas = peilvListDatasSuperSet[0]
        }else {
            //判断peilvListDatasSuperSet 是否存在当前选择的 peilvListDatas
            var hadSelected = true
            for (index,peilvs) in peilvListDatasSuperSet.enumerated() {
                //表示已经选择过该左侧边栏玩法
                if !isEmptyString(str: peilvWebResults[0].parentCode) && !CRBetLogic.isHeaderPlay(parentCode: peilvWebResults[0].parentCode) {
//                    if peilvs[0].parentCode == peilvWebResults[0].parentCode {
//                        //                    if peilvs[0].code == peilvWebResults[0].code {
//                        self.peilvListDatas = peilvs
//                        return
//                    }else if index == peilvListDatasSuperSet.count - 1 {
//                        // 未选择过该左侧边栏玩法
//                        hadSelected = false
//                    }
                    
                    //表示已经选择过该左侧边栏玩法
                    if !isEmptyString(str: peilvWebResults[0].fakeParentName) && peilvs[0].fakeParentName == peilvWebResults[0].fakeParentName {
                        self.peilvListDatas = peilvs
                        return
                    }else if peilvs[0].parentCode == peilvWebResults[0].parentCode && isEmptyString(str: peilvWebResults[0].fakeParentName) && isEmptyString(str: peilvs[0].fakeParentName) {
                        //                    if peilvs[0].code == peilvWebResults[0].code {
                        self.peilvListDatas = peilvs
                        return
                    }else if index == peilvListDatasSuperSet.count - 1 {
                        // 未选择过该左侧边栏玩法
                        hadSelected = false
                    }
                    
                }else if !isEmptyString(str: peilvWebResults[0].code){
                    if peilvs[0].code == peilvWebResults[0].code {
                        
                        if peilvs[0].parentCode == "hx" {
                            let peilvsPeilvs = peilvs[0].peilvs
                            let peilvWebResultsPeilvs = peilvWebResults[0].peilvs
                            if peilvsPeilvs.count > 0 && peilvWebResultsPeilvs.count > 0 {
                                if peilvsPeilvs[0].code == peilvWebResultsPeilvs[0].code {
                                    self.peilvListDatas = peilvs
                                    return
                                }else if index == peilvListDatasSuperSet.count - 1 {
                                    // 未选择过该玩法(如"合肖不中"等有两个header可选的玩法)
                                    hadSelected = false
                                }
                            }
                        }else {
                            self.peilvListDatas = peilvs
                            return
                        }
                    }else if index == peilvListDatasSuperSet.count - 1 {
                        // 未选择过该左侧边栏玩法
                        hadSelected = false
                    }
                }
                
            }
            
            if !hadSelected {
                peilvListDatasSuperSet.append(peilvWebResults)
                if let peilvs = peilvListDatasSuperSet.last {
                    self.peilvListDatas = peilvs
                }
            }
            
        }
    }
    
    //MARK: 根据返水的变化，改变所有球的赔率
    func dynamicCalculateOdds(currentRate: Float,handler:([HonestResult]) -> ()) {
        
        var tempHonest_odds:[HonestResult] = []
        for honestIndex in 0..<(self.honest_odds.count) {
            let honestResult: HonestResult = self.honest_odds[honestIndex]
            
            let tempWebResultsArray: Array = honestResult.odds
            var webResultsArray: [PeilvWebResult] = []
            for webIndex in 0..<(tempWebResultsArray.count) {
                let webResult: PeilvWebResult = tempWebResultsArray[webIndex]
                webResult.currentOdds = webResult.maxOdds - currentRate*abs(webResult.maxOdds - webResult.minOdds)
                
                webResult.currentSecondOdds = webResult.secondMaxOdds - currentRate*abs(webResult.secondMaxOdds - webResult.secondMinodds)
                webResultsArray.append(webResult)
            }
            
            honestResult.odds = webResultsArray
            tempHonest_odds.append(honestResult)
        }
        
        self.honest_odds = tempHonest_odds
        handler(self.honest_odds)
    }
}

extension CRNumSelectView:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isPeilvVersion() {
            let specialPlay = lhcLogic.isSingleLineLayout()
            if specialPlay{
                let cellHeight = self.peilvListDatas[indexPath.row].peilvs.count
                return CGFloat(cellHeight*44 + 30 + 10)
            }else{
                let cellHeight = self.peilvListDatas[indexPath.row].peilvs.count
                var height = cellHeight/2
                if cellHeight%2 > 0{
                    height += 1
                }
                
                return CGFloat(height * 46 + 30)
            }

        }else {
            let ballCount = self.ballonDatas[indexPath.row].ballonsInfo.count
            let isWeishuShow = self.ballonDatas[indexPath.row].showWeiShuView
            
            var lines = ballCount / BALL_COUNT_PER_LINE_WHEN_EXPAND
            if ballCount > BALL_COUNT_PER_LINE_WHEN_EXPAND{
                if ballCount % BALL_COUNT_PER_LINE_WHEN_EXPAND != 0{
                    lines = lines + 1
                }
            }
            
            if lines == 0{
                lines = lines + 1
            }
            
            var height = CGFloat(CGFloat(lines)*40+65)
            
            if isWeishuShow!{
                height = height + 40
            }
            
            return height
        }
    }
    
    func isPeilvVersion() -> Bool {
        return CRBetLogic.isPeilvVersion(cpVersion: self.cpVersion, lhcSelect: self.lhcSelect)
    }
    
    //MARK: - Bet Logic
    //根据侧边大玩法对应的所有小玩法列表，来取出最大的返水比例
    func update_honest_seekbar(handler:(_ maxOddResult:PeilvWebResult) -> ()) {
        if self.selectPlay == nil {
            return
        }
        
        let selectPlay = self.selectPlay!
        let odds = self.honest_odds
        //这里根据赔率列表来决定是否显示返水拖动条
        if isEmptyString(str: cpTypeCode) || isEmptyString(str: selectPlay.code) {
            return
        }
        
        var maxOddResult:PeilvWebResult!
        if !odds.isEmpty{
            for subPlay in selectPlay.children{
                for result in odds{
                    if subPlay.code == result.code{
                        for odd in result.odds{
                            if maxOddResult == nil{
                                maxOddResult = odd
                            }else{
                                if odd.rakeback > maxOddResult.rakeback{
                                    maxOddResult = odd
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
        
        if maxOddResult != nil{
            handler(maxOddResult)
        }
    }
    
    /**
     * 开始计算投注号码串及注数(赔率版)
     * @param datasAfterSelected 非机选投注时，用户已经选择完投注号码的所有赔率列表数据
     */
    func calc_bet_orders_for_honest(datasAfterSelected:[BcLotteryPlay],updateBottomUIHandler:@escaping () ->()) -> Void {
        DispatchQueue.global().async {
            self.selectNumList.removeAll()
            for play in self.peilvListDatas{
                if !play.peilvs.isEmpty{
                    for result in play.peilvs{
                        if datasAfterSelected.count > 0 {
                            result.headerCode = datasAfterSelected[0].code
                        }
                        if result.isSelected{
                            self.selectNumList.append(result)
                        }
                    }
                }
            }
            print("select num count = ",self.selectNumList.count)  //12
            if datasAfterSelected[0].code == "siqz" {
                if self.selectNumList.count > 12 {
                    DispatchQueue.main.async {
                        showToast(view:self, txt: "最多只能选择12个号码,超过则不计算注数！")
                    }
                    return ;
                }
            }
            //计算注数
            let datas = self.lhcLogic.calcOrder(selectDatas: self.selectNumList)
            DispatchQueue.main.async {
                self.honest_orders.removeAll()
                self.honest_orders = datas
                updateBottomUIHandler()
            }
        }
    }
    
    /** 用以遍历peilvlistdatas，获得所有 honest_orders 投注数据 */
    func calculateBetDatasWithPeilvListDatas(peilvListDatas:[BcLotteryPlay]) -> [PeilvOrder] {
        var localSelectNumList:[PeilvWebResult] = []
        var localHonest_orders:[PeilvOrder] = []
        let localPeilvListDatas:[BcLotteryPlay] = [] + peilvListDatas
        
        for play in localPeilvListDatas {
            if !play.peilvs.isEmpty{
                for result in play.peilvs{
                    if result.isSelected{
                        if peilvListDatas.count > 0 {
                            result.headerCode = peilvListDatas[0].code
                        }
                        if isEmptyString(str: result.itemName) {
                            result.itemName = play.name
                        }
                        localSelectNumList.append(result)
                    }
                }
            }
        }
        
        //计算注数
        let datas = self.lhcLogic.calcOrder(selectDatas: localSelectNumList)
        
        localHonest_orders = datas
        return localHonest_orders
    }
    
    //MARK: - 多玩法
    /** 当大玩法下的小玩法，含有header，可以选择更多小玩法时，计算所有该打玩法下的注数 */
    func calculateSubHeaderRulesCount() -> Int {
        var localHonest_orders:[PeilvOrder] =  []
        
        for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
            
            if peilvListDatas.count > 0 {
                if !isEmptyString(str: subPlayCode) && peilvListDatas[0].parentCode == subPlayCode {
                    localHonest_orders = localHonest_orders + self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                }
            }
        }
        
        return localHonest_orders.count
    }
    
    func clearMutiPlaysBetAndReload() {
        if !peilvListDatasSuperSet.isEmpty {
            peilvListDatasSuperSet.removeAll()
        }
        
        selectedPlayRulesModel = SelectedPlayRulesModel()
        
        lhcLogic.clearHonest_oddsDic()
        
        for model in peilvListDatas {
            for inModel in model.peilvs {
                inModel.isSelected = false
            }
        }
        
        if isPeilvVersion() {
            if let model = self.selectPlay {
                self.getOddsFromAllPlayCodes(playData: model, showDialog: false)
            }
        }
        
        betMoneyWhenPeilv = ""
        handlePeilvListDatasSuperSet(peilvListDatas: peilvListDatas, peilvListDatasSuperSet: peilvListDatasSuperSet)
        
        UIView.performWithoutAnimation {
            self.numSelectTableView.reloadData()
        }
        
        self.updateBottomUI()
        selectedPlayRulesModel.clearModel()
    }
    
    func clearOfficialBetAndReload() {
        for ball in self.ballonDatas{
            for ballNumInfo in ball.ballonsInfo{
                if ballNumInfo.isSelected {
                    ballNumInfo.isSelected = false
                }
            }
            if ball.showWeiShuView{
                for weishuData in ball.weishuInfo{
                    if weishuData.isSelected {
                        weishuData.isSelected = false
                    }
                }
            }
        }
        
        self.official_orders.removeAll()
        self.numSelectTableView.reloadData()
        self.updateBottomUI()
    }
    
}

extension CRNumSelectView:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isPeilvVersion() {
            return self.peilvListDatas.count
        }else {
            return self.ballonDatas.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //官方
        if !self.isPeilvVersion() {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OFFICIAL_IDENTIFIER, for: indexPath) as? CRJianjinPaneCell else{
                fatalError("dequeueReusableCell CRJianjinPaneCell failure")
            }
            
            cell.cpTypeCode = self.cpTypeCode
            cell.playCode = self.subPlayCode
            cell.btnsDelegate = self
            
            let ruleTestStr: String! = self.ballonDatas[indexPath.row].ruleTxt
            cell.ruleUI.layer.cornerRadius = 10
            
            if ruleTestStr != nil {
                cell.ruleUI.setTitle(ruleTestStr, for: .normal)
            }
            
            cell.weishuView.cellDelegate = self
            cell.funcView.cellDelegate = self
            cell.weishuView.cellPos = indexPath.row
            cell.funcView.cellPos = indexPath.row
            cell.toggleWeishuView(show: self.ballonDatas[indexPath.row].showWeiShuView)
            
            cell.initFuncView()
            
            if self.ballonDatas[indexPath.row].showWeiShuView{
                cell.weishuView.setData(array: self.ballonDatas[indexPath.row].weishuInfo,playRuleShow:false)
            }
            
            if let cr = self.codeRank{
                if !cr.isEmpty{
                    if indexPath.row < cr.count {
                        
                        let isColdHot = self.isColdHotNormalType == 0
                        cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,codeRank: cr[indexPath.row],
                                         isColdHot:isColdHot,showCodeRank:self.showCodeRank)
                    }else{
                        cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,showCodeRank:false)
                    }
                }else{
                    cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,showCodeRank:self.showCodeRank)
                }
            }else{
                cell.fillBallons(balls: self.ballonDatas[indexPath.row].ballonsInfo,showCodeRank:self.showCodeRank)
            }
            
            let shouldHideWeiShuView = !self.ballonDatas[indexPath.row].showWeiShuView
            let shouldHideFunctionView = !self.ballonDatas[indexPath.row].showFuncView
            cell.weishuView.isHidden = shouldHideWeiShuView
            cell.funcView.isHidden = shouldHideFunctionView
            
            return cell
        }else {
            // 信用
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PEILV_IDENTIFIER) as? CRPeilvTouzhuCell  else {
                fatalError("The dequeued cell is not an instance of peilvCell.")
            }
            
            cell.selectionStyle = .none
            cell.cellDelegate = self
            if !self.peilvListDatas.isEmpty{
                if indexPath.row >= self.peilvListDatas.count{
                    return cell
                }
                let data = self.peilvListDatas[indexPath.row]
                cell.cellRow = indexPath.row
                cell.is_play_bar_show = true
//                cell.setupData(data: data,mode:is_fast_bet_mode,
//                               specialMode: lhcLogic.isSingleLineLayout(),
//                               cpCode: self.cpTypeCode,cpVerison: self.cpVersion,lotCode: self.cpBianHao)
                cell.setupData(data: data,mode:true,
                               specialMode: lhcLogic.isSingleLineLayout(),
                               cpCode: self.cpTypeCode,cpVerison: self.cpVersion,lotCode: self.cpBianHao)
            }
            
            return cell
        }
        
    }
}

//MARK: - 信用版号码球选择 PeilvCellDelegate
extension CRNumSelectView:PeilvCellDelegate {
    func onCellSelect(data: PeilvWebResult, cellIndex: Int, row: Int, volume: Bool) {
        data.parentsPlayCode = subPlayCode.isEmpty ? selectedSubPlayCode : subPlayCode
        //播放按键音
        if volume{
            if YiboPreference.isPlayTouzhuVolume(){
                playClickBallSoundHandler?()
            }
        }
        let mCell = self.numSelectTableView.cellForRow(at: IndexPath.init(row: row, section: 0))
        if mCell == nil{
            return
        }
        let cell = mCell as! CRPeilvTouzhuCell
        var cell2:CRPeilvCollectionViewCell!
        
        if let s = cell.tableContent.cellForItem(at: IndexPath.init(row: cellIndex, section: 0)){
            cell2 = s as! CRPeilvCollectionViewCell
        }
        
        if cell2 == nil{
            return
        }
        
        let clickData = self.peilvListDatas[row].peilvs[cellIndex]
        if !clickData.checkbox{
            if clickData.isSelected{
                if !isEmptyString(str: self.betMoneyWhenPeilv){
                    cell2.moneyInputTV.text = self.betMoneyWhenPeilv
                    clickData.inputMoney = Float(self.betMoneyWhenPeilv)!
                }
            }else{
                cell2.moneyInputTV.text = ""
                clickData.inputMoney = 0
            }
        }
        
        if clickData.isSelected{
            self.sortWhenSelectPeilvNumber += 1;
            clickData.selectedPosSortWhenClick = self.sortWhenSelectPeilvNumber
        }
        //开始计算投注号码串及注数
        self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
            self.updateBottomUI()
        } )
    }
    
    func callAsyncCalcZhushu(data: PeilvWebResult, cellIndex: Int, row: Int, volume: Bool) {
        onCellSelect(data: data, cellIndex: cellIndex, row: row,volume:volume)
    }
}

//MARK: - 官方版号码球选择 CellBtnsDelegate
extension CRNumSelectView:CellBtnsDelegate {
    //点击“万千百十个”位数按钮时的响应动作
    func clickWeiBtns(weiTag:Int,cellPos:Int,btnIndex:Int) -> Void {
        switch weiTag {
        default:
            let cell = self.numSelectTableView.cellForRow(at: IndexPath.init(row: cellPos, section: 0)) as! CRJianjinPaneCell
            let isSelected = self.ballonDatas[cellPos].weishuInfo?[btnIndex].isSelected
            let weiBtn = cell.viewWithTag(weiTag) as! UIButton
            print("btn tag = \(weiBtn.tag)")
            if !isSelected! {
                weiBtn.setTitleColor(UIColor.white, for: .normal)
                weiBtn.theme_backgroundColor = "Global.themeColor"
            }else{
                weiBtn.backgroundColor = UIColor.white
                weiBtn.layer.theme_borderColor = "Global.themeColor"
                weiBtn.theme_setTitleColor("Global.themeColor", forState: .normal)
            }
            
            self.ballonDatas[cellPos].weishuInfo?[btnIndex].isSelected = !isSelected!
        }
    }
    
    func clickBigSmall(isBig:Bool,cellPos:Int) -> Void{
        let ball = self.ballonDatas[cellPos]
        for index in 0...ball.ballonsInfo.count-1{
            if index >= ball.ballonsInfo.count/2{
                ball.ballonsInfo[index].isSelected = isBig ? true : false
            }else{
                ball.ballonsInfo[index].isSelected = isBig ? false : true
            }
        }
        self.numSelectTableView.reloadData()
    }
    
    func clickSingleDouble(isSingle:Bool,cellPos:Int,handler:() -> ()) -> Void {
        let ball = self.ballonDatas[cellPos]
        for ballInfo in ball.ballonsInfo{
            if isPurnInt(string: ballInfo.num){
                let scanner = Scanner(string: ballInfo.num)
                scanner.scanUpToCharacters(from: CharacterSet.decimalDigits, into: nil)
                var number :Int = 0
                scanner.scanInt(&number)
                if !isSingle{
                    if number % 2 == 0{
                        ballInfo.isSelected = true
                    }else{
                        ballInfo.isSelected = false
                    }
                }else{
                    if number % 2 == 0{
                        ballInfo.isSelected = false
                    }else{
                        ballInfo.isSelected = true
                    }
                }
            }else{
                ballInfo.isSelected = false
            }
        }
        
        handler()
    }
    
    ///奖金版下注时--点击每个列玩法球中的辅助功能按钮的响应事件
    func onBtnsClickCallback(btnTag: Int, cellPos: Int) {
        if YiboPreference.isPlayTouzhuVolume(){
//            CRBetLogic.playVolume()
            playClickBallSoundHandler?()
        }
        
        switch btnTag {
        case 20,21,22,23,24://wan
            clickWeiBtns(weiTag: btnTag, cellPos: cellPos,btnIndex: btnTag - 20)
        //all
        case 10:
            let ball = self.ballonDatas[cellPos]
            for ballNumInfo in ball.ballonsInfo{
                ballNumInfo.isSelected = true
            }
            self.numSelectTableView.reloadData()
            break
        //big
        case 11:
            self.clickBigSmall(isBig: true, cellPos: cellPos)
            break
        //small
        case 12:
            self.clickBigSmall(isBig: false, cellPos: cellPos)
            break
        //single
        case 13:
            self.clickSingleDouble(isSingle: true, cellPos: cellPos,handler: {() in
                self.numSelectTableView.reloadData()
            })
            break
        //doule
        case 14:
            self.clickSingleDouble(isSingle: false, cellPos: cellPos,handler: {() in
                self.numSelectTableView.reloadData()
            })
            break
        case 15:
            let ball = self.ballonDatas[cellPos]
            for ballNumInfo in ball.ballonsInfo{
                ballNumInfo.isSelected = false
            }
            self.numSelectTableView.reloadData()
            break
        default:
            break
        }
        
        onNumBallClickCallback(number: "", cellPos: 0)
    }
    
    ///奖金版号码选择时回调的方法
    func onNumBallClickCallback(number: String, cellPos: Int) {
        if YiboPreference.isPlayTouzhuVolume(){
            playClickBallSoundHandler?()
        }
        
        //开始计算投注号码串及注数
        calc_bet_orders(selectedDatas: self.ballonDatas,selectNumHandler: {(rightRakeback,rightMaxOdds, rightMinOdds) in

        },updateBottomHandler: {() in
            self.updateBottomUI()
        })
    }
    
    /**
     * 开始计算投注号码串及注数
     * @param selectedDatas 非机选投注时，用户已经选择完投注球的球列表数据
     * @param cpVersion 彩票版本
     * @param czCode 彩种代号
     * @param subCode 小玩法
     */
    func calc_bet_orders(selectedDatas:[BallonRules],selectNumHandler:@escaping (_ rakeback:Float,_ maxOdds:Float,_ minOdds:Float) -> (),updateBottomHandler:@escaping () -> ()) -> Void {
        DispatchQueue.global().async {
            let modeInt = CRBetLogic.convertYJFModeToInt(mode: "y")
            let orders = LotteryOrderManager.calcOrders(list: selectedDatas, rcode: self.subPlayCode, lotType: Int(self.cpTypeCode)!, rateback: 0, mode: modeInt, beishu: 1, oddsList: self.officail_odds)

            DispatchQueue.main.async {
                self.official_orders.removeAll()
                self.official_orders = self.official_orders + orders

                //更新一些特殊玩法的拖动条
                if self.subPlayCode == "qwxczw" || self.subPlayCode == "qwxdds" ||
                    self.subPlayCode == "k3hz2"{
                }

                updateBottomHandler()
            }
        }
    }
    
    //MARK: - API
    ///信用投注,更新底部视图数据
    func updateBottomUI() -> Void {
        if isPeilvVersion() {
            var currentZhushu = 0 //当前玩法注数
            var total_money:Float = 0.0
            
            var localHonest_orders:[PeilvOrder] =  []
            var isCurrentHonest_orders = false //遍历的是否是当前的 honest_orders
            
            for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
                
                if peilvListDatas.count > 0 {
                    
                    if !isEmptyString(str: subPlayCode) && peilvListDatas[0].fakeParentCode == fakeSubPlayCode && !CRBetLogic.isHeaderPlay(parentCode: subPlayCode) && !isEmptyString(str: peilvListDatas[0].fakeParentCode) {
                        peilvListDatas[0].parentName = subPlayName
                        isCurrentHonest_orders = true
                        localHonest_orders = self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                        break
                        
                    }else if !isEmptyString(str: subPlayCode) && peilvListDatas[0].parentCode == subPlayCode && !CRBetLogic.isHeaderPlay(parentCode: subPlayCode) && isEmptyString(str: fakeSubPlayCode) && isEmptyString(str: peilvListDatas[0].fakeParentCode) {
                        peilvListDatas[0].parentName = subPlayName
                        isCurrentHonest_orders = true
                        localHonest_orders = self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                        break
                    }else if !isEmptyString(str: headerCode) && peilvListDatas[0].code == headerCode && CRBetLogic.isHeaderPlay(parentCode: subPlayCode) {
                        
                        //合肖是用以判断，合-中 or 合-不中
                        if peilvListDatas[0].peilvs.count > 0 && subPlayCode == "hx" {
                            if peilvListDatas[0].peilvs[0].code == moreHeadersSubCode {
                                peilvListDatas[0].parentName = subPlayName
                                isCurrentHonest_orders = true
                                localHonest_orders = self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                                break
                            }
                        }else {
                            peilvListDatas[0].parentName = subPlayName
                            isCurrentHonest_orders = true
                            localHonest_orders = self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                            break
                        }
                        
                    }else if !isEmptyString(str: headerCode) && peilvListDatas[0].code == headerCode {
                        peilvListDatas[0].parentName = subPlayName
                        isCurrentHonest_orders = true
                        localHonest_orders = self.calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                        break
                    }
                }
            }
            
            for item in localHonest_orders {
                if isCurrentHonest_orders {
                    currentZhushu += 1
                }
                
                if !is_fast_bet_mode{
                    total_money += item.a
                    betShouldPayMoney = Double(total_money)
                    //                        print("total money \(self.betShouldPayMoney)")
                }else{
                    if is_fast_bet_mode && !isEmptyString(str: self.betMoneyWhenPeilv){
                        total_money += Float(self.betMoneyWhenPeilv) ?? 0
                        betShouldPayMoney = Double(total_money)
                        print("betMoneyWhenPeilv = \(betMoneyWhenPeilv) 元")
                        print("total_money = \(total_money) 元")
                        print("betShouldPayMoney = \(betShouldPayMoney) 元")
                        print("")
                    }else{
                        total_money += item.a
                    }
                }
            }
            
            if CRBetLogic.isHeaderPlay(parentCode: subPlayCode) {
                self.selectedPlayRulesModel.currentConunt = calculateSubHeaderRulesCount()
            }else {
                self.selectedPlayRulesModel.currentConunt = currentZhushu
            }
            
            let count = self.selectedPlayRulesModel.getTotoalCount()
            
            if is_fast_bet_mode && !isEmptyString(str: self.betMoneyWhenPeilv) {
                total_money = Float(Float(count) * (Float(self.betMoneyWhenPeilv) ?? 0))

            }
            
            let betShouldPayMoneyStr = String.init(format: "%d注,%.3f元", count,total_money)
            self.betInfoHandler?(count,total_money,betShouldPayMoneyStr)
            
            if self.selectedPlayRulesModel.getTotoalCount() > 200 {
                showToast(view: self, txt: "最多选中200注")
            }
            
        }else {
            var zhushu = 0
            var total_money:Float = 0.0
            if !self.official_orders.isEmpty{
                for item in self.official_orders{
                    zhushu += item.n
                }
            }
            total_money = (Float(zhushu) * Float(1) * Float(2))
            
            let total_moneyStr = String.init(format: "%.3f",(total_money * multiplyValue * Float(selectedBeishu)))
            var betShouldPayMoneyStr = ""
            
            if let total_moneyFloat = Float(total_moneyStr) {
                betShouldPayMoneyStr = String.init(format: "共选%d注,共%.2f元",zhushu,total_moneyFloat)
                self.betInfoHandler?(zhushu,total_moneyFloat,betShouldPayMoneyStr)
            }
        }
    }
    
    //MARK: 格式化赔率版 数据，进入投注页面
    func formPeilvBetDataAndEnterOrderPage(isRandom:Bool = false,order:[PeilvOrder],peilvs:[BcLotteryPlay],lhcLogic:LHCLogic2,handler:() -> ()) {
        var datas = [OrderDataInfo]()
        
        if peilvListDatasSuperSet.count > 0 {
            for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
                let data = calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                
                for order in data{
                    if !isEmptyString(str: self.betMoneyWhenPeilv) {
                        order.a = Float(self.betMoneyWhenPeilv)!
                    }
                }
                
                if data.count > 0 {
                    datas = datas + fromPeilvBetOrder(peilv_orders: data, subPlayName: peilvListDatas[0].parentName, subPlayCode: peilvListDatas[0].parentCode, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: 1)
                }
            }
        }
        
        handler()
        
        let current_rate:Float = 1
        self.gotoBetNewPeilvHandler?(datas,peilvs,lhcLogic,subPlayName,self.subPlayCode,self.cpTypeCode,self.cpBianHao,current_rate,self.cpName,self.cpVersion,"")
    }
    
    func gotoBetNewPeilvWith(handler:([OrderDataInfo],[BcLotteryPlay],LHCLogic2,String,String,String,String,Float,String,String,String) -> ()) {
        
        var datas = [OrderDataInfo]()
        
        if peilvListDatasSuperSet.count > 0 {
            for (_,peilvListDatas) in peilvListDatasSuperSet.enumerated() {
                let data = calculateBetDatasWithPeilvListDatas(peilvListDatas: peilvListDatas)
                
                for order in data{
                    if !isEmptyString(str: self.betMoneyWhenPeilv) {
                        order.a = Float(self.betMoneyWhenPeilv)!
                    }
                }
                
                if data.count > 0 {
                    
                    let name = isEmptyString(str: peilvListDatas[0].fakeParentName) ? peilvListDatas[0].parentName : peilvListDatas[0].fakeParentName
                    
                    datas = datas + fromPeilvBetOrder(peilv_orders: data, subPlayName:
                        name, subPlayCode: peilvListDatas[0].parentCode, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: 1)
                }
            }
        }
        
        let current_rate:Float = 1
         handler(datas,self.peilvListDatas,lhcLogic,subPlayName,self.subPlayCode,self.cpTypeCode,self.cpBianHao,current_rate,self.cpName,self.cpVersion,"")
    }
    
    func formBetDataAndEnterOrderPageWith(handler:(_ official_orders:[OfficialOrder],_ subPlayName:String,_ subPlayCode:String,_ selectedBeishu:Int,_ cpTypeCode:String,_ cpBianHao:String,_ current_rate:Float,_ selectMode:String,_ cpName:String,_ cpVersion:String) -> ()) {
        
        handler(self.official_orders,self.subPlayName,self.subPlayCode,1,self.cpTypeCode,self.cpBianHao,0,"y",self.cpName,self.cpVersion)
    }
}

extension CRNumSelectView : PlayButtonDelegate {
    func onButtonDelegate() {
        if let play = self.choosedPlay{
            let map = lhcLogic.getListWhenSpecialClick(play: play)
            if map != nil {
                lhcLogic.setFixFirstNumCountWhenLHC(fixCount: (map?.keys.contains("fixFirstCount"))! ? map!["fixFirstCount"] as! Int : 0)
                let list = (map?.keys.contains("datas"))! ? map!["datas"] : ([] as AnyObject)
                if let datas = list as? [BcLotteryPlay] {
                    for (_,data) in datas.enumerated() {
                        data.parentCode = subPlayCode
                    }
                    //判断是否 是 合肖
                    let data = datas[0]
                    if data.peilvs.count > 0 && subPlayCode == "hx"{
                        moreHeadersSubCode = data.peilvs[0].code
                    }
                    
                    self.prapare_peilv_datas_for_tableview(peilvWebResults:datas)
                }
                
                if isPeilvVersion() {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        //开始计算投注号码串及注数
                        self.calc_bet_orders_for_honest(datasAfterSelected: self.peilvListDatas,updateBottomUIHandler:{() in
                            self.updateBottomUI()
                        } )
                    }
                }
            }

            self.numSelectTableView.reloadData()
        }
    }
}




