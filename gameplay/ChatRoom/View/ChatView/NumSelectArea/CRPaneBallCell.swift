//
//  CRPaneBallCell.swift
//  gameplay
//
//  Created by admin on 2019/8/17.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRPaneBallCell: UICollectionViewCell {
    @IBOutlet weak var ballBtn:UIButton!
    @IBOutlet weak var rectAngleBtn: UIButton!
    @IBOutlet weak var codeRankUI:UILabel!
    
//    lazy var xibView:UIView = {
//        return Bundle.main.loadNibNamed("CRPaneBallCell", owner: self, options: nil)?.first as! UIView
//    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rectAngleBtn.titleLabel?.font = UIFont.init(name: "Helvetica-Bold", size: 16)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        xibView.frame = bounds
//        addSubview(xibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        addSubview(self.xibView)
//        xibView.frame = self.bounds
    }
}
