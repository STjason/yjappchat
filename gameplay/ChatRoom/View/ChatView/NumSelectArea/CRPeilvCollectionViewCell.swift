//
//  PeilvCollectionViewCell.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/13.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

import UIKit
//赔率版投注界面列表项我中collectionview cell
class CRPeilvCollectionViewCell: UICollectionViewCell,UITextFieldDelegate {
    
    var posInListView = 0
    var posInGridview = 0
    var moneyDelegate:PeilvMoneyInputDelegate?
    
    //冠亚组合的球的第 二球
    @IBOutlet weak var secondButton: UIButton!
    //冠亚和值的球 / 冠亚组合的球的第一球
    @IBOutlet weak var firstSecondButton: UIButton!
    @IBOutlet weak var numberTV:UIButton!
    @IBOutlet weak var numberTVF:UIButton!
    @IBOutlet weak var numberTVS:UIButton!
    @IBOutlet weak var numberLabelTV:UILabel!
    @IBOutlet weak var peilvTV:UILabel!
    @IBOutlet weak var secondPeilvTV:UILabel!
    @IBOutlet weak var moneyInputTV:CustomFeildText!
    @IBOutlet weak var money_input_height_constraint:NSLayoutConstraint!
    
    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var leftLine: UIView!
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var rightLine: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        numberTV.titleLabel?.font = UIFont.init(name: "Helvetica-Bold", size: 16)
        numberLabelTV.font = UIFont.init(name: "Helvetica-Bold", size: 18.5)
        
        moneyInputTV.delegate = self
        moneyInputTV.addTarget(self, action: #selector(onTextChange(ui:)), for: .editingChanged)
        moneyInputTV.background = nil
        moneyInputTV.backgroundColor = UIColor.clear
        moneyInputTV.layer.cornerRadius = 5
        moneyInputTV.layer.borderColor = UIColor.white.cgColor
        moneyInputTV.layer.borderWidth = 1
        
        self.backgroundColor = UIColor.white.withAlphaComponent(0)
        topLine.backgroundColor = UIColor.white
        leftLine.backgroundColor = UIColor.white
        bottomLine.backgroundColor = UIColor.white
        rightLine.backgroundColor = UIColor.white
    }
    
    func setupData(data:PeilvWebResult?,mode:Bool,lotCode:String,lotType:String){
        if data == nil{
            return
        }
        
        numberTVF.isHidden = true
        numberTV.isHidden = true
        
        if (firstSecondButton != nil) && (secondButton != nil) {
            firstSecondButton.isHidden = true
            firstSecondButton.setTitle("", for: .normal)
            firstSecondButton.setBackgroundImage(nil, for: .normal)
            firstSecondButton.layer.cornerRadius = 0
            firstSecondButton.backgroundColor = UIColor.clear
            
            secondButton.isHidden = true
            secondButton.setTitle("", for: .normal)
            secondButton.setBackgroundImage(nil, for: .normal)
            secondButton.layer.cornerRadius = 0
            secondButton.backgroundColor = UIColor.clear
        }
        
        numberLabelTV.backgroundColor = UIColor.clear
        
        let codeName = data?.playCode
        
        if codeName == "gyz" { //冠亚组合
            if (firstSecondButton != nil) && (secondButton != nil) {
                numberLabelTV.isHidden = true
                numberTVS.isHidden = true
                firstSecondButton.isHidden = false
                secondButton.isHidden = false
                
                firstSecondButton.whc_Width(24).whc_Height(24)
                secondButton.whc_Width(24).whc_Height(24)
                
                let numName = data?.numName ?? ""
                let numArray = numName.components(separatedBy: "_")
                if numArray.count != 2 {
                    return
                }
                
                let imgNameFirst = figureSaiCheImage(num: numArray[0])
                let imgNameSecond = figureSaiCheImage(num: numArray[1])
                
                firstSecondButton.setTitle("", for: .normal)
                secondButton.setTitle("", for: .normal)
                firstSecondButton.setBackgroundImage(UIImage.init(named: imgNameFirst), for: .normal)
                secondButton.setBackgroundImage(UIImage.init(named: imgNameSecond), for: .normal)
            }
        }else if !isPurnInt(string: (data?.numName)!){
            numberLabelTV.isHidden = false
            numberTVS.isHidden = true
            numberLabelTV.text = data?.numName
        }else {
            numberLabelTV.isHidden = true
            numberTVS.isHidden = false
            numberTV.whc_Width(32).whc_Height(32)
//            numberTV.setBackgroundImage(UIImage.init(named: "bet_grey_ball"), for: .normal)
            
            if isKuaiLeShiFeng(lotType: lotType) {
                    numberTVS.setTitleColor(UIColor.red, for: .normal)
                    let imgName = figureXYNCImage(num: (data?.numName)!)
                    if !isEmptyString(str: imgName){
                        numberLabelTV.isHidden = true
                        numberTVS.isHidden = false
                        numberTVS.isUserInteractionEnabled = false
                        numberTVS.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
                        numberTVS.setTitle("", for: .normal)
                    }else{
                        numberLabelTV.isHidden = false
                        numberTVS.isHidden = true
                        numberLabelTV.text = data?.numName
                    }

            } else if isSaiche(lotType: lotType) {
            
                print("playCode = \(String(describing: data?.playCode))")
                
            if codeName == "dgj" || codeName == "ddshm" || codeName == "ddsm"  || codeName == "ddsim" || codeName == "ddwm" || codeName == "ddlm" || codeName == "ddqm" || codeName == "ddbm" || codeName == "ddjm" || codeName == "dyj" {
            
                numberTVS.setTitleColor(UIColor.red, for: .normal)
                let imgName = figureSaiCheImage(num: (data?.numName)!)
                if !isEmptyString(str: imgName){
                    numberLabelTV.isHidden = true
                    numberTVS.isHidden = false
                    numberTVS.isUserInteractionEnabled = false
                    numberTVS.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
                    numberTV.whc_Width(32).whc_Height(32)
                    numberTVS.setTitle("", for: .normal)
                }else{
                    numberLabelTV.isHidden = false
                    numberTVS.isHidden = true
                    numberLabelTV.text = data?.numName
                }
            }else if codeName == "gyh" { //冠亚和值
                if (firstSecondButton != nil) && (secondButton != nil) {
                    numberLabelTV.isHidden = true
                    numberTVS.isHidden = true
                    
                    firstSecondButton.whc_Width(28).whc_Height(28)
                    secondButton.whc_Width(28).whc_Height(28)

                    secondButton.isHidden = false
                    secondButton.setTitle(data?.numName, for: .normal)
                    
                    let hexColor = getRacingFirstSecondBGColor(with: data?.numName ?? "")
                    
                    if !hexColor.isEmpty {
                        secondButton.backgroundColor = UIColor.colorWithHexString(hexColor)
                        secondButton.layer.cornerRadius = 14
                        secondButton.layer.masksToBounds = true
                    }
                }
            }else {
                numberLabelTV.isHidden = false
                numberTVS.isHidden = true
                numberLabelTV.text = data?.numName
            }
            
           } else if isSixMark(lotCode: lotCode){
                let imgName = figureLhcImages(num: (data?.numName)!)
                numberTVS.setTitleColor(switch_LHC_option_icons() ? UIColor.white : UIColor.black, for: .normal)
                numberTVS.titleLabel?.font = UIFont.systemFont(ofSize: 14)
                numberTVS.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
                numberTVS.setTitle(data?.numName, for: .normal)
            }else if isKuai3(lotType: lotType){
                var nums = [String]()
                if (data?.playCode ?? "") == "dp" || (data?.playCode ?? "") == "cp" {
                    let numName = (data?.numName ?? "")
                    nums = separateStringToArray(string: numName)
                }
                
                let imgName = figureKuai3Images(num: (data?.numName)!)

                if !isEmptyString(str: imgName) || nums.count == 2 { //count == 2来判断短牌，长牌.即需要图片展示的球⚽️
                    numberLabelTV.isHidden = true
                    numberTVS.isHidden = false
                    numberTVS.isUserInteractionEnabled = false
                    numberTV.whc_Width(32).whc_Height(32)
                    numberTVS.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
                    numberTVS.setTitle("", for: .normal)
                           
                    let playCode = (data?.playCode ?? "")
                    if playCode == "qws" { //全骰
                        numberTVF.isHidden = false
                        numberTV.isHidden = false
                        numberTV.whc_Width(26).whc_Height(26)
                    }else if playCode == "dp" || playCode == "cp" { //短牌，长牌
                        numberTV.isHidden = false
                        numberTVF.isHidden = true
                    }else {
                        numberTVF.isHidden = true
                        numberTV.isHidden = true
                    }
                    
                    if (data?.playCode ?? "") == "qws" {
                        numberTVF.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
                        numberTV.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
                        
                        numberTVF.setTitle("", for: .normal)
                        numberTV.setTitle("", for: .normal)
                    }else if (playCode == "dp" || playCode == "cp") {
                        let imgNameLeft = figureKuai3Images(num: nums[0])
                        let imgNameRight = figureKuai3Images(num: nums[1])
                        numberTVS.setBackgroundImage(UIImage.init(named: imgNameLeft), for: .normal)
                        numberTV.setBackgroundImage(UIImage.init(named: imgNameRight), for: .normal)
                        
                        numberTVS.setTitle("", for: .normal)
                        numberTV.setTitle("", for: .normal)
                    }
                }else{
                    numberLabelTV.isHidden = false
                    numberLabelTV.text = data?.numName
                    
                    numberTVF.isHidden = true
                    numberTVS.isHidden = true
                    numberTV.isHidden = true
                }
            }else{
                numberTVS.setTitle(data?.numName, for: .normal)
                numberTVS.setTitleColor(UIColor.black, for: .normal)
                numberTVS.setBackgroundImage(UIImage.init(named: "ballDark_plain_blue"), for: .normal)
            }
        }
        
        setupPeilv(dataP: data)
        
        if (data?.inputMoney)! > Float(0){
            moneyInputTV.text = String.init(format: "%.3f", (data?.inputMoney)!)
        }else{
            moneyInputTV.text = ""
        }
        if mode{
            moneyInputTV.isHidden = true
        }else{
            moneyInputTV.isHidden = false
        }
    }
    
    private func setupPeilv(dataP:PeilvWebResult?) {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 3
        formatter.minimumFractionDigits = 1
        if let data = dataP {
            // 三中二，和二中特
            if !(data.code.hasPrefix("sze") || data.code.hasPrefix("ezt")) {
                if showTwopeilv() {
                    let string = String.init(format: "%f", (data.minOdds))
                    let double: Double = (Double(string)! * 10000).rounded() / 10000
//                    var resultString: String = String.init(format: "%.3lf", (double))
//                    if resultString.length > 5{
//                        resultString = resultString.subString(start: 0, length: 5)
//                    }
                    if let floatValue = Float(string) {
                        //                        secondPeilvTV.text = "\(floatValue)"
//                        peilvTV.text = resultString
                        peilvTV.text = formatter.string(from: NSNumber(value: double))
                        secondPeilvTV.isHidden = false
                    }else {
                        secondPeilvTV.text = ""
                        secondPeilvTV.isHidden = true
                    }
                }else {
                    secondPeilvTV.text = ""
                    secondPeilvTV.isHidden = true
                }
                
                if (data.currentOdds) > Float(0){
                    let string = String.init(format:"%f",data.currentOdds)
                    let double: Double = (Double(string)! * 10000).rounded() / 10000
//                    var resultString: String = String.init(format: "%.3lf", (double))
//                    if resultString.length > 5{
//                        resultString = resultString.subString(start: 0, length: 5)
//                    }
                    if let floatValue = Float(string) {
                        //                        peilvTV.text = "\(floatValue)"
//                        peilvTV.text = resultString
                        peilvTV.text = formatter.string(from: NSNumber(value: double))
                    }else {
                        peilvTV.text = ""
                    }
                }else{
                    var string = String.init(format: "%f", (data.maxOdds))
//                    if string.length > 5{
//                        string = string.subString(start: 0, length: 5)
//                    }
                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
                        peilvTV.text = formatter.string(from: NSNumber(value: data.maxOdds))
                    }else {
                        peilvTV.text = ""
                    }
                }
            }else {
                // 三中二，和二中特
                if (data.currentOdds) > Float(0){
                    var string = String.init(format: "%f", (data.currentOdds))
//                    if string.length > 5{
//                        string = string.subString(start: 0, length: 5)
//                    }
                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
                        peilvTV.text = formatter.string(from: NSNumber(value: data.currentOdds))
                    }else {
                        peilvTV.text = ""
                    }
                }else{
                    var  string = String.init(format: "%f", (data.maxOdds))
//                    if string.length > 5{
//                        string = string.subString(start: 0, length: 5)
//                    }
                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
                        peilvTV.text = formatter.string(from: NSNumber(value: data.maxOdds))
                    }else {
                        peilvTV.text = ""
                    }
                }
                
                
                var string = String.init(format: "%f", (data.currentSecondOdds))
//                if string.length > 5{
//                    string = string.subString(start: 0, length: 5)
//                }
                if let floatValue = Float(string) {
//                    secondPeilvTV.text = "\(floatValue)"
                    secondPeilvTV.text = formatter.string(from: NSNumber(value: data.currentSecondOdds))
                    secondPeilvTV.isHidden = false
                }else {
                    secondPeilvTV.text = ""
                    secondPeilvTV.isHidden = true
                }
                
                
            }
        }
    }
    
    //金额输入框内容变化回调
    @objc func onTextChange(ui:UITextField) -> Void {
        var moneyValue = ui.text!
        
        if !isEmptyString(str: moneyValue) {
            let max = Int.max
            if let doubleSvalue:Double = Double(moneyValue) {
                if doubleSvalue > Double(max) {
                    showToast(view: self, txt: "输入数值过大")
                    moneyValue = moneyValue.subString(start: 0, length: moneyValue.length - 1)
                    ui.text = moneyValue
                }
            }
        }
        
        if let delegate = self.moneyDelegate{
            delegate.onMoneyChange(money: moneyValue, gridPos: posInGridview, listViewPos: posInListView)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func figureLhcImages(num:String) -> String {
        let redBO = ["1","2","7","8","12","13","18","19","23","24","29","30","34","35","40","45","46"]
        let blueBO = ["3","4","9","10","14","15","20","25","26","31","36","37","41","42","47","48"]
        let greenBO = ["5","6","11","16","17","21","22","27","28","32","33","38","39","43","44","49"]
        if redBO.contains(num){
            return switch_LHC_option_icons() ? "lhc_red_bg" :  "lhc_red_bg_3D"
        }else if blueBO.contains(num){
            return switch_LHC_option_icons() ? "lhc_blue_bg" :  "lhc_blue_bg_3D"
        }else if greenBO.contains(num){
            return switch_LHC_option_icons() ? "lhc_green_bg" :  "lhc_green_bg_3D"
        }
        return switch_LHC_option_icons() ?  "lhc_red_bg" :  "lhc_red_bg_3D"
    }
    
    func figureKuai3Images(num:String) -> String {
        switch num {
        case "1":
            return "kuai3_bg_one"
        case "2":
            return "kuai3_bg_two"
        case "3":
            return "kuai3_bg_three"
        case "4":
            return "kuai3_bg_four"
        case "5":
            return "kuai3_bg_five"
        case "6":
            return "kuai3_bg_six"
        default:
            break
        }
        return ""
    }
    
}

//
//  CRPeilvCollectionViewCell.swift
//  YiboGameIos
//
//  Created by yibo-johnson on 2018/1/13.
//  Copyright © 2018年 com.lvwenhan. All rights reserved.
//

//import UIKit
////赔率版投注界面列表项我中collectionview cell
//class CRPeilvCollectionViewCell: UICollectionViewCell,UITextFieldDelegate {
//
//    var posInListView = 0
//    var posInGridview = 0
//    var moneyDelegate:PeilvMoneyInputDelegate?
//
//    @IBOutlet weak var numberTV:UIButton!
//    @IBOutlet weak var numberLabelTV:UILabel!
//    @IBOutlet weak var peilvTV:UILabel!
//    @IBOutlet weak var secondPeilvTV:UILabel!
//    @IBOutlet weak var moneyInputTV:CustomFeildText!
//    @IBOutlet weak var money_input_height_constraint:NSLayoutConstraint!
//
//    @IBOutlet weak var topLine: UIView!
//    @IBOutlet weak var leftLine: UIView!
//    @IBOutlet weak var bottomLine: UIView!
//    @IBOutlet weak var rightLine: UIView!
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//

//
//    func setupData(data:PeilvWebResult?,mode:Bool,lotCode:String,lotType:String){
//        if data == nil{
//            return
//        }
//
//        if !isPurnInt(string: (data?.numName)!){
//            numberLabelTV.isHidden = false
//            numberTV.isHidden = true
//            numberLabelTV.text = data?.numName
//        }else{
//            numberLabelTV.isHidden = true
//            numberTV.isHidden = false
//            numberTV.whc_Width(32).whc_Height(32)
//            //            numberTV.setBackgroundImage(UIImage.init(named: "bet_grey_ball"), for: .normal)

//
//            if isKuaiLeShiFeng(lotType: lotType) {
//                numberTV.setTitleColor(UIColor.red, for: .normal)
//                let imgName = figureXYNCImage(num: (data?.numName)!)
//                if !isEmptyString(str: imgName){
//                    numberLabelTV.isHidden = true
//                    numberTV.isHidden = false
//                    numberTV.isUserInteractionEnabled = false
//                    numberTV.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
//                    numberTV.setTitle("", for: .normal)
//                }else{
//                    numberLabelTV.isHidden = false
//                    numberTV.isHidden = true
//                    numberLabelTV.text = data?.numName
//                }
//
//            } else if isSaiche(lotType: lotType) {
//
//                print("playCode = \(String(describing: data?.playCode))")
//
//                let codeName = data?.playCode
//                if codeName == "dgj" || codeName == "ddshm" || codeName == "ddsm"  || codeName == "ddsim" || codeName == "ddwm" || codeName == "ddlm" || codeName == "ddqm" || codeName == "ddbm" || codeName == "ddjm" || codeName == "dyj" {
//
//                    numberTV.setTitleColor(UIColor.red, for: .normal)
//                    let imgName = figureSaiCheImage(num: (data?.numName)!)
//                    if !isEmptyString(str: imgName){
//                        numberLabelTV.isHidden = true
//                        numberTV.isHidden = false
//                        numberTV.isUserInteractionEnabled = false
//                        numberTV.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
//                        numberTV.whc_Width(26).whc_Height(26)
//                        numberTV.setTitle("", for: .normal)
//                    }else{
//                        numberLabelTV.isHidden = false
//                        numberTV.isHidden = true
//                        numberLabelTV.text = data?.numName
//                    }
//                }else {
//                    numberLabelTV.isHidden = false
//                    numberTV.isHidden = true
//                    numberLabelTV.text = data?.numName
//                }
//
//            } else if isSixMark(lotCode: lotCode){
//                let imgName = figureLhcImages(num: (data?.numName)!)
//                numberTV.setTitleColor(switch_LHC_option_icons() ? UIColor.white : UIColor.black, for: .normal)
//                numberTV.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//                numberTV.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
//                numberTV.setTitle(data?.numName, for: .normal)
//            }else if isKuai3(lotType: lotType){
//                let imgName = figureKuai3Images(num: (data?.numName)!)
//                if !isEmptyString(str: imgName){
//                    numberLabelTV.isHidden = true
//                    numberTV.isHidden = false
//                    numberTV.isUserInteractionEnabled = false
//                    numberTV.whc_Width(26).whc_Height(26)
//                    numberTV.setBackgroundImage(UIImage.init(named: imgName), for: .normal)
//                    numberTV.setTitle("", for: .normal)
//                }else{
//                    numberLabelTV.isHidden = false
//                    numberTV.isHidden = true
//                    numberLabelTV.text = data?.numName
//                }
//            }else{
//                numberTV.setTitle(data?.numName, for: .normal)
//                numberTV.setTitleColor(UIColor.black, for: .normal)
//                numberTV.setBackgroundImage(UIImage.init(named: "ballDark_plain_blue"), for: .normal)
//            }
//        }
//
//        setupPeilv(dataP: data)
//
//        if (data?.inputMoney)! > Float(0){
//            moneyInputTV.text = String.init(format: "%.3f", (data?.inputMoney)!)
//        }else{
//            moneyInputTV.text = ""
//        }
//        if mode{
//            moneyInputTV.isHidden = true
//        }else{
//            moneyInputTV.isHidden = false
//        }
//    }
//
//    private func setupPeilv(dataP:PeilvWebResult?) {
//        if let data = dataP {
//            if !(data.code.hasPrefix("sze") || data.code.hasPrefix("ezt")) {
//                if showTwopeilv() {
//                    let string = String.init(format: "%.3f", (data.minOdds))
//                    if let floatValue = Float(string) {
//                        secondPeilvTV.text = "\(floatValue)"
//                        secondPeilvTV.isHidden = false
//                    }else {
//                        secondPeilvTV.text = ""
//                        secondPeilvTV.isHidden = true
//                    }
//                }else {
//                    secondPeilvTV.text = ""
//                    secondPeilvTV.isHidden = true
//                }
//
//                if (data.currentOdds) > Float(0){
//                    let string = String.init(format: "%.3f", (data.currentOdds))
//                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
//                    }else {
//                        peilvTV.text = ""
//                    }
//                }else{
//                    let string = String.init(format: "%.3f", (data.maxOdds))
//                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
//                    }else {
//                        peilvTV.text = ""
//                    }
//                }
//            }else {
//                // 三中二，和二中特
//                if (data.currentOdds) > Float(0){
//                    let string = String.init(format: "%.3f", (data.currentOdds))
//                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
//                    }else {
//                        peilvTV.text = ""
//                    }
//                }else{
//                    let string = String.init(format: "%.3f", (data.maxOdds))
//                    if let floatValue = Float(string) {
//                        peilvTV.text = "\(floatValue)"
//                    }else {
//                        peilvTV.text = ""
//                    }
//                }
//
//
//                let string = String.init(format: "%.3f", (data.currentSecondOdds))
//                if let floatValue = Float(string) {
//                    secondPeilvTV.text = "\(floatValue)"
//                    secondPeilvTV.isHidden = false
//                }else {
//                    secondPeilvTV.text = ""
//                    secondPeilvTV.isHidden = true
//                }
//
//
//            }
//        }
//    }
//
//    //金额输入框内容变化回调
//    @objc func onTextChange(ui:UITextField) -> Void {
//        var moneyValue = ui.text!
//
//        if !isEmptyString(str: moneyValue) {
//            let max = Int.max
//            if let doubleSvalue:Double = Double(moneyValue) {
//                if doubleSvalue > Double(max) {
//                    showToast(view: self, txt: "输入数值过大")
//                    moneyValue = moneyValue.subString(start: 0, length: moneyValue.length - 1)
//                    ui.text = moneyValue
//                }
//            }
//        }
//
//        if let delegate = self.moneyDelegate{
//            delegate.onMoneyChange(money: moneyValue, gridPos: posInGridview, listViewPos: posInListView)
//        }
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
//
//    func figureLhcImages(num:String) -> String {
//        let redBO = ["1","2","7","8","12","13","18","19","23","24","29","30","34","35","40","45","46"]
//        let blueBO = ["3","4","9","10","14","15","20","25","26","31","36","37","41","42","47","48"]
//        let greenBO = ["5","6","11","16","17","21","22","27","28","32","33","38","39","43","44","49"]
//        if redBO.contains(num){
//            return switch_LHC_option_icons() ? "lhc_red_bg" :  "lhc_red_bg_3D"
//        }else if blueBO.contains(num){
//            return switch_LHC_option_icons() ? "lhc_blue_bg" :  "lhc_blue_bg_3D"
//        }else if greenBO.contains(num){
//            return switch_LHC_option_icons() ? "lhc_green_bg" :  "lhc_green_bg_3D"
//        }
//        return switch_LHC_option_icons() ?  "lhc_red_bg" :  "lhc_red_bg_3D"
//    }
//
//    func figureKuai3Images(num:String) -> String {
//        switch num {
//        case "1":
//            return "kuai3_bg_one"
//        case "2":
//            return "kuai3_bg_two"
//        case "3":
//            return "kuai3_bg_three"
//        case "4":
//            return "kuai3_bg_four"
//        case "5":
//            return "kuai3_bg_five"
//        case "6":
//            return "kuai3_bg_six"
//        default:
//            break
//        }
//        return ""
//    }
//
//}
//
/////////////////////////////////////////////////////////////////////////////////////////
