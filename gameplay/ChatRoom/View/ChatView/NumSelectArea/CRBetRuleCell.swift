//
//  CRBetRuleCell.swift
//  gameplay
//
//  Created by admin on 2019/8/15.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetRuleCell: UICollectionViewCell {
    lazy var titleLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("required CRBetRuleCell init?(coder aDecoder: NSCoder) failure")
    }
    
    private func setupUI() {
        self.addSubview(self.titleLabel)
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
    }
    
    func configWith(title:String) {
        self.titleLabel.text = title
    }
    
    func configUI(isSelected:Bool,hadSelected:Bool = false) -> Void {
        if isSelected{
            self.backgroundColor = UIColor.init(hexString: "#ec2829")
        }else if hadSelected {
            self.backgroundColor = UIColor.init(hexString: "#F48888")
        }else{
            self.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
    }
    
}






