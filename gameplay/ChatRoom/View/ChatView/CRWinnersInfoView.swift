//
//  CRWinnersInfoView.swift
//  gameplay
//
//  Created by admin on 2019/10/21.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRWinnersInfoView: UIView {
    var closeHandler:(() -> Void)?
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        closeButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
    }
    
    @objc func closeAction() {
        closeWinnersInfoView()
    }
    
    func closeWinnersInfoView() {
        closeHandler?()
    }
}
