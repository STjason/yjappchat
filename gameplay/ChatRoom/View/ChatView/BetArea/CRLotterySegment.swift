//
//  CRLotterySegment.swift
//  gameplay
//
//  Created by admin on 2019/8/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRLotterySegment: UIView {
    
    enum DirectionCorner {
        case directionCornerLeft
        case directionCornerRight
    }
    
    var leftClickHandler:(() -> Void)?
    var rightClickHandler:(() -> Void)?
    
    var buttonLeftSize = CGSize.init(width: 90, height: 40)
    let buttonRightSize = CGSize.init(width: 90, height: 40)
    
    lazy private var leftButton:UIButton = {
        let button = self.getSegItem(backgroundColor:UIColor.black.withAlphaComponent(0.8), textColor: UIColor.orange, directionCorner: .directionCornerLeft)
        button.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        button.setTitle("彩种名称", for: .normal)
        return button
    }()
    
    lazy var rightButton:UIButton = {
        let button = self.getSegItem(backgroundColor:UIColor.black.withAlphaComponent(0.65), textColor: UIColor.white, directionCorner: .directionCornerRight)
        button.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        button.setTitle("切换彩种", for: .normal)
        return button
    }()

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupUI()
//    }
    
    convenience init(leftButtonWidth: CGFloat) {
        self.init()
        buttonLeftSize = CGSize.init(width: leftButtonWidth, height: 40)
        setupUI()
    }
    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    private func setupUI() {
        self.isHidden = true
        
        addSubview(self.leftButton)
        addSubview(self.rightButton)
        layout()
    }
    
    private func layout() {
        leftButton.snp.makeConstraints { (make) in
            make.top.left.bottom.equalTo(0)
        }
        
        self.rightButton.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.left.equalTo(weakSelf.leftButton.snp.right)
                make.width.equalTo(90)
                make.right.equalTo(0)
            }
        }
    }
    
    private func getSegItem(backgroundColor:UIColor,textColor:UIColor,directionCorner:DirectionCorner) -> UIButton {
        let button = UIButton()
        button.backgroundColor = backgroundColor
        button.setTitleColor(textColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        
        let path = UIBezierPath.init(roundedRect: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: directionCorner == .directionCornerLeft ? buttonLeftSize : buttonRightSize), byRoundingCorners: directionCorner == .directionCornerLeft ? .topLeft : .topRight, cornerRadii: CGSize.init(width: 5, height: 5))
        let mask = CAShapeLayer.init()
        mask.path = path.cgPath
        button.layer.mask = mask
        
        return button
    }
    
    //MARK: - EVENTS
    @objc private func leftButtonAction() {
        leftClickHandler?()
    }
    
    @objc private func rightButtonAction() {
        rightClickHandler?()
    }
    
    //MARK: - API
    func show(show:Bool) {
        self.isHidden = !show
    }
    
    ///更换彩种名字
    func updateLotteryName(name:String) {
//        let label = UILabel()
//        label.font = UIFont.systemFont(ofSize: 16)
//        label.text = name
//        let size = label.sizeThatFits(CGSize.init(width: 120, height: CGFloat(MAXFLOAT)))
//
//        leftButton.snp.remakeConstraints { (make) in
//            make.top.left.bottom.equalTo(0)
//            make.width.equalTo(size.width + 15)
//        }
        
        leftButton.setTitle(name, for: .normal)
    }
}







