//
//  CRBetTopView.swift
//  gameplay
//
//  Created by admin on 2019/8/13.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetTopView: UIView {
    @IBOutlet private weak var playRuleNameLabel: UILabel!
    @IBOutlet private weak var previousPeriodLabel: UILabel!
    @IBOutlet private weak var balanceLabel: UILabel!
    @IBOutlet private weak var currentPeriodLabel: UILabel!
    @IBOutlet private weak var countdownLabel: UILabel!
    @IBOutlet weak var leftCheckButton: UIButton!
    @IBOutlet weak var rightCheckButton: UIButton!
    @IBOutlet weak var numViews: BallsView!
    
    
    var showViewIndexChangeHandler:((_ index:Int) -> Void)?
    /// type:0 冷热 1 遗漏  2 快捷 3 一般
    var checkButtonHandler:((_ type:Int) -> Void)?
    var checkButtonType = 0 {
        didSet {
            checkButtonHandler?(checkButtonType)
        }
    }
    
    private var isPeilvVersion = false { //是否是赔率版,默认官方
        didSet {
            self.configHotColdOrFastMormal(isPeilv: isPeilvVersion, isLeft: true)
        }
    }
    
    /// 0:玩法选择view，1:号码选择view，2:注单确认view
    private var showViewIndex = 1 {
        didSet {
            self.showViewIndexChangeHandler?(showViewIndex)
        }
    }
    
    var tapRuleNameHandler:((_ showViewIndex:Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupUI()
    }
    
    private func setupUI() {
        self.playRuleNameLabel.isUserInteractionEnabled = true
        let playRuleNameGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapRuleNameGesture))
        self.playRuleNameLabel.addGestureRecognizer(playRuleNameGesture)
        
        self.leftCheckButton.addTarget(self, action: #selector(leftButtonClick), for: .touchUpInside)
        self.rightCheckButton.addTarget(self, action: #selector(rightButtonClick), for: .touchUpInside)
    }
    
    //MARK: - EVENTS
    @objc private func leftButtonClick(sender:UIButton) {
        self.rightCheckButton.isSelected = false
        self.leftCheckButton.isSelected = true
    }
    
    @objc private func rightButtonClick(sender:UIButton) {
        self.leftCheckButton.isSelected = false
        self.rightCheckButton.isSelected = true
    }
    
    @objc private func tapRuleNameGesture() {
        if self.showViewIndex == 1 {
            self.showViewIndex = 0
        }else if self.showViewIndex == 2 {
            self.showViewIndex = 1
        }
        
        self.tapRuleNameHandler?(self.showViewIndex)
    }
    
    //MARK: - private
    private func configHotColdOrFastMormal(isPeilv:Bool,isLeft:Bool) {
        
        self.leftCheckButton.isSelected = isLeft ? true : false
        self.rightCheckButton.isSelected = isLeft ? false : true
        
        if isPeilv {
            self.checkButtonType = isLeft ? 0 : 1
            self.leftCheckButton.setTitle("冷热", for: .normal)
            self.rightCheckButton.setTitle("遗漏", for: .normal)
        }else {
            self.checkButtonType = isLeft ? 2 : 3
            self.leftCheckButton.setTitle("快捷", for: .normal)
            self.rightCheckButton.setTitle("一般", for: .normal)
        }
    }
    
    //MARK: - API
    func shownormalFastButtons(show:Bool) {
//        self.leftCheckButton.isHidden = !show
//        self.rightCheckButton.isHidden = !show
    }
    
    func updateHotColdOrFastNormal(isPeilv:Bool) {
        self.isPeilvVersion = isPeilv
    }
    
    ///更新当前视图index
    func updateShowRuleView(isShowRuleView:Int) {
        self.showViewIndex = isShowRuleView
    }
    
    /// 0:玩法选择view，1:号码选择view，2:注单确认view
    func getShowViewIndex() -> Int {
        return self.showViewIndex
    }
    
    func updateRule(name:String) {
        self.playRuleNameLabel.text = "\(name) >"
    }
    
    func updatePreviousPeriod(period:String) {
        self.previousPeriodLabel.text = period
    }
    
    func updateBalance(balance:String) {
        self.balanceLabel.text = balance
    }
    
    func updateCurrentPeriod(period:String) {
        self.currentPeriodLabel.text = period
    }
    
    func updateCountdown(time:String) {
        self.countdownLabel.text = time
    }
}
