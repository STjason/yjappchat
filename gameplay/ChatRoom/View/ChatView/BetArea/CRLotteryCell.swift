//
//  CRLotteryCell.swift
//  gameplay
//
//  Created by admin on 2019/8/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class CRLotteryCell: UICollectionViewCell {
    var lotteryImage = UIImageView()
    lazy var lotteryName:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()
    
    private let lotteryNameH:CGFloat = 40
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.addSubview(self.lotteryImage)
        self.addSubview(self.lotteryName)
        
        self.lotteryImage.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.left.right.equalTo(0)
                make.bottom.equalTo(weakSelf.lotteryName.snp.top)
            }
        }
        
        self.lotteryName.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(0)
            make.height.equalTo(lotteryNameH)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configWith(model:LotteryData) {
        self.lotteryName.text = model.name
        
        if let code = model.code {
            self.upateLotImage(groupLotCode: code,lotteryIcon: model.lotteryIcon)
        }
    }
    
    func upateLotImage(groupLotCode:String,lotteryIcon: String){
        if isEmptyString(str: lotteryIcon) {
            let imageURL = URL(string: BASE_URL + PORT + "/native/resources/images/" + groupLotCode + ".png")
            if let url = imageURL{
                self.lotteryImage.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }else {
            if let url = URL(string: lotteryIcon) {
                self.lotteryImage.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
    }
    
}
