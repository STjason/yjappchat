//
//  CRLotteryArea.swift
//  gameplay
//
//  Created by admin on 2019/8/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRLotteryArea: UIView {
    //选择某个彩种
    var lotterySelectHandler:((_ lottery:LotteryData) ->Void)?
    var officalOrCreditHandler:((_ index:Int) -> Void)?
    
    //collection相关
    let IDENTIFIER = "CRLotteryCell"
    var numsInHorizontal = 4 //每行个数
    var minimumLine:CGFloat = 30 //水平间距
    var sectionHorMargin:CGFloat = 15 //水水平间距
    var sectionTopMargin:CGFloat = 65  //竖直间距
    var sectionBottomMargin:CGFloat = 25  //竖直间距
    var itemSize:CGSize = .zero
    let animateTime:TimeInterval = 0.3
    let minimumInteritem:CGFloat = 10 //垂直间距
    private let lotteryNameH:CGFloat = 40 //lotteryName 高度
    
    var collectionH:CGFloat = 0 //collection's height
    var collectionW:CGFloat = 0
    var lotteryModels:[LotteryData] = [LotteryData]() { //彩种数组
        didSet {
            self.updateLotteryWith()
        }
    }
    
    lazy var officalCreditSeg:UISegmentedControl = {
        let seg = UISegmentedControl.init()
        let config = getSystemConfigFromJson()

        let playKindsOrder = (config?.content.switch_xfwf=="off" ? [0, 1] : [1, 0])
        seg.isHidden = false
        seg.insertSegment(withTitle: "官方", at: playKindsOrder.first!, animated: false)
        seg.insertSegment(withTitle: "信用", at: playKindsOrder[1], animated: false)

        seg.selectedSegmentIndex = 0
        
        if #available(iOS 13.0, *) {
            seg.setBackgroundImage(UIImage(), for: .normal, barMetrics: .default)
            seg.setBackgroundImage(UIImage().createImageWithColor(color: UIColor.white), for: .selected, barMetrics: .default)
            
            let font = UIFont.boldSystemFont(ofSize: 17)
            // selected text color
            seg.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font:font], for: .selected)
            // default text color
            seg.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:font], for: .normal)
        }
        
        seg.backgroundColor = UIColor.black
        seg.tintColor = UIColor.white
        
        seg.addTarget(self, action: #selector(segValueChangeAction), for: .valueChanged)
        
        return seg
    }()
    
    lazy var collection:UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: sectionTopMargin, left: sectionHorMargin, bottom: sectionBottomMargin,  right: sectionHorMargin)
        layout.minimumLineSpacing = minimumLine
        layout.minimumInteritemSpacing = minimumInteritem
        layout.itemSize = self.itemSize
        
        var view = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        view.register(CRLotteryCell.self, forCellWithReuseIdentifier: IDENTIFIER)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = UIColor.clear
        
        view.isPagingEnabled = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    convenience init(size:CGSize,models:[LotteryData]) {
        self.init()
        self.collectionW = size.width
        self.collectionH = size.height
        
        // 计算设置 item的 size
        let itemWidth = (self.collectionW - 2 * sectionHorMargin - CGFloat(numsInHorizontal - 1) * minimumLine) / CGFloat(numsInHorizontal) //每个item的宽
        self.itemSize = CGSize.init(width: itemWidth, height: itemWidth + self.lotteryNameH)
        
        self.setupUI()
        self.lotteryModels = models
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init?(coder aDecoder: NSCoder) failure")
    }
    
    private func setupUI() {
        self.addSubview(self.collection)
        self.addSubview(self.officalCreditSeg)
        self.bringSubviewToFront( self.officalCreditSeg)
        
        self.layout()
    }
    
    private func layout() {
        self.collection.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        self.officalCreditSeg.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(2)
                make.centerX.equalTo(weakSelf.snp.centerX)
                make.width.equalTo(120)
                make.height.equalTo(40)
            }
        }
    }
    
    /// 根据数据刷新彩种视图
    func updateLotteryWith() {
//        let pages = calculatePages(models: self.lotteryModels)
//        self.collection.contentSize = CGSize.init(width: self.collectionW * CGFloat(pages), height: self.collectionH)
        self.collection.reloadData()
    }
    
    ///计算共多少页
    private func calculatePages(models:[String]) -> Int {
        let itemsPerPage = 8 //每页最多展示个数
        let remainder = models.count % itemsPerPage
        let results = models.count / itemsPerPage
        let pages = remainder == 0 ? results : results + 1
        return pages
    }
    
    //MARK: - API
    /// 根据数据刷新彩种视图
    func reloadViewWith(models:[LotteryData]) {
        self.lotteryModels = models
        self.collection.reloadData()
    }
    
    //MARK: - EVENTS
    @objc private func segValueChangeAction(sender:UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        self.officalOrCreditHandler?(index)
    }
}

extension CRLotteryArea:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.lotteryModels[indexPath.row]
        if isSixMark(lotCode: model.code ?? "") {
            model.lotVersion = 2
        }
        
        self.lotterySelectHandler?(model)
    }
}


extension CRLotteryArea:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.lotteryModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IDENTIFIER, for: indexPath) as? CRLotteryCell else {fatalError("dequeueReusableCell CRLotteryCell failure")}
        
        let model = self.lotteryModels[indexPath.row]
        cell.configWith(model:model)
        return cell
    }
    
}


