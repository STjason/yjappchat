//
//  CRBetArea.swift
//  gameplay
//
//  Created by admin on 2019/8/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit

class CRBetArea: UIView {
    ///本页面是否显示
    var isShow = false
    private var lotteryH:CGFloat = 350 //彩种选择视图height
    private let segmentH:CGFloat = 40 //切换彩种segment
    private let animationTime:TimeInterval = 0.35 //彩种和号码选择视图切换动画时间

    // 切换彩种的segment
    lazy var segment:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    lazy var betView:UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.clear
       return view
    }()
    
    lazy var lotteryView:CRLotteryArea = {
        let view = CRLotteryArea.init(size: CGSize.init(width: screenWidth, height: self.lotteryH), models: self.lotteryModels)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    lazy var numSelectArea:CRNumSelectArea = {
        let view = CRNumSelectArea()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
//    lazy var lotterySegment:CRLotterySegment = {
//        let view = CRLotterySegment.init(frame: .zero)
//        view.backgroundColor = UIColor.clear
//        return view
//    }()
    
    var lotterySegment:CRLotterySegment?
    
    var lotteryModels:[LotteryData] = [LotteryData]() { //彩种数组
        didSet {
            self.lotteryView.reloadViewWith(models: lotteryModels)
        }
    }
    
    convenience init (models:[LotteryData]) {
        self.init()
        self.lotteryModels = models
        self.setupUI()
    }
    
    private override init(frame: CGRect) {
        super.init(frame: frame)   
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configShow() {
        self.isShow = !self.isShow
    }
    
    func getIsShow() -> Bool {
        return self.isShow
    }
    
    func reloadWithModels() {
        self.lotteryView.reloadViewWith(models: lotteryModels)
    }
    
    //MARK: - UI
    private func setupUI() {
        self.layer.masksToBounds = true
        self.addSubview(self.segment)
        self.addSubview(self.betView)
//        self.segment.addSubview(self.lotterySegment)
        self.betView.addSubview(self.lotteryView)
        self.betView.addSubview(self.numSelectArea)
        
        self.layout()
    }
    
    private func layout() {
        self.betView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.bottom.equalTo(0)
                make.left.right.equalTo(0)
                make.height.equalTo(weakSelf.lotteryH)
            }
        }
        
        self.segment.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.bottom.equalTo(weakSelf.betView.snp.top)
                make.height.equalTo(weakSelf.segmentH)
                make.left.right.equalTo(0)
            }
        }
        
//        let segSize = self.lotterySegment.buttonSize
//        self.lotterySegment.snp.makeConstraints { (make) in
//            make.top.bottom.equalTo(0)
//            make.left.equalTo(20)
//            make.width.equalTo(segSize.width * 2)
//        }
        
        self.lotteryView.snp.makeConstraints {[weak self] (make) in
            if let _ = self {
                make.edges.equalTo(UIEdgeInsets.zero)
            }
        }
        
        self.numSelectArea.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.lotteryView.snp.top)
                make.left.equalTo(weakSelf.lotteryView.snp.right)
                make.bottom.equalTo(weakSelf.lotteryView.snp.bottom)
                make.width.equalTo(weakSelf.lotteryView.snp.width)
            }
        }
        
    }
    
    //MARK: - API
    ///获得本视图的高度
    func getHeight() -> CGFloat {
        return self.lotteryH + self.segmentH
    }
    
    func updateWithModels(models:[LotteryData]) {
        self.lotteryModels = models
    }
    
    ///显示彩种视图
    func showLotteryArea() {
        self.lotteryView.snp.remakeConstraints {[weak self] (make) in
            if let _ = self {
                make.edges.equalTo(UIEdgeInsets.zero)
            }
        }
        
        UIView.animate(withDuration: self.animationTime) {
            self.betView.layoutIfNeeded()
        }
    }
    
    ///显示投注页面
    func showNumSelectArea() {
        self.lotteryView.snp.remakeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.bottom.equalTo(0)
                make.right.equalTo(weakSelf.betView.snp.left)
                make.width.equalTo(weakSelf.betView.snp.width)
            }
        }
        
        UIView.animate(withDuration: self.animationTime) {
            self.betView.layoutIfNeeded()
        }
    }
    
    func hideBetView(){
        
    }

}

















