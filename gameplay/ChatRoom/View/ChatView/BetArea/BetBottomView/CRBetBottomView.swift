//
//  CRBetBottomView.swift
//  gameplay
//
//  Created by admin on 2019/8/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRBetBottomView: UIView {
    var cancelHandler:(() -> Void)?
    var confirmHandler:(() -> Void)?
    @IBOutlet weak var cacelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    
    private func setupUI() {
        self.cacelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        self.confirmButton.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        self.cacelButton.layer.cornerRadius = 5
        self.confirmButton.layer.cornerRadius = 5
    }
    
    @objc private func cancelAction() {
        self.cancelHandler?()
    }
    
    @objc private func confirmAction() {
        self.confirmHandler?()
    }
    
    func updateInfoLabel(info:String) {
        self.infoLabel.text = info
    }
    
    func updateButtons(isConfirmView:Bool) {
        self.confirmButton.setTitle(isConfirmView ? "投注" : "确定", for: .normal)
        self.cacelButton.setTitle(isConfirmView ? "返回" : "清除", for: .normal)
    }
}




