
//
//  CRInputAreaView.swift
//  Chatroom
//
//  Created by admin on 2019/7/13.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit

enum inputStatus {
    /**普通状态*/
    case Normal
    /**正在输入*/
    case Typing
    /**禁止输入*/
    case Prohibit
}
enum YJKeyboardType {
    /**文本编辑*/
    case text
    /**发语音消息*/
    case voice
    /**表情*/
    case emoji
}

protocol refreshMessageDelegate:NSObjectProtocol {
    func refreshMessageLocation()
}
//用正则表达式匹配@消息 @消息后面须加一个空格
let kATRegular = "[@\\u4e00-\\u9fa5\\w\\-\\_]+ "

class CRInputAreaView: UIView {
    let bandAllTips = "房间全体禁言" //房间禁言提示
    let bandPersonalTips = "您已被禁言" //个人禁言提示
    let buttonWidth:CGFloat = 38
    
    ///申述
    var appealHandler:(() -> Void)?
    var sendMessageHandler:((_ message:String?) -> Void)?
    ///isSelected则弹出更多视图，否则隐藏
    var moreFunctionHandler:((_ showMoreFunction:Bool) -> Void)?
    
    var textFieldString :String = ""
    //记录输入框状态
    var textFieldStatus:inputStatus = .Normal
    //默认键盘类型
    var keyboardType:YJKeyboardType = .text
    //刷新代理
    weak var refreshDelegate:refreshMessageDelegate?
    //输入栏高度
    weak var chatBarDelegate:CRIputViewDelegate?
    /**键盘代理*/
    weak var keyboradDelegate:CRKeyboardDelegate?
    /** 👿👿👿*/
    weak var emojiKeyboardDelegate:CREmojiKeyboardDelegate?
    
//    weak var moreKeyboarddelegate:CRMoreKeyboardDelegate?
    /**记录当前键盘的状态*/
    var status:CRChatBarStatus = .Init
    
    /**禁止输入*/
    var prohibitInput:Bool = false{
        didSet{
            setViewInteractionEnabled(enabled: !prohibitInput)
            inputField.textColor = prohibitInput ? UIColor.gray:UIColor.black
            //更新UI
            textFieldStatus = prohibitInput ? .Prohibit:.Normal
            setupInputAreaViewUI()
        }
    }
    
    //设置控件交互
    func setViewInteractionEnabled(enabled:Bool) {
        voiceButton.isUserInteractionEnabled = enabled
        emotionButton.isUserInteractionEnabled = enabled
        moreFunctionButton.isUserInteractionEnabled = enabled
        inputField.isEditable = enabled
    }
    
    //MARK: - 控件
    lazy var appealBandBtn:UIButton = {
        let view = UIButton()
        view.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        view.setTitleColor(UIColor.red, for: .normal)
        view.setTitle("点击申诉", for: .normal)
        view.addTarget(self, action: #selector(appealAction), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    lazy var inputField:UITextView = {
        let view = UITextView()
//        view.addTarget(self, action: #selector(inputWords(textField:)), for: UIControl.Event.editingChanged)
        view.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        view.layer.cornerRadius = 3
        view.font = UIFont.systemFont(ofSize: 16)
        view.returnKeyType = .send
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.colorWithRGB(r: 0, g: 0, b: 0, alpha: 0.3).cgColor
        view.autocorrectionType = .no
        view.scrollsToTop = false
        view.delegate = self
        view.isUserInteractionEnabled = true
        return view
    }()
    //切换音频和文字输入
    lazy var voiceButton:UIButton = {
        let view = UIButton()
        view.isSelected = false
        view.addTarget(self, action: #selector(tapVoiceOrText), for: .touchUpInside)
        view.setImage(UIImage.init(named: "ToolViewInputVoice"), for: .normal)
        view.setImage(UIImage.init(named: "ToolViewKeyboard"), for: .selected)
        return view
    }()
    
    lazy var speakerBar:CRVoiceButton = {
        
        let view = CRVoiceButton.init(frame: CGRect.zero)
        view.titleLabel.text = "按住说话(限时15s)"
        view.isHidden = true
        weak var weakSelf = self
        view.setTouchBeginAction(touchBegin: {[weak self] () -> (Void) in
            self?.chatBarDelegate?.chatBarStartRecording(chatBar: self!)
            }, willTouchCancel: { [weak self] (isCancel:Bool) -> (Void) in
                self?.chatBarDelegate?.chatBarWillCancelRecording(chatBar: self!, cancel: isCancel)
            }, touchEnd: { [weak self] () -> (Void) in
                self?.chatBarDelegate?.chatBarFinishedRecoding(chatBar: self!)
            }, touchCancel: {[weak self] () -> (Void) in
                self?.chatBarDelegate?.chatBarDidCancelRecording(chatBar: self!)
        })
        return view
    }()
    
    lazy var emotionButton:UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "ToolViewEmotion"), for: .normal)
        view.addTarget(self, action: #selector(emojiClick(button:)), for: .touchUpInside)
        return view
    }()
    
    lazy var moreFunctionButton:UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "TypeSelectorBtn_Black"), for: .normal)
        view.addTarget(self, action: #selector(tapMoreFunction), for: .touchUpInside)
        return view
    }()
    //发送消息
    lazy var sendButton:UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 2
        button.layer.masksToBounds = true
        button.isHidden = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.addTarget(self, action: #selector(sendMessage), for: .touchUpInside)
        button.setTitle("发送", for:.normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.blue
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewInteractionEnabled(enabled: false)
        self.setupUI()
    }
    
    func layoutVoicceButton(show:Bool) {
        let topBottomMargin:CGFloat = 5
        
        voiceButton.snp.remakeConstraints { (make) in
            make.top.equalTo(topBottomMargin)
            make.leading.equalTo(10)
            make.bottom.equalTo(topBottomMargin * -1)
            make.width.equalTo((show ? buttonWidth : 0))
        }
        
        if !show && voiceButton.isSelected {
            tapVoiceOrText(sender: voiceButton)
        }
    }
    
    //更新被禁言状态
    func updateWith(isRoomBand:Bool,isPersonBand:Bool) {
        prohibitInput = isRoomBand || isPersonBand
        appealBandBtn.removeFromSuperview()
        
        if isRoomBand {
            inputField.text = bandAllTips
        }else if isPersonBand {
            inputField.text = bandPersonalTips
            
            addSubview(appealBandBtn)
            
            appealBandBtn.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.width.equalTo(80)
                make.height.equalToSuperview()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.appealBandBtn.isHidden = false
            }
        }else {
            inputField.text = ""
        }
    }
    
    //设置UI
    private func setupUI() {
        self.addSubview(self.voiceButton)
        self.addSubview(self.inputField)
        self.addSubview(self.speakerBar)
        self.addSubview(self.emotionButton)
        self.addSubview(self.moreFunctionButton)
        self.addSubview(self.sendButton)
        
        let topBottomMargin:CGFloat = 5
        
        layoutVoicceButton(show: true)
        
        self.inputField.snp.makeConstraints {[weak self] (make) in
            make.top.equalTo(8)
            if let weakSelf = self {
                make.leading.equalTo(weakSelf.voiceButton.snp.trailing).offset(5)
                make.trailing.equalTo(weakSelf.emotionButton.snp.leading).offset(-5)
                make.height.equalTo(HEIGHT_CHATBAR_TEXTVIEW)
            }
            make.bottom.equalTo(-8)
        }
        
        //表情
        if CRDefaults.getChatroomSendExpression() == true{
            self.emotionButton.isHidden = false
            self.emotionButton.setImage(UIImage(named: "ToolViewEmotion"), for: .normal)
            self.emotionButton.snp.remakeConstraints {[weak self] (make) in
                make.top.equalTo(topBottomMargin)
                if let weakSelf = self {
                    make.width.equalTo(buttonWidth)
                    make.right.equalTo(weakSelf.moreFunctionButton.snp.left).offset(-5)
                }
                make.bottom.equalTo(topBottomMargin * -1)
            }
        }else{
            //被禁止发送表情
            self.emotionButton.isHidden = true
            self.emotionButton.setImage(UIImage.init(named: ""), for: .normal)
            self.emotionButton.snp.remakeConstraints {[weak self] (make) in
                make.top.equalTo(topBottomMargin)
                if let weakSelf = self {
                    
                    make.right.equalTo(weakSelf.moreFunctionButton.snp.left).offset(-5)
                }
                make.bottom.equalTo(topBottomMargin * -1)
                make.width.equalTo(0)
            }
        }
        
        self.speakerBar.snp.makeConstraints {[weak self] (make) in
            make.top.equalTo(8)
            if let weakSelf = self {
                make.leading.equalTo(weakSelf.voiceButton.snp.trailing).offset(5)
                make.trailing.equalTo(weakSelf.emotionButton.snp.leading).offset(-5)
            }
            make.bottom.equalTo(-8)
        }
        
        self.moreFunctionButton.snp.makeConstraints {[weak self] (make) in
            make.top.equalTo(topBottomMargin)
            if let weakSelf = self {
                make.width.equalTo(buttonWidth)
            }
            make.bottom.equalTo(topBottomMargin * -1)
            make.right.equalTo(0)
            
        }
        
        self.sendButton.snp.makeConstraints { (make) in
            make.right.equalTo(-topBottomMargin)
            make.height.equalTo(28)
            make.width.equalTo(48)
            make.centerY.equalTo(inputField.snp.centerY)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Events
    @objc func appealAction() {
        appealHandler?()
    }
    
    @objc private func tapVoiceOrText(sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            inputField.resignFirstResponder()
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: status, toStatus: .Voice)
            status = .Voice
        }
        self.speakerBar.isHidden = !sender.isSelected //voiceButton选中时为发语音
        self.inputField.isHidden = sender.isSelected
        textFieldStatus = .Normal
        setupInputAreaViewUI()
        inputField.text = ""
        print("点击语音切换当前键盘输入状态" + "\(status)")
    }
    //MARK：更多的功能的点击事件
    @objc private func tapMoreFunction(sender:UIButton){
        sender.isSelected = !sender.isSelected
        self.inputField.resignFirstResponder()
        self.moreFunctionHandler?(sender.isSelected)
        if voiceButton.isSelected{
            //发送音频还是选中状态
            voiceButton.isSelected = false
            inputField.isHidden = false
            speakerBar.isHidden = true
        }
        //当前状态
        if status == .More {
            status = .Init
            self.inputField.becomeFirstResponder()
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: .More, toStatus: .Keyboard)
            
        }else if status == .Emoji{
            
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: status, toStatus: .More)
            status = .More
            
        }else if status == .Voice{
            //当前状态是音频
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: status, toStatus: .More)
            status = .More
            
        }else{
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: status, toStatus: .More)
            status = .More
        }
    }}
//MARK:响应事件
extension CRInputAreaView{
    //输入框的事件
    @objc private func inputWords(textField:UITextField){

        textFieldString = textField.text ?? ""
        if textFieldString.isEmpty{
            textFieldStatus = .Normal
        }else{
            textFieldStatus = .Typing
        }
        //更新UI
        setupInputAreaViewUI()
    }
    //发送消息
    @objc private func sendMessage(){
        
        sendCurrentText()
        textFieldString = ""
        textFieldStatus = .Normal
        setupInputAreaViewUI()
        self.inputField.text = nil;
    }
    //emoji表情功能
    @objc private func emojiClick(button:UIButton){
        // 开始文字输入
        if (status == .Emoji) {
        
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: self.status, toStatus: .Keyboard)
            self.emotionButton.setImage(UIImage(named: "kEmojiImage"), for: .normal)
            self.emotionButton.setImage(UIImage(named: "kEmojiImageHL"), for: .highlighted)
            self.inputField.becomeFirstResponder()
            self.speakerBar.isHidden = true
            self.inputField.isHidden = false
            self.status = .Keyboard;
        }else {
            // 打开表情键盘
            self.chatBarDelegate?.chatBar(chatBar: self, fromStatus: self.status, toStatus: .Emoji)
            if (self.status == .Voice) {
                
                self.voiceButton.isSelected = false
//                self.voiceButton.setImage(UIImage(named: "kVoiceImage"), for: .normal)
//                self.voiceButton.setImage(UIImage(named: "kVoiceImageHL"), for: .highlighted)
                self.speakerBar.isHidden = true
                self.inputField.isHidden = false
                
            }else if (self.status == .More) {
                //语音切到原始状态
                self.speakerBar.isHidden = true
                self.inputField.isHidden = false
                self.voiceButton.isSelected = false
                self.moreFunctionButton.setImage(UIImage.init(named: "TypeSelectorBtn_Black"), for: .normal)
            }else{
                
            }
            self.emotionButton.setImage(UIImage(named: "ToolViewKeyboard"), for: .normal)
            self.emotionButton.setImage(UIImage(named: "ToolViewKeyboard"), for: .highlighted)
            self.inputField.resignFirstResponder()
            status = .Emoji;
        }
    }
}
extension CRInputAreaView{
    //更新UI
    func setupInputAreaViewUI(){
        if textFieldStatus == .Normal{
            //普通状态
            sendButton.isHidden = true
            moreFunctionButton.isHidden = false
            inputField.snp.updateConstraints { (make) in
                make.trailing.equalTo(emotionButton.snp.leading).offset(-5)
                make.height.equalTo(HEIGHT_CHATBAR_TEXTVIEW)
            }
            if CRDefaults.getChatroomSendExpression() == true{
                self.emotionButton.isHidden = false
                self.emotionButton.snp.remakeConstraints {(make) in
                    make.top.equalTo(5)
                    make.width.equalTo(buttonWidth)
                    make.right.equalTo(moreFunctionButton.snp.left).offset(-5)
                    make.bottom.equalTo(5 * -1)
                }
            }else{
                self.emotionButton.isHidden = true
                self.emotionButton.snp.remakeConstraints {(make) in
                    make.top.equalTo(5)
                    make.width.equalTo(0)
                    make.right.equalTo(moreFunctionButton.snp.left).offset(-5)
                    make.bottom.equalTo(5 * -1)
                }
            }
          
        }else if (textFieldStatus == .Typing){
            //正在输入
            sendButton.isHidden = false
            moreFunctionButton.isHidden = true
            inputField.snp.updateConstraints { (make) in
                make.trailing.equalTo(emotionButton.snp.leading).offset(-10)
            }
            
            if CRDefaults.getChatroomSendExpression() == true{
                self.emotionButton.isHidden = false
                self.emotionButton.snp.remakeConstraints { (make) in
                    make.top.equalTo(5)
                    make.width.equalTo(buttonWidth)
                    make.right.equalTo(sendButton.snp.left).offset(-5)
                    make.bottom.equalTo(5 * -1)
                }
            }else{
                self.emotionButton.isHidden = true
                self.emotionButton.snp.remakeConstraints { (make) in
                    make.top.equalTo(5)
                    make.width.equalTo(0)
                    make.right.equalTo(sendButton.snp.left).offset(-5)
                    make.bottom.equalTo(5 * -1)
                }
            }
           
        }else if (textFieldStatus == .Prohibit){
            //禁止输入
            sendButton.isHidden = true
            moreFunctionButton.isHidden = false
            
            inputField.snp.updateConstraints { (make) in
                make.trailing.equalTo(emotionButton.snp.leading).offset(-5)
            }
            self.emotionButton.snp.remakeConstraints {(make) in
                make.top.equalTo(5)
                make.width.equalTo(buttonWidth)
                make.right.equalTo(moreFunctionButton.snp.left).offset(-5)
                make.bottom.equalTo(5 * -1)
            }
        }
    }
}

//MARK:--UITextViewDelegate
extension CRInputAreaView:UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        p_reloadTextViewWithAnimation(animation: true)
    }
    func textViewDidChange(_ textView: UITextView) {
        p_reloadTextViewWithAnimation(animation: true)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //判断用户按回车键发送消息
        if text == "\n"{
            self.sendCurrentText()
            textView.text = ""
            return false
        }else if text == ""{
//            configAt(range:NSRange(location: range.location, length: 1))
            let selectRange = textView.selectedRange
            if selectRange.length > 0{
                return true
            }
            var string = inputField.text as? NSString
            let matches = findAllAt()
            var inAt:Bool = false
            var index = range.location
            for match:NSTextCheckingResult in matches ?? []{
                let newRange = NSRange(location: match.range.location + 1, length: match.range.length - 1)
                if NSLocationInRange(range.location, newRange){
                    inAt = true
                    index = match.range.location
                    //整体替换
                    //把NSRange 转化成Range
                    string = (string?.replacingCharacters(in: match.range, with: "") as! NSString)
//                    string =  string?.replacingCharacters(in:match.range, with:"")
                    break
                }
            }
            if inAt{
                textView.text = string as! String
                textView.selectedRange = NSRange.init(location: index, length: 0)
                return false
            }
        }

        return true
    }
    func textViewDidChangeSelection(_ textView: UITextView) {
        let sendString = inputField.text.replacingOccurrences(of: " ", with: "")
        if sendString.length > 0 {
            textFieldStatus = prohibitInput ? .Prohibit:.Typing
        }else{
            textFieldStatus = .Normal
        }
        setupInputAreaViewUI()
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.status != CRChatBarStatus.Keyboard {
            chatBarDelegate?.chatBar(chatBar: self, fromStatus: self.status, toStatus: CRChatBarStatus.Keyboard)
            if self.status == CRChatBarStatus.Emoji{
                self.emotionButton.setImage(UIImage(named: "chat_toolbar_emotion"), for: .normal)
                self.emotionButton.setImage(UIImage(named: "chat_toolbar_emotion_HL"), for: .highlighted)
            }else if (self.status == CRChatBarStatus.More){
                self.moreFunctionButton.setImage(UIImage(named: "chat_toolbar_more"), for: .normal)
                self.moreFunctionButton.setImage(UIImage(named: "chat_toolbar_more_HL"), for: .highlighted)
            }
            self.status = CRChatBarStatus.Keyboard
        }
        return true
    }
    
    //发送文字
    func sendCurrentText(){
        let sendString = inputField.text.replacingOccurrences(of: " ", with: "")
        if sendString.length > 0 {
            self.chatBarDelegate?.chatBar(text: inputField.text)
        }
        self.inputField.text = ""
        self.p_reloadTextViewWithAnimation(animation: true)
    }
    
    func p_reloadTextViewWithAnimation(animation:Bool){
        let textHeight = inputField.sizeThatFits(CGSize(width: self.inputField.width, height: CGFloat(MAXFLOAT))).height
        var height = Double(textHeight) > HEIGHT_CHATBAR_TEXTVIEW ? Double(textHeight) : HEIGHT_CHATBAR_TEXTVIEW
        height = Double(textHeight) <= HEIGHT_MAX_CHATBAR_TEXTVIEW ? Double(textHeight): HEIGHT_MAX_CHATBAR_TEXTVIEW
        if Double(textHeight) > height {
            inputField.isScrollEnabled = true
        }else {
            inputField.isScrollEnabled = false
        }
        if  height != Double(self.inputField.frame.height) {
            if animation {
                UIView.animate(withDuration: 0.2, animations: {
                    self.inputField.snp.updateConstraints({ (make) in
                        make.height.equalTo(height)
                    })
                    self.chatBarDelegate?.chatBarDidChangeTextView(chatBar: self, height: self.inputField.frame.height)
                }) { (_) in
                    if Double(textHeight) > height{
                        self.inputField.setContentOffset(CGPoint(x: 0, y: Double(textHeight) - height), animated: true)
                    }
                    self.chatBarDelegate?.chatBarDidChangeTextView(chatBar: self, height: CGFloat(height))
                }
            }else {
                self.inputField.snp.updateConstraints { (make) in
                    make.height.equalTo(height)
                }
                self.chatBarDelegate?.chatBarDidChangeTextView(chatBar: self, height: CGFloat(height))
                if Double(textHeight) > height{
                    self.inputField.setContentOffset(CGPoint(x: 0, y: Double(textHeight) - height), animated: true)
                }
            }
        }else if (Double(textHeight) > height){
            if (animation){
                let offsetY = self.inputField.contentSize.height - self.inputField.frame.height
                DispatchQueue.main.async {
                    self.inputField.setContentOffset(CGPoint(x: 0, y: offsetY), animated: true)
                }
                
            }else {
                self.inputField.setContentOffset(CGPoint(x: CGFloat(0), y: self.inputField.contentSize.height - self.inputField.frame.height), animated: false)
            }
        }
    }

    //MARK:找到输入框中所有的@消息
    func findAllAt() -> [NSTextCheckingResult]?{
        let string = inputField.text ?? ""
        let regex = try? NSRegularExpression(pattern: kATRegular, options: .caseInsensitive)
        let matches = regex?.matches(in: string, options: .reportProgress, range: NSRange.init(location: 0, length: string.length))
        return matches
    }
}
//MARK:emoji方法
extension CRInputAreaView{
    //删除emoji
    func deleteLastCharacter(){
        
        self.inputField.deleteBackward()
    }
    //输入emoji
    func addEmojiString(emojiString:String){
        
        let emojiUntcode =  decTohex(number: Int(emojiString)!)
        
        //实例化字符扫描
        let scanner = Scanner(string: emojiUntcode)
        
        //从emojiString中扫描出十六进制的数值
        var result:UInt32 = 0
        scanner.scanHexInt32(&result)
        //使用Uint32的数值，生成一个UTF8的字符
        let c = Character(UnicodeScalar(result)!)
        
        let emoji = String(c)
        
        print(emoji)
        
        let str = String.init(format:"%@%@",self.inputField.text,emoji)
        self.inputField.text = str
        p_reloadTextViewWithAnimation(animation: true)
    }
    // MARK: - 十进制转十六进制
     func decTohex(number:Int) -> String {
        return String(format: "%0X", number)
    }
}



