//
//  CRAnnounceView.swift
//  Chatroom
//
//  Created by admin on 2019/7/13.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit

class CRAnnounceView: UIView {
    var titleLabel = CRMarqueeView.init(frame: .zero)
    var tipsLabel:UILabel = {
        let label = UILabel.init(frame: .zero)
        label.textColor = UIColor.red
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.text = "最新公告"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        let separateLine = UIView()
        separateLine.backgroundColor = UIColor.groupTableViewBackground
        
        self.addSubview(self.tipsLabel)
        self.addSubview(separateLine)
        self.addSubview(self.titleLabel)
        
        separateLine.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(5)
                make.right.equalTo(weakSelf.tipsLabel.snp.right).offset(-5)
                make.bottom.equalTo(-5)
                make.width.equalTo(2)
            }
        }
        
        self.tipsLabel.snp.makeConstraints {(make) in
            make.top.left.bottom.equalTo(0)
            make.width.equalTo(90)
        }
        
        self.titleLabel.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(0)
                make.left.equalTo(weakSelf.tipsLabel.snp.right).offset(0)
                make.bottom.equalTo(0)
                make.right.equalTo(-5)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configWith(title:String) {
        self.titleLabel.setupView(title: title)
    }
    
}
