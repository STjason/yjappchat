//
//  CRRedPacketDetailCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/30.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRRedPacketDetailCell: UITableViewCell {
    /**领取红包的用户icon*/
    lazy var userIcon:UIImageView = {
        let icon = UIImageView()
        icon.layer.cornerRadius = 40 * 0.5
        icon.layer.masksToBounds = true
        return icon
    }()
    /**领取红包的用户昵称*/
    lazy var userName:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //当前用户领取💰💰💰💰💰💰💰💰💰💰
    lazy var receiveMoneyLabel:UILabel = {
        let moneyLabel = UILabel()
        moneyLabel.font = UIFont.systemFont(ofSize: 14)
        moneyLabel.textColor = UIColor.colorWithHexString("#ffc001")
        return moneyLabel
    }()
    //
    var receiveDetailItem:CRReceiveDetailOther?{
        didSet{
            guard let receivePeopleDetailItem = receiveDetailItem else {return}
             userName.text = receivePeopleDetailItem.nickName
            if receivePeopleDetailItem.nickName.isEmpty {
                userName.text = receivePeopleDetailItem.nativeAccount
            }
            let imageUrlString = receivePeopleDetailItem.avatar
            userIcon.kf.setImage(with: URL(string: imageUrlString), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
            receiveMoneyLabel.text = String.init(format:"%.2f元",receivePeopleDetailItem.money)
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.clear
        selectionStyle = .none
        contentView.addSubview(userIcon)
        contentView.addSubview(userName)
        contentView.addSubview(receiveMoneyLabel)
        //用户
        userIcon.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(5)
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
        //用户昵称
        userName.snp.makeConstraints { (make) in
            make.left.equalTo(userIcon.snp.right).offset(5)
            make.centerY.equalTo(userIcon.snp.centerY)
        }
        //该用户领取的金额
        receiveMoneyLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-5)
            make.centerY.equalTo(userName.snp.centerY)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
