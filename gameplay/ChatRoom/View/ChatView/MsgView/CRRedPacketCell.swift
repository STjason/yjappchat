//
//  CRRedPacketCell.swift
//  gameplay
//
//  Created by admin on 2019/9/26.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
//红包图片
//let redPackgeImageUrl = "https://chatrestest.mxjianzhi01.com:59789/wap/img/Go.d8d01ce.png"

class CRRedPacketCell: CRBaseChatCell {
    
    weak var chatMessageViewDelegate:CRChatMessageViewDelegate?
    
    lazy var backView:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var msgImageV:CRMessageImageView = {
        let imageV = CRMessageImageView.init(frame: CGRect.zero)
        //        imageV.contentMode = .scaleAspectFill
        let backgroudImage = UIImage(named: "chatroom_redpacket_backgroud")
        let handleImage = backgroudImage?.stretchableImage(withLeftCapWidth: Int((backgroudImage?.size.width)! * 0.5), topCapHeight: Int((backgroudImage?.size.height)! * 0.5))
        
        imageV.image = handleImage
        imageV.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(receiveRedPacketClick))
        imageV.addGestureRecognizer(tap)
        return imageV
    }()
    
    lazy var redPacketIndicateImageV:UIImageView = {
        let imgV = UIImageView()
        imgV.image = UIImage(named: "chatroom_redpacket_indicate")
        return imgV
    }()
    
    lazy var underView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.colorWithRGB(r: 230, g: 230, b: 230, alpha: 1.0)
        return view
    }()
    
    lazy var underLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithRGB(r: 168, g: 169, b: 170, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "快来抢吧！"
        return label
    }()
    
    lazy var redPacketStatusLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithHexString("#ffe88b")
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "领取红包"
        return label
    }()
    
    
    /**红包名字*/
    lazy var remakLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.colorWithHexString("#ffe88b")
        label.font = UIFont.systemFont(ofSize: 13)
        label.backgroundColor = UIColor.white.withAlphaComponent(0)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(backView)
        backView.addSubview(msgImageV)
        backView.addSubview(underView)
        msgImageV.addSubview(remakLabel)
        underView.addSubview(underLabel)
        msgImageV.addSubview(redPacketIndicateImageV)
        msgImageV.addSubview(redPacketStatusLabel)
        msgImageV.image = UIImage(named: "chatroom_redpacket_backgroud")
        //        msgImageV.kf.setImage(with: URL.init(string: redPackgeImageUrl))
        
        backView.snp.makeConstraints { (make) in
            
            make.size.equalTo((message?.messageFrame?.contentSize ?? CGSize.zero))
        }
        
        underView.snp.makeConstraints { (make) in
            make.height.equalTo(25)
            make.left.right.bottom.equalTo(backView)
        }
        msgImageV.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(backView)
            make.bottom.equalTo(underView.snp.top)
        }
        
        redPacketIndicateImageV.snp.makeConstraints { (make) in
            make.left.equalTo(msgImageV.snp.left).offset(5)
            make.centerY.equalTo(msgImageV.snp.centerY)
            make.width.equalTo(55)
            make.height.equalTo(55)
        }
        
        redPacketStatusLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(msgImageV.snp.bottom).offset(-12)
            make.right.equalTo(remakLabel.snp.right)
            make.height.equalTo(13)
            
        }
        remakLabel.snp.makeConstraints { (make) in
            make.left.equalTo(redPacketIndicateImageV.snp.right).offset(0)
            make.right.equalTo(msgImageV.snp.right).offset(-10)
            make.top.equalTo(redPacketIndicateImageV.snp.top)
            make.bottom.lessThanOrEqualTo(redPacketStatusLabel.snp.top)
        }
        
        underLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(underView.snp.centerY)
            make.left.equalTo(underView.snp.left).offset(15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setMessage(message: CRMessage) {
        //
        let imageMessage = message as? CRRedPacketMessage
        super.setMessage(message: message)
        remakLabel.text = imageMessage?.remark
        
        if imageMessage?.ownerType == .TypeSelf{
            backView.snp.remakeConstraints { (make) in
                make.top.equalTo(bubbleImageV.snp.top)
                make.right.equalTo(bubbleImageV.snp.right)
                make.size.equalTo((imageMessage?.messageFrame?.contentSize)!)
            }
            //            activeityIndicator.snp.makeConstraints { (make) in
            //                make.centerY.equalTo(msgImageV.snp.centerY)
            //                make.right.equalTo(msgImageV.snp.left).offset(-10)
            //            }
            redPacketIndicateImageV.snp.remakeConstraints { (make) in
                make.right.equalTo(msgImageV.snp.right).offset(-5)
                make.centerY.equalTo(msgImageV.snp.centerY)
                make.width.equalTo(55)
                make.height.equalTo(55)
            }
            
            remakLabel.snp.remakeConstraints { (make) in
                make.right.equalTo(redPacketIndicateImageV.snp.left)
                make.left.greaterThanOrEqualTo(msgImageV.snp.left).offset(10)
                make.top.equalTo(redPacketIndicateImageV.snp.top)
                make.bottom.lessThanOrEqualTo(redPacketStatusLabel.snp.top)
            }
            
            redPacketStatusLabel.snp.remakeConstraints { (make) in
                make.bottom.equalTo(redPacketIndicateImageV.snp.bottom)
                make.right.equalTo(remakLabel.snp.right)
                make.height.equalTo(13)
                
            }
            
            underLabel.snp.remakeConstraints { (make) in
                make.centerY.equalTo(underView.snp.centerY)
                make.right.equalTo(underView.snp.right).offset(-15)
            }
        }else if (imageMessage?.ownerType == .Others){
            
            backView.snp.remakeConstraints { (make) in
                make.top.equalTo(bubbleImageV.snp.top)
                make.left.equalTo(bubbleImageV.snp.left)
                make.size.equalTo((imageMessage?.messageFrame?.contentSize)!)
            }
            
            redPacketIndicateImageV.snp.remakeConstraints { (make) in
                make.left.equalTo(msgImageV.snp.left).offset(5)
                make.centerY.equalTo(msgImageV.snp.centerY)
                make.width.equalTo(55)
                make.height.equalTo(55)
            }
            remakLabel.snp.remakeConstraints { (make) in
                make.left.equalTo(redPacketIndicateImageV.snp.right)
                make.right.equalTo(msgImageV.snp.right).offset(-10)
                make.top.equalTo(redPacketIndicateImageV.snp.top)
                make.bottom.lessThanOrEqualTo(redPacketStatusLabel.snp.top)
            }
            
            redPacketStatusLabel.snp.remakeConstraints { (make) in
                make.bottom.equalTo(redPacketIndicateImageV.snp.bottom).offset(0)
                make.left.equalTo(remakLabel.snp.left)
                make.height.equalTo(13)
                
            }
            
            underLabel.snp.remakeConstraints { (make) in
                make.centerY.equalTo(underView.snp.centerY)
                make.left.equalTo(underView.snp.left).offset(15)
            }
        }
        
        self.message = message
    }
}
extension CRRedPacketCell{
    //领取红包
    @objc func receiveRedPacketClick(){
        
        self.messageDelegate?.messageCellTap(message: self.message!)
    }
}
