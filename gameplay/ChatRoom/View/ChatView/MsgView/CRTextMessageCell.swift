//
//  CRTextMessageCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRTextMessageCell: CRBaseChatCell {
    //文本消息
    lazy var textMessageLabel: UILabel = {
        let messageLabel = UILabel()
        
        let font = getChatRoomSystemConfigFromJson()?.source?.switch_chat_word_size_show
        let fontColor = getChatRoomSystemConfigFromJson()?.source?.name_word_color_info ?? "FFFFFF"
        if let textFont = font{
            let textFonts = CGFloat(textFont)
             messageLabel.font = UIFont.systemFont(ofSize:textFonts)
        }else{
             messageLabel.font = UIFont.systemFont(ofSize: 15)
        }
        messageLabel.textColor = UIColor.colorWithHexString(String(format:"#%@",fontColor))
        messageLabel.numberOfLines = 0
        return messageLabel
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(textMessageLabel)
        
        textMessageLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(bubbleImageV.snp.top).offset(10)
            make.bottom.equalTo(bubbleImageV.snp.bottom).offset(-15 )
            make.size.equalTo(message?.messageFrame?.contentSize ?? CGSize.zero)
        })
    }
    
    override func setMessage(message: CRMessage) {
        let textMessage = message as? CRTextMessageItem

        textMessage?.text = message.msgStr
        super.setMessage(message: message)
        
        textMessageLabel.attributedText = textMessage?.attrText
        textMessageLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 500), for: NSLayoutConstraint.Axis.horizontal)
        bubbleImageV.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 100), for: NSLayoutConstraint.Axis.horizontal)
        if message.ownerType == .TypeSelf{
            
            bubbleImageV.image = UIImage(named: "message_sender_bg")
            bubbleImageV.highlightedImage = UIImage(named: "message_sender_bgHL")
            textMessageLabel.snp.makeConstraints { (make) in
                make.right.equalTo(bubbleImageV.snp.right).offset(-19)
                make.top.equalTo(bubbleImageV.snp.top).offset(10)
                make.bottom.equalTo(bubbleImageV.snp.bottom).offset(-10)
            }
            bubbleImageV.snp.remakeConstraints { (make) in
                make.left.equalTo(textMessageLabel.snp.left).offset(-19)
                make.bottom.equalTo(textMessageLabel.snp.bottom).offset(15)
                make.top.equalTo(headIcon.snp.top).offset(5)
                make.right.equalTo(headIcon.snp.left).offset(-5)
            }
            
            
        }else if (message.ownerType == .Others){
            bubbleImageV.image = UIImage(named: "message_receiver_bg")
            bubbleImageV.highlightedImage = UIImage(named: "message_receiver_bgHL")
            textMessageLabel.snp.makeConstraints({ (make) in
                make.left.equalTo(bubbleImageV.snp.left).offset(19)
                make.top.equalTo(bubbleImageV.snp.top).offset(10)
                make.bottom.equalTo(bubbleImageV.snp.bottom).offset(-10)
            })
            
            bubbleImageV.snp.remakeConstraints { (make) in
                make.right.equalTo(textMessageLabel.snp.right).offset(19)
                make.bottom.equalTo(textMessageLabel.snp.bottom).offset(15)
                make.top.equalTo(headIcon.snp.top).offset(5)
                make.left.equalTo(headIcon.snp.right).offset(5)
            }
        }
        let levelBackgroudColorDic = CRDefaults.getLevelColor()
        if !levelBackgroudColorDic.isEmpty {
            var key = message.levelName
            if key.contains("后台管理"){
                key = "后台管理"
            }
            let colorDict = levelBackgroudColorDic[key] as? [String:AnyObject] ?? [:]
            if !colorDict.isEmpty{
                let color = String.init(format:"#%@",colorDict["backgroudColor"] as? String ?? "")
                var backgroudColor:UIColor = UIColor.colorWithHexString(color)
                if color.length < 3{
                    backgroudColor = UIColor.clear
                }
                bubbleImageV.image? = (bubbleImageV.image?.createImageWithColor(color: backgroudColor))!
            }

        }
        
        bubbleImageV.layer.cornerRadius = 4
        bubbleImageV.layer.masksToBounds = true
      
        textMessageLabel.snp.updateConstraints({ (make) in
            
            make.size.equalTo((message.messageFrame?.contentSize ?? CGSize.zero))
        })
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
