//
//  CRGainsLossesCell.swift
//  gameplay
//
//  Created by Gallen on 2019/12/4.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit
class CRGainsLossesCell: CRBaseChatCell {
    
    lazy var gainsLossesBackView:UIView  = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.colorWithHexString("#ececec")
        return view
    }()
    /**今日总盈利icon*/
    lazy var totalTodayGainsImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.image = UIImage(named: "chatroom_today_gains")
        return imageV
    }()
    /**今日总盈利标题*/
    lazy var totalTodayGainsTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "今日总盈利:"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.colorWithHexString("#676767")
        return label
    }()
    /**今日总盈利的金额*/
    lazy var totalTodayGainsMoneyLabel:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.colorWithHexString("#fd6945")
        return label
    }()
    
    
    /**今日总投注icon*/
    lazy var totalTodayLotterBetImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.image = UIImage(named: "chatroom_today_bet")
        return imageV
    }()
    /**今日总投注标题*/
    lazy var totalTodayLotterBetTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "今日总投注:"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.colorWithHexString("#676767")
        return label
    }()
    /**今日总投注的金额*/
    lazy var totalTodayLotterBetMoneyLabel:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.colorWithHexString("#fd6945")
        return label
    }()
    /**分享*/
    lazy var shareTodayIndicateImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.image = UIImage(named: "shareToday_indicate")
        return imageV
    }()
    

    /**今日总盈亏icon*/
    lazy var totalTodayGainsLossesImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.image = UIImage(named: "chatroom_today_gainslosses")
        return imageV
    }()
    /**今日总盈亏标题*/
    lazy var totalTodayGainsLossesTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "今日总盈亏:"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.colorWithHexString("#676767")
        return label
    }()
    /**今日总盈亏的金额*/
    lazy var totalTodayGainsLossesMoneyLabel:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.colorWithHexString("#fd6945")
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(gainsLossesBackView)
        //今日总盈利
        gainsLossesBackView.addSubview(totalTodayGainsImageV)
        gainsLossesBackView.addSubview(totalTodayGainsTitleLabel)
        gainsLossesBackView.addSubview(totalTodayGainsMoneyLabel)
        //今日总投注
        gainsLossesBackView.addSubview(totalTodayLotterBetImageV)
        gainsLossesBackView.addSubview(totalTodayLotterBetTitleLabel)
        gainsLossesBackView.addSubview(totalTodayLotterBetMoneyLabel)
        //今日总盈亏
        gainsLossesBackView.addSubview(totalTodayGainsLossesImageV)
        gainsLossesBackView.addSubview(totalTodayGainsLossesTitleLabel)
        gainsLossesBackView.addSubview(totalTodayGainsLossesMoneyLabel)
        //
        
        gainsLossesBackView.addSubview(shareTodayIndicateImageV)
        //添加约束
        addLayoutConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setMessage(message: CRMessage) {
        
        let gainsLossessItem = message as? CRShareGainsLossesItem
        totalTodayGainsMoneyLabel.text = String(format:"%.2f",gainsLossessItem?.yingli ?? 0)
        totalTodayLotterBetMoneyLabel.text = String(format:"%.2f",gainsLossessItem?.touzhu ?? 0)
        totalTodayGainsLossesMoneyLabel.text = String(format:"%.2f",gainsLossessItem?.yingkui ?? 0)
        
//        totalTodayGainsMoneyLabel.text = "222.2222"
//        totalTodayLotterBetMoneyLabel.text = "222.2222"
//        totalTodayGainsLossesMoneyLabel.text = "222.2222"
        super.setMessage(message: message)
//        remakLabel.text = imageMessage?.remark
        if gainsLossessItem?.ownerType == .TypeSelf{
            gainsLossesBackView.snp.remakeConstraints { (make) in
                make.top.equalTo(bubbleImageV.snp.top)
                make.right.equalTo(bubbleImageV.snp.right)
                make.size.equalTo((gainsLossessItem?.messageFrame?.contentSize)!)
            }
//            activeityIndicator.snp.makeConstraints { (make) in
//                make.centerY.equalTo(msgImageV.snp.centerY)
//                make.right.equalTo(msgImageV.snp.left).offset(-10)
//            }
        }else if (gainsLossessItem?.ownerType == .Others){
            
            gainsLossesBackView.snp.remakeConstraints { (make) in
                make.top.equalTo(bubbleImageV.snp.top)
                make.left.equalTo(bubbleImageV.snp.left)
                make.size.equalTo((gainsLossessItem?.messageFrame?.contentSize)!)
            }
        }
        
        self.message = message
        
    }
}

//MARK:设置UI
extension CRGainsLossesCell{
    //添加约束
    func addLayoutConstraint(){
        
        gainsLossesBackView.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(200)
            make.height.equalTo(100)
            make.right.equalTo(headIcon.snp.left).offset(-5)
            make.top.equalTo(headIcon.snp.bottom).offset(5)
        }
        //指示图片
        shareTodayIndicateImageV.snp.makeConstraints { (make) in
            make.centerY.equalTo(gainsLossesBackView.snp.centerY)
            //337 × 257
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.right.equalTo(gainsLossesBackView.snp.right)
        }
        //今日总盈利
        totalTodayGainsImageV.snp.makeConstraints { (make) in
            make.left.equalTo(gainsLossesBackView).offset(5)
            make.top.equalTo(gainsLossesBackView).offset(5)
            make.size.equalTo(CGSize(width: 25, height: 25))
        }
        
        totalTodayGainsTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayGainsImageV.snp.right)
            make.centerY.equalTo(totalTodayGainsImageV.snp.centerY)
            make.width.equalTo(65)
        }
        
        totalTodayGainsMoneyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayGainsTitleLabel.snp.right)
            make.centerY.equalTo(totalTodayGainsImageV.snp.centerY)
            make.right.lessThanOrEqualTo(shareTodayIndicateImageV.snp.left)
            make.top.equalTo(gainsLossesBackView.snp.top)
        }
        
        //今日总投注
        totalTodayLotterBetImageV.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayGainsImageV.snp.left)
            make.top.equalTo(totalTodayGainsImageV.snp.bottom).offset(5)
            make.height.equalTo(25)
            make.width.equalTo(25)
        }

        totalTodayLotterBetTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayLotterBetImageV.snp.right)
            make.centerY.equalTo(totalTodayLotterBetImageV.snp.centerY)
            make.width.equalTo(65)
        }

        totalTodayLotterBetMoneyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayLotterBetTitleLabel.snp.right)
            make.centerY.equalTo(totalTodayLotterBetTitleLabel.snp.centerY)
            make.right.lessThanOrEqualTo(shareTodayIndicateImageV.snp.left)
        }
        //今日总盈亏
        totalTodayGainsLossesImageV.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayLotterBetImageV.snp.left)
            make.top.equalTo(totalTodayLotterBetImageV.snp.bottom).offset(5)
            make.size.equalTo(CGSize(width: 25, height: 25))
        }

        totalTodayGainsLossesTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayGainsLossesImageV.snp.right)
            make.centerY.equalTo(totalTodayGainsLossesImageV.snp.centerY)
            make.width.equalTo(65)
        }

        totalTodayGainsLossesMoneyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalTodayGainsLossesTitleLabel.snp.right)
            make.centerY.equalTo(totalTodayGainsLossesTitleLabel.snp.centerY)
            make.right.lessThanOrEqualTo(shareTodayIndicateImageV.snp.left)
            make.bottom.equalTo(gainsLossesBackView.snp.bottom)
        }
    }
}

