//
//  CRPlanCell.swift
//  gameplay
//
//  Created by admin on 2019/11/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRPlanCell: CRBaseChatCell {
    var planMessage:CRPlanMsgItem?
    var getPlanHeightHandler:(() -> Void)?
    
    lazy var planView:CRPlanItemView = {
        let view = Bundle.main.loadNibNamed("CRPlanItemView", owner: self, options: nil)?.last as! CRPlanItemView
        view.bubbleImageV = bubbleImageV
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.isHidden = true
        addSubview(planView)
        
//        planView.snp.makeConstraints({ (make) in
//            make.size.equalTo(message?.messageFrame?.contentSize ?? CGSize.zero)
//        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setMessage(message: CRMessage) {
        guard let planMsg = message as? CRPlanMsgItem else {
            return
        }
        
        planMsg.text = planMsg.msgStr
        planMessage = planMsg
        
        super.setMessage(message: message)
        
//        planView.snp.makeConstraints({ (make) in
//            make.left.equalTo(bubbleImageV.snp.left).offset(19)
//            make.top.equalTo(bubbleImageV.snp.top).offset(0)
//        })
        
        self.planView.configWith(model: self.planMessage!)
        
        planView.getPlanHeightHandler = {[weak self] (message) in
            guard let weakSelf = self else {return}
            weakSelf.isHidden = false
            
            weakSelf.layoutSubviews()
            
            weakSelf.getPlanHeightHandler?()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}


