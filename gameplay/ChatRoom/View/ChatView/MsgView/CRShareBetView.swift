//
//  CRShareBetView.swift
//  gameplay
//
//  Created by admin on 2019/9/23.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class CRShareBetView: UIView {
    @IBOutlet weak var lotteryImg: UIImageView! //彩种icon
    @IBOutlet weak var lotteryNameLabel: UILabel! //彩种名字
    @IBOutlet weak var periodLabel: UILabel! //投注期号
    @IBOutlet weak var ruleValeName: UILabel! //玩法名称
    @IBOutlet weak var numsLabel: UILabel!//投注内容
    @IBOutlet weak var betMoneyLabel: UILabel! //投注金额
    @IBOutlet weak var coverButton: UIButton!
    //跟单
    @IBOutlet weak var followOrderButton: UIButton!
    
    var index = 0
    var coverTapHandler:((_ index:Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        followOrderButton.layer.borderColor = UIColor.colorWithHexString("#FFB332").cgColor
        followOrderButton.layer.borderWidth = 1.5
        coverButton.addTarget(self, action: #selector(coverTap), for: .touchUpInside)
    }
    
    @objc func coverTap() {
        coverTapHandler?(index)
    }
    
    func configWith(lotteryName:String,period:String,ruleVale:String,nums:String,betMoney:String,lotCode:String) {
        self.lotteryNameLabel.text = lotteryName
        self.periodLabel.text = "期号: \(period)期"
        self.ruleValeName.text = ruleVale
        self.numsLabel.text = nums
        if let money = Float(betMoney) {
            self.betMoneyLabel.text = String.init(format: "%.2f", money)
        }
        
        upateLotImage(lotCode: lotCode)
    }
    
    func upateLotImage(lotCode:String){
        let imageURL = URL(string: BASE_URL + PORT + "/native/resources/images/" + lotCode + ".png")
        if let url = imageURL{
            self.lotteryImg.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
}
