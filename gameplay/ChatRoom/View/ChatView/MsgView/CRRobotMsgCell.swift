//
//  CRRobotMsgCell.swift
//  gameplay
//
//  Created by admin on 2019/11/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import WebKit

class CRRobotMsgCell: CRBaseChatCell {
    var robotMsg:CRRobotMsgItem?
    var webView = WKWebView()
    var getRobotWebHeightHandler:(() -> Void)?
    //     var webView = UIWebView()
    
    var webViewH:CGFloat = 0 {
        didSet {
            
            guard let msg = robotMsg else {
                return
            }
            
            if webViewH == oldValue && msg.messageFrame != nil {
                return
            }
                       
            let msgFrame = CRMessageFrame()
            msgFrame.contentSize = CGSize.init(width: MAX_MESSAGE_SYS_WIDTH, height: webViewH)
            
            let offsetHeight = Float(30)
            msgFrame.height = Float(webViewH) + offsetHeight
            msg.messageFrame = msgFrame
            updateWebViewSize()
            
            getRobotWebHeightHandler?()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.webViewH > CGFloat(0) {
            self.isHidden = false
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.layer.masksToBounds = true
        self.isHidden = true
        webView.scrollView.isScrollEnabled = false
        webView.layer.cornerRadius = 3
        webView.layer.borderWidth = 1
        webView.layer.borderColor = UIColor.lightGray.cgColor
        webView.layer.masksToBounds = true
        webView.uiDelegate = self
        self.addSubview(webView)
        
        webView.snp.makeConstraints({ (make) in
            make.size.equalTo(message?.messageFrame?.contentSize ?? CGSize.zero)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setMessage(message: CRMessage) {
        guard let robotMessage = message as? CRRobotMsgItem else {
            return
        }
        
        robotMsg = robotMessage
        robotMessage.text = message.msgStr
        super.setMessage(message: message)
        
        if let messageItem = robotMessage.robotMessage {
            let plaintext = messageItem.record
            
            webView.navigationDelegate = self
            webView.sizeToFit()
            
            var webContents = ""
            if plaintext.contains("<img src") {

                 webContents = plaintext + "<meta name=" + "\"viewport\"" + " " + "content=\"width=device-width," + " shrink-to-fit=yes\">"
            }else {
                webContents = plaintext + "<meta name=" + "\"viewport\"" + " " + "content=\"width=device-width," + " " + "initial-scale=1.0," + " " + "shrink-to-fit=no\">"
            }
          
            webView.loadHTMLString(webContents, baseURL: nil)
        }
        
        webView.snp.makeConstraints({ (make) in
            make.left.equalTo(headIcon.snp.right).offset(5)
            make.top.equalTo(bubbleImageV.snp.top).offset(0)
        })
        
        updateWebViewSize()
    }
    
    func updateWebViewSize() {
        guard let frame = robotMsg?.messageFrame else {
            webView.snp.updateConstraints({ (make) in
                make.size.equalTo(CGSize.init(width: MAX_MESSAGE_SYS_WIDTH, height: 0))
            })
            return
        }
        
        webView.snp.updateConstraints({ (make) in
            make.size.equalTo(frame.contentSize)
        })
        
        layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension CRRobotMsgCell:WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: {[weak self] (complete, error) in
            guard let weakSelf = self else {
                return
            }
            if complete != nil {
                webView.evaluateJavaScript("document.documentElement.offsetHeight", completionHandler: { (height, error) in
                    if let webViewHeight = height as? CGFloat{
                        weakSelf.webViewH = webViewHeight
                    }
                })
            }
        })
    }

}


extension CRRobotMsgCell:WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        guard let url = navigationAction.request.url else {
            return nil
        }
        
        UIApplication.shared.openURL(url)
        return nil
    }
}
