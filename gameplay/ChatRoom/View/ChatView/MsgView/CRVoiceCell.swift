//
//  CRVoiceCell.swift
//  gameplay
//
//  Created by admin on 2019/10/28.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol CRVoiceCellDelegate:NSObjectProtocol {
    func voiceTap(message:CRVoiceMessageItem,voiceImage:CRVoiceImageView)
}

class CRVoiceCell: CRBaseChatCell {
    
    weak var voiceMsgDelegate:CRVoiceCellDelegate?
    
    lazy var voiceImageView: CRVoiceImageView = {
        let view = CRVoiceImageView.init(frame: .zero)
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    lazy var durationLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14.0)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        contentView.addSubview(voiceImageView)
        contentView.addSubview(durationLabel)
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapMsgBGView))
        bubbleImageV.isUserInteractionEnabled = true
        bubbleImageV.addGestureRecognizer(gesture)
       
    }
    
    @objc private func didTapMsgBGView() {
        voiceImageView.startPlayingAnimation()
        if let message = self.message as? CRVoiceMessageItem {
            self.voiceMsgDelegate?.voiceTap(message: message,voiceImage:voiceImageView)
        }
    }
    
    override func setMessage(message: CRMessage) {
        let voiceMessage = message as? CRVoiceMessageItem
        
        super.setMessage(message: message)
        let durationTime = Int((message.content["time"] as? String) ?? "") ?? 1
        print("durationTime语音时长" + "\(durationTime)")
        
        durationLabel.text = voiceMessage?.showTime
        
        voiceImageView.snp.makeConstraints({ (make) in
//            make.size.equalTo(voiceMessage?.messageFrame?.contentSize ?? CGSize.zero)
            make.size.equalTo(CGSize.init(width: 20, height: 25))
        })
        
        bubbleImageV.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 100), for: NSLayoutConstraint.Axis.horizontal)
        if message.ownerType == .TypeSelf{
            bubbleImageV.image = UIImage(named: "message_sender_bg")
            bubbleImageV.highlightedImage = UIImage(named: "message_sender_bgHL")
            voiceImageView.setIsFromMe(fromMe: true)
            voiceImageView.snp.remakeConstraints { (make) in
                make.right.equalTo(bubbleImageV.snp.right).offset(-12)
                make.top.equalTo(bubbleImageV.snp.top).offset(14)
            }
            bubbleImageV.snp.remakeConstraints { (make) in
                make.right.equalTo(headIcon.snp.left).offset(-5)
                make.bottom.equalTo(voiceImageView.snp.bottom).offset(20)
                make.top.equalTo(headIcon.snp.top).offset(5)
                make.width.equalTo(voiceMessage?.messageFrame?.contentSize.width ?? 0)
            }
            
            durationLabel.snp.remakeConstraints {[weak self] (make) in
                guard let weakSelf = self else {return}
                make.centerY.equalTo(weakSelf.voiceImageView.snp.centerY)
                make.right.equalTo(weakSelf.bubbleImageV.snp.left).offset(-3)
            }
//
//
            activeityIndicator.snp.remakeConstraints() { (make) in
                make.right.equalTo(bubbleImageV.snp.left).offset(-20)
                make.centerY.equalTo(bubbleImageV.snp.centerY).offset(-2)
                make.width.height.equalTo(0)
            }
//
//            //消息没有发送成功 给个状态icon
            sendMessageStatusImageV.snp.remakeConstraints { (make) in
                make.right.equalTo(bubbleImageV.snp.left).offset(-20)
                make.centerY.equalTo(bubbleImageV.snp.centerY).offset(-2)
                make.width.height.equalTo(15)
            }
            
        }else if (message.ownerType == .Others){
            
            bubbleImageV.image = UIImage(named: "message_receiver_bg")
            bubbleImageV.highlightedImage = UIImage(named: "message_receiver_bgHL")
            voiceImageView.setIsFromMe(fromMe: false)
            voiceImageView.snp.remakeConstraints({ (make) in
                make.left.equalTo(bubbleImageV.snp.left).offset(12)
                make.top.equalTo(bubbleImageV.snp.top).offset(14)
            })
            
            bubbleImageV.snp.remakeConstraints { (make) in
                make.bottom.equalTo(voiceImageView.snp.bottom).offset(20)
                make.top.equalTo(headIcon.snp.top).offset(5)
                make.left.equalTo(headIcon.snp.right).offset(5)
                make.width.equalTo(voiceMessage?.messageFrame?.contentSize.width ?? 0)
            }
            
            durationLabel.snp.remakeConstraints {[weak self] (make) in
                guard let weakSelf = self else {return}
                make.centerY.equalTo(weakSelf.voiceImageView.snp.centerY)
                make.left.equalTo(weakSelf.bubbleImageV.snp.right).offset(3)
            }
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
