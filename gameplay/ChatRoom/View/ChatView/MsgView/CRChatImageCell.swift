//
//  CRChatImageCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class CRChatImageCell: CRBaseChatCell {
    var canCollectTag = true //放置重复点击收藏
    /** 图片 路径code */
    var fileCode: String = ""
    //
    var progress:Double = 0
    //进度条视图
    lazy var progressView : CRProgressView = {
        let progress = CRProgressView.init(frame: CGRect.zero)
        progress.backgroundColor = UIColor.clear
        return progress
    }()
    
    lazy var progressLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(self.msgImageV)
        msgImageV.addSubview(progressView)
        progressView.addSubview(progressLabel)
        msgImageV.snp.makeConstraints { (make) in
            make.size.equalTo((message?.messageFrame?.contentSize ?? CGSize.zero))
        }
        progressView.snp.makeConstraints { (make) in
            make.centerX.equalTo(msgImageV.snp.centerX)
            make.centerY.equalTo(msgImageV.snp.centerY)
            make.width.height.equalTo(50)
        }
        progressLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(progressView.snp.centerX)
            make.centerY.equalTo(progressView.snp.centerY)
        }
    }
    
    override func setMessage(message: CRMessage) {
        msgImageV.alpha = 1.0
        if let imageMessage = message as? CRImageMessage{
            super.setMessage(message: message)
            if imageMessage.imagePath == "" {
                msgImageV.kf.setImage(with: URL(string: imageMessage.imageUrl!), placeholder: UIImage(named: "image_placeholder"), options: nil, progressBlock: { (curProgress, totalProgress) in
                    
                }) { (image, error, cache, url) in
                    imageMessage.imageSize = (image?.size) ?? CGSize.zero
                    imageMessage.kMessageFrame = nil
                    if image != nil{
                        imageMessage.sendState = .Success
                    }
                    
                }
                
            }else{
                //获取图片路径
                let imgPath = FileManager.pathUserChatImage(imageName: (imageMessage.imagePath))
                if !imgPath.isEmpty{
//                    msgImageV.image = UIImage.init(contentsOfFile: imgPath)
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        self.msgImageV.kf.setImage(with: nil, placeholder: UIImage.init(contentsOfFile: imgPath), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
            }
            if imageMessage.ownerType == .TypeSelf{
                msgImageV.setBackgroundImage(image: UIImage(named: "message_sender_bg")!)
                msgImageV.snp.remakeConstraints { (make) in
                    make.top.equalTo(bubbleImageV.snp.top)
                    make.right.equalTo(bubbleImageV.snp.right)
                }
            }else if (imageMessage.ownerType == .Others){
                msgImageV.setBackgroundImage(image: UIImage(named: "message_receiver_bg")!)
                msgImageV.snp.remakeConstraints { (make) in
                    make.top.equalTo(bubbleImageV.snp.top)
                    make.left.equalTo(bubbleImageV.snp.left)
                }
            }
            msgImageV.snp.makeConstraints { (make) in
                make.size.equalTo((imageMessage.messageFrame?.contentSize)!)
            }
            
            activeityIndicator.snp.makeConstraints { (make) in
                make.centerY.equalTo(msgImageV.snp.centerY)
                make.right.equalTo(msgImageV.snp.left).offset(-10)
            }
            
            let url = imageMessage.content["url"] as? String ?? ""
//            fileCode = getQueryStringParameter(url: url, param: "fileId") ?? ""
            //Adam 添加，使用原生app图片添加方式，图片展示混乱，所以改为wap端图片拼接方式展示
            fileCode = url.components(separatedBy: "/").last ?? ""
            
            self.message = message
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    lazy var msgImageV:CRMessageImageView = {
        let imageV = CRMessageImageView.init(frame: CGRect.zero)
        imageV.layer.cornerRadius = 4
        imageV.layer.masksToBounds = true
        imageV.isUserInteractionEnabled = true
        //图片点击
        let clickTap = UITapGestureRecognizer(target: self, action: #selector(imageClick))
        imageV.addGestureRecognizer(clickTap)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPress(sender:)))
        imageV.addGestureRecognizer(longPress)
        
        return imageV
    }()
    
}
extension CRChatImageCell{
    //图片点击手势
    @objc func imageClick(){
        self.messageDelegate?.messageCellTap(message: message!)
    }
    // 图片长按
    @objc func imageLongPress(sender: UILongPressGestureRecognizer)
    {
        guard sender.state == .began else {
            return
        }
        self.becomeFirstResponder()
        
        let menuC = UIMenuController.shared
        
//        let quoteItem       = UIMenuItem(title: "引用", action: #selector(quoteClick))
//        let copyItem        = UIMenuItem(title: "复制", action: #selector(copyIClick))
//        let forwardItem     = UIMenuItem(title: "转发", action: #selector(forwardClick))
        let connectItem     = UIMenuItem(title: "收藏", action: #selector(collectClick))
//        let deleteItem      = UIMenuItem(title: "删除", action: #selector(deleteClick))
//        let shareItem       = UIMenuItem(title: "分享", action: #selector(shareClick))
        
//        menuC.menuItems = [quoteItem, copyItem, forwardItem, connectItem, deleteItem, shareItem]
        menuC.menuItems = [connectItem]
        
        menuC.setTargetRect(self.msgImageV.frame, in: self)
        menuC.setMenuVisible(true, animated: true)
        
        print("+++++++++++图片收藏")
    }
    @objc func quoteClick() {
        showErrorHUD(errStr: "暂不支持")
    }
    
    @objc func copyIClick() {
        showErrorHUD(errStr: "暂不支持")
    }
    
    @objc func forwardClick() {
        showErrorHUD(errStr: "暂不支持")
    }
    
    @objc func collectClick() {
        if canCollectTag {
            canCollectTag = false
            
            NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "imageCollect"), object: self, userInfo: ["fileCode" : fileCode])
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.canCollectTag = true
            }
        }
    }
    
    @objc func deleteClick() {
        showErrorHUD(errStr: "暂不支持")
    }
    @objc func shareClick() {
        showErrorHUD(errStr: "暂不支持")
    }
    override var canBecomeFirstResponder: Bool
    {
        return true
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if [#selector(quoteClick), #selector(copyIClick), #selector(forwardClick), #selector(collectClick), #selector(deleteClick), #selector(shareClick)].contains(action) {
            return true
        }
        return false
    }
}
extension CRChatImageCell{
    //刷新
    func refreshData(){
//        self.messageDelegate?.reloadImageCell()
    }
}
