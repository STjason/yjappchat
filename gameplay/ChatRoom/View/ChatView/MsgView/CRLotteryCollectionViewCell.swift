//
//  CRLotteryCollectionViewCell.swift
//  gameplay
//
//  Created by Gallen on 2019/10/1.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class CRLotteryCollectionViewCell: UICollectionViewCell {
    /**彩种图像*/
    lazy var lotterIconImageV:UIImageView = {
        let icon = UIImageView()
        icon.layer.cornerRadius = 40 * 0.5
        icon.layer.masksToBounds = true
        return icon
    }()
    /**彩种名*/
    lazy var lottorNameLabel:UILabel = {
        let label = UILabel()
        label.text = "六合彩"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    /**期号*/
   lazy var serialNumberLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    /**开奖结果*/
    lazy var lotteryNumberView:BallsView = {
        let view = BallsView()
//        view.backgroundColor = UIColor.yellow
        
        return view
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.groupTableViewBackground
        layer.cornerRadius = 3
        layer.masksToBounds = true
        contentView.addSubview(lotterIconImageV)
        contentView.addSubview(lottorNameLabel)
        contentView.addSubview(serialNumberLabel)
        contentView.addSubview(lotteryNumberView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        lotterIconImageV.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.left.equalTo(8)
        }
        lottorNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(5)
            make.left.equalTo(lotterIconImageV.snp.right).offset(5)
        }
        serialNumberLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(lottorNameLabel.snp.bottom)
            make.right.equalTo(contentView).offset(-10)
        }
        lotteryNumberView.snp.makeConstraints { (make) in
            make.left.equalTo(lotterIconImageV.snp.right).offset(5)
            make.top.equalTo(lottorNameLabel.snp.bottom)
            make.right.equalTo(contentView).offset(-5)
            make.bottom.equalTo(contentView).offset(-8)
        }
    }
    
    var lottotItem:CRLotteryData?{
        didSet{
            if let lotterItem = lottotItem {
                lottorNameLabel.text = lotterItem.name
                if lotterItem.qiHao.length != 0{
                    serialNumberLabel.text = "第" + "\(lotterItem.qiHao)" + "期"
                    let nums = lotterItem.haoMa.components(separatedBy: ",")
                    lotteryNumberView.basicSetupBalls(nums: nums, offset: 5, lotTypeCode: lotterItem.lotType, cpVersion: lotterItem.lotVersion, ballWidth: 30, small: false, gravity_bottom: false, ballsViewWidth: 0, forBetPageBelow: true, isBetTopView: true, lotteryResults: false, time: lotterItem.date, year: 0,fromChatRoom:true)
                }else{
                    serialNumberLabel.text = "暂无期号"
                    let nums = ["?","?","?","?","?"]
                    lotteryNumberView.basicSetupBalls(nums: nums, offset: 5, lotTypeCode: lotterItem.lotType, cpVersion: lotterItem.lotVersion, ballWidth: 30, small: false, gravity_bottom: false, ballsViewWidth: 0, forBetPageBelow: true, isBetTopView: true, lotteryResults: false, time: lotterItem.date, year: 0,fromChatRoom:true)
                }
                
                upateLotImage(groupLotCode: lotterItem.code, lotteryIcon: lotterItem.icon)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
//MARK:--获取彩种的图片信息
extension CRLotteryCollectionViewCell{
    func upateLotImage(groupLotCode:String,lotteryIcon: String){
        if isEmptyString(str: lotteryIcon) {
            let imageURL = URL(string: BASE_URL + PORT + "/native/resources/images/" + groupLotCode + ".png")
            if let url = imageURL{
                self.lotterIconImageV.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }else {
            if let url = URL(string: lotteryIcon) {
                self.lotterIconImageV.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
    }
}
