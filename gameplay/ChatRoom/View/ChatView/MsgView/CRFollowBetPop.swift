//
//  CRFollowBetPop.swift
//  gameplay
//
//  Created by admin on 2019/9/26.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

class CRFollowBetPop: UIView {
    var inIndex = 0 //跟单列表索引
    /// show：true键盘即将弹出，false键盘即将隐藏
    var followBetKeyboardStatusHandler:((_ show:Bool) -> Void)?
    var followBetHandler:((_ message:CRShareBetMessageItem,_ betModel:Int,_ inIndex:Int) -> Void)?
    var message:CRShareBetMessageItem!
    var currency_unitArray: [String]? //元角分开关
    
    var currentModel = 1 { //最新选择的元角分模式
        didSet {
            if currentModel == 1 {
                yuanCheckBtn.isSelected = true
            }else if currentModel == 10 {
                jiaoCheckBtn.isSelected = true
            }else if currentModel == 100 {
                fenCheckBtn.isSelected = true
            }
        }
    }
    
    @IBOutlet weak var lotImageView: UIImageView!
    @IBOutlet weak var lotteryNameLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var betStatusLabel: UILabel!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var ruleNameLabel: UILabel!
    @IBOutlet weak var numsLabel: UILabel!
    @IBOutlet weak var betCountLabel: UILabel!
    @IBOutlet weak var betMoneyLabel: UILabel!
    @IBOutlet weak var inputField: CustomFeildText!
    @IBOutlet weak var followBetButton: UIButton!
    
    @IBOutlet weak var editMoneyLabel: UILabel!
    @IBOutlet weak var followBetModeH: NSLayoutConstraint!
    
    @IBOutlet weak var yuanCheckBtn: UIButton!
    @IBOutlet weak var yuanTitleBtn: UIButton!
    @IBOutlet weak var jiaoCheckBtn: UIButton!
    @IBOutlet weak var jiaoTitleBtn: UIButton!
    @IBOutlet weak var fenCheckBtn: UIButton!
    @IBOutlet weak var fenTitleBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        followBetButton.addTarget(self, action: #selector(followBetAction), for: .touchUpInside)
        yuanCheckBtn.addTarget(self, action: #selector(modeSelectYuan), for: .touchUpInside)
        jiaoCheckBtn.addTarget(self, action: #selector(modeSelectJiao), for: .touchUpInside)
        fenCheckBtn.addTarget(self, action: #selector(modeSelectFen), for: .touchUpInside)
        inputField.delegate = self
        inputField.addTarget(self, action: #selector(inputValueChange(_:)), for: .editingChanged)
    }
    
    ///输入内容改变
    @objc func inputValueChange(_ textField:UITextField) {
        guard let betInfo = getBetInfo() else {
            showToast(view: self, txt: "注单信息有误")
            return
        }
        
        guard let text = textField.text,let _ = Double(text) else {
            if !(textField.text ?? "").isEmpty {
                showToast(view: self, txt: "请输入正确的金额")
            }
            
            let money = formatPureIntIfIsInt(value: "0")
            betMoneyLabel.text = money
            
            return
        }
        
        if betInfo.version == "1" {
            betMoneyLabel.text = updateOfficalTotalMoney(currentModel: currentModel)
        }else {
            let money = formatPureIntIfIsInt(value: text)
            betMoneyLabel.text = money
        }
        
    }
    
    @objc func modeSelectYuan() {
        modelSelect(selectModel: 1, selectButton: yuanCheckBtn)
    }
    
    @objc func modeSelectJiao() {
        modelSelect(selectModel: 10, selectButton: jiaoCheckBtn)
    }
    
    @objc func modeSelectFen() {
        modelSelect(selectModel: 100, selectButton: fenCheckBtn)
    }
    
    private func modelSelect(selectModel:Int,selectButton:UIButton) {
        currentModel = selectModel
        resetCheckMode()
        selectButton.isSelected = true
        betMoneyLabel.text = updateOfficalTotalMoney(currentModel: currentModel)
    }
    
    //重制元角分模式
    private func resetCheckMode() {
        yuanCheckBtn.isSelected = false
        jiaoCheckBtn.isSelected = false
        fenCheckBtn.isSelected = false
    }
    
    func configWith(message:CRShareBetMessageItem,inIndex:Int = 0) {
        
        var currency_unit = getChatRoomSystemConfigFromJson()?.source?.switch_currency_unit_show
        currency_unit = currency_unit?.replacingOccurrences(of: "yuan:", with: "")
        currency_unit = currency_unit?.replacingOccurrences(of: "jiao:", with: "")
        currency_unit = currency_unit?.replacingOccurrences(of: "fen:", with: "")

        if let array = currency_unit?.components(separatedBy: ",") {
            
            for (index,modelSwitch) in array.enumerated() {
                if index == 0 {
                    yuanCheckBtn.isHidden = !(modelSwitch == "1")
                    yuanTitleBtn.isHidden = !(modelSwitch == "1")
                }else if index == 1 {
                    jiaoCheckBtn.isHidden = !(modelSwitch == "1")
                    jiaoTitleBtn.isHidden = !(modelSwitch == "1")
                }else if index == 2 {
                    fenCheckBtn.isHidden = !(modelSwitch == "1")
                    fenTitleBtn.isHidden = !(modelSwitch == "1")
                }
            }
            
            currency_unitArray = array
        }
        
        self.inIndex = inIndex
        
        resetCheckMode()
        
        self.message = message
        
        guard let shareBetModel = message.shareBetModel,let betInfos = shareBetModel.betInfos else {
            showToast(view: self, txt: "跟单信息有误")
            return
        }
        
        let betInfo = betInfos[inIndex]
        configWithSingle(betInfo: betInfo)
    }
    
    ///model：原始元角分model,因为原始元角分模式可能被设置为不显示，所以需要判断最终默认显示的元角分模式，返回元组 (原始model是否被打开,最终返回的model)
    func getYJFModelWith(model:Int) -> (Bool,Int) {
        if let unitArray = currency_unitArray {
            
            var switchYJFIndex = 0 //原始元角分模式，在开关数组中对应的index
            if model == 1 {
                switchYJFIndex = 0
            }else if model == 10 {
                switchYJFIndex = 1
            }else if model == 100 {
                switchYJFIndex = 2
            }
            
            let switchYJFValue = unitArray[switchYJFIndex] //原始元角分模式，在开关数组中对应的 value ,1开，2关
            if switchYJFValue == "1" { //原始模式开关 开状态
                currentModel = model
                return (true,model)
            }else { ////原始模式开关 关状态
                if unitArray[0] == "1" {
                    currentModel = 1
                    return (false,1)
                }else if unitArray[1] == "1" {
                    currentModel = 10
                    return (false,10)
                }else if unitArray[2] == "1" {
                    currentModel = 100
                    return (false,100)
                }else { //元角分都被关闭,返回 原始模式
                    currentModel = model
                    return (false,model)
                }
            }
            
        }else { //开关不存在则 返回 原始模式
            currentModel = model
            return (true,model)
        }
    }
    
    func configWithSingle(betInfo:CRShareBetInfo) {
        let leftView = UILabel()
        leftView.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        leftView.backgroundColor = UIColor.clear
        leftView.font = UIFont.systemFont(ofSize: 16)
        leftView.textColor = UIColor.orange
        inputField.leftView = leftView
        inputField.leftViewMode = .always
        
        let betModel = betInfo.model + 0
        if betInfo.version == "1" {
            followBetModeH.constant = 40
            editMoneyLabel.isHidden = true
            leftView.text = "倍数"
            
            let _ = getYJFModelWith(model: betModel)
            
//            currentModel = betModel + 0
            
            self.inputField.text = "1"
            betMoneyLabel.text = updateOfficalTotalMoney(currentModel: currentModel)
            
        }else if betInfo.version == "2" {
            followBetModeH.constant = 0
            editMoneyLabel.isHidden = false
            leftView.text = "¥"
            
            if let money = Float(betInfo.lottery_amount) {
                let money = formatPureIntIfIsInt(value: "\(money)")
                betMoneyLabel.text = money
                self.inputField.text = money
            }
        }
        
        upateLotImage(info: betInfo)
        
        let officialPeilv = betInfo.version == "1" ? "[官]" : "[信]"
        
        self.lotteryNameLabel.text = "\(betInfo.lottery_type)\(officialPeilv)"
        self.periodLabel.text = "期号:\(betInfo.lottery_qihao)期"
        self.ruleNameLabel.text = betInfo.lottery_play
        self.numsLabel.text = betInfo.lottery_content
        self.betCountLabel.text = betInfo.lottery_zhushu
    }
    
    func getBetInfo() -> CRShareBetInfo? {
        guard let shareBetModel = message.shareBetModel,let betInfos = shareBetModel.betInfos else {
            return nil
        }
        
        let betInfo = betInfos[inIndex]
        return betInfo
    }
    
    ///信用总金额刷新
    func updatepeilvTotalMoney() {
        betMoneyLabel.text  = self.inputField.text
    }
    
    ///根据倍数 和模式计算总金额 【官方】,currentModel当前选择的元角分模式
    func updateOfficalTotalMoney(currentModel:Int) -> String? {

        guard let betInfo = getBetInfo() else {
            showErrorHUD(errStr: "请重试")
            return nil
        }
        //倍数
        var changeBeishu = 1
        if let beishu = Int(self.inputField.text ?? "") {
            changeBeishu = beishu
            
        }else {
            showToast(view: self, txt: "请输入倍数")
        }
        
        //元角分模式
        let mode = Double(betInfo.model) //原始元角分模式
        var modebeishu:Double = 1.0 //当前元角分模式，和之前元角分模式 相除 获得

        if mode < Double(currentModel) {
            modebeishu = mode / Double(currentModel)
        }else if mode > Double(currentModel) {
            modebeishu = mode / Double(currentModel)
        }
        
//        guard let money = Double(betInfo.lottery_amount) else {
//            showErrorHUD(errStr: "注单信息金额不正确")
//            return nil
//        }
//
//        let finanMoney = money * modebeishu * Double(changeBeishu)
        
        guard let zhushu = Double(betInfo.lottery_zhushu) else {
            showErrorHUD(errStr: "注单信息金额不正确")
            return nil
        }
        
        let finanMoney = zhushu * modebeishu * Double(changeBeishu) * Double(2) * Double( 1.0 / Double(mode))

        //原始总金额
        let moneyString = formatPureIntIfIsInt(value: "\(finanMoney)")
        return moneyString
    }
    
    func updatePeriod(value:String) {
        if let shareBetModel = message.shareBetModel,let betInfo = shareBetModel.betInfo {
            betInfo.lottery_qihao = value
        }
        
        let period = "期号: \(trimQihao(currentQihao: value))期"
        periodLabel.text = period
    }
    
    @objc func followBetAction() {
        
        guard let text = inputField.text,let _ = Double(text) else {
            if !inputField.isFirstResponder {
                showToast(view: self, txt: "请输入正确的金额")
            }
            
            return
        }
        
        followBetHandler?(message,currentModel,inIndex)
    }
    
    //更新彩种图片
    func upateLotImage(info:CRShareBetInfo){
        var imageString = info.lotIcon.isEmpty ? info.icon : info.lotIcon
        if imageString.isEmpty {
            imageString = BASE_URL + PORT + "/native/resources/images/" + info.lotCode + ".png"
        }
        
        let imageURL = URL(string: imageString)
        if let url = imageURL{
            lotImageView.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}


extension CRFollowBetPop:UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        followBetKeyboardStatusHandler?(true)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        followBetKeyboardStatusHandler?(false)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location >= 7 {
            showToast(view: self, txt: "当前允许输入最大长度为7位")
            return false
        }
        return true
    }
}














