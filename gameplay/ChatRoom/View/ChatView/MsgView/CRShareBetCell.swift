//
//  CRShareBetCell.swift
//  gameplay
//
//  Created by admin on 2019/9/23.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRShareBetCell: CRBaseChatCell {
    var index = 0
    var coverTapHandler:((_ index:Int) -> Void)? //原有单注跟单
    var mergeCoverTapHandler:((_ index:Int,_ inIndex:Int) -> Void)? //合并跟单类型的跟单点击
    //    lazy var shareBetView:CRShareBetView = {
    //       let view = Bundle.main.loadNibNamed("CRShareBetView", owner: self, options: nil)?.last as! CRShareBetView
    //        view.layer.borderColor = UIColor.lightGray.cgColor
    //        view.layer.borderWidth = 0.2
    //        view.layer.cornerRadius = 4
    //        view.layer.masksToBounds = true
    //
    //        return view
    //    }()
    
    lazy var mergeShareBetView:CRShareBetAllView = {
        let view = CRShareBetAllView.init(frame: .zero)
        view.layer.borderWidth = 0.2
        view.layer.cornerRadius = 4
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //        self.addSubview(self.shareBetView)
        
        addSubview(mergeShareBetView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configIndex(index:Int) {
        self.index = index
        mergeShareBetView.index = index
        //        shareBetView.index = index
    }
    
    override func setMessage(message: CRMessage) {
        let shareOrderMessage = message as? CRShareBetMessageItem
        
        super.setMessage(message: message)
        //是否显示胜率
        if isShowWinRate(rate: shareOrderMessage?.winRate ?? 0){
            winRateLabel.isHidden = false
            sendDateLabel.isHidden = true
            let winRate = String(format:"投注命中率%.2f%%",shareOrderMessage?.winRate ?? 0)
            let nsString = NSString(string: winRate)
            let range = nsString.range(of: "投注命中率")
            let attributeString = NSMutableAttributedString(string: winRate)
            attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
            attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 12), range: range)
            winRateLabel.attributedText = attributeString
            
            winRateLabel.sizeToFit()
            //            winRateLabel.size.width =  winRateLabel.size.width  + 10
        }else{
            winRateLabel.isHidden = true
            sendDateLabel.isHidden = false
        }
        
        if let message = message as? CRShareBetMessageItem,let shareBetModel = message.shareBetModel {
            self.configWith(model: shareBetModel)
        }
        
        //        if shareOrderMessage?.ownerType == .TypeSelf{
        //            shareBetView.snp.remakeConstraints {[weak self] (make) in
        //                if let weakSelf = self {
        //                    make.left.equalTo(weakSelf.snp.left).offset(40)
        //                }
        //                make.top.equalTo(bubbleImageV.snp.top)
        //                make.right.equalTo(bubbleImageV.snp.right)
        //                make.height.equalTo((shareOrderMessage?.messageFrame?.contentSize.height)!)
        //            }
        //
        //        }else {
        //            shareBetView.snp.remakeConstraints {[weak self] (make) in
        //                make.top.equalTo(bubbleImageV.snp.top)
        //                make.left.equalTo(bubbleImageV.snp.left)
        //                if let weakSelf = self {
        //                    make.right.equalTo(weakSelf.snp.right).offset(-40)
        //                }
        //                make.height.equalTo((shareOrderMessage?.messageFrame?.contentSize.height)!)
        //            }
        //        }
        
        if shareOrderMessage?.ownerType == .TypeSelf {
            mergeShareBetView.snp.remakeConstraints { (make) in
                make.top.equalTo(bubbleImageV.snp.top)
                make.left.equalTo(self.snp.left).offset(40)
                if let msg = shareOrderMessage,let frame = msg.messageFrame {
                    make.height.equalTo(frame.contentSize.height)
                }else {
                    make.height.equalTo(0)
                }
                
                make.right.equalTo(bubbleImageV.snp.right)
            }
        }else {
            mergeShareBetView.snp.remakeConstraints { (make) in
                make.top.equalTo(bubbleImageV.snp.top)
                make.left.equalTo(bubbleImageV.snp.left)
                if let msg = shareOrderMessage,let frame = msg.messageFrame {
                    make.height.equalTo(frame.contentSize.height)
                }else {
                    make.height.equalTo(0)
                }
                make.right.equalTo(self.snp.right).offset(-40)
            }
            
        }
        
        self.message = message
    }
    
    private func configWith(model:CRShareBetModel) {
        //        if let betInfo = model.betInfo {
        //            //            let officalPeilv = betInfo.version == "1" ? "[官]" : "[信]"
        //            let officalPeilv = ""
        //            let lotteryName = "\(betInfo.lottery_type)\(officalPeilv)"
        //
        //            self.shareBetView.configWith(lotteryName: lotteryName, period: betInfo.lottery_qihao, ruleVale: betInfo.lottery_play, nums: betInfo.lottery_content, betMoney: betInfo.lottery_amount,lotCode:betInfo.lotCode)
        //
        //            shareBetView.coverTapHandler = {[weak self] (index) in
        //                guard let weakSelf = self else {return}
        //                weakSelf.coverTapHandler?(index)
        //            }
        //        }
        
        if let betInfos = model.betInfos {
            mergeShareBetView.configWith(infos: betInfos)
            mergeShareBetView.coverTapHandler = {[weak self] (index,inIndex) in
                guard let weakSelf = self else {return}
                weakSelf.coverTapHandler?(index)
                weakSelf.mergeCoverTapHandler?(index,inIndex)
            }
        }
    }
}

extension CRShareBetCell{
    //是否显示胜率
    func isShowWinRate(rate:Float) -> Bool{
        //胜率
        let config = getChatRoomSystemConfigFromJson()
        //后台设置的显示胜率
        let showWinRate = config?.source?.name_user_win_tips_per ?? 0
        //用户投注胜率大于后台设置的胜率才展示
        if rate > showWinRate{
            return true
        }
        return false
    }
    
}
