//
//  CRPlanItemView.swift
//  gameplay
//
//  Created by admin on 2019/11/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import WebKit

class CRPlanItemView: UIView {
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lotteryName: UILabel!
    @IBOutlet weak var webBgView: UIView!
    @IBOutlet weak var webHeight: NSLayoutConstraint!
    var bubbleImageV:UIImageView? //计划消息的气泡，传到这里用力做 remake布局使用
    var webView = WKWebView()
    var getPlanHeightHandler:((_ message:CRPlanMsgItem) -> Void)?
    
    var planMessage:CRPlanMsgItem?
    
    var webViewH:CGFloat = 0 {
        didSet {
            
            guard let msg = planMessage else {
                return
            }
            
            if webViewH == oldValue && msg.messageFrame != nil {
                return
            }
            
            let msgFrame = CRMessageFrame()
            let headerHeight:CGFloat = 67
            let planHeight = webViewH + headerHeight
            
            msgFrame.contentSize = CGSize.init(width: MAX_MESSAGE_SYS_WIDTH, height: planHeight)
            msgFrame.webHeight = webViewH
            webHeight.constant = webViewH
            let offsetHeight = Float(20)
            msgFrame.height = Float(planHeight) + offsetHeight
            msg.messageFrame = msgFrame
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                self.updateWebViewSize()
            }
            
            getPlanHeightHandler?(msg)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.masksToBounds = true
        bottomView.layer.borderWidth = 2
        bottomView.layer.borderColor = UIColor.init(hexString: "#8001F3")?.cgColor
        bottomView.layer.cornerRadius = 3
        bottomView.layer.masksToBounds = true
        
        webView.scrollView.isScrollEnabled = false
        webView.scrollView.isScrollEnabled = false
        webView.backgroundColor = UIColor.red
        webView.uiDelegate = self
        webBgView.addSubview(webView)
        
        webView.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.edges.equalTo(weakSelf.webBgView.snp.edges)
        }
    }
    
    func configWith(model:CRPlanMsgItem) {
        planMessage = model
        lotteryName.text = model.planItem?.lotteryName
        
        var originalPlaintext = model.planItem?.text ?? ""
        var plaintext = ""
        
        if originalPlaintext.contains("<p>") && originalPlaintext.contains("</p>") {
            plaintext = originalPlaintext
        }else {
            originalPlaintext = "<p style=\"text-align:center\"> <font" + " color=" + "\"#8001F3\">" + originalPlaintext + "</font>" + "</p>"
            plaintext = originalPlaintext.replacingOccurrences(of: "\n", with: "<br>")
        }
        
        webView.navigationDelegate = self
        webView.sizeToFit()
        
        var webContents = ""
        if plaintext.contains("<img src") {
            webContents = plaintext + "<meta name=" + "\"viewport\"" + " " + "content=\"width=device-width," + " shrink-to-fit=yes\">"
        }else {
            webContents = plaintext + "<meta name=" + "\"viewport\"" + " " + "content=\"width=device-width," + " " + "initial-scale=1.0," + " " + "shrink-to-fit=no\">"
        }
        
        webView.loadHTMLString(webContents, baseURL: nil)
        
        updateWebViewSize()
    }
    
    func updateWebViewSize() {
        guard let frame = planMessage?.messageFrame else {
            self.snp.remakeConstraints({ (make) in
                make.size.equalTo(CGSize.init(width: MAX_MESSAGE_SYS_WIDTH, height: 0))
            })
            return
        }
        
        webHeight.constant = webViewH
        
        webBgView.snp.remakeConstraints { (make) in
            make.top.equalTo(27)
            make.left.right.bottom.equalTo(0)
        }
        
        webView.snp.remakeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.edges.equalTo(weakSelf.webBgView.snp.edges)
        }
        
        if self.superview != nil {
            self.snp.remakeConstraints({(make) in
                make.size.equalTo(CGSize.init(width: MAX_MESSAGE_SYS_WIDTH, height: CGFloat(frame.contentSize.height)))
                make.left.equalTo(55)
                make.top.equalTo(5)
            })
        }else {
            print("CRPlanItemView.superview为空")
        }
        
        
        layoutSubviews()
    }
}


extension CRPlanItemView:WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        if let msgFrame = planMessage?.messageFrame {
            if msgFrame.webHeight > 0 {
                return
            }
        }
        
        webView.evaluateJavaScript("document.readyState", completionHandler: {[weak self] (complete, error) in
            guard let weakSelf = self else {return}
            
            if complete != nil {
                webView.evaluateJavaScript("document.documentElement.offsetHeight", completionHandler: { (height, error) in
                    
                    if let webViewHeight = height as? CGFloat{
                        print("webView高度: \(webViewHeight)")
                        weakSelf.webViewH = webViewHeight
                    }
                })
            }
        })
    }
}

extension CRPlanItemView:WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        guard let url = navigationAction.request.url else {
            return nil
        }
        
        UIApplication.shared.openURL(url)
        return nil
    }
}
