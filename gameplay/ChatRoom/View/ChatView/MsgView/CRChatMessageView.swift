//
//  CRChatMessageView.swift
//  Chatroom
//
//  Created by admin on 2019/7/13.
//  Copyright © 2019年 yun. All rights reserved.
//  聊天视图父视图

import UIKit
import SnapKit
import MJRefresh

class CRChatMessageView: UIView {
    let informSoomeOne = "@TA"
    let clearScreen = "清屏"
    let prohibitTalk = "禁止此人发言"
    let cancelProhibitTalk = "解除此人禁言"
    let prohibitTalkAll = "全体禁言"
    let cancelProhibitTalkAll = "解除全体禁言"
    let retractMsg = "撤回"
    let personInfo = "用户资料"
    let privateChat = "私聊"
    
    var userType = 1 //用户类型
    
    var toolPermission = [String]() {//用户房间权限
        didSet {
            CRChatRoomLogic.updatePrivatePermisson(toolPermission:toolPermission)
        }
    }
    var isPrivateChat = false //是否是私聊
    
    var loadMoreMsgsHandler:(() -> Void)? //拉取更多历史消息
    let refreshHeader = MJRefreshNormalHeader()
    var remoterY:CGFloat = 300 //福利 悬浮按钮的 Y
    var remoter = UIButton() //福利 悬浮按钮
    var cellCountPrint = 0 //打印cell的执行次数
    var robotIndexpathes = [IndexPath]() //机器人消息所有数据对应 IndexPath
    var planIndexpathes = [IndexPath]() //计划消息所有数据对应 IndexPath
    /** 是否存在未读消息 默认 false */
    var hasUnReadMsg = false
    var firstLoadMsg = true //进入页面后第一次请求到消息数据
    var msgItems: [CRMessage] = []
    var msgSizes = [String:CGSize]()
    let maxMsgCount = 550 //当消息大于 maxMsgCount 条时，开始删除多余消息，只保留 (maxMsgCount - 50)条
    
    weak var chatMessageViewDelegate:CRChatMessageViewDelegate?
    
    var mergeFollowBethandler:((_ message:CRMessage) -> Void)? //点击合并跟单
    var voiceTapHandler:((_ voiceMsg:CRVoiceMessageItem,_ voiceImage:CRVoiceImageView) -> Void)? //点击语音
    var coverShareTapHandler:(() -> Void)? //点击分享注单视图
    var followBetHandler:((_ message:CRShareBetMessageItem,_ betModel:Int,_ inIndex:Int) -> Void)?
    var showFollowBetHandler:((_ betInfo:CRShareBetInfo?) -> Void)? //分享注单的注单信息
    var scrollViewWillBeginDraggingHandler:(() -> Void)?
    
    var tapAvatarPop:SwiftPopMenu? //点击头像弹出的菜单
    
    lazy var newMsgButton:UIButton = {
        let view = UIButton()
        view.setBackgroundImage(UIImage.init(named: "chatUnRead"), for: .normal)
        view.addTarget(self, action: #selector(tapNewMsgBtn), for: .touchUpInside)
        view.backgroundColor = UIColor.clear
        view.setTitleColor(UIColor.white, for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        return view
    }()
    
    lazy var followBetPop:CRFollowBetPop = {
        let view = Bundle.main.loadNibNamed("CRFollowBetPop", owner: self, options: nil)?.last as! CRFollowBetPop
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 3.0
        view.isHidden = true
        
        view.followBetHandler = {(message,betModel,inIndex) in
            self.followBetHandler?(message,betModel,inIndex)
        }
        
        return view
    }()
    
    //半透明弹窗
    lazy var floatView:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.frame = CGRect.init(x: 0, y: 0, width: CRConstants.screenWidth, height: CRConstants.screenHeight)
        view.addTarget(self, action: #selector(tapFloatView), for: .touchUpInside)
        return view
    }()
    
    lazy var remoteView:CRRemoteView = { //类遥控器控件
        let tutples = [("榜","drawerLotteryList", CRRemoteViewType.CRRemoteViewTypeLotteryList),("签","drawerSignIn", CRRemoteViewType.CRRemoteViewTypeSignIn),("客","drawerOnlineService", CRRemoteViewType.CRRemoteViewTypeOnlineService)]
        var tutpleOns:[(String,String,CRRemoteViewType)] = []
        if let config = getChatRoomSystemConfigFromJson()?.source{
            //中奖榜单,盈利榜单
            if [1,2,3].contains(switcnWinnerAndProfitListStatus()) {
                tutpleOns.append(("榜","drawerLotteryList", CRRemoteViewType.CRRemoteViewTypeLotteryList))
            }
            
            if config.switch_check_in_show == "1"{
                
                tutpleOns.append(("签","drawerSignIn", CRRemoteViewType.CRRemoteViewTypeSignIn))
            }
            //客服有链接地址
            if !config.name_station_online_service_link.isEmpty{
                tutpleOns.append(("客","drawerOnlineService", CRRemoteViewType.CRRemoteViewTypeOnlineService))
            }
            
            if config.switch_plan_user_show == "1" {
                tutpleOns.append(("计划","drawerTutorPlan", CRRemoteViewType.CRRemoteViewTypeMasterPlan))
            }
            if config.switch_long_dragon_show == "1"{
                tutpleOns.append(("长龙","drawerLongDragon", CRRemoteViewType.CRRemoteViewTypeLongDragon))
            }
        }
        let view = CRRemoteView.init(items: tutpleOns, locationY: remoterY)
        
        return view
    }()
    
    lazy var msgTable:UITableView = {
        let table = UITableView()
        table.backgroundColor = UIColor.clear
        table.delegate = self
        table.dataSource = self
        table.tableFooterView = UIView()
        //tableView的点击事件
        let tap = UITapGestureRecognizer(target: self, action: #selector(tableViewClick))
        tap.delegate = self
        table.addGestureRecognizer(tap)
        table.addObserver(self, forKeyPath: "bounds", options: [.new,.old], context: nil)
        table.register(CRTextMessageCell.self, forCellReuseIdentifier: "CRTextMessageCell")
        table.register(CRChatImageCell.self, forCellReuseIdentifier: "CRChatImageCell")
        table.register(CRShareBetCell.self, forCellReuseIdentifier: "CRShareBetCell")
        table.register(CRRedPacketCell.self, forCellReuseIdentifier: "CRRedPacketCell")
        table.register(CRVoiceCell.self, forCellReuseIdentifier: "CRVoiceCell")
        table.register(CRPlanCell.self, forCellReuseIdentifier: "CRPlanCell")
        table.register(CRRobotMsgCell.self, forCellReuseIdentifier: "CRRobotMsgCell")
        table.register(CRGainsLossesCell.self, forCellReuseIdentifier: "CRGainsLossesCell")
        table.separatorStyle = .none
        table.estimatedRowHeight = 150
        
        return table
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        backgroundColor = UIColor.init(hexString: "#F5F5F5")
        backgroundColor = UIColor.clear
        addSubview(msgTable)
        self.addSubview(self.newMsgButton)
        
        self.markAllMsgReaded()
        
        self.bringSubviewToFront( self.newMsgButton)
        
//        setupRefreshView()
        
        msgTable.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        
        self.newMsgButton.snp.makeConstraints { (make) in //根据图片比例，布局未读消息提示btn
            make.right.equalTo(-8)
            make.bottom.equalTo(-5)
            make.width.equalTo(41.8)
            make.height.equalTo(51.7)
        }
        
    }
    
    //MARK: - 刷新
    private func setupRefreshView() {
        refreshHeader.setRefreshingTarget(self, refreshingAction: #selector(headerRefresh))
        msgTable.mj_header = refreshHeader
    }
    
    @objc fileprivate func headerRefresh() {
        loadMoreMsgsHandler?()
        let timeOut:TimeInterval = 15
        
        DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
            self.msgTable.mj_header?.endRefreshing()
        }
    }
    
    ///显示 隐藏遥控器控件
    func showRemoter(show:Bool) {
        remoter.isHidden = !show
    }
    
    //设置遥控器
    func setupRemoter() {
        let originalX = remoteView.originalX
        remoter.layer.cornerRadius = remoteView.itemWidth * 0.5
        remoter.layer.masksToBounds = true
        remoter.setImage(UIImage.init(named: "drawerWelfare"), for: .normal)
        remoter.frame = CGRect.init(x: originalX, y: remoterY, width: remoteView.itemWidth, height: remoteView.itemHeight)
        remoter.addTarget(self, action: #selector(remoterAction), for: .touchUpInside)
        self.addSubview(remoter)
        self.addSubview(remoteView)
        
        self.bringSubviewToFront( remoter)
        self.bringSubviewToFront( remoteView)
    }
    
    @objc func remoterAction() {
        remoteView.showOrHideRemoterView()
    }
    
    //点击半透明背景视图
    @objc func tapFloatView() {
        self.removeFollowPop()
    }
    
    ///添加跟单弹窗, 合并跟单中，跟单的inIndex
    func addFollowPop(message:CRMessage,inIndex:Int = 0) {
        if let window = UIApplication.shared.keyWindow {
            self.followBetPop.isHidden = false
            window.addSubview(self.floatView)
            self.floatView.addSubview(self.followBetPop)
            window.bringSubviewToFront( self.floatView)
            
            self.floatView.snp.makeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets.zero)
            }
            
            self.followBetPop.snp.makeConstraints { (make) in
                make.centerY.equalTo(window.snp.centerY)
                make.centerX.equalTo(window.snp.centerX)
                make.height.equalTo(320)
                make.width.equalTo(270)
            }
            
            if let message = message as? CRShareBetMessageItem {
                self.followBetPop.configWith(message:message,inIndex: inIndex)
                
                if let shareBetModel = message.shareBetModel,let betInfos = shareBetModel.betInfos {
                    let betInfo = betInfos[inIndex]
                    //获取跟单彩种倒计时参数
                    showFollowBetHandler?(betInfo)
                }
            }
        }
    }
    ///移除跟单弹窗
    func removeFollowPop() {
        self.followBetPop.isHidden = true
        self.followBetPop.removeFromSuperview()
        self.floatView.removeFromSuperview()
    }
    
    func addMessage(message:CRMessage){
        if message.ownerType == .TypeSelf {
            msgItems.append(message)
            msgTable.reloadData()
            
            message.isReaded = true
            self.markAllMsgReaded(fromSelf: true)
            return
        }else {
            message.isReaded = false
            msgItems.append(message)
        }
        
        if !atBottomOfMsgTable() {
            self.hasUnReadMsg = true
            self.newMsgButton.isHidden = false
            self.refreshUnReadMsg()
            
            UIView.performWithoutAnimation {
                msgTable.reloadData()
            }
        }else {
            if msgItems.count >= maxMsgCount { //当消息大于 maxMsgCount 条时，开始删除多余消息，只保留 (maxMsgCount - 50)条
                let delCount = msgItems.count - maxMsgCount + 50
                msgItems.removeSubrange(0..<delCount)
            }
            
            msgTable.reloadData()
            markAllMsgReaded()
        }
    }
    
    ///重新赋值 msgItems
    func reAssignMsgItems(msgItems:[CRMessage]) {
        self.msgItems = msgItems
        UIView.performWithoutAnimation {
            msgTable.reloadData()
        }
    }
    
    //MARK: 收到消息的时候，是否在消息列表最下方
    func atBottomOfMsgTable() -> Bool {
        if let indexes = self.msgTable.indexPathsForVisibleRows {
            if indexes.count == 0 {
                return true
            }else {
                if let last = indexes.last{
                    let lastIndex = last.row
                    let counts = self.msgItems.count //当前消息总数
                    return lastIndex + 1 + 1 == counts //因为最新收到的消息，还没有reloaddata
                }
            }
        }
        return true
    }
    
    //MARK: 刷新未读消息数,从页面上塞选未读数，减少数据查询
    func refreshUnReadMsg() {
        if let indexes = self.msgTable.indexPathsForVisibleRows {
            if hasUnReadMsg {
                let lastIndex = indexes.last!.row
                //当前消息总数
                let counts = self.msgItems.count
                //未读数
                let unreadCount = counts - (lastIndex + 1)
                
                if unreadCount == 0 {
                    self.markAllMsgReaded()
                }else {
                    let unReadCount = realUnReadMsg(count: unreadCount)
                    self.newMsgButton.setTitle("\(unReadCount)", for: .normal)
                }
            }
        }
    }
    
    //MARK: 真正的消息未读数,
    ///count:筛选未显示出来的消息个数
    func realUnReadMsg(count:Int) -> Int {
        let unreadIndex = self.msgItems.count - count
        
        var unReadCount = 0
        for (index,model) in self.msgItems.enumerated() {
            if index < unreadIndex {
                model.isReaded = true
            }else if !model.isReaded {
                unReadCount += 1
            }
        }
        
        return unReadCount
        
        //        let readAry = self.msgItems
        //
        //        let ary = self.msgItems.suffix(from: self.msgItems.count - count) //需要遍历未读的数组
        //
        //        for (index,model) in ary.enumerated() {
        //            if !model.isReaded {
        //                return ary.count - (index + 1)
        //            }
        //        }
        //
        //        return 0
    }
    
    func deleteMessage(message:CRMessage){
        
        //        deleteMessage(message: message, animation: true)
    }
    
    
    //    func deleteMessage(message:CRMessage,animation:Bool){
    //        if let tempMessage = message{
    //            let index = self.msgItems
    //            let flag = self.chatMessageViewDelegate.
    //        }
    //    }
    //        - (void)deleteMessage:(TLMessage *)message withAnimation:(BOOL)animation
    //    {
    //    if (message == nil) {
    //    return;
    //    }
    //    NSInteger index = [self.data indexOfObject:message];
    //    if (self.delegate && [self.delegate respondsToSelector:@selector(chatMessageDisplayView:deleteMessage:)]) {
    //    BOOL ok = [self.delegate chatMessageDisplayView:self deleteMessage:message];
    //    if (ok) {
    //    [self.data removeObject:message];
    //    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:animation ? UITableView.RowAnimationAutomatic : UITableView.RowAnimationNone];
    //    //            [MobClick event:EVENT_DELETE_MESSAGE];
    //    }
    //    else {
    //    [TLUIUtility showAlertWithTitle:@"错误" message:@"从数据库中删除消息失败。"];
    //    }
    //    }
    //    }
    
    
    deinit {
        msgTable.removeObserver(self, forKeyPath: "bounds")
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: 设置所有消息已读
    func markAllMsgReaded(fromSelf:Bool = false) {
        self.newMsgButton.isHidden = true
        self.hasUnReadMsg = false
        
        for model in self.msgItems {
            model.isReaded = true
        }
        
        if self.msgItems.count > 0 && !fromSelf {
            self.msgTable.scrollToRow(at: IndexPath.init(item: (self.msgItems.count - 1), section: 0), at: .bottom, animated: true)
        }
    }

    //设置点击头像的 popMenu
    func setupPopMenu(message: CRMessage, headIcon: UIView) {
//        if message.userId == CRDefaults.getUserID() {
//            click_version_menu(message:message,isOthers:false)
//        }
        
        
        let tapAvatarPopDatas = getAvatarPopDatas(message: message)
        
        guard let delegate = UIApplication.shared.delegate,let window = delegate.window else {return}
        
        let isSender = message.userId == CRDefaults.getUserID()
        let popWidth:CGFloat = 120 //菜单宽
        let point = headIcon.convert(CGPoint.zero, to: window)
        let popHeight:CGFloat = CGFloat(44 * tapAvatarPopDatas.count)
        
        var x:CGFloat = 0
        if  isSender {
            x = point.x - 5 - SwiftPopMenu.arrowViewHeight - popWidth
        }else {
            x = point.x + headIcon.width + 5 + SwiftPopMenu.arrowViewHeight
        }
        
        
        var y:CGFloat = 0 //菜单相对screen的 y坐标
        var arrowMargin:CGFloat = 0 //箭头相对菜单 坐标 Y的偏移
        
        if point.y >= (window?.height)! * 0.5 {
            y = point.y - popHeight + headIcon.height
            arrowMargin = popHeight - headIcon.height * 0.5
        }else {
            y = point.y
            arrowMargin = headIcon.height * 0.5
        }
        
        let frame = CGRect.init(x: x, y: y, width: popWidth, height: popHeight)
        
        tapAvatarPop = SwiftPopMenu.init(frame: frame, arrowMargin: arrowMargin)
        tapAvatarPop?.arrowDirection = isSender ? .right : .left
        tapAvatarPop?.popData = tapAvatarPopDatas
        
        tapAvatarPop?.didSelectMenuBlock = { [weak self](index:Int)->Void in
            guard let weakSelf = self else {
                return
            }
            weakSelf.tapAvatarPop?.dismiss()
            weakSelf.click_version_menu(index: index,message:message,isOthers: true)
        }
        
        tapAvatarPop?.show()
    }
    
    ///点击头像 弹出菜单的某一项
    func click_version_menu(index:Int = 0,message: CRMessage,isOthers:Bool) {
//        if isOthers {
        
        let datas = getAvatarPopDatas(message: message)
        let title = datas[index].title
        
        var type = ChatTapAvtarType.informSomeOne
        
        if title == personInfo {
            type = .personInfo
        }else if title == informSoomeOne { //@
            type = .informSomeOne
        }else if title == clearScreen {//清屏
            type = .clearScreen
        }else if title == prohibitTalk { //禁言
            type = .prohibitTalk
        }else if title == cancelProhibitTalk { //取消禁言
            type = .cancelProhibitTalk
        }else if title == retractMsg { //撤回
            type = .retractMsg
        }else if title == cancelProhibitTalkAll { //全体解除禁言
            type = .cancelProhibitTalkAll
        }else if title == prohibitTalkAll { //全体禁言
            type = .prohibitTalkAll
        }else if title == privateChat { //私聊
            type = .privateChat
        }
        
        chatMessageViewDelegate?.chatUserIconDidClick(message:message,type: type)
        
//        }
        
//        else {
//            chatMessageViewDelegate?.chatUserIconDidClick(message:message,type: .informSomeOne)
//        }
    }
    
    ///根据message和用户信息，获得pop数据
    func getAvatarPopDatas(message:CRMessage) -> [(icon:String,title:String)] {
        var array = [(icon:String,title:String)]()
        
        let isSender = message.userId == CRDefaults.getUserID()
        if !isSender {
            array.append((icon:"",title:informSoomeOne))
        }else {
            array.append((icon:"",title:personInfo))
        }
        
        array.append((icon:"",title:clearScreen))
        
        if toolPermission.contains("\(sendMessageType.prohibit.rawValue)") && !isSender { //禁言,取消禁言
            array.append((icon:"",title:prohibitTalk))
            array.append((icon:"",title:cancelProhibitTalk))
        }
        
        if toolPermission.contains("\(sendMessageType.prohibitAll.rawValue)") && !isSender { //禁言房间,取消房间禁言 
            array.append((icon:"",title:prohibitTalkAll))
            array.append((icon:"",title:cancelProhibitTalkAll))
        }
        
        let isRedpacket = message.msgType == "\(CRMessageType.RedPacket.rawValue)" //是红包消息
        if toolPermission.contains("\(sendMessageType.retract.rawValue)") && !isRedpacket && (isSender || agentRoomHostSelf || userType == 4)  { //撤回
            array.append((icon:"",title:retractMsg))
        }
        
        if !["6","7"].contains(message.msgType) {
            
            var finalPermission = false //是否可以私聊
            
            if privatePermission { //如果有权限则可以私聊任何会员,即使对方没权限
                finalPermission = true
            }else if userType == 4 { //自己是前台管理员
                //当自己没权限时，如果可以私聊对方，则对方必需要有权限 && 对方是管理员
                finalPermission = message.userType == "4" && message.privatePermission
            }else { //自己不是前台管理员
                //自己不是前台管理员 && 没有权限时，需要 ( 对方是管理员 && 有权限)
                finalPermission = message.userType == "4" && message.privatePermission
            }
            
            if finalPermission {
                array.append((icon:"", title:privateChat)) //私聊
            }
        }
        
        return array
    }
    
}

extension CRChatMessageView: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let model = self.msgItems[indexPath.row]
        //        if model.messageType == .ShareBet {
        //            chatMessageViewDelegate?.chatMessageViewTableViewClick()
        //            self.addFollowPop(message: model)
        //        }
        
        coverShareTapHandler?()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = msgItems[indexPath.row]
        //文本
        if message.messageType == .Text {
            guard let textMsgCell =  tableView.dequeueReusableCell(withIdentifier: "CRTextMessageCell",for:indexPath) as? CRTextMessageCell else{
                fatalError("dequeueReusableCell CRTextMessageCell failure")
            }
            textMsgCell.setMessage(message: message)
            textMsgCell.messageDelegate = self
            return textMsgCell
            
        }else if (message.messageType == .Image){
            guard let imgMsgCell = tableView.dequeueReusableCell(withIdentifier: "CRChatImageCell", for: indexPath) as?CRChatImageCell else {
                fatalError("dequeueReusableCell CRChatImageCell failure")
            }
            imgMsgCell.msgImageV.image = UIImage(named: "")
            imgMsgCell.setMessage(message: message)
            imgMsgCell.messageDelegate = self
            
            return imgMsgCell
        }else if (message.messageType == .ShareBet) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRShareBetCell", for: indexPath) as? CRShareBetCell else {
                fatalError("dequeueReusableCell CRShareBetCell failure")
            }
            cell.setMessage(message: message)
            cell.messageDelegate = self
            
            cell.configIndex(index: indexPath.row)
            
            //            cell.coverTapHandler = {[weak self] (index) in
            //                guard let weakSelf = self else {return}
            //
            //                let model = weakSelf.msgItems[indexPath.row]
            //                if model.messageType == .ShareBet {
            //                    weakSelf.chatMessageViewDelegate?.chatMessageViewTableViewClick()
            //                    weakSelf.addFollowPop(message: model)
            //                }
            //
            //                weakSelf.coverShareTapHandler?()
            //            }
            
            cell.mergeCoverTapHandler = {[weak self] (index,inIndex) in
                guard let weakSelf = self else {return}
                
                let model = weakSelf.msgItems[indexPath.row]
                if model.messageType == .ShareBet {
                    weakSelf.chatMessageViewDelegate?.chatMessageViewTableViewClick()
                    
                    if inIndex == -1 {
                        weakSelf.mergeFollowBethandler?(model)
                    }else {
                        weakSelf.addFollowPop(message: model,inIndex: inIndex)
                    }
                }
                
                weakSelf.coverShareTapHandler?()
            }
            
            return cell
        }else if message.messageType == .RedPacket {
            //红包
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRRedPacketCell", for: indexPath) as? CRRedPacketCell else {
                fatalError("dequeueReusableCell CRRedPacketCell failure")
            }
            cell.setMessage(message: message)
            //消息点击的代理
            cell.messageDelegate = self
            return cell
        }else if message.messageType == .ShareGainsLosses{
            //分享今日赢亏
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRGainsLossesCell") as? CRGainsLossesCell else{
                fatalError("dequeueReusableCell CRRedPacketCell failure")
            }
            cell.setMessage(message: message)
            
            cell.messageDelegate = self
            return cell
            
        }else if message.messageType == .Voice {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRVoiceCell", for: indexPath) as? CRVoiceCell else {fatalError("dequeueReusableCell CRVoiceCell failure")}
            cell.setMessage(message: message)
            cell.voiceMsgDelegate = self
            cell.messageDelegate = self
            return cell
        }else if message.messageType == .PlanMsg {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRPlanCell", for: indexPath) as? CRPlanCell else {
                fatalError("dequeueReusableCell CRPlanCell failure")
            }
            
            if let visibleIndexes = tableView.indexPathsForVisibleRows {
                if visibleIndexes.contains(indexPath) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        cell.setMessage(message: message)
                    })
                }
            }
            
            cell.getPlanHeightHandler = {[weak self] () in
                guard let weakSelf = self else {return}
                
                //                if !weakSelf.planIndexpathes.contains(indexPath) { 
                //                    weakSelf.planIndexpathes.append(indexPath)
                //                }
                
                if let visibleIndexes = tableView.indexPathsForVisibleRows {
                    if visibleIndexes.contains(indexPath) {
                        UIView.performWithoutAnimation {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                if let visibleIndexes = tableView.indexPathsForVisibleRows {
                                    if visibleIndexes.contains(indexPath) {
                                        weakSelf.msgTable.reloadData()
                                    }
                                }
                            })
                        }
                    }
                }
            }
            
            return cell
        }else if message.messageType == .robotMsg {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRRobotMsgCell", for: indexPath) as? CRRobotMsgCell else {
                fatalError("dequeueReusableCell CRRobotMsgCell failure")
            }
            
            cell.setMessage(message: message)
            
            cell.getRobotWebHeightHandler = {[weak self] () in
                guard let weakSelf = self else {return}
                
                if !weakSelf.robotIndexpathes.contains(indexPath) {
                    weakSelf.robotIndexpathes.append(indexPath)
                }
                
                if let visibleIndexes = tableView.indexPathsForVisibleRows {
                    if visibleIndexes.contains(indexPath) {
                        UIView.performWithoutAnimation {
                            if weakSelf.robotIndexpathes.count == 1 {
                                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                                    UIView.performWithoutAnimation {
                                        weakSelf.msgTable.beginUpdates()
                                        weakSelf.msgTable.endUpdates()
                                    }
                                })
                            }else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                                    if let visibleIndexes = tableView.indexPathsForVisibleRows {
                                        if visibleIndexes.contains(indexPath) {
                                            UIView.performWithoutAnimation {
                                                weakSelf.msgTable.beginUpdates()
                                                weakSelf.msgTable.endUpdates()
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
            
            return cell
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let message = self.msgItems[indexPath.row]
        var margin: CGFloat = 0
        if message.isShowTime {
            margin = 25 //显示时间高20间距5
        }
        
        if ((message.messageType == .PlanMsg || message.messageType == .robotMsg)) && (message.messageFrame == nil || (message.messageFrame?.height ?? 0) == 0)  {
            return 80
        }
        
        return CGFloat(message.messageFrame?.height ?? 0) + margin
    }
    
}

//MARK: - UIScrollViewDelegate
extension CRChatMessageView:UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //        self.scrollViewWillBeginDraggingHandler?()
        chatMessageViewDelegate?.chatMessageViewTableViewClick()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.refreshUnReadMsg()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
}
extension CRChatMessageView:refreshMessageDelegate{
    func refreshMessageLocation() {
        if atBottomOfMsgTable() || firstLoadMsg {
            markAllMsgReaded()
        }
        
        firstLoadMsg = false
    }
}

extension CRChatMessageView{
    //判断图片长度
    func imageShowSize(imgString:String) -> CGSize {
        
        let decodedImageData = Data(base64Encoded: imgString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
        let decodeImage = UIImage(data: decodedImageData!)
        let image = decodeImage
        let imageWidth = image?.size.width ?? 0
        let imageHeight = image?.size.height ?? 0
        //屏的70%
        let max_width = kScreenWidth * 0.7
        let max_height = 100
        if Float(imageWidth) >= Float(imageHeight) {
            
            return CGSize(width: max_width, height: imageHeight * max_width/imageWidth)
        }else{
            
            return CGSize(width: ((imageWidth) * CGFloat(max_height)/imageHeight), height: CGFloat(max_height))
        }
    }
}
//MARK监听方法
extension CRChatMessageView{
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if  keyPath == "bounds" {
            guard let changes = change else{return}
            let oldBounds = changes[NSKeyValueChangeKey.oldKey] as? CGRect
            let newBounds = changes[NSKeyValueChangeKey.newKey] as? CGRect
            let t = (oldBounds?.size.height)! - (newBounds?.size.height)!
            if t > 0 && fabs(msgTable.contentOffset.y + t + (newBounds?.size.height)! - msgTable.contentSize.height) < 1.0{
                msgTable.scrollToBottomAnimation(animation: false)
            }
        }
    }
}
//MARK:cell代理的点击方法
extension CRChatMessageView:CRMessageCellDelegate{
    ///点击头像
    func messageCellDidClickUserIcon(message: CRMessage, headIcon: UIView) {
        let isSender = message.userId == CRDefaults.getUserID()
        
        if !isPrivateChat {
            setupPopMenu(message: message, headIcon: headIcon)
        }else if isSender {
            chatMessageViewDelegate?.chatUserIconDidClick(message:message,type: .personInfo)
        }
    }
    
    // 刷新图片的大小
    func reloadImageCell() {
        msgTable.reloadData()
    }
    
    func messageCellLongPress(message: CRMessage) {
        
    }
    
    func messageCellTap(message:CRMessage) {
        //图片点击
        self.chatMessageViewDelegate?.chatMessageDisplayViewDidClick(message:message)
    }
}
//MARK:事件响应
extension CRChatMessageView{
    @objc func tapNewMsgBtn() {
        self.markAllMsgReaded()
    }
    
    @objc func tableViewClick(){
        chatMessageViewDelegate?.chatMessageViewTableViewClick()
    }
}

extension CRChatMessageView:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if String(describing: touch.view?.superview?.classForCoder).contains("CRShareBetView") || String(describing: touch.view?.superview?.classForCoder).contains("CRShareBetCell") {
            return false
        }else {
            return true
        }
    }
}
extension CRChatMessageView:CRVoiceCellDelegate {
    func voiceTap(message: CRVoiceMessageItem, voiceImage: CRVoiceImageView) {
        voiceTapHandler?(message,voiceImage)
        
    }
}
