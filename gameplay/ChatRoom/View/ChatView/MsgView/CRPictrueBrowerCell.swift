//
//  CRPictrueBrowerCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/27.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

protocol PhotoBrowserViewCellDelegate : NSObjectProtocol {
    func imageViewClick()
}

class CRPictrueBrowerCell: UICollectionViewCell {
    
    fileprivate lazy var progressView : CRProgressView = CRProgressView()
    
    var delegate : PhotoBrowserViewCellDelegate?
    /**图片预览加载*/
    lazy var pictrueActiveityIndicator:UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView()
        indicatorView.layer.cornerRadius = 5
//        indicatorView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        //        indicatorView.startAnimating()
        indicatorView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        indicatorView.style = UIActivityIndicatorView.Style.whiteLarge
        return indicatorView
    }()
    
    var imageUrl:String?{
        didSet{
            setupContent(url: imageUrl)
        }
    }
    var progress:CGFloat?{
        didSet{
            
        }
    }
    lazy var scrollView : UIScrollView = {
        let scv = UIScrollView()
        scv.delegate = self
        return scv
    }()
    var imageUrlImageV:UIImageView? = {
        let imageV = UIImageView()
        imageV.isUserInteractionEnabled = true
        return imageV
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(scrollView)
        scrollView.addSubview(imageUrlImageV!)
        imageUrlImageV?.addSubview(pictrueActiveityIndicator)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideImageV))
        imageUrlImageV?.addGestureRecognizer(tap)
        
        // 2.设置子控件frame
        scrollView.frame = contentView.bounds
        scrollView.frame.size.width -= 20
        progressView.bounds = CGRect(x: 0, y: 0, width: 50, height: 50)
        progressView.center = CGPoint(x: UIScreen.main.bounds.width * 0.5, y: UIScreen.main.bounds.height * 0.5)
        
        // 3.设置控件的属性
        progressView.isHidden = true
        
        scrollView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(contentView)
        }
        imageUrlImageV?.snp.makeConstraints { (make) in
            make.center.equalTo(scrollView.snp.center)
        }
        pictrueActiveityIndicator.snp.makeConstraints { (make) in
            make.centerX.equalTo((imageUrlImageV?.snp.centerX)!)
            make.centerY.equalTo((imageUrlImageV?.snp.centerY)!)
        }
        
        pictrueActiveityIndicator.startAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
//MARK:设置cell的内容
extension CRPictrueBrowerCell{
    fileprivate func setupContent(url:String?){
        guard  let imageUrl = url else {
            return
        }

        
        imageUrlImageV?.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "image_placeholder"), options: nil, progressBlock: { (currentProgress, totalProgress) in
            
            self.progressView.progress = Double(currentProgress / totalProgress)
            
        }) {[weak self] (image, error, cache, imagUrl) in
           
            if image == nil{
                return
            }
            let originalImageWidth = image?.size.width ?? 0
            let originaImageHeight = image?.size.height ?? 0
            //需要的图片宽
            var imageWidth:CGFloat = 0
            // 图片宽大于屏宽
            if originalImageWidth > kScreenWidth{
                imageWidth = kScreenWidth
            }else{
                imageWidth = originalImageWidth
            }
            //等比缩放
            let imageHeight = kScreenWidth * originaImageHeight / originalImageWidth
            if let weakSelf = self{
                
                weakSelf.progressView.isHidden = true
                weakSelf.imageUrlImageV?.image = image
                if let pictrueImageV = weakSelf.imageUrlImageV {
                    pictrueImageV.snp.makeConstraints({ (make) in
                        make.width.equalTo(imageWidth)
                        make.height.equalTo(imageHeight)
                    })
                }
               
                // 设置代理
                // 设置最大缩放比例
                weakSelf.scrollView.maximumZoomScale = 2.0;
                // 设置最小缩放比例
                weakSelf.scrollView.minimumZoomScale = 1;
                //停止菊花转动
                weakSelf.pictrueActiveityIndicator.stopAnimating()
            }
        }
        // 5.设置scrollView的contentSize
        scrollView.contentSize = CGSize(width: width, height: height)
    }
    
}

extension CRPictrueBrowerCell:UIScrollViewDelegate{
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return imageUrlImageV
    }
}
extension CRPictrueBrowerCell{
    @objc func hideImageV(){
        delegate?.imageViewClick()
    }
   
}
