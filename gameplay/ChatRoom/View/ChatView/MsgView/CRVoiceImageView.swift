//
//  CRVoiceImageView.swift
//  gameplay
//
//  Created by admin on 2019/10/28.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRVoiceImageView: UIImageView {
    var imagesArray = [UIImage]()
    var normalImage = UIImage()
    var isFromMe = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setIsFromMe(fromMe: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIsFromMe(fromMe:Bool) {
        isFromMe = fromMe
        if isFromMe {
            imagesArray.removeAll()
            imagesArray.append(UIImage.init(named: "message_voice_sender_playing_1")!)
            imagesArray.append(UIImage.init(named: "message_voice_sender_playing_2")!)
            imagesArray.append(UIImage.init(named: "message_voice_sender_playing_3")!)
            normalImage = UIImage.init(named: "message_voice_sender_normal")!
        }else {
            imagesArray.removeAll()
            imagesArray.append(UIImage.init(named: "message_voice_receiver_playing_1")!)
            imagesArray.append(UIImage.init(named: "message_voice_receiver_playing_2")!)
            imagesArray.append(UIImage.init(named: "message_voice_receiver_playing_3")!)
            normalImage = UIImage.init(named: "message_voice_receiver_normal")!
        }
        image = normalImage
    }
    
    func startPlayingAnimation() {
        animationImages = imagesArray
        animationRepeatCount = 0
        animationDuration = 1
        startAnimating()
    }
    
    func stopPlayingAnimation() {
        stopAnimating()
    }
}


















