//
//  CRVoiceButton.swift
//  gameplay
//
//  Created by Gallen on 2019/10/1.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRVoiceButton: UIView {
    
    var touchBegin:(()->())?
    var touchMove:((_ cancel:Bool)->())?
    var touchCancel:(()->())?
    var touchEnd:(()->())?
    
    var normalTitle:String?{
        get{
            return  "按住说话(限时15s)"
        }
    }
    var cancelTitle:String?{
        get{
            return "松开 结束"
        }
    }
    
    var highlightTitle:String?{
        get{
            return "松开 取消"
        }
     
    }
    var highlightColor:UIColor?{
   
        get{
            return UIColor(white: 0, alpha: 0.1)
        }
    }
    lazy var titleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.colorWithRGB(r: 0.3, g: 0.3, b: 0.3, alpha: 1)
        label.textAlignment = .center
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 4
        layer.masksToBounds = true
        layer.borderWidth = 0.5
        layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
        addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    func setTouchBeginAction(touchBegin:@escaping ()->(Void),
                             willTouchCancel:@escaping (_ cancel:Bool)->(Void),
                             touchEnd:@escaping ()->(Void),
                             touchCancel:@escaping ()->(Void)){
        self.touchBegin = touchBegin;
        self.touchMove = willTouchCancel;
        self.touchCancel = touchCancel;
        self.touchEnd = touchEnd;
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        backgroundColor = highlightColor
        titleLabel.text = highlightTitle
        touchBegin?()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch : AnyObject in touches {
            let t : UITouch = touch as! UITouch;
            let currentPoint = t.location(in: self)
            let moveIn = currentPoint.x >= 0 && currentPoint.x < width && currentPoint.y >= 0 && currentPoint.y <= height
            if moveIn{
                titleLabel.text = highlightTitle
            }else{
                titleLabel.text = cancelTitle
            }
            touchMove?(!moveIn)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        backgroundColor = UIColor.clear
        titleLabel.text = normalTitle
        for touch : AnyObject in touches {
            let t : UITouch = touch as! UITouch;
            let currentPoint = t.location(in: self)
            let moveIn = currentPoint.x >= 0 && currentPoint.x <= width && currentPoint.y >= 0 && currentPoint.y <= height;
            if moveIn && (touchEnd != nil){
                self.touchEnd?()
            }else if (!moveIn && touchCancel != nil){
                self.touchCancel?()
            }
        }
        
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        backgroundColor = UIColor.clear
        titleLabel.text = self.normalTitle
        touchCancel?()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
