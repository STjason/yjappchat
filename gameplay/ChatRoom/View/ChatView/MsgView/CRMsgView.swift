//
//  CRMsgView.swift
//  Chatroom
//
//  Created by admin on 2019/7/20.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

enum CRMsgViewType {
    case MsgViewSend
    case MsgViewReceive
}



class CRMsgView: UIView {
    //默认消息类型
    var msgType:messageType = .TEXT_MSG_TYPE
    
    var bubbleImageView = UIImageView()
    
    let headUrl = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1563640771334&di=8bd24d89fb93f690c04bd2fe0e581c61&imgtype=0&src=http%3A%2F%2Fmmbiz.qpic.cn%2Fmmbiz_png%2FGkwibtSWqHyPz7D7a6mGjVPQ4gPKTL5ictc4iaJtSYMmKIfBlUsjzTeKQJ2coArVGh9hVHibx3APGh3dKCRvr8sJiag%2F640%3Fwx_fmt%3Dpng"
    
    lazy var headImage:UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 20
        return view
    }()
    
    lazy var chatBaseRight:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var chatBaseLeft:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var chatLabel:UILabel = {
        let view = UILabel()
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    lazy var imageMessageImage:UIImageView = {
        let msgImageV = UIImageView()
        return msgImageV
    }()
    
    
    convenience init(type:CRMsgViewType,msgType:messageType) {
        self.init()
        self.configWith(type: type, msgType: msgType)
    }
    
    func updateMsg(text:String?,
                   font:UIFont,
                   textColor:UIColor,
                   textAlignment:NSTextAlignment,
                   numberOfLines:Int,
                   msgType:messageType,
                   msgImageUrl:String?) {
        if msgType == .TEXT_MSG_TYPE {
            
            self.chatLabel.text = text
            self.chatLabel.font = font
            self.chatLabel.textColor = textColor
            self.chatLabel.textAlignment = textAlignment
            self.chatLabel.numberOfLines = numberOfLines
            
        }else if msgType == .IMAGE_MSG_TYPE{
            
            if msgImageUrl?.length != 0 {
                let decodedImageData = Data(base64Encoded: msgImageUrl!, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
                let decodeImage = UIImage(data: decodedImageData!)
                imageMessageImage.image = decodeImage
                
                bubbleImageView.image = UIImage(named: "senderTextBubble")
                
                let layer = bubbleImageView.layer
                layer.frame = CGRect(origin: CGPoint.zero, size: bubbleImageView.layer.frame.size)
                imageMessageImage.layer.mask = layer
            }
        }else if msgType == .SHARE_BET_MSG_TYPE {
            
        }
       
    }
    
    
    func updateMsgAlignment(textAlignment:NSTextAlignment) {
        self.chatLabel.textAlignment = textAlignment
    }
    //配置样式
    private func configWith(type:CRMsgViewType,msgType:messageType) {
        
        let headImageWH:CGFloat = 40
        self.addSubview(self.headImage)
        
        if let url = URL.init(string: self.headUrl) {
            self.headImage.kf.setImage(with: ImageResource.init(downloadURL: url))
        }
        
      
        if type == .MsgViewSend {
            //背景气泡图
            bubbleImageView.image = UIImage.init(named: "senderTextBubble")?.resizableImage(withCapInsets: UIEdgeInsets(top: 30, left: 30, bottom: 35, right: 35), resizingMode: .stretch)
            self.addSubview(self.chatBaseRight)
            self.chatBaseRight.addSubview(bubbleImageView)
            bubbleImageView.snp.makeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            }
          
            
            self.headImage.snp.makeConstraints { (make) in
                make.width.equalTo(headImageWH)
                make.height.equalTo(headImageWH)
                make.right.equalTo(-10)
                make.top.equalTo(0)
            }
            
            self.chatBaseRight.snp.makeConstraints {[weak self] (make) in
                make.top.equalTo(0)
                make.left.equalTo(0)
                make.bottom.equalTo(0)
                if let weakSelf = self {
                    make.right.equalTo(weakSelf.headImage.snp.left).offset(-5)
                }
            }
            if msgType == .TEXT_MSG_TYPE{
                //添加显示文字
                self.chatBaseRight.addSubview(self.chatLabel)
                self.chatLabel.snp.makeConstraints { (make) in
                    make.left.equalTo(10)
                    make.right.equalTo(-15)
                    make.top.equalTo(0)
                    make.bottom.equalTo(-10)
                }
            }else if (msgType == .IMAGE_MSG_TYPE){
                //添加显示图片
                self.chatBaseRight.addSubview(imageMessageImage)
                imageMessageImage.snp.makeConstraints { (make) in
                    make.left.equalTo(0)
                    make.right.equalTo(0)
                    make.top.equalTo(0)
                    make.bottom.equalTo(0)
                }
            }
           
            
        }else {
            let bubbleImage = UIImageView.init()
            
            self.addSubview(self.chatBaseLeft)
            self.chatBaseLeft.addSubview(bubbleImage)
            self.chatBaseLeft.addSubview(self.chatLabel)
            
            bubbleImage.image = UIImage.init(named: "receiverTextBubble")?.resizableImage(withCapInsets: UIEdgeInsets(top: 40, left: 30, bottom: 45, right: 35), resizingMode: .stretch)
            bubbleImage.snp.makeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            }
            
            self.headImage.snp.makeConstraints { (make) in
                make.width.equalTo(headImageWH)
                make.height.equalTo(headImageWH)
                make.left.equalTo(10)
                make.top.equalTo(10)
            }
            
            self.chatBaseLeft.snp.makeConstraints {[weak self] (make) in
                make.top.equalTo(0)
                make.right.equalTo(0)
                make.bottom.equalTo(0)
                if let weakSelf = self {
                    make.left.equalTo(weakSelf.headImage.snp.right).offset(5)
                }
            }
            
            self.chatLabel.snp.makeConstraints { (make) in
                make.left.equalTo(15);
                make.right.equalTo(-10);
                make.top.equalTo(10);
                make.bottom.equalTo(0);
            }
        }
    }

}








