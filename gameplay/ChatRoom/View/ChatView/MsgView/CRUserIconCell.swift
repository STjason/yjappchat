//
//  CRUserIconCell.swift
//  gameplay
//
//  Created by Gallen on 2019/10/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRUserIconCell: UICollectionViewCell {
    lazy  var imageV:UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = ((kScreenWidth - 50) / 4) * 0.5
        imageView.layer.masksToBounds = true
        return imageView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageV)
        layer.cornerRadius = 4
        layer.masksToBounds = true
        backgroundColor = UIColor.clear
        imageV.snp.makeConstraints { (make) in
           make.edges.equalTo(contentView)
        }
    }
    
    var urlString:String?{
        didSet{
            if let imageString = urlString{
                imageV.kf.setImage(with: URL(string: imageString))
            }
        }
    }
    
    var localImageString:String?{
        didSet{
            if let imageString = localImageString{
                imageV.image(imageString)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
