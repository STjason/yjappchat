//
//  CRReceiveRedPacketDetailView.swift
//  gameplay
//
//  Created by Gallen on 2019/9/29.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

enum redPackStatus:String {
    /**已领取*/
    case received = "已领取"
    /**已领完*/
    case haveFinished = "已领完"
}

class CRReceiveRedPacketDetailView: UIView {
    
    var isShow:Bool = false
    //是否查看领取明细
    var isShowDetail:Bool = true{
        didSet{
            if isShowDetail {
                self.receiveCountDescriptionLabel.isHidden = false
            }else{
                self.receiveCountDescriptionLabel.isHidden = true
            }
        }
    }
    
    var redPacketStatus:String?{
        didSet{
            if redPacketStatus == "se34" {
                redPacketReceiveMoneyLabel.isHidden = false
                redPacketStatusLabel.text = "您已领过该红包"
            }else if redPacketStatus == "se23"{
                redPacketReceiveMoneyLabel.isHidden = true
                redPacketStatusLabel.text = "红包已被抢完"
            }else if redPacketStatus == "se44"{
                redPacketReceiveMoneyLabel.isHidden = true
                redPacketStatusLabel.text = "该红包已过期"
            }else{
//                redPacketReceiveMoneyLabel.isHidden = false
                redPacketStatusLabel.text = "已存入余额"
                //红包领取金额
            }
        }
    }
    var receiveRedPacketDetailItem:CRReceiveDetailParent?{
        didSet{
            guard let receiveRedItem  = receiveRedPacketDetailItem  else {return}
            //红包剩余金额
            let balance = receiveRedItem.balance
            //红包个数
            let countNum = receiveRedItem.countNum
            //已经领取个数
            let hasNum = receiveRedItem.hasNum
            //红包备注
            let remark = receiveRedItem.remark
            //红包总金额
            let countMoney = receiveRedItem.countMoney
            
//            self.receiveCountDescriptionLabel.text = String.init(format:"已领取%d/%d个,共%.2f/%.2f元",hasNum,countNum,Float(Float(countMoney)  - Float(balance)),countMoney)
            self.receiveCountDescriptionLabel.text = String.init(format:"已领取%d/%d个,已抢%.2f元",hasNum,countNum,Float(Float(countMoney)  - Float(balance)))
            redPacketNameLabel.text = remark
        }
    }
    
    
    var receiveRedPacketDetailCustomItem:CRReceiveDetailCustom?{
        didSet{
            guard let redPacketCustomItem = receiveRedPacketDetailCustomItem else{return}
            redPacketOwnerIconImageV.kf.setImage(with: URL(string: redPacketCustomItem.avatar), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
            if redPacketCustomItem.status == "se34" {
                redPacketReceiveMoneyLabel.isHidden = true
                redPacketStatusLabel.text = "您已领过该红包"
            }else if redPacketCustomItem.status == "se23"{
                redPacketReceiveMoneyLabel.isHidden = true
                redPacketStatusLabel.text = "红包已被抢完"
            }else if redPacketCustomItem.status == "se44"{
                redPacketReceiveMoneyLabel.isHidden = true
                redPacketStatusLabel.text = "该红包已过期"
            }
            //红包领取金额
            redPacketReceiveMoneyLabel.text = String.init(format:"%.2f元",redPacketCustomItem.money)
            
            //发红包的用户昵称
            let nickName = redPacketCustomItem.nickName
            
            redPacketOwnerNameLabel.text = nickName.isEmpty ? redPacketCustomItem.nativeAccount : nickName
        }
    }
    
    var totalPeopleDetails:[CRReceiveDetailOther]?{
        didSet{
            //领取红包的详情
            guard let redPacketDetails = totalPeopleDetails else {return}
            
            if redPacketDetails.count > 0 {
                //有红包详情
                let chatRoomConfig = getChatRoomSystemConfigFromJson()
                
                detailTableView.snp.updateConstraints { (make) in
                    //显示红包详情
                    if chatRoomConfig?.source?.switch_red_info == "1"{
                        if redPacketDetails.count > 3{
                            make.height.equalTo(218 + 50 * 3)
                        }else{
                            make.height.equalTo(218 + 50 * redPacketDetails.count)
                        }
                    }else{
                        //不显示红包详情
                        make.height.equalTo(218)
                    }
                    
                }
                detailTableView.reloadData()
            }
        }
    }
    
    lazy var backView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 232, height: 218))
        view.backgroundColor = UIColor.white
        let backImageV = UIImageView()
        backImageV.frame = view.frame
        backImageV.image = UIImage(named: "redpacketbackgroud")
        view.addSubview(backImageV)
        return view
    }()
    
    lazy var closeButton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "redpacket_close_icon"), for: .normal)
        button.setTitleColor(UIColor.colorWithHexString("#bd2513"), for: .normal)
        button.addTarget(self, action: #selector(closeCurrentView), for: .touchUpInside)
        return button
    }()
     /**领取红包的人数详情*/
    lazy var detailTableView:UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero)
        tableView.delegate = self
        tableView.dataSource  = self
        tableView.bounces = false
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.separatorColor = UIColor.clear
        tableView.backgroundColor = UIColor.colorWithHexString("#c43928")
        tableView.layer.cornerRadius = 4
        tableView.layer.masksToBounds = true
        return tableView
    }()
    /**发送红包的人物icon*/
    lazy var redPacketOwnerIconImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.layer.cornerRadius = 50 * 0.5
        imageV.layer.masksToBounds = true
        return imageV
    }()
    /**发送红包的人物昵称*/
    lazy var redPacketOwnerNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.colorWithHexString("#bd2513")
        return label
    }()
    /**红包名字*/
    lazy var redPacketNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .center
        label.numberOfLines = 4
        label.textColor = UIColor.colorWithHexString("#bd2513")
        return label
    }()
    /**红包领取的状态*/
    lazy var redPacketStatusLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "已存入余额"
        label.textColor = UIColor.colorWithHexString("#f89c1b")
        return label
    }()
    /**领取多少*/
    lazy var redPacketReceiveMoneyLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.colorWithHexString("#f6ab25")
        return label
    }()
    /**领取人数详情*/
    lazy var receiveCountDescriptionLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .center
        label.textColor = UIColor.colorWithHexString("#b6b6b6")
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        addSubview(detailTableView)
        //发红包用户图像
        backView.addSubview(redPacketOwnerIconImageV)
        //关闭按钮
        backView.addSubview(closeButton)
        //红包的名字
        backView.addSubview(redPacketNameLabel)
        //收了红包多少钱
        backView.addSubview(redPacketReceiveMoneyLabel)
        //收了红包的状态
        backView.addSubview(redPacketStatusLabel)
        //发红包用户昵称
        backView.addSubview(redPacketOwnerNameLabel)
        //红包被领了多少
        backView.addSubview(receiveCountDescriptionLabel)
        //
//        backView.addSubview(detailTableView)
        self.detailTableView.tableHeaderView = backView
        let chatRoomConfig = getChatRoomSystemConfigFromJson()
        if chatRoomConfig?.source?.switch_red_info == "1"{
            //不显示详情
            isShowDetail = true
        }else{
            isShowDetail = false
        }
         //添加约束
        addLayoutConstraint()
        
        detailTableView.register(CRRedPacketDetailCell.self, forCellReuseIdentifier: "CRRedPacketDetailCell")
    }
    //添加约束
    func addLayoutConstraint(){
        
        detailTableView.snp.makeConstraints { (make) in
            make.width.equalTo(232)
            make.height.equalTo(218)
            make.centerX.equalTo(self.centerX)
            make.centerY.equalTo(self.centerY)
        }
        closeButton.snp.makeConstraints { (make) in
            make.right.equalTo(backView).offset(0)
            make.top.equalTo(backView).offset(0)
            make.width.height.equalTo(40)
        }
        redPacketOwnerIconImageV.snp.makeConstraints { (make) in
            make.top.equalTo(backView).offset(5)
            make.centerX.equalTo(backView.snp.centerX)
            make.width.height.equalTo(50)
        }
        redPacketOwnerNameLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.top.equalTo(redPacketOwnerIconImageV.snp.bottom).offset(10)
        }
        redPacketNameLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.left.equalTo(backView).offset(30)
            make.right.equalTo(backView).offset(-30)
            make.top.equalTo(redPacketOwnerNameLabel.snp.bottom).offset(10)
        }
        redPacketStatusLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.top.equalTo(redPacketNameLabel.snp.bottom).offset(5)
        }
        redPacketReceiveMoneyLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.top.equalTo(redPacketStatusLabel.snp.bottom).offset(3)
        }
        
        receiveCountDescriptionLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.left.equalTo(backView.snp.left).offset(5)
            make.right.equalTo(backView.snp.right).offset(-5)
            make.bottom.equalTo(backView.snp.bottom).offset(-6)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension CRReceiveRedPacketDetailView{
    @objc func closeCurrentView(){
        removeFromSuperview()
        isShow = false
    }
}
extension CRReceiveRedPacketDetailView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let chatRoomConfig = getChatRoomSystemConfigFromJson() else{return totalPeopleDetails?.count ?? 0}
        if chatRoomConfig.source?.switch_red_info == "1"{
            return totalPeopleDetails?.count ?? 0
        }else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CRRedPacketDetailCell", for: indexPath) as? CRRedPacketDetailCell
        let receiveDetailItem = totalPeopleDetails?[indexPath.row]
        cell?.receiveDetailItem = receiveDetailItem
        return  cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let chatRoomConfig = getChatRoomSystemConfigFromJson() else{return 50}
        if chatRoomConfig.source?.switch_red_info == "1"{
            return 50
        }else{
            return 0
        }
    }
}
