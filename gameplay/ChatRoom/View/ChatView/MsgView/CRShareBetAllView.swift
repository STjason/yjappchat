//
//  CRShareBetAllView.swift
//  YiboGameIos
//
//  Created by admin on 2019/12/18.
//  Copyright © 2019年 com.lvwenhan. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

//合单view
class CRShareBetAllView: UIView {
    var infos = [CRShareBetInfo]()
    static let singleH:CGFloat = 130
    static let followAllH:CGFloat = 40 //合并跟单高度
    
    var index = 0 //当前view持有消息列表中的索引
    ///index：所属消息的index，inIndex：是第几个跟单，-1则表示合并跟单
    var coverTapHandler:((_ index:Int,_ inIndex:Int) -> Void)?
    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = UIColor.clear
        view.layer.masksToBounds = true
        return view
    }()
    
    let containerView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    lazy var followAllOrderButton: UIButton = {
        let view = UIButton()
        view.setTitleColor(UIColor.init(hexString: "#FC7A11"), for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        view.titleLabel?.textAlignment = .center
        view.backgroundColor = UIColor.clear
        view.setTitle("合并跟单", for: .normal)
        view.addTarget(self, action: #selector(coverTap), for: .touchUpInside)
        return view
    }()
    
    @objc func coverTap() {
        coverTapHandler?(index,-1)
    }
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //配置UI数据
    func configWith(infos:[CRShareBetInfo]) {
        self.infos = infos
        
        for view in containerView.subviews {
            view.removeFromSuperview()
        }
        
        for view in scrollView.subviews {
            view.removeFromSuperview()
        }
        
        for view in self.subviews {
            view.removeFromSuperview()
        }
        
        scrollView.addSubview(containerView)
        addSubview(scrollView)
        
//        let height = infos.count >= 2 ? CGFloat(2) * CRShareBetAllView.singleH :  CRShareBetAllView.singleH
        
        var height:CGFloat = 0
        if infos.count >= 2 {
            height = CGFloat(2) * CRShareBetAllView.singleH
        }else {
            height = CRShareBetAllView.singleH
        }
        
        scrollView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(0)
            make.height.equalTo(height)
        }

        containerView.snp.makeConstraints { (make) in
            make.edges.equalTo(scrollView)
            make.width.equalTo(scrollView)
        }
        
//        scrollView.frame = CGRect.init(x: 0, y: 0, width: 300, height: height)
//        scrollView.contentSize = CGSize.init(width: 300, height: CGFloat(infos.count) * CRShareBetAllView.singleH)
        
        for (index,model) in infos.enumerated() {
            let view = CRShareBetSingleView.init(frame: .zero)
            view.inIndex = index
            view.index = self.index
            
            view.bottomLine.isHidden = infos.count == 1
            containerView.addSubview(view)
            
            view.configWith(info: model)
            
            view.coverTapHandler = {[weak self] (index,inIndex) in
                guard let weakSelf = self else {return}
                weakSelf.coverTapHandler?(index,inIndex)
            }
            
            let isLast = (index + 1 == infos.count)
            
            view.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(CGFloat(index) * CRShareBetAllView.singleH)
                make.left.right.equalTo(0)
                make.height.equalTo(CRShareBetAllView.singleH)

                if isLast {
                    make.bottom.equalTo(containerView.snp.bottom).offset(0)
                }
            }
            
//            view.frame = CGRect.init(x: 0, y: (CGFloat(index) * CRShareBetAllView.singleH), width: 300, height: CRShareBetAllView.singleH)
            
        }
        
        if infos.count > 1 { //数据多余 1条时，显示合并跟单按钮 
            addSubview(followAllOrderButton)
            
            followAllOrderButton.snp.makeConstraints { (make) in
                make.left.right.equalTo(0)
                make.height.equalTo(CRShareBetAllView.followAllH)
                make.bottom.equalTo(self)
                make.top.equalTo(scrollView.snp.bottom)
            }
        }
        
        //数据多余 2条时，需要滚动展示
        if infos.count > 2 {
            //显示条目多余两条的提示
            let tipImg = UIImageView.init(image: UIImage.init(named: "shareBet_icon_scroll"))
            tipImg.contentMode = .scaleAspectFill
            addSubview(tipImg)
            
            tipImg.snp.makeConstraints { (make) in
                make.bottom.equalTo(followAllOrderButton.snp.top).offset(-10)
                make.centerX.equalTo(self.snp.centerX)
                make.width.height.equalTo(40)
            }
        }
    }
}

//单个分享注单view
class CRShareBetSingleView: UIView {
    var inIndex = 0//当前跟单，在跟单列表中的索引
    var index = 0//当前view持有消息列表中的索引
    ///index：所属消息的index，inIndex：是第几个跟单，-1则表示合并跟单
    var coverTapHandler:((_ index:Int,_ inIndex:Int) -> Void)?
    
    let lotteryImgWH:CGFloat = 30 //彩种图片宽高
    
    lazy var lotteryImg:UIImageView = { //彩种icon
        var view = UIImageView()
        view.layer.cornerRadius = lotteryImgWH * 0.5
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFill
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var lotteryNameLabel: UILabel = { //彩种名字
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.black, alignment: .left)
        view.text = ""
        return view
    }()
    
    lazy var periodTitleLabel: UILabel = { //投注期号 title
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.lightGray, alignment: .left)
        view.text = "期号："
        return view
    }()
    
    lazy var periodLabel: UILabel = { //投注期号 value
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.lightGray, alignment: .left)
        view.text = ""
        return view
    }()
    
    lazy var ruleValeTitleName: UILabel = { //玩法名称 title
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.lightGray, alignment: .right)
        view.text = "玩法:"
        return view
    }()
    
    lazy var ruleValeName: UILabel = { //玩法名称 value
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.init(hexString: "#FC7A11"), alignment: .right)
        view.text = ""
        return view
    }()
    
    lazy var followOrderButton: UIButton = { //跟单
        let view = UIButton()
        view.layer.cornerRadius = 3
        view.backgroundColor = UIColor.init(hexString: "FF9D42")
        view.setTitleColor(UIColor.white, for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        view.setTitle("跟单", for: .normal)
        view.addTarget(self, action: #selector(coverTap), for: .touchUpInside)
        return view
    }()
    
    @objc func coverTap() {
        coverTapHandler?(index,inIndex)
    }
    
    lazy var inHorBottomLine: UIView = { //彩种期号和 投注金额,内容的分割线
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    lazy var inVerBottomLine: UIView = { //投注内容和 投注金额的分割线
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    lazy var numsTitleLabel: UILabel = {//投注内容 title
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.lightGray, alignment: .center)
        view.text = "投注内容"
        return view
    }()
    
    lazy var numsLabel: UILabel = {//投注内容 value
        let view = setupLabel(font: UIFont.systemFont(ofSize: 15), textColor: UIColor.init(hexString: "#FC7A11"), alignment: .center)
        view.numberOfLines = 0
        view.text = ""
        return view
    }()
    
    lazy var betMoneyTitleLabel: UILabel = { //投注金额 title
        let view = setupLabel(font: UIFont.systemFont(ofSize: 14), textColor: UIColor.lightGray, alignment: .center)
        view.text = "投注金额"
        return view
    }()
    
    lazy var betMoneyLabel: UILabel = { //投注金额 value
        let view = setupLabel(font: UIFont.systemFont(ofSize: 15), textColor: UIColor.init(hexString: "#FC7A11"), alignment: .center)
        view.text = ""
        return view
    }()
    
    lazy var bottomLine: UIView = { //多单时底部分割线
        let view = UIView()
        view.backgroundColor = UIColor.init(hexString: "#FC7A11")
        return view
    }()
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        addSubview(lotteryImg)
        addSubview(lotteryNameLabel)
        addSubview(periodTitleLabel)
        addSubview(periodLabel)
        addSubview(ruleValeTitleName)
        addSubview(ruleValeName)
        addSubview(followOrderButton)
        addSubview(inHorBottomLine)
        addSubview(inVerBottomLine)
        addSubview(numsTitleLabel)
        addSubview(numsLabel)
        addSubview(betMoneyTitleLabel)
        addSubview(betMoneyLabel)
        addSubview(bottomLine)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //更新彩种图片
    func upateLotImage(info:CRShareBetInfo){
        var imageString = info.lotIcon.isEmpty ? info.icon : info.lotIcon
        if imageString.isEmpty {
            imageString = BASE_URL + PORT + "/native/resources/images/" + info.lotCode + ".png"
        }
        
        let imageURL = URL(string: imageString)
        if let url = imageURL{
            self.lotteryImg.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    //配置UI数据
    func configWith(info:CRShareBetInfo) {
        
        let officalPeilv = info.version == "1" ? "[官]" : "[信]"
        let lotteryName = "\(info.lottery_type)\(officalPeilv)"
        
        self.upateLotImage(info: info)
        
        lotteryNameLabel.text = lotteryName
        periodLabel.text = info.lottery_qihao
        ruleValeName.text = info.lottery_play
        numsLabel.text = info.lottery_content
        
        if let money = Float(info.lottery_amount) {
            betMoneyLabel.text = String.init(format: "%.2f", money)
        }
        
        setupLayout()
    }
    
    func setupLayout() {
        lotteryImg.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.left.equalTo(8)
            make.bottom.equalTo(inHorBottomLine.snp.top).offset(-15)
            make.width.height.equalTo(lotteryImgWH)
        }
        
        lotteryNameLabel.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(inVerBottomLine.snp.left).offset(40)
            make.top.equalTo(lotteryImg.snp.top).offset(-10)
            make.left.equalTo(lotteryImg.snp.right).offset(5)
        }
        
        periodTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(lotteryNameLabel.snp.left)
            make.bottom.equalTo(lotteryImg.snp.bottom).offset(10)
        }
        
        periodLabel.snp.makeConstraints { (make) in
            make.left.equalTo(periodTitleLabel.snp.right)
            make.top.equalTo(periodTitleLabel.snp.top)
        }
        
        ruleValeName.snp.makeConstraints { (make) in
            make.top.equalTo(lotteryNameLabel.snp.top)
            make.right.equalToSuperview().offset(-3).priority(500)
        }
        
        ruleValeTitleName.snp.makeConstraints { (make) in
            make.left.greaterThanOrEqualTo(lotteryNameLabel.snp.right).offset(2)
            make.width.equalTo(40)
            make.right.equalTo(ruleValeName.snp.left).offset(0)
            make.top.equalTo(lotteryNameLabel.snp.top)
        }
        
        followOrderButton.snp.makeConstraints { (make) in
            make.right.equalTo(-8)
            make.height.equalTo(25)
            make.bottom.equalTo(periodTitleLabel.snp.bottom)
            make.width.equalTo(60)
        }
        
        inHorBottomLine.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        numsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(inHorBottomLine.snp.bottom).offset(0)
            make.left.equalTo(0)
            make.bottom.equalTo(numsTitleLabel.snp.top).offset(0)
            make.right.equalTo(inVerBottomLine.snp.left)
        }
        
        numsTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(numsLabel.snp.left)
            make.bottom.equalToSuperview().offset(-8)
            make.right.equalTo(numsLabel.snp.right)
        }
        
        inVerBottomLine.snp.makeConstraints { (make) in
            make.top.equalTo(inHorBottomLine.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(-8)
            make.width.equalTo(0.5)
        }
        
        betMoneyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(numsLabel.snp.top)
            make.left.equalTo(inVerBottomLine.snp.right)
            make.bottom.equalTo(numsLabel.snp.bottom)
            make.right.equalTo(0)
        }
        
        betMoneyTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(numsTitleLabel.snp.top)
            make.bottom.equalTo(numsTitleLabel.snp.bottom)
            make.left.equalTo(inVerBottomLine.snp.right)
            make.right.equalTo(0)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.left.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(1)
        }
    }
    
    //MARK: - private
    //配置label
    private func setupLabel(font:UIFont,textColor:UIColor?,alignment:NSTextAlignment) -> UILabel {
        let label = UILabel()
        label.font = font
        label.textColor = textColor
        label.textAlignment = alignment
        return label
    }
    
}
