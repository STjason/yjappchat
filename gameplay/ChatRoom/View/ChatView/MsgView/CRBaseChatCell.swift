//
//  CRBaseChatCell.swift
//  Chatroom
//
//  Created by admin on 2019/7/10.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import Kingfisher
/**发送消息者类型*/
enum senderType {
    /**自己发送的消息*/
    case owner
    /**别人发送的消息*/
    case others
    /**系统消息*/
    case system
}

protocol CRBaseChatCellDelegate:NSObjectProtocol {
    /**修改用户信息*/
    func modifyUserInfo()
}
class CRBaseChatCell: UITableViewCell {
    
    var senderType:senderType = .owner
    var message:CRMessage?
    //胜率 当前胜率大于后台的设置的胜率 就展示出来
    var winRage:Float = 0
    
    weak var messageDelegate:CRMessageCellDelegate?
    weak var modifyUserInfoDelegate:CRBaseChatCellDelegate?
    
    //覆盖在title一栏内容，用于响应点击，显示用户账户名和nickName的切换
    lazy var titleRowButton:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.clear
        view.addTarget(self, action: #selector(tapTitleBtn), for: .touchUpInside)
        return view
    }()
    
    //头像
    lazy var headIcon:UIImageView = {
        let imageV = UIImageView()
        imageV.layer.cornerRadius = 40 * 0.5
        imageV.layer.masksToBounds = true
        imageV.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(settingProfileInfo))
        imageV.addGestureRecognizer(tap)
        return imageV
    }()
    
    //头像
    lazy var headManagerIcon:UIImageView = {
        let imageV = UIImageView()
        imageV.layer.cornerRadius = 40 * 0.5
        imageV.layer.masksToBounds = true
        imageV.isHidden = true
        imageV.image = UIImage.init(named: "adminIcon")
        return imageV
    }()

    //发送过程中
    lazy var activeityIndicator:UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView()
        indicatorView.layer.cornerRadius = 5
        indicatorView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        indicatorView.style = UIActivityIndicatorView.Style.gray
        return indicatorView
    }()
    //消息气泡
    lazy var bubbleImageV:UIImageView = {
        let imageV = UIImageView()
        return imageV
    }()
        //用户名
    lazy var userNameLabel:UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = UIFont.systemFont(ofSize: 12)
        nameLabel.textColor = UIColor.black
        return nameLabel
    }()
    // 发送时间 新增
//    lazy var
        //发送时间
    lazy var timeLabel:UILabel = {
        let dateLabel = UILabel()
        dateLabel.isHidden = true
        dateLabel.font = UIFont.systemFont(ofSize: 12)
        dateLabel.textColor = UIColor.white
        dateLabel.backgroundColor = UIColor.gray
        dateLabel.textAlignment = .center
        dateLabel.alpha = 0.7
        dateLabel.layer.masksToBounds = true
        dateLabel.layer.cornerRadius = 5
        return dateLabel
    }()
    //消息状态
    lazy var sendMessageStatusImageV:UIImageView = {
        let sendImageV = UIImageView()
        sendImageV.isHidden = true
        sendImageV.image = UIImage(named: "msg_state_fail_resend")
        return sendImageV
    }()
    /**等级图片*/
    lazy var levelImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.contentMode = .scaleAspectFit
        return imageV
    }()
    /**等级名称*/
    lazy var levelNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.black
        return label
    }()
    
    /**导师*/
    lazy var teacherLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.red
        label.text = "导师"
        label.isHidden = true
        return label
    }()
    
    //发送时间
    lazy var sendDateLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.black
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 5
//        label.isHidden = true
        return label
    }()
    // 中奖率
    lazy var winRateLabel: CRInsetsLabel = {
        let label = CRInsetsLabel()
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.backgroundColor = UIColor.red
        label.textColor = UIColor.colorWithRGB(r: 252, g: 252, b: 0, alpha: 1.0)
        label.textInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        label.text = "投注命中率:90%"
        label.textAlignment = .center
        label.sizeToFit()
        label.layer.cornerRadius = 4
        label.layer.masksToBounds = true
        label.isHidden = true
        return label
    }()

    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.clear
        selectionStyle = .none
        //用户图像
        contentView.addSubview(headIcon)
        contentView.addSubview(headManagerIcon)
        contentView.addSubview(bubbleImageV)
        contentView.addSubview(userNameLabel)
        contentView.addSubview(timeLabel)
        contentView.addSubview(sendMessageStatusImageV)
        contentView.addSubview(activeityIndicator)
        //等级图片
        contentView.addSubview(levelImageV)
        //等级名称
        contentView.addSubview(levelNameLabel)
        contentView.addSubview(teacherLabel) //导师
        //发送时间
        contentView.addSubview(sendDateLabel)
        contentView.addSubview(titleRowButton)
        //胜率
        contentView.addSubview(winRateLabel)
        addConstraintLayout()
      
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func tapTitleBtn() {
        
        if !(CRDefaults.getUserType() == 2 || CRDefaults.getUserType() == 4) {
            return
        }
        
        guard let message = self.message else {
            return
        }
        
        if let userName = userNameLabel.text {
            var finalNickName = message.nickName.unicodeStr //nickName可能为空，这里处理最终显示为nickName的字段
            if finalNickName.isEmpty && !message.nativeAccount.isEmpty {
                finalNickName = message.nativeAccount.unicodeStr
            }else if finalNickName.isEmpty && !message.account.isEmpty {
                finalNickName = message.account.unicodeStr
            }
            
            let account = message.account.unicodeStr
            let formatCurrentName = userName.unicodeStr
            
            if finalNickName == formatCurrentName { //当前显示的是nickName
                userNameLabel.text = account
                userNameLabel.textColor = UIColor.red
                message.isShowAccountNow = true
            }else {
                userNameLabel.text = finalNickName
                userNameLabel.textColor = UIColor.black
                message.isShowAccountNow = false
            }
        }
    }
    
    func updateMessage(message:CRMessage){
        setMessage(message: message)
    }
    
    func addConstraintLayout(){
        headIcon.snp.makeConstraints { (make) in
            make.width.height.equalTo(40)
            make.right.equalTo(contentView).offset(-8)
            make.top.equalTo(timeLabel.snp.bottom).offset(8)
        }
        
        headManagerIcon.snp.makeConstraints { (make) in
            make.width.height.equalTo(40)
            make.top.equalTo(headIcon.snp.top)
            make.left.equalTo(headIcon.snp.left)
        }
        
        userNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(headIcon.snp.bottom).offset(-1)
            make.right.equalTo(headIcon.snp.left).offset(1)
        }
        
        sendDateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabel.snp.right).offset(2)
            make.centerY.equalTo(userNameLabel.snp.centerY)
        }
        
        winRateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabel.snp.right).offset(2)
            make.centerY.equalTo(userNameLabel.snp.centerY)
            make.height.equalTo(18)
        }
     
        bubbleImageV.snp.remakeConstraints { (make) in
            make.right.equalTo(headIcon.snp.left).offset(-5)
            make.top.equalTo(timeLabel.snp.bottom).offset(1)
        }
        //消息没有发送成功 给个状态icon
        sendMessageStatusImageV.snp.remakeConstraints { (make) in
            make.right.equalTo(bubbleImageV.snp.left).offset(-3)
            make.centerY.equalTo(bubbleImageV.snp.centerY)
            make.width.height.equalTo(15)
        }
        
        levelImageV.snp.makeConstraints { (make) in
            make.left.equalTo(headIcon.snp.left)
            make.top.equalTo(timeLabel.snp.bottom)
        }
        levelNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(levelImageV.snp.right).offset(5)
            make.centerY.equalTo(levelImageV.snp.centerY)
        }
        
        teacherLabel.snp.makeConstraints { (make) in
            make.left.equalTo(levelNameLabel.snp.right).offset(5)
            make.centerY.equalTo(levelNameLabel.snp.centerY)
        }
        
        activeityIndicator.snp.remakeConstraints { (make) in
            make.right.equalTo(bubbleImageV.snp.left).offset(-10)
            make.centerY.equalTo(bubbleImageV.snp.centerY)
            make.width.height.equalTo(15)
        }
    }
    
    func setMessage(message:CRMessage){
        //同一时间段 只显示一处时间(以天为单位eg:今天 昨天 昨天以前)
        if message.isShowTime {
            timeLabel.text = Date.creatDateString(creatAtString: message.time)
            timeLabel.isHidden = false
        }else{
            timeLabel.isHidden = true
        }
        //用户昵称
        var finalNickName = message.nickName.unicodeStr //nickName可能为空，这里处理最终显示为nickName的字段
        if finalNickName.isEmpty && !message.nativeAccount.isEmpty {
            finalNickName = message.nativeAccount.unicodeStr
        }else if finalNickName.isEmpty && !message.account.isEmpty {
            finalNickName = message.account.unicodeStr
        }
        
        let account = message.account.unicodeStr
        
        if message.isShowAccountNow {
            userNameLabel.text = account
            userNameLabel.textColor = UIColor.red
        }else {
//            if message.stopTalkType == "1" { //被禁言
//                userNameLabel.textColor = UIColor.init(hexString: "#808080")
//            }else if message.stopTalkType == "0" {
                userNameLabel.textColor = UIColor.black
//            }
            
            userNameLabel.text = finalNickName
        }
        
//        if message.stopTalkType == "1" { //被禁言
//            headIcon.alpha = 0.5
//        }else {
//            headIcon.alpha = 1
//        }
        
        //时间
        //等级名称 该等级颜色根据后台 配置
        //如果是计划员，不显示等级而显示为 “计划员”
        if message.ownerType == .TypeSelf {   
            teacherLabel.isHidden = !(CRDefaults.getPlanUser() == 1)
            if message.userType != "2"{
                levelNameLabel.text = message.levelName
            }else{
                levelNameLabel.isHidden = true
            }
            
            headManagerIcon.isHidden = !(CRDefaults.getUserType() == 2 || CRDefaults.getUserType() == 4)
            
            titleRowButton.snp.remakeConstraints { (make) in
                make.left.equalTo(sendDateLabel.snp.left)
                make.right.equalTo(levelImageV.snp.right)
                make.top.equalTo(userNameLabel.snp.top).offset(-5)
                make.bottom.equalTo(userNameLabel.snp.bottom).offset(5)
            }
            
        }else {
            teacherLabel.isHidden = !(message.isPlanUser == "1") //1为计划员
            if message.userType != "2"{
                levelNameLabel.text = message.levelName
            }else{
                levelNameLabel.isHidden = true
            }
                       
            headManagerIcon.isHidden = !(message.userType == "2" || message.userType == "4")
            
            titleRowButton.snp.remakeConstraints { (make) in
                make.left.equalTo(levelImageV.snp.left)
                make.right.equalTo(sendDateLabel.snp.right)
                make.top.equalTo(userNameLabel.snp.top).offset(-5)
                make.bottom.equalTo(userNameLabel.snp.bottom).offset(5)
            }
        }
        //发送时间
        if message.sentTime.length > 10 {
            sendDateLabel.text = message.sentTime.subString(start: 10)
        }else{
            sendDateLabel.text = message.sentTime
        }
        
       let levelTitleColorDict =  CRDefaults.getLevelTitleColor()
        if !levelTitleColorDict.isEmpty{
            var key = message.levelName
            if key.contains("后台管理"){
                key = "后台管理"
            }
            let dict = levelTitleColorDict[key] as? [String : AnyObject] ?? [:]
            if !dict.isEmpty{
                let titleColor =  String.init(format:"#%@",dict["titleColor"] as? String ?? "")
                if titleColor.length > 3{
                   levelNameLabel.textColor = UIColor.colorWithHexString(titleColor)
                }
            }
        }
        
        let userIconString = message.avatar ?? ""
        
        headIcon.kf.setImage(with: URL(string: userIconString), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
        
        if message.messageType == CRMessageType.PlanMsg {
            if userIconString.isEmpty {
                headIcon.image = UIImage.init(named: "crPlanAvatar")
            }else {
                headIcon.kf.setImage(with: URL(string: userIconString), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            levelNameLabel.text = ""
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.height.equalTo(message.isShowTime ? 20 : 0)
            make.top.equalTo(contentView).offset(message.isShowTime ? 5 : 0)
            make.centerX.equalTo(contentView)
        }
        //用户icon
        headIcon.snp.remakeConstraints { (make) in
            make.width.height.equalTo(40)
            if message.ownerType == .TypeSelf{
                make.right.equalTo(contentView).offset(-8)
            }else{
                make.left.equalTo(contentView).offset(8)
            }
            make.top.equalTo(timeLabel.snp.bottom).offset(20)
        }
        
        levelImageV.snp.remakeConstraints { (make) in
            if message.ownerType == .TypeSelf{
                
                make.right.equalTo(headIcon.snp.right)
            }else{
                make.left.equalTo(headIcon.snp.left)
            }
            make.top.equalTo(timeLabel.snp.bottom)
        }
        
        levelNameLabel.snp.remakeConstraints { (make) in
            if message.ownerType == .TypeSelf{
                make.right.equalTo(levelImageV.snp.left).offset(-2)
                
            }else{
                make.left.equalTo(levelImageV.snp.right).offset(2)
            }
            make.centerY.equalTo(levelImageV.snp.centerY)
        }
        
        teacherLabel.snp.remakeConstraints { (make) in
            if message.ownerType == .TypeSelf{
                make.right.equalTo(levelNameLabel.snp.left).offset(-2)
                if CRDefaults.getPlanUser() != 1 {
                    make.width.equalTo(0)
                }
            }else{
                make.left.equalTo(levelNameLabel.snp.right).offset(2)
                if message.isPlanUser != "1" {
                    make.width.equalTo(0)
                }
            }
            make.centerY.equalTo(levelImageV.snp.centerY)
        }
        
        let chatroomConfig = getChatRoomSystemConfigFromJson()
        if let config = chatroomConfig?.source{
            //房间配置 等级图标
            if  config.switch_level_ico_show == "1"{
                if message.levelIcon.isEmpty {
                    levelImageV.isHidden = true
                    levelImageV.snp.remakeConstraints { (make) in
                        if message.ownerType == .TypeSelf{
                            
                            make.right.equalTo(headIcon.snp.right)
                        }else{
                            make.left.equalTo(headIcon.snp.left)
                        }
                        make.size.equalTo(CGSize(width: 0, height: 40))
                        make.top.equalTo(timeLabel.snp.bottom)
                    }
                    levelNameLabel.snp.remakeConstraints() { (make) in
                        if message.ownerType == .TypeSelf{
                            
                            make.right.equalTo(headIcon.snp.right)
                        }else{
                            make.left.equalTo(headIcon.snp.left)
                        }
                        make.top.equalTo(timeLabel.snp.bottom)
                    }
                }else{
                    levelImageV.isHidden = false
                    levelImageV.kf.setImage(with: URL(string: message.levelIcon))
                    levelImageV.snp.remakeConstraints { (make) in
                        if message.ownerType == .TypeSelf{
                            
                            make.right.equalTo(headIcon.snp.right)
                        }else{
                            make.left.equalTo(headIcon.snp.left)
                        }
//                        make.size.equalTo(CGSize(width: 40, height: 40))
                        make.width.height.equalTo(40)
                        make.top.equalTo(timeLabel.snp.bottom)
                        make.bottom.equalTo(headIcon.snp.top)
                    }
                }
            }else{
                levelImageV.isHidden = true
                levelImageV.snp.remakeConstraints { (make) in
                    if message.ownerType == .TypeSelf{
                        
                        make.right.equalTo(headIcon.snp.right)
                    }else{
                        make.left.equalTo(headIcon.snp.left)
                    }
                    make.size.equalTo(CGSize(width: 0, height: 40))
                    make.top.equalTo(timeLabel.snp.bottom)
                }
            }
        }
        //会员等级开关
        if chatroomConfig?.source?.switch_level_show == "1" {
            levelNameLabel.isHidden = false
            levelNameLabel.snp.remakeConstraints() { (make) in
                if message.ownerType == .TypeSelf{
                    
                    make.right.equalTo(levelImageV.snp.left).offset(-3)
                }else{
                    make.left.equalTo(levelImageV.snp.right).offset(3)
                }
                make.bottom.equalTo(headIcon.snp.top).offset(-3)
            }
            
        }else{
            levelNameLabel.isHidden = true
            levelNameLabel.snp.makeConstraints { (make) in
                make.width.equalTo(0)
            }
        }
        //用户昵称
        userNameLabel.snp.remakeConstraints { (make) in
            if message.ownerType == .TypeSelf{
                
                make.right.equalTo(teacherLabel.snp.left).offset(-5)
            }else{
                make.left.equalTo(teacherLabel.snp.right).offset(5)
            }
            
            make.bottom.equalTo(headIcon.snp.top).offset(-3)
            make.centerY.equalTo(teacherLabel.snp.centerY)
        }
        //聊天背景气泡
        bubbleImageV.snp.remakeConstraints { (make) in
            if message.ownerType == .TypeSelf{
                make.right.equalTo(self.headIcon.snp.left).offset(-5)
       
            }else{
                make.left.equalTo(self.headIcon.snp.right).offset(5)
            }
            make.top.equalTo(self.userNameLabel.snp.bottom).offset(8)
        
        }
        
        activeityIndicator.snp.remakeConstraints { (make) in
            make.right.equalTo(bubbleImageV.snp.left).offset(-10)
            make.centerY.equalTo(bubbleImageV.snp.centerY)
            make.width.height.equalTo(15)
        }
        userNameLabel.snp.makeConstraints { (make) in
            make.height.equalTo(14)
        }
        
        if message.ownerType == .TypeSelf {
            sendDateLabel.snp.remakeConstraints { (make) in
                make.right.equalTo(userNameLabel.snp.left).offset(-2)
                make.centerY.equalTo(userNameLabel.snp.centerY)
            }
            
            winRateLabel.snp.remakeConstraints { (make) in
                make.right.equalTo(userNameLabel.snp.left).offset(-2)
                make.centerY.equalTo(userNameLabel.snp.centerY)
                make.height.equalTo(18)
            }
        }else{
            
            sendDateLabel.snp.remakeConstraints { (make) in
                make.left.equalTo(userNameLabel.snp.right).offset(2)
                make.centerY.equalTo(userNameLabel.snp.centerY)
            }
            
            winRateLabel.snp.remakeConstraints { (make) in
                make.left.equalTo(userNameLabel.snp.right).offset(2)
                make.centerY.equalTo(userNameLabel.snp.centerY)
                make.height.equalTo(18)
            }
        }
        
        updateMsgStatus(message: message)
        self.message = message
    }
    
    //刷新message的发送状态
    func updateMsgStatus(message:CRMessage) {
        if message.ownerType == .TypeSelf {
            if message.sendState == .Fail {
                sendMessageStatusImageV.isHidden = false
                activeityIndicator.isHidden = true
                activeityIndicator.stopAnimating()
            }else if message.sendState == .progress{
                sendMessageStatusImageV.isHidden = true
                activeityIndicator.isHidden = false
                activeityIndicator.startAnimating()
            }else if message.sendState == .Success{
                activeityIndicator.stopAnimating()
                activeityIndicator.isHidden = true
                sendMessageStatusImageV.isHidden = true
            }
            
        }else{
            activeityIndicator.isHidden = true
            activeityIndicator.stopAnimating()
            sendMessageStatusImageV.isHidden = true
        }
    }
    
}


extension CRBaseChatCell{
    //设置个人信息
    @objc func settingProfileInfo(){
        self.message?.currentUserName = self.userNameLabel.text ?? ""
        //点击用户图像
        messageDelegate?.messageCellDidClickUserIcon(message: self.message!,headIcon:headIcon)
    }
}
