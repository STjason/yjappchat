//
//  CRMessageImageView.swift
//  gameplay
//
//  Created by Gallen on 2019/9/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMessageImageView: UIImageView {
    
    var backgroudImage:UIImage?
    
    private weak var maskLayer:CAShapeLayer?
    
    private weak var contentLayer:CALayer?
    
    lazy var progressView : CRProgressView = {
        let progress = CRProgressView.init(frame: CGRect.zero)
        progress.backgroundColor = UIColor.clear
        return progress
    }()
    
    lazy var progressLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let maskLayer = CAShapeLayer()
        maskLayer.contentsCenter = CGRect(x: 0.5, y: 0.6, width: 0.1, height: 0.1)
        maskLayer.contentsScale = UIScreen.main.scale
        
        let contentLayer = CALayer()
        contentLayer.mask = maskLayer
        layer.addSublayer(contentLayer)
        
        self.maskLayer = maskLayer
        self.contentLayer = contentLayer
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews(){
        super.layoutSubviews()
        maskLayer?.frame = CGRect(x: 0, y: 0, width: width, height: height)
        contentLayer?.frame = CGRect(x: 0, y: 0, width: width, height: height)
    }
    
    func setThumbnaiPath(imagePath:String?,imageUrl:String){
        if imagePath == ""{
//            contentLayer?.contents = nil
            let imageV = UIImageView()
            imageV.kf.indicatorType = .activity
            
            imageV.kf.setImage(with: URL.init(string: imageUrl), placeholder: UIImage(named: "image_placeholder"), options: nil, progressBlock: { (curProgress, totalProgress) in
                
                self.progressView.progress = Double(curProgress / totalProgress)
                
                self.progressLabel.text = String(format:"%.2f%%",Double(curProgress / totalProgress) * 100)
                print("当前进度" + "\(curProgress)" + "总进度" + "\(totalProgress)" )
                
            }) { (image, error, cacheType, imgUrl) in
                if image != nil{
                    self.progressView.isHidden = true
                    let imageMessage = CRImageMessage()
                    imageMessage.imageSize = (image?.size) ?? CGSize.zero
                    imageMessage.kMessageFrame = nil
                    imageMessage.sendState = .Success
                    DispatchQueue.main.async {
                        self.contentLayer?.contents = image?.cgImage
                    }
                    
                    self.layoutIfNeeded()
                }
            }
        }else{
            if let image = UIImage(named: imagePath ?? ""){
                 contentLayer?.contents = image.cgImage
            }else{
                print("图片不存在")
            }
        }
    }
    
    func setBackgroundImage(image:UIImage){
        maskLayer?.contents = image.cgImage
    }

}
