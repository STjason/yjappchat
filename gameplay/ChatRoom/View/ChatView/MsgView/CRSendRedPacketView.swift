//
//  CRSendRedPacketView.swift
//  gameplay
//
//  Created by Gallen on 2019/9/27.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
/**textView占位图*/
let textViewPlaceholder = "恭喜发财，大吉大利!"
class CRSendRedPacketView: UIView {
    ///私聊还是群聊
    var isPrivateChat = false
    /**红包金额*/
    var totalMoney:String = ""
    /**红包个数*/
    var redPacketCount:Int = 0
    /**红包描述*/
    var redPacketDescribe:String = ""
    /**红包详情的回调*/
    var redPacketClouse:((_ redPacketItem:CRRedPacketMessage)->(Void))?
    /**由后台设置红包备注*/
    var systemTextViewPlaceholder:String = ""
    
    lazy var backView:UIView = {
        let bgV = UIView()
        bgV.backgroundColor = UIColor.white
        return bgV
    }()
    /**发红包标题*/
    lazy var redPacketTopView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.colorWithHexString("#ff5858")
        return view
    }()
    /**主题*/
    lazy var mainTitleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = UIColor.white
        label.text = "发红包"
        return label
    }()
    lazy var closeButton:UIButton = {
        let button = UIButton()
        button.setTitle("关闭", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(closeInputRedPacket), for: .touchUpInside)
        return button
    }()
    /**总金额*/
    lazy var totalMoneyTextField:UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(inputMoneyClick(textField:)), for: UIControl.Event.editingChanged)
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.textColor = UIColor.black
        textField.tintColor = UIColor.black
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 0.5
        textField.placeholder = "0.00"
        textField.textAlignment = .right
        
        let renminbiLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        renminbiLabel.font = UIFont.systemFont(ofSize: 14)
        renminbiLabel.text = "元"
        renminbiLabel.textAlignment = .center
        textField.rightView = renminbiLabel
        textField.rightViewMode = .always
        
        let totalMoneyTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 30))
        totalMoneyTitleLabel.text = " 总金额"
        totalMoneyTitleLabel.font = UIFont.systemFont(ofSize: 14)
        textField.leftView = totalMoneyTitleLabel
        textField.leftViewMode = .always
        
        textField.layer.cornerRadius = 2
        textField.clipsToBounds = true
        //        textField.layer.shadowOpacity = 0.4
        //        textField.layer.shadowOffset = CGSize(width: 0, height: 0)
        textField.delegate = self
        
        return textField
    }()
    /**红包个数*/
    lazy var redPacketCountTextField:UITextField = {
        let textField = UITextField()
        textField.layer.masksToBounds = true
        textField.textColor = UIColor.black
        textField.tintColor = UIColor.black
        textField.addTarget(self, action: #selector(inputRedPacketCountClick(textField:)), for: UIControl.Event.editingChanged)
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 0.5
        textField.keyboardType = UIKeyboardType.decimalPad
        textField.placeholder = "填写个数"
        textField.textAlignment = .right
        let packetCountLabel = UILabel(frame: CGRect(x: 10, y: 0, width: 30, height: 30))
        packetCountLabel.font = UIFont.systemFont(ofSize: 14)
        packetCountLabel.textAlignment = .center
        packetCountLabel.text = "个"
        textField.rightView = packetCountLabel
        textField.rightViewMode = .always
        
        let redPacketTitleLabel = UILabel(frame: CGRect(x: 10, y: 0, width: 70, height: 30))
        redPacketTitleLabel.text = " 红包个数"
        redPacketTitleLabel.font = UIFont.systemFont(ofSize: 14)
        textField.leftView = redPacketTitleLabel
        textField.leftViewMode = .always
        
        textField.layer.cornerRadius = 2
        textField.clipsToBounds = true
        return textField
    }()
    /**红包描述*/
    lazy var redPacketDescriptionTextView:UITextView = {
        let textView = UITextView()
        textView.delegate = self
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.layer.cornerRadius = 3
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.text = systemTextViewPlaceholder
        textView.textColor = UIColor.gray
        return textView
    }()
    /**字数限制*/
    lazy var imposeWordsCountLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.textColor = UIColor.colorWithHexString("#ababab")
        label.text = "字数限制30"
        return label
    }()
    /**塞进红包*/
    lazy var commitButton:UIButton = {
        let button = UIButton()
        button.setTitle("塞钱进红包", for: .normal)
        button.layer.cornerRadius = 3
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.colorWithHexString("#ff5858")
        button.addTarget(self, action: #selector(sendRedPackge), for: .touchUpInside)
        return button
    }()
    
    convenience init(isPrivateChat:Bool,size:CGSize) {
        self.init()
        
        self.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.isPrivateChat = isPrivateChat
        addSubview(backView)
        backView.addSubview(redPacketTopView)
        redPacketTopView.addSubview(mainTitleLabel)
        redPacketTopView.addSubview(closeButton)
        backView.addSubview(totalMoneyTextField)
        backView.addSubview(redPacketCountTextField)
        backView.addSubview(redPacketDescriptionTextView)
        backView.addSubview(imposeWordsCountLabel)
        backView.addSubview(commitButton)
        
        if isPrivateChat {
            redPacketCountTextField.text = "1"
            redPacketCount = 1
        }
        
        //添加约束
        addLayoutConstrains()
        //获取房间系统配置
        if let configuration = getChatRoomSystemConfigFromJson(){
            if let systemRemarkString = configuration.source?.name_red_bag_remark_info{
                systemTextViewPlaceholder = systemRemarkString
            }else{
                systemTextViewPlaceholder = textViewPlaceholder
            }
        }else{
            //获取失败 就用恭喜发财，大吉大利!
            systemTextViewPlaceholder = textViewPlaceholder
        }
        
        redPacketDescriptionTextView.text = systemTextViewPlaceholder
    }
    
    //MARK:添加约束
    func addLayoutConstrains(){
        
        let  chatRoomConfig = getChatRoomSystemConfigFromJson()
        backView.snp.makeConstraints { (make) in
            //280 325
            make.width.equalTo(280)
            if chatRoomConfig?.source?.switch_red_bag_remark_show == "1"{
                make.height.equalTo(325)
            }else{
                make.height.equalTo(220)
            }
            
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY).offset(-50)
        }
        redPacketTopView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(backView)
            make.height.equalTo(40)
        }
        mainTitleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(redPacketTopView.snp.centerX)
            make.top.bottom.equalTo(redPacketTopView)
        }
        closeButton.snp.makeConstraints { (make) in
            make.right.equalTo(redPacketTopView).offset(-5)
            make.centerY.equalTo(redPacketTopView.snp.centerY)
        }
        totalMoneyTextField.snp.makeConstraints { (make) in
            make.top.equalTo(redPacketTopView.snp.bottom).offset(5)
            make.height.equalTo(40)
            make.left.equalTo(5)
            make.right.equalTo(-5)
        }
        
        redPacketCountTextField.snp.makeConstraints { (make) in
            make.top.equalTo(totalMoneyTextField.snp.bottom).offset(5)
            make.left.equalTo(totalMoneyTextField.snp.left)
            make.height.equalTo(isPrivateChat ? 0 : totalMoneyTextField.snp.height)
            make.right.equalTo(totalMoneyTextField.snp.right)
        }
        
        
        redPacketDescriptionTextView.snp.makeConstraints { (make) in
            make.top.equalTo(redPacketCountTextField.snp.bottom).offset(5)
            make.left.equalTo(totalMoneyTextField.snp.left)
            make.right.equalTo(totalMoneyTextField.snp.right)
            if chatRoomConfig?.source?.switch_red_bag_remark_show == "1"{
                make.height.equalTo(100)
                imposeWordsCountLabel.isHidden = false
            }else{
                imposeWordsCountLabel.isHidden = true
                make.height.equalTo(0)
            }
        }
        imposeWordsCountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(totalMoneyTextField.snp.left)
            make.right.equalTo(totalMoneyTextField.snp.right)
            make.height.equalTo(20)
            make.top.equalTo(redPacketDescriptionTextView.snp.bottom)
        }
        commitButton.snp.makeConstraints { (make) in
            make.left.equalTo(totalMoneyTextField.snp.left)
            make.right.equalTo(totalMoneyTextField.snp.right)
            make.height.equalTo(40)
            make.top.equalTo(imposeWordsCountLabel.snp.bottom).offset(5)
        }
    }
    
}
//MARK：--响应事件
extension CRSendRedPacketView{
    //输入发红包金额
    @objc func inputMoneyClick(textField:UITextField){
        //总金额
        totalMoney = String.init(format:"%@",textField.text ?? "")
    }
    //输入发红包个数
    @objc func inputRedPacketCountClick(textField:UITextField){
        redPacketCount = Int(textField.text ?? "") ?? 0
    }
    //发送红包
    @objc func sendRedPackge(){
        if totalMoney.count == 0 || redPacketCount == 0{
            showToast(view: self, txt: "请输入红包金额或个数")
            return
        }
        if redPacketDescribe.count == 0 {
            redPacketDescribe = systemTextViewPlaceholder
        }
        if let redPacketBlock = redPacketClouse {
            let redPacketItem = CRRedPacketMessage()
            redPacketItem.count = redPacketCount
            redPacketItem.amount = totalMoney
            redPacketItem.remark = redPacketDescribe
            //发红包的消息编号
            redPacketItem.code = "R7009"
            redPacketBlock(redPacketItem)
        }
        closeInputRedPacket()
    }
    //关闭红包输入界面
    @objc func closeInputRedPacket(){
        removeFromSuperview()
    }
}
//MARK:--UITextViewDelegate
extension CRSendRedPacketView : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        redPacketDescribe = textView.text
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count < 1 {
            textView.text = systemTextViewPlaceholder
            textView.textColor = UIColor.gray
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == systemTextViewPlaceholder {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.location>=30){
            return false;
        }else{
            return true;
        }
    }
}
extension CRSendRedPacketView:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location>=10){
            return false;
        }else{
            return true;
        }
    }
}
