//
//  CRLotteryBannerView.swift
//  Chatroom
//
//  Created by admin on 2019/7/13.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit

class CRLotteryBannerView: UIView {
    var horSpacing:CGFloat = 15
    /**刷新数据的定时器*/
    var refreshDataTimer:Timer?
    /**记录上一次开奖的期号*/
    var lastQiHao:String = ""
    //
    var recordIndex = 0
    var lotterData:[CRLotteryData]?{
        didSet{
            if let dataSources = lotterData {
                self.dataSource = dataSources
                self.lotterCollectionView.reloadData()
                
                let chatroomConfig = getChatRoomSystemConfigFromJson()
                for (index,data) in dataSources.enumerated(){
                    let datas = data as CRLotteryData
                    let defaultLotter = chatroomConfig?.source?.switch_lottery_result_default_type_show ?? ""
                    if datas.code == defaultLotter{
                        //默认彩种
                        recordIndex = index
                    }
                }
                DispatchQueue.main.async{
                    let indexPath = IndexPath(row: self.recordIndex, section: 0)
                    self.scrollToStep(index: indexPath)
                }
                
            }
        }
    }
    var dataSource:[CRLotteryData] = []
    
    lazy var lotterCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout.init()
        
        let itemWidth = ScreenWidth - 2 * horSpacing
        layout.itemSize = CGSize.init(width: itemWidth, height: 75)
        layout.sectionInset = UIEdgeInsets(top: 10, left: horSpacing, bottom: 10, right: horSpacing)
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        
        let collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.clear
        
        return collectionView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(lotterCollectionView)
        lotterCollectionView.register(CRLotteryCollectionViewCell.self, forCellWithReuseIdentifier: "CRLotteryCollectionViewCell")
        lotterCollectionView.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(self)
            make.bottom.equalTo(self.snp.bottom).offset(-5)
        }
        startTimer()
        
      
        
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
//            print("Are we there yet?")
//            self.scrollToStep(index: IndexPath(row: self.recordIndex-1, section: 0))
//        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
extension CRLotteryBannerView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataSource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lotterCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CRLotteryCollectionViewCell", for: indexPath) as? CRLotteryCollectionViewCell
        let lotterItem = dataSource[indexPath.row]
        lotterCell?.lottotItem = lotterItem
//        netLastOpenResult(lotterItem: lotterItem!, indexPath: indexPath)
        return lotterCell!
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: self.width, height: self.height)
//    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return horSpacing * 2
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return horSpacing * 2
    }
}
//MARK:获取当前彩种的最新开奖数据
extension CRLotteryBannerView{
    private func netLastOpenResult(lotterItem:CRLotteryData,indexPath:IndexPath,isUpdate:Bool) {
        var p = [String:Any]()
        p["lotCode"] = lotterItem.code
        p["pageSize"] = 1
        CRNetManager.shareInstance().netLastOpenResult(paramters: p) { [weak self](success, msg, models) in
            guard let weakSelf = self else{ return}
            if success{
                if let lotters = models{
                    if lotters.count > 0 {
                        //最新开奖结果
                        let firstResult = lotters[0]
                        let qihaoStr = trimQihao(currentQihao: firstResult.qiHao)
                      
                        print(firstResult.haoMa,firstResult.qiHao)
                        //如果再一次请求的开奖结果期号与上一期的是一样直接return
                        if weakSelf.lastQiHao == firstResult.qiHao{
                            return
                        }
                        lotterItem.haoMa = firstResult.haoMa
                        lotterItem.qiHao = qihaoStr
                        lotterItem.date = firstResult.date
                        
                        weakSelf.lastQiHao = firstResult.qiHao
                        if isUpdate{
                            //重新请求当前页面的开奖结果和期号
                            for (index ,value )in weakSelf.dataSource.enumerated(){
                                if value.code == lotterItem.code{
                                    //替换需要更新的开奖结果模型
                                    weakSelf.dataSource[index] = lotterItem
                                }
                            }
                        }else{
                            //有开奖结果返回
                            weakSelf.dataSource.append(lotterItem)
                        }
                    }else{
                        //没有期号没有开奖结果
                        if !isUpdate{
                             weakSelf.dataSource.append(lotterItem)
                        }
                    }
                }
            }
            weakSelf.lotterCollectionView.reloadData()
        }
    }
    
    //MARK:滚动到系统默认的当前彩种
    func scrollToStep(index: IndexPath){
//        if let _ =  lotterCollectionView.dataSource?.collectionView(lotterCollectionView, cellForItemAt: index){
            lotterCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
//        }
    }
}

extension CRLotteryBannerView{
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if self.dataSource.count != 0 {
            let scrollViewOffsetX = scrollView.contentOffset.x
            
            let index = Int(scrollViewOffsetX / scrollView.width)
            let lotterItem = self.dataSource[index]
            let indexPath = IndexPath.init(row: index, section: 0)
            netLastOpenResult(lotterItem:lotterItem,indexPath:indexPath, isUpdate: true)
        }
     
    }
}
extension CRLotteryBannerView{
    //启动定时器
    func startTimer(){
        refreshDataTimer = Timer(timeInterval: 8, target: self, selector: #selector(updateData), userInfo: nil, repeats: true)
         RunLoop.current.add(refreshDataTimer!, forMode: RunLoop.Mode.common)
    }
    //定时器每隔5秒调用一次方法
    @objc func updateData(){
        if refreshDataTimer == nil{
            return
        }
        scrollViewDidEndDecelerating(scrollView: self.lotterCollectionView)
    }
    
}
