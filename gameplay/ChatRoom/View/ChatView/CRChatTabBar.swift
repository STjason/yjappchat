//
//  CRChatTabBar.swift
//  Chatroom
//
//  Created by admin on 2019/7/12.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit

class CRChatTabBar: UIView {
    var firstItem = CRTabbarButton()
    var secondItem = CRTabbarButton()
    var thirdItem = CRTabbarButton()
//    var fourthItem = CRTabbarButton()
//    var fifthItem = CRTabbarButton()
    
    var tabClickWithIndex:((_ index:Int) -> Void)?
    var lastSelectedIndex = -1 //tabbar选中的item的index，-1表示没有任何item被选中
    
    let BASE_TAG = 10000
    
    var barWidth:CGFloat = 0
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        configTabBar(removeItems: [Int](),firstLoad: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configWith(width:CGFloat) {
        self.barWidth = width
    }
    
    func configTabBar(removeItems:[Int],firstLoad:Bool = false) {

        let tabItems = [firstItem,secondItem,thirdItem]

        let imagesAndTitles = [("tabBet_normal","tabBet_selected","投注"),
                               ("tabBarPlan_normal","tabBarPlan_selected","注单"),
                               ("tabbar_lotteryticketplan_nomarl","tabbar_lotteryticketplan_selected","计划"),]

        
        for (index,item) in tabItems.enumerated() {
            item.isHidden = true
            self.configItem(item: item,configs: imagesAndTitles[index],index:index)
            self.addSubview(item)
        }
        self.layout(items: tabItems,removeItems:removeItems,firstLoad:firstLoad)
    }
    
    private func layout(items:[UIButton],removeItems:[Int] = [Int](),firstLoad:Bool) {
        for (index,item) in items.enumerated() {
            
            if firstLoad {
                item.snp.makeConstraints {[weak self] (make) in
                    make.top.equalTo(0)
                    make.bottom.equalTo(0)
                    
                    if let weakSelf = self {
                        
                        make.width.equalTo(weakSelf.barWidth / CGFloat(items.count))
                        
                        if index != 0 {
                            make.leading.equalTo(items[index - 1].snp.trailing)
                        }else {
                            make.leading.equalTo(0)
                        }
                    }
                }

            }else {
                item.snp.remakeConstraints {[weak self] (make) in
                    make.top.equalTo(0)
                    make.bottom.equalTo(0)
                    
                    if let weakSelf = self {
                        
                        //是否被隐藏
                        var shouldHide = false
                        for hideIndex in removeItems {
                            if index == hideIndex {
                                let item = items[index]
                                item.isHidden = true
                                shouldHide = true
                                break
                            }
                        }
                        
                        if shouldHide {
                            make.width.equalTo(0)
                            item.imageView?.isHidden = true
                        }else {
                            make.width.equalTo(weakSelf.barWidth / CGFloat(items.count - removeItems.count))
                        }
                        
                        
                        if index != 0 {
                            make.leading.equalTo(items[index - 1].snp.trailing)
                        }else {
                            make.leading.equalTo(0)
                        }
                    }
                }
                
                UIView.animate(withDuration: 0) {
                    self.layoutIfNeeded()
                }
                
                item.isHidden = false

            }
        }
    }

    ///配置tabItem
    private func configItem(item:CRTabbarButton,configs:(String,String,String),index:Int) {
        //配置点击是否选中状态，现阶段不需要区别 选中和 普通状态
        let imageNormal = configs.0
        let imageSelected = configs.1
        let title = configs.2
        item.setImage(UIImage.init(named: imageNormal), for: .normal)
        item.setTitleColor(UIColor.colorWithHexString("#333333"), for: .normal)
        item.setImage(UIImage.init(named: imageSelected), for: .highlighted)
        item.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        item.setTitleColor(UIColor.colorWithHexString("#00d5c1"), for: .highlighted)
        item.setTitle(title, for: .normal)
        item.tag = index + BASE_TAG
        item.addTarget(self, action: #selector(clickAction), for: .touchUpInside)
       
    }
    
    @objc func clickAction(sender:UIButton) {
        let index = sender.tag - BASE_TAG
        
//        if index != lastSelectedIndex || index == 0 {
//           // 配置点击是否选中状态，现阶段不需要区别 选中和 普通状态
//            self.changeSelectedStatus(currentItem: sender)
            self.lastSelectedIndex = index
   
            self.tabClickWithIndex?(index)
        
//        }
    }
    
    private func changeSelectedStatus(currentItem:UIButton) {
        if let previousButton = self.viewWithTag(self.lastSelectedIndex + BASE_TAG) as? CRVerticalButton {
            previousButton.isSelected = false
        }
        
        currentItem.isSelected = true
    }
    
    
    
}
