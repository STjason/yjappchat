//
//  CREmojiDisplayView.swift
//  gameplay
//
//  Created by Gallen on 2019/9/23.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CREmojiDisplayView: UIImageView {

    let sizeTips:CGSize = CGSize(width: 55, height: 100)
    
    var emojiItem :CRExpressionItem?{
        didSet{
            if emojiItem?.type == CREmojiType.Emoji{
                self.imageLabel.isHidden = false
                self.imageView.isHidden = true
                self.titleLabel.isHidden = true
                self.imageLabel.text = emojiItem?.name
            }else if (emojiItem?.type == CREmojiType.Face){
                self.imageLabel.isHidden = true
                self.imageView.isHidden = false
                self.titleLabel.isHidden = false
                guard let emojiName = emojiItem?.name else{return}
                self.imageView.image = UIImage(named:emojiName)
                self.titleLabel.text = emojiName.subString(start: 1, length: emojiName.length - 2)
            }
        }
    }
    
    var rect:CGRect?{
        didSet{
            self.centerX = (rect?.origin.x ?? 0) + (rect?.size.width ?? 0) * 0.5
            
            let a = (rect?.origin.y ?? 0)
            let b = (rect?.size.height ?? 0)
            let c = self.height + 5
            self.centerY = a + b - c
        }
    }
    
    lazy var imageView :UIImageView = {
        let imageV = UIImageView()
        return imageV
    }()
   lazy var imageLabel:UILabel = {
        let imageL = UILabel()
        imageL.font = UIFont.systemFont(ofSize: 30)
        imageL.isHidden = true
        imageL.textAlignment = NSTextAlignment.center
        imageL.textColor = UIColor.gray
        return imageL
    }()
    lazy var titleLabel:UILabel = {
        let titleL = UILabel()
        titleL.textColor = UIColor.gray
        titleL.font = UIFont.systemFont(ofSize: 12)
        return titleL
    }()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: sizeTips.width, height: sizeTips.height))
        self.image = UIImage(named: "emojiKB_tips")
        self.addSubview(imageLabel)
        self.addSubview(imageView)
        self.addSubview(titleLabel)
        /**添加约束*/
        addLayoutConstraint()
        
    }
    /**添加约束*/
    func addLayoutConstraint(){
        self.imageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(10)
            make.left.equalTo(self.snp.left).offset(12)
            make.right.equalTo(self.snp.right).offset(-12)
            make.height.equalTo(self.imageView.snp.width)
        }
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageView.snp.bottom).offset(5)
            make.centerX.equalTo(self.snp.centerX)
            make.width.lessThanOrEqualTo(self.snp.width)
        }
        self.imageLabel.snp.makeConstraints { (make) in
            make.width.height.centerX.equalTo(self.imageView)
            make.top.equalTo(self.snp.top).offset(12)
        }
    }
    func displayEmojiAtRect(emojiItem:CRExpressionItem,rect:CGRect){
        self.rect = rect
        self.emojiItem = emojiItem
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
