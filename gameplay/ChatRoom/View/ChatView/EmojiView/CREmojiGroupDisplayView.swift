//
//  CREmojiGroupDisplayView.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CREmojiGroupDisplayView: UIView {

    var displayData:[Any] = []
    var lastX :CGFloat = 0;
    weak var emojiDisplayDelegate:CREmojiGroupDisplayViewDelegate?
    
    lazy var emojiCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection  = UICollectionView.ScrollDirection.horizontal
        let collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.scrollsToTop = false
        return collectionView
    }()
    var curPageIndex:Int = 0
    
    var data:[Any]?{
        didSet{
            self.height = 215 - 57
            self.width = kScreenWidth
            var displayDatas:[Any] = []
            for (emojiGroupIndex,_) in (data?.enumerated())! {
                let groupItem = data?[emojiGroupIndex] as? CRExpressionGroupModel
                //已有的表情个数
                let count = groupItem?.picCount ?? 0
                if count > 0{
                    var cellWidth:CGFloat = 0
                    var cellHeight:CGFloat = 0
                    var spaceX :CGFloat = 0
                    var spaceYTop:CGFloat = 0
                    var spaceYBotton:CGFloat = 0
                    cellWidth = CGFloat(Int(self.width - 20) / (groupItem?.colNumber)!)
                    spaceX = (self.width - CGFloat(Double(cellWidth) * Double((groupItem?.colNumber)!))) / 2
                    
                    if (groupItem?.emojiType == CREmojiType.Emoji) || (groupItem?.emojiType == CREmojiType.Face) {
                        cellHeight = (self.height - 15) / CGFloat((groupItem?.rowNumber)!)
                        spaceYTop = 10
                    }else if (groupItem?.emojiType == CREmojiType.ImageWithTitle){
                        cellHeight = (self.height - 10) / CGFloat((groupItem?.rowNumber)!);
                        spaceYTop = 10;
                    }else{
                        cellHeight = (self.height - 40) / CGFloat((groupItem?.rowNumber)!);
                        spaceYTop = 20;
                    }
                    spaceYBotton = (self.height - cellHeight * CGFloat((groupItem?.rowNumber)!)) - spaceYTop;
                    
                    for index in 0..<(groupItem?.pageNumber)!{
                        let model = CREmojiGroupDisplayModel.init(emojiGroup: groupItem!, pageNumber:index, count: groupItem!.pageItemCount)
                        if model.emojiType == CREmojiType.Emoji || groupItem?.emojiType == CREmojiType.Face{
                            let emojiItem = CRExpressionItem()
                            emojiItem.eid = "-1"
                            emojiItem.name = "del"
                            model.addEmoji(emojiItem: emojiItem)
                            model.pageItemCount = model.pageItemCount + 1
                        }
                        model.emojiGroupIndex = emojiGroupIndex
                        model.pageIndex = index
                        model.cellSize = CGSize(width: cellWidth, height: cellHeight)
                        model.selectionInset = UIEdgeInsets.init(top: spaceYTop, left: spaceX, bottom: spaceYBotton, right: spaceX)
                        displayDatas.append(model)
                    }
                }
            }
            self.displayData = displayDatas
            self.emojiCollectionView.reloadData()
            let groupDisplayItem = self.displayData[0] as? CREmojiGroupDisplayModel
            self.emojiCollectionView.setContentOffset(CGPoint.zero, animated: true)
            self.emojiDisplayDelegate?.emojiGroupDisplayViewDidScrollToPageIndexForGroupIndex(displayView: self, pageIndex: 0, groudIndex: groupDisplayItem?.emojiGroupIndex ?? 0)
    }
}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(emojiCollectionView)
        emojiCollectionView.snp.makeConstraints { (make) in
            make.top.right.bottom.left.equalTo(0)
        }
        emojiCollectionView.register(CREmojiFaceItemCell.self, forCellWithReuseIdentifier:"CREmojiFaceItemCell")
        emojiCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "UICollectionViewCell")
   
    }
    func scrollToEmojiGroupAtIndex(index:Int){
        if (index > (self.data?.count)!) {
            return;
        }
        curPageIndex = index;
        var page:Int = 0;
        for i in 0..<index {
            let group:CRExpressionGroupModel = self.data?[i] as! CRExpressionGroupModel
            page += group.pageNumber;
        }

        self.emojiCollectionView.setContentOffset(CGPoint(x: (CGFloat(page)) * self.emojiCollectionView.width, y: (0)), animated: true)
        if (self.displayData.count > page) {
            guard let groupItem:CREmojiGroupDisplayModel = self.displayData[page] as? CREmojiGroupDisplayModel else{
                return
            }
            self.emojiDisplayDelegate?.emojiGroupDisplayViewDidScrollToPageIndexForGroupIndex(displayView: self, pageIndex: 0, groudIndex: groupItem.emojiGroupIndex)
        }
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
//MARK:collectionViewDelegate
extension CREmojiGroupDisplayView:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.displayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       let groupItem = self.displayData[section] as! CREmojiGroupDisplayModel
        return groupItem.pageItemCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let groupItem = self.displayData[indexPath.section] as! CREmojiGroupDisplayModel
        let index = transformModelByRowCount(rowCount: groupItem.rowNumeber, colCount: groupItem.colNumber, atIndex: indexPath.row)
        guard let emojiItem = groupItem.insetAtIndex(index: index) as? CRExpressionItem else{
            let emptyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UICollectionViewCell", for: indexPath) as UICollectionViewCell
            return emptyCell
        }
        if groupItem.emojiType == .Emoji || groupItem.emojiType == .Face {
            let faceEmojiCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CREmojiFaceItemCell", for: indexPath) as? CREmojiFaceItemCell
            faceEmojiCell?.emojiItem = emojiItem
           
            return faceEmojiCell!
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let groupItem = self.displayData[indexPath.section] as? CREmojiGroupDisplayModel
        return groupItem?.cellSize ?? CGSize(width: 32, height: 32)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let groupItem = self.displayData[section] as? CREmojiGroupDisplayModel
        return groupItem?.selectionInset ?? UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let groupItem = self.displayData[indexPath.section] as? CREmojiGroupDisplayModel
        let index = transformModelByRowCount(rowCount: (groupItem?.rowNumeber)!, colCount: (groupItem?.colNumber)!, atIndex: indexPath.row)
        guard let emojiItem = groupItem?.insetAtIndex(index: index) as? CRExpressionItem else{return}
        
        if emojiItem.eid == "-1" {
            //删除
            self.emojiDisplayDelegate?.emojiGroupDisplayViewDeleteButtonPressed(displayView: self)
        }else{
            emojiItem.type = (groupItem?.emojiType)!;
            self.emojiDisplayDelegate?.emojiGroupDisplayViewDidClickedEmoji(displayView: self, emojiItem: emojiItem)
        }
    }
}

extension CREmojiGroupDisplayView{
    
    func transformModelByRowCount(rowCount:Int,colCount:Int,atIndex:Int) -> Int{
        //row
        let x = atIndex / rowCount
        let y = atIndex % rowCount
        return colCount * y + x
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let page = Int((scrollView.contentOffset.x)/scrollView.width)
        guard let emojiDisplayItem = self.displayData[page] as? CREmojiGroupDisplayModel else{return}
        self.emojiDisplayDelegate?.emojiGroupDisplayViewDidScrollToPageIndexForGroupIndex(displayView: self, pageIndex:(emojiDisplayItem.pageIndex) , groudIndex: emojiDisplayItem.emojiGroupIndex)
        
    }
}


