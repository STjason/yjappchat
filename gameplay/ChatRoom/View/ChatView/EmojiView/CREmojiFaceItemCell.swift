//
//  CREmojiFaceItemCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/24.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CREmojiFaceItemCell: CREmojiBaseCell {
    
    lazy var imageV:UIImageView = {
        let imgV = UIImageView()
        return imgV
    }()
    
    lazy var label:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(imageV)
        contentView.addSubview(label)
        
        imageV.snp.makeConstraints { (make) in
            make.center.equalTo(contentView.center)
            make.size.equalTo(CGSize(width: 32, height: 32))
        }
       
        label.snp.makeConstraints { (make) in
            make.edges.equalTo(imageV)
        }
    }
    
    override var emojiItem: CRExpressionItem?{
        didSet{
            //删除按钮
            if emojiItem?.eid == "-1" {
                imageV.isHidden = false
                label.isHidden = true
                imageV.image = UIImage(named: "emojiKB_emoji_delete")
            }else{

                label.isHidden = false
                imageV.isHidden = true
                guard let emojiString = emojiItem?.name else{
                    return
                }
              
                //转十六进制
                let string16 = decTohex(number: Int(emojiString) ?? 0)
                label.text = uticode(code: string16)
                
            }
        }
    }
    
    func uticode(code:String) -> String{
        //实例化字符扫描
        let scanner = Scanner(string: code)
        //从str中扫描出十六进制的数值
        var result:UInt32 = 0
        scanner.scanHexInt32(&result)
        //使用Uint32的数值，生成一个UTF8的字符
        let c = Character(UnicodeScalar(result)!)
        let emoji = String(c)
        return emoji
    }
    
    // MARK: - 十进制转十六进制
     func decTohex(number:Int) -> String {
        return String(format: "%0X", number)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
