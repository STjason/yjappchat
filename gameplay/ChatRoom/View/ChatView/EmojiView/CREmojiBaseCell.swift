//
//  CREmojiBaseCell.swift
//  gameplay
//
//  Created by Gallen on 2019/9/23.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
class CREmojiBaseCell: UICollectionViewCell  {
    var emojiItem : CRExpressionItem?
    var bgView : UIImageView = {
        let backView = UIImageView()
        backView.layer.masksToBounds = true
        backView.layer.cornerRadius = 5
        return backView
    }()
    /**选中的背景图片 默认为空*/
    var highlightImage:UIImage?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(bgView)
        bgView.snp.makeConstraints { (make) in
            make.edges.equalTo(contentView)
        }
        
    }
    
    var showHighlightImage:Bool?{
        didSet{
            if showHighlightImage == true {
                
                bgView.image = self.highlightImage
            }else{
                bgView.image = UIImage(named: "")
            }
        }
    }
    
    func displayBaseRect() -> CGRect{
        return self.frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
