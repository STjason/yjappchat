//
//  CRChatUserProtocol.swift
//  gameplay
//
//  Created by Gallen on 2019/9/12.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

enum CRChatUserType:Int {
    case User = 0
    case Group = 1
}



protocol CRChatUserProtocol : NSObjectProtocol {
  
}
