//
//  CRChatMessageDisplayViewDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/22.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol CRChatMessageViewDelegate : NSObjectProtocol {
    //点击消息
    func chatMessageDisplayViewDidClick(message:CRMessage)
    /**用户图像点击,type:点击头像 选择事件的类型*/
    func chatUserIconDidClick(message:CRMessage,type:ChatTapAvtarType)
    /**点击tableView收键盘*/
    func chatMessageViewTableViewClick()
    /**点击语音消息动画*/
    func chatVoiceViewClick(voiceImageV:CRVoiceImageView)
}

