//
//  CRMessageCellDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol CRMessageCellDelegate:NSObjectProtocol {
    
    func messageCellTap(message:CRMessage)
    
    func messageCellLongPress(message:CRMessage)
    /**点击用户icon*/
    func messageCellDidClickUserIcon(message:CRMessage,headIcon:UIView)
    //刷新图片的UI
    func reloadImageCell()
}
