//
//  CRIputViewDelegate.swift
//  gameplay
//
//  Created by Gallen on 2019/9/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol CRIputViewDelegate:NSObjectProtocol {
    /**
     *  chatBar状态改变
     */
    func chatBar(chatBar:CRInputAreaView,fromStatus:CRChatBarStatus,toStatus:CRChatBarStatus)
    /**
     *  输入框高度改变
     */
    func chatBarDidChangeTextView(chatBar:CRInputAreaView,height:CGFloat)
    /**
     *  发送文字
     */
    func chatBar(text:String)
     // 录音控制
    func chatBarStartRecording(chatBar:CRInputAreaView)
    
    func chatBarWillCancelRecording(chatBar:CRInputAreaView,cancel:Bool)
    
    func chatBarDidCancelRecording(chatBar:CRInputAreaView)
    
    func chatBarFinishedRecoding(chatBar:CRInputAreaView)
    
}
