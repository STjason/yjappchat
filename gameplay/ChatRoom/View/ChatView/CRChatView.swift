//
//  CRChatView.swift
//  Chatroom
//
//  Created by admin on 2019/7/17.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit
import PKHUD

class CRChatView: UIView ,refreshMessageDelegate{
    var lastWelcomeTop:CGFloat = 0 //之前一条欢迎进房消息的 Y
    var hideGrayBackgroundViewHandler:(() -> Void)?
   
    func refreshMessageLocation() {
//        if messageArea.msgItems.count > 0{
//            let indexLocation = IndexPath.init(row: messageArea.msgItems.count - 1, section: 0)
//            messageArea.msgTable.scrollToRow(at: indexLocation, at: .top, animated: true)
//        }
        
        messageArea.refreshMessageLocation()
    }
    
    var confirmCloseWinnerInfoHandler:((_ winnerView:CRWinnersInfoView) -> Void)?
    var tabClickHandler:((_ index:Int) -> Void)?
    let tabBarH:CGFloat = IS_IPHONE_X ? (49+34) : 49
    
    var chatDelegate : CRIputViewDelegate?
    //是否激活状态
    var activity : Bool = false
    var curText :String = ""
    var hasCloseWinnerInfo = false //是否手动关闭了开奖结果bar
    var remainCount = 0 //还有几次开奖结果动画需要执行
    var userType:Int = 0{
        didSet{
          roomsBottomFucView.userType = userType
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LAZY INIT
    //所有视图父视图
    lazy var contentView:UIView = {
        let size = self.frame.size
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: size.width, height: size.height))
        return view
    }()
    
    //半透明弹窗
    lazy var floatView:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.frame = CGRect.init(x: 0, y: 0, width: CRConstants.screenWidth, height: CRConstants.screenHeight)
        view.addTarget(self, action: #selector(tapFloatView), for: .touchUpInside)
        return view
    }()
    
    ///投注页面
    lazy var betArea:CRBetArea = {
        let view = CRBetArea.init(models: [LotteryData]())
//        view.backgroundColor = UIColor.red.withAlphaComponent(0.35)
        return view
    }()
    
    ///密码输入框
    lazy var roomKeyView:CRRoomKeyView = {
        let view = Bundle.main.loadNibNamed("CRRoomKeyView", owner: self, options: nil)?.last as! CRRoomKeyView
        view.closeHandler = {[weak self] in
            if let weakSelf = self {
                weakSelf.removeRoomKeyView()
            }
        }
        return view
    }()

    lazy var tabbar:CRChatTabBar = {
        let view = CRChatTabBar.init(frame: .zero)
        view.backgroundColor = UIColor.white
        
        view.tabClickWithIndex = {[weak self] (index) in
            if let weakSelf = self {
                weakSelf.tabClickWith(index:index)
            }
        }
        return view
    }()
    
    lazy var inputAreaView:CRInputAreaView = {
        let view = CRInputAreaView.init(frame: .zero)
        view.backgroundColor = UIColor.groupTableViewBackground
        //更多功能按钮的回调
        view.moreFunctionHandler = {[weak self] (showMoreFunction) in
            if let weakSelf = self {
                weakSelf.showOrHideMoreFuncView(show:showMoreFunction)
            }
        }
        view.refreshDelegate = self
        view.setViewInteractionEnabled(enabled: false)
        
        return view
    }()
    
    lazy var messageArea:CRChatMessageView = {
        let view = CRChatMessageView.init(frame: .zero)
        return view
    }()
    /**开奖结果*/
    lazy var lotteryBanner:CRLotteryBannerView = {
        let view = CRLotteryBannerView.init(frame: .zero)
        view.backgroundColor = UIColor.clear
        return view
    }()
    /**公告*/
    lazy var announceView:CRAnnounceView = {
        let view = CRAnnounceView.init(frame: .zero)
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var roomsTopView:CRRoomsTopEntryView = {
        let view = CRRoomsTopEntryView()
        view.backgroundColor = UIColor.init(hexString: "#F5F5F5")
        //房间列表视图显示 隐藏的frame
        let hiddenFrame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: 0)
        view.configWith(hiddenFrame: hiddenFrame)
        return view
    }()
    
    lazy var roomsBottomFucView:CRRoomsBottomFucView = {
        let view = CRRoomsBottomFucView()
        return view
    }()

    //MARK: - UI
    private func setupUI() {
        self.addSubview(self.contentView)
        contentView.addSubview(self.announceView)
        contentView.insertSubview(self.lotteryBanner, at: 0)
        contentView.insertSubview(self.messageArea, belowSubview: lotteryBanner)
        contentView.addSubview(self.inputAreaView)
        contentView.addSubview(self.roomsBottomFucView)
        contentView.addSubview(self.tabbar)
        addSubview(self.roomsTopView)
        addSubview(self.betArea)
        self.layout()
    }
    
    private func layout() {
        let inputAreaH:CGFloat = 50
        let announceAreaH:CGFloat = 30
        let lotteryBannerH:CGFloat = 80
        
        self.announceView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.leading.equalTo(0)
            make.height.equalTo(announceAreaH)
            make.trailing.equalTo(0)
        }
        
        self.lotteryBanner.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.announceView.snp.bottom)
                make.leading.equalTo(0)
                make.height.equalTo(lotteryBannerH + 10)
                make.trailing.equalTo(0)
            }
        }
        
        self.messageArea.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.lotteryBanner.snp.bottom)
                make.leading.equalTo(0)
                make.trailing.equalTo(0)
                make.bottom.equalTo(weakSelf.inputAreaView.snp.top).offset(0)
            }
        }
        
        self.inputAreaView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.messageArea.snp.bottom)
                make.height.greaterThanOrEqualTo(inputAreaH)
                make.bottom.equalTo(weakSelf.roomsBottomFucView.snp.top)
                make.right.left.equalTo(0)
            }
        }
       
        
//        let betAreaH = self.betArea.getHeight()
        self.betArea.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.left.right.equalTo(0)
                make.bottom.equalTo(weakSelf.tabBarH)
                make.height.equalTo(0)
            }
        }
        
        self.tabbar.snp.makeConstraints { (make) in
            make.leading.bottom.trailing.equalTo(0)
            make.height.equalTo(KTabBarHeight)
        }
        
        self.tabbar.configWith(width: self.frame.size.width)
        
        self.roomsBottomFucView.configWith(width: CRConstants.screenWidth)
        
        self.roomsBottomFucView.snp.makeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.inputAreaView.snp.bottom).offset(0)
                make.left.equalTo(0)
                make.bottom.equalTo(weakSelf.tabbar.snp.top).offset(0)
                make.height.equalTo(0)
                make.right.equalTo(0)
            }
        }
    }
    
    //MARK: - EVENTS
    //点击了tabbarItem
    private func tabClickWith(index:Int) {
        if index == 0 {
            self.showOrHideBetArea()
        }
        self.tabClickHandler?(index)
    }
    
    //点击半透明背景视图
    @objc func tapFloatView() {
        self.removeRoomKeyView()
        if self.betArea.getIsShow() {
            self.showOrHideBetArea()
        }else {
            self.showGrayBackgroundView(show: false)
        }
    }
    
    //MARK: - 内部功能方法
    ///更多功能视图的显示和隐藏
    func showOrHideMoreFuncView(show:Bool) {
        let time = self.roomsBottomFucView.animateTime
        let height = self.roomsBottomFucView.collectionH
        
        self.inputAreaView.moreFunctionButton.isSelected = show
        self.roomsBottomFucView.configShow(show:show)
        
        self.roomsBottomFucView.snp.remakeConstraints {[weak self] (make) in
            if let weakSelf = self {
                make.top.equalTo(weakSelf.inputAreaView.snp.bottom).offset(0)
                make.left.equalTo(0)
                make.bottom.equalTo(weakSelf.tabbar.snp.top).offset(0)
//                make.height.equalTo(weakSelf.roomsBottomFucView.isShow ? height : 0)
                 make.height.equalTo(show ? height : 0)
                make.right.equalTo(0)
            }
        }
        
        UIView.animate(withDuration: time) {
            self.contentView.layoutIfNeeded()
        }
    }
    
    //MARK: 投注视图的显示和隐藏
    func showOrHideBetArea() {
        self.betArea.configShow()
        let show = self.betArea.getIsShow()
        let height = self.betArea.getHeight()
        
        self.betArea.snp.remakeConstraints { (make) in
            if show {
                self.insertSubview(self.floatView, belowSubview: self.betArea)
                self.floatView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                make.height.equalTo(height)
            }else {
                self.floatView.removeFromSuperview()
                make.height.equalTo(0)
            }
            make.left.bottom.right.equalTo(0)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
     //隐藏投注界面
    func hideBetArea(){
        self.betArea.snp.remakeConstraints { (make) in
            self.floatView.removeFromSuperview()
            make.height.equalTo(0)
            make.left.bottom.right.equalTo(0)
        }
        self.betArea.isShow = false
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    //MARK: 显示灰置背景
    func showGrayBackgroundView(show:Bool) {
        if show {
            self.addSubview(self.floatView)
            self.floatView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }else {
            self.hideGrayBackgroundViewHandler?()
            self.floatView.removeFromSuperview()
        }
    }
    
    //MARK: - 外部调用API
    //进入房间，密码输入view
    func addRoomKeyView(password:String) {
        if self.roomKeyView.isShow {
            self.removeRoomKeyView()
        }else {
            self.roomKeyView.isShow = true
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                self.roomKeyView.inputSqure?.becomeFirstResponder()
                self.roomKeyView.inputSqure?.textField.becomeFirstResponder()
            }
        }
        
        let width:CGFloat = 280
        self.roomKeyView.configWith(contents: password,width:width)
        self.floatView.addSubview(self.roomKeyView)
        
        self.roomKeyView.snp.makeConstraints { (make) in
            make.center.equalTo(self.contentView.center)
            make.width.equalTo(width)
            make.height.equalTo(120)
        }
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(self.floatView)
            window.bringSubviewToFront( self.floatView)
        }
    }
    /// 移除密码输入框
    func removeRoomKeyView() {
        self.roomKeyView.isShow = false
        self.roomKeyView.removeFromSuperview()
        self.floatView.removeFromSuperview()
    }
    
    func getNewWelcomeTopY() -> CGFloat {
        if lastWelcomeTop == 80 {
            lastWelcomeTop = 0
            return lastWelcomeTop
        }else {
            lastWelcomeTop = lastWelcomeTop + 40
        }
        
        return lastWelcomeTop
    }
    
    ///欢迎进入房间公告
    func welcomeNewClient(message: CRWelcomeMsgItem,removeMsgHandler:@escaping ((_ msg:CRWelcomeMsgItem) -> ())) {
        let name = message.nickName
        
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        let welcomeNoticeLabel = label
        
        welcomeNoticeLabel.text = "欢迎\(name)进入房间"
        self.addSubview(welcomeNoticeLabel)
        self.bringSubviewToFront( welcomeNoticeLabel)
        
        let size = welcomeNoticeLabel.sizeThatFits(CGSize.init(width: 200, height: CGFloat(MAXFLOAT)))
        
        setupAttributed(label: welcomeNoticeLabel, text: "欢迎\(name)进入房间")
        
        let winnerInfoHeight:CGFloat = 80 //中奖公告高度
        let topY = getNewWelcomeTopY() + winnerInfoHeight
        
        let height:CGFloat = 40
        let width:CGFloat = size.width + 20
        welcomeNoticeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.right)
            make.height.equalTo(height)
            make.width.equalTo(width)
            make.top.equalTo(lotteryBanner.snp.bottom).offset(topY)
        }
        
        self.layoutSubviews()
        
        welcomeNoticeLabel.snp.remakeConstraints { (make) in
            make.right.equalTo(self.snp.left)
            make.height.equalTo(height)
            make.width.equalTo(width)
            make.top.equalTo(lotteryBanner.snp.bottom).offset(topY)
        }
        
        UIView.animate(withDuration: 3) {
            self.layoutIfNeeded()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            welcomeNoticeLabel.removeFromSuperview()
            removeMsgHandler(message)
        })
    }
    
    //开奖结果 bar
    func winnersInfoViewConfig(model:CRWinnersInfo) {
        let userName = model.userName
        let prizeProject = model.prizeProject
        let prizeMoney = "\(model.prizeMoney)"
        let contents = "恭喜\(userName)在\(prizeProject)玩法中奖\(prizeMoney)元"
        
        let winnerInfoView = Bundle.main.loadNibNamed("CRWinnersInfoView", owner: self, options: nil)?.last as! CRWinnersInfoView
        winnerInfoView.backgroundColor = UIColor.clear
        
        winnerInfoView.infoLabel.text = contents
        contentView.addSubview(winnerInfoView)
        contentView.bringSubviewToFront( winnerInfoView)
        
        winnerInfoView.closeHandler = {[weak self] in
            if let weakSelf = self {
                weakSelf.confirmCloseWinnerInfoHandler?(winnerInfoView)
            }
        }
        
        //富文本设置
        setupAttributedWinner(label: winnerInfoView.infoLabel, text: contents,userName: userName,prizeProject: prizeProject,prizeMoney: prizeMoney)
        
        let height:CGFloat = 80
        let width:CGFloat = ScreenWidth - 20
        
        winnerInfoView.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.left.equalTo(weakSelf.contentView.snp.right)
            make.height.equalTo(height)
            make.width.equalTo(width)
            make.top.equalTo(weakSelf.lotteryBanner.snp.bottom)
        }
        
        let inOutAnimateTime:TimeInterval = 0.8 //进出场动画时间
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            winnerInfoView.snp.remakeConstraints {[weak self] (make) in
                guard let weakSelf = self else {return}
                make.height.equalTo(height)
                make.width.equalTo(width)
                make.centerX.equalTo(weakSelf.snp.centerX)
                make.top.equalTo(weakSelf.lotteryBanner.snp.bottom)
            }
            
            UIView.animate(withDuration: inOutAnimateTime) { //进场动画设置
                self.layoutIfNeeded()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + inOutAnimateTime, execute: { //进场完成后 烟花效果展示
                CRCustomSpark().createSpark(view: self)
            })
            
            var marginsTime:TimeInterval = 2 //中奖榜单停留时间
            if let config = getChatRoomSystemConfigFromJson()?.source {
                //中奖榜单
                if let time = Double(config.name_win_lottery_animation_interval) {
                    marginsTime = TimeInterval(time)
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + marginsTime) {
                
                //准备离场时，展示烟花效果
                CRCustomSpark().createSpark(view: self)
                
                winnerInfoView.snp.remakeConstraints {[weak self] (make) in
                    guard let weakSelf = self else {return}
                    make.right.equalTo(weakSelf.contentView.snp.left)
                    make.height.equalTo(height)
                    make.width.equalTo(width)
                    make.top.equalTo(weakSelf.lotteryBanner.snp.bottom)
                }
                
                UIView.animate(withDuration: inOutAnimateTime) {
                    self.layoutIfNeeded()
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + inOutAnimateTime) {
                    winnerInfoView.removeFromSuperview()
                }
            }
        }
    }
    
    //MARK: 开奖结果 bar
    private func setupAttributedWinner(label:UILabel,text:String,userName:String,prizeProject:String,prizeMoney:String) {
//        let contents = "恭喜\(userName)在\(prizeProject)玩法中奖\(prizeMoney)元"
        let attributeString = NSMutableAttributedString(string: text)
        //恭喜
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange.init(location: 0, length: 2))
        //userName
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.green, range: NSRange.init(location: 2, length: userName.length))
        //在prizeProject玩法中奖
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange.init(location: (2 + userName.length), length: (prizeProject.length + 5) ))
        //prizeMoney元
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow, range: NSRange.init(location: (2 + userName.length + prizeProject.length + 5), length: (prizeMoney.length + 1) ))
        
        label.attributedText = attributeString
    }
    
    //MARK: 进入房间欢迎词
    private func setupAttributed(label:UILabel,text:String) {
        let attributeString = NSMutableAttributedString(string: text)
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange.init(location: 0, length: 2))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#854AEA"), range: NSRange.init(location: 2, length: text.length - 4))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange.init(location: text.length - 4, length: 4))
        label.attributedText = attributeString
    }
    
    deinit {
        print("CRChatView被释放了")
    }
    
}






