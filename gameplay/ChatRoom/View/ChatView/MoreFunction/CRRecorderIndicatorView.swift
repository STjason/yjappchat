//
//  CRRecorderIndicatorView.swift
//  gameplay
//
//  Created by Gallen on 2019/10/6.
//  Copyright © 2019年 yibo. All rights reserved.
// 发送音频指示视图

import UIKit
//发送音频消息的状态
enum CRRecorderStatus:Int {
    case recording
    case willCancel
    case tooShort
}

private let str_recording = "手指上滑，取消发送"
private let str_cancel = "手指松开，取消发送"
private let str_rec_short = "说话时间太短"
class CRRecorderIndicatorView: UIView {
    var status:CRRecorderStatus?{
        didSet{
            if status == .willCancel{
                centerImageV.isHidden = false
                centerImageV.image = UIImage(named: "chat_record_cancel")
                titleBackgroudView.isHidden = false
                recImageView.isHidden = true
                volumeImageView.isHidden = true
                titleLabel.text = str_cancel
            }else if status == .recording{
                volumeImageView.isHidden = false
                centerImageV.isHidden = true
                titleBackgroudView.isHidden = true
                recImageView.isHidden = false
                self.titleLabel.text = str_recording
            }else if status == .tooShort{
                centerImageV.isHidden = false
                centerImageV.image = UIImage(named: "chat_record_cancel")
                titleBackgroudView.isHidden = true
                recImageView.isHidden = true
                volumeImageView.isHidden = true
                titleLabel.text = str_rec_short
                DispatchQueue.main.after(1) {
                    DispatchQueue.main.async {
                        self.removeFromSuperview()
                    }
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                // your code here
                if self.status == .tooShort{
                    self.removeFromSuperview()
                }
            }
        }
    }
    /**音频大小 取值0-1*/
    var volume:Float?{
        didSet{
            var picId = Int(100 * (volume ?? 0 < Float(0) ? 0:(volume ?? 0 > Float(1.0) ? 1.0 :volume)!))
            picId = picId > 8 ? 8 : picId
            let imgName = "chat_record_signal_\(picId)"
            print(imgName)
            volumeImageView.image = UIImage(named: String(format:imgName))
        }
    }
    
    lazy var backgroudView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.6
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var titleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = str_recording
        return label
    }()
    
    lazy var titleBackgroudView:UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = UIColor.red
        view.layer.cornerRadius = 2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var recImageView:UIImageView = {
        let imageV = UIImageView.init(image: UIImage(named: "chat_record_recording"))
        return imageV
    }()
    lazy var centerImageV:UIImageView = {
        let imageV = UIImageView()
        imageV.isHidden = true
        return imageV
    }()
    //声音
    lazy var volumeImageView:UIImageView = {
        let imageV = UIImageView.init(image: UIImage(named: "chat_record_signal_1"))
        return imageV
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(backgroudView)
        addSubview(recImageView)
        addSubview(volumeImageView)
        addSubview(centerImageV)
        addSubview(titleBackgroudView)
        addSubview(titleLabel)
        addLayoutContraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //添加约束
    func addLayoutContraint(){
        
        backgroudView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        recImageView.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.left.equalTo(21)
        }
        volumeImageView.snp.makeConstraints { (make) in
            make.top.height.equalTo(recImageView)
            make.right.equalTo(self).offset(-21)
        }
        centerImageV.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self).offset(15)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.bottom.equalTo(self).offset(-15)
        }
        
        titleBackgroudView.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.top).offset(-2)
            make.left.equalTo(self.titleLabel.snp.left).offset(-5)
            make.bottom.equalTo(self.titleLabel.snp.bottom).offset(2)
            make.right.equalTo(self.titleLabel.snp.right).offset(5)
        }

    }


}
