//
//  CRAudioPlayer.swift
//  gameplay
//
//  Created by Gallen on 2019/10/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import AVFoundation

class CRAudioPlayer: NSObject {
    /**读取的状态*/
    var isPlaying:Bool{
        get{
            if (self.player != nil) {
                return (self.player?.isPlaying)!
            }
            return false
        }
    }
    
    var completeClouser:((_ finished:Bool)->(Void))?
    
    var player:AVAudioPlayer?
    
   static let sharedAudioPlayer:CRAudioPlayer = CRAudioPlayer()

}
extension CRAudioPlayer{
    
    func playAudioAtPath(path:String, complete:@escaping (_ finished:Bool)->(Void)){
        if (self.player?.isPlaying)! {
            self.stopPlayingAudio()
        }
        self.completeClouser = complete
        self.player = try? AVAudioPlayer.init(contentsOf: URL(fileURLWithPath: path))
        self.player?.delegate = self
    }
    //停止播放
    func stopPlayingAudio(){
        self.player?.stop()
    }
}
//MARK:AVAudioPlayerDelegate
extension CRAudioPlayer:AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.completeClouser?(true)
        self.completeClouser = nil
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        //播放音频文件出错
        self.completeClouser?(false)
        self.completeClouser = nil
    }
}
