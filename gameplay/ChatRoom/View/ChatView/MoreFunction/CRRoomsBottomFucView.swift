//
//  CRRoomsBottomFucView.swift
//  Chatroom
//
//  Created by admin on 2019/7/29.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit

class CRRoomsBottomFucView: UIView {
    var isPrivateChat = false
    /** 快速发言回调 */
    var clickSpeakQuicklyHandle: (() -> Void)?
    /** 图片收藏列表展示事件回调 */
    var clickImageCollectListHandle: (() -> Void)?
    
    let IDENTIFIER = "CRRoomsBottomFucCell"
    var isShow = false
    var collectionW:CGFloat = 0
    var collectionH:CGFloat = 0
    //collection相关
    var numsInHorizontal = 4 //每行个数
    var minimumLine:CGFloat = 10 //垂直间距
    var sectionMargin:CGFloat = 15
    var itemSize:CGSize = .zero
    let animateTime:TimeInterval = 0.3
    let minimumInteritem:CGFloat = 10 //水平间距
    /**用户角色*/
    var userType:Int = 0
    
    var functionViewHeight:CGFloat = 0
    
    var itemModels = [(String,String)]() //imageName,title
    
    var selectRoomHandler:((_ name: String) -> Void)?
    
    lazy var collection:UICollectionView = {
        
        var layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: sectionMargin, left: sectionMargin, bottom: sectionMargin, right: sectionMargin)
        layout.minimumLineSpacing = minimumLine
        layout.minimumInteritemSpacing = minimumInteritem
        layout.itemSize = CGSize.init(width: itemSize.width, height: itemSize.height)
        
        var view = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        view.register(CRRoomsBottomFucCell.self, forCellWithReuseIdentifier: IDENTIFIER)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }()
    
    lazy var topLineView:UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.gray
        return lineView
    }()
    
//    var moreFunctions:[Tuple] = [("moreKB_image","图片"),("moreKB_wallet","红包")]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        
        updateItemModels(isPricateChat: false)

        self.setupSeparateLine()
    }
    
    func updateItemModels(isPricateChat:Bool) {
//        if isPricateChat {
//            self.itemModels = [("ic_ctype_image","图片"), ("speakQuickly_normal","快速发言"), ("imageCollect_normal","收藏图片")]
//        }else {
            self.itemModels = [("ic_ctype_image","图片"), ("redpacket_normal","红包"), ("speakQuickly_normal","快速发言"), ("imageCollect_normal","收藏图片")]
//        }
        
//        collection.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSeparateLine() {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        line.backgroundColor = UIColor.lightGray
        self.addSubview(line)
        line.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.right.equalTo(0)
            make.height.equalTo(0.5)
        }
    }
    
    ///配置collection 信息
    func configWith(width:CGFloat) {
        self.collectionW = width
        
        // 计算设置 item的 size
        let itemWidth = (self.collectionW - 2 * sectionMargin - CGFloat(numsInHorizontal - 1) * minimumInteritem) / CGFloat(numsInHorizontal) //每个item的宽
        self.itemSize = CGSize.init(width: itemWidth, height: itemWidth)
        self.configWith(models: self.itemModels)
    }
    
    func configShow(show:Bool) {
        self.isShow = show
    }
    
    func configWith(models:[(String,String)]) {
        self.itemModels = models
        
        self.collectionH = self.calculateViewHeight(itemsCount: self.itemModels.count)
        
        self.addSubview(self.collection)
        
        self.collection.snp.makeConstraints { (make) in
            make.top.equalTo(0.5)
            make.left.bottom.right.equalTo(0)
        }
        self.collection.reloadData()
    }
    
    //计算collection高度
    private func calculateViewHeight(itemsCount:Int) -> CGFloat {
        let rows = self.itemModels.count % numsInHorizontal == 0 ? (self.itemModels.count / numsInHorizontal) : 1 + self.itemModels.count / numsInHorizontal //行数
        let mumLinesHeight =  CGFloat(rows - 1) * minimumLine //所有行间距的和
        let sectionMargin = self.sectionMargin * 2 //上下间距
        let itemsHeight = CGFloat(rows) * self.itemSize.height
        let height = mumLinesHeight + sectionMargin + itemsHeight
        functionViewHeight = height
        return height
    }
    
    @objc func iconClick(sender:UIButton) {
        
        let title = sender.titleLabel?.text
        let chatroomConfig = getChatRoomSystemConfigFromJson()
        //红包可发
        if title == "红包"{
            if userType != 4 && isPrivateChat {
                showToast(view: self, txt: "只有前台管理员可以发红包")
                return
            }
            
            if  CRDefaults.getChatroomSendRedPacket() == false{
                showToast(view: self, txt: "该房间不可发红包 请联系客服")
            }else{
                if chatroomConfig?.source?.switch_red_bag_send == "1"{
                    self.selectRoomHandler?(title ?? "")
                }else if chatroomConfig?.source?.switch_red_bag_send == "2"{
                    //前台管理员可发但普通用户不能发
                    if userType == 4{
                        self.selectRoomHandler?(title ?? "")
                    }else{
                        showToast(view: self, txt: "该房间您没有发红包的权限 请联系客服")
                    }
                    
                }else{
                    showToast(view: self, txt: "该房间不可发红包 请联系客服")
                }
            }
        }else if title == "图片"{
            if CRDefaults.getChatroomSendRedImage() == false{
                showToast(view: self, txt: "该房间不可发图片 请联系客服")
            }else{
                if chatroomConfig?.source?.switch_send_image_show == "1"{
                    self.selectRoomHandler?(title ?? "")
                }else{
                    showToast(view: self, txt: "该房间不可发图片 请联系客服")
                }
            }
        }else if title == "快速发言" {
            if CRDefaults.getChatroomFastTalk() {
                self.clickSpeakQuicklyHandle?()
            }else {
                showToast(view: self, txt: "您无法在该房间快速发言，请联系客服！")
            }
        }else if title == "收藏图片" {
            self.clickImageCollectListHandle?()
        }
    }
    
}

//MARK: - UICollectionViewDelegate
extension CRRoomsBottomFucView:UICollectionViewDelegate {
    
}


//MARK: - UICollectionViewDataSource
extension CRRoomsBottomFucView:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IDENTIFIER, for: indexPath) as? CRRoomsBottomFucCell else {
            fatalError("dequeueReusableCell CRRoomsBottomFucCell failure")
        }
        
        let model = self.itemModels[indexPath.row]
        cell.configItemWith(image: model.0, title: model.1)
        
        cell.item.addTarget(self, action: #selector(iconClick(sender:)), for: .touchUpInside)
        
        return cell
    }
    
}



