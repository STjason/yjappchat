//
//  CRRoomsBottomFucCell.swift
//  Chatroom
//
//  Created by admin on 2019/7/29.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import SnapKit

class CRRoomsBottomFucCell: UICollectionViewCell {
    var item = CRVerticalButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.item)
        self.item.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configItemWith(image:String,title:String) {
        let normalImg = UIImage.init(named: image)
        item.setImage(normalImg, for: .normal)
        item.setTitle(title, for: .normal)
        item.setTitleColor(UIColor.darkText, for: .normal)
        item.titleLabel?.font = UIFont.systemFont(ofSize: 15.0)
    }
    
}
