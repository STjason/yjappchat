//
//  CRAudioRecorder.swift
//  gameplay
//
//  Created by Gallen on 2019/10/1.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import AVFoundation
//音频路径
let file_path = NSSearchPathForDirectoriesInDomains(.documentationDirectory, .userDomainMask, true).first?.appending("/rec.caf")

class CRAudioRecorder: NSObject {
    
    static let sharedRecord:CRAudioRecorder = CRAudioRecorder()
    
    var volumeChangedClouser:((_ valume:Float)->(Void))?
    var completeCluser:((_ path:String,_ time:Float)->(Void))?
    var cancelClouser:(()->(Void))?
    var timer:Timer?
    
    var player: AVAudioPlayer?
    
    lazy var recorder:AVAudioRecorder = {
        let session = AVAudioSession.sharedInstance()
        let sessionError:Error?
        do{
//            try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions(rawValue: 0))
//            try session.setActive(true)
            
            if #available(iOS 10.0, *) {
                try! AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .moviePlayback)
            }
            else {
                // Workaround until https://forums.swift.org/t/using-methods-marked-unavailable-in-swift-4-2/14949 isn't fixed
                AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.playback)
            }
            
            try session.setActive(true)
            
        } catch{
            print("Error creating session")
        }
        let recordSetting: [String: Any] = [AVSampleRateKey: NSNumber(value: 44100),//采样率
            
            AVFormatIDKey: NSNumber(value: kAudioFormatAppleIMA4),//音频格式
            
            AVLinearPCMBitDepthKey: NSNumber(value: 8),//采样位数
            
            AVNumberOfChannelsKey: NSNumber(value: 1),//通道数
            
            AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.medium.rawValue)]//录音质量
        
        let path = file_path ?? ""
        let tempRecorder = try! AVAudioRecorder.init(url: URL.init(fileURLWithPath: path), settings: recordSetting)
        tempRecorder.delegate = self
        tempRecorder.isMeteringEnabled = true
        return tempRecorder

    }()
 
  
}
extension CRAudioRecorder{
    //MARK:开始录音
    func startRecordingWithVouumeChangeClouser(_ change: ((_ valume:Float)->(Void))?,_ finish: ((_ path:String,_ time:Float)->(Void))?,_ cancel: (()->(Void))?){
        self.volumeChangedClouser = change
        self.completeCluser = finish
        self.cancelClouser = cancel
        if FileManager.default.fileExists(atPath: file_path!) {
           try? FileManager.default.removeItem(atPath: file_path!)
        }
        recorder.prepareToRecord()
        recorder.record()
        
        if #available(iOS 10.0, *) {
            timer?.invalidate()
            let recorder = self.recorder
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: {[weak self] (timer:Timer) in
                if let weakSelf = self{
                    weakSelf.recorder.updateMeters()
                    let peakPower = pow(10, 0.05 * weakSelf.recorder.peakPower(forChannel: 0))
                    if let volumeChange = weakSelf.volumeChangedClouser{
                        volumeChange(peakPower)
                    }
                }
            })
        } else {
            // Fallback on earlier versions
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }
    //结束录音
    func stopRecording(){
        self.timer = nil
        self.timer?.invalidate()
        let time = self.recorder.currentTime
        self.recorder.stop()
        self.completeCluser?(file_path!,Float(time))
    }
    //取消录音
    func cancelRecording(){
        self.timer = nil
        self.timer?.invalidate()
        self.recorder.stop()
        self.cancelClouser?()
    }
    //播放
    func play() {
        do {
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: file_path!))
            
            print("歌曲长度：\(player!.duration)")
            
            player!.play()
            
        } catch let err {
            
            print("播放失败:\(err.localizedDescription)")
            
        }
    }
    
    @objc func update(){
        self.recorder.updateMeters()
        let peakPower = pow(10, 0.05 * self.recorder.peakPower(forChannel: 0))
        if let volumeChange = self.volumeChangedClouser{
            volumeChange(peakPower)
        }
    }
}
    //MARK:AVAudioRecorderDelegate
extension CRAudioRecorder:AVAudioRecorderDelegate{
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            print("录音成功")
        }
    }
}

