//
//  CRChatBarStatus.swift
//  gameplay
//
//  Created by Gallen on 2019/9/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

let  HEIGHT_CHATBAR_TEXTVIEW      =   36.0
let  HEIGHT_MAX_CHATBAR_TEXTVIEW   =  111.5
let  HEIGHT_CHAT_KEYBOARD  =     215.0

@objc enum CREmojiType:Int {
    case Emoji
    case Favorite
    case Face
    case Image
    case ImageWithTitle
    case Other
}

enum CRChatBarStatus:Int {
    case Init
    case Voice
    case Emoji
    case More
    case Keyboard
    case KeyboardFollowField
}

