//
//  CRShareGainsLossesItem.swift
//  gameplay
//
//  Created by Gallen on 2019/12/3.
//  Copyright © 2019年 yibo. All rights reserved.
// 聊天室个人信息中的分享今日盈亏模型

import UIKit
import HandyJSON

class CRShareGainsLossesItem: CRMessage {
    /**总盈利*/
    var yingli:Float = 0
    /**总投注*/
    var touzhu:Float = 0
    /**总盈亏*/
    var yingkui:Float = 0
    
    required init() {
        super.init()
        self.messageType = .ShareGainsLosses
    }
    
    override var messageFrame: CRMessageFrame?{
        get{
            kMessageFrame = CRMessageFrame()
            let width = 220
            let height = 100
            kMessageFrame?.contentSize = CGSize(width: width, height: height)
            let frameHeight = kMessageFrame?.contentSize.height ?? 0
            kMessageFrame?.height = Float(frameHeight) + 60
            return kMessageFrame
        }
        set{
            super.messageFrame = newValue
        }
    }
}
