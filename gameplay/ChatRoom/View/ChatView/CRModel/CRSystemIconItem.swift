//
//  CRSystemIconItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON
class CRSystemIconItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRSystemUserIconSourceItem?
    var status:String = ""
    required init(){}
}

class CRSystemUserIconSourceItem:HandyJSON{
    var items:[String] = []
    var nickName:String = ""
    var avatar:String = ""
    var msgUUID:String = ""
    required init(){}
}
