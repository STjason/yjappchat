//
//  CRVoiceMessageItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

enum CRVoiceMessageStatus:Int {
    case normal
    case recording
    case playing
}

class CRVoiceMessageItem: CRMessage {
    
    var fileCode:String = ""
    var fileString:String = ""

    var recFileName:String{
        get{
            let path = self.content["path"] as? String ?? ""
            return path
        }
        set{
            self.content["path"] = newValue as AnyObject
        }
    }
    var path:String?
//    {
//        didSet{
//            path = FileManager.pathUserChatVoice(voiceName: self.recFileName)
//        }
//    }
     var voiceUrl:String?{
        get{
            let path = self.content["url"] as? String ?? ""
            return path
        }
        set{
            self.content["url"] = newValue as AnyObject
        }
    }
      var durationTime:String{
        get{
            let duration = self.content["time"] as? String ?? ""
            return duration
        }set{
            self.content["time"] = newValue as AnyObject
        }
    }
    var msgStatus:CRVoiceMessageStatus?

    
    required init() {
        super.init()
        self.messageType = .Voice
    }
    
    var showTime:String {
        get {
            if let timeFloat = Float(durationTime) {
                let time = Int(timeFloat)
                return "\(time)\""
            }
            
            return ""
        }
    }
    
    override var messageFrame: CRMessageFrame?{
        get{
            kMessageFrame = CRMessageFrame()
            
            var width = 0
            if let timeFloat = Float(durationTime) {
                let time = Int(timeFloat)
                
                if time > 2 {
                    width = time * 4 + 60
                }else {
                    width = 60
                }
                
                if width > 180 {
                    width = 180
                }
            }
            
            let height = 30
            kMessageFrame?.contentSize = CGSize(width: width, height: height)
            let frameHeight = kMessageFrame?.contentSize.height ?? 0
            kMessageFrame?.height = Float(frameHeight  + 30 + 15 + 3)
            return kMessageFrame
        }
        set{
            super.messageFrame = newValue
        }
    }
}

class CRAudioMsgStrItem:HandyJSON {
    
    var audioMsg:CRAudioMsgItem?
    var record = ""
    var msgUUID = ""
    var msgId = ""
    var fileString = ""
    var fileType:String = ""
    var code:String = ""
    var agentRoomHost:String = ""
    required init() {}
}

class CRAudioMsgItem:HandyJSON{
    var duration:Int = 1
    var code:String = ""
    var size:String = ""
    var record:String = ""
    var source:String = ""
    var thumbPath:String = ""
    var userId:String = ""
    
    required init() {}
}



