//
//  CRReceiveDetailItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/2.
//  Copyright © 2019年 yibo. All rights reserved.
// 红包领取详情

import UIKit
import HandyJSON

class CRReceiveDetailItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRReceiveDetailSourceItem?
    var status:String = ""
    var msgUUID:String = ""
    
    required init(){}
}
class CRReceiveDetailSourceItem: HandyJSON {
    var parent:CRReceiveDetailParent?
    var redStatus:String = ""
    var other:[CRReceiveDetailOther]?
    var c:CRReceiveDetailCustom?
    required init(){}
}
class CRReceiveDetailParent:HandyJSON{
    var balance:Float = 0
    var countNum:Int = 0
    var hasNum:Int = 0
    var remark:String = ""
    var countMoney:Float = 0
    required init(){}
}
class CRReceiveDetailOther:HandyJSON{
    var money:Float = 0
    var nickName:String = ""
    var levelName:String = ""
    var userType:String = ""
    var avatar:String = ""
    var id:String = ""
//    var account:String = ""
    var nativeAccount:String = ""
    var levelIcon:String = ""
    var onLine:String = ""
    required init(){}
}
class CRReceiveDetailCustom:HandyJSON{
    var nickName:String = ""
    var levelName:String = ""
    var avatar:String = ""
    var totalCount:String = ""
    var type:String = ""
    var userId:String = ""
    var parentId:String = ""
    var remaining:String = ""
    var money:Float = 0
    var createTime:String = ""
    var userType:String = ""
    var id:String = ""
//    var account:String = ""
    var nativeAccount:String = ""
    var levelIcon:String = ""
    var stationId:String = ""
    var status:String = ""
    required init(){}
}
