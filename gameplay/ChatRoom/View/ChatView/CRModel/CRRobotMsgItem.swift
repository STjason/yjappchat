//
//  CRRobotMsgItem.swift
//  gameplay
//
//  Created by admin on 2019/11/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRRobotMsgItem: CRMessage {
    var robotMessage:CRRobotMessage?
    
    lazy var attrText:NSAttributedString = {
        var attri = NSAttributedString(string: text ?? "")
        
        guard let message = self.robotMessage  else {
            return "".toMessageString()
        }
        
        let record = message.record
        attri = record.toMessageString()
        return attri
    }()
    
    required init() {
        super.init()        
    }
}

class CRRobotMessage: HandyJSON {
    var code = ""
    var fromUser:CRRobotFromUser?
    var robotName = ""
    //机器人的网页
    var record = ""
    var levelName = ""
    var userType = ""
    var time = ""
    var msgUUID = ""
    var roomId = ""
    required init() {}
}

class CRRobotFromUser:HandyJSON {
    var robotImage = ""
    var createTime = ""
    var enable = ""
    var levelId = ""
    var name = ""
    var banSpeakSend = ""
    var levelName = ""
    var id = ""
    var stationId = ""
    required init() {}
}








