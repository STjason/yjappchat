//
//  CRLotterySchemeModel.swift
//  gameplay
//
//  Created by JK on 2019/12/3.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRLotterySchemeModel: HandyJSON {
    
    var msg: String = ""
    var code: String = ""
    var success: Bool = false
//    var source: CRLotterySchemeSource?
    var source: CRLotterySchemeData?
    var status: String = ""
    var msgUUID = ""
    
    required init(){}
}

class CRLotterySchemeSource: HandyJSON {
    
//    var msg:CRLotterySchemeData?
    var code:String = ""
    var success:Bool = false
    var status:String = ""
    
    required init(){}
}

class CRLotterySchemeData: HandyJSON {
    
    var lotteryCode: String = ""
    var playList: [String] = []
    var lotteryName: String = ""
    var resultList: [CRResultListModel] = []
    var lotteryList: [CRLotteryListModel] = []
    var option: String = ""
    
    required init(){}
}

class CRResultListModel: HandyJSON {
    
    var lotteryResult: String = ""
    var lotteryNum: String = ""
    var resultDate: String = ""
    var forecast: String = ""
    var source: String = ""
    var lotteryCode: String = ""
    var lotteryName: String = ""
    var stationId: String = ""
    
    required init(){}
}

class CRLotteryListModel: HandyJSON {
    
    var source: String = ""
    var lotteryCode: String = ""
    var lotteryName: String = ""
    var stationId: String = ""
    
    required init(){}
}

class CRSpeakQuicklyListModel: HandyJSON {
    
    var msg: String = ""
    var code: String = ""
    var success: Bool = false
    var source: [CRSpeakQuicklyModel] = []
    var status: String = ""
    
    required init(){}
}

class CRSpeakQuicklyModel: HandyJSON {
    
    var createTime: String = ""
    var enable: String = ""
    var record: String = ""
    var updateTime: String = ""
    var id: String = ""
    var stationId: String = ""
    
    required init(){}
}

class CRImageCollectListModel: HandyJSON {
    
    var msg: String = ""
    var code: String = ""
    var success: Bool = false
    var source: CRImageCollectModel?
    var status: String = ""
    
    required init(){}
}

class CRImageCollectModel: HandyJSON {
    
    var collectImageArray:[String] = []
    var option: String = ""
    
    required init(){}
}
