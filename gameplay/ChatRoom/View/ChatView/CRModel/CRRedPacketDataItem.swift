//
//  CRRedPacketDataItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/30.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRRedPacketDataItem: HandyJSON {
    
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRRedPacketMessageItem?
    var status:String = ""
    var msgUUID = ""
    required init(){}
}
//class CRRedPacketSourceItem:HandyJSON{
//    var msg:CRRedPacketMessageItem?
//    var msg:String = ""
//    var code:String = ""
//    var success:Bool = false
//    var msgUUID:String = ""
//    var status:String = ""
//
//    "code":"R7009",
//    "money":0.1,
//    "createTime":"2019-10-17 22:53:59",
//    "success":true,
//    "count":1,
//    "remark":"恭喜发财,大吉大利!",
//    "payId":"83a1cc2ee19a42e195db76c0975c9cac",
//    "userType":1,
//    "msgUUID":"7bdaa4d7f3b048b2b1d1031914649e4e593016837190706",
//    "user":Object{...},
//    "roomId":"008f0dcf7e7e4eecbf17537eb5bcdc1d_yunt0Chat_2",
//    "stationId":"yunt0Chat_2"
//    required init(){}
//}
class CRRedPacketMessageItem:HandyJSON{
    var code:String = ""
    var money:String = ""
    var createTime:String = ""
    var success:Bool = false
    var count:Int = 0
    var remark:String = ""
    var payId:String = ""
    var userType:Int = 0
    var user:CRRedPacketUserItem?
    var msgUUID:String = ""
    var roomId:String = ""
    var stationId:String = ""
     required init(){}
}
class CRRedPacketUserItem:HandyJSON{
    var nickName:String = ""
    var levelName:String = ""
    var userType:Int = 0
    var avatar:String = ""
    var id:String = ""
    var account:String = ""
    var levelIcon:String = ""
    
    required init(){}
}
