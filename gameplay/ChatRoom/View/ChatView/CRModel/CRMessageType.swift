//
//  CRMessageType.swift
//  gameplay
//
//  Created by Gallen on 2019/9/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

enum  CRMessageType:Int {
    /**系统消息*/
    case System = 0
    /**群聊 文本消息*/
    case Text = 1
    /**私聊 文本消息*/
    case PrivateText = 33
    /**图片消息*/
    case Image = 2
    /**红包消息*/
    case RedPacket = 3
    /**领红包*/
    case PickRedPacket = 4
    /**分享注单消息*/
    case ShareBet = 5
    /**计划消息*/
    case PlanMsg = 6
    /**机器人消息*/
    case robotMsg = 7
    /**跟注消息*/
    case FollowBet = 8
    /**今日盈亏分享*/
    case ShareGainsLosses = 9
    /**语音消息*/
    case Voice = 11
    /** 进房消息 */
    case InsideRoom = 13
    /** 开奖结果消息 */
    case WinnerInfo = 14
    ///禁言或取消禁言
    case Band_speak_OrNot = 17
    case Native_Band_speak_OrNot = 35
    ///全体禁言，解除禁言
    case Band_speakALL_OrNot = 21
    ///撤回消息
    case retractMsg = 31
    ///禁言申述
    case appealBand = 22
}

let MAX_MESSAGE_SYS_WIDTH          = kScreenWidth * 0.68
let MAX_MESSAGE_WIDTH              = kScreenWidth * 0.58
let MAX_MESSAGE_IMAGE_WIDTH        = kScreenWidth * 0.45
let MIN_MESSAGE_IMAGE_WIDTH        = kScreenWidth * 0.25
let MAX_MESSAGE_EXPRESSION_WIDTH   = kScreenWidth * 0.35
let MIN_MESSAGE_EXPRESSION_WIDTH   = kScreenWidth * 0.2

