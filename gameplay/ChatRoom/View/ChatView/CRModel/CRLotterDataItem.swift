//
//  CRLotterDataItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/1.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRLotterDataItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRLotterDataSource?
    var status:String = ""
    var msgUUID = ""
    required init(){}
}
class CRLotterDataSource:HandyJSON{
    var success:Bool = false
    var lotteryData:[CRLotteryData]?
    required init(){}
}
class CRLotteryData: HandyJSON {
    var sortNo:String = ""
    var code:String = ""
    var ago:String = ""
    var icon:String = ""
    var name:String = ""
    var className:String = ""
    var lotVersion:String = ""
    var id:String = ""
    var lotType:String = ""
    var groupCode:String = ""
    var stationId:String = ""
    var status:String = ""
    var haoMa:String = ""
    var qiHao:String = ""
    var lastQiHao:String = ""
    var date:Int64 = 0
    required init(){}
}
