//
//  CRShareBetMessageItem.swift
//  gameplay
//
//  Created by admin on 2019/9/25.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRShareBetMessageItem: CRMessage {
    
    var shareBetModel:CRShareBetModel? //注单分享数据
    //胜率
    var winRate:Float = 0
    
    required init() {
        super.init()
    }
}
