//
//  CRSystemNoticeItem.swift
//  gameplay
//
//  Created by Gallen on 2019/10/1.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRSystemNoticeItem: HandyJSON {
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:[CRSystemNoticesourceItem]?
    var status:String = ""

    required init(){}
}

class CRSystemNoticesourceItem:HandyJSON {
    var id:String = ""
    var title:String = ""
    var body:String = ""
    var enable:String = ""
    var updateTime:String = ""
    var createTime:String = ""
    var stationId:String = ""
    required init(){}
}
