//
//  CRImageMessage.swift
//  gameplay
//
//  Created by Gallen on 2019/9/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRImageMessage: CRMessage {
    var imageReesource:CRImageResource?
    /**本地图片path*/
    var imagePath:String{
        get {
            let imgPath = self.content["path"] as? String ?? ""
            return imgPath
        }
        set{
            self.content["path"] = newValue as AnyObject
        }
    }
    /**网络图片url*/
    var imageUrl:String?{
        get{
            let imgUrl = self.content["url"] as? String ?? ""
            return imgUrl
        }
        set{
            self.content["url"] = newValue as AnyObject
        }
    }
    
    var fileCode:String = ""
    
    var progress:Double = 0{
        didSet{
            
        }
    }
    //图片的尺寸
    var imageSize:CGSize{
        get{
            let width = self.content["w"] as? Double ?? 0
            let height = self.content["h"] as? Double ?? 0
            return CGSize(width: width, height: height)
        }
        set{   
            self.content["w"] = newValue.width as AnyObject
            self.content["h"] = newValue.height as AnyObject
        }
    }
    
    required init() {
        super.init()
        messageType = .Image
    }
    
    override var messageFrame: CRMessageFrame?{
        get{
            if kMessageFrame == nil {
                kMessageFrame = CRMessageFrame()
                kMessageFrame?.height = 40 //40
                let imageSize = self.imageSize
                if __CGSizeEqualToSize(imageSize, CGSize.zero){
                    kMessageFrame?.contentSize = CGSize(width: 168.75, height: 112.6125)
                }else if (imageSize.width > imageSize.height){
                    var height = MAX_MESSAGE_IMAGE_WIDTH * imageSize.height / imageSize.width
                    height = height < MIN_MESSAGE_IMAGE_WIDTH ? MIN_MESSAGE_IMAGE_WIDTH : height
                    kMessageFrame?.contentSize = CGSize(width: MAX_MESSAGE_IMAGE_WIDTH, height: height)
                }else{
                    var width = MAX_MESSAGE_IMAGE_WIDTH * imageSize.width / imageSize.height
                    width = width < MIN_MESSAGE_IMAGE_WIDTH ? MIN_MESSAGE_IMAGE_WIDTH : width
                    kMessageFrame?.contentSize = CGSize(width: width, height: MAX_MESSAGE_IMAGE_WIDTH)
                }
                kMessageFrame?.height = Float(CGFloat((kMessageFrame?.contentSize.height)!) + CGFloat((kMessageFrame?.height)!))
            }
            return kMessageFrame
        }
        set{
            super.kMessageFrame = newValue
        }
        
    }
}

class CRImageResource:HandyJSON {
    var record = ""
    var msgUUID = ""
    var msgId = ""
    var fileString = ""
//    "record": "1911bc78a5fdf7cb4833a8cd6fabdea796aa",
//    "sentTime": "2019-11-13 20:28:38",
//    "msgUUID": "e7b69fc3f3f74dd3",
//    "source": "app",
//    "userType": 0,
//    "type": 2,
//    "userId": "911a330a7e41410c9b65ca8e7ca835c1",
//    "roomId": "4bc576891e1c478c89699ce50e615034"
    required init() {}
}










