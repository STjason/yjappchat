//
//  CRMessage.swift
//  gameplay
//
//  Created by Gallen on 2019/9/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

/**发送消息者类型*/
enum CRMessageOwnerType:Int {
    case Unknow = -1
    /**自己发送的消息*/
    case TypeSelf
    /**别人发送的消息*/
    case Others
    /**系统消息*/
    case System

}
enum CRMessageSendState {
    /**消息发送成功*/
    case Success
    /**发送过程中*/
    case progress
    /**消息发送失败*/
    case Fail
}

class CRMessageWraper: HandyJSON {
    var code = ""
    var source = ""
    var nativeMsg:String = "" //app返回消息类型
    required init() {}
}

class CRMessage: HandyJSON {
    //当前cell所显示的用户名
    var currentUserName = ""
    var msg = ""
    var success = false
    var status = ""
    
    var userMessageId = "" //私聊 messageId
    var fromUser:CRPrivateFromUser? //私聊历史记录使用
    
    ///是否被禁言 1--是 0--不是
    var stopTalkType = "0"
    var isPlanUser = "0" //1为 计划员,该字段为历史消息中返回
    var lotteryName = ""
//    var text = ""
    
    var text:String?{
        didSet{
            if "\(String(describing: messageType?.rawValue))" != "1" {
                content["text"] = text as AnyObject
            }
        }
    }
    //本地判断消息是否已读
    var isReaded = true
    //消息ID
    var messageId:String = ""
    var msgUUID:String = ""
    //发送者id
    var userId:String = ""
    //房间ID
    var roomId :String = ""
    /**发送消息类型编号*/
    var code:String = "";
    /**消息内容*/
    var msgStr:String = ""
    
    var msgType:String = ""
    /**app*/
    var source:String = ""
    /**房间*/
    var stationId = ""
    //发送时间
    var sentTime:String = ""
    //用户图像
    var avatar:String?
    /**发送消息的类型 1.快捷发言 2.自由发言 3.发送表情 4.投注消息 5.@其它用户的消息 6.快捷图片*/
    var speakType:String = ""
    
    var isShowName:Bool = false
    
    var isShowTime:Bool = false
    
    var ownerType:CRMessageOwnerType?
    
    var sendState:CRMessageSendState?
    
    var kMessageFrame:CRMessageFrame?
    
    var content:[String:AnyObject] = [:]
    
    var messageType:CRMessageType?
    //消息frame
    var messageFrame:CRMessageFrame?
    
    var isSuccess:Bool = false
    
    var msgResult:msgResultItem?
    var audioMsg:CRAudioMsgItem?
    
    var nativeMsg:String = ""
    
    /**文件路径*/
    var fileStr:String = ""

    var agentRoomHost:String = ""
    var fileType:String = ""
    var date:String = ""
    var nickName:String = ""
    var privatePermission = false 
    var msgId:String = ""
    var levelName:String = ""
    var sender:String = ""
    /**跟单 文本*/
    var context:String = ""
    /**web端 分享注单数据*/
    var nativeContent = ""
    var userType:String = "1" //1普通玩家，2后台管理员，3机器人，4前台管理员 5前台普通用户（后台试玩 暂时没用到）
    var time:String = ""
    var account:String = ""
    var isShowAccountNow = false //当前是否在显示 不带星号的账户名
    var nativeAccount:String = ""
    var levelIcon:String = ""
    /**服务器上的该图片的编码*/
    var url:String?
    //红包总金额
//    var money:String = ""
    /**红包Id*/
    var payId:String = ""
    /**红包的名字 备注*/
    var remark:String = ""
    /**音频时长*/
    var duration:Int = 1
    
    var record = ""

    //用户胜率模型
    var winOrLost:CRWinRateItem?
    

    func createMessageByType(type:CRMessageType) ->CRMessage{
        var className = ""
        if type == .Text {
            className = "CRTextMessage"
        }else if (type == .Image){
            className = "CRImageMessage"
        }
        
        if className.isEmpty{
             return CRMessage()
        }else{
            return NSClassFromString(className) as AnyObject as! CRMessage
        }
      
    }
    
    func resetMessageFrame(){
//        kMessageFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    }
    
    required init() {
        
    }
}

class msgResultItem:HandyJSON{
    
    var userEntity:userEntityItem?
    var code:String = ""
    var msgType:String = ""
    var wapMsgStr:String = ""
    var msgStr:String = ""
    var source:String = ""

    
    required init() {}
}

class userEntityItem:HandyJSON{
    var privatePermission = false //是否有私聊权限
    var account:String = ""
    var nativeAccount = ""
    var nickName:String = ""
    var accountType:String = ""
    var createTime:String = ""
    var id:String = ""
    var levelIcon:String = ""
    var levelId:String = ""
    var levelName:String = ""
    var stationId:String = ""
    var updateTime:String = ""
    var userClust:String = ""
    var userCode:String = ""
    var userDetailEntity:userDetailEntityItem?
    var msgType:String = ""
    var planUser = "0" //是否是计划员 0否，1 是
    var userType = "1" //1普通玩家，2后台管理员，3机器人，4前台管理员 5前台普通用户（后台试玩 暂时没用到）
    required init() {}
}

class userDetailEntityItem:HandyJSON{
    var avatarCode:String = ""
    var id:String = ""
    var userIp:String = ""
    required init() {}
}
//
class CRMessageEntity:HandyJSON{
    
    var channelId:String = ""
    var code:String = ""
    var msgUUID :String = ""
    var msgId = ""
    var option1 :String = ""
    var record :String = ""
    var roomId :String = ""
    var source :String = ""
    var remark:String = ""
    var type:String = ""
    var userId:String = ""
    var picMsg:String = ""
    var fileString = ""
    required init() {}
}

class CRSpeakingCloseItem:HandyJSON{
    
    //"msgStr": "{\"code\":\"R0009\",\"speakingClose\":0,\"sysUserAccount\":\"yjtest1admin\",\"source\":\"backGround\",\"msgUUID\":\"10defd2941d94961\",\"speakingCloseId\":\"bedb8d749eb9485da0d8ce577c18bee8\",\"roomId\":\"b501656b1d7f4acb8474e64e6d49b856\"}",

    var code = ""
    var speakingClose = ""
    var sysUserAccount = ""
    var source = ""
    var msgUUID = ""
    var speakingCloseId = ""
    var roomId = ""
     required init() {}
}


