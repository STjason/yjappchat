//
//  CRRedPacketContentItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/29.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRRedPacketContentItem : HandyJSON {
    
    var amount:String = ""
    var channelId:String = ""
    var code:String = ""
    var count:Int = 0
    var msgUUID:String = ""
    var remark:String = ""
    var roomId:String = ""
    var source:String = ""
    var stationId:String = ""
    var userId:String = ""
    var payId:String = ""
    var msg:String = ""
    required init(){}
}
class CRRedPacketMsgItem: HandyJSON {
    var code:String = ""
    var count:Int = 0
    var createTime:String = ""
    var money:String = ""
    var payId:String = ""
    var remark:String = ""
    var roomId:String = ""
    var stationId:String = ""
    var user:String = ""
    var userType:Int = 1
    
    
    
     required init(){}
}

