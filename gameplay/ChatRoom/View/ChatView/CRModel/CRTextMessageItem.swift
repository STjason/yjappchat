//
//  CRTextMessageItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/9.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRTextMessageItem: CRMessage {
    /**文字信息展示*/
//    var text:String = ""
    /**格式化文字信息*/
//    var attrText:NSAttributedString?
    
    var textLabel:UILabel?
    
    lazy var attrText:NSAttributedString = {
        var attri = NSAttributedString(string: text ?? "")
        
        attri = self.text!.toMessageString()
        return attri
    }()
    required init() {
        super.init()
        
        let groupItem = CRExpressionHelper.sharedHelper.defaultFaceGroup as? CRExpressionGroupModel
        
        if textLabel == nil {
            textLabel = UILabel()
            let font = getChatRoomSystemConfigFromJson()?.source?.switch_chat_word_size_show
            
            if let textFont = font{
                var textFonts = CGFloat(textFont)
                if textFonts < 15{
                    textFonts = 15
                }
                textLabel!.font = UIFont.systemFont(ofSize:textFonts)
            }else{
                textLabel!.font = UIFont.systemFont(ofSize: 15)
            }
            textLabel?.numberOfLines = 0
        }
    }
    
    override var messageFrame: CRMessageFrame?{
        get{
            if kMessageFrame == nil{
                kMessageFrame = CRMessageFrame()
                kMessageFrame?.height = 60
                textLabel?.attributedText = attrText
                kMessageFrame?.contentSize = (textLabel?.sizeThatFits(CGSize(width: MAX_MESSAGE_WIDTH, height: CGFloat(MAXFLOAT))))!
                kMessageFrame?.height = Float(CGFloat((kMessageFrame?.contentSize.height)!) + CGFloat((kMessageFrame?.height)!))
               
            }
            return kMessageFrame
        }
        set{
            super.messageFrame = newValue
        }
      
    }


}
