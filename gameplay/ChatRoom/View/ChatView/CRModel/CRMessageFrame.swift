//
//  CRMessageFrame.swift
//  gameplay
//
//  Created by Gallen on 2019/9/8.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMessageFrame: NSObject {

    var height:Float = 0 // 消息高度
    var contentSize:CGSize = CGSize.zero //内容高度
    var webHeight:CGFloat = 0 //计划消息内的webview高度
}
