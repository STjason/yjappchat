//
//  CRFollowBetModel.swift
//  gameplay
//
//  Created by admin on 2019/10/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRFollowBetWrapper: CRBaseModel {
    var source:CRFollowBetSource?
    required init() {}
}

class CRFollowBetSource:HandyJSON {
    var success = true //默认为true
    var msg = ""
    var accessToken = ""
    var code = ""
    required init() {}
}
