//
//  CRReceiveRedPacketItem.swift
//  gameplay
//
//  Created by Gallen on 2019/9/30.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class CRReceiveRedPacketItem: HandyJSON{
    var msg:String = ""
    var code:String = ""
    var success:Bool = false
    var source:CRReceiveRedPacketSourceItem?
    var status:String = ""
    var msgUUID:String = ""
    required init(){}
}

class CRReceiveRedPacketSourceItem:HandyJSON{
    var parentData:CRReceiveRedPacketparentData?
    var history:CRReceiveRedPacketHistoryItem?
    var pickData:CRReceiveRedPacketPickDataItem?
    var payId:String = ""
    /**判断红包状态是否被领取或领完*/
    var status:String = ""
    var msgUUID:String = ""
    required init(){}
}
class CRReceiveRedPacketparentData:HandyJSON{
    var id :String = ""
    var userId:String = ""
    var type:String = ""
    /**当前用户领取了多少*/
    var money:String = ""
    var createTime:String = ""
    var parentId:String = ""
    /**红包剩余*/
    var remaining:String = ""
    var remark:String = ""
    var status:String = ""
    var stationId:String = ""
    var name:String = ""
    var totalCount:Int = 0
    var remarkId:String = ""
    var sysPayId:String = ""
    var payConfigId:String = ""
    required init(){}
}
//history
class CRReceiveRedPacketHistoryItem:HandyJSON{
    
    required init(){}
}

class CRReceiveRedPacketPickDataItem:HandyJSON{
    var success:String = ""
    var payId:String = ""
    var newPay:CRReceiveNewPayRedPacketItem?
     required init(){}
}
class CRReceiveNewPayRedPacketItem:HandyJSON{
    var id:String = ""
    var userId:String = ""
    var type:String = ""
    var money:String = ""
    var createTime:String = ""
    var parentId:String = ""
    var remaining:String = ""
    var remark:String = ""
    var status:String = ""
    var stationId:String = ""
    var name:String = ""
    var totalCount:String = ""
    var remarkId:String = ""
    var sysPayId:String = ""
    var payConfigId:String = ""
     required init(){}
}


