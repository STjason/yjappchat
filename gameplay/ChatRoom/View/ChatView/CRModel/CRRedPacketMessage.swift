//
//  CRRedPacketMessage.swift
//  gameplay
//
//  Created by admin on 2019/9/26.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRRedPacketMessage: CRMessage {
    required init() {
        super.init()
    }
    /**红包个数*/
    var count:Int = 0
    /**红包描述*/
//    var remark:String = ""
    /**红包总金额*/
    var amount:String = ""
    /**抢红包Id*/
//    override var payId:String?
    
    var redPacketContentItem:CRRedPacketContentItem?{
        didSet{
            if let redPacketContent = redPacketContentItem{
                remark = redPacketContent.remark
                amount = redPacketContent.amount
                count = redPacketContent.count
                payId = redPacketContent.payId
            }
           
        }
    }
    
    override var messageFrame: CRMessageFrame?{
        get{
            if kMessageFrame == nil{
                kMessageFrame = CRMessageFrame()
                kMessageFrame?.height = 50
                kMessageFrame?.contentSize = CGSize.init(width: kScreenWidth * 0.7, height: 120)
                kMessageFrame?.height = Float(CGFloat((kMessageFrame?.contentSize.height)!) + CGFloat((kMessageFrame?.height)!))
                
            }
            return kMessageFrame
        }
        
        set{
            super.messageFrame = newValue
        }
    }
}
