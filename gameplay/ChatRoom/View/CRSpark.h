//
//  CRSpark.h
//  gameplay
//
//  Created by admin on 2019/10/22.
//  Copyright © 2019年 yibo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CRSpark : NSObject
//+ (void)setupSpark:(UIView *)view imageOne:(NSString *)imageOne imageTwo:(NSString *)imageTwo;
+ (void)setupSpark:(UIView *)view images:(NSArray *)images;
@end
