//
//  CRChatDrawerHeader.swift
//  gameplay
//
//  Created by admin on 2019/12/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import Kingfisher

protocol CRDrawerInfoDelegate {
    func tapDrawerAvatar() //点击头像
    func drawerDeposit() //充值
    func drawerWithdraw() //提款
    func drawerRefreshBalance() //刷新余额
}

class CRChatDrawerHeader: UIView {
    var delegate:CRDrawerInfoDelegate?
    var headerMargin:CGFloat = 5
    var reloadWinLostCount = 0 //获取 R7038有时 winLost字段为空，这时要再次获取。改字段表示第几次重新获取
    var requestUserInfoHandler:(() -> Void)? //获取昵称和头像
    
    var gainsLossesItem:CRUserInfoCenterSourceWinLostItem?
    
    var titleNames:[String] =
        ["今日中奖",//今日总盈利
        "今日盈亏",//今日总盈亏
        "今日总投注",
        "累计充值",
        "今日充值",
        "打码量"]
    var titleNamesOn:[String] = []
    var userInfoArray:[CRUserInfoItem] = []
    var headerWidth:CGFloat = 120
    
    //头部顶部
    var headerTop = UIView()//头部视图的顶部 //放置头像，会员等级，余额
    
    lazy var avatar: UIImageView = {
        let view = UIImageView()
        view.isUserInteractionEnabled = true
        view.layer.cornerRadius = 30
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFill
        view.image = UIImage.init(named: "chatroom_default")
        return view
    }()
    
    lazy var name: UILabel = { //名字
        let view = UILabel()
        view.textAlignment = .center
        view.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        view.text = ""
        view.textColor = UIColor.lightText
        return view
    }()
    
    lazy var level: UILabel = { //等级
        let view = UILabel()
        view.textAlignment = .center
        view.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        view.text = ""
        view.textColor = UIColor.lightText
        return view
    }()
    
    lazy var balance: UILabel = { //余额
        let view = UILabel()
        view.textAlignment = .center
        view.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        view.text = "余额:"
        view.textColor = UIColor.orange
        return view
    }()
    
    lazy var refreshBalance:UIButton = { //刷新金额
        let view = UIButton()
        view.setBackgroundImage(UIImage.init(named: "icon_refresh"), for: .normal)
        view.contentMode = .scaleAspectFill
        view.addTarget(self, action: #selector(refreshBalanceAction), for: .touchUpInside)
        return view
    }()
    
    //头部底部
    lazy var headerBottom:UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var deposit:UIButton = { //充值
        let view = UIButton()
        view.setTitleColor(UIColor.white, for: .normal)
        view.backgroundColor = UIColor.clear
        view.setTitle("充值", for: .normal)
        view.addTarget(self, action: #selector(depositAction), for: .touchUpInside)
        return view
    }()
    
    lazy var withdraw:UIButton = { //提款
        let view = UIButton()
        view.setTitleColor(UIColor.white, for: .normal)
        view.backgroundColor = UIColor.clear
        view.setTitle("提款", for: .normal)
        view.addTarget(self, action: #selector(withdrawAction), for: .touchUpInside)
        return view
    }()
    
    lazy var refreshButton:UIButton = {
        let view = UIButton()
        view.setTitle("刷新", for: .normal)
        view.layer.cornerRadius = 3
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.colorWithHexString("#BF7D4C")
        view.setTitleColor(UIColor.lightText, for: .highlighted)
        view.setTitleColor(UIColor.white, for: .normal)
        view.addTarget(self, action: #selector(refreshAction), for: .touchUpInside)
        return view
    }()
    
    lazy var shareButton:UIButton = {
        let view = UIButton()
        view.setTitle("分享", for: .normal)
        view.layer.cornerRadius = 3
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.colorWithHexString("#073984")
        view.setTitleColor(UIColor.lightText, for: .highlighted)
        view.setTitleColor(UIColor.white, for: .normal)
//        view.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        return view
    }()
    
    lazy var collection:UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = .zero
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize.init(width: (headerWidth - 2 * headerMargin) * CGFloat(0.33), height: 60)
        
        var collection = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collection.isScrollEnabled = false
        collection.backgroundColor = UIColor.clear
        collection.register(CRDrawerHeaderInfoCell.self, forCellWithReuseIdentifier: "CRDrawerHeaderInfoCell")
        collection.dataSource = self
        return collection
    }()
    
    convenience init(width:CGFloat) {
        self.init()
        
        headerWidth = width
        let frame = CGRect.init(x: 0, y: 0, width: headerWidth, height: 127 + kStatusHeight)
        createGradientLayer(view:headerTop,frame:frame,hexColors:["#6A007C","#5100A1"])
        
//        let headerTopImg = UIImageView()
//        headerTopImg.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//        headerTopImg.image = UIImage.init(named: "chatNavBar")
        
        //顶部,高度 127 + kStatusHeight
        addSubview(headerTop)
//        headerTop.addSubview(headerTopImg)
        headerTop.addSubview(avatar)
        headerTop.addSubview(name)
        headerTop.addSubview(level)
        headerTop.addSubview(balance)
        headerTop.addSubview(refreshBalance)
        
        addSubview(headerBottom)
        headerBottom.addSubview(deposit)
        headerBottom.addSubview(withdraw)
        headerBottom.addSubview(collection)
        headerBottom.addSubview(refreshButton)
        headerBottom.addSubview(shareButton)
        
        //充值，提款中间的 竖直分割线
        let depositVertical = UIView()
        depositVertical.backgroundColor = UIColor.lightText
        headerBottom.addSubview(depositVertical)
        
        //充值，提款中间的 水平分割线
        let depositHortical = UIView()
        depositHortical.backgroundColor = UIColor.lightText
        headerBottom.addSubview(depositHortical)
        
        //刷新 分享上方的 水平分割线
        let refreshShareTopLine = UIView()
        refreshShareTopLine.backgroundColor = UIColor.lightText
        headerBottom.addSubview(refreshShareTopLine)
        
        depositVertical.snp.makeConstraints { (make) in
            make.top.equalTo(deposit.snp.top)
            make.left.equalTo(deposit.snp.right)
            make.bottom.equalTo(deposit.snp.bottom).offset(-0.5)
            make.width.equalTo(0.5)
        }
        
        depositHortical.snp.makeConstraints { (make) in
            make.left.equalTo(deposit.snp.left)
            make.bottom.equalTo(deposit.snp.bottom).offset(-0.5)
            make.height.equalTo(0.5)
            make.right.equalTo(withdraw.snp.right)
        }
        
        refreshShareTopLine.snp.makeConstraints { (make) in
            make.top.equalTo(collection.snp.bottom)
            make.left.right.equalTo(0)
            make.height.equalTo(0.5)
        }
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tapAvatarAction))
        avatar.addGestureRecognizer(gesture)
        
        headerTop.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(0)
            make.height.equalTo(127 + kStatusHeight)
        }
        
//        headerTopImg.snp.makeConstraints { (make) in
//            make.edges.equalTo(UIEdgeInsets.zero)
//        }
        
        avatar.snp.makeConstraints { (make) in
            make.top.equalTo(kStatusHeight)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize.init(width: 60, height: 60))
        }
        
        name.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(weakSelf.avatar.snp.bottom).offset(2)
            make.centerX.equalToSuperview()
            make.height.equalTo(20)
        }
        
        level.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(weakSelf.name.snp.bottom)
            make.centerX.equalToSuperview()
            make.height.equalTo(20)
        }
        
        balance.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(weakSelf.level.snp.bottom)
            make.centerX.equalToSuperview()
            make.height.equalTo(20)
        }
        
        refreshBalance.snp.makeConstraints { (make) in
            make.left.equalTo(balance.snp.right).offset(5)
            make.centerY.equalTo(balance.snp.centerY)
            make.width.height.equalTo(18)
            make.right.lessThanOrEqualToSuperview().offset(-5)
        }
        
        //头部底部
        //高度：充值 40,collection 90，refreshButton 40 = 190
        headerBottom.snp.makeConstraints { (make) in
            make.top.equalTo(headerTop.snp.bottom)
            make.left.equalTo(headerTop.snp.left)
            make.right.equalTo(headerTop.snp.right)
            make.bottom.equalToSuperview()
        }
        
        deposit.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview()
            make.height.equalTo(40)
            make.right.equalTo(withdraw.snp.left).offset(-0.5)
            make.width.equalTo(withdraw.snp.width)
        }
        
        withdraw.snp.makeConstraints { (make) in
            make.top.equalTo(deposit.snp.top)
            make.bottom.equalTo(deposit.snp.bottom)
            make.right.equalToSuperview()
        }
        
        collection.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(weakSelf.deposit.snp.bottom)
            make.left.equalTo(headerMargin)
            make.right.equalTo(-headerMargin)
        }
        
        refreshButton.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(collection.snp.bottom).offset(10)
            make.centerX.equalTo(weakSelf.headerWidth*0.25)
            make.width.equalTo(60)
            make.height.equalTo(35)
            make.bottom.equalTo(-5)
        }
        
        shareButton.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.centerX.equalTo(weakSelf.headerWidth*0.75)
            make.width.equalTo(60)
            make.height.equalTo(35)
            make.bottom.equalTo(-5)
        }
        
        setupPlaceholderDataForCollection()
    }
    
    //设置collection占位数据
    func setupPlaceholderDataForCollection() {
        userInfoArray.removeAll()
        
        for on in 0..<titleNames.count{
            let userItem = CRUserInfoItem()
            userItem.titleName = titleNames[on]
            if userItem.titleName == "今日中奖"{
                //今日总盈亏 净盈利 = 中奖金额 + 投注反水 - 投注金额
                //今日中奖
                userItem.money = 0
            }else if  userItem.titleName == "今日盈亏"{
                //今日盈亏
                userItem.money = 0
                
            }else if  userItem.titleName == "今日总投注"{
                //今日总投注
                userItem.money = 0
            }else if userItem.titleName == "累计充值"{
                userItem.money = 0
            }else if userItem.titleName == "今日充值"{
                userItem.money = 0
            }else if userItem.titleName == "打码量"{
                userItem.money = 0
            }
            
            userInfoArray.append(userItem)
        }
        
        collection.reloadData()
    }
    
    func updateBalance(value:String) {
        if let floatValue = Float(value) {
            balance.text = String.init(format: "余额:%.2f元", floatValue)
        }
    }
    
    func updateUserInfo(userItem:CRUserInfoCenterItem?) {
        guard let userInfoItem =  userItem, let sourceItem = userInfoItem.source else{
            showToast(view: self, txt: "刷新失败，请重试")
            return
        }
        
        //当前用户的图像
        let userIconString = sourceItem.avatar
        //user's icon
        var userNickname = sourceItem.account
        if sourceItem.nickName != "" {
            userNickname = sourceItem.nickName
        }
        
        avatar.kf.setImage(with: URL(string: userIconString), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
        
        name.text = userNickname
        level.text = sourceItem.level
 
        guard let userInfoDetailItem = sourceItem.winLost else{
            reloadWinLostCount = reloadWinLostCount + 1
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.requestUserInfoHandler?()
            }
            return
        }
        
        gainsLossesItem = userInfoDetailItem
        userInfoArray.removeAll()
        
        for on in 0..<titleNamesOn.count{
            let userItem = CRUserInfoItem()
            userItem.titleName = titleNamesOn[on]
            if userItem.titleName == "今日中奖"{
                //今日总盈亏 净盈利 = 中奖金额 + 投注反水 - 投注金额
                //今日中奖
                userItem.money = userInfoDetailItem.allWinAmount
                userItem.isSwitchOn = false
            }else if  userItem.titleName == "今日盈亏"{
                //今日盈亏
                userItem.money = userInfoDetailItem.yingkuiAmount
                userItem.isSwitchOn = false
                
            }else if  userItem.titleName == "今日总投注"{
                //今日总投注
                userItem.money = userInfoDetailItem.allBetAmount
                userItem.isSwitchOn = false
            }else if userItem.titleName == "累计充值"{
                userItem.money = userInfoDetailItem.sumDepost
            }else if userItem.titleName == "今日充值"{
                userItem.money = userInfoDetailItem.sumDepostToday
            }else if userItem.titleName == "打码量"{
                userItem.money = userInfoDetailItem.betNum
            }
            
            userInfoArray.append(userItem)
        }
        
        collection.reloadData()
    }
    
    func setupData(){
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return
        }
        
        for (_,value) in titleNames.enumerated() {
            if value == "今日中奖"{
                titleNamesOn.append(value)
            }else if value == "今日盈亏"{
                if  source.switch_yingkui_show ==  "1"{
                    //显示今日盈亏
                    titleNamesOn.append(value)
                }
            }else if value == "今日总投注"{
                titleNamesOn.append(value)
            }else if value == "累计充值"{
                if source.switch_sum_recharge_show == "1" {
                    titleNamesOn.append(value)
                }
            }else if value == "今日充值"{
                if source.switch_today_recharge_show == "1" {
                    titleNamesOn.append(value)
                }
            }else if value == "打码量"{
                if source.switch_bet_num_show == "1"{
                    titleNamesOn.append(value)
                }
            }
        }
        
        requestUserInfoHandler?()
    }
    
    
    //MARK: - 事件
    //刷新金额
    @objc func refreshBalanceAction() {
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(1) //设置动画时间
        refreshBalance.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi))
        UIView.commitAnimations()
        
        //独立旋转，以初始位置旋转
        refreshBalance.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi*2))
        
        if let delegate = self.delegate {
            delegate.drawerRefreshBalance()
        }
    }
    
    //刷新输赢信息
    @objc func refreshAction() {
        let loading = UIActivityIndicatorView()
        loading.hidesWhenStopped = true
        loading.style = .whiteLarge
        loading.startAnimating()
        refreshButton.addSubview(loading)
        
        loading.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize.init(width: 20, height: 20))
            make.center.equalToSuperview()
        }
        
        refreshButton.setTitleColor(UIColor.clear, for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.refreshButton.setTitleColor(UIColor.white, for: .normal)
            loading.stopAnimating()
            loading.removeFromSuperview()
        }
        
        requestUserInfoHandler?()
    }
    //分享，被拿到外部处理了
    @objc func shareAction() {
        
    }
    
    //点击头像
    @objc func tapAvatarAction() {
        if let delegate = self.delegate {
            delegate.tapDrawerAvatar()
        }
    }
    
    //存款
    @objc func depositAction() {
        if let delegate = self.delegate {
            delegate.drawerDeposit()
        }
    }
    //取款
    @objc func withdrawAction() {
        if let delegate = self.delegate {
            delegate.drawerWithdraw()
        }
    }
}


extension CRChatDrawerHeader:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userInfoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CRDrawerHeaderInfoCell", for: indexPath) as? CRDrawerHeaderInfoCell  else {
            fatalError("dequeueReusableCell CRDrawerHeaderInfoCell error")
        }
        
        //隐藏最后一列分割线
//        let isLastRow = (indexPath.row + 1) % 3 == 0
//        cell.verticalLine.isHidden = isLastRow
        
        let userItem = userInfoArray[indexPath.row]
        cell.userInfoItem = userItem
        
        return cell
    }
    
    
}














