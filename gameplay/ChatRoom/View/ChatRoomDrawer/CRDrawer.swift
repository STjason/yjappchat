//
//  CRDrawer.swift
//  drawerTest
//
//  Created by admin on 2019/12/2.
//  Copyright © 2019年 abc. All rights reserved.
//

/*
 控件功能描述
 * 聊天室抽屉，根页面是CRDrawer，CRDrawer铺满整个当前视图,当抽屉完全消失的时候，该view才隐藏，然后removeFromSuperView；
 * 之上一层是 bgButton:UIButton，bgButton是铺满整个当前页面的视图,该view可以响应拖拽手势，从而相对CRDrawer改变frame，当bgButton的x 达到CRDrawer边缘时，CRDrawer隐藏然后移除；
 * bgButton之上是subviewsBGView，抽屉中实际显示的控件的父视图，width在初始化时被设置，CRDrawer在被初始化时设置width，width即subviewsBGView的宽度，抽屉上的实际显示的控件的宽度；
 * subviewsBGView之上放置实际显示的控件
 */

import UIKit
import SnapKit


protocol CRDrawerDelegate: NSObjectProtocol {
    /**分享今日输赢*/
    func shareGainsAndLossesToday(gainsLossesItem:CRUserInfoCenterSourceWinLostItem)
    /**刷新最新数据*/
    func refreshNewDataGainsAndLossess()
}

enum CRDrawerDirection {
    case CRDrawerDirectionLeft
    case CRDrawerDirectionRight
}

class CRDrawer: UIView {
    var userType = 1
    var drawerWidth:CGFloat = 0 //抽屉宽度，即subviewsBGView的宽度
    var animateDuration:TimeInterval = 0 //抽屉动画时间
    var drawDirection:CRDrawerDirection = .CRDrawerDirectionRight
    var drawerAlpha:CGFloat = 0
    var isOpen = false //抽屉打开状态
    var drawerSuperView:UIView?
    var drawerDelegate:CRDrawerDelegate?
    
//    var gainsLossesItem:CRUserInfoCenterSourceWinLostItem? 
    
    var closeHandler:(() -> Void)? //关闭抽屉，闭包开放到其他位置做相应处理
    
    //底部视图,用于响应抽屉手势拖拽
    lazy var bgButton:UIButton = {
        let view = UIButton()
        view.addTarget(self, action: #selector(tapAction), for: .touchUpInside)
       
        /*
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panAction))
        view.addGestureRecognizer(panGesture)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapAction))
        view.addGestureRecognizer(tapGesture)
 */
        
        return view
    }()
    
    //bgButton的subview，抽屉中实际显示的控件的父视图
    lazy var subviewsBGView:CRChatDrawer = {
        let view = CRChatDrawer.init(width: drawerWidth,userType:userType)
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 10.0)
        view.layer.shadowOpacity = 0.9
        view.layer.shadowRadius = 4.0 
        
        return view
    }()
    
    convenience init(userType:Int,superView:UIView,direction:CRDrawerDirection = .CRDrawerDirectionRight,width: CGFloat,alpha:CGFloat = 0.5,duration:TimeInterval) {
        self.init()
        self.userType = userType
        drawerAlpha = alpha
        drawerWidth = width
        drawDirection = direction
        animateDuration = duration
        drawerSuperView = superView
        
        drawerSuperView?.addSubview(self)
        self.addSubview(bgButton)
        bgButton.addSubview(subviewsBGView)
        self.backgroundColor = UIColor.black.withAlphaComponent(drawerAlpha)
        //刷新个人信息数据
        self.subviewsBGView.header.refreshButton.addTarget(self, action: #selector(refreshDate), for: .touchUpInside)
        //分享今日输赢
        self.subviewsBGView.header.shareButton.addTarget(self, action: #selector(shareGainsLosses), for: .touchUpInside)
        
        setupLayoutViews()
        
        subviewsBGView.setupData()
    }
    
    ///初始化视图布局
    func setupLayoutViews() {
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        
        bgButton.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.bottom.equalTo(0)
            make.left.equalTo(weakSelf.snp.right)
            make.width.equalTo(weakSelf.snp.width)
        }
        
        subviewsBGView.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.bottom.equalTo(weakSelf.bgButton).offset(0)
            make.width.equalTo(weakSelf.drawerWidth)
            
            if weakSelf.drawDirection == .CRDrawerDirectionRight {
                make.right.equalTo(weakSelf.bgButton.snp.right)
            }else {
                make.left.equalTo(weakSelf.bgButton.snp.left)
            }
        }
    }
   
    ///打开关闭抽屉更新布局
    func openDrawer(open:Bool) {
        isOpen = open
        
        //根据打开和关闭抽屉，添加和移除，显示隐藏 视图
        if self.isOpen {
            self.isHidden = false
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + animateDuration) {
                self.isHidden = true
                self.removeFromSuperview()
            }
        }
        
        //根据打开和关闭抽屉，视图布局
        if isOpen {
            bgButton.snp.remakeConstraints {[weak self] (make) in
                guard let weakSelf = self else {return}
                make.top.bottom.equalTo(0)
                make.left.equalTo(weakSelf.snp.left)
                make.width.equalTo(weakSelf.snp.width)
            }
        }else {
            bgButton.snp.remakeConstraints {[weak self] (make) in
                guard let weakSelf = self else {return}
                make.top.bottom.equalTo(0)
                make.left.equalTo(weakSelf.snp.right)
                make.width.equalTo(weakSelf.snp.width)
            }
        }
        
        var realDuration = animateDuration
        if isOpen {
            realDuration = animateDuration * 0.5
        }
        
        UIView.animate(withDuration: realDuration) {
            self.layoutIfNeeded()
        }
    }
    
    //MARK: - 事件
    //点击
    @objc func tapAction() {
        closeAction()
    }
    
    func closeAction() {
        openDrawer(open: false)
        closeHandler?()
    }
    
    //拖拽
    @objc private func panAction(gesture: UIPanGestureRecognizer) {
//        if gesture.state == UIGestureRecognizer.State.began || gesture.state == UIGestureRecognizer.State.changed
//        {
//            
//        }else if gesture.state == UIGestureRecognizer.State.ended
//        {
//
//        }
//
//        gesture.setTranslation(CGPoint.zero, in: self)
    }
    


}

//MARK:事件响应
extension CRDrawer{
    //刷新数据
     @objc func refreshDate(){
        drawerDelegate?.refreshNewDataGainsAndLossess()
//        closeAction()
    }
    //分享输赢
     @objc func shareGainsLosses(){
        drawerDelegate?.shareGainsAndLossesToday(gainsLossesItem: self.subviewsBGView.header.gainsLossesItem ?? CRUserInfoCenterSourceWinLostItem())
        closeAction()
    }
}
