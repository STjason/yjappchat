//
//  CRDrawerUserCell.swift
//  gameplay
//
//  Created by admin on 2019/12/6.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class CRDrawerUserCell: UITableViewCell {
    var clickPrivateChatHandler:((_ model:CROnLineUserModel, _ type:CRChatDrawerSectionType) -> Void)?
    var userType = 1 //1普通玩家，2后台管理员，3机器人，4前台管理员 5前台普通用户（后台试玩 暂时没用到）
    var userModel = CROnLineUserModel()
    var cellType = CRChatDrawerSectionType.DrawerSectionManager //默认为管理员列表
    var avatarDiameter:CGFloat = 40 //头像直径
    var unReadNumWidth:CGFloat = 25 //未读数btn width
    
    lazy var avatara:UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = avatarDiameter * 0.5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var name:UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.systemFont(ofSize: 17)
        view.textAlignment = .left
        return view
    }()
    
    lazy var chatButton:UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "cr_chatIcon"), for: .normal)
        view.contentMode = .scaleAspectFill
        view.setTitleColor(UIColor.white, for: .normal)
        view.addTarget(self, action: #selector(tapChatAction), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    lazy var unReadNumBtn:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.red
        view.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        view.setTitleColor(UIColor.white, for: .normal)
        view.isHidden = true
        view.isUserInteractionEnabled = false
        view.layer.cornerRadius = unReadNumWidth * 0.5
        return view
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.backgroundColor = UIColor.init(hexString: "#0E0F16")?.withAlphaComponent(0.5)
        
        addSubview(avatara)
        addSubview(name)
        addSubview(chatButton)
        addSubview(unReadNumBtn)
        
        avatara.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.height.equalTo(avatarDiameter)
        }
        
        name.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(avatara.snp.right).offset(15)
            make.right.equalTo(chatButton.snp.left).offset(-5)
        }
        
        chatButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-8)
            make.width.height.equalTo(25)
        }
        
        unReadNumBtn.snp.makeConstraints { (make) in
            make.center.equalTo(chatButton.snp.center)
            make.size.equalTo(CGSize.init(width: unReadNumWidth, height: unReadNumWidth))
        }
    }

    func configWith(model: CROnLineUserModel,type:CRChatDrawerSectionType,userType:Int) {
        userModel = model
        cellType = type
        self.userType = userType
        
        let urlString = !model.avatar.isEmpty ? model.avatar : model.avatarUrl
        if let url = URL.init(string: urlString) {
            avatara.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage.init(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }else {
            avatara.image = UIImage.init(named: "chatroom_default")
        }
        
        if !model.nickName.isEmpty {
            name.text = model.nickName
        }else {
            if model.account.contains("*") {
                name.text = model.account
            }else {
                let account = model.account + ""
                var subStr = account.subString(start: 2, length: account.length - 4)
                subStr = account.replacingOccurrences(of: subStr, with: "**")
                name.text = subStr
            }
        }
        
        if type == .DrawerSectionPrivateChat {
            chatButton.isHidden = true
        }else if type == .DrawerSectionManager {
            if privatePermission {
                chatButton.isHidden = false
            }else {
                //对方是管理员时，只要且必须管理员有权限 (和自己是否有权限没有关系)
                chatButton.isHidden = !model.privatePermission
            }
        }else if type == .DrawerSectionOnlineUser { //对方不是管理员
            chatButton.isHidden = !privatePermission
        }
        
        chatButton.isHidden = isEmptyString(str: model.id)
        var hideUnRead = true
        if let unReadValue = Int(model.isRead) {
            hideUnRead = unReadValue == 0
            var countStr = "\(unReadValue)"
            if unReadValue > 99 {
                countStr = "99+"
            }
            unReadNumBtn.setTitle(countStr, for: .normal)
        }else {
            hideUnRead = true
        }
        
        unReadNumBtn.isHidden = hideUnRead
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func tapChatAction() {
        clickPrivateChatHandler?(userModel,cellType)
    }

}
