//
//  CRChatDrawer.swift
//  gameplay
//
//  Created by admin on 2019/12/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit

enum CRChatDrawerSectionType {
    case DrawerSectionManager //管理员
    case DrawerSectionOnlineUser //在线用户
    case DrawerSectionPrivateChat //私聊
}

class CRChatDrawer: UIView {
    var isSearchStatus = false //显示的是搜索的数据，还是所有数据
    var insidePrivateChatHandler:((_ model:CRPrivateChatRoomList) -> Void)?
    var clickPrivateChatHandler:((_ model:CROnLineUserModel, _ type:CRChatDrawerSectionType) -> Void)? //在线用户私聊
    var requestOnLineListHandler:(() -> Void)? //为各组请求数据
    var updateUnreadMsgNum:((_ unReadNum:Int) -> Void)? //更新未读私聊消息数
    
    var userIconLists:[String] = [] //用户图像列表
    var totalRealUserListCount = 0 //真实用户数据的条数
    let noMoreUsersTip = "没有更多数据"
    var drawerWidth:CGFloat = 0
    var userType = 1 //默认普通用户
    lazy var openArray:NSMutableArray=NSMutableArray()//保存各分组的开闭状态，若数组中包含分组section，则说明该分组是展开的
    var onlineList: [CROnLineUserModel]? { //在线用户列表
        didSet {
            handleSectionData()
            tableView.reloadData()
        }
    }
    
    var allOnLineList = [CROnLineUserModel]() //所有在线用户
        
    var fakeOnlineList = [CROnLineUserModel]() //在线用户假数据
    
    var managerList: [CROnLineUserModel]? { //管理员列表
        didSet {
            handleSectionData()
            tableView.reloadData()
        }
    }
    
    var privateChatList: [CRPrivateChatRoomList]? { //私聊列表
        didSet {
            if let wraperList = privateChatList {
                var localUnReaedInTotal = 0
                
                for wraperModel in wraperList {
                    if let unReadNum = Int(wraperModel.isRead) {
                        localUnReaedInTotal += unReadNum
                    }
                }
                
                unReaedInTotal = localUnReaedInTotal
            }
            
            tableView.reloadData()
        }
    }
    
    // 组消息:头部标题, icon, 类型（管理员，在线用户,私聊）
    var headerTuples = [(String,String,CRChatDrawerSectionType)]()
    ///未读总数
    var unReaedInTotal = 0 { //消息未读总数
        didSet {
            //根据 未读消息数更新到聊天室导航栏右侧按钮的 状态
            updateUnreadMsgNum?(unReaedInTotal)
        }
    }
    
    //顶部个人信息栏
    lazy var header:CRChatDrawerHeader = {
        let view = CRChatDrawerHeader.init(width: drawerWidth)
        let height:CGFloat = showOneLineHeaderCollection() ? 290 : 350
        view.frame = CGRect.init(x: 0, y: 0, width: drawerWidth, height: (height + kStatusHeight))
        view.layer.cornerRadius = 3
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            
        return view
    }()
    
    func showOneLineHeaderCollection() -> Bool {
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return false
        }
        
        var count = 0
        
        if source.switch_bet_num_show == "1"{
            count += 1
        }
        
        if source.switch_yingkui_show ==  "1" {
            count += 1
        }
        
        if source.switch_sum_recharge_show == "1" {
            count += 1
        }
        
        if source.switch_today_recharge_show == "1" {
            count += 1
        }
        
        return count <= 1
    }

    
    //  懒加载
    lazy var tableView:UITableView = {
        let view:UITableView = UITableView.init(frame: .zero, style: UITableView.Style.grouped)
        view.delegate = self
        view.dataSource = self
        view.separatorInset = .zero
        view.bounces = false
        view.separatorColor = UIColor.init(hexString: "#A9A9A9")
        view.tableHeaderView = header
        view.tableFooterView = UIView()
        view.estimatedRowHeight = 50
        view.rowHeight = UITableView.automaticDimension
        view.keyboardDismissMode = .onDrag
        view.estimatedRowHeight = 50
        view.register(CRDrawerUserCell.self, forCellReuseIdentifier: "CRDrawerUserCell")
        view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        view.showsVerticalScrollIndicator = false
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tapTableView))
        view.addGestureRecognizer(gesture)
        return view
    }()

    @objc func tapTableView() {
        self.endEditing(true)
    }
    
    convenience init(width:CGFloat,userType:Int) {
        self.init()
        drawerWidth = width
        self.userType = userType
        
        setupUI()
    }
    
    func setupData() {
        requestOnLineListHandler?()
    }
    
    func setupUI() {
        addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(-kStatusHeight)
            make.left.right.bottom.equalTo(0)
        }
    }
    
    /// 在线用户，管理员列表
    func updateInfo(list: [CROnLineUserModel]?,type:CRChatDrawerSectionType) {
        if type == .DrawerSectionOnlineUser {
            
            if let wraperList = list {
                totalRealUserListCount = wraperList.count
                var totalCount = totalRealUserListCount + getFakeUserTotalCount()
                if totalCount < 10{
                    totalCount = 10
                }
                let addFakeList = wraperList + judgeFakeDataLoad(firstLoad:true,currentCount: wraperList.count, totoalCount: totalCount)
                onlineList = addFakeList
                allOnLineList = (onlineList ?? [CROnLineUserModel]()) + [CROnLineUserModel]()
                handleSectionData()
            }
            
        }else if type == .DrawerSectionManager {
            managerList = list
            handleSectionData()
        }
    }
    
    ///私聊列表
    func updatePrivateChatWith(list:[CRPrivateChatRoomList]?) {
        privateChatList = list
    }
    
    ///处理组数据。管理员，在线用户,或者其他
    func handleSectionData() {
        headerTuples.removeAll()
        headerTuples.append(("管理员列表","chat_drawer_manager",.DrawerSectionManager))
        
        
        //管理员才可以查看 在线用户判断
        if !switchOnlyManagerCanWatchOnlineUser() || (userType == 2 || userType == 4) { //2 后台管理员 4前台管理员
            var  onLineCount = totalRealUserListCount + getFakeUserTotalCount()
            if onLineCount < 10{
                onLineCount = 10
            }
            headerTuples.append(("在线用户-\(onLineCount)人","chat_drawer_onlineUser", .DrawerSectionOnlineUser))
        }
    
//        if privatePermission {
            headerTuples.append(("私聊列表","chat_drawer_priChat",.DrawerSectionPrivateChat))
//        }
    }
    
    //MARK: 根据当前已经展示的用户数，和假数据加上真数据的总量判断，来处理假用户数据的加载(真数据一次加载完毕)
    func judgeFakeDataLoad(firstLoad:Bool = false,currentCount:Int,totoalCount:Int,pageCount:Int = 10) -> [CROnLineUserModel] {
        if totoalCount <= currentCount && !firstLoad { //没有更多数据 && 不是第一次加载，才提示没有更多数据
            showToast(view: self, txt: noMoreUsersTip)
            return [CROnLineUserModel]()
        }else { //有更多数据
            
            let remainCount = totoalCount - currentCount
            var loadCount = 0
            
            if firstLoad {
                if currentCount < pageCount { //真实数据小于 一页应展示数据
                    if pageCount - currentCount <= remainCount { //剩下的数据足够补足 真实数据
                        loadCount = pageCount - currentCount
                    }else {
                        loadCount = remainCount
                    }
                }
            }else {
                loadCount = pageCount >= remainCount ? remainCount : pageCount
            }
            
            return getFakeOnlineUserModel(count: loadCount)
        }
    }
    
    ///用户列表 假用户数据总数
    func getFakeUserTotalCount() -> Int {
        let  chatRoomConfig = getChatRoomSystemConfigFromJson()
        return chatRoomConfig?.source?.name_room_people_num ?? 0
    }
    
    //MARK: 获取在线用户假数据
    func getFakeOnlineUserModel(count:Int) -> [CROnLineUserModel] {
        var items = [CROnLineUserModel]()
        for _ in 0..<count {
            let model = CROnLineUserModel()
            
            let length = arc4random() % 3 + 5
            model.account = randomStr(len: Int(length))
            
            var randomAvatar = ""
            if userIconLists.count > 0 {
                let randomAvatarIndex = Int(arc4random() % UInt32(userIconLists.count))
                randomAvatar = userIconLists[randomAvatarIndex]
            }
            
            model.avatar = randomAvatar
            
            items.append(model)
        }
        
        return items
    }
    
    //获取随机用户名
    func randomStr(len : Int) -> String{
        let random_str_characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        var ranStr = ""
        for _ in 0..<len {
            let index = Int(arc4random_uniform(UInt32(random_str_characters.count)))
            ranStr.append(random_str_characters[random_str_characters.index(random_str_characters.startIndex, offsetBy: index)])
        }
        
//        //随机添加 ***
//        let insertPositon = arc4random() % UInt32(len)
//        ranStr.asInsert(string: "***", ind: Int(insertPositon))
        
        return ranStr
    }
    
    //收到消息之后，更新未读消息数
    func updateUnreadNum(msg:CRMessage) {
        //处理
        if let list = privateChatList {
            let stationId = msg.stationId
            
            var localUnreadNum = unReaedInTotal
            
            for model in list {
                if model.roomId == msg.roomId || (model.roomId + "_" + stationId == msg.roomId) {
                    localUnreadNum += 1
                    
                    if let unReadNum = Int(model.isRead) {
                        model.isRead = "\(unReadNum + 1)"
                    }
                    
                    UIView.performWithoutAnimation {
                        tableView.reloadData()
                    }
                }
            }
            
            unReaedInTotal = localUnreadNum
        }
    }
}


extension CRChatDrawer: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tuple = headerTuples[indexPath.section]
        
        if tuple.2 == .DrawerSectionPrivateChat {
            guard let list = privateChatList else {return}
            
            let model = list[indexPath.row]
            insidePrivateChatHandler?(model) //进入私聊房间
        }
    }
}

extension CRChatDrawer: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CRDrawerUserCell") as? CRDrawerUserCell else {
            fatalError("dequeueReusableCell CRDrawerUserCell failure")
        }
        
        cell.selectionStyle = .none
        
        let tuple = headerTuples[indexPath.section]
        var userModel:CROnLineUserModel?
        var cellType:CRChatDrawerSectionType = .DrawerSectionOnlineUser
        
        if tuple.2 == .DrawerSectionManager { //管理员
            if let list = managerList {
                let model = list[indexPath.row]
                userModel = model
                cellType = .DrawerSectionManager
            }

        }else if tuple.2 == .DrawerSectionOnlineUser { //在线用户
            if let list = onlineList {
                let model = list[indexPath.row]
                userModel = model
                cellType = .DrawerSectionOnlineUser
            }
        }else if tuple.2 == .DrawerSectionPrivateChat { //私聊
            guard let wraperList = privateChatList else {
                return cell
            }
            
            let wraperModel = wraperList[indexPath.row]
            if let model = wraperModel.fromUser {
                model.isRead = wraperModel.isRead
                userModel = model
                cellType = .DrawerSectionPrivateChat
            }
        }
        
        if let model = userModel {
            cell.configWith(model: model,type: cellType,userType:userType)
        }
        
        cell.clickPrivateChatHandler = {[weak self] (model,sectionType) in //点击会话
            guard let weakSelf = self else {return}
            weakSelf.clickPrivateChatHandler?(model,sectionType)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //如果数组中包含当前的section则返回数据源中当前section的数组大小，否则返回零
        if(self.openArray.contains(section)){
            
            let tuple = headerTuples[section]
            
            if tuple.2 == .DrawerSectionManager { //管理员
                return managerList?.count ?? 0
            }else if tuple.2 == .DrawerSectionOnlineUser { //在线用户
                return onlineList?.count ?? 0
            }else if tuple.2 == .DrawerSectionPrivateChat { //私聊
                return privateChatList?.count ?? 0
            }
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerTuples.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let tuple = headerTuples[section]
        if tuple.2 == .DrawerSectionOnlineUser && openArray.contains(section) {
            return (40 + 30)
        }else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let tuple = headerTuples[section]

        if tuple.2 == .DrawerSectionOnlineUser && openArray.contains(section) {
            return 40
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var height:CGFloat = 40
        var bottomHeight:CGFloat = 0
        let tuple = headerTuples[section]
        if tuple.2 == .DrawerSectionOnlineUser && openArray.contains(section) {
            bottomHeight = 30
            height += bottomHeight
        }
        
        let header = CRDrawerSectionHeader.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: height), bottomHeight: bottomHeight)
        header.setupSection(section: section)//绑定section,方便标识子项的开闭状态
        header.headerDelegate = self
        
        if bottomHeight != 0 {
            header.textChange = {[weak self] (text) in
                guard let weakSelf = self else {return}
                weakSelf.searchUserWith(text: text)
            }
            
            if !isSearchStatus && !(header.searchBar.text ?? "").isEmpty {
                header.searchBar.text = ""
            }
        }

        header.labelTV.text = tuple.0
        header.imgTV.image = UIImage.init(named: tuple.1)
        
        if tuple.2 == .DrawerSectionPrivateChat {
            header.unReadDot.isHidden = unReaedInTotal == 0
        }else {
            header.unReadDot.isHidden = true
        }
        
        let isExpand = self.openArray.contains(section)
        header.updateIndictor(expand: isExpand,index:section)
        return header
    }
    
    //MARK: 搜索用户
    func searchUserWith(text:String) {
        let lowerText = text.lowercased()
        if !text.isEmpty {
            if let users = onlineList {
                var results = [CROnLineUserModel]()
                for user in users {
                    var showName = ""
                    if !user.nickName.isEmpty {
                        showName = user.nickName
                    }else {
                        showName = user.account
                    }
                    
                    showName = showName.lowercased()
                    
                    if showName.contains(lowerText) {
                        results.append(user)
                    }
                }
                
                isSearchStatus = true
                onlineList = results
            }
        }else {
            isSearchStatus = true
            onlineList = allOnLineList
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 41))
        footer.backgroundColor = UIColor.clear
        
        let loadButton = UIButton.init(frame: footer.frame)
        loadButton.backgroundColor = UIColor.clear
        if isSearchStatus {
            loadButton.setTitle("显示所有用户", for: .normal)
        }else {
            loadButton.setTitle("点击加载更多", for: .normal)
        }
        
        loadButton.setTitleColor(UIColor.groupTableViewBackground, for: .normal)
        loadButton.addTarget(self, action: #selector(clickLoadMoreAction), for: .touchUpInside)
        
        footer.addSubview(loadButton)
        return footer
    }
    
    @objc func clickLoadMoreAction() {
        if isSearchStatus {
            isSearchStatus = false
            onlineList = allOnLineList
            tableView.reloadData()
        }else {
            var totalCount = totalRealUserListCount + getFakeUserTotalCount()
            if totalCount < 10 {
                totalCount = 10
            }
            if let wraperList = onlineList {
                let fakeList = judgeFakeDataLoad(currentCount: wraperList.count, totoalCount: totalCount)
                if fakeList.count > 0 {
                    let addFakeList = wraperList + fakeList
                    onlineList = addFakeList
                }
            }
        }
    }
    
}

//点击section
extension CRChatDrawer:CRDrawerHeaderDelegate {
    func onHeaderClick(section: Int) {
        //判断是否是张开或合住
        if(openArray.contains(section)) {
            openArray.remove(section)
        }else {
            openArray.add(section)
        }
        self.tableView.reloadSections(IndexSet.init(integer: section), with: .fade)
    }
}
