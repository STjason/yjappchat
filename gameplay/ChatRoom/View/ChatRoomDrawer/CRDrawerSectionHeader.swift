//
//  CRDrawerSectionHeader.swift
//  gameplay
//
//  Created by admin on 2019/12/5.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit


protocol CRDrawerHeaderDelegate {
    func onHeaderClick(section:Int)
}

class CRDrawerSectionHeader: UIView {
    var textChange:((_ text:String) -> Void)?
    var imgTV:UIImageView!
    var labelTV:UILabel!
    var indictor:UIImageView!
    var searchBar:UISearchBar!
    var headerSection = 0
    var topLine = UIView() //顶部分割线,第一个section的线要隐藏
    
    let unReadDotWidth:CGFloat = 12
    lazy var unReadDot:UIButton = { //未读消息红点
        let view = UIButton()
        view.backgroundColor = UIColor.red
        view.layer.cornerRadius = unReadDotWidth * 0.5
        return view
    }()
    
    var sectionIndex = 0 { //是第几组的section
        didSet {
            topLine.isHidden = sectionIndex == 0
        }
    }
    
    var headerDelegate:CRDrawerHeaderDelegate?
    
    convenience init(frame: CGRect,bottomHeight:CGFloat) {
        self.init()
        self.frame = frame
        self.backgroundColor = UIColor.init(hexString: "#343434")
       
        imgTV = UIImageView.init(frame: CGRect.init(x: 10, y: (self.bounds.height - bottomHeight)/2-10, width: 20, height: 20))
        labelTV = UILabel.init(frame: CGRect.init(x: 40, y: (self.bounds.height - bottomHeight)/2-10, width: self.bounds.width - 40, height: 20))
        labelTV.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        labelTV.textColor = UIColor.white
        
        indictor = UIImageView.init(frame: CGRect.init(x: self.bounds.width - 40, y: (self.bounds.height - bottomHeight)/2-2, width: 12, height:8))
        indictor.image = UIImage.init(named: "arrow_down")
        
        searchBar = UISearchBar.init(frame: CGRect.init(x: 0, y: frame.height - bottom, width: frame.width, height: bottomHeight))
        
        if bottomHeight != 0 {
//            searchBar.backgroundColor = UIColor.clear
            searchBar.barTintColor = UIColor.clear
            searchBar.delegate = self
            if #available(iOS 13.0, *) {
                self.searchBar.searchTextField.backgroundColor = UIColor.clear
                self.searchBar.searchTextField.textColor = UIColor.white
            }else {
                if let textField = searchBar.value(forKey: "_searchField") as? UITextField {
                    textField.backgroundColor = UIColor.clear
                    textField.textColor = UIColor.white
                }
            }
              
            let y = (self.bounds.height - bottomHeight)/2 - 5
            searchBar.frame = CGRect.init(x: 0, y: (y + 20), width: self.bounds.width, height: bottomHeight)
            searchBar.placeholder = "通过名字查找"
        }
        
        topLine.backgroundColor = UIColor.init(hexString: "#A9A9A9")
        
        addSubview(imgTV)
        addSubview(labelTV)
        addSubview(unReadDot)
        addSubview(indictor)
        addSubview(searchBar)
        addSubview(topLine)
        searchBar.layer.masksToBounds = true
        
        searchBar.isHidden = bottomHeight == 0
        
        topLine.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        unReadDot.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(108)
            make.size.equalTo(CGSize.init(width: unReadDotWidth, height: unReadDotWidth))
        }

        let tap = UITapGestureRecognizer.init(target: self, action: #selector(onHeaderClickEvent(_:)))
        self.addGestureRecognizer(tap)
    }
    
    func setupSection(section:Int) -> Void {
        self.headerSection = section
    }
    
    func updateIndictor(expand:Bool,index:Int) -> Void {
        sectionIndex = index
        
        if expand{
            indictor.image = UIImage.init(named: "arrow_up")
        }else{
            indictor.image = UIImage.init(named: "arrow_down")
        }
    }
    
    @objc func onHeaderClickEvent(_ recongnizer: UIPanGestureRecognizer) {
        if let delegate = self.headerDelegate{
            delegate.onHeaderClick(section: headerSection)
        }
    }
}


extension CRDrawerSectionHeader:UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text = searchBar.text ?? ""
        self.textChange?(text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.endEditing(true)
    }
}
