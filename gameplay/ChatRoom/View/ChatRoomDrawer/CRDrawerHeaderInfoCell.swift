//
//  CRDrawerHeaderInfoCell.swift
//  gameplay
//
//  Created by admin on 2019/12/5.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit

class CRDrawerHeaderInfoCell: UICollectionViewCell {
    var valueLabel = UILabel()
    var tipsLabel = UILabel()
//    var verticalLine = UIView()
    
    var userInfoItem:CRUserInfoItem?{
        didSet{
            guard let infoItem = userInfoItem else {
                return
            }
            
            tipsLabel.text = infoItem.titleName
            valueLabel.text = String(format:"%.2f",infoItem.money)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.backgroundColor = UIColor.clear
//        verticalLine.backgroundColor = UIColor.white
        
        valueLabel.numberOfLines = 0
        valueLabel.text = "0"
        tipsLabel.text = "今日总盈亏"
        valueLabel.textColor = UIColor.init(hexString: "#ff9400")
        tipsLabel.textColor = UIColor.lightGray
        valueLabel.font = UIFont.systemFont(ofSize: 13)
        tipsLabel.font = UIFont.systemFont(ofSize: 13)
        valueLabel.textAlignment = .center
        tipsLabel.textAlignment = .center
        
        addSubview(valueLabel)
        addSubview(tipsLabel)
//        addSubview(verticalLine)
        
        valueLabel.snp.makeConstraints {[weak self] (make) in
            guard let weakSelf = self else {return}
            make.top.left.right.equalTo(0)
            make.bottom.equalTo(weakSelf.tipsLabel.snp.top)
        }
        
        tipsLabel.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(20)
        }
        
//        verticalLine.snp.makeConstraints {[weak self] (make) in
//            guard let weakSelf = self else {return}
//            make.right.equalTo(weakSelf.snp.right).offset(-1)
//            make.width.equalTo(0.5)
//            make.top.equalTo(3)
//            make.bottom.equalTo(-3)
//        }
    }
    
    func configWith(model:CRUserInfoItem) {
        
    }
}




