//
//  CRLotteryListView.swift
//  gameplay
//
//  Created by JK on 2019/12/4.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CRLotteryListView: UIView {

    /** 回执 选中的彩种index */
    var selectLotteryIndexHandle: ((_ selectLotteryIndex: Int) -> Void)?
    var isShow = false
    var hiddenFrame = CGRect.zero //视图隐藏时的frame
    var showFrame = CGRect.zero //视图显示时的frame
    //collection相关
    var numsInHorizontal = 3 //每行个数
    var minimumLine:CGFloat = 10 //垂直间距
    var minimumInteritem:CGFloat = 10 //水平间距
    var sectionMargin:CGFloat = 15
    var itemHeight:CGFloat = 44
    var itemModels = [CRLotteryListModel]()
    /** 彩种选择 默认第一个 */
    var lotterySelectIndex = 0
    
    lazy var lotteryListView: UICollectionView = {
        
        //每个item的宽
        var itemWidth = (kScreenWidth - 3 * sectionMargin - CGFloat(numsInHorizontal - 1) * minimumInteritem) / CGFloat(numsInHorizontal)
        
        var layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: sectionMargin, left: sectionMargin, bottom: sectionMargin, right: sectionMargin)
        layout.minimumLineSpacing = minimumLine
        layout.minimumInteritemSpacing = minimumInteritem
        layout.itemSize = CGSize.init(width: itemWidth, height: itemHeight)
        
        let listView = UICollectionView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: 1), collectionViewLayout: layout)
        listView.delegate = self
        listView.dataSource = self
        listView.backgroundColor = UIColor.clear
        listView.register(CRLotteryListViewCell.self, forCellWithReuseIdentifier: "lotteryListViewCell")
        
        return listView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        
        addSubview(lotteryListView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getCurrentListData(listData: [CRLotteryListModel]) {
        itemModels = listData
        lotteryListView.reloadData()
        
        self.refreshHeight()
    }
    
    func refreshHeight() {
        lotteryListView.layoutIfNeeded()
        let contentContentSize = lotteryListView.contentSize;
        showFrame = kCGRect(x: 0, y: 0, width: contentContentSize.width, height: contentContentSize.height)
        self.frame = kCGRect(x: 0, y: 0, width: kScreenWidth, height: 0)
        lotteryListView.frame = showFrame
    }
    
    ///显示或者隐藏视图
    func show() {
        let frame = self.isShow ? kCGRect(x: 0, y: 0, width: kScreenWidth, height: 0) : self.showFrame
        self.isShow = !self.isShow
        
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = frame
        }) { (complete) in
        }
    }
    
    func hideView() {
        let frame = kCGRect(x: 0, y: 0, width: kScreenWidth, height: 0)
        self.isShow = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = frame
        }) { (complete) in
        }
    }
}

extension CRLotteryListView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.lotterySelectIndex = indexPath.row
        self.selectLotteryIndexHandle!(indexPath.row)
        
        self.lotteryListView.reloadData()
    }
}

extension CRLotteryListView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lotteryListViewCell", for: indexPath) as? CRLotteryListViewCell else {
            fatalError("dequeueReusableCell")
        }
        cell.getCurrentModel(model: itemModels[indexPath.row], isSelect: lotterySelectIndex == indexPath.row)
        
        return cell
    }
    
    
}
