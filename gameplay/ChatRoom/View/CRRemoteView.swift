//
//  CRRemoteView.swift
//  gameplay
//
//  Created by admin on 2019/12/11.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

protocol CRRemoteViewDelegate: NSObjectProtocol {
    func remoteViewClick(type:CRRemoteViewType)
}

enum CRRemoteViewType {
    case CRRemoteViewTypeLotteryList //中奖榜单
    case CRRemoteViewTypeSignIn //签到
    case CRRemoteViewTypeOnlineService //在线客服
    case CRRemoteViewTypeMasterPlan //导师计划
    case CRRemoteViewTypeLongDragon //长龙
}

class CRRemoteView: UIView {
    var bgView = UIView()
    var animationTime:TimeInterval = 0.5
    var delegate:CRRemoteViewDelegate?
    private var floatBtnsTuple = [(String,String,CRRemoteViewType)]() //每个btn设置数据
    var baseTag = 100*100 //为btn设置tag
    var itemWidth:CGFloat = 40
    var itemHeight:CGFloat = 40
    var marginToScreen:CGFloat = 10
    var marginsInButtons:CGFloat = 5 //按钮竖直间距
    var originalY:CGFloat = 0 //遥控器弹出按钮视图的Y
    var originalX:CGFloat = 0
    var remoterOrigianY:CGFloat = 0 //遥控器的Y，不是遥控器弹出按钮视图的Y
    var showFrame = CGRect.zero
    var hideFrame = CGRect.zero
    
    convenience init(items:[(String,String,CRRemoteViewType)],locationY:CGFloat) {
        self.init()
        
        floatBtnsTuple = items
        self.isHidden = true
        self.clipsToBounds = true
        
        remoterOrigianY = locationY
        let remoterHeight = CGFloat(floatBtnsTuple.count) * (itemHeight + marginsInButtons)
        floatBtnsTuple = items
        originalX = kScreenWidth - itemWidth - marginToScreen
        originalY = locationY - remoterHeight
        
        self.frame = CGRect.init(x: originalX, y: originalY, width: itemWidth, height: remoterHeight)
        showFrame = self.bounds
        hideFrame = CGRect.init(x: 0, y: remoterHeight, width: itemWidth, height: remoterHeight)
        
        setupView(locationY:locationY)
    }
    
    func setupView(locationY:CGFloat) {
        
        bgView.frame = hideFrame
        
        self.addSubview(bgView)
        
        for (index,model) in floatBtnsTuple.enumerated() {
            let btn = UIButton()
            btn.tag = baseTag + index
            btn.setImage(UIImage.init(named: model.1), for: .normal)
            
            if model.1.isEmpty {
                btn.setTitle(model.0, for: .normal)
                btn.setTitleColor(UIColor.red, for: .normal)
                btn.backgroundColor = UIColor.lightGray
            }

            btn.addTarget(self, action: #selector(clickBtn), for: .touchUpInside)
            let itemY = CGFloat(index) * itemHeight +  CGFloat(index) * marginsInButtons
            let frame = CGRect.init(x: 0, y: itemY, width: itemWidth, height: itemHeight)
            btn.frame = frame
            btn.layer.cornerRadius = itemWidth * 0.5
            btn.layer.masksToBounds = true
            
            bgView.addSubview(btn)
        }
    }
    
    @objc func clickBtn(sender:UIButton) {
        let index = sender.tag - baseTag
        let item = floatBtnsTuple[index]
        if let delegate = self.delegate {
            delegate.remoteViewClick(type: item.2)
        }
    }
    
    //MARK: - 动画
    func showOrHideRemoterView() {
        if self.isHidden {
            showRemoterview()
        }else {
            hideRemoterview()
        }
    }
    
    func showRemoterview() {
        self.isHidden = false
        
        UIView.animate(withDuration: animationTime) {
            self.bgView.frame = self.showFrame
        }
    }
    
    func hideRemoterview() {
        
        UIView.animate(withDuration: animationTime) {
            self.bgView.frame = self.hideFrame
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + animationTime) {
            self.isHidden = true
        }
    }
}

