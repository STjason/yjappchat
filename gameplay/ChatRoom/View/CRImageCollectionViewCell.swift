//
//  CRImageCollectionViewCell.swift
//  gameplay
//
//  Created by JK on 2019/12/9.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CRImageCollectionViewCell: UICollectionViewCell {
    //显示缩略图
    var imageView:UIImageView = {
        let imgV = UIImageView()
        imgV.contentMode = .scaleAspectFill
        imgV.clipsToBounds = true
        return imgV
    }()
    
    //显示选中状态的图标
    var selectedIcon:UIImageView = {
        let selectV = UIImageView()
        
        return selectV
    }()
    
    override var isSelected: Bool {
        
        didSet{
            if isSelected {
                selectedIcon.image = UIImage(named: "hg_image_selected")
            }else{
                selectedIcon.image = UIImage(named: "hg_image_not_selected")
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView.frame = self.bounds
        selectedIcon.frame = kCGRect(x: self.frame.width - kCurrentScreen(x: 100), y: 0, width: kCurrentScreen(x: 100), height: kCurrentScreen(x: 100))
        
        addSubview(imageView)
        addSubview(selectedIcon)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //播放动画，是否选中的图标改变时使用
    func playAnimate() {
        //图标先缩小，再放大
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .allowUserInteraction,
                                animations: {
                                    UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2,
                                                       animations: {
                                                        self.selectedIcon.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                                    })
                                    UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.4,
                                                       animations: {
                                                        self.selectedIcon.transform = CGAffineTransform.identity
                                    })
        }, completion: nil)
    }
    
}
