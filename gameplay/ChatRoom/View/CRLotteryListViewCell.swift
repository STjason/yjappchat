//
//  CRLotteryListViewCell.swift
//  gameplay
//
//  Created by JK on 2019/12/4.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CRLotteryListViewCell: UICollectionViewCell {
    
    lazy var titleButton:UIButton = {
        var view = UIButton(type: .custom)
        view.frame = self.bounds
        view.isUserInteractionEnabled = false
        view.setTitleColor(UIColor.black, for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 40))
        view.setTitleColor(UIColor.black, for: .normal)
        view.clipsToBounds = true
        view.layer.cornerRadius = 3
        view.layer.borderWidth = 1
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getCurrentModel(model: CRLotteryListModel, isSelect: Bool) {
        titleButton.setTitle(model.lotteryName, for: .normal)
        titleButton.layer.borderColor = isSelect ? UIColor.red.cgColor : UIColor.lightGray.cgColor
        titleButton.setTitleColor(isSelect ? UIColor.red : UIColor.black, for: .normal)
    }
}
