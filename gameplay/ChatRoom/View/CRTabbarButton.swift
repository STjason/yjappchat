//
//  CRTabbarButton.swift
//  gameplay
//
//  Created by Gallen on 2019/12/2.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRTabbarButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.titleLabel?.textAlignment = .center
    }
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        
        let titleY = IS_IPHONE_X ? contentRect.size.height * 0.3 : contentRect.size.height * 0.65
        
        let titleW = contentRect.size.width
        let titleH = contentRect.height - titleY
        return CGRect(x: 0, y: titleY, width: titleW, height: titleH)
    }
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        var imageW = 32.0
        var imageH = 32.0
        
        if self.width < 32 {
            imageW = 0
            imageH = 0
        }
        
        print(self.width)
        let imageX = Double((self.width * 0.5) - CGFloat((imageW * 0.5)))
        return CGRect(x:imageX, y: IS_IPHONE_X ? 8 : 2, width: imageW, height: imageH)
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
