//
//  CRLongDragonCell.swift
//  YiboGameIos
//
//  Created by JK on 2020/4/30.
//  Copyright © 2020 com.lvwenhan. All rights reserved.
//

import UIKit
import Kingfisher

class CRLongDragonCell: UITableViewCell {
    /**彩种图像*/
    lazy var lotteryImageV:UIImageView = {
        let imageV = UIImageView()
        return imageV
    }()
    /**彩种名称*/
    lazy var lotteryNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize:14)
        return label
    }()
    /**彩种期号*/
    lazy var lotteryNumLabel:UILabel = {
        let label = UILabel()
        label.text = "20200430237"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    /**彩种中奖类型一*/
    lazy var lotteryResultLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
//        label.font = UIFont.systemFont(ofSize: 14)
        label.layer.cornerRadius = 2
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.colorWithRGB(r: 123, g: 129, b: 239, alpha: 1)
        return label
    }()
    /**彩种中奖类型二*/
    lazy var playNameLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
//        label.font = UIFont.systemFont(ofSize: 14)
        label.layer.cornerRadius = 2
        label.layer.masksToBounds = true
        label.backgroundColor = .lightGray
        return label
    }()
    /**连续出现期数*/
    lazy var continueStageLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
//        label.font = UIFont.systemFont(ofSize: 14)
        label.layer.cornerRadius = 2
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.colorWithRGB(r: 213, g: 67, b: 49, alpha: 1.0)
        return label
    }()
    lazy var underLineView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.colorWithHexString("#e9e9e9")
        return view
    }()
    
    var longDragonDetailItem:CRLongDragonList?{
        didSet{
            lotteryNameLabel.text = longDragonDetailItem?.lotteryName
            lotteryNumLabel.text = longDragonDetailItem?.lotteryNum
            lotteryResultLabel.text = longDragonDetailItem?.lotteryResult
            playNameLabel.text = longDragonDetailItem?.playName
            continueStageLabel.text = (longDragonDetailItem?.continueStage ?? "") + "期"
            let imageURL = URL(string: BASE_URL + PORT + "/native/resources/images/" + (longDragonDetailItem?.lotteryCode)! + ".png")
            if let url = imageURL{
                lotteryImageV.kf.setImage(with: ImageResource(downloadURL: url), placeholder: UIImage(named: "default_lottery"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        contentView.addSubview(lotteryImageV)
        contentView.addSubview(lotteryNameLabel)
        contentView.addSubview(lotteryResultLabel)
        contentView.addSubview(lotteryNumLabel)
        contentView.addSubview(playNameLabel)
        contentView.addSubview(continueStageLabel)
        contentView.addSubview(underLineView)

        lotteryImageV.snp.makeConstraints { (make) in
            make.left.equalTo(50)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.top.equalTo(5)
        }

        lotteryNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(lotteryImageV.snp.bottom)
            make.centerX.equalTo(lotteryImageV)
        }

        lotteryNumLabel.snp.makeConstraints { (make) in
            make.left.equalTo(lotteryImageV.snp.right).offset(45)
            make.top.equalTo(lotteryImageV).offset(5)
        }
        
        lotteryResultLabel.snp.makeConstraints { (make) in
            make.left.equalTo(lotteryNumLabel)
            make.bottom.equalTo(lotteryNameLabel).offset(-5)
        }

        playNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(lotteryResultLabel.snp.right).offset(5)
            make.centerY.equalTo(lotteryResultLabel)
        }
        
        continueStageLabel.snp.makeConstraints { (make) in
            make.left.equalTo(playNameLabel.snp.right).offset(5)
            make.centerY.equalTo(playNameLabel)
        }
        
        underLineView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(1)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


