//
//  CRLongDragonView.swift
//  YiboGameIos
//
//  Created by JK on 2020/4/30.
//  Copyright © 2020 com.lvwenhan. All rights reserved.
//

import UIKit

class CRLongDragonListView: UIView {
    
    lazy var backView:UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var longDragonListTableView:UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CRLongDragonCell.self, forCellReuseIdentifier: "CRLongDragonCell")
        
        return tableView
    }()
    //中奖标题View
    lazy var longDragonTitleView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.red
        return view
    }()
    
    /**中奖标题*/
    lazy var prizeTitle:UILabel = {
        let title = UILabel()
        title.text = "长龙"
        title.textColor = UIColor.white
        title.font = UIFont.systemFont(ofSize: 15)
        return title
    }()
    /**隐藏视图*/
    lazy var closeButton:UIButton = {
        let button = UIButton()
        button.setTitle("X", for: .normal)
        button.addTarget(self, action: #selector(closeWinPrizeListView), for: .touchUpInside)
        button.titleLabel?.textColor = UIColor.white
        return button
    }()
    
    var innerLongDragonLists:[CRLongDragonList]?{
        didSet{
            guard let dataSource = innerLongDragonLists else{
                return
            }
            if dataSource.count > 0 {
                longDragonListTableView.reloadData()
            }
        }
    }
    
    //    override init(frame: CGRect) {
    //        super.init(frame: frame)
    //        //标题
    //
    //    }
    init(frame: CGRect,longDragonLists:[CRLongDragonList]?) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(backView)
        
        longDragonTitleView.addSubview(prizeTitle)
        //关闭
        longDragonTitleView.addSubview(closeButton)
        
        backView.addSubview(longDragonTitleView)
        backView.addSubview(longDragonListTableView)
        
        backView.snp.makeConstraints { (make) in
            //            make.centerY.equalTo(self.snp.centerY)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(360)
            make.height.equalTo(280)
            make.centerY.equalTo(self.snp.centerY)
        }
        
        
        longDragonTitleView.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(backView)
            make.height.equalTo(44)
        }
        
        prizeTitle.snp.makeConstraints { (make) in
            make.centerX.equalTo(longDragonTitleView.snp.centerX)
            make.centerY.equalTo(longDragonTitleView.snp.centerY)
        }
        
        longDragonListTableView.snp.makeConstraints { (make) in
            make.top.equalTo(longDragonTitleView.snp.bottom)
            make.left.bottom.right.equalTo(backView)
        }
        
        closeButton.snp.makeConstraints { (make) in
            make.right.equalTo(longDragonTitleView).offset(-10)
            make.centerY.equalTo(longDragonTitleView.snp.centerY)
        }
        
        innerLongDragonLists = longDragonLists
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CRLongDragonListView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return innerLongDragonLists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let longDragonCell = tableView.dequeueReusableCell(withIdentifier: "CRLongDragonCell") as? CRLongDragonCell
        let longDragonItem = innerLongDragonLists![indexPath.row]
        longDragonCell?.longDragonDetailItem = longDragonItem
        return longDragonCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension CRLongDragonListView{
    @objc private func closeWinPrizeListView(){
        self.removeFromSuperview()
    }
}
