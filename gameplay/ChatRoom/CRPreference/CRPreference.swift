//
//  CRPreference.swift
//  gameplay
//
//  Created by Gallen on 2019/10/10.
//  Copyright © 2019年 yibo. All rights reserved.
//  聊天室的配置开关

import UIKit
import HandyJSON

class CRPreference{
    
    var name_red_bag_remark_info:String = ""
    
    ///前台投注开关
    var switch_front_bet_show:String = ""
    
    var name_vip_nick_name_update_num:String = ""
    
    var switch_lottery_result_default_type_show:String = ""
    
    var switch_room_show:String = ""
    
    var switch_room_voice:String = ""
    
    var name_word_color_info:String = ""
    
    var switch_check_in_show:String = ""
    /**追加在线人数*/
    var name_room_people_num:String = ""
    
    var switch_open_simple_chat_room_show:String = ""
    /**自定义用户名开关*/
    var switch_user_name_show:String = ""
    
    var switch_chat_word_size_show:Int = 0
    
    var switch_send_image_show:String = ""
    /**中奖榜单*/
    var switch_winning_list_show:String = ""
    
    var name_new_members_default_photo:String = ""
    
    var switch_front_admin_ban_send:String = ""
    /**等级图标显示开关*/
    var switch_level_ico_show:String = ""
    /**投注命中率 用户的胜率高于该胜率显示*/
    var name_user_win_tips_per:Float = 0
    
    var switch_level_show:String = ""
    /**在线人数只有管理员可见*/
    var switch_room_people_admin_show:String = ""
    /**聊天背景颜色选择*/
    var native_name_backGround_color_info:String = ""
    
    var switch_room_tips_show:String = ""
    /**开奖结果显示开关*/
    var switch_lottery_result_show:String = ""
    
    var switch_red_bag_remark_show:String = ""
    
    var switch_msg_tips_show:String = ""
    
    var switch_yingkui_show:String = ""
    /**发送红包开关*/
    var switch_red_bag_send:String = ""
    /**红包领取详情开关*/
    var switch_red_info:String = ""
    /**等级文字颜色选择*/
    
    var native_name_level_title_color_info:String = ""
    /**打码量*/
    var switch_bet_num_show:String = ""
    
    /**彩票计划开关*/
    var switch_plan_list_show = "1"
    
    static let CHATROOMCONFIG:String = "crconfig"
    //保存
    func saveObject(object:AnyObject){
        UserDefaults.standard.set(object, forKey: "")
    }
    /**保存聊天室开关配置*/
    static func saveChatRoomConfig(value:AnyObject) -> Void {
        UserDefaults.standard.set(value, forKey: CHATROOMCONFIG)
    }
    
    static func getChatRoomConfig() -> String {
        let value = UserDefaults.standard.string(forKey: CHATROOMCONFIG)
        if let v = value{
            return v
        }
        return ""
    }
}

