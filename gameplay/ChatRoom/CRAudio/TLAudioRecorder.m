
//
//  TLAudioRecorder.m
//  TLChat
//
//  Created by 李伯坤 on 16/7/11.
//  Copyright © 2016年 李伯坤. All rights reserved.
//

#import "TLAudioRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import "NSFileManager+Paths.h"
#import "NSTimer+Block.h"
#import "amrFileCodec.h"

#define     PATH_RECFILE        [[NSFileManager cachesPath] stringByAppendingString:@"/rec.wav"]
typedef void (^writeSuccessBlock)(BOOL success,NSData *amrData);

@interface TLAudioRecorder () <AVAudioRecorderDelegate>

@property (nonatomic, strong) AVAudioRecorder *recorder;

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) void (^volumeChangedBlock)(CGFloat valume);
@property (nonatomic, strong) void (^completeBlock)(NSString *path, CGFloat time);
@property (nonatomic, strong) void (^cancelBlock)();
@property (nonatomic ,copy)NSURL * voiceUrl;


@end

@implementation TLAudioRecorder

+ (TLAudioRecorder *)sharedRecorder
{
    static dispatch_once_t once;
    static TLAudioRecorder *audioRecorder;
    dispatch_once(&once, ^{
        audioRecorder = [[TLAudioRecorder alloc] init];
    });
    return audioRecorder;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
//        [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proximityStateChanged:) name:UIDeviceProximityStateDidChangeNotification object:nil];
    }
    return self;
}

- (void)startRecordingWithVolumeChangedBlock:(void (^)(CGFloat volume))volumeChanged
                               completeBlock:(void (^)(NSString *path, CGFloat time))complete
                                 cancelBlock:(void (^)())cancel;
{
    self.volumeChangedBlock = volumeChanged;
    self.completeBlock = complete;
    self.cancelBlock = cancel;
    if ([[NSFileManager defaultManager] fileExistsAtPath:PATH_RECFILE]) {
        [[NSFileManager defaultManager] removeItemAtPath:PATH_RECFILE error:nil];
    }
    [self.recorder prepareToRecord];
    [self.recorder record];
    
    if (self.timer && self.timer.isValid) {
        [self.timer invalidate];
    }
    __weak typeof(self) weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 block:^(NSTimer *timer) {
        [weakSelf.recorder updateMeters];
        float peakPower = pow(10, (0.05 * [weakSelf.recorder peakPowerForChannel:0]));
        if (weakSelf && weakSelf.volumeChangedBlock) {
            weakSelf.volumeChangedBlock(peakPower);
        }
    } repeats:YES];
}

- (void)stopRecording
{
    [self.timer invalidate];
    CGFloat time = self.recorder.currentTime;
    if ([self.recorder isRecording]) {
         [self.recorder stop];
    }
    //转码
    NSString *path = PATH_RECFILE;
    NSURL *url = [NSURL URLWithString:path];
    if (url != nil) {
        [self writeAuToAmrFile:url callback:^(BOOL success, NSData *amrData) {
            
        }];
    }
    
    if (self.completeBlock) {
        self.completeBlock(path, time);
        self.completeBlock = nil;
    }
}

- (void)cancelRecording
{
    [self.timer invalidate];
    self.timer = nil;
//    [self.recorder stop];
    self.recorder = nil;
    if (self.cancelBlock) {
        self.cancelBlock();
        self.cancelBlock = nil;
    }
}

+(BOOL)convertCAFtoAMR:(NSString *)filePath savePath:(NSString *)savePath{
    NSData * data=[NSData dataWithContentsOfFile:filePath];
    data = EncodeWAVEToAMR(data,1,16);
    
    BOOL isSuccess=[data writeToURL:[NSURL fileURLWithPath:savePath] atomically:YES];
    
    if (isSuccess) {
        NSLog(@"TLAudioRecorder转换为amr格式成功,大小为%@",[self fileSizeAtPath:savePath]);
    }
    return isSuccess;
}

//转换为wav格式并生成文件到savePath(showSize是否在控制台打印转换后的文件大小)
+(BOOL)convertAMRtoWAV:(NSString *)filePath savePath:(NSString *)savePath{
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    data = DecodeAMRToWAVE(data);
    
    BOOL isSuccess = [data writeToURL:[NSURL fileURLWithPath:savePath] atomically:YES];
    
    if (isSuccess) {
        NSLog(@"TLAudioRecorder转换为wav格式成功,大小为%@",[self fileSizeAtPath:savePath]);
        //移动该文件夹  并重新命名
//        [NSFileManager.defaultManager moveItemAtPath:filePath toPath:savePath error:nil];
    }
    return isSuccess;
}

+ (NSString *)fileSizeAtPath:(NSString*)filePath{
    unsigned long long size=0;
    
    NSFileManager* manager =[NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        size=[[manager attributesOfItemAtPath:filePath error:nil] fileSize];
        
        if (size >=pow(10,9)) {
            // size >= 1GB
            return [NSString stringWithFormat:@"%.2fGB",size/pow(10,9)];
        } else if (size>=pow(10,6)) {
            // 1GB > size >= 1MB
            return [NSString stringWithFormat:@"%.2fMB",size/pow(10,6)];
        } else if (size >=pow(10,3)) {
            // 1MB > size >= 1KB
            return [NSString stringWithFormat:@"%.2fKB",size/pow(10,3)];
        } else {
            // 1KB > size
            return [NSString stringWithFormat:@"%zdB",size];
        }
    }
    return @"0";
}

/**
 *  得到arm格式的Data
 */
- (NSData*)encodeWAVEToAMROfData:(NSData*)cafData{
    NSData *data = EncodeWAVEToAMR(cafData, 1, 16);
    return data;
}

- (NSData*)encodeWAVEToAMROfFile:(NSURL*)cafFileUrl{
    NSData *cafData = [NSData dataWithContentsOfFile:cafFileUrl.absoluteString];
    NSData *data = EncodeWAVEToAMR(cafData, 1, 16);
    return data;
}

/**
 *  写入文件
 */
- (void)writeAuToAmrFile:(NSURL*)tmpFileUrl callback:(writeSuccessBlock)block{
    if ([tmpFileUrl isKindOfClass:[NSURL class]]) {
        NSData *amrData = [self encodeWAVEToAMROfFile:tmpFileUrl];
        [self writeToAmrFile:tmpFileUrl amrData:amrData call:block];
    }
}

- (void)writeToAmrFile:(NSURL*)tempFile0 amrData:(NSData*)curAudioData call:(writeSuccessBlock)block{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    NSString *amrName = [[[tempFile0 lastPathComponent] componentsSeparatedByString:@"."] firstObject];
    NSString *amrFile = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.amr",amrName]];
    BOOL ist = [curAudioData writeToFile:amrFile atomically:YES];
    if (block) {
        block(ist,curAudioData);
    }
    
}

#pragma mark - # Delegate
//MARK: AVAudioRecorderDelegate
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    if (flag) {
//        NSLog(@"录音成功");
        
        if ([self.delegate respondsToSelector:@selector(recordFinishWithUrl:isSuccess:)]) {
            [_delegate recordFinishWithUrl:PATH_RECFILE isSuccess:flag];
        }
    }
}

#pragma mark - # Getter
- (AVAudioRecorder *)recorder
{
    if (_recorder == nil) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *sessionError;
        
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
        
        if(session == nil){
            NSString *info = [NSString stringWithFormat:@"Error creating session: %@",[sessionError description]];
            NSLog(info);
            return nil;
        }else {
            [session setActive:YES error:nil];
        }
        
        // 设置录音的一些参数
        // 设置录音的一些参数
        NSMutableDictionary *setting = [NSMutableDictionary dictionary];
        setting[AVFormatIDKey] = @(kAudioFormatLinearPCM);              // 音频格式
        setting[AVSampleRateKey] = @(8000);                            // 录音采样率(Hz)
        setting[AVEncoderBitRateKey] = @(16000);
        setting[AVNumberOfChannelsKey] = @(1);                          // 音频通道数 1 或 2
        setting[AVLinearPCMBitDepthKey] = @(16);                         // 线性音频的位深度
//        setting[AVEncoderAudioQualityKey] = [NSNumber numberWithInt:AVAudioQualityHigh];        //录音的质量
        
        //扬声器播放
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute,sizeof (audioRouteOverride),&audioRouteOverride);

        _recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:self.recFilePath] settings:setting error:NULL];
        _recorder.delegate = self;
        _recorder.meteringEnabled = YES;
    }
    return _recorder;
}

- (NSString *)recFilePath
{
    return PATH_RECFILE;
}


//删除默认录音地址的录音文件
-(void)deleteAudioFile{
    if (PATH_RECFILE) {
        [self deleteFileWithUrl:PATH_RECFILE];
    }
}

//删除本地url文件
-(void)deleteFileWithUrl:(NSString *)url{
    if ([url isEqualToString:@""]||url==nil||[url isKindOfClass:[NSNull class]]) {
        return;
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:[NSURL URLWithString:url] error:NULL];
}

//- (void)proximityStateChanged:(NSNotification *)notification {
//    if ([[UIDevice currentDevice] proximityState] == YES) {
//        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
//    }else {
//        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//    }
//}

@end
