//
//  TLAudioRecorder.h
//  TLChat
//
//  Created by 李伯坤 on 16/7/11.
//  Copyright © 2016年 李伯坤. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TLAudioRecorderDelegate <NSObject>

//@optional
/**
 *  录音结束(url为录音文件地址,isSuccess是否录音成功)
 */
-(void)recordFinishWithUrl:(NSString *)url isSuccess:(BOOL)isSuccess;

@end

@interface TLAudioRecorder : NSObject

+ (TLAudioRecorder *)sharedRecorder;

- (void)startRecordingWithVolumeChangedBlock:(void (^)(CGFloat volume))volumeChanged
                               completeBlock:(void (^)(NSString *path, CGFloat time))complete
                                 cancelBlock:(void (^)())cancel;
- (void)stopRecording;
- (void)cancelRecording;

+ (BOOL)convertCAFtoAMR:(NSString *)filePath savePath:(NSString *)savePath;

+ (BOOL)convertAMRtoWAV:(NSString *)filePath savePath:(NSString *)savePath;

@property (nonatomic,weak) id <TLAudioRecorderDelegate> delegate;

@end
