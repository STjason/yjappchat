//
//  TLAudioPlayer.h
//  TLChat
//
//  Created by 李伯坤 on 16/7/12.
//  Copyright © 2016年 李伯坤. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@interface TLAudioPlayer : NSObject

+ (TLAudioPlayer *)sharedAudioPlayer;


- (void)playAudioAtPath:(NSString *)path complete:(void (^)(BOOL finished))complete;

- (void)stopPlayingAudio;

@property (nonatomic, strong) AVAudioPlayer *player;

@end
