//
//  CRLotteryLogic.swift
//  gameplay
//
//  Created by admin on 2019/8/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRLotteryLogic: NSObject {
    static let instance = CRLotteryLogic()
    
    class func shareInstance() -> CRLotteryLogic {
        return instance
    }
        
    /// 格式化彩种数据
    ///
    /// - Parameters:
    ///   - type: 0官方，1信用，2真人，3电子，4体育，5棋牌
    ///   - lotteryDatas: 所有彩票，真人，电子，体育，棋牌数据
    /// - Returns: 根据type返回对应类型 LotteryData数组
    func prepare_data_for_subpage(type:Int,lotteryDatas:[LotteryData]) -> [LotteryData]{
        let allDatas = lotteryDatas + [LotteryData]()
        var gameDatas:[LotteryData] = []
        for sub in allDatas{
            let moduleCode = sub.moduleCode
            //判断是官方或信用
            let version_str = String.init(format: "%d", sub.lotVersion)
            if type == 0 {
                if moduleCode == CAIPIAO_MODULE_CODE && version_str == VERSION_1{
                    gameDatas = gameDatas + sub.subData
                }
            }else if type == 1 {
                if moduleCode == CAIPIAO_MODULE_CODE && version_str == VERSION_2{
                    gameDatas = gameDatas + sub.subData
                }
            }else if type == 2 {
                if moduleCode == REAL_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if type == 4 {
                if moduleCode == SPORT_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if type == 3 {
                if moduleCode == GAME_MODULE_CODE{
                    gameDatas.append(sub)
                }
            }else if type == 5
            {
                if moduleCode == CHESS_MODULE_CODE
                {
                    gameDatas.append(sub)
                }
            }
        }
        
        return gameDatas
    }
}
