//
//  CRChatRoomLogic.swift
//  YiboGameIos
//
//  Created by admin on 2020/1/8.
//  Copyright © 2020年 com.lvwenhan. All rights reserved.
//

import UIKit
import HandyJSON

class CRChatRoomLogic: NSObject {
    ///是私聊消息，则重新设置 msgType和 群聊消息相同
    class func resetPrivateMsgType(msgStr:String,msg:CRMessage) {
        class MsgStrModel: HandyJSON {
            var msgType = ""
            required init() {}
        }
        
        //私聊 刷新未读消息数 :会显示在页面上的消息
        //私聊消息：【1 文本】【2 图片】【4 分享盈亏】                   【5 分享注单消息】             【6 发红包消息】
        //群聊消息：【1 文本】【2 图片】【分享盈亏 ShareGainsLosses = 9】【5 分享注单消息ShareBet = 5】 【发红包消息 RedPacket = 3】
        if let msgStrModel = MsgStrModel.deserialize(from: msgStr) {
            msg.msgType = msgStrModel.msgType
        }
        
        let message = msg
        if  message.msgType == "4" {
            message.msgType = "\(CRMessageType.ShareGainsLosses.rawValue)"
        }else if message.msgType == "6" {
            message.msgType = "\(CRMessageType.RedPacket.rawValue)"
        }
    }
    
    ///是否有被私聊的权限
    class func updatePrivatePermisson(toolPermission:[String]) {
        privatePermission = toolPermission.contains("\(sendMessageType.privateChatInit.rawValue)")
    }
    
    ///收到消息后，对私聊消息进行处理
    class func formatWithPrivateMessage(privateMsg:CRPrivateMessage,realMsg:CRMessage) {
        if let messageItem = realMsg as? CRTextMessageItem {
            messageItem.text = privateMsg.record
            messageItem.msgId = privateMsg.userMessageId
            messageItem.msgUUID =  privateMsg.msgUUID
            
            if let fromUser = privateMsg.fromUser {
                messageItem.levelName = fromUser.levelName
                messageItem.userType = fromUser.userType
                messageItem.avatar = fromUser.avatar
                messageItem.userId = fromUser.id
                messageItem.account = fromUser.account
                messageItem.levelIcon = fromUser.levelIcon
            }
            
            return
        }
        
        if let messageItem = realMsg as? CRImageMessage {
            messageItem.text = privateMsg.record
            messageItem.msgId = privateMsg.userMessageId
            messageItem.msgUUID =  privateMsg.msgUUID
            messageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + privateMsg.record
            
            if let fromUser = privateMsg.fromUser {
                messageItem.levelName = fromUser.levelName
                messageItem.userType = fromUser.userType
                messageItem.avatar = fromUser.avatar
                messageItem.userId = fromUser.id
                messageItem.account = fromUser.account
                messageItem.levelIcon = fromUser.levelIcon
            }
            
            return
        }
        
        if let messageItem = realMsg as? CRShareGainsLossesItem {
            messageItem.text = privateMsg.record
            messageItem.msgId = privateMsg.userMessageId
            messageItem.msgUUID =  privateMsg.msgUUID
            
            if let fromUser = privateMsg.fromUser {
                messageItem.levelName = fromUser.levelName
                messageItem.userType = fromUser.userType
                messageItem.avatar = fromUser.avatar
                messageItem.userId = fromUser.id
                messageItem.account = fromUser.account
                messageItem.levelIcon = fromUser.levelIcon
            }
            
            return
        }
        
        if let messageItem = realMsg as? CRRedPacketMessage {
            messageItem.text = privateMsg.record
            messageItem.msgId = privateMsg.userMessageId
            messageItem.msgUUID =  privateMsg.msgUUID
            messageItem.payId = privateMsg.payId
            if let fromUser = privateMsg.user {
                messageItem.levelName = fromUser.levelName
                messageItem.userType = fromUser.userType
                messageItem.avatar = fromUser.avatar
                messageItem.userId = fromUser.id
                messageItem.account = fromUser.account
                messageItem.levelIcon = fromUser.levelIcon
            }
            
            return
        }
        
    }
    
    ///获得发送的消息的功能描述，和对参数进行统一处理
    class func getMsgFuncDes(code:String,msgType:String, parameters:[String:Any]) -> (String,[String:Any]) {
        var funcs = ""
        var parameters = parameters
        
        if msgType == CR_USER_JOIN_ROOM_S {
            funcs = "切换房间"
        }else {
            switch code {
            case CRUrl.shareInstance().LOGIN_AUTHORITY:
                funcs = "聊天室登录"
                break
            case CRUrl.shareInstance().CHAT_ROOM_LIST:
                funcs = "获取房间列表"
                break
            case CRUrl.shareInstance().JOIN_CHAT_ROOM:
                funcs = "进入房间"
                break
            case CRUrl.shareInstance().CODE_ROOM_CONFIG:
                funcs = "拉取房间配置"
                break
            case "\(sendMessageType.redPacket.rawValue)":
                funcs = "发送红包"
                break
            case "\(sendMessageType.image.rawValue)":
                funcs = "发送图片"
                break
            case "\(sendMessageType.shareOrder.rawValue)":
                parameters["multiBet"] = "1"
                funcs = "分享注单"
                break
            case "\(sendMessageType.followOrder.rawValue)":
                funcs = "跟单"
                break
            case "\(sendMessageType.historyRecord.rawValue)":
                funcs = "拉取聊天记录"
                break
            case "\(sendMessageType.pullLotteryTicket.rawValue)":
                funcs = "拉取开奖结果"
                break
            case "\(sendMessageType.historyBetShare.rawValue)":
                funcs = "拉取投注历史"
                break
            case "\(sendMessageType.userInfo.rawValue)":
                funcs = "拉取用户信息"
                break
            case "\(sendMessageType.roomOnLineList.rawValue)":
                funcs = "拉取在线用户"
                break
            case "\(sendMessageType.signIn.rawValue)":
                funcs = "签到"
            case "\(sendMessageType.signIn.rawValue)":
                funcs = "拉取导师计划"
            case "\(sendMessageType.privateChatInit.rawValue)":
                
                if let type = parameters["type"] as? String {
                    if type == "1" {
                        funcs = "初始化私聊"
                    }else if type == "2" {
                        funcs = "拉取私聊列表"
                    }else if type == "3" {
                        print("私聊消息 已设置为‘已读’")
                    }else if type == "4" {
                        print("设置私聊备注")
                    }else if type == "join" {
                        funcs = "切换私聊房间"
                    }
                }
            case "\(sendMessageType.privateChatSend.rawValue)":
                if let type = parameters["type"] as? String {
                    if type == "1" {
                        print("在私聊房间发消息")
                    }else if type == "2" {
                        funcs = "拉取私聊历史消息"
                    }
                }
            case "\(sendMessageType.prohibit.rawValue)" :
                funcs = "禁言中"
            case "\(sendMessageType.retract.rawValue)" :
                funcs = "撤回中"
            case "\(sendMessageType.longDragon.rawValue)" :
                funcs = "长龙数据获取中"
            default:
                break
            }
        }
        
        return (funcs,parameters)
    }
    
    ///如果html文本只含有 <p>标签，则删除<p>标签,否则直接返回,用以将html文本转为普通文本消息展示
    static func deleteHtmlTagsWhenPTag(contents:String) -> String {
        var results = contents
        //含有除 <p>之外的其他标签
        let isHtmlContents = contents.contains("<span") || contents.contains("<br") || contents.contains("style=") || contents.contains("<img")
        
        if contents.contains("<img") && !contents.contains("<p>") {
            results = "<p>\(results)</p>"
        }
        
        if results.contains("<p>") && !isHtmlContents {
            results = contents.replacingOccurrences(of: "<p>", with: "")
            results = results.replacingOccurrences(of: "</p>", with: "")
        }
        
        return results
    }
    
    //Base64转码为 UTF8 String
    class func base64ToNormalString(base64Msg:String) -> String {
        guard let baseData = Data(base64Encoded: base64Msg, options: Data.Base64DecodingOptions(rawValue: NSData.Base64DecodingOptions.RawValue(0))) else {
            return ""
        }
        
        return String.init(data:baseData, encoding: .utf8) ?? ""
    }
    
    class func newMsgModel(msgModel:CRMessageModel) -> CRMessageModel {
        let model = CRMessageModel()
        model.source = msgModel.source + ""
        model.code = msgModel.code + ""
        model.stationId = msgModel.stationId + ""
        model.userId = msgModel.userId + ""
        model.roomId = msgModel.roomId + ""
        return model
    }
    
    ///将加密字符串数组，格式化为解码后的字符串数组 CR_LOGIN_R
    class func formatDecodedStringArray(msgType:String = "",datas:[Any]) -> (jsonArray:[String],type:String)? {
        if let encodeStrings = datas as? [String] {
            var deStringArray = [String]()
            var code = ""
            for jsonString in encodeStrings {
                var key =  newAESKey
                if [CR_LOGIN_R,CR_USER_JOIN_ROOM_R].contains(msgType) {
                    key = aesKey
                }
                
                var deJsonString:String?
                if [CR_USER_JOIN_GROUP_R].contains(msgType) {
                    deJsonString = Encryption.Decode_AES_ECB(key: aesKey, iv:aesIV , strToDecode: jsonString)
                }else {
                    deJsonString = Encryption.Decode_AES_ECB(key: key,strToDecode: jsonString)
                }
                
                let model = CRSysSocketBaseModel.deserialize(from: deJsonString)
                code = model?.code ?? ""
                
                deStringArray.append(deJsonString ?? "")
            }
            
            return (deStringArray,code)
        }
        
        return (([""],""))
    }
    
    ///根据msgUUID从 消息数组中查找对应 CRMessage
    class func getMsgFromMessageItemsWith(msgUUID:String?,msgItems:[CRMessage]) -> CRMessage? {
        
        for i in (0..<msgItems.count).reversed() {
            let item = msgItems[i]
            if item.msgUUID == msgUUID {
                return item
            }
        }
        return nil
    }
    
    ///根据payid获取对应的红包消息
    class func getRedpacketWith(payId:String,msgItems:[CRMessage]) -> CRRedPacketMessage? {
        for i in (0..<msgItems.count).reversed() {
            if let item = msgItems[i] as? CRRedPacketMessage {
                if item.payId == payId {
                    return item
                }
            }
        }
        return nil
    }
}
