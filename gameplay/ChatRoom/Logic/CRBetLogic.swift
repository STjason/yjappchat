//
//  CRBetLogic.swift
//  gameplay
//
//  Created by admin on 2019/8/19.
//  Copyright © 2019年 yibo. All rights reserved.
//
import UIKit
import AVFoundation
import AudioToolbox


class CRBetLogic: NSObject {
    static let instance = CRBetLogic()
    
    class func shareInstance() -> CRBetLogic {
        return instance
    }
    
    /// 金额和注数判断
    ///
    /// - Parameters:
    ///   - zhuShu: 已选中注数
    ///   - betMoney: 下注金额
    ///   - accountMoney: 账户金额
    /// - Returns: Bool 下注金额和注数是否合法，String 下注非法时的错误信息
    class func judgeBeforeBet(zhuShu:Int,betMoney:Float,accountMoney:Double) -> (Bool,String) {
        if zhuShu == 0 {
            return (false,TIP_BETNUM_ILLEGAL)
        }else if accountMoney < Double(betMoney) {
            return (false,TIP_MONEY_NOT_ENOUGH)
        }else {
           return (true,"")
        }
    }
    
    //对有header的玩法的判断
    class func isHeaderPlay(parentCode:String) -> Bool {
        return ["zxbz","zm16","ztm","lm","hx","lx",weishulian].contains(parentCode)    }
    
    
    //MARK: 是否赔率版下注
    class func isPeilvVersion(cpVersion:String,lhcSelect:Bool) ->  Bool{
        if cpVersion == VERSION_2 || lhcSelect{
            return true
        }
        return false
    }
    
    ///官方版，获取反水
    class func update_slide_when_peilvs_obtain(rateBack:[PeilvWebResult]) -> PeilvWebResult? {
        //这里根据赔率列表来决定是否显示返水拖动条
        if rateBack.isEmpty{
            return nil
        }
        
        var rightResult:PeilvWebResult!
        for odd in rateBack{
            if rightResult != nil{
                if odd.maxOdds > rightResult.maxOdds{
                    rightResult = odd
                }
            }else{
                rightResult = odd
            }
        }
        
        return rightResult
    }
    
    /// 圆角分模式
    class func convertYJFModeToInt(mode:String) -> Int {
        if mode == YUAN_MODE {return 1}
        else if mode == JIAO_MODE {return 10}
        else if mode == FEN_MODE {return 100}
        else {return 1}
    }
}
