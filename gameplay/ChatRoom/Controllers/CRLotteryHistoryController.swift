//
//  CRLotteryHistoryController.swift
//  gameplay
//
//  Created by admin on 2019/10/7.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRLotteryHistoryController: UIViewController {
    
    var sharebetHandler:((_ models:[CRBetHistoryMsgModel]) -> Void)?
    let cellID = "CRLotteryHistoryCell"
    
    var historyModels = [CRBetHistoryMsgModel]() { //列表显示的历史投注数据
        didSet {
            tableView.tableViewDisplayWithLabel(datasCount: historyModels.count,tableView: tableView,message:"暂无投注记录，赶快去投注吧！",textColor:UIColor.white)
            tableView.reloadData()
        }
    }
    
    var selectedRows = [Int]()
    
    lazy var bottomView:CRHistoryShareBottom = {
        let view = Bundle.main.loadNibNamed("CRHistoryShareBottom", owner: self, options: nil)?.last as! CRHistoryShareBottom
        
        return view
    }()
    
    lazy var tableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .plain)
        view.separatorInset = UIEdgeInsets.zero
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        
        let nib = UINib.init(nibName: "CRLotteryHistoryCell", bundle: nil)
        
        view.register(nib, forCellReuseIdentifier: cellID)
        view.tableFooterView = UIView.init()
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
//    convenience init(stationId:String,userId:String,roomId:String) {
//        self.init()
//        self.obtainLotteryQuery(stationId: stationId, userId: userId,roomId: roomId)
//    }
    
    deinit {
        print("")
    }
    
    func setupUI() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.bottomView)
        
        self.setupClosure() //设置闭包
        
        self.tableView.snp.makeConstraints {[weak self] (make) in
            make.top.left.right.equalTo(0)
            
            if let weakSelf = self {
                make.bottom.equalTo(weakSelf.bottomView.snp.top)
            }
        }
        
        self.bottomView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(IS_IPHONE_X ? 50 + 34 : 50)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - REQUEST
    private func obtainLotteryQuery(stationId:String,userId:String,roomId:String) {
        let historyMessage = CRLotteryHistoryMessage()
        historyMessage.code = CRUrl.shareInstance().CODE_HISTORY_SHARE
        historyMessage.dataType = "1"
        historyMessage.isPrivate = false
        historyMessage.stationId = stationId
        historyMessage.userId = userId
        historyMessage.source = "app"
        historyMessage.msgUUID = CRCommon.generateMsgID()
        historyMessage.roomId = roomId
        
        if let parameter:[String:Any] = historyMessage.toJSON() {
            CRNetManager.shareInstance().sendMessage(paramters: parameter, method: .post, requestType: .historyBetShare) {[weak self] (success, msg, models) in
                guard let weakSelf = self else {
                    return
                }
                
                if let historyModels = models as? [CRBetHistoryMsgModel] {
                    weakSelf.historyModels = historyModels
                    weakSelf.tableView.reloadData()
                }
            }
            
        }
    }
    
    //MARK: 判断cell是否被选中
    private func judgeIsSelected(row:Int) -> Bool {
        return self.selectedRows.contains(row)
    }
    
    //MARK: 分享注单
}

extension CRLotteryHistoryController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        tableView.tableViewDisplayWithLabel(datasCount: historyModels.count,tableView: tableView,message:"暂无投注记录，赶快去投注吧！",textColor:UIColor.white)
        return historyModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? CRLotteryHistoryCell else {
            fatalError("dequeueReusableCell CRLotteryHistoryCell failure")
        }
        
        cell.selectionStyle = .none
        
        let model = self.historyModels[indexPath.row]
        
        cell.configWith(isSelected: self.judgeIsSelected(row: indexPath.row),model: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 44))
        label.textColor = UIColor.white
        label.text = "投注历史"
        label.textAlignment = .center
        label.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
}


extension CRLotteryHistoryController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.judgeIsSelected(row: indexPath.row) {
            for (index,value) in self.selectedRows.enumerated() {
                if value == indexPath.row {
                    self.selectedRows.remove(at: index)
                    break
                }
            }
        }else {
            self.selectedRows.append(indexPath.row)
        }
        
        let isAllSelected = selectedRows.count == historyModels.count
        let selectBtn = self.bottomView.selectAllButton
        let checkBtn = self.bottomView.selectAllCheck
        selecteBtnState(allButton: selectBtn!, allCheckButton: checkBtn!, allSelect: isAllSelected)
        
        tableView.reloadData()
    }
}

//MARK: - closure
extension CRLotteryHistoryController {
    private func setupClosure() {
        //MARK: 分享注单点击事件
        self.bottomView.sharebetHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            //遍历所有订单 分享
            var models = [CRBetHistoryMsgModel]()
            for index in 0..<weakSelf.selectedRows.count {
                let selectedIndex = weakSelf.selectedRows[index]
                let model = weakSelf.historyModels[selectedIndex]
                models.append(model)
            }
            
            if models.count > 0 {
                weakSelf.sharebetHandler?(models)
            }else if weakSelf.historyModels.count == 0 {
                showToast(view: weakSelf.view, txt: "没有可分享的注单")
            }else {
                showToast(view: weakSelf.view, txt: "请选择想要分享的注单")
            }
        }
        
        //MARK: 全选注单点击事件
        self.bottomView.allSelectHandler = {[weak self] (allButton,allCheckButton) in
            if let weakSelf = self {
                if weakSelf.historyModels.count == weakSelf.selectedRows.count {
                    weakSelf.selectedRows.removeAll()
                    
                    weakSelf.selecteBtnState(allButton:allButton,allCheckButton:allCheckButton,allSelect: false)
                }else {
                    weakSelf.selectedRows.removeAll()
                    
                    for i in 0..<(weakSelf.historyModels.count) {
                        weakSelf.selectedRows.append(i)
                    }
                    
                    weakSelf.selecteBtnState(allButton:allButton,allCheckButton:allCheckButton,allSelect: true)
                }
                
                weakSelf.tableView.reloadData()
            }
        }
    }
    
    private func selecteBtnState(allButton:UIButton,allCheckButton:UIButton,allSelect:Bool) {
        allButton.isSelected = allSelect
        allCheckButton.isSelected = allSelect
    }
    
}







