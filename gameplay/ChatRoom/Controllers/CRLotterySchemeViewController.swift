//
//  CRLotterySchemeViewController.swift
//  gameplay
//
//  Created by JK on 2019/12/2.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import MBProgressHUD

class CRLotterySchemeViewController: CRBaseController {
    
    /** 网络请求控制器 */
    var networkVc : CRChatController?
    /** 彩种数据集合 */
    var lotteryListData: [CRLotteryListModel] = []
    /** 玩法数据集合 */
    var playListData : [String] = []
    /** 开奖列表数据集合 */
    var resultListData : [CRResultListModel] = []
    /** 彩种 code */
    var lotteryCode : String = ""
    /** 玩法名称 */
    var playName : String = ""
    /** 彩种名称 */
    var lotteryName : String = ""
    /** 彩种选择 默认第一个 */
    var lotterySelectIndex = 0
    /** 玩法选择 默认第一个 */
    var playSelectIndex = 0
    /** 玩法选择列表 */
    var playPopMenu : HCPopListMenu?
    /** 设置15s 超时定时器 */
    var timer: Timer?
    /** 设置15s 超时时间 */
    var overtime: Int = 5
    /** 懒加载 */
    var loadingHud: MBProgressHUD?
    
    // 彩种列表
    lazy var lotteryListView: CRLotteryListView = {
        let view = CRLotteryListView(frame: .zero)
        view.backgroundColor = UIColor.init(hexString: "#F5F5F5")
        return view
    }()
    
    //半透明弹窗
    lazy var floatView:UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.frame = CGRect.init(x: 0, y: 0, width: CRConstants.screenWidth, height: CRConstants.screenHeight)
        view.addTarget(self, action: #selector(tapFloatView), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    /// 定义彩种选择按钮
    lazy var lotteryNameBtn: UIButton = {
        let button = UIButton()
        button.frame = kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 400), height: 30)
        button.center = (self.navigationController?.navigationBar.center)!
        button.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        button.addTarget(self, action: #selector(disPlayLotteryList), for: .touchUpInside)
        button.backgroundColor = UIColor.clear
        
        // 设置按钮 文本在左 icon在右
        button.semanticContentAttribute = .forceRightToLeft
        return button
    }()
    
    /// 定义玩法选择按钮
    lazy var playNameBtn: UIButton = {
        let button = UIButton()
        button.frame = kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 350), height: 30)
        button.center = (self.navigationController?.navigationBar.center)!
        button.titleLabel?.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 45))
        button.addTarget(self, action: #selector(disPlayPlayList), for: .touchUpInside)
        button.backgroundColor = UIColor.clear
        
        // 设置按钮 文本在左 icon在右
        button.semanticContentAttribute = .forceRightToLeft
        return button
    }()
    
    /** 列表 */
    lazy var planListView : UITableView = {
        let tabView = UITableView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - CGFloat(KNavHeight)), style: .grouped)
        tabView.delegate = self
        tabView.dataSource = self
        
        tabView.register(CRLotterySchemeListCell.self, forCellReuseIdentifier: "lotterySchemeListCell")
        /// cell分割线补全
        tabView.separatorInset = UIEdgeInsets(top: 0,left: 0, bottom: 0,right: 0);
        
        return tabView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.addSubview(planListView)
        view.addSubview(floatView)
        view.addSubview(lotteryListView)
        
        self.setNav()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.requestListData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let navBackgroudImage = UIImage(named: "chatNavBar")
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroudImage?.getNavImageWithImage(image: navBackgroudImage!), for: .default)
        
        if let view = getSemicycleButton() {
            view.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
    }
    /// 设置导航栏
    func setNav() {
        self.navigationItem.titleView = lotteryNameBtn
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: playNameBtn)
    }
    
    @objc func disPlayLotteryList() {
        lotteryListView.selectLotteryIndexHandle = {[weak self](selectIndex) in
            if let weakSelf = self {
                weakSelf.lotterySelectIndex = selectIndex
                weakSelf.lotteryCode = weakSelf.lotteryListData[selectIndex].lotteryCode
                weakSelf.playName = ""
                weakSelf.playSelectIndex = 0
                
                weakSelf.requestListData(option: "2")
            }
        }
        
        if lotteryListView.isShow {
            lotteryListView.hideView()
        }else {
            lotteryListView.getCurrentListData(listData: lotteryListData)
            lotteryListView.show()
        }
        self.showGrayBackgroundView(show: lotteryListView.isShow)
    }
    
    @objc func disPlayPlayList() {
        if lotteryListView.isShow {
            self.disPlayLotteryList()
        }
        playPopMenu = HCPopListMenu.init(titles: playListData)
        
        playPopMenu?.delegate = self
        playPopMenu?.show()
    }
    
    func requestListData(option: String = "1") {
        
        if self.lotteryListView.isShow {
            self.disPlayLotteryList()
        }
        
        loadingHud = showLoadingHUD(loadingStr: "加载数据中...")
        loadingHud?.show(animated: true)
        networkVc?.getGameListData(option: option, lotteryCode: lotteryCode, playName: playName)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(overtimeCountDown), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: RunLoop.Mode.common)
        
        networkVc?.lotterySchemeModelHandle = {[weak self](lotterySchemeModel) in
            if let weakSelf = self {
                weakSelf.timer?.invalidate()
                weakSelf.timer = nil
                
                weakSelf.loadingHud?.hide(animated: true)
                
                if lotterySchemeModel.success == false {
                    
                    showErrorHUD(errStr: "请求失败")
                    return
                }
                
//                let playList     = lotterySchemeModel.source?.msg?.playList ?? [String]()
//                let lotteryList  = lotterySchemeModel.source?.msg?.lotteryList ?? [CRLotteryListModel]()
//                let resultList   = lotterySchemeModel.source?.msg?.resultList ?? [CRResultListModel]()
                
                let playList     = lotterySchemeModel.source?.playList ?? [String]()
                let lotteryList  = lotterySchemeModel.source?.lotteryList ?? [CRLotteryListModel]()
                let resultList   = lotterySchemeModel.source?.resultList ?? [CRResultListModel]()
                
                if !playList.isEmpty {
                    weakSelf.playListData = playList
                    weakSelf.playName = weakSelf.playListData[weakSelf.playSelectIndex]
                }
                if !lotteryList.isEmpty {
                    weakSelf.lotteryListData = lotteryList
                    weakSelf.lotteryCode = weakSelf.lotteryListData[weakSelf.lotterySelectIndex].lotteryCode
                    weakSelf.lotteryName = weakSelf.lotteryListData[weakSelf.lotterySelectIndex].lotteryName
                }
                if !resultList.isEmpty {
                    weakSelf.resultListData = resultList
                    weakSelf.lotteryName = weakSelf.lotteryListData[weakSelf.lotterySelectIndex].lotteryName
                }
                
                weakSelf.planListView.reloadData()
                weakSelf.updateData()
                
            }
        }
    }
    /// 刷新导航栏 彩种名称、玩法名称
    func updateData() {
        lotteryNameBtn.layer.cornerRadius = 5
        lotteryNameBtn.layer.borderColor = UIColor.white.cgColor
        lotteryNameBtn.layer.borderWidth = 1
        
        lotteryNameBtn.setTitle(lotteryName, for: .normal)
        lotteryNameBtn.setImage(UIImage(named: "downWhite"), for: .normal)
        
        playNameBtn.setTitle(playName, for: .normal)
        playNameBtn.setImage(UIImage(named: "downWhite"), for: .normal)
    }
    /// 超时倒计时
    @objc func overtimeCountDown() {
        overtime -= 1
        if overtime < 0 {
            timer?.invalidate()
            timer = nil
            
            loadingHud?.hide(animated: true)
            showErrorHUD(errStr: "网络超时")
        }
    }
    //点击半透明背景视图
    @objc func tapFloatView() {
        self.lotteryListView.hideView()
        self.showGrayBackgroundView(show: false)
    }
    //MARK: 显示灰置背景
    func showGrayBackgroundView(show:Bool) {
        if show {
            floatView.isHidden = false
        }else {
            floatView.isHidden = true
        }
    }
}

extension CRLotterySchemeViewController: PopMenuViewDelegate
{
    func menu(_ menu: HCPopListMenu!, didSelectRowAt index: Int) {
        playSelectIndex = index
        playName = playListData[index]
        
        self.requestListData(option: "3")
    }
}

extension CRLotterySchemeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kCurrentScreen(x:125)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kCurrentScreen(x:130)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView = UIView(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: kCurrentScreen(x: 130)))
        headView.backgroundColor = .clear
        
        let periodLabel = UILabel(frame: kCGRect(x: 0, y: 0, width: kCurrentScreen(x: 385), height: headView.height))
        periodLabel.textAlignment = .center
        periodLabel.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 50))
        periodLabel.text = "期号"
        
        let resultLabel = UILabel(frame: kCGRect(x:periodLabel.frame.maxX, y: 0, width: kCurrentScreen(x: 280), height: headView.height))
        resultLabel.textAlignment = .center
        resultLabel.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 50))
        resultLabel.text = "开奖结果"
        
        let lotterySchemeLabel = UILabel(frame: kCGRect(x:resultLabel.frame.maxX, y: 0, width: kCurrentScreen(x: 280), height: headView.height))
        lotterySchemeLabel.textAlignment = .center
        lotterySchemeLabel.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 50))
        lotterySchemeLabel.text = "购彩计划"
        
        let lotteryTimeLabel = UILabel(frame: kCGRect(x: lotterySchemeLabel.frame.maxX, y: 0, width: kCurrentScreen(x: 285), height: headView.height))
        lotteryTimeLabel.textAlignment = .center
        lotteryTimeLabel.font = UIFont.systemFont(ofSize: kCurrentScreen(x: 50))
        lotteryTimeLabel.text = "开奖时间"
        
        headView.addSubview(periodLabel)
        headView.addSubview(resultLabel)
        headView.addSubview(lotterySchemeLabel)
        headView.addSubview(lotteryTimeLabel)
        
        for index in 0..<3 {
            let line = UIView()
            line.frame = kCGRect(x: kCurrentScreen(x: 385) + CGFloat(index) * kCurrentScreen(x: 280), y: 0, width: 0.5, height: kCurrentScreen(x:130))
            line.backgroundColor = UIColor.lightGray
            
            headView.addSubview(line)
        }
        
        return headView
    }
}

extension CRLotterySchemeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "lotterySchemeListCell") as? CRLotterySchemeListCell else {
            fatalError("The dequeued cell is not an instance of NewSelectedViewCell.")
        }
        cell.getCurrentModel(model: self.resultListData[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
}
