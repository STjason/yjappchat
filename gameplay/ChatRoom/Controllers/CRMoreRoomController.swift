//
//  CRMoreRoomController.swift
//  gameplay
//
//  Created by admin on 2019/10/13.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRMoreRoomController: UIViewController {
    var insideRoomHandler:((_ roomModel:CRRoomListModel) -> Void)?
    var userType:Int = 0
    var roomsList = [CRRoomListModel]()
    let cellID = "CRMoreRoomCell"
    
    lazy var tableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .plain)
        view.separatorInset = UIEdgeInsets.zero
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.separatorStyle = .none
        view.register(UINib.init(nibName: "CRMoreRoomCell", bundle: nil), forCellReuseIdentifier: cellID)
        view.tableFooterView = UIView.init()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "房间列表"
        self.view.backgroundColor = UIColor.white
    
        self.view.addSubview(tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let navBackgroudImage = UIImage(named: "chatNavBar")
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroudImage?.getNavImageWithImage(image: navBackgroudImage!), for: .default)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
    }
}


extension CRMoreRoomController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = roomsList[indexPath.row]
        insideRoomHandler?(model)
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension CRMoreRoomController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as? CRMoreRoomCell  else {
            fatalError("dequeueReusableCell CRMoreRoomCell failure")
        }
        
        let remainder = (indexPath.row + 1) % 3
        
        let model = self.roomsList[indexPath.row]
        cell.configWith(model: model,colorType:remainder, userType: userType)
        
        return cell
    }
}






