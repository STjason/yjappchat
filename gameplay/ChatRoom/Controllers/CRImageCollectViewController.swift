//
//  CRImageCollectViewController.swift
//  gameplay
//
//  Created by JK on 2019/12/8.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import Photos
import Kingfisher

class CRImageCollectViewController: UIViewController {
    
    /** 网络请求控制器 */
    var networkVc : CRChatController?
    /** 最大选中数量 默认5张 */
    var maxSelected: Int = 5
    /** 每行个数 */
    var numsInHorizontal = 3
    /** 间距 */
    var margin: CGFloat = 1
    /** 图片选择器 */
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout.init()
        layout.minimumLineSpacing = margin
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        //每个item的宽 为防止计算丢失精度 作-0.1px 操作
        var itemWidth = (kScreenWidth - CGFloat(numsInHorizontal + 1) * margin) / CGFloat(numsInHorizontal) - 0.1
        layout.itemSize = CGSize.init(width: itemWidth, height: itemWidth)
        
        let collection = UICollectionView.init(frame: kCGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - CGFloat(KNavHeight) - (IS_IPHONE_X ? CGFloat(83) : CGFloat(49))), collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = UIColor.clear
        collection.allowsMultipleSelection = true
        collection.bounces = true
        
        collection.register(CRImageCollectionViewCell.self, forCellWithReuseIdentifier: "imageCollectionViewCell")
        
        return collection
    }()
    
    /** 底部工具栏 */
    lazy var toolBar: CRImageCollectBottomToolBarView = {
        let bar = CRImageCollectBottomToolBarView(frame: kCGRect(x: 0, y: kScreenHeight - (kIsPhone_X ? 83 : 49) - CGFloat(KNavHeight), width: kScreenWidth, height: kIsPhone_X ? 83 : 49))
        
        return bar
    }()
    
    /** 底部工具栏 取消按钮 */
    lazy var deleteBtn: UIButton = {
        return toolBar.deleteBtn
    }()
    
    /** 底部工具栏 确定&删除按钮 */
    lazy var sendBtn: UIButton = {
        return toolBar.sendBtn
    }()
    
    /** 图片数据源 */
    var imageData: [URL] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "收藏图片"
        
        view.backgroundColor = .white
        view.addSubview(collectionView)
        view.addSubview(toolBar)
        
        deleteBtn.addTarget(self, action: #selector(deleteClick), for: .touchUpInside)
        sendBtn.addTarget(self, action: #selector(sendClick), for: .touchUpInside)
        
        self.requestImageCollect(option: "1", fileCode: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let navBackgroudImage = UIImage(named: "chatNavBar")
        
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroudImage?.getNavImageWithImage(image: navBackgroudImage!), for: .default)
        
        if let view = getSemicycleButton() {
            view.isHidden = true
        }
    }
    
    @objc func requestImageCollect(option: String, fileCode: String) {
        self.networkVc?.requestImageCollect(option: option, fileCode: fileCode)
        
        self.networkVc?.imageCollectModelHandle = {[weak self](imageCollectModel) in
            if let weakSelf = self {
                let option = imageCollectModel.source?.option
                /// option 为1 则刷新列表
                if option == "1" {
                    let imageIdData = imageCollectModel.source?.collectImageArray ?? [String]()
                    if imageIdData.isEmpty {
                        showErrorHUD(errStr: "暂无收藏图片")
                        return
                    }
                    weakSelf.imageData = []
                    for imageId in imageIdData {
                        
//                        let urlStr = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imageUrlCode
                        
                        let urlStr = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imageId
                        
                        let imageUrl = URL(string: urlStr)
                        weakSelf.imageData.append(imageUrl!)
                    }
                    weakSelf.collectionView.reloadData()
                }else {
                    if imageCollectModel.success {
                        showErrorHUD(errStr: "删除成功")
                        weakSelf.navigationController?.popViewController(animated: true)
                    }else
                    {
                        showErrorHUD(errStr: "删除失败")
                    }
                }
            }
        }
    }
    
    //获取已选择个数
    func selectedCount() -> Int {
        
        return self.collectionView.indexPathsForSelectedItems?.count ?? 0
    }
    
    //删除按钮点击
    @objc func deleteClick() {
        
        if selectedCount() == 0 {
            showErrorHUD(errStr: "请选择图片")
            return
        }
        
        let alert = UIAlertController(title: "是否删除选中图片", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let confirmAction = UIAlertAction(title: "确定", style: .destructive) { (action) in
            self.networkVc?.requestImageCollect(option: "3", fileCode: self.imageIds(), imageIdKey: "imageIds")
        }
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //发送按钮点击
    @objc func sendClick() {
        
        if selectedCount() == 0 {
            showErrorHUD(errStr: "请选择图片")
            return
        }
        
        let selectItems = self.collectionView.indexPathsForSelectedItems
        for indexP in selectItems! {
//            let cell = collectionView.cellForItem(at: indexP) as? CRImageCollectionViewCell
//            self.networkVc?.sendImageMessage(image: cell?.imageView.image ?? UIImage())
            let imageUrl = imageData[indexP.row].absoluteString
            
            let selectedFileCode = imageUrl.components(separatedBy: "/").last ?? ""
            self.networkVc?.sendGifImageMessage(imageUrl: imageUrl,fileCode:selectedFileCode)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func imageIds() -> String {
        let selectItems = self.collectionView.indexPathsForSelectedItems
        var imageId: String = ""
        for indexPath in selectItems! {
            let url = imageData[indexPath.row]
//            let fileCode = getQueryStringParameter(url: url.absoluteString, param: "fileId")
            //Adam 使用原生图片拼接展示方式，图片展示会错乱，所以改成了wap端拼接图片格式
            
            let fileCode = url.absoluteString.components(separatedBy: "/").last ?? ""
            imageId = imageId + (imageId.isEmpty ? "" : ",") + fileCode
            
//            imageId = imageId + (imageId.isEmpty ? "" : ",") + fileCode!
        }
        return imageId
    }
}

extension CRImageCollectViewController: UICollectionViewDelegate {
    
    //单元格选中响应
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)
            as? CRImageCollectionViewCell {
            cell.isSelected = true
            //获取选中的数量
            let count = self.selectedCount()
            //如果选择的个数大于最大选择数
            if count > self.maxSelected {
                //设置为不选中状态
                collectionView.deselectItem(at: indexPath, animated: false)
                //弹出提示
                let title = "你最多只能选择\(self.maxSelected)张照片"
                let alertController = UIAlertController(title: title, message: nil,
                                                        preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title:"我知道了", style: .cancel,
                                                 handler:nil)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                cell.playAnimate()
            }
        }
    }
    
    //单元格取消选中响应
    func collectionView(_ collectionView: UICollectionView,
                        didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)
            as? CRImageCollectionViewCell {
            cell.isSelected = false
            cell.playAnimate()
        }
    }
}

extension CRImageCollectViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionViewCell", for: indexPath) as! CRImageCollectionViewCell
        
        let imageUrl = imageData[indexPath.row] as URL
        cell.imageView.kf.setImage(with: ImageResource(downloadURL: imageUrl), placeholder: UIImage(named: "image_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.isSelected = false
        
        return cell
    }
    
    
}
