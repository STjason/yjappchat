//
//  CRChatController.swift
//  Chatroom
//
//  Created by admin on 2019/7/10.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import YogaKit
import PKHUD
import YPImagePicker
import IQKeyboardManagerSwift
import HandyJSON
import Kingfisher
import MBProgressHUD
import AudioToolbox
import AVFoundation
import BSImagePicker
import Photos
import SocketIO

////键盘触发者
//enum CRKeyboardType {
//    case keyboardFollowField //跟单金额输入框
//    case keyboardFollowOthers //其他
//}

/**发送通知*/
let KMESSAGENOTI:Int = 999

class CRChatController: CRBaseController{
    var winPrizeView:CRWinPrizeListView? //中奖榜单，盈利榜单视图
    var winPrizeLists:[CRWinningListItem]? { //中奖榜单
        didSet {
            winPrizeView?.updateDatas(type: 2, datas: winPrizeLists)
        }
    }
    
    var profitPrizeLists:[CRWinningListItem]? { //盈利榜单
        didSet {
            winPrizeView?.updateDatas(type: 4, datas: profitPrizeLists)
        }
    }
      
    var backPreviousViewHandler:(() -> Void)?
    ///个人被禁言 1 禁言，0没禁言
    var isPersonBandSpeak:String = "1" {
        didSet {
            bandSpeak(isRoomBand: isRoomBandSpeak == "1" , isPersonBand: isPersonBandSpeak == "1")
        }
    }
    ///房间被禁言 1 禁言，0 没禁言
    var isRoomBandSpeak:String = "1" {
        didSet {
            bandSpeak(isRoomBand: isRoomBandSpeak == "1" , isPersonBand: isPersonBandSpeak == "1")
        }
    }

    var auto_share_bet_arrive_amount = "" //投注达到该金额，不需要确认，投注成功就分享注单
    var localIP = "" //本机IP
    var canReconnect = true //用以限定短时间内不多次重复重连
    var historyStart = 0 //聊天室历史记录当前已有页数
    var historyCountPerPage = 30 //聊天室每一页拉取的历史记录
    //用户图像
    var userIconLists:[String] = []
    var planUser = 0 //用户自己是否是计划员，0非计划员，1计划员
    var userInfoCenter: CRUserInfoCenterItem? //侧边栏输赢信息，个人信息
    let welcomeLock = NSLock.init() //欢迎消息很多的时候，加入到数组中，当消息从视图移除时，从消息数组中删除消息
    var welcomList =  [CRWelcomeMsgItem]() //欢迎消息数组,当有大量消息时，用该数组为 该计划消息排队有序展示欢迎消息view，当消息从视图移除时，从消息数组中删除消息
    
    var authMsgModel:CRAuthMsgModel? //保存授权model，用以拼接代理房model到roomlist
    var drawer:CRDrawer? //抽屉
    /** 新增彩票计划数据回调 */
    var lotterySchemeModelHandle: ((_ lotterySchemeModel: CRLotterySchemeModel) -> Void)?
    /** 新增图片收藏数据回调 */
    var imageCollectModelHandle: ((_ imageCollectModel: CRImageCollectListModel) -> Void)?
    /** 当前录音剩余录制时长 s */
    var recordingDuration = 15
    /** 录音 15s限制倒计时定时器 */
    var recordingTimer : Timer?
    /**录音的唯一标识*/
    var voiceMsgUUID = ""
    var getInroomCount = 0
    //记录当前播放的音频动画图片
    var recordVoiceImage:CRVoiceImageView?
    ///默认-1表示第一次进入房间， 最新的房间列表选择索引, 
    var lastRoomIndex = -1
    ///之前的的房间infoModel
    var preRoomInfo:CRRoomInfoModel?
    var bombSoundEffect: AVAudioPlayer?
    var winnerTimer:Timer? //开奖结果显示
    var winnersInfo = [CRWinnersInfo]() //开奖结果数组
    var currentWinnerIndex = 0 //当前展示的开奖结果
    var lotteryHistoryController:CRLotteryHistoryController?
    //
    lazy var imageProgressView:CRProgressView =  CRProgressView()
    
//    var keyboardType = CRKeyboardType.keyboardFollowOthers
    //MARK: - 彩种数据
//    var lastResultTime = 0
    let offset:Int = 1//偏差1秒
    var tickTime:Int64 = 0////倒计时查询最后开奖的倒计时时间
    var lastKaiJianResultTimer:Timer?//查询最后开奖结果的倒计时器
    /**历史记录的图片数据源*/
    var imageUrls:[String] = []
    /**敏感词汇数组*/
    var prohibitLanguageArray:[CRProhibitLanguageSourceItem] = []
    //初始化音频播放对象，将音频播放对象，作为试图控制器的类的属性
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    
    //MARK: - 跟单相关
    var betInfo:CRShareBetInfo? //分享注单的注单信息
    
    //MARK: 跟单封盘倒计时
    ////////////////////////////////////////////////////////////
    var endlineTouzhuTimer:Timer?//距离停止下注倒计时器
    var disableBetCountDownTimer:Timer?//禁止下注倒计时器
    
    var endBetDuration:Int = 0;//秒
    var endBetTime:Int = 0//距离停止下注的时间
    
    var userPasswordKey:[String:[[String:String]]] = [String:[[String:String]]]()
    
    var roomPasswords:[[String:String]] = [[String:String]]()
    
    //禁言 用于禁用tabbar上的按钮 用户被禁言 可投注和查看彩票计划
    var prohibit:Bool = false //个人禁言
    var roomProhibit:Bool = false //房间禁言
    var isPrivateChat = false  //当前是否是私聊 页面
    

    /**聊天的背景ImageV*/
    lazy var tableViewBackView:UIImageView = {
        let tableViewImage = UIImageView()
        return tableViewImage
    }()

    
    var currentQihao:String = "" {
        didSet {
            let value = "第 \(self.currentQihao)期"
            self.numSelectArea.topView.updateCurrentPeriod(period: value)
        }
    }//当前期号
    
    var betDeadlineDes = "" {
        didSet {
            let value = "第 \(self.currentQihao)期"
            self.numSelectArea.topView.updateCurrentPeriod(period: value)
        }
    }
    
    var disableBetTime:Int64 = 0//距离再次开始下注的剩余时间
    var countDownValue = "" {
        didSet {
            let time = "\(self.betDeadlineDes) \(countDownValue)"
            self.numSelectArea.topView.updateCountdown(time: time)
        }
    }
    
    var ago:Int64 = 0//开奖时间与封盘时间差,单位秒
    //---------
    var agoShare:Int64 = 0//开奖时间与封盘时间差,单位秒_跟单
    
    var countDownShareValue = "" {
        didSet {
            if let label = self.followBetPop.countdownLabel {
                label.text = countDownShareValue
            }
        }
    }
    /**语音时长*/
    var voiceDuartion:Float = 0
    var voiceFileName:String = ""

    
    ///跟单弹窗
    lazy var followBetPop = self.contentView.messageArea.followBetPop
    
    var endlineTouzhuShareTimer:Timer?//距离停止下注倒计时器_跟单页面面
    var disableBetCountDownShareTimer:Timer?//禁止下注倒计时器_跟单页面面
    
    var disableBetShareTime:Int64 = 0//距离再次开始下注的剩余时间_跟单
    var endBetShareDuration:Int = 0;//秒_跟单页面面
    var endBetShareTime:Int = 0//距离停止下注的时间_跟单页面面
    
    var previousResults = "" //最新开奖结果期号
    var previousShareResults = "" //最新开奖结果期号_跟单页面面
    
    var currentShareQihao:String = "" {
        didSet {
            self.followBetPop.updatePeriod(value: currentShareQihao)
        }
    }//当前期号
    
    var betDeadSharelineDes = "" {
        didSet {
            let value = "\(self.betDeadSharelineDes)"
            self.followBetPop.betStatusLabel.text = value
        }
    }
    
    ////////////////////////////////////////////////////////////
    var moneyTotal = 0
    var peilvs:[BcLotteryPlay]?
    var order:[OrderDataInfo]?
    var sectionModels = [[OrderDataInfo]]()
    var lhcLogic = LHCLogic2()//特殊六合彩玩法的处理类
    var peilvListDatas:[BcLotteryPlay] = []//赔率版彩票列表数据源
    var honest_orders:[PeilvOrder] = []////信用版所有下注主单列表
    var honest_odds:[HonestResult] = []//信用版用户选择的侧边打玩法对应的所有小玩法对应的所有赔率列表数据
    var official_orders:[OfficialOrder] = []//官方版所有下注主单列表
    var subPlayName = ""
    var subPlayCode:String = ""
    var officalBonus:Double?
    var cpTypeCode:String = "" //彩票类型代号
    var cpBianHao:String = "";//彩票编码
    var cpName:String = ""
    var cpVersion:String = VERSION_1;//当前彩票版本
    var officail_odds = [PeilvWebResult]()//官方版所有赔率
    var selectedBeishu = 1;//选择的倍数
    var current_rate:Float = 0;//当前拖动选择的反水比例
    var lhcSelect = false;//是否在奖金版本中选中了六合彩，十分六合彩
    var selectMode = ""  { //金额模式，元模式
        didSet {
            if selectMode == YUAN_MODE {
                multiplyValue = 1.0
            }else if selectMode == JIAO_MODE {
                multiplyValue = 0.1
            }else if selectMode == FEN_MODE {
                multiplyValue = 0.01
            }
            self.numSelectView.multiplyValue = self.multiplyValue
            self.numSelectView.updateBottomUI()
        }
    }
    var multiplyValue:Float = 1.0
    var awardNum = ""
    var fixRateStep:Float = 0//返水条加减点击步长，后台配置
    var selectZhuShu = 0 //选中注数
    var selectMoney:Float = 0 //投注金额
    var current_odds:Float = 0;//当前奖金或赔率
    var meminfo:Meminfo?
    var selected_rule_position = 0;//用户在侧边玩法栏选择的玩法位置
    var mKeyBoardAnimateDuration: Double = 0
    var mKeyBoardHeight: CGFloat = 0
    var lastDifY: CGFloat = 0
    var animateOption: UIView.AnimationOptions!
    var mUserInfo: Dictionary<AnyHashable, Any>!
    var oldOffsetY: CGFloat = 0
    var allLotteries:[LotteryData]? //玩法数据，所有类型的LotteryData
    
    //MARK: - 聊天室
//    var shareBetInfo = CRShareBetInfo() //本地生成，用于展示分享注单 和跟单页面
    var shareOrders = [CRShareOrder]()
    var shareBetModels = [CRShareBetModel]() //存放分享注单
    var shareBetModel:CRShareBetModel? //分享注单
    var msgModel = CRMessageModel()
    var roomList = [CRRoomListModel]()
    var roomInfo:CRRoomInfoModel? //进入房间成功后获得的房间信息
    /**业务系统的类型：1会员，2代理，3总代理，4普通试玩账号*/
    var userAccountType:Int = 1
    /**用户代理*/
    var agentUserCode:String = ""
    /**前端管理员角色*/
    var userType:Int = 1
    /**房间名称*/
    var roomName:String = ""
    
    var agentRoomHost:String = "" {
        didSet {
            agentRoomHostSelf = agentRoomHost == "1"
        }
    }
    ///聊天室内投注逻辑
    var lotteryLogic = CRLotteryLogic.shareInstance()
    
    var fileCode:String = ""
    
    var accountBanlance:Double = 0.0 {
        didSet {
            self.contentView.betArea.numSelectArea.topView.updateBalance(balance: "\(accountBanlance)元")
        }
    }
    
    var chat_privateRoomId = "" // 当前私聊房间id
    var chat_passiveUserId = "" //被邀请私聊的 userId
    var passiveUser:CROnLineUserModel? //被邀请私聊用户的数据
    //进入房间的ID
    var chat_roomId:String = ""
    //进入房间的userId
    var chat_userId:String = ""
    //是否从推送通知过来
    var isNotiFrom:Bool = false
    var chat_stationId:String = ""

    lazy var numSelectArea:CRNumSelectArea = {
       return self.contentView.betArea.numSelectArea
    }()
    
    lazy var numSelectView:CRNumSelectView = {
        return self.contentView.betArea.numSelectArea.numSelectView
    }()
    
    var moreKeyboardHelper:CRMoreKeyboardHelper?
    var lastStatus:CRChatBarStatus?
    var curStatus:CRChatBarStatus?
    /**红包的领取明细*/
    lazy var redPacketDetailView: CRReceiveRedPacketDetailView = {
        let view = CRReceiveRedPacketDetailView(frame: UIScreen.main.bounds)
        return view
    }()

    var recentResults:[BcLotteryData] = [] { //最近开奖结果
        didSet {
            if recentResults.count == 0 || isEmptyString(str: recentResults[0].qiHao) || isEmptyString(str: recentResults[0].haoMa) {
                
                return
            }
            let firstResult:BcLotteryData = recentResults[0]
            
            let qihaoStr = trimQihao(currentQihao: firstResult.qiHao)
            let lotteryStr = String.init(format: "第 %@期", qihaoStr)
            let haomaArr = firstResult.haoMa.components(separatedBy: ",")
            
            var ballWidth:CGFloat = 30
            var small = false
            if isSaiche(lotType: self.cpTypeCode){
                ballWidth = 20
            }else if isFFSSCai(lotType:self.cpTypeCode){
                ballWidth = 30
                small = false
            }else if isXYNC(lotType: self.cpTypeCode){
                ballWidth = 20
            }
            
            let topView = self.contentView.betArea.numSelectArea.topView
            
            topView.updatePreviousPeriod(period: lotteryStr)
            
            //如何需要计算六合彩类彩种的生肖；这里需要根据开奖时间计算出当前这一期开奖结果所在的农历年份
            if let numViews = topView.numViews {
                numViews.basicSetupBalls(nums: haomaArr, offset: 0, lotTypeCode: self.cpTypeCode, cpVersion: self.cpVersion,ballWidth: ballWidth,small: small,gravity_bottom:false,ballsViewWidth: numViews.width,isBetTopView:true,time:firstResult.date,fromChatRoom:true,isChatBottom:true)
            }
        }
    }
    
//    var selected_rule_position = 0;//用户在侧边玩法栏选择的玩法位置
//
//    var mKeyBoardAnimateDuration: Double!
//
//    var mKeyBoardHeight: CGFloat!
//
//    var animateOption: UIView.AnimationOptions!
//
//    var mUserInfo: Dictionary<AnyHashable, Any>!
    
    var isKeyboardShowed = false
    ///冷热遗漏数据
    var codeRank = [CodeRankModel]() {
        didSet {
            self.contentView.betArea.numSelectArea.numSelectView.codeRank = codeRank
        }
    }
    
    //当前显示的LotteryDatas（官方，或者信用，或者其他）
    var lotteryModels = [LotteryData]() {
        didSet {
            self.contentView.betArea.updateWithModels(models: lotteryModels)
        }
    }
    
    var lotteryData = LotteryData() { //当前选择的彩种
        didSet {
            self.updateLocalConstants(lotData: lotteryData)
            
            self.contentView.betArea.numSelectArea.numSelectView.updateLocalConstants(lotData: lotteryData) {[weak self] (lotteryName) in //lotteryName 彩种的名字
                guard let weakSelf = self else {return}
                
                //替换当前彩种的名字
                let officalpeilv = weakSelf.lotteryData.lotVersion == 1 ? "[官]" : "[信]"
                let name = "\(lotteryName)\(officalpeilv)"
                
                let label = UILabel()
                label.font = UIFont.systemFont(ofSize: 16)
                label.text = name
                let size = label.sizeThatFits(CGSize.init(width: 120, height: CGFloat(MAXFLOAT)))
                
                let lotterySegment = CRLotterySegment.init(leftButtonWidth: size.width + 15)
                lotterySegment.updateLotteryName(name: name)
                
                //切换到彩种页面
                lotterySegment.rightClickHandler = {[weak self] in
                    guard let weakSelf = self else {return}
                    lotterySegment.show(show: false)
                    weakSelf.contentView.betArea.showLotteryArea()
                }
                
                let segmentWidth = size.width + 15 + lotterySegment.buttonRightSize.width
                let segment = weakSelf.contentView.betArea.segment
                weakSelf.contentView.betArea.lotterySegment = lotterySegment
                
                segment.addSubview(weakSelf.contentView.betArea.lotterySegment!)
                
                weakSelf.contentView.betArea.lotterySegment!.snp.remakeConstraints({ (make) in
                    make.top.bottom.equalTo(0)
                    make.left.equalTo(20)
                    make.width.equalTo(segmentWidth)
                })

                UIView.animate(withDuration: 0, animations: {
                    segment.layoutIfNeeded()
                })
                
                weakSelf.contentView.betArea.numSelectArea.topView.updateShowRuleView(isShowRuleView: 1)
                weakSelf.contentView.betArea.numSelectArea.showNumSelectView()
                weakSelf.getCountDownByCpCodeAction()
                
                weakSelf.netLastOpenResult()
                weakSelf.createLastResultTimer()
            }
        }
    }
    
    //当前选择的侧边玩法
    var selectPlay = BcLotteryPlay() {
        didSet {
            let numSelectArea = self.contentView.betArea.numSelectArea
            
            //快捷 一般，冷热 遗漏的显示隐藏
            let isPeilv = self.contentView.betArea.numSelectArea.numSelectView.isPeilvVersion()
            self.contentView.betArea.numSelectArea.topView.updateHotColdOrFastNormal(isPeilv: isPeilv)
            
            //根据条件将peilvListDatas装载到集合里,用以保留已选中数据,以达到多玩法投注
            let numSelectView = numSelectArea.numSelectView
            handlePeilvListDatasSuperSet(peilvListDatas: numSelectView.peilvListDatas, peilvListDatasSuperSet: numSelectView.peilvListDatasSuperSet)
            
            //更新当前玩法的名称
            let firstRuleName = isEmptyString(str: selectPlay.fakeParentName) ? selectPlay.name : selectPlay.fakeParentName
            numSelectArea.topView.updateRule(name: firstRuleName)
            
            //侧边玩法栏列表项点击事件，号码选择球的操作
            self.contentView.betArea.numSelectArea.numSelectView.handlePlayRuleClick(playData: selectPlay, position: self.selected_rule_position,fromPlayCellSelect:true,handler: {[weak self] (result) in
                if let weakSelf = self {
                    if weakSelf.isPeilvVersion() {
                        //设置赔率版本的slider最大赔率
//                        numSelectArea.numSelectView.update_honest_seekbar(handler: {(maxOddResult) in
//                            numSelectArea.confirmBetArea.peilvBottom.creditbottomTopSlider.setupLogic(odd: maxOddResult)
//                        })
                    }else {
                        if let oddSlider = weakSelf.contentView.betArea.numSelectArea.confirmBetArea.officalBottom.oddSlider {
                            oddSlider.setupLogic(odd: result, resetValue: false)
                        }
                    }
                }
            })
        }
    }
    
    var playRules = [BcLotteryPlay]() { //所有叶子玩法列表数据
        didSet {
            self.contentView.betArea.numSelectArea.betRuleView.updateRulesWith(models: playRules)
        }
    }
    

    //MARK: - 聊天室
//    var msgModel = CRMessageModel()
//    var roomList = [CRRoomListModel]()
//    var roomInfo:CRRoomInfoModel?
////    var msgItems = [CRMessageModel]() //聊天消息对象数组
    var messageItems = [CRMessage]()
    //用户icon
    var userIconUrl:String = ""
    var userNickname:String = ""
    var userLevelName:String = ""
    var userLevelIcon:String = ""
    var userAccount = ""
    var userNativeAccount = ""
    //胜率
    var userWinRate:Float = 0
//
//    ///聊天室内投注逻辑
//    var lotteryLogic = CRLotteryLogic.shareInstance()
    
    //聊天页面
    lazy var contentView:CRChatView = {
        if self.view != nil {
            let size = self.view.frame.size
            let view = CRChatView.init(frame: CGRect.init(x: 0.0, y: CGFloat(KNavHeight), width: size.width, height: (size.height - CGFloat(KNavHeight))))
            view.backgroundColor = UIColor.white
            view.messageArea.chatMessageViewDelegate = self
            return view
        }else {
            return CRChatView()
        }
    }()
    
    var emojiKBHelper:CREmojiKeyboardHelper?
    /**记录上一次推送过来的消息*/
    var recordMsgUUIDContentItem:CRRedPacketContentItem = CRRedPacketContentItem()
    
    //MARK: - 提取到首页的聊天室的初始化
    //进入聊天室页面根据条件，进行聊天室的初始化
    ///获取授权的参数
    func loginAndAuthGetParameters() {
        
        HUDView = getHUD(text: "授权中...")
        CRNetManager.shareInstance().netAuthenticateLogin(paramters: [:], method: .get) {[weak self] (success, msg, model) in
            
            guard let weakSelf = self else {
                return
            }
            
            weakSelf.HUDView.hide(animated: true)
            
            if !success {
                showToast(view: weakSelf.view, txt: msg)
                return
            }
            
            guard let m = model else {
                return
            }
            
            weakSelf.netAuthenticate(model: m)
        }
    }
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if isSocketConnect {
//            socketHandler()
//            observeSocket()
//            sendMessageloginRoom(model: authenticateModel)
//        }else {
            loginAndAuthGetParameters()
//        }
        
        isRoomBandSpeak = "1"
        //加截键盘
        loadKeyboard()
        
        view.insertSubview(contentView, belowSubview: navigationBar)
        
        //默认隐藏导航单右边button，遥控器
        navigationBar.rightView.isHidden = true
        contentView.messageArea.showRemoter(show:false)
        
        setupChatViewClosure() //闭包设置
        moreKeyboardHelper = CRMoreKeyboardHelper()
        setChatMoreKeyboardData(moreKeyboardData: (self.moreKeyboardHelper?.chatMoreKeyboardData)!)
        //          [[NSNotificationCenter defaultCenter] addObserver:self selectoxr:@selector(resetChatVC) name:NOTI_CHAT_VIEW_RESET object:nil];
        emojiKBHelper = CREmojiKeyboardHelper.sharedKBHelper
        emojiKBHelper?.emojiGroupDataByUserID(userID: "", complete: { (emojiDatas) in
            //设置emoji键盘数据源
            self.setChatEmojiKeyboard(emojiKeyboardData: emojiDatas)
        })
        NotificationCenter.default.addObserver(self, selector: #selector(resetChatVC), name: NSNotification.Name(rawValue: NOTI_CHAT_VIEW_RESET), object: nil)
        
        
        /// 新增图片收藏
        NotificationCenter.default.addObserver(self, selector: #selector(imageCollect(notifi:)), name: NSNotification.Name(rawValue: "imageCollect"), object: nil)
//        self.contentView.announceView.configWith(title: "唧唧复唧唧，木兰当户织，不闻机杼声，唯闻女叹息。问女何所思，问女何所忆")
      
        contentView.inputAreaView.chatBarDelegate = self
        contentView.inputAreaView.keyboradDelegate = self
        contentView.inputAreaView.emojiKeyboardDelegate = self
    }
    
    @objc func imageCollect(notifi: Notification) {
        print("------图片收藏")
        requestImageCollect(option: "2", fileCode: notifi.userInfo!["fileCode"] as! String)
    }
    
    //释放控制器
    deinit {
        recordingTimer?.invalidate()
        recordingTimer = nil
        
        contentView.lotteryBanner.refreshDataTimer = nil
        contentView.lotteryBanner.refreshDataTimer?.invalidate()
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isChatActive = true
        currentIsChat = true
        contentView.hasCloseWinnerInfo = false
        
        YiboPreference.setIsChatBet(value: "on" as AnyObject)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name:UIResponder.keyboardDidShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        //开启拉取开奖结果定时器(暂时)
        contentView.lotteryBanner.startTimer()
        
        if let view = getSemicycleButton() {
            view.isHidden = true
        }
        
        if let ip = GetIPAddresses() {
            localIP = ip
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isChatActive = false
        currentIsChat = false
        contentView.hasCloseWinnerInfo = true
        winnerTimer?.invalidate()
        
        YiboPreference.setIsChatBet(value: "off" as AnyObject)
        CRNetManager.shareInstance().cancelRequest()
        //暂时停止拉取开奖结果定时器
        contentView.lotteryBanner.refreshDataTimer?.invalidate()
        contentView.lotteryBanner.refreshDataTimer = nil
        TLAudioPlayer.shared().stopPlayingAudio()
        self.recordVoiceImage?.stopPlayingAnimation()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "imageCollect"), object: nil)
        
        if let view = getSemicycleButton() {
            view.isHidden = showSemicycleButton()
        }
        /// 退出时校验登录状态
        kCheckLoginStauts()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
        //        contentView.lotteryBanner.refreshDataTimer?.invalidate()
        //        contentView.lotteryBanner.refreshDataTimer = nil
        //        IQKeyboardManager.sharedManager().enable = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - UI事件
    //MARK: 初始化 MBProgressHUD
    func getHUD(text:String = "") -> MBProgressHUD {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        if !text.isEmpty {
            hud.label.text = text
        }
        hud.label.textColor = UIColor.white
        hud.bezelView.color = UIColor.black
        hud.label.layoutMargins = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        hud.backgroundView.style = .solidColor
        hud.removeFromSuperViewOnHide = true
        hud.isUserInteractionEnabled = false
//        hud.activityIndicatorColor = UIColor.white
        
        return hud
    }
    
    lazy var HUDView:MBProgressHUD = {
        let hud = getHUD()
        return hud
    }()
    
    override func leftBarItemTapAction() {
        //返回上一个界面
        super.onBackClick()
        backPreviousViewHandler?()
    }
    //选择房间列表
    override func titleBarItemTapAction() {
        contentView.inputAreaView.inputField.resignFirstResponder()
        //展
        if self.contentView.betArea.getIsShow() {
            //展开的
           self.contentView.hideBetArea()
        }
        if self.contentView.roomsTopView.isShow {
            self.contentView.showGrayBackgroundView(show: false)
        }else {
            self.contentView.showGrayBackgroundView(show: true)
            
            self.contentView.roomsTopView.show()
            self.contentView.bringSubviewToFront( self.contentView.roomsTopView)
        }
    }
    
    override func rightBarItemTapAction() {
        setupDrawer(andShow: true)
    }
    
    func showUserProfileCtrl() {
        //设置 跳转个人中心
        let userProfileCtrl = CRUserProfileInfoCtrl()
        userProfileCtrl.commitUserInfoClourse = {[weak self](userInfoItem:CRUserInfoCenterSource) in
            guard let weakSelf = self else {return}

            //拉取历史记录
            if userInfoItem.avatar.length > 0{
                weakSelf.userIconUrl = userInfoItem.avatar
            }
            if userInfoItem.nickName.length > 0{
                weakSelf.userNickname = userInfoItem.nickName
            }
            weakSelf.getMsgHistoryRecordData()
        }
        userProfileCtrl.roomId = self.msgModel.roomId
        userProfileCtrl.stationId = self.msgModel.stationId
        userProfileCtrl.avatar = self.userIconUrl
        userProfileCtrl.nickName = self.userNickname
        
        navigationController?.pushViewController(userProfileCtrl, animated: true)
        
        userProfileCtrl.updateUserInfoHandler = {[weak self] () in //获取用户avatar和nickName
            guard let weakSelf = self else {return}
            weakSelf.sendMsgUserIconAndnickNameData()
        }
        
        userProfileCtrl.updateAvatarListHandler = {[weak self] in //获取默认图片列表
            guard let weakSelf = self else {return}
            weakSelf.sendMsgGetUserAvatarList(userId: CRDefaults.getUserID())
        }
        
        userProfileCtrl.updateEditInfoHandler = {[weak self] in //编辑用户信息
            guard let weakSelf = self else {return}
            weakSelf.sendMsgSubmitInfo()
        }
        
        userProfileCtrl.updateBetInfoHandler = {[weak self] in //今日输赢信息 
            guard let weakSelf = self else {return}
        }
        
        userProfileCtrl.shareGainsAndLossesForTodayHandler = {[weak self] gainsLossesItem  in
            guard let weakSelf = self else {return}
            //今日分享盈亏
            if !weakSelf.prohibit  && !weakSelf.roomProhibit{
                 weakSelf.shareGainsAndLossesForToday(gainsLossesItem: gainsLossesItem)
            }else{
                showToast(view: weakSelf.view, txt: "禁言中无法发送消息!")
            }
           
            
        }
    }
    
    
    /// 设置抽屉
    ///
    /// - Parameter andShow: true 同时显示，false 设置单不显示
    func setupDrawer(andShow:Bool) {
        drawer = CRDrawer.init(userType:userType,superView: self.view, width: contentView.frame.size.width * 0.6, duration: 0.5)

        drawer?.subviewsBGView.header.delegate = self
        drawer?.drawerDelegate = self

        if andShow {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.drawer?.openDrawer(open: true)
                
                if let info = self.userInfoCenter,let drawerHeader = self.drawer?.subviewsBGView.header {
                    drawerHeader.updateUserInfo(userItem: info)
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.contentView.endEditing(true)
            }
            
        }else {
            drawer?.isHidden = true
            drawer?.closeAction()
        }
        
        setupDrawerClosure(drawer:drawer)
        
        drawer?.subviewsBGView.header.setupData()
        drawer?.subviewsBGView.setupData()
        
        sendMsgGetUserAvatarList(userId: chat_userId)
    }
    
    func loadKeyboard(){
        self.emojiKeyboard.keyboardDelegate = self;
        self.emojiKeyboard.emojiKeybaordDelegate = self
        self.moreKeyboard.keyboardDelegate = self
    }
    
    //MARK: - DATA
    //MARK: 更新公告列表内容
    func updateNoticeView(notice:CRNoticeResult){
        self.contentView.announceView.configWith(title: notice.body)
    }
    
    //MARK: 更新聊天室房间背景
    func updateRoomBg(img:String){
        //
        tableViewBackView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - KTabBarHeight - 44)
        tableViewBackView.kf.setImage(with: URL(string: img), placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: nil)
        contentView.insertSubview(tableViewBackView, at: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK://键盘弹出收起等操作
    @objc override func keyboardWillShow(notification: NSNotification) {
        contentView.messageArea.tapAvatarPop?.dismiss() //键盘变化，隐藏菜单
        if curStatus == CRChatBarStatus.KeyboardFollowField {
            
            if !self.followBetPop.isHidden {
                if let window = UIApplication.shared.keyWindow {
                    self.followBetPop.snp.remakeConstraints { (make) in
                        make.bottom.equalTo(window.snp.bottom).offset(-320)
                        make.centerX.equalTo(window.snp.centerX)
                        make.height.equalTo(320)
                        make.width.equalTo(270)
                    }
                    UIView.animate(withDuration: 0.3) {
                        self.contentView.messageArea.floatView.layoutIfNeeded()
                    }
                }
            }
            return
        }
        
        if curStatus != CRChatBarStatus.Keyboard {
            return
        }
        //间距弹起时间
        let keyboardDuration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0.25
        self.scrollTableViewToBottom(animated: true, duartime: 0)
//        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
    }
    @objc func keyboardDidShow(notification:NSNotification){
        if curStatus == CRChatBarStatus.KeyboardFollowField {
            return
        }
        if (curStatus != .Keyboard) {
            return;
        }
        if (lastStatus == .More) {
            self.moreKeyboard.dismissWithAnimation(animation: false)
        }
        else if (lastStatus == .Emoji) {
            self.emojiKeyboard.dismissWithAnimation(animation: false)
        }
//          self.scrollTableViewToBottom(animated: true, duartime: 0.25)
        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
    }
    
    @objc func keyboardFrameWillChange(notification:NSNotification){
        if curStatus == CRChatBarStatus.KeyboardFollowField {
            
            return
        }
        if curStatus != .Keyboard && lastStatus != .Keyboard {
            return
        }
        if let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]as? NSValue)?.cgRectValue{
            if lastStatus == .More || lastStatus == .Emoji {
                if keyboardFrame.size.height <= 215{
                    return
                }
            }else if (curStatus == .Emoji || curStatus == .More){
                return
            }
            contentView.inputAreaView.snp.makeConstraints { (make) in
                make.bottom.equalTo(contentView).offset(-keyboardFrame.size.height)
            }
            view.layoutIfNeeded()
            contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
        }
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        contentView.messageArea.tapAvatarPop?.dismiss() //键盘变化，隐藏菜单
        
        if curStatus == CRChatBarStatus.KeyboardFollowField {
            
            if let window = UIApplication.shared.keyWindow {
                if !self.followBetPop.isHidden {
                    self.followBetPop.snp.remakeConstraints { (make) in
                        make.centerY.equalTo(window.snp.centerY)
                        make.centerX.equalTo(window.snp.centerX)
                        make.height.equalTo(320)
                        make.width.equalTo(270)
                    }
                    
                    UIView.animate(withDuration: 0.3) {
                        self.contentView.messageArea.floatView.layoutIfNeeded()
                    }
                }
            }
            
            return
        }
        
        if (curStatus != .Keyboard && lastStatus != .Keyboard) {
            return;
        }
        if (curStatus == .Emoji || curStatus == .More) {
            return;
        }
        contentView.tabbar.snp.makeConstraints { (make) in
            make.height.equalTo(KTabBarHeight)
        }
        contentView.inputAreaView.snp.remakeConstraints { (make) in
            make.left.right.equalTo(contentView)
        }
     
        contentView.betArea.transform = CGAffineTransform.identity
        view.layoutIfNeeded()
    }
    
    func updateLocalConstants(lotData:LotteryData?) {
//        self.lotData = lotData
//        
//        if let lotName = lotData?.name{
//            self.cpName = lotName
//            
//            handler(lotName)
//        }
//        //        if let lotCode = lotData?.czCode{
//        //            self.czCode = lotCode
//        //        }
        
        contentView.betArea.numSelectArea.betRuleView.selected_rule_position = 0
        
        if let lotAgo = lotData?.ago{
            self.ago = lotAgo
            self.disableBetTime = lotAgo
        }
        
        if let lotCpCode = lotData?.code{
            self.cpBianHao = lotCpCode
            //            lotCpCodeChangedHandler?(lotCpCode)
        }
//        //        if let lotteryICON = lotData?.lotteryIcon {
//        //            self.lotteryICON = lotteryICON
//        //        }
        if let lotCodeType = lotData?.lotType{
            self.cpTypeCode = String.init(describing: lotCodeType)
        }
        if let lotVersion = lotData?.lotVersion{
            self.cpVersion = String.init(describing: lotVersion)
        }
        
        self.tickTime = self.ago + Int64(self.offset)
        if isSixMark(lotCode: self.cpBianHao){
            lhcSelect = true
            self.cpVersion = VERSION_2
        }
        
        self.contentView.betArea.numSelectArea.confirmBetArea.updateWith(isPeilv: self.isPeilvVersion())
    }
    
    func isPeilvVersion() -> Bool {
        return CRBetLogic.isPeilvVersion(cpVersion: self.cpVersion, lhcSelect: self.lhcSelect)
    }
    
    func show_mode_switch_dialog(sender:UIButton,handler:@escaping (_ title: String) -> ()) -> Void {
        let alert = UIAlertController.init(title: "模式切换", message: nil, preferredStyle: .actionSheet)
        
        let mode = YiboPreference.getYJFMode();
        if YiboPreference.getYJFMode() == YUAN_MODE{
            let action = UIAlertAction.init(title: "元", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "元",handler: {(contents) in handler(contents)   })
            })
            alert.addAction(action)
        }else if YiboPreference.getYJFMode() == JIAO_MODE{
            let action1 = UIAlertAction.init(title: "元", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "元",handler: {(contents) in handler(contents)   })
            })
            let action2 = UIAlertAction.init(title: "角", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "角",handler: {(contents) in handler(contents)   })
            })
            alert.addAction(action1)
            alert.addAction(action2)
        }else if YiboPreference.getYJFMode() == FEN_MODE{
            let action1 = UIAlertAction.init(title: "元", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "元",handler: {(contents) in handler(contents)   })
            })
            let action2 = UIAlertAction.init(title: "角", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "角",handler: {(contents) in handler(contents)   })
            })
            let action3 = UIAlertAction.init(title: "分", style: .default, handler: {(action:UIAlertAction) in
                self.selectModeFromDialog(title: "分",handler: {(contents) in handler(contents)   })
            })
            alert.addAction(action1)
            alert.addAction(action2)
            alert.addAction(action3)
        }
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        //ipad使用，不加ipad上会崩溃
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        self.present(alert,animated: true,completion: nil)
    }
    
    private func selectModeFromDialog(title:String,handler:(_ title: String) -> ()) -> Void{
        if title == "元"{
            self.selectMode = YUAN_MODE
            multiplyValue = 1.0
        }else if title == "角"{
            self.selectMode = JIAO_MODE
            multiplyValue = 0.1
        }else if title == "分"{
            self.selectMode = FEN_MODE
            multiplyValue = 0.01
        }
        handler(title)
    }
    
    func selectMoneyFromDialog(money:String){
        if isEmptyString(str: money){
            return
        }
        self.numSelectArea.confirmBetArea.peilvBottom.creditbottomTopField.text = money
        self.numSelectView.betMoneyWhenPeilv = money
        
        self.numSelectView.updateBottomUI()
//        creditbottomTopField.text = money
    }
    
    func updateAccount(memInfo:Meminfo) {
        self.meminfo = memInfo
        if !isEmptyString(str: memInfo.balance){
            let leftMoneyName = "\(memInfo.balance)"
    
            if let value = Double(leftMoneyName) {
                self.accountBanlance = value
            }
        }
    }
    
    /// 检查是否存在没有金额的号码
    ///
    /// - Returns: true都有金额，false存在没金额的号码
    private func judgeOrderMoney() -> Bool {
        let peilvBottom = self.contentView.betArea.numSelectArea.confirmBetArea.peilvBottom
        if let moneyValueString = peilvBottom.creditbottomTopField.text,let moneyValue = Double(moneyValueString) {
            for (_,models) in self.sectionModels.enumerated() {
                for (_,data) in models.enumerated() {
                    data.money = moneyValue
                }
            }
            
            for (_,models) in self.sectionModels.enumerated() {
                for (_,data) in models.enumerated() {
                    if data.money == 0{
                        showToast(view: self.view, txt: "号码没有下注金额，请先输入金额")
                        return false
                    }
                }
            }

        }else {
            showToast(view: self.view, txt: "号码没有下注金额，请先输入金额")
            return false
        }
        
        return true
    }
    
    private func getDatasFromSectionDatas() -> [OrderDataInfo] {
        var datas = [OrderDataInfo]()
        
        for (_,models) in sectionModels.enumerated() {
            for (_,inModel) in models.enumerated() {
                datas.append(inModel)
            }
        }
        
        return datas
    }
    
    private func formatData() -> (data:[PeilvOrder],rateback:Float) {
        
        self.order = getDatasFromSectionDatas()

        self.order =  self.order ?? [OrderDataInfo]()
        handlesectionModels()
        
        var peilvOrders:[PeilvOrder] = []
        
        var rateback:Float = 0
        for (_,models) in self.sectionModels.enumerated() {
            for (_,data) in models.enumerated() {
                if data.money == 0{
                    showToast(view: self.view, txt: "号码没有下注金额，请先输入金额")
                    return (data:peilvOrders,rateback:0)
                }
                
                let order = PeilvOrder()
                order.i = data.oddsCode
                order.c = data.numbers
                order.d = data.cpCode
                order.a = Float(data.money)
                
                rateback = Float(data.rate)
                peilvOrders.append(order)
            }
        }
        
        return (data:peilvOrders,rateback:rateback)
    }
    
    //MARK: -信用下注，数据构造
    private func peilvBetAction() {
        
        self.order = self.order ?? [OrderDataInfo]()
        handlesectionModels()
        
//        for (_,models) in self.sectionModels.enumerated() {
//            for (_,data) in models.enumerated() {
//                moneyTotal += Int(data.money)
//            }
//        }
        
        moneyTotal = Int(selectMoney)
        
        let accoundMode = YiboPreference.getAccountMode()
        if accountBanlance < Double(moneyTotal) && accoundMode != 4{
            openChargeMoney(controller: self, meminfo: self.meminfo)
            showToast(view: self.view, txt: "余额不足，请充值")
            return
        }
        
        if !self.judgeOrderMoney() {
            return
        }
        
        let data:(data:[PeilvOrder],rateback:Float) = formatData()
        
        if data.data.count == 0 {
            showToast(view: self.view, txt: "注单数据错误")
            return
        }
        
        //遍历准备分享注单数据
        var orderInfos = [OrderDataInfo]()
        var peilvOrders = [PeilvOrder]()
        
        for model in data.data {
            if let orderDataInfo = self.getOrderDataInfoWith(peilvOrder: model)  {
                orderInfos.append(orderDataInfo)
                peilvOrders.append(model)
            }
        }
        
        self.do_peilv_bet(models: peilvOrders, orderDataInfos: orderInfos, rateback: self.current_rate)
    }
    
    //根据 PeilvOrder，获得对应的OrderDataInfo，用于分享注单使用
    private func getOrderDataInfoWith(peilvOrder:PeilvOrder) -> OrderDataInfo? {
        if let models = self.order {
            for model in models {
                if model.cpCode == peilvOrder.d {
                    model.zhushu = 1
                    return model
                }
            }
        }
        
        return nil
    }
    
    //MARK: 构造下注post数据
    private func buildPostData(models: [PeilvOrder]) -> [Dictionary<String,AnyObject>] {
        var bets = [Dictionary<String,AnyObject>]()
        for order in models{
            var bet = Dictionary<String,AnyObject>()
            bet["i"] = order.i as AnyObject
            bet["c"] = order.c as AnyObject
            bet["d"] = order.d as AnyObject
            bet["a"] = order.a as AnyObject
            bets.append(bet)
        }
        
        return bets
    }
    
    /** 注单数据分组处理 */
    private func handlesectionModels() {
        var sectionsModelDic = [String:[OrderDataInfo]]()
        guard let datas  = self.order else {
            return
        }
        
        for (_,data) in datas.enumerated() {
            if sectionsModelDic.keys.contains(data.subPlayName) {
                var newModels = sectionsModelDic["\(data.subPlayName)"]
                newModels?.append(data)
                sectionsModelDic["\(data.subPlayName)"] = newModels
            }else {
                sectionsModelDic["\(data.subPlayName)"] = [data]
            }
        }
        
        self.sectionModels.removeAll()
        
        for (_,value) in sectionsModelDic {
            sectionModels.append(value)
        }
    }

    
    //MARK: - 投注接口调用
    //MARK: 信用下注
    
    /*
     真正开始赔率下注
     @param order 下注注单
     @param rateback 用户选择的返水
     @param last 是否是最后一单投注
     */
    func do_peilv_bet(models:[PeilvOrder],orderDataInfos:[OrderDataInfo], rateback:Float){
        if sectionModels.isEmpty{
            showToast(view: self.view, txt: "没有需要提交的订单，请先投注!")
            return
        }
        
        let bets = buildPostData(models: models)
        
        let postData = ["lotCode":self.cpBianHao,"data":bets,"kickback":rateback] as [String : Any]
        if (JSONSerialization.isValidJSONObject(postData)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: postData, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            //do bet
            request(frontDialog: true, method: .post, loadTextStr: "正在下注...", url:DO_PEILVBETS_URL_V2,params: ["data":str!],
                    callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                        guard let weakSelf = self else {
                            return
                        }
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: weakSelf.view, txt: convertString(string: "下注失败"))
                            }else{
                                showToast(view: weakSelf.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = DoBetWrapper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                
                                //分享注单数据准备
                                let orderIds = result.content.orders.components(separatedBy: ",")
                                //                                let money = "\(weakSelf.moneyTotal)"
                                
                                var localShareOrders = [CRShareOrder]()
                                for (index, orderIds) in orderIds.enumerated() {
                                    let shareOrder = CRShareOrder()
                                    shareOrder.orderId = orderIds
                                    shareOrder.betMoney = "\(orderDataInfos[index].money)"
                                    localShareOrders.append(shareOrder)
                                }
                                
                                weakSelf.formatBetShareMsgModelYJ(models: orderDataInfos, shareOrders: localShareOrders)
                                
                                weakSelf.contentView.showOrHideBetArea()
                                
                                if  CRDefaults.getChatroomSendBetting(){
                                    //自动分享注单，不需要确认
                                    let auto_share_bet_amount = shouldAutoShareBet(askMoney: weakSelf.auto_share_bet_arrive_amount, realMoney: "\(weakSelf.selectMoney)")
                                    
                                    if auto_share_bet_amount {
                                        //用户禁言和房间禁言都要是关闭的才能发送注单消息
                                        if !weakSelf.prohibit && !weakSelf.roomProhibit{
                                            weakSelf.sendMsgShareBetMessage()
                                        }else{
                                            weakSelf.shareBetModels.removeAll()
                                            weakSelf.shareBetModel = nil
                                        }
                                    }else {
                                        let alert = UIAlertController.init(title: "温馨提示", message: "下注成功，是否发送到聊天室", preferredStyle: .alert)
                                        let confirmAction = UIAlertAction.init(title: "发送", style: .default, handler: { (_) in
                                            //用户禁言和房间禁言都要是关闭的才能发送注单消息
                                            if !weakSelf.prohibit && !weakSelf.roomProhibit{
                                                weakSelf.sendMsgShareBetMessage()
                                            }else{
                                                showToast(view: weakSelf.view, txt: "禁言中无法分享注单!")
                                                weakSelf.shareBetModels.removeAll()
                                                weakSelf.shareBetModel = nil
                                            }
                                        })
                                        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: { (_) in
                                            weakSelf.shareBetModels.removeAll()
                                            weakSelf.shareBetModel = nil
                                        })
                                        
                                        alert.addAction(cancelAction)
                                        alert.addAction(confirmAction)
                                        weakSelf.present(alert, animated: true, completion: nil)
                                    }
                                }

                                YiboPreference.saveTouzhuOrderJson(value: "" as AnyObject)
                                weakSelf.sectionModels.removeAll()
                                weakSelf.clearBetInfo()
                            }else{
                                if !isEmptyString(str: result.msg){
                                    weakSelf.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: weakSelf.view, txt: convertString(string: "下注失败"))
                                }
                                //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                                //所以此接口当code == 0时表示帐号被踢，或登录超时
                                if (result.code == 0 || result.code == -1) {
                                    loginWhenSessionInvalid(controller: weakSelf)
                                    return
                                }
                            }
                        }
            })
        }
    }
    
  //MARK: 官方投注接口调用
    ///官方投注接口
    func postBets(orders:[OrderDataInfo]) -> Void {
        
        //构造下注POST数据
        var bets = [Dictionary<String,AnyObject>]()
        
        for order in orders {
            var bet = Dictionary<String,AnyObject>()
            
            bet["i"] = order.oddsCode as AnyObject
            bet["c"] = order.numbers as AnyObject
            bet["n"] = order.zhushu as AnyObject
            bet["t"] = order.beishu as AnyObject
            bet["k"] = order.rate as AnyObject
            bet["m"] = order.mode as AnyObject
            bet["a"] = order.money as AnyObject
            
            bets.append(bet)
        }
        
        let postData = ["lotCode":self.cpBianHao,"data":bets,"zuihaoList":"","stopAfterWin":true] as [String : Any]
        if (JSONSerialization.isValidJSONObject(postData)) {
            let data : NSData! = try? JSONSerialization.data(withJSONObject: postData, options: []) as NSData
            let str = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue)
            request(frontDialog: true, method: .post, loadTextStr: "正在下注...", url:DO_BETS_URL_V2,params: ["data":str!],
                    callback: {[weak self] (resultJson:String,resultStatus:Bool)->Void in
                        guard let weakSelf = self else {
                            return
                        }
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: weakSelf.view, txt: convertString(string: "下注失败"))
                            }else{
                                showToast(view: weakSelf.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = DoBetWrapper.deserialize(from: resultJson){
                            if result.success{
                                
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                
                                //分享注单数据准备
                                let orderIds = result.content.orders.components(separatedBy: ",")
//                                let money = "\(weakSelf.moneyTotal)"
                                
                                var localShareOrders = [CRShareOrder]()
                                for (index, orderIds) in orderIds.enumerated() {
                                    let shareOrder = CRShareOrder()
                                    shareOrder.orderId = orderIds
                                    shareOrder.betMoney = "\(orders[index].money)"
                                    localShareOrders.append(shareOrder)
                                }
                                
                                weakSelf.formatBetShareMsgModelYJ(models: orders, shareOrders: localShareOrders)
                                
                                weakSelf.contentView.showOrHideBetArea()
                                
                                if CRDefaults.getChatroomSendBetting(){
                                    //自动分享注单，不需要确认
                                    let auto_share_bet_amount = shouldAutoShareBet(askMoney: weakSelf.auto_share_bet_arrive_amount, realMoney: "\(weakSelf.selectMoney)")
                                    
                                    if auto_share_bet_amount {
                                        if !weakSelf.prohibit && !weakSelf.roomProhibit{
                                            //用户禁言和房间禁言都要是关闭的才能发送注单消息
                                            weakSelf.sendMsgShareBetMessage()
                                        }else{
                                            weakSelf.shareBetModels.removeAll()
                                            weakSelf.shareBetModel = nil
                                        }
                                    }else {
                                        let alert = UIAlertController.init(title: "温馨提示", message: "下注成功，是否发送到聊天室", preferredStyle: .alert)
                                        let confirmAction = UIAlertAction.init(title: "发送", style: .default, handler: { (_) in
                                            
                                            if !weakSelf.prohibit && !weakSelf.roomProhibit{
                                                //用户禁言和房间禁言都要是关闭的才能发送注单消息
                                                weakSelf.sendMsgShareBetMessage()
                                            }else{
                                                showToast(view: weakSelf.view, txt: "禁言中无法分享注单!")
                                                weakSelf.shareBetModels.removeAll()
                                                weakSelf.shareBetModel = nil
                                            }
                                        })
                                        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: { (_) in
                                            weakSelf.shareBetModels.removeAll()
                                            weakSelf.shareBetModel = nil
                                        })
                                        
                                        alert.addAction(cancelAction)
                                        alert.addAction(confirmAction)
                                        weakSelf.present(alert, animated: true, completion: nil)
                                    }
                                }
                               
                                weakSelf.clearBetInfo()
                            }else{
                                if !isEmptyString(str: result.msg){
                                    weakSelf.print_error_msg(msg: result.msg)
                                }else{
                                    showToast(view: weakSelf.view, txt: convertString(string: "下注失败"))
                                }
                                //超時或被踢时重新登录，因为后台帐号权限拦截抛出的异常返回没有返回code字段
                                //所以此接口当code == 0时表示帐号被踢，或登录超时
                                if (result.code == 0 || result.code == -1) {
                                    loginWhenSessionInvalid(controller: weakSelf)
                                    return
                                }
                            }
                        }
            })
        }
    }

    ///注单确认页面,点击确定投注按钮
    func confirmBetInConfirmView() {
     
        if self.isPeilvVersion() {
            self.peilvBetAction()
        }else {
            let orders = fromBetOrder(official_orders: self.official_orders, subPlayName: self.subPlayName, subPlayCode: self.subPlayCode, selectedBeishu: self.selectedBeishu, cpTypeCode: self.cpTypeCode, cpBianHao: self.cpBianHao, current_rate: self.current_rate, selectMode: self.selectMode)
            
            var totalMoney:Float = 0
            for info in orders {
                totalMoney = totalMoney + Float(info.money)
            }
            
            //若总投注金额大于账户余额，跳转到支付页
            let accoundMode = YiboPreference.getAccountMode()
            
            if let meminfo = self.meminfo{
                if !isEmptyString(str: meminfo.balance){
                    let balance = Float(meminfo.balance)!
                    if totalMoney > balance && accoundMode != 4{
                        showToast(view: self.view, txt: "余额不足，请先充值")
                        openChargeMoney(controller: self, meminfo: meminfo)
                        return
                    }
                }
            }
            
            postBets(orders: orders)
        }
    }
    
    ///号码选择页面，点击确定投注按钮
    func confirmctionInNumSelectArea() {
        if isSixMark(lotCode: cpBianHao) && selectZhuShu > 200{
            showToast(view: self.view, txt: "最多选中200注")
            return
        }
        
        let numSelectArea = self.contentView.betArea.numSelectArea
        if isPeilvVersion() {
            
            let info = CRBetLogic.judgeBeforeBet(zhuShu: self.selectZhuShu, betMoney: self.selectMoney, accountMoney: self.accountBanlance)
            
            if info.0 {
                //跳转到确认注单页面
                numSelectArea.topView.updateShowRuleView(isShowRuleView: 2)
                numSelectArea.showConfirmArea()
                
                numSelectArea.numSelectView.gotoBetNewPeilvWith {[weak self] (datas, peilvs,lhcLogic, subPlayName, subPlayCode, cpTypeCode, cpBianHao, current_rate,cpName, cpVersion,lotteryICON) in
                    if let weakSelf = self {
                        weakSelf.order = datas
                        weakSelf.peilvs = peilvs
                        weakSelf.lhcLogic = lhcLogic
                        weakSelf.subPlayName = subPlayName
                        weakSelf.subPlayCode = subPlayCode
                        weakSelf.cpTypeCode = cpTypeCode
                        weakSelf.cpBianHao = cpBianHao
                        weakSelf.current_rate = current_rate
                        weakSelf.cpName = cpName
                        weakSelf.cpVersion = cpVersion
                        
                        //为注单确认页面赋值注单数据
                        let tuples = filterRepeatData(datas: datas)
                        let newDatas = tuples.0
                        let filterdDatas = tuples.1
                        if filterdDatas.count > 0 {
                            showToast(view: weakSelf.view, txt: "已过滤 \(filterdDatas.count)条 重复注单")
                        }
                        
                        numSelectArea.confirmBetArea.datas = newDatas
                    }
                }
                
            }else {
                showToast(view: self.view, txt: "\(info.1)")
                return
            }
            
        }else {
            let info = CRBetLogic.judgeBeforeBet(zhuShu: self.selectZhuShu, betMoney: self.selectMoney, accountMoney: self.accountBanlance)
            if info.0 {
                //跳转到确认注单页面
                numSelectArea.topView.updateShowRuleView(isShowRuleView: 2)
                numSelectArea.showConfirmArea()
                numSelectArea.numSelectView.formBetDataAndEnterOrderPageWith(handler: {[weak self] (official_orders, subPlayName, subPlayCode, selectedBeishu, cpTypeCode, cpBianHao, current_rate, selectMode, cpName, cpVersion) in
                    if let weakSelf = self {
                        weakSelf.official_orders = official_orders
                        weakSelf.subPlayName = subPlayName
                        weakSelf.subPlayCode = subPlayCode
                        weakSelf.selectedBeishu = selectedBeishu
                        weakSelf.cpTypeCode = cpTypeCode
                        weakSelf.cpBianHao = cpBianHao
                        weakSelf.current_rate = current_rate
                        weakSelf.selectMode = selectMode
                        weakSelf.cpName = cpName
                        weakSelf.cpVersion = cpVersion
                        
                        //为注单确认页面赋值注单数据
                        let order = fromBetOrder(official_orders: weakSelf.official_orders, subPlayName: weakSelf.subPlayName, subPlayCode: weakSelf.subPlayCode, selectedBeishu: weakSelf.selectedBeishu, cpTypeCode: weakSelf.cpTypeCode, cpBianHao: weakSelf.cpBianHao, current_rate: weakSelf.current_rate, selectMode: weakSelf.selectMode)
                        
                        
                        
                        numSelectArea.confirmBetArea.datas = order
                    }
                })
                
            }else {
                showToast(view: self.view, txt: "\(info.1)")
                return
            }

        }
    }
    
    func showStopSaleWindow(){
        let stopSaleView:StopSaleWindow = Bundle.main.loadNibNamed("stop_sale_window", owner: nil, options: nil)?.first as! StopSaleWindow
        stopSaleView.controller = self
        stopSaleView.show()
    }
    
    //MARK: -清楚号码的选择
    func clearBetInfo(viewIndex:Int = 1) {
        let numSelectView = self.contentView.betArea.numSelectArea.numSelectView
        if self.isPeilvVersion() {
            numSelectArea.confirmBetArea.peilvBottom.creditbottomTopField.text = ""
            numSelectView.clearMutiPlaysBetAndReload()
            
            numSelectArea.betRuleView.collection.reloadData()
            numSelectArea.numSelectView.updateBottomUI()
        }else {
            self.selectMode = YUAN_MODE
            self.selectedBeishu = 1 //选择的倍数
            numSelectArea.confirmBetArea.officalBottom.beishu_money_input.text = "1"
            numSelectView.selectedBeishu = 1
            numSelectArea.confirmBetArea.officalBottom.modeBtn.setTitle("元", for: .normal)
            numSelectView.clearOfficialBetAndReload()
        }
        
        if viewIndex == 1 {
            self.numSelectArea.topView.updateShowRuleView(isShowRuleView: 1)
            self.numSelectArea.showNumSelectView()
            netAccountWeb()
        }
    }
    
    //MARK: - 倒计时
    /**
     * 创建停止下注倒计时
     * @param duration
     */
    func createEndBetTimer() -> Void {
        if let timer = self.endlineTouzhuTimer {
            timer.invalidate()
        }
        
        if let timer = self.disableBetCountDownTimer {
            timer.invalidate()
        }
        
        YiboPreference.setAbleBet(value: "on")
        
        self.endlineTouzhuTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(endBetTickDown), userInfo: nil, repeats: true)
        
        if let timer = self.endlineTouzhuTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    /** 创建停止下注倒计时_跟单页面 */
    func createEndBetShareTimer() -> Void {
        if let timer = self.endlineTouzhuShareTimer {
            timer.invalidate()
        }
        
        if let timer = self.disableBetCountDownShareTimer {
            timer.invalidate()
        }
        
        YiboPreference.setAbleBetShare(value: "on")
        
        self.endlineTouzhuShareTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(endBetShareTickDown), userInfo: nil, repeats: true)
        
        if let timer = self.endlineTouzhuShareTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    //MARK: 封盘时间结束后再次获取下一期的倒计时时间
    private func getCountDownByCpCodeAction(shouldReStartDisableTimer:Bool = true) {
        self.netGetCountDownByCpcode()
    }
    
    //MARK: 封盘时间结束后再次获取下一期的倒计时时间_跟单
    func getCountDownShareByCpCodeAction(shouldReStartDisableTimer:Bool = true) {
        self.netGetCountDownShareByCpcode()
    }
    
    @objc func endBetTickDown() {
        //将剩余时间减少1秒
        self.endBetTime -= 1
        
        if self.endBetTime > 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.endBetTime)))
            
            if YiboPreference.getAbleBet() == "on" {
                self.clearDisableBetStatus()
                self.betDeadlineDes = "截止剩余时间"
                self.countDownValue = String.init(format:"%@", dealDuration)
            }else {
                self.getCountDownByCpCodeAction()
            }
            
        }else if self.endBetTime <= 0{
            self.disableBetTime = self.ago
            self.createDisableBetCountDownTimer()
            self.getCountDownByCpCodeAction(shouldReStartDisableTimer: false)
            
            //这里应该加上 self.titckTime
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                self.netLastOpenResult()
                self.createLastResultTimer()
            }
        }
    }
    
    @objc func endBetShareTickDown() {
        //将剩余时间减少1秒
        self.endBetShareTime -= 1
        
        if self.endBetShareTime > 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.endBetShareTime)))
            
            if YiboPreference.getAbleBetShare() == "on" {
                self.clearDisableBetShareStatus()
                self.betDeadSharelineDes = "截止剩余时间"
                self.countDownShareValue = String.init(format:"%@", dealDuration)
            }else {
                self.getCountDownShareByCpCodeAction()
            }
            
        }else if self.endBetShareTime <= 0 {
            self.disableBetShareTime = self.agoShare
            self.createDisableBetCountDownShareTimer()
            self.getCountDownShareByCpCodeAction(shouldReStartDisableTimer: false)
        }
    }
    
    //MARK: 清除不可投注状态
    private func clearDisableBetStatus() {
        if let timer = self.disableBetCountDownTimer {
            disableBetTime = 0
            timer.invalidate()
        }
        
        YiboPreference.setAbleBet(value: "on")
        setupAbleBetUI(able: !isBetNotAble())
        
        let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(0))
        self.countDownValue = String.init(format:"%@", dealDuration)
    }
    
    //MARK: 清除不可投注状态_跟单
    private func clearDisableBetShareStatus() {
        if let timer = self.disableBetCountDownShareTimer {
            disableBetShareTime = 0
            timer.invalidate()
        }
        
        YiboPreference.setAbleBetShare(value: "on")
        setupAbleBeSharetUI(able: !isBetNotAbleShare())
        let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(0))
        
        self.countDownShareValue = String.init(format:"%@", dealDuration)
    }
    
    /// 设置是否可投注状态时，UI变化
    ///
    /// - Parameter able: true可投，false封盘
    private func  setupAbleBetUI(able:Bool) {
        if let confirmButton = self.contentView.betArea.numSelectArea.bottomView.confirmButton {
            confirmButton.backgroundColor = able ? UIColor.red : UIColor.lightGray
            confirmButton.isEnabled = able ? true : false
        }
    }
    
    /// 设置是否可投注状态时，UI变化_跟单
    ///
    /// - Parameter able: true可投，false封盘
    private func  setupAbleBeSharetUI(able:Bool) {
        if let confirmButton = self.followBetPop.followBetButton {
            confirmButton.backgroundColor = able ? UIColor.orange : UIColor.lightGray
            confirmButton.isEnabled = able ? true : false
        }
    }
    
    ///更新下注倒计时
    func updateCurrenQihaoCountDown(countDown:CountDown,shouldReStartDisableTimer:Bool,successHandler:(_ dealDurationP:String) -> ()) -> Void {
        //创建开奖周期倒计时器
        let serverTime = countDown.serverTime;
        let activeTime = countDown.activeTime;
        let value = abs(activeTime) - abs(serverTime)
        let preStartTime = countDown.preStartTime
        self.endBetDuration = Int(abs(value))/1000
        
        self.endBetTime = self.endBetDuration
        self.currentQihao = trimQihao(currentQihao: countDown.qiHao)
        let dealDuration = getFormatTime(secounds: TimeInterval(Int64(self.endBetDuration)))
        
        if  preStartTime - serverTime < 0{
            self.createEndBetTimer()
        }else if shouldReStartDisableTimer {
            //封盘
            let agoTime = preStartTime - serverTime
            if agoTime < ago * 1000 {
                self.disableBetTime = agoTime / 1000
                self.createDisableBetCountDownTimer()
            }
        }
        
        successHandler(dealDuration)
    }
    
    ///更新下注倒计时_跟单
    func updateCurrenQihaoCountShareDown(countDown:CountDown,shouldReStartDisableTimer:Bool,successHandler:(_ dealDurationP:String) -> ()) -> Void {
        //创建开奖周期倒计时器
        let serverTime = countDown.serverTime;
        let activeTime = countDown.activeTime;
        let value = abs(activeTime) - abs(serverTime)
        let preStartTime = countDown.preStartTime
        self.endBetShareDuration = Int(abs(value))/1000
        
        self.endBetShareTime = self.endBetShareDuration
        self.currentShareQihao = countDown.qiHao
        let dealDuration = getFormatTime(secounds: TimeInterval(Int64(self.endBetShareDuration)))
        
        if  preStartTime - serverTime < 0{
            self.createEndBetShareTimer()
        }else if shouldReStartDisableTimer {
            //封盘
            let agoTime = preStartTime - serverTime
            if agoTime < agoShare * 1000 {
                self.disableBetShareTime = agoTime / 1000
                self.createDisableBetCountDownShareTimer()
            }
        }
        
        successHandler(dealDuration)
    }
    
    /**
     * 创建封盘到开奖的倒计时器
     * @param duration
     */
    func createDisableBetCountDownTimer() -> Void {
        if let timer = self.endlineTouzhuTimer {
            timer.invalidate()
        }
        
        if let timer = self.disableBetCountDownTimer {
            timer.invalidate()
        }
        
        if self.disableBetTime > 0 {
            YiboPreference.setAbleBet(value: "off")
        }
        
        self.disableBetCountDownTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(disableBetTickDown), userInfo: nil, repeats: true)
        
        if let timer = self.disableBetCountDownTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
        
        self.betDeadlineDes = "开盘剩余时间"
        
        let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(self.disableBetTime))
        self.countDownValue = String.init(format:"%@", dealDuration)
        
        if isBetNotAble() {
            if let confirmButton = self.contentView.betArea.numSelectArea.bottomView.confirmButton {
                confirmButton.backgroundColor = UIColor.lightGray
                confirmButton.isEnabled = false
            }
        }
    }
    
    /**
     * 创建封盘到开奖的倒计时器_跟单
     * @param duration
     */
    func createDisableBetCountDownShareTimer() -> Void {
        if let timer = self.endlineTouzhuShareTimer {
            timer.invalidate()
        }
        
        if let timer = self.disableBetCountDownShareTimer {
            timer.invalidate()
        }
        
        if self.disableBetShareTime > 0 {
            YiboPreference.setAbleBetShare(value: "off")
        }
        
        self.disableBetCountDownShareTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(disableBetShareTickDown), userInfo: nil, repeats: true)
        
        if let timer = self.disableBetCountDownShareTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
        
        self.betDeadSharelineDes = "开盘剩余时间"
        
        let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(self.disableBetShareTime))
        self.countDownShareValue = String.init(format:"%@", dealDuration)
        
        if isBetNotAbleShare() {
            self.setupAbleBeSharetUI(able: false)
        }
    }

    @objc func disableBetTickDown() {
        //将剩余时间减少1秒
        self.disableBetTime -= 1
        
        if self.disableBetTime > 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(self.disableBetTime)))
            self.countDownValue = String.init(format:"%@", dealDuration)
            
        }else if self.disableBetTime <= 0{
            clearDisableBetStatus()
            
            //封盘时间结束后再次获取下一期的倒计时时间
            self.getCountDownByCpCodeAction()
        }
    }
    
    @objc func disableBetShareTickDown() {
        //将剩余时间减少1秒
        disableBetShareTime -= 1
        
        if disableBetShareTime >= 0{
            let dealDuration = getFormatTimeWithSpace(secounds: TimeInterval(Int64(disableBetShareTime)))
            countDownShareValue = String.init(format:"%@", dealDuration)
            
        }else if self.disableBetShareTime <= 0{
//            clearDisableBetShareStatus()
//            //封盘时间结束后再次获取下一期的倒计时时间
//            getCountDownShareByCpCodeAction()
        }
    }

    
    // 录音倒计时
    @objc func recordingCountDown() {
        recordingDuration -= 1
        if recordingDuration < 0 {
            self.chatBarFinishedRecoding(chatBar: self.contentView.inputAreaView)
        }
    }
    
    ///踢出房间
    func kickOutRoom() {
        popVC()
        HUD.flash(.label("您被请出当前房间，请联系客服！"), delay: 2)
    }
    
    func dismissVC() {
        self.dismiss(animated: true) {
            if let inNav = self.navigationController,let top = inNav.topViewController {
                if top.isKind(of: CRChatController.self) || top.isKind(of: CRUserProfileInfoCtrl.self) {
                    let count = inNav.viewControllers.count
                    if count > 1 {
                        self.popVC()
                    }
                }
            }
        }
    }
    
    func popVC() {
        if let nav = self.navigationController {
            let count = nav.viewControllers.count
            if count > 1{
                nav.popToRootViewController(animated: true)
                dismissVC()
            }else {
                dismissVC()
            }
        }
    }
    
    //禁言申述
    func appealBandAlert() {
        let alert = UIAlertController.init(title: "申述", message: nil, preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "请填写申述内容"
        }
        
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        let commitAction = UIAlertAction.init(title: "申述", style: .default) { (action) in
            let text = alert.textFields![0].text ?? ""
            if text.isEmpty {
                showToast(view: self.view, txt: "输入内容不能为空")
            }else {
                self.sendMsgAppealBand(remark: text)
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(commitAction)
        self.present(alert, animated: true) {}
    }
}

//MARK: - tabbar点击
extension CRChatController {
    ///点击了 index上的tabbarItem
    func tapTabBarWith(index:Int) {
        switch index {
        case 0:
            self.tapTabBarFirst()
            break
        case 1:
            self.addLotteryHistoryView()
            break
        case 2:
            
            //  测试入口 跳转彩票计划
            let lotterySchemeVc = CRLotterySchemeViewController()
            lotterySchemeVc.networkVc = self
            navigationController?.pushViewController(lotterySchemeVc, animated: true)
//            goDepositOrWithdraw(index: index)
//            chooseGoChargeController(meminfo: meminfo, controller: self)
            break
        case 3:
            
            
//            let loginVC = UIStoryboard(name: "withdraw_page", bundle: nil).instantiateViewController(withIdentifier: "withDraw")
//            let recordPage = loginVC as! WithdrawController
//            recordPage.meminfo = meminfo
//            let nav = UINavigationController.init(rootViewController: recordPage)
//            self.present(nav, animated: true, completion: nil)
            break
        default:
            return
        }
    }
    
    private func goDepositOrWithdraw(index:Int){
        if judgeIsMobileGuest() {
            showToast(view: self.view, txt: tip_shiwan)
            return
        }
        let p = [String:Any]()
        CRNetManager.shareInstance().netAccountWeb(paramters: p) {[weak self] (success, msg, model) in
            guard let weakSelf = self else {
                return
            }
            if success {
                if let model = model {
                    weakSelf.updateAccount(memInfo: model)
                    if index == 2{
                        weakSelf.goDepositPage()
                    }else if index == 3{
                        weakSelf.goWithdrawPage(controller: weakSelf, meminfo: model)
                    }
                }
            }else {
                showToast(view: weakSelf.view, txt: msg)
            }
        }
    }
    
    private func goDepositPage(){
        if self.navigationController != nil{
            openChargeMoney(controller: self,meminfo:meminfo)
        }else{
            chooseGoChargeController(meminfo: meminfo, controller: self)
        }
    }
    
    private func goWithdrawPage(controller:UIViewController,meminfo:Meminfo){
        if controller.navigationController != nil{
            openPickMoney(controller: controller,meminfo:meminfo)
        }else{
            let loginVC = UIStoryboard(name: "withdraw_page", bundle: nil).instantiateViewController(withIdentifier: "withDraw")
            let recordPage = loginVC as! WithdrawController
            recordPage.meminfo = meminfo
            controller.navigationController?.pushViewController(recordPage, animated: true)
            let nav = UINavigationController.init(rootViewController: recordPage)
            controller.present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK:显示投注页面
    private func tapTabBarFirst() {
        self.netAccountWeb()
        self.netAllLotteries()
    }
    
    //MARK:显示聊天室--历史投注页面
    private func addLotteryHistoryView() {
        contentView.showGrayBackgroundView(show: true)
        
//         self.lotteryHistoryController = CRLotteryHistoryController.init(stationId: self.msgModel.stationId, userId: self.msgModel.userId,roomId:self.msgModel.roomId)
        
        lotteryHistoryController = CRLotteryHistoryController()
        
        let viewheight:CGFloat = 350
        
        let frame = CGRect.init(x: 0, y: (self.view.height), width: ScreenWidth, height: 0)
        self.lotteryHistoryController!.view.frame = frame
        
        self.lotteryHistoryController!.willMove(toParent: self)
        self.view.addSubview(self.lotteryHistoryController!.view)
        self.addChild(self.lotteryHistoryController!)
        self.lotteryHistoryController!.didMove(toParent: self)
        
        self.lotteryHistoryController!.view.snp.remakeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(viewheight)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        //MARK: 历史注单分享
        if let vc = self.lotteryHistoryController {
            vc.sharebetHandler = {[weak self] (models) in
                guard let weakSelf = self else {return}
                if !weakSelf.prohibit && !weakSelf.roomProhibit{
                    weakSelf.sendMsgFormatAllBetShareMsgModelInHistoryYJ(models: models) //格式化注单数据
                    weakSelf.sendMsgShareBetMessage() //发送注单消息
                }else{
                    showToast(view: weakSelf.view, txt: "禁言中无法分享注单")
                }
                
                weakSelf.contentView.showGrayBackgroundView(show: false)
            }
        }
        
        sendMsgBetHistory() //请求 投注历史数据
    }
    
    //MARK: 移除聊天室--历史投注页面
    func removeaddLotteryHistoryView() {
        guard let vc = self.lotteryHistoryController else {
            return
        }
        
        vc.view.snp.remakeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(0)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            vc.view.removeFromSuperview()
            vc.removeFromParent()
            self.lotteryHistoryController = nil
        }
    }
    
}


//MARK: - 加号 更多功能
extension CRChatController {
    ///点击了index上的item
    func tapMoreFucItemWith(name: String) {
        switch name {
        case "图片":
            //选择图
            tapImageChoice()
            break
        case "红包":
            //发红包
             sendRedPackage()
            
        default:
            showToast(view: self.view, txt: "选择不存在")
            break
        }
    }
    
    ///选择图片
    private func tapImageChoice() {
        
        let vc = BSImagePickerViewController()
        //        vc.maxNumberOfSelections = 5
        
        var selectectCount:Int = 0
        
        bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in

//                                            if let view = getSemicycleButton() {
//                                                view.isHidden = true
//                                            }
                                            selectectCount =  selectectCount + 1

                                            if selectectCount > 5{
                                                DispatchQueue.main.async {
                                                    vc.maxNumberOfSelections = 6
                                                    showToast(view: vc.view, txt: "发送图片不能超过5张")
                                                    return
                                                }
                                                return
                                            }
                                            // User selected an asset.
                                            // Do something with it, start upload perhaps?
        }, deselect: { (asset: PHAsset) -> Void in
            // User deselected an assets.
            // Do something, cancel upload?
            selectectCount =  selectectCount - 1
        }, cancel: { (assets: [PHAsset]) -> Void in
            selectectCount = 0
            // User cancelled. And this where the assets currently selected.
        }, finish: { (assets: [PHAsset]) -> Void in
            // User finished with these assets
            // 这时 assetsFetchResults 中包含的，应该就是各个资源（PHAsset）
            if assets.count > 5{
                DispatchQueue.main.async {
                    showToast(view: self.view, txt: "发送图片不能超过5张")
                    return
                }
               return
            }
            for  phAsset in assets {
                // 获取一个资源（PHAsset）
                if (phAsset.mediaType == .video) {//视频
                    let options = PHVideoRequestOptions()
                    options.version = .current;
                    options.deliveryMode = .automatic;
                    let manager = PHImageManager.default()
                    manager.requestAVAsset(forVideo: phAsset, options: options, resultHandler: { (asset, audioMix, info) in
//                        let urlAssets = assets as? AVURLAsset
//                        let videoUrl = urlAssets?.url
//                        let mp4 = sel
                    })


                }else{//图片或者gif
                    let option = PHImageRequestOptions()
                    option.resizeMode = PHImageRequestOptionsResizeMode.exact
                    option.isNetworkAccessAllowed = true
                    option.isSynchronous = false;//同步执行
                    PHImageManager.default().requestImageData(for: phAsset, options: option, resultHandler: { (data:Data?, uti, orientation, dic) in
                        if uti == "com.compuserve.gif"{
                            if  data != nil{
//                                var path:String = ""
//                                let urlPath = dic!["PHImageFileURLKey"] as? URL
//                                var fileName:String = ""
//                                if (urlPath != nil){
//                                    fileName = (urlPath?.absoluteString.last as? String) ?? ""
//                                }
                            }
                        }else{
                            //图片
                            if let iamgeData = data{
                                let image = UIImage.init(data: iamgeData)

                                self.sendImageMessage(image:image!)
                            }
                        }
                    })
                }
            }
        }, completion: {
            
            if let view = getSemicycleButton() {
                view.isHidden = true
            }
        })
        
    }
    
    ///选择视频
    private func tapVideoChoice() {
        var config = YPImagePickerConfiguration()
        config.library.maxNumberOfItems = 3
    
        config.screens = [.video]
        config.library.mediaType = .video
        
        let picker = YPImagePicker(configuration: config)
        
        picker.didFinishPicking { items, cancelled in
            for item in items {
                switch item {
                case .video(let video):
                    print(video)
                case .photo(let p):
                    print(p)
                }
            }
            
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
    
    private func tapFileChoice() {
        showToast(view: self.view, txt: "选择文件")
    }
    //MARK:发送红包 输入红包内容
    func sendRedPackage(){
        
        let redPackge = CRSendRedPacketView.init(isPrivateChat:isPrivateChat,size:UIScreen.main.bounds.size)
        redPackge.redPacketClouse = {[weak self] redPacketItem in
            if let weakSelf = self {
                redPacketItem.sendState = .progress
                //                weakSelf.sendRedPackageMessageRequest(message: redPacketItem)
                redPacketItem.msgUUID = CRCommon.generateMsgID()
                weakSelf.sendMsgRedPackageMessageRequest(message: redPacketItem)
            }
        }
        UIApplication.shared.keyWindow?.addSubview(redPackge)
        
        //
    }
}

//MARK: -键盘相关
extension CRChatController{
    
    @objc func scrollTableViewToBottom(animated:Bool,duartime:TimeInterval = 0){
        if (contentView.messageArea.msgItems.count > 0) {
            let indexLocation = IndexPath(row: (contentView.messageArea.msgItems.count - 1), section: 0)
            UIView.animate(withDuration: TimeInterval(duartime)) {
                 self.contentView.messageArea.msgTable.scrollToRow(at: indexLocation, at:.bottom, animated: animated)
            }
        }
    }
}

//MARK:--CRIputViewDelegate
extension CRChatController:CRIputViewDelegate{
  
    func chatBar(chatBar: CRInputAreaView, fromStatus: CRChatBarStatus, toStatus: CRChatBarStatus) {
        if curStatus == toStatus {
            return
        }
        lastStatus = fromStatus
        curStatus = toStatus
        if toStatus == .Init{
            //输入状态
            if fromStatus == .More {
                //更多功能键盘弹起
                //更多状态键盘切 输入框状态
                moreKeyboard.dismissWithAnimation(animation: true)
            }else if (fromStatus == .Emoji){
                //emoji键盘弹起
                emojiKeyboard.dismissWithAnimation(animation: true)
            }else if (fromStatus == .Voice){
                //键盘音频状态时 切到输入框时
            }
        }else if (toStatus == .Voice){
            if fromStatus == .More{
                
                moreKeyboard.dismissWithAnimation(animation: true)
                
            }else if (fromStatus == .Emoji){
                
                emojiKeyboard.dismissWithAnimation(animation: true)
            }
        }else if (toStatus == .Emoji){
            //emoji键盘
            emojiKeyboard.showInView(view: view, animation: true)
            
        }else if (toStatus == .More){
            emojiKeyboard.dismissWithAnimation(animation: true)
            let moreFuncHeight = contentView.roomsBottomFucView.functionViewHeight + KTabBarHeight
            contentView.inputAreaView.snp.remakeConstraints() { (make) in
                make.bottom.equalTo(contentView.snp.bottom).offset(-moreFuncHeight)
                make.left.right.equalTo(contentView)
            }
            contentView.showOrHideMoreFuncView(show: true)
            
            
        }else if (toStatus == .Keyboard){
            //收起emoji键盘
            emojiKeyboard.dismissWithAnimation(animation: true)
            moreKeyboard.dismissWithAnimation(animation: true)
            //弹起文本键盘
            
            contentView.inputAreaView.snp.remakeConstraints() { (make) in
                make.bottom.equalTo(contentView.snp.bottom).offset(-215 - 44)
                make.left.right.equalTo(contentView)
            }
        }
    }
    //输入框的高度改变
    func chatBarDidChangeTextView(chatBar: CRInputAreaView, height: CGFloat) {
        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: false)
    }
    
    //MARK: 录音
    func chatBarStartRecording(chatBar: CRInputAreaView) {
        if CRDefaults.getChatroomSendVoice() == false{
            showToast(view: view, txt: "你无法在该房间发送语音，请联系客服")
            return
            
        }
        recordingDuration = 15
        recordingTimer = Timer(timeInterval: 1, target: self, selector: #selector(recordingCountDown), userInfo: nil, repeats: true)
        RunLoop.main.add(recordingTimer!, forMode: RunLoop.Mode.common)
        
        // 先停止播放
        if TLAudioPlayer.shared().player != nil {
            if TLAudioPlayer.shared().player.isPlaying{
                TLAudioPlayer.shared().stopPlayingAudio()
                self.recordVoiceImage?.stopPlayingAnimation()
            }
        }
        //
        TLAudioRecorder.shared().delegate = self
        recorderIndicateView.status = .recording
        contentView.messageArea.addSubview(recorderIndicateView)
        
        recorderIndicateView.snp.makeConstraints { (make) in
            make.center.equalTo(contentView.messageArea.snp.center)
            make.size.equalTo(CGSize(width: 150, height: 150))
        }
        var time_count = 0
        let voiceMessage = CRVoiceMessageItem()
        voiceMessage.ownerType = .TypeSelf
        voiceMessage.msgStatus = .recording
        voiceMessage.sentTime = "2019"
        
        TLAudioRecorder.shared().startRecording(volumeChangedBlock: {[weak self] (volume) in
            guard let weakSelf = self else {return}
            
            time_count += 1
            if time_count == 2 {
//                weakSelf.addToShowMessage(message: voiceMessage)
            }
            
            weakSelf.recorderIndicateView.volume = Float(volume)
            
            }, complete: {[weak self] (filePath, time) in
                guard let weakSelf = self else {return}
                weakSelf.recorderIndicateView.removeFromSuperview()
                
                if time < 1.0{
                    
                    return
                }
                
                weakSelf.voiceDuartion = Float(time)
                
                if FileManager.default.fileExists(atPath: filePath ?? ""){
                    let fileName = String(format:"%f.wav",Date().timeIntervalSince1970 * 1000)
                    let path = FileManager.pathUserChatVoice(voiceName: fileName)
                    //移动该文件夹  并重新命名
                    //                    try? FileManager.default.moveItem(atPath: filePath ?? "", toPath: path)
                    try? FileManager.default.copyItem(atPath: filePath!, toPath: path)
                    //语音UUID
                    let msgUUID = CRCommon.generateMsgID()
                    print(msgUUID)
                    weakSelf.voiceMsgUUID = msgUUID
                    voiceMessage.recFileName = fileName
                    voiceMessage.path = path
                    voiceMessage.durationTime = "\(time)"
                    voiceMessage.msgStatus = .normal
                    //发送进度
                    voiceMessage.sendState = .progress
                    voiceMessage.msgUUID = msgUUID
                    voiceMessage.resetMessageFrame()
                    weakSelf.voiceFileName = fileName
                    weakSelf.sendMessage(message: voiceMessage)
                }
                
        }) {[weak self] in
            if let weakSelf = self{
                weakSelf.recorderIndicateView.removeFromSuperview()
            }
        }
        
    }

    func chatBarWillCancelRecording(chatBar: CRInputAreaView, cancel: Bool) {
        self.recorderIndicateView.status = cancel ? .willCancel : .recording
    }

    func chatBarDidCancelRecording(chatBar: CRInputAreaView) {
        recordingTimer?.invalidate()
        recordingTimer = nil
        TLAudioRecorder.shared().cancelRecording()
    }

    func chatBarFinishedRecoding(chatBar: CRInputAreaView) {
        recordingTimer?.invalidate()
        recordingTimer = nil
        TLAudioRecorder.shared().stopRecording()
    }
}


extension CRChatController:TLAudioRecorderDelegate{
    
    func recordFinish(withUrl url: String!, isSuccess: Bool) {
        if (voiceDuartion < 1.0) {
            //时间太短了
            return;
        }
        //url为得到的wav录音文件地址,可直接进行播放,也可进行转码为amr上传服务器
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last
//        let filePath = FileManager.pathUserChatVoice(voiceName: self.voiceFileName)

        //上传amr文件
        let amrFilePath =  FileManager.pathUploadVoicePath(amrPath: self.voiceFileName)
        TLAudioRecorder.convertCAFtoAMR(url, savePath: amrFilePath)
        print("转码amr格式成功----文件地址为" + "\(amrFilePath)")
        let voiceMessageItem = CRVoiceMessageItem()
        voiceMessageItem.durationTime = "\(voiceDuartion)"
        voiceMessageItem.recFileName = self.voiceFileName
        voiceMessageItem.msgUUID = self.voiceMsgUUID + ""
        uploadVoiceFileToServer(voiceItem: voiceMessageItem, filePath: amrFilePath)
        voiceDuartion = 0
    }
}

//MARK:收键盘
extension CRChatController{
    func dismissKeyboard(){
        if curStatus == CRChatBarStatus.More {
//            curStatus = .Init
            moreKeyboard.dismissWithAnimation(animation: true)
        }else if (curStatus == CRChatBarStatus.Emoji){
            //当前键盘状态是emoji 关掉emoji键盘
            emojiKeyboard.dismissWithAnimation(animation: true)
        }
        contentView.inputAreaView.inputField.resignFirstResponder()
    }
}

//MARK:消息点击事件
extension CRChatController:CRChatMessageViewDelegate {
  
    //点击tableView收键盘
    func chatMessageViewTableViewClick() {
        UIView.animate(withDuration: 0.25) {
            self.contentView.showOrHideMoreFuncView(show: false)
            self.contentView.inputAreaView.moreFunctionButton.isSelected = false
            self.contentView.inputAreaView.snp.remakeConstraints { (make) in
                make.bottom.equalTo(self.contentView.snp.bottom).offset(-KTabBarHeight)
                make.left.right.equalTo(self.contentView)
            }
        }
       
        dismissKeyboard()
    }
    func chatMessageDisplayViewDidClick(message: CRMessage) {
        //点击的消息类型是图片
        if message.messageType == .Image {
            if let imgMsgItem = message as? CRImageMessage {
                //默认的是第一张
                var indexPath = IndexPath.init(row: 0, section: 0)
                //点击选中的图片
                let selectectImageUrl = imgMsgItem.content["url"] as? String ?? ""
                for (index,value)in imageUrls.enumerated(){
                    if value == selectectImageUrl{
                        indexPath = IndexPath.init(row: index, section: 0)
                        break
                    }
                }
             
             
                if imageUrls.count > 0{
                    let pictureBrowserCtrl = CRPictureBrowser.init(urls: imageUrls, indexPath: indexPath)
                    pictureBrowserCtrl.images = imageUrls
                    present(pictureBrowserCtrl, animated: false, completion: nil)
                }
               
            }
        }else if message.messageType == .RedPacket{
            
            if CRDefaults.getChatroomReceiveRedPacket() == false{
                showToast(view: view, txt: "您没有领取该红包的权限")
            }else{
                //红包消息
                if let redPacketItem = message as? CRRedPacketMessage{
                    sendMsgReceiveRedPacketMessageRequest(message:redPacketItem)
                }
            }
        }
    }
    //MARK:--点击修改用户信息 点击自己的才可以修改个人信息
    func chatUserIconDidClick(message: CRMessage,type:ChatTapAvtarType){
        //查看个人信息
        if message.userId == CRDefaults.getUserID() && type == .personInfo {
            //当前用户自己的图像
            let userProfileCtrl = CRUserProfileInfoCtrl()
            userProfileCtrl.commitUserInfoClourse = {[weak self](userInfoItem:CRUserInfoCenterSource) in
                guard let weakSelf = self else {return}
                
                //拉取历史记录
                if userInfoItem.avatar.length > 0{
                    weakSelf.userIconUrl = userInfoItem.avatar
                }
                if userInfoItem.nickName.length > 0{
                    weakSelf.userNickname = userInfoItem.nickName
                }
                weakSelf.getMsgHistoryRecordData()
            }
            
            userProfileCtrl.roomId = self.msgModel.roomId
            userProfileCtrl.stationId = self.msgModel.stationId
            userProfileCtrl.avatar = self.userIconUrl
            userProfileCtrl.nickName = self.userNickname
            
            userProfileCtrl.updateUserInfoHandler = {[weak self] () in //获取用户avatar和nickName
                guard let weakSelf = self else {return}
                weakSelf.sendMsgUserIconAndnickNameData()
            }
            
            userProfileCtrl.updateAvatarListHandler = {[weak self] in //获取默认图片列表
                guard let weakSelf = self else {return}
                weakSelf.sendMsgGetUserAvatarList(userId: CRDefaults.getUserID())
            }
            
            userProfileCtrl.updateEditInfoHandler = {[weak self] in //编辑用户信息
                guard let weakSelf = self else {return}
                weakSelf.sendMsgSubmitInfo()
            }
            
            userProfileCtrl.updateBetInfoHandler = {[weak self] in //今日输赢信息
                guard let weakSelf = self else {return}
            }
            
            userProfileCtrl.shareGainsAndLossesForTodayHandler = {[weak self] gainsLossesItem  in
                guard let weakSelf = self else {return}
                //今日分享盈亏
                
//                if !weakSelf.prohibit {
//                    weakSelf.shareGainsAndLossesForToday(gainsLossesItem: gainsLossesItem)
//                }else{
//                    showToast(view: weakSelf.view, txt: "禁言中无法发送消息!")
//                }
                
            }
            navigationController?.pushViewController(userProfileCtrl, animated: true)
        } else {
            if type == .informSomeOne { // @
                let otherUserName = message.currentUserName.isEmpty ? (message.nickName.isEmpty ? (message.nativeAccount.isEmpty ? message.account : message.nativeAccount) : message.nickName) : message.currentUserName
                var sendText = contentView.inputAreaView.inputField.text ?? ""
                let sendMessageString = "@" + "\(otherUserName) "
                let attubeString = NSMutableAttributedString.init(string: sendMessageString)
                attubeString.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.red], range: NSRange.init(location: 0, length: sendMessageString.length))
                sendText = sendText + sendMessageString
                //            let attributedString = NSAttributedString.init(string: sendMessageString, attributes: nil)
                contentView.inputAreaView.inputField.text = sendText
            }else if type == .clearScreen {
                //清当前屏
//                if let indexPaths = contentView.messageArea.msgTable.indexPathsForVisibleRows {
//
//                    var msgItems = contentView.messageArea.msgItems
//                    var shouldDeleteMsgIds = [String]()
//
//                    for indexPath in indexPaths {
//                        let row = indexPath.row
//                        let msg = msgItems[row]
//                        shouldDeleteMsgIds.append(msg.msgId)
//                    }
//
//                    deleteMsgsWith(msgIds: shouldDeleteMsgIds)
//                }
                
                //清所有
                deleteMsgsWith(msgIds: [String](),deleteAll: true)
            }else if type == .prohibitTalk {//禁言
                sendMsgProhibit(messageItem: message, close: "1")
            }else if type == .cancelProhibitTalk { //取消禁言
                sendMsgProhibit(messageItem: message, close: "0")
            }else if type == .retractMsg { //撤回
                sendMsgRetract(messageItem: message)
            }else if type == .prohibitTalkAll { //全体禁言
                sendMsgProhibitAll(messageItem: message, close: "1")
            }else if type == .cancelProhibitTalkAll { //取消全体禁言
                sendMsgProhibitAll(messageItem: message, close: "0")
            }else if type == .privateChat { //私聊
                let user = CROnLineUserModel()
                user.id = message.userId
                user.userType = message.userType
                user.nickName = message.nickName.isEmpty ? message.account : message.nickName
                sendMsgRequestPrivateChat(type: "1", model: user)
            }
        }
    }
    
    //MARK: 撤回功能，传入msgUUID，删除本地消息
    func deleteMsgsWith(msgIds:[String],deleteAll:Bool = false) {
        var newMsgItems = [CRMessage]()
        
        if !deleteAll {
            let msgItems = contentView.messageArea.msgItems
            for msg in msgItems {
                if !msgIds.contains(msg.msgId) {
                    newMsgItems.append(msg)
                }
            }
        }
        
        contentView.messageArea.reAssignMsgItems(msgItems: newMsgItems)
    }
    
    func handleProhibitAll(model:CRProhibitModel) {
        if model.roomId.contains(chat_roomId)  {
            isRoomBandSpeak = model.isBanSpeak
        }
    }
    
    func bandSpeak(isRoomBand:Bool,isPersonBand:Bool) {
        prohibit = isPersonBand
        roomProhibit = isRoomBand
        contentView.inputAreaView.updateWith(isRoomBand: isRoomBand, isPersonBand: isPersonBand)
        
        if isRoomBand || isPersonBand {
            chatMessageViewTableViewClick() //键盘弹起来中的状态 全部收起
        }
    }
    
    //MARK: 禁言消息的处理
    func handleProhibit(message:CRPorhibitMsgItem) {
        guard let model = message.prohibitModel else {return}
        let userId = model.speakingCloseId //是被禁言的userid
        let stopTalkType = model.speakingClose
        
//        if model.roomId == chat_roomId { //同一个房间
        //        }
        
        if deleteMsgsWhenProhibit() {
            var newMsgItems = [CRMessage]()
            
            for msg in contentView.messageArea.msgItems {
                if msg.userId != userId {
                    newMsgItems.append(msg)
                }
            }
            
            contentView.messageArea.msgItems = newMsgItems
        }else {
            for msg in contentView.messageArea.msgItems {
                if msg.userId == userId {
                    msg.stopTalkType = stopTalkType
                }
            }
        }
            
        contentView.messageArea.reAssignMsgItems(msgItems: contentView.messageArea.msgItems)

        if userId == chat_userId { //被禁言,被取消禁言的 的是当前用户
            isPersonBandSpeak = stopTalkType
        }
    }
    
    func chatVoiceViewClick(voiceImageV: CRVoiceImageView) {
        //停止对应的语音播放动画
        voiceImageV.stopPlayingAnimation()
    }
}

//MARK:-播放声音
extension CRChatController{
    //MARK 播放发送消息成功后的声音
    func playSendMessageSuccessVoice(){
        //音频文件的路径
        let path = Bundle.main.path(forResource: "send_msg", ofType: ".mp3")
        if  CRDefaults.getChatroomSendMessageVoice(){
            //播放声音
            if let pathWraper = path {
                playSoundWithPath(path: pathWraper)
            }
        }
    }
     func playSoundWithPath(path: String){
        let soundUrl = URL(fileURLWithPath: path)
        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: soundUrl)
//            audioPlayer.play()
            bombSoundEffect = try AVAudioPlayer(contentsOf: soundUrl as URL)
            bombSoundEffect?.play()
        } catch {
            print("音频文件找不到了")
        }
    }
    
    func playVolume() -> Void{
        let pathIndex = YiboPreference.getCurrentSoundEffect()
        let pathStr = SoundEffectPaths[pathIndex]
        let path = Bundle.main.path(forResource: pathStr, ofType: "mp3")
        playClickSoundWithPath(path: path!)
    }
    
    func playKaiJianVolume() -> Void{
        let path = Bundle.main.path(forResource: "open_result", ofType: "mp3")
        playSoundWithPath(path: path!)
    }
    
    func playClickSoundWithPath(path: String){
        let url = NSURL(fileURLWithPath: path)
        
        do {
            //            #if DEBUG
            //
            //            #else
            bombSoundEffect = try AVAudioPlayer(contentsOf: url as URL)
            bombSoundEffect?.play()
            //            #endif
        } catch {
            print("音频文件找不到了")
        }
    }
}

extension CRChatController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        picker.dismiss(animated: true, completion: nil)
//        dismiss(animated: true, completion: nil)
        //获得照片
//        let image:UIImage = info[UIImagePickerControllerEditedImage] as! UIImage
        let  image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as! UIImage
        // 拍照
        if picker.sourceType == .camera {
            //保存相册
            UIImageWriteToSavedPhotosAlbum((image), self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
        }
        self.sendImageMessage(image:image)
//
    }
    
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
        
        if error != nil {
            
            print("保存失败")
            
            
        } else {
            
            print("保存成功")
            
            
        }
    }
}
//MARK: CRKeyboardDelegate
extension CRChatController :CRKeyboardDelegate{
    
    func chatKeyboardWillShow(keyboard: Any, animation: Bool) {
        
        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
    }
    
    func chatKeyboardDidShow(keyboard: Any, animation: Bool) {
        
        if (curStatus == .More && lastStatus == .Emoji) {

            emojiKeyboard.dismissWithAnimation(animation: false)
        }
        else if (curStatus == .Emoji && lastStatus == .More) {

            moreKeyboard.dismissWithAnimation(animation: false)
        }
        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
    }
    
    func chatKeyboardDidDismiss(keyboard: Any, animation: Bool) {
        
    }
    
    func chatKeyboardWillDismiss(keyboard: Any, animation: Bool) {
        
//        contentView.inputAreaView.snp.remakeConstraints() { (make) in
//            make.bottom.equalTo(self.contentView).offset(-KTabBarHeight)
//            make.left.right.equalTo(self.contentView)
//            make
//        }
//        view.layoutIfNeeded()
//        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
    }
    
    func chatKeyboardDidChangeHeight(keyboard: Any, height: CGFloat) {
        contentView.inputAreaView.snp.remakeConstraints() { (make) in
            make.bottom.equalTo(view).offset(-height)
            make.left.right.equalTo(contentView)
        }
        view.layoutIfNeeded()
        contentView.messageArea.msgTable.scrollToBottomAnimation(animation: true)
    }
    
}
extension CRChatController: CREmojiKeyboardDelegate{
    func emojiKeyboardDidTouchEmojiItemAtRect(emojiKeyboard: CREmojiKeyboard, emojiItem: CRExpressionItem, rect: CGRect) {
        
    }
    
    func emojiKeyboardCancelTouchEmojiItem(emojikeybarod: CREmojiKeyboard) {
        
    }
    
    func emojiKeyboardDidSelectedEmojiItem(emojiKeyboard: CREmojiKeyboard, emojiItem: CRExpressionItem) {
        
        contentView.inputAreaView.addEmojiString(emojiString: emojiItem.name)
        
    }
    
    func emojiKeyboardSendButtonDown() {
        
    }
    // 删除emoji
    func emjiKeyboardDeleteButtonDonw() {
        
        contentView.inputAreaView.deleteLastCharacter()
    }
    
    func emojiKeyboardEmojiEditButtonDown() {
        
    }
    
    func emojiKeyboardMyEmojiEditButtonDown() {
        
    }
    
    func emojiKeyboardSelectedEmojiGroupType(emojiKeyboard: CREmojiKeyboard, type: CREmojiType) {
        
    }
    
    func chatInputViewHasText() -> Bool {
        return true
    }
}

//MARK: - 反水 赔率slider 代理
extension CRChatController:SeekbarChangeEvent {
    func onSeekbarEvent(currentRate: Float, currentWin: Float, progressRate: Float,reload: Bool) {
        let numSelectArea = self.contentView.betArea.numSelectArea
        
        let isPeilvVersion = self.isPeilvVersion()
        if !isPeilvVersion {
            let officalBottom = numSelectArea.confirmBetArea.officalBottom
            let ratebackTV = officalBottom.ratebackTV!
            let currentOddTV = officalBottom.currentOddTV!
            self.current_odds = currentWin
            self.current_rate = currentRate
            
            var award = ""
            let awardStr = String.init(format: "%.3f", currentWin * multiplyValue)
            award = awardStr
            awardNum = awardStr
            award += "元"
            currentOddTV.text = award
            
            let ratebackStr = String.init(format: "%.3f",currentRate)
            if let ratebackFloat = Float(ratebackStr) {
                ratebackTV.text = "\(ratebackFloat)"  + "%"
            }
        }else {
            self.current_rate = currentRate
            
            let creditbottomSlideLeftStr = String.init(format: "%.3f", currentRate)
            if let creditbottomSlideLeftFloat = Float(creditbottomSlideLeftStr) {
                numSelectArea.confirmBetArea.peilvBottom.creditbottomSlideLeftLabel.text = "\(creditbottomSlideLeftFloat)" + "%"
            }
            
            numSelectArea.numSelectView.dynamicCalculateOdds(currentRate: progressRate, handler: {[weak self] (honest_odds) in
                if let weakSelf = self {
                    weakSelf.honest_odds = honest_odds
                    numSelectArea.numSelectView.numSelectTableView.reloadData()
                }
            })
        }
    }
}

//MARK: - 业务事件
extension CRChatController {
    //MARK: 跟单
    
    ///合并跟单
    func followMergeLogic(message:CRShareBetMessageItem,moneyOrMulti:String) {
        guard let shareBetModel = message.shareBetModel,let _ = shareBetModel.betInfos,let orders =   shareBetModel.orders else {
            showToast(view: self.view, txt: "跟单信息有误")
            return
        }
        
        var parameters = [String:Any]()
        parameters["code"] = CRUrl.shareInstance().CODE_FOLLOW_BET
        parameters["userId"] = CRDefaults.getUserID()
        parameters["source"] = "app"
        parameters["roomId"] =  chat_roomId
        parameters["msgUUID"] = CRCommon.generateMsgID()
        parameters["betMoney"] = moneyOrMulti
        parameters["userIp"] = localIP
        
        var ordersIds = ""
        for (index,order) in orders.enumerated() {
            ordersIds = ordersIds + order.orderId
            
            if index + 1 != orders.count {
                ordersIds += ","
            }
        }
        
        parameters["orderIds"] = ordersIds
        sendMsgFollowBet(parameter:parameters)
    }
    
    ///单注跟单
    func followLogic(message:CRShareBetMessageItem,betModel:Int,inIndex:Int) {
        contentView.messageArea.removeFollowPop()
        
        var p = [String:Any]()
        
        guard let shareBetModel = message.shareBetModel,let betInfos = shareBetModel.betInfos else {
            showToast(view: self.view, txt: "跟单信息有误")
            return
        }
        
        let betInfo = betInfos[inIndex]
        
//        if let shareBetModel = message.shareBetModel {
        
        p["code"] = CRUrl.shareInstance().CODE_FOLLOW_BET
        p["userId"] = CRDefaults.getUserID()
        p["source"] = "app"
        p["roomId"] =  chat_roomId
        p["msgUUID"] = CRCommon.generateMsgID()
//        p["qihao"] = betInfo.lottery_qihao + ""
        p["qihao"] = currentShareQihao + ""
        p["userIp"] = localIP
        
        var moneyValid = false //判断金额是否有效
        
        //官方
        if betInfo.version == "1" {
            if let beishu = followBetPop.inputField.text {
                if let beishu = Int(beishu) {
                    if beishu > 0 {
                        p["betMoney"] = "\(beishu)"
                        p["model"] = betModel //原始元角分模式
                        moneyValid = true
                    }
                }
            }
        }else {
            if let money = followBetPop.inputField.text {
                if let moneyDouble = Double(money) {
                    if moneyDouble > 0 {
                        //                                var finalMoney = moneyDouble
                        
                        let moneyString = formatPureIntIfIsInt(value: "\(moneyDouble)")
                        p["betMoney"] = moneyString
                        p["model"] = 1
                        
                        moneyValid = true
                    }
                }
            }
        }
        
        if !moneyValid {
            showToast(view: self.view, txt: "请输入正确的金额")
            return
        }
        
        var orderId = ""
        if let orderIdWraper = shareBetModel.betInfo?.orderId {
            orderId = orderIdWraper
        }
        
        if orderId.isEmpty {
            if let orders = shareBetModel.orders {
                orderId = orders[inIndex].orderId
            }
        }
        
        if orderId.isEmpty {
            showToast(view: view, txt: "注单id为空")
            return
        }
        
        p["orderId"] = orderId
        sendMsgFollowBet(parameter:p)
//        }
    }
    
    /// 显示快速发言列表
    func showSpeakQuicklyList(list: [CRSpeakQuicklyModel]) {
        
        let alertController = UIAlertController(title: "快速发言", message: nil, preferredStyle: .actionSheet)
        for model in list {
            let style = UIAlertAction.Style.default
            let listAction = UIAlertAction(title: model.record, style: style, handler: {(alert: UIAlertAction) in
                
                self.sendTxtMsgRequest(text: model.record,speakType:"1")
            })
            alertController.addAction(listAction)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        if alertController.actions.count > 5 {
            /// 设置高度
            alertController.view.addConstraint(NSLayoutConstraint(item: alertController.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.6))
        }
        
        
        self.present(alertController, animated: true, completion: nil)
    }
}
extension CRChatController:CRDrawerDelegate{
    func shareGainsAndLossesToday(gainsLossesItem:CRUserInfoCenterSourceWinLostItem) {
        if !prohibit && !roomProhibit{
            self.shareGainsAndLossesForToday(gainsLossesItem: gainsLossesItem)
        }else{
            showToast(view: view, txt: "禁言中无法发送消息!")
        }
    }
    //刷新最新数据
    func refreshNewDataGainsAndLossess(){
        self.sendMsgUserIconAndnickNameData()
    }
}

extension CRChatController: CRDrawerInfoDelegate {

    //点击抽屉头像
    func tapDrawerAvatar() {
        
        drawer?.closeAction()
        showUserProfileCtrl()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let info = self.userInfoCenter,let topControlelr = self.navigationController?.topViewController as? CRUserProfileInfoCtrl {
                topControlelr.updateUserInfo(userItem: info)
            }
        }
    }
    
    func drawerDeposit() { //充值
        drawer?.closeAction()
        goDepositOrWithdraw(index: 2)
    }
    
    func drawerWithdraw() { //提款
        drawer?.closeAction()
        goDepositOrWithdraw(index: 3)
    }
    
    func drawerRefreshBalance() { //刷新余额
        sendMsgRequestUserBalance(userId: CRDefaults.getUserID())
    }
}

///MARK: 竖向悬浮按钮代理
extension CRChatController: CRRemoteViewDelegate {
    func remoteViewClick(type: CRRemoteViewType) {
        switch type {
        case .CRRemoteViewTypeLotteryList: //中奖榜单
            let status = switcnWinnerAndProfitListStatus()
            if status == 1 {
                getLotterResults(prizeType: 2)
                getLotterResults(prizeType: 4)
            }else if status == 2 {
                getLotterResults(prizeType: 2)
            }else if status == 3 {
                getLotterResults(prizeType: 4)
            }
            
            showWinAndProfitListPop() //显示弹窗
            break
        case .CRRemoteViewTypeSignIn: //签到
            sendMsgSignIn(userId: chat_userId)
            break
        case .CRRemoteViewTypeOnlineService: //在线客服
            if let onlineServer = getChatRoomSystemConfigFromJson()?.source?.name_station_online_service_link {
                if !onlineServer.isEmpty {
                    openActiveDetail(controller: self, title: "在线客服", content: "", foreighUrl: onlineServer, isChat: true)
                    return
                }
            }
            
            showToast(view: self.view, txt: "没有在线客服链接，请检查是否配置")
            break
        case .CRRemoteViewTypeMasterPlan: //导师计划
            let vc = CRMasterPlanController()
            
            vc.getTutorPlanHandler = {[weak self] () in //拉取导师计划消息
                guard let weakSelf = self else {return}
                weakSelf.sendMsgTutorPlan(roomId: weakSelf.chat_roomId)//导师计划
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case .CRRemoteViewTypeLongDragon:   //长龙
            sendMsgLongDragon()
            break
        }
    }
}












