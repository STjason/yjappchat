//
//  CRChatController+Socket.swift
//  gameplay
//
//  Created by admin on 2019/11/1.
//  Copyright © 2019年 yibo. All rights reserved.
// socket收发消息相关

import Foundation
import PKHUD
import SocketIO
import HandyJSON
import Alamofire

extension CRChatController {
    ///初始化socket
    func netAuthenticate(model:CRAuthenticateModel) {
        
        /* 同步中转系统返回的聊天室域名和文件系统域名*/
        var chatFileUrl = model.chatFileUrl
        let array = model.chatFileUrl.components(separatedBy: "/")
        if let components = array.last {
            if components == "" {
                chatFileUrl = chatFileUrl.subString(start: 0, length: (chatFileUrl.length - 1))
            }
        }
        
        if chatFileUrl.length != 0 {
            CRUrl.shareInstance().CHAT_FILE_BASE_URL = chatFileUrl
        }
        
        CRUrl.shareInstance().CRCHAT_URL = model.chatDomainUrl
        authenticateModel = model
        
        setupNewSocket()
    }
    
    //socket初始化
    func setupNewSocket() {
        socket = nil
        
        let params = ["key":"#!@$%&^*AEUBSJXK","token":CRCommon.generateMsgID()]
        guard let paramsString = jsonStringFrom(dictionary: params) else {
            return
        }
        
//        /////////////////
        let a = Encryption.Endcode_AES_ECB(key: aesKey, iv: aesIV, strToEncode: "aceg")
        let b = Encryption.Decode_AES_ECB(key: aesKey, strToDecode: a) ?? ""
        print("a = \(a),\nb = \(b)")
//        /////////////////
        
        let encryParams = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: paramsString)
        
        let chatURL = CRUrl.shareInstance().CRCHAT_URL
        guard let url = URL.init(string: chatURL) else {return}
        
        print("\n初始化SocketIOClient,参数:\(params)\nurl:\(url)")
        
        let optionParams = SocketIOClientOption.connectParams(["key":encryParams])
        let optionForceNew = SocketIOClientOption.forceNew(true)
        let optionLog = SocketIOClientOption.log(false)
        let optionCompress = SocketIOClientOption.compress
        let forceWebsockets = SocketIOClientOption.forceWebsockets(true)
        let config = SocketIOClientConfiguration.init(arrayLiteral: optionParams,optionForceNew,optionLog,optionCompress,forceWebsockets)
        
        let manager = SocketManager(socketURL: url, config: config)
        socketManager = manager
        socket = manager.defaultSocket
        socket?.connect()
        socketHandler()
    }
    
    func socketHandler() {

        socket?.off("new message")
        socket?.off(clientEvent: .connect)
        socket?.off(clientEvent: .disconnect)
        socket?.off(clientEvent: .error)
        socket?.off(clientEvent: .reconnect)
        socket?.off(clientEvent: .reconnectAttempt)
        socket?.off(clientEvent: .statusChange)

        HUDView.hide(animated: true)
        HUDView = getHUD(text: "聊天室连接中...")

        socket?.on("new message") { (data, ack) in
            print("socket==========2\nnew message dada:\(data),ack = \(ack)")
        }

        socket?.on(clientEvent: .connect) {[weak self] (data, ack) in

            print("socket==========3\nsocket.on connect dada:\(data),ack = \(ack)")
            guard let weakSelf = self else {
                return
            }

            weakSelf.HUDView.hide(animated: true)

            isSocketConnect = true
            weakSelf.canReconnect = true
            weakSelf.observeSocket()
            weakSelf.sendMessageloginRoom(model: authenticateModel)
        }

        socket?.on(clientEvent: .disconnect) {[weak self] (data, ack) in
            print("socket==========4\nsocket.on disconnect dada:\(data),ack = \(ack)")
            isSocketConnect = false
            guard let weakSelf = self else {return}
            weakSelf.HUDView.hide(animated: true)
        }

        socket?.on(clientEvent: .error) {[weak self] (data, ack) in

            isSocketConnect = false
            print("socket==========5\nsocket.on error dada:\(data),ack = \(ack)")
            guard let weakSelf = self else {return}

            if !NetwordUtil.isNetworkValid()
            {
                weakSelf.HUDView.hide(animated: true)
                showToast(view: weakSelf.view, txt: "网络连接不可用，请检测")
            }else {

                weakSelf.canReconnect = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {

                    weakSelf.canReconnect = true
//                    weakSelf.HUDView.hide(animated: true)

                    if !isSocketConnect {
//                        showErrorHUD(errStr: "聊天室即将重连...") 切换房间
                        socketManager?.reconnect()
                    }
                })
            }
        }

        socket?.on(clientEvent: .reconnect) {(data, ack) in
            print("socket==========6\nsocket.on reconnect dada:\(data),ack = \(ack)")
        }

        socket?.on(clientEvent: .reconnectAttempt) {[weak self] (data, ack) in
            print("socket==========7\nsocket.on reconnectAttempt dada:\(data),ack = \(ack)")

            guard let weakSelf = self else {return}
            weakSelf.HUDView.hide(animated: true)
            weakSelf.HUDView = weakSelf.getHUD(text: "聊天室连接中...")
        }

        socket?.on(clientEvent: .statusChange) {[weak self] (data, ack) in
            print("socket==========8\nsocket.on statusChange dada:\(data),ack = \(ack)")
            guard let _ = self else {
                return
            }
        }
    }

    //MARK: 监听自定义的socket事件
    func observeSocket() {
        
        socket?.off(CR_PUSH_R)
        socket?.off(CR_LOGIN_R)
        socket?.off(CR_USER_R)
        socket?.off(CR_USER_JOIN_ROOM_R)
        socket?.off(CR_USER_JOIN_GROUP_R)
        socket?.off(CR_REONNNECT_R)
        
        socket?.on(CR_PUSH_R) {[weak self] (data, ack) in //推送

            guard let weakSelf = self else {return}
            if let messages = weakSelf.formatMsgModelsWithStrings(datas: data) {
                for model in messages{
                    weakSelf.didReceivedMessage(message: model)
                }
            }
        }
        
        socket?.on(CR_USER_JOIN_GROUP_R) {[weak self] (data,ack) in //加入组 管理员
            guard let weakSelf = self else {return}
            
            weakSelf.receiveUserMsg(msgType:CR_USER_JOIN_GROUP_R, datas: data, handler: { (success, msg, model,type,_,_) in
                print("")
            })
        }
        
        socket?.on(CR_REONNNECT_R) {[weak self] (data,ack) in //重连
            guard let weakSelf = self else {return}
            
        }
        
        //MARK:发消息回调
        socket?.on(CR_USER_R) { [weak self] (data,ack) in //发消息回调
            guard let weakSelf = self else {return}
            weakSelf.handleReceiveUserMsgWith(data: data)
        }
        
        socket?.on(CR_LOGIN_R) {[weak self] (data,ack) in //登录
            guard let weakSelf = self else {return}
            
            weakSelf.receiveUserMsg(msgType:CR_LOGIN_R, datas: data, handler: { (success, msg, model,type,_,_) in
                if type == CRUrl.shareInstance().LOGIN_AUTHORITY {
//                    if !success {
//                        weakSelf.kickOutRoom()
//                        return
//                    }
                    
                    if let m = model as? CRAuthMsgModel {
                        weakSelf.handleLogin(model: m)
                    }
                }
            })
        }
        
        socket?.on(CR_USER_JOIN_ROOM_R) {[weak self] (data,ack) in //换房间
            guard let weakSelf = self else {return}
            
            weakSelf.receiveUserMsg(msgType:CR_USER_JOIN_ROOM_R, datas: data, handler: { (success, msg, model,type,_,_)   in
                if !success {
                    showToast(view: weakSelf.view, txt: msg)
                    return
                }
                
                guard let roomInfo = weakSelf.preRoomInfo else {
                    showToast(view: weakSelf.view, txt: "切换房间失败，请重试")
                    return
                }
                
                weakSelf.handleInsideRoom(roomInfo: roomInfo)
            })
        }
    }
    
    func handleSignin(model:CRSignModel) {
        let alert = UIAlertController.init(title: "连续签到 \(model.days)天", message: "签到积分 \(model.score)", preferredStyle: .alert)
        let action = UIAlertAction.init(title: "确定", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: 进入房间成功
    func handleInsideRoom(roomInfo:CRRoomInfoModel) {
//        self.roomInfo = roomInfo //房间信息，包括房间权限等
        
        if contentView.messageArea.toolPermission.count == 0 {
            contentView.messageArea.toolPermission = roomInfo.toolPermission
            
            if roomInfo.toolPermission.count == 0 {
                sendGetToolPermission()//请求工具箱权限
            }
        }
        
        //切换房间后，清除历史记录
        historyStart = 0
        
        //进入房间
        if  roomInfo.permissionObj?.ENTER_ROOM == "1"{
            CRDefaults.setChatroomEnterRoom(value: true)
        }else{
            CRDefaults.setChatroomEnterRoom(value: false)
            showToast(view: self.view, txt: "您没有权限进入改房间,请联系客服")
            return
        }
        
        navigationBar.rightView.isHidden = false
        contentView.messageArea.showRemoter(show:true)
        
        userType = roomInfo.userType
        contentView.messageArea.userType = userType
        CRDefaults.setUserType(value: userType)
        CRDefaults.setPlanUser(value: roomInfo.planUser)
        
        contentView.hasCloseWinnerInfo = false
        winnerTimer?.invalidate()
        //是否是该房间的代理人
        agentRoomHost = roomInfo.agentRoomHost
        //发送投注
        if roomInfo.permissionObj?.SEND_BETTING == "1"{
            CRDefaults.setChatroomSendBetting(value: true)
        }else {
            CRDefaults.setChatroomSendBetting(value: false)
        }
        
        sendMsgPullRoomConfig() //拉取房间配置
        
        //发送表情
        if roomInfo.permissionObj?.SEND_EXPRESSION == "1"{
            CRDefaults.setChatroomSendExpression(value: true)
        }else{
            CRDefaults.setChatroomSendExpression(value: false)
        }
        
        //发送文本
        if  roomInfo.permissionObj?.SEND_TEXT == "1"{
            CRDefaults.setChatroomSendText(value: true)
        }else{
            CRDefaults.setChatroomSendText(value: false)
        }
        //收红包
        if  roomInfo.permissionObj?.RECEIVE_RED_PACKET == "1"{
            CRDefaults.setChatroomReceiveRedPacket(value: true)
        }else{
            CRDefaults.setChatroomReceiveRedPacket(value: false)
        }
        //发红包
        if roomInfo.permissionObj?.SEND_RED_PACKET == "1"{
            CRDefaults.setChatroomSendRedPacket(value: true)
        }else{
            CRDefaults.setChatroomSendRedPacket(value: false)
        }
    
        if  roomInfo.permissionObj?.SEND_IMAGE == "1"{
            CRDefaults.setChatroomSendImage(value: true)
        }else{
            CRDefaults.setChatroomSendImage(value: false)
        }
    
        if roomInfo.permissionObj?.FAST_TALK == "1"{
            CRDefaults.setChatroomFastTalk(value: true)
        }else{
            CRDefaults.setChatroomFastTalk(value: false)
        }
        
        //房间配置器 音频配置
        if roomInfo.permissionObj?.SEND_AUDIO == "1" {
            CRDefaults.setChatroomSendVoice(value: true)
        }else{
            CRDefaults.setChatroomSendVoice(value: false)
        }
    
        let roomsTopView = contentView.roomsTopView
        let cellCount = roomsTopView.getCellCount()
        if lastRoomIndex != (cellCount - 1) {
            let canSwitchRoom = getChatRoomSystemConfigFromJson()?.source?.switch_room_show != "0"
            navigationBar.setTitleButtonCanClick(can: canSwitchRoom, title: roomInfo.title)
            
            roomsTopView.currentSelectIndex = lastRoomIndex
            roomsTopView.collection.reloadData()
        }
    
        contentView.hasCloseWinnerInfo = false
        winnerTimer?.invalidate()

//            getRoomNotice(paramters: noticeParamter)
    

        //进入房间成功 拉取该房间的历史记录
//            getHistoryRecordData()
        //获取系统公告
        sendMsgSystemNoticeRequest()
        //更新标题房间名称
        navigationBar.titleButton.setTitle(roomInfo.title, for: .normal)
        
        isRoomBandSpeak = roomInfo.isBanSpeak == 1 ? "1" : "0"
        isPersonBandSpeak = roomInfo.speakingFlag == 1 ? "1" : "0"

        chat_roomId = roomInfo.roomId
        last_chat_roomId = chat_roomId
        //进入房间成功 拉取该房间的历史记录
        
        pullLotterReward()
        //会员图像
        userIconUrl = roomInfo.avatar
        //会员等级图标
        userLevelIcon = roomInfo.levelIcon
        //会员等级
        userLevelName = roomInfo.level
        //会员昵称
        
        if roomInfo.nickName.count > 0{
            userNickname = roomInfo.nickName
        }else{
            userNickname = roomInfo.account
        }
        
        userNativeAccount = roomInfo.account
        
        //更新房间背景图
        updateRoomBg(img:(roomInfo.backGround))
        //更新标题房间名称
        navigationBar.titleButton.setTitle(roomInfo.title, for: .normal)
        //房间名字 推送通知显示
        roomName = roomInfo.title
    
    }
    
    //MARK: 获取房间列表成功之后的处理
    func handleGetRoomList(rooms:CRRoomListModels) {
        
        roomList.removeAll()
        
        var agentRoomId = ""//代理房 id
        var defaultRoomId = "" //默认房 id
        
        //代理房间
        if let model = authMsgModel,let agentRoom = model.agentRoom,let listModel = model.roomEntify {
            agentRoomId = agentRoom.id
            roomList.insert(listModel, at: 0)
        }
        
        //为用户设置的默认房间，插入于代理房间之前
        if let model = authMsgModel,let agentRoom = model.userConfigRoom {
            defaultRoomId = agentRoom.id
        }
   
        if let d = rooms.data {
            if d.count > 0 {
                for roomDetailItem in d{
                    if ![agentRoomId].contains(roomDetailItem.id) {
                        if defaultRoomId == roomDetailItem.id {
                            roomDetailItem.roomKey = ""
                            roomList.insert(roomDetailItem, at: 0)
                        }else {
                            roomList.append(roomDetailItem)
                        }
                    }
                }
            }
        }
        
        self.contentView.roomsTopView.configWith(models: self.roomList)
        var roomIndex = 0
        if self.isNotiFrom { //如果从通知进来的
            roomIndex = handleInsideFromNoti()
        }else{
            
            last_chat_roomId = CRDefaults.getLastRoom()
            if !last_chat_roomId.isEmpty {
                for (index,model) in roomList.enumerated() {
                    if model.id != last_chat_roomId && (index + 1 == self.roomList.count) {
                        sendMsgInsideChatRoom(model: roomList[roomIndex],roomIndex: roomIndex)
                    }else if model.id == last_chat_roomId {
                        roomIndex = index
                        sendMsgInsideChatRoom(model: model,roomIndex:roomIndex)
                        break
                    }
                }
            }else if let roomItem = self.roomList.first { //默认进入列表中的第一个房间
                sendMsgInsideChatRoom(model: roomItem,roomIndex:0)
            }
        }
        
        if roomList.count == 0 {
            isRoomBandSpeak = "1"
            showToast(view: (self.view)!, txt: "暂无房间可以进入")
            return
        }
        
        let roomsTopView = self.contentView.roomsTopView
        let canSwitchRoom = getChatRoomSystemConfigFromJson()?.source?.switch_room_show != "0"
        //第一次进入房间的标题不用直接显示 有可能是密码房
        self.navigationBar.setTitleButtonCanClick(can: canSwitchRoom, title: "")
        roomsTopView.currentSelectIndex = roomIndex
        roomsTopView.collection.reloadData()
    }
    
    //MARK: 获取长龙数据
    func sendMsgLongDragon() {
        
        HUDView.hide(animated: true)
        HUDView = getHUD(text: "长龙数据获取中")
        
        let code = "\(sendMessageType.longDragon.rawValue)"
        let parameters = ["code":code,
                          "stationId": "\(chat_stationId)",
            "channelId":"ios",
            "userId":CRDefaults.getUserID(),
            "source":"app"]
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameters)
    }
    
    //处理从通知进入聊天室
    func handleInsideFromNoti() -> Int {
        var roomIndex = 0
        
        if isPrivateChat {
            sendMsgRequestPrivateChat(type: "1", model: passiveUser)
            isPersonBandSpeak = "0"
            isRoomBandSpeak = "0"
            
            navigationBar.rightView.isHidden = false
            contentView.messageArea.showRemoter(show:true)
        }else {
            for (index,value) in self.roomList.enumerated(){
                if value.id != self.chat_roomId && (index + 1 == self.roomList.count) {
                    sendMsgInsideChatRoom(model: roomList[roomIndex],roomIndex: roomIndex)
                }else if value.id == self.chat_roomId{
                    sendMsgInsideChatRoom(model: roomList[index],roomIndex: index)
                    roomIndex = index
                    return roomIndex
                }
            }
        }
        
        return roomIndex
    }
    
    //MARK: -  私聊数据 返回model的处理
    //MARK: 处理私聊发送消息相关model, ，privateType type的值 表示具体功能, 2历史记录，1发消息
    func handlePrivateChatSend(model:Any?, privateType:String?) {
        if privateType == "2" {
            if let m = model as? CRPrivateHistoryRecordItem {
                handleHistoryRecord(historys: m.source?.data ?? [],isPrivateChat:true)
            }

        }else if privateType == "1" {
            
        }
    }
    
    //MARK: 处理私聊初始化相关model
    func handlePrivateChatInitWith(model:Any?) {
        
        if let initWraper = model as? CRPrivateChatInitialWraper { //初始化私聊
            if let model = initWraper.source {
                sendMsgChangeGroup(model: model) //切换私聊房间
            }
            
        }else if let listWraper = model as? CRPrivateChatListWraper { //拉取私聊列表
            guard let source = listWraper.source,let list = source.roomList else {
                showToast(view: self.view, txt: "拉取私聊列表失败")
                return
            }
            
            self.drawer?.subviewsBGView.updatePrivateChatWith(list: list) //赋值并刷新私聊列表
        }
    }
    
    //MARK: 处理私聊 发消息相关 response, type 私聊里 用type判断具体功能
    func handlePrivateChatSendHandleWith(code:String, response:String,handler:@escaping (_ success:Bool,_ mzsg:String, _ model:Any?,_ type:String?,_ msgUUID:String?, _ privateType:String?) -> ()) {
        var funcDes = "私聊处理"
        
        guard let baseWraper = CRPrivateChatBaseWraper.deserialize(from: response) else {
            handler(false,"\(funcDes)失败",nil,code, nil, nil)
            return
        }
        
        if let baseSourcce = baseWraper.source {
            let type = baseSourcce.type
            if type == "2" { //历史记录
                guard let wraper = CRPrivateHistoryRecordItem.deserialize(from: response) else {
                    funcDes = "拉取私聊历史记录"
                    handler(false,"\(funcDes)失败",nil,code, nil, type)
                    return
                }
                
                if wraper.success {
                    handler(true,"",wraper,code, nil, type)
                }else {
                    handler(false,wraper.msg.isEmpty ? funcDes : wraper.msg, wraper,code, nil, type)
                }
            }else if type == "1" { //发送消息
                
//                class PrivateWraper: HandyJSON {
//                    var success = false
//                    var msg = ""
//                    var source: PrivateMsg?
//                    required init() {}
//                }
//
//                class PrivateMsg: HandyJSON {
//                    var msgUUID = ""
//                    required init() {}
//                }
//
//                guard let wraper = PrivateWraper.deserialize(from: response),let msg = wraper.source else {
//                    funcDes = "私聊发送消息"
//                    handler(false,"\(funcDes)失败",nil,code,nil,type)
//                    return
//                }
//
//                if wraper.success {
//                    updateMsgOnCell(msgUUID: msg.msgUUID, sendStatus: .Success)
//                }else {
//                    showToast(view: self.view, txt: wraper.msg.isEmpty ? funcDes : wraper.msg)
//                }
            }
        }
        
        print("")
    }
    
    
    //MARK: 处理私聊 初始化相关 response
    func handlePrivateChatInitalHandleWith(code:String, response:String,handler:@escaping (_ success:Bool,_ mzsg:String, _ model:Any?,_ type:String?,_ msgUUID:String?) -> ()) {
        
        var funcDes = "私聊处理"
        
        guard let baseWraper = CRPrivateChatBaseWraper.deserialize(from: response) else {
            handler(false,"\(funcDes)失败",nil,code, nil)
            return
        }
        
        if baseWraper.success {
            
            if let baseSource = baseWraper.source {
                if baseSource.type == "1" {
                    funcDes = "初始化私聊"
                    
                    guard let wraper = CRPrivateChatInitialWraper.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code, nil)
                        return
                    }
                    
                    if wraper.success {
                        handler(true,"",wraper,code, nil)
                    }else {
                        handler(false,wraper.msg,wraper,code, nil)
                    }
                    
                }else if baseSource.type == "2" {
                    funcDes = "拉取私聊列表"
                    
                    guard let wraper = CRPrivateChatListWraper.deserialize(from: response),let source = wraper.source,let _ = source.roomList  else {
                        handler(false,"\(funcDes)失败",nil,code, nil)
                        return
                    }
                    
                    if wraper.success {
                        handler(true,"",wraper,code, nil)
                    }else {
                        handler(false,wraper.msg,wraper,code, nil)
                    }
                    
                }else if baseSource.type == "join" {
                    print("")
                }else if baseSource.type == "3" {
                    
                }else if baseSource.type == "4" {
                    
                }
            }
        }else {
            showToast(view: self.view, txt: baseWraper.msg.isEmpty ? "私聊初始化相关操作失败" : baseWraper.msg)
        }
    }
    
    //MARK: 长龙私聊 初始化相关 response
    func handleLongDragonWith(code:String, response:String,handler:@escaping (_ success:Bool,_ mzsg:String, _ model:Any?,_ type:String?,_ msgUUID:String?) -> ()) {
        
        guard let baseWraper = CRLongDragonBaseWraper.deserialize(from: response) else {
            handler(false,"\("长龙初始化")失败",nil,code, nil)
            return
        }
        
        if baseWraper.success {
            if let longDragonModel: CRLongDragonModel = baseWraper.source{
                if let longDragonLists: [CRLongDragonList] = longDragonModel.longDragon{
                    if longDragonLists.count > 0{
                        let longDragonView =  CRLongDragonListView.init(frame: UIScreen.main.bounds, longDragonLists: longDragonLists)
                        UIApplication.shared.keyWindow?.addSubview(longDragonView)
                        
                    }else{
                        showToast(view: self.view, txt: "没有长龙数据")
                    }
                }
                else{
                    showToast(view: self.view, txt: "没有长龙数据")
                }
            }else{
                showToast(view: self.view, txt: "没有长龙数据")
            }
            
        }else {
            showToast(view: self.view, txt: baseWraper.msg.isEmpty ? "长龙初始化相关操作失败" : baseWraper.msg)
        }
        
        self.HUDView.hide(animated: true)
    }
    
    //MARK: - socket消息，如发送文本，分享注单，发送图片等
    
    //MARK: 获取权限 如 禁言，撤回等
    func sendGetToolPermission() {
        let code = "\(sendMessageType.toolPermisson.rawValue)"
        
        let parameters = ["code":code,
                          "userId":chat_userId,
                          "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameters)
    }
    
    //MARK: - 私聊发送消息
    //MARK: type:1初始化私聊,type:2 拉取私聊列表,type:3 把私聊消息都变成已读,type:4 设置私聊备注
    /// type:1初始化私聊,type:2 拉取私聊列表,type:3 把私聊消息都变成已读,type:4 设置私聊备注
    ///
    /// - Parameters:
    ///   - model: type为1时需要传，用以获取被邀请私聊用户的id，userType，pNickName
    func sendMsgRequestPrivateChat(type:String,model:CROnLineUserModel?) {
        let code = "\(sendMessageType.privateChatInit.rawValue)"
        
        var parameters = ["type":type,
                          "code":code,
                          "stationId": chat_stationId,
                          "userId":chat_userId,
                          "source":"app"]
        
        if type == "1" {
            if let m = model {
                passiveUser = m
                
                parameters["passiveUserId"] = m.id
                parameters["userType"] = m.userType
                parameters["pNickName"] = !m.nickName.isEmpty ? m.nickName : m.account
            }
        }else if type == "2" {
            print("拉取私聊列表")
            parameters["userType"] = "\(userType)"
        }else if type == "3" {
            if let m = passiveUser {
                parameters["userType"] = m.userType
            }
            
            parameters["passiveUserId"] = chat_passiveUserId
            parameters["roomId"] = chat_privateRoomId
            parameters["userType"] = "\(userType)"
        }
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameters)
    }
    
    //MARK: 进入私聊房间
    func sendMsgInsidePrivateChatGroup(type:String ,model:CRPrivateChatRoomList) {
        let code = "\(sendMessageType.privateChatInit.rawValue)"
        
        var parameters = ["type":type,
                          "code":code,
                          "stationId": "\(chat_stationId)",
            "userId":chat_userId,
            "source":"app"]
        
            if type == "1" {
                if let m = model.fromUser {
                    passiveUser = model.fromUser
                    parameters["passiveUserId"] = m.id
                    parameters["userType"] = m.userType
                    parameters["pNickName"] = !m.nickName.isEmpty ? m.nickName : m.account
                }
            }else if type == "3" {
                if let m = passiveUser {
                    parameters["userType"] = m.userType
                }
                
                parameters["passiveUserId"] = chat_passiveUserId
                parameters["roomId"] = chat_privateRoomId
                parameters["userType"] = "\(userType)"
            }
        
        sendSysMessage(msgType: CR_USER_S, code:code,parameters: parameters)
    }
    
    //MARK: 拉取私聊历史 type=2 查询私聊历史记录; type=1，在私聊房间发消息
    func sendMsgPrivateChatHistory(roomId:String,type:String) {
        let code = "\(sendMessageType.privateHistory.rawValue)"
        let parameters = ["code":code,
                          "type":type,
                          "start":"0",
                          "count":"30",
                          "roomId":roomId,
                          "userId":chat_userId,
                          "stationId": "\(chat_stationId)",
                          "passiveUserId":chat_passiveUserId,
                          "source":"app"]
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameters)
    }
    
    //MARK: 私聊切换房间
    func sendMsgChangeGroup(model:CRPrivateChatInitialModel) {
        let code = "\(sendMessageType.privateChatSend.rawValue)"
        
        let parameters = ["code":code,
                          "type":"join",
                          "roomId":model.roomId,
                          "userId":chat_userId,
                          "stationId": "\(chat_stationId)",
                          "source":"app"]
        
        sendSysMessage(msgType: CR_USER_JOIN_GROUP_S, code:code,parameters: parameters)
    }
    
    //签到
    func sendMsgSignIn(userId:String) {
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.signIn.rawValue)",
                         "channelId":"ios",
                         "roomId":chat_roomId,
                         "userId":chat_userId,
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.signIn.rawValue)", parameters: parmaters)
    }
    
    //导师计划
    func sendMsgTutorPlan(roomId:String) {
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.tutorPlan.rawValue)",
                         "channelId":"ios",
                         "roomId":roomId,
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.tutorPlan.rawValue)", parameters: parmaters)
    }
    
    //获取当前余额
    func sendMsgRequestUserBalance(userId:String) {
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.currentBalance.rawValue)",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "roomId":chat_roomId,
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.currentBalance.rawValue)", parameters: parmaters)
    }
    
    //MARK: 获取在线用户
    func sendMsgRequestOnLineList() {
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.roomOnLineList.rawValue)",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "roomId":chat_roomId,
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.roomOnLineList.rawValue)", parameters: parmaters)
    }
    
    //MARK: 获取管理员列表
    func sendMsgRequestManagerList() {
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.managerList.rawValue)",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "roomId":chat_roomId,
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.managerList.rawValue)", parameters: parmaters)
    }
    
    //MARK: 提交用户修改个人信息
    func sendMsgSubmitInfo() {
        guard let topVC = self.navigationController?.topViewController as? CRUserProfileInfoCtrl else {
            return
        }
        
        let modifyNickName = topVC.modifyNickName
        let modifyAvatar = topVC.modifyAvatar
        
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.submitUserInfo.rawValue)",
                         "nickName":modifyNickName,
                         "avatar":modifyAvatar,
                         "msgUUID":CRCommon.generateMsgID(),
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.submitUserInfo.rawValue)", parameters: parmaters)
    }
    
    //MARK: 获取默认用户图片列表
    func sendMsgGetUserAvatarList(userId:String) {
        let parmaters = ["stationId":chat_stationId,
                         "code":"\(sendMessageType.userIcon.rawValue)",
                         "msgUUID":CRCommon.generateMsgID(),
                         "channelId":"ios",
                         "userId":userId,
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.userInfo.rawValue)", parameters: parmaters)
    }
    
    //MARK: 更改 user头像
    func sendMsgUpdateUserAvatarList(fileString: String){
        let parmaters = ["fileString":fileString,
                         "code":"\(sendMessageType.updatePersonnalAvatar.rawValue)",
                         "source":"app",
                         "stationId":chat_stationId,
                         "msgUUID":CRCommon.generateMsgID(),
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID()
                        ]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.updatePersonnalAvatar.rawValue)", parameters: parmaters)
    }
    
    //MARK: 获取当前用户图像和昵称和其他信息
    func sendMsgUserIconAndnickNameData() {
        let parmaters = ["code": "\(sendMessageType.userInfo.rawValue)",
                         "msgUUID":CRCommon.generateMsgID(),
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app",
                         "roomId":chat_roomId]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.userInfo.rawValue)", parameters: parmaters)
    }
    
    //MARK: 领红包的人的详情
    func sendMsgReceiverDataRequest(message:CRRedPacketMessage){
        let parmaters = ["payId":message.payId,
                         "roomId":chat_roomId,
                         "msgUUID":CRCommon.generateMsgID(),
                         "code":"\(sendMessageType.receiveDetail.rawValue)",
            "channelId":"ios",
            "userId":CRDefaults.getUserID(),
            "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.receiveDetail.rawValue)", parameters: parmaters)
    }
    
    //MARK:抢红包
    func sendMsgReceiveRedPacketMessageRequest(message:CRRedPacketMessage){
        let parmaters = ["payId":message.payId,
                         "roomId":chat_roomId,
                         //代理房间 1或0
            "agentRoomHost":agentRoomHost,
            //简易版
            "openSimpleChatRoom":"",
            "msgUUID":CRCommon.generateMsgID(),
            "code": "\(sendMessageType.receiveRedPacket.rawValue)",
            "channelId":"ios",
            "userId":CRDefaults.getUserID(),
            "source":"app"] as [String : Any]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.receiveRedPacket.rawValue)", parameters: parmaters)
    }
    
    //MARK: 获取投注历史
    func sendMsgBetHistory() {
        let historyMessage = CRLotteryHistoryMessage()
        historyMessage.code = CRUrl.shareInstance().CODE_HISTORY_SHARE
        historyMessage.dataType = "1"
        historyMessage.isPrivate = false
        historyMessage.stationId = chat_stationId
        historyMessage.userId = chat_userId
        historyMessage.source = "app"
        historyMessage.msgUUID = CRCommon.generateMsgID()
        historyMessage.roomId = chat_roomId
        
        guard let json = historyMessage.toJSON() else {
            return
        }
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.historyBetShare.rawValue)", parameters: json)
    
    }
    
    //MARK: 获取历史消息记录 ,start:第几页数据
    func getMsgHistoryRecordData(start:String = "0"){
        let parmaters = ["roomId":chat_roomId,
                         "stationId":self.msgModel.stationId,
                         "count":"\(historyCountPerPage)",
                         "start":start,
                         "msgUUID":CRCommon.generateMsgID(),
                         "code":"\(sendMessageType.historyRecord.rawValue)",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app"]
        
//        start,count
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.historyRecord.rawValue)", parameters: parmaters)
    }
    
    //MARK: 拉取开奖结果
    func sendMsgpullLotteryTicket() {
        let parmaters = ["roomId":chat_roomId,
                         "stationId":chat_stationId,
                         "context":"",
                         "noticeId":"",
                         "userType":"\(userType)",
                         "msgUUID":CRCommon.generateMsgID(),
                         "code":"\(sendMessageType.pullLotteryTicket.rawValue)",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.pullLotteryTicket.rawValue)", parameters: parmaters)
    }
    
    //MARK: 跟单
    func sendMsgFollowBet(parameter:[String:Any]) {
        print("跟单参数: \(parameter.description),date:\(Date())")
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.followOrder.rawValue)", parameters: parameter)
    }
    
    //MARK: 获得系统公告
    func sendMsgSystemNoticeRequest() {
        let parmaters = ["roomId":chat_roomId,
                         "stationId":chat_stationId,
                         "context":"",
                         "noticeId":"",
                         "userType":"\(userType)",
                         "msgUUID":CRCommon.generateMsgID(),
                         "code":"\(sendMessageType.systemNotice.rawValue)",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.systemNotice.rawValue)", parameters: parmaters)
    }
    
    /// 请求彩票计划列表数据
    func getGameListData(option: String, lotteryCode: String = "", playName: String = "") {
        let paramters = ["stationId": chat_stationId,
                         "option": option,
                         "lotteryCode": lotteryCode,
                         "playName": playName,
                         "stationList": "",
                         "lotteryResult": "",
                         "stage": "",
                         "sysUserAccount": "",
                         "code" : "R0028",
                         "source" : "app"]
        sendSysMessage(msgType: CR_USER_S, code: CRUrl.shareInstance().CODE_GAME_LIST, parameters: paramters)
    }
    
    /// 请求快速发言列表数据
    func getSpeakQuicklyListData() {
        let paramters = ["stationId": chat_stationId,
                         "roomId" : chat_roomId,
                         "userId" : CRDefaults.getUserID(),
                         "code" : "R0011",
                         "source" : "app"]
        sendSysMessage(msgType: CR_USER_S, code: CRUrl.shareInstance().CODE_SPEAKQUICKLY_LIST, parameters: paramters)
    }
    
    /// 新增图片收藏 option: 1---查询用户收藏的图片 2--收藏图片 3--删除收藏的图片
    func requestImageCollect(option: String, fileCode: String, imageIdKey: String = "imageId") {
        let paramters = ["stationId": chat_stationId,
                         "userId": CRDefaults.getUserID(),
                         "option": option,
                         imageIdKey: fileCode,
                         "code": "R0038",
                         "source": "app"]
        sendSysMessage(msgType: CR_USER_S, code: CRUrl.shareInstance().CODE_IMAGECOLLECT, parameters: paramters)
    }
    
    //MARK: - 分享注单
    //MARK: 分享注单 最终调用,当注单数超过15，会出现后台无法处理的情况，所以拆分注单数最多为15
    func sendMsgShareBetMessage() {        
        guard let modelP = shareBetModel,let betInfos = modelP.betInfos, let orders = modelP.orders, let _ = modelP.toJSON() else {
            showToast(view: self.view, txt: "注单错误")
            return
        }
        
        var inBetInfos = [CRShareBetInfo]()
        var inOrders = [CRShareOrder]()
        
        var betInfosCopy = betInfos + [CRShareBetInfo]()
        var ordersCopy = orders + [CRShareOrder]()
        
        for (index,betInfo) in betInfosCopy.enumerated() {
            
            inBetInfos.append(betInfo)
            inOrders.append(ordersCopy[index])
            
            if index + 1 == betInfosCopy.count { //发送完毕
                let model = getShareBetModelCopy(shareBetModel: modelP)
                
                model.betInfos = inBetInfos + [CRShareBetInfo]()
                model.orders = inOrders + [CRShareOrder]()
                
                guard let p = model.toJSON() else {
                    showToast(view: self.view, txt: "注单错误")
                    return
                }
                
                var parmaters = p
                var code = ""
                
                if isPrivateChat {
                    code = "\(sendMessageType.privateChatSend.rawValue)"
                    
                    parmaters["passiveUserId"] = chat_passiveUserId
                    parmaters["msgType"] = "5" //1 文本， 2 图片，4 分享今日输赢，5 分享注单
                    parmaters["type"] = 1
                    parmaters["code"] = code
                    parmaters["roomId"] = chat_privateRoomId
                }
                
                parmaters["stationId"] = chat_stationId
                sendShareBetMessage(model: model) //添加 分享注单消息到本地
                sendSysMessage(msgType: CR_USER_S, code: code, parameters: parmaters) //发送分享注单消息
                
                shareBetModel = nil
                inBetInfos.removeAll()
                inOrders.removeAll()
                playSendMessageSuccessVoice()
                
                return
            }else if ((index + 1) % 15) == 0 && index != 0 {
                let model = getShareBetModelCopy(shareBetModel: modelP)
                
                model.betInfos = inBetInfos + [CRShareBetInfo]()
                model.orders = inOrders + [CRShareOrder]()
                
                guard let p = model.toJSON() else {
                    showToast(view: self.view, txt: "注单错误")
                    return
                }
                
                var parmaters = p
                var code = ""
                
                if isPrivateChat {
                    code = "\(sendMessageType.privateChatSend.rawValue)"
                    
                    parmaters["passiveUserId"] = chat_passiveUserId
                    parmaters["msgType"] = "5" //1 文本， 2 图片，4 分享今日输赢，5 分享注单
                    parmaters["type"] = 1
                    parmaters["code"] =
                    parmaters["roomId"] = chat_privateRoomId
                }else {
                    code = CRUrl.shareInstance().CODE_SHARE_BET_MSG
                }
                
                parmaters["stationId"] = chat_stationId
                sendShareBetMessage(model: model) //添加 分享注单消息到本地
                sendSysMessage(msgType: CR_USER_S, code: code, parameters: parmaters) //发送分享注单消息
                
                inBetInfos.removeAll()
                inOrders.removeAll()
            }
            
        }
        
        betInfosCopy.removeAll()
        ordersCopy.removeAll()
    }
    
    func getShareBetModelCopy(shareBetModel:CRShareBetModel) -> CRShareBetModel {
        let model = CRShareBetModel()
        model.roomId = shareBetModel.roomId
        model.agentRoomHost = shareBetModel.agentRoomHost
        model.stationId = shareBetModel.stationId
        model.code = shareBetModel.code
        model.source = shareBetModel.source
        model.userId = shareBetModel.userId
        model.speakType = shareBetModel.speakType
        model.userType = shareBetModel.userType
        model.multiBet = shareBetModel.multiBet
        model.winOrLost = shareBetModel.winOrLost
        model.msgUUID = CRCommon.generateMsgID()
        return model
    }
    
    //MARK: - 系统socket消息入口
    //MAR: 禁言申述
    ///禁言申述
    func sendMsgAppealBand(remark:String) {
        let code = "\(sendMessageType.appealBand.rawValue)"
        let parameter = ["roomId":chat_roomId,
                         "taskType":"1",
                         "remark":remark,
                         "userId":CRDefaults.getUserID(),
                         "channelId":"ios",
                         "source":"app",
                         "stationId":chat_stationId,
                         "code":code]
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameter)
    }
    
    //MARK: 房间禁言
    ///close 0 解除禁言，1 禁言
    func sendMsgProhibitAll(messageItem:CRMessage,close:String) {
        let code = "\(sendMessageType.prohibitAll.rawValue)"
        let parameter = ["roomId":chat_roomId,
                         "isBanSpeak":close,
                         "operatorAccount":YiboPreference.getUserName(),
                         "channelId":"ios",
                         "source":"app",
                         "stationId":chat_stationId,
                         "code":code]
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameter)
    }
    
    //MARK: 禁言
    ///close 0 解除禁言，1 禁言
    func sendMsgProhibit(messageItem:CRMessage,close:String) {
        let code = "\(sendMessageType.prohibit.rawValue)"
        let parameter = ["roomId":chat_roomId,
                        "speakingClose":close,
                        "speakingCloseId":messageItem.userId,
                        "operatorAccount":YiboPreference.getUserName(),
                        "channelId":"ios",
                        "source":"app",
                        "code":code]
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameter)
    }
    
    //MARK: 撤回
    ///撤回
    func sendMsgRetract(messageItem:CRMessage) {
        let code = "\(sendMessageType.retract.rawValue)"
        let parameter = ["roomId":chat_roomId,
                         "msgId":messageItem.msgId,
                         "userType":"\(userType)",
                         "stationId":chat_stationId,
                         "channelId":"ios",
                         "source":"app",
                         "code":code]
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parameter)
    }
    
    //MARK: 拉取违禁词
    func sendMsgBandWords() {
        let paramters = [
            "stationId":"\(chat_stationId)",
            "statid":"\(chat_stationId)",
            "msgUUID":CRCommon.generateMsgID(),
            "code":CRUrl.shareInstance().CODE_BAND_WORDS,
            "userType":"\(userType)",
            "channelId":"ios",
            "userId":CRDefaults.getUserID(),
            "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code: CRUrl.shareInstance().CODE_BAND_WORDS, parameters: paramters)
    }
    
    //MARK: 拉取房间配置
    func sendMsgPullRoomConfig() {
        let code = CRUrl.shareInstance().CODE_ROOM_CONFIG
        let parameter = [
            "stationId":chat_stationId,
            "msgUUID":CRCommon.generateMsgID(),
            "code":code,
            "channelId":"ios",
            "userId":CRDefaults.getUserID(),
            "source":"app"]
        
        sendSysMessage(msgType: CR_USER_S, code:code , parameters: parameter)
    }
    
    //MARK: 切换房间
    func sendMsgChangeRoom(roomInfo:CRRoomInfoModel) {
        
        var p = [String:Any]()
        
        if lastRoomIndex == -1 {
            p["oldRoomId"] = ""
        }else {
            if let model = preRoomInfo {
                p["oldRoomId"] = model.roomId
            }
        }
        
        for (index,model) in roomList.enumerated() {
            if model.name == roomInfo.title {
                lastRoomIndex = index
                break
            }
        }
        
        if roomList.count == 0 {
            showToast(view: view, txt: "没有可以进入的房间")
            return
        }else if lastRoomIndex + 1 > roomList.count {
            lastRoomIndex = 0
        }
        
        let newRoomListModel = roomList[lastRoomIndex]
        
        p["source"] = "app"
        p["stationId"] = newRoomListModel.stationId
        p["newRoomId"] = roomInfo.roomId
        p["nickName"] = roomInfo.nickName
        p["account"] = roomInfo.account
        
        preRoomInfo = roomInfo
        
        sendSysMessage(msgType: CR_USER_JOIN_ROOM_S, code:"",parameters: p)
    }
    
    ///进入房间
    func sendMsgInsideChatRoom(model:CRRoomListModel,roomIndex:Int = 0) {
        
        var p = [String:Any]()
        p["code"] = CRUrl.shareInstance().JOIN_CHAT_ROOM
        p["source"] = "app"
        p["userId"] = CRDefaults.getUserID()
        p["roomId"] = model.id
        p["roomKey"] = model.roomKey
        p["nickName"] = YiboPreference.getUserName()
        p["account"] = YiboPreference.getUserName()
        
        if !agentUserCode.isEmpty{
            p["agentUserCode"] = agentUserCode
        }
        self.msgModel.roomId = model.id
        chat_stationId = model.stationId
        chat_roomId = model.id
        last_chat_roomId = chat_roomId
        //先取房间名
//        self.navigationBar.titleButton.setTitle(model.name, for: .normal)
        self.navigationBar.isHidden = false
        
        //MARK:该房间有密码
        if model.roomKey.count > 0 {
            //
            //获取
            let roomPassword =  CRDefaults.getRoomPassword()
            let userPasswordArray = roomPassword[chat_userId] ?? []
            
            if userPasswordArray.count != 0{
                var  password = ""
                for dict in userPasswordArray{
                    if dict[chat_roomId] != nil{
                        //有这个房间的密码
                        password = dict[chat_roomId] ?? ""
                        break
                    }
                }
                if password == model.roomKey{
                    //保存的密码
                    //密码相同 直接进入房间
                    //进入房间
                    sendSysMessage(msgType: CR_USER_S, code:CRUrl.shareInstance().JOIN_CHAT_ROOM,parameters: p)
                }else{
                    //密码错误 需要重新输入
                    //没有保存过密码
                    self.contentView.addRoomKeyView(password: model.roomKey)
                    self.contentView.roomKeyView.updateRoomkeyTips(roomName: model.name)
                    //输入房间密码
                    self.contentView.roomKeyView.confirmHandler = {[weak self] (input) in
                        guard let weakSelf = self else {return}
                        
                        if input != model.roomKey {
                            HUD.flash(.label("密码错误"), delay: 1)
                            weakSelf.isPersonBandSpeak = "1"
                            return
                        }else {
                            //密码输入正确  保存密码 下次进该房间不需输入密码
                            var roomPassword:[String:String] = [String:String]()
                            //保存在房间密码
                            roomPassword[weakSelf.chat_roomId] = input
                            //房间密码保存在用户UserId下
                            weakSelf.roomPasswords.append(roomPassword)
                            weakSelf.userPasswordKey[weakSelf.chat_userId]? = weakSelf.roomPasswords
                            
                            CRDefaults.setSaveRoomPassword(passwordDict: weakSelf.userPasswordKey)
                            
                            weakSelf.contentView.removeRoomKeyView()
                            //进入房间
                            weakSelf.sendSysMessage(msgType: CR_USER_S, code:CRUrl.shareInstance().JOIN_CHAT_ROOM,parameters: p)
                        }
                    }
                }
            }else{
                //没有保存过密码
                self.contentView.addRoomKeyView(password: model.roomKey)
                self.contentView.roomKeyView.updateRoomkeyTips(roomName: model.name)
                //输入房间密码
                self.contentView.roomKeyView.confirmHandler = {[weak self] (input) in
                    guard let weakSelf = self else {return}
                    
                    if input != model.roomKey {
                        HUD.flash(.label("密码错误"), delay: 1)
                        weakSelf.isPersonBandSpeak = "1"
                        return
                    }else {
                        //密码输入正确  保存密码 下次进该房间不需输入密码
                        //
                        var roomPassword:[String:String] = [String:String]()
                        //保存在房间密码
                        roomPassword[weakSelf.chat_roomId] = input
                        //房间密码保存在用户UserId下
                        weakSelf.roomPasswords.append(roomPassword)
                        weakSelf.userPasswordKey[weakSelf.chat_userId] = weakSelf.roomPasswords
                        
                        CRDefaults.setSaveRoomPassword(passwordDict: weakSelf.userPasswordKey)
                        weakSelf.contentView.removeRoomKeyView()
                        //进入房间
                        weakSelf.sendSysMessage(msgType: CR_USER_S, code:CRUrl.shareInstance().JOIN_CHAT_ROOM,parameters: p)
                    }
                }
            }
            self.contentView.roomKeyView.closeHandler = { [weak self] () in
                guard let weakSelf = self else {return}
                weakSelf.isPersonBandSpeak = "1"
                weakSelf.contentView.removeRoomKeyView()
            }
        }else {
           
            isPersonBandSpeak = model.isBanspeak == 1 ? "1" : "0"
            sendSysMessage(msgType: CR_USER_S, code: CRUrl.shareInstance().JOIN_CHAT_ROOM,parameters: p)
        }
    }
 

    //MARK: socket方式登录
    func sendMessageloginRoom(model:CRAuthenticateModel?) {

        guard let modelWraper = model else {
            showToast(view: self.view, txt: "授权失败")
            return
        }
        
        var p = [String:Any]()
        let token =  "clusterId=" + modelWraper.clusterId + "&encrypted=" + modelWraper.encrypted + "&sign=" + modelWraper.sign
        p["token"] = token
        p["code"] = CRUrl.shareInstance().LOGIN_AUTHORITY
        p["source"] = "app"
        
        sendSysMessage(msgType: CR_LOGIN_S, code:CRUrl.shareInstance().LOGIN_AUTHORITY,parameters: p)
    }
    
    //MARK: 获取授权参数成功
    func handleLogin(model:CRAuthMsgModel) {
        
        newAESKey = model.securityKey
        
        CRDefaults.setUserID(value: model.selfUserId)
        //用户的角色
        userAccountType = model.accountType
        //是否是代理的标记
        agentUserCode = model.agentRoom?.agentUserCode ?? ""
        //用户管理员角色
        userType = model.userType
        contentView.userType = userType
        chat_userId = model.selfUserId
        chat_stationId = model.stationId
        //拉取该房间配置
//            getChatRoomSystemProfile()
        //拉取开奖结果
        pullLotterReward()
//        sendMsgPullRoomConfig()
        sendMsgBandWords()
        
        //获取聊天室列表
//        netGetChatRoomList(model: m)
        
        //代理房处理
        //代理房如果在 R70000返回，就插入到 roomlist头部，并做去重处理
        authMsgModel = model
        
        sendMessageRoomList(model: model)
    }
    
    func pullLotterReward(isPrivateChat:Bool = false){
        if isPrivateChat {
            contentView.lotteryBanner.isHidden = true
            contentView.lotteryBanner.snp.updateConstraints { (make) in
                make.height.equalTo(0)
            }
        }else {
            //拉取彩种列表(根据开关控制是否要拉)
            guard let config = getChatRoomSystemConfigFromJson() else{return}
            //开奖结果已开启
            if  config.source?.switch_lottery_result_show == "1"{
                contentView.lotteryBanner.isHidden = false
                contentView.lotteryBanner.snp.updateConstraints { (make) in
                    make.height.equalTo(80)
                }
                
                //MARK：--拉取开奖结果
                sendMsgpullLotteryTicket()
            }else{
                //不用显示彩种的开奖结果
                contentView.lotteryBanner.isHidden = true
                contentView.lotteryBanner.snp.updateConstraints { (make) in
                    make.height.equalTo(0)
                }
            }
        }
    }
    
    //MARK: 获取聊天室列表
    func sendMessageRoomList(model:CRAuthMsgModel) {
        var p = [String:Any]()
        p["userId"] = model.selfUserId
        p["stationId"] = model.stationId
        p["code"] = CRUrl.shareInstance().CHAT_ROOM_LIST
        p["source"] = "app"
        
        if let agentRoom = model.agentRoom {
            p["agentUser"] = "1"
            if agentRoom.switchAgentEnterPublicRoom == "1" {
                p["switchAgentEnterPublicRoom"] = "1"
            }else {
                p["switchAgentEnterPublicRoom"] = "0"
            }
        }else {
            p["agentUser"] = "0"
            p["switchAgentEnterPublicRoom"] = "0"
        }
        
        sendSysMessage(msgType: CR_USER_S, code:CRUrl.shareInstance().CHAT_ROOM_LIST, parameters: p)
    }
    
    //MARK:获取 prizeType 2 开奖榜单, 4 盈利榜单
    func getLotterResults(prizeType:Int){
        let code = "\(sendMessageType.winPrizeList.rawValue)"
        var dict = [String:Any]()
        dict["prizeType"] = prizeType
        dict["stationId"] = chat_stationId
        dict["code"] = code
        dict["source"] = "app"
        dict["roomId"] = chat_roomId
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: dict)
    }
    /**导师计划*/
    func toturPlanData(){
        var paramters = [String:AnyObject]()
        paramters["stationId"] = chat_stationId as AnyObject
        paramters["code"] = sendMessageType.tutorPlan.rawValue as AnyObject
        paramters["source"] = "app" as AnyObject
        paramters["roomId"] = chat_roomId as AnyObject
        sendSysMessage(msgType: CR_USER_S, code:sendMessageType.tutorPlan.rawValue , parameters: paramters)
    }
  
    //MARK: 进入房间消息
    func receiveInsideRoom(message:CRMessage) {
        if let welcomeItem = CRWelcomeMsgItem.deserialize(from: message.msgStr) {
            if welcomList.count < 3 {
                self.handleWelcomeMessage(welcomeItem: welcomeItem)
            }else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.receiveInsideRoom(message:message)
                }
            }
        }
    }
    
    func handleWelcomeMessage(welcomeItem:CRWelcomeMsgItem) {
        
        welcomeItem.msgUUID = CRCommon.generateMsgID()
        welcomeLock.lock()
        welcomList.append(welcomeItem)
        welcomeLock.unlock()
        
        self.contentView.welcomeNewClient(message: welcomeItem, removeMsgHandler: {[weak self] (msg) in
            guard let weakSelf = self else {return}
            weakSelf.welcomeLock.lock()
            var msgIndex:Int?
            for (index,model) in weakSelf.welcomList.enumerated() {
                if model.msgUUID == msg.msgUUID {
                    msgIndex = index
                }
            }
            
            if let index = msgIndex {
                weakSelf.welcomList.remove(at: index)
            }
            
            weakSelf.welcomeLock.unlock()
        })
    }
    
    //MARK: 收到撤回消息的处理
    func handleRetractWith(message:CRRetractMsgItem) {
        guard let model = message.rectractModel else {return}
        if model.roomId == chat_roomId { //同一房间
            deleteMsgsWith(msgIds: [model.msgId])
        }
    }
    
    //MARK: 收到开奖结果消息数组
    func receiveWinnersInfo(message:CRMessage) {
        winnerTimer?.invalidate()
        //关闭了显示，则取消显示
        if contentView.hasCloseWinnerInfo {
            return
        }
        
        if let wraper = CRWinnersInfoWrapper.deserialize(from: message.msgStr),let source = wraper.source, let winners = source.winningList {
            winnersInfo = winners
        }else {
            return
        }
        
        var array = [CRWinnersInfo]()
        for _ in 0..<10 {
            let randomIndex =  Int(arc4random_uniform(UInt32(winnersInfo.count)))
            array.append(winnersInfo[randomIndex])
        }
        
        winnersInfo.removeAll()
        winnersInfo = array
        
        let inOutAnimateTime:TimeInterval = 0.8 * 2 //进出场动画时间
        var marginsTime:TimeInterval = 2 //中奖榜单停留时间
        if let config = getChatRoomSystemConfigFromJson()?.source {
            //中奖榜单
            if let time = Double(config.name_win_lottery_animation_interval) {
                marginsTime = TimeInterval(time)
            }
        }
        
        currentWinnerIndex = 0
        
        winnerTimer = Timer.scheduledTimer(timeInterval: (inOutAnimateTime + marginsTime), target: self, selector: #selector(winnersTimeAtion), userInfo: nil, repeats: true)
        
        if let timer = winnerTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    @objc func winnersTimeAtion() {
        if contentView.hasCloseWinnerInfo { //关闭了显示，则取消显示
            winnerTimer?.invalidate()
            return
        }
        
        let msg = winnersInfo[currentWinnerIndex]
        currentWinnerIndex += 1
        contentView.winnersInfoViewConfig(model: msg)
        
        var images = [String]()
        for _ in 0..<6 {
            let imageIndex = arc4random() % 25 + 1
            let imageName = "spark\(imageIndex)"
            images.append(imageName)
        }
        
        //        CRSpark.setupSpark(self.view, images: images)
        
        if currentWinnerIndex + 1 == winnersInfo.count {
            winnerTimer?.invalidate()
        }
    }
    
    ///刷新cell页面数据显示
    public func updateMessage(message:CRMessage){
        let visibleCells = contentView.messageArea.msgTable.visibleCells
        for cell  in visibleCells {
            if cell is CRBaseChatCell{
                let cellView = cell as? CRTextMessageCell
                if  cellView?.message?.messageId == message.messageId{
                    cellView?.updateMessage(message: message)
                    return
                }
            }
        }
    }
    
    //MARK: - 发送消息代理
    ///发送文本消息
    func chatBar(text: String) {
        if CRDefaults.getChatroomSendText() == false {
            showToast(view: self.view, txt: "该房间您没有发送文字权限 请联系客服")
            return
        }
        
        //判断用户发送是否包含敏感词汇
        var isCanSend = true
        let config = getChatRoomSystemConfigFromJson()
        //前端管理员和后台发禁词开关打开
        if userType == 4 && config?.source?.switch_front_admin_ban_send == "1"{
            isCanSend = true
        }else{
            for languageItem in prohibitLanguageArray{
                if languageItem.enable == "1"{
                    //该敏感词汇开启
                    let languageString = text.lowercased()
                    if  languageString.contains(languageItem.word){
                        showToast(view: view, txt:"您所发的消息内容包含敏感词汇")
                        isCanSend = false
                        break
                    }
                }
            }
        }
        
        if isCanSend {
            sendTxtMsgRequest(text: text)
        }
    }
    
    //MARK: - 工具方法    
    //从json字符串格式化 [CRMessageModel]
    func formatMsgModelsWithStrings(datas:[Any]) -> [CRMessage]? {
        if let stringArray = datas as? [String] {
            var models = [CRMessage]()
//            let isFirstLoadMessages = contentView.messageArea.msgItems.count == 0
            for (_,jsonString) in stringArray.enumerated(){
                let deJsonString = Encryption.Decode_AES_ECB(key: aesKey, strToDecode: jsonString)
                
                guard let deJsonUnWrap = deJsonString,let wraper = CRMessageWraper.deserialize(from: deJsonUnWrap) else {
                    return nil
                }
                
                if let model = CRMessage.deserialize(from: wraper.nativeMsg) {
                    //用户ID
                    let userId = CRDefaults.getUserID()
                    
                    if model.userId == userId && model.msgType == "16" { //踢出
                        kickOutRoom()
                    }else {
                        models.append(model)
                    }
                }
            }
            
            return models
        }
        return nil
    }
    
    //MARK: - 消息的数据回调处理 && 数据统一处理
    func getNickNameWith(msg:CRMessage) -> String {
        var name = ""
        if !msg.nickName.isEmpty {
            name = msg.nickName
        }else if !msg.nativeAccount.isEmpty {
            name = msg.nativeAccount
        }else if !msg.account.isEmpty {
            name = msg.account
        }
        
        return name
    }

    //MARK: 接收socket 推过来的消息 入口
    public func didReceivedMessage(message:CRMessage) {
        if recordMsgUUIDContentItem.msgUUID == message.msgId && !message.msgId.isEmpty {
            return
        }
        
        recordMsgUUIDContentItem.msgUUID = message.msgId
        
        if message.sentTime.isEmpty {
            let currentTime = String().getCurrentTimeString()
            
            message.sentTime = currentTime.subString(start: 10)
        }
        
        let chatRoomConfig = getChatRoomSystemConfigFromJson()
        
        if message.msgType == "\(CRMessageType.Band_speakALL_OrNot.rawValue)" { //房间禁言，解除禁言
            if !message.msgStr.isEmpty {
                guard let model = CRProhibitModel.deserialize(from: message.msgStr) else {return}
                //禁言
                handleProhibitAll(model: model)
            }
        }else if message.msgType == "\(CRMessageType.Native_Band_speak_OrNot.rawValue)" { //禁言 或取消禁言
            if !message.msgStr.isEmpty {
                guard let model = CRProhibitModel.deserialize(from: message.msgStr) else {return}
                let prohibitMsg = CRPorhibitMsgItem()
                prohibitMsg.prohibitModel = model
                //禁言
                handleProhibit(message: prohibitMsg)
            }
        }else if message.msgType == "\(CRMessageType.Band_speak_OrNot.rawValue)" {
            if message.userId == CRDefaults.getUserID() {
                
                //用户ID 是自己就处理
                //后台对用户禁言
                let speakingItem =  CRSpeakingCloseItem.deserialize(from: message.msgStr)
                if speakingItem?.speakingClose == "0"{
                    //针对在线用户的禁言解禁
                    //1.解禁言的时候要判断之前房间的状态
                    isPersonBandSpeak = "0"
                }else if speakingItem?.speakingClose == "1"{
                    //对在线用户禁言
                    isPersonBandSpeak = "1"
                }
            }
            
        }else if message.msgType == "\(CRMessageType.retractMsg.rawValue)" { //撤回消息
            if !message.msgStr.isEmpty {
                guard let model = CRRetractModel.deserialize(from: message.msgStr) else {return}
                let retractMsg = CRRetractMsgItem()
                retractMsg.rectractModel = model
                handleRetractWith(message:retractMsg) //收到撤回消息的处理
            }
            
        }else if message.msgType == "\(CRMessageType.WinnerInfo.rawValue)" {
            //中奖公告
            guard let config = chatRoomConfig?.source else {
                if !isPrivateChat {
                    self.receiveWinnersInfo(message: message)
                }
                
                return
            }
            /** 中奖榜单轮播开关 */
            if config.switch_winning_banner_show == "1" {
                if !isPrivateChat {
                    self.receiveWinnersInfo(message: message)
                }
            }
            
        }else if message.msgType == "\(CRMessageType.InsideRoom.rawValue)" {
            //进入房间通知
            if CRDefaults.getChatroomEnterRoomNoti() && !isPrivateChat {
                self.receiveInsideRoom(message: message)
            }
        }else if message.msgType == "\(CRMessageType.appealBand.rawValue)" { //禁言申述
            if !message.msgStr.isEmpty {
                guard let model = CRAppealBandModel.deserialize(from: message.msgStr) else {return}
                if model.state == "0" {
                    if prohibit {
                        showToast(view: self.view, txt: "您已被解除禁言")
                        isPersonBandSpeak = "0"
                    }
                }else {
                    showToast(view: self.view, txt: (model.msg.isEmpty ? "您的禁言申述没有通过" : model.msg))
                }
            }
        }else{
            var localMessage:CRMessage?
            
            //非登录状态不接收聊天室通知
            if !YiboPreference.getLoginStatus(){
                return
            }
            
            if let msgResult = message.msgResult,let userEntity = msgResult.userEntity {
                message.nickName = userEntity.nickName
                message.account = userEntity.account
                message.nativeAccount = userEntity.nativeAccount
                message.privatePermission = userEntity.privatePermission
            }
            
            let localNotification = UILocalNotification()
            localNotification.alertBody = "您有一条通知，请注意查看"
            if CRDefaults.getChatroomRecerveMessageVoicei(){
                localNotification.soundName = "notify_msg.caf"
            }else{
                localNotification.soundName = ""
            }
            
            localNotification.hasAction = true
            localNotification.alertAction = "解锁查看"
            //发送消息通知
            
            var infoDic:[String:Any] = [:]
            infoDic["code"] = KMESSAGENOTI
            
            //        if isPrivateChat {
            //            infoDic["roomId"] = chat_privateRoomId
            //        }else {
            infoDic["roomId"] = chat_roomId
            //        }
            
            infoDic["userId"] = CRDefaults.getUserID()
            infoDic["stationId"] = message.stationId
            //用户图像
            infoDic["headIcon"] = self.userIconUrl
            //房间名
            infoDic["roomName"] = ""
            //房间图标
            infoDic["roomIcon"] = ""
            
            let className = NSStringFromClass(self.classForCoder)
            infoDic["ctrl"] = className
            
            //区分判断 私聊和群聊
            let decodeString = message.msgStr
            var privateMsgOption:CRPrivateMessage?
            
            
            var isPrivateMsg = false
            if message.msgType == "\(CRMessageType.PrivateText.rawValue)" {
                isPrivateMsg = true
                //在这里格式化私聊 msgType 和 群聊 msgType相同
                CRChatRoomLogic.resetPrivateMsgType(msgStr: decodeString,msg: message) //收到的消息是群聊还是私聊,false 群聊，true 私聊
            }
            
            //            infoDic["isPrivateChat"] = isPrivateChat //是否是群聊页面
            infoDic["isPrivateChat"] = isPrivateMsg //是否是群聊消息
            
            if isPrivateMsg { //收到的是私聊消息
                if let msg = CRPrivateMessage.deserialize(from: decodeString) {
                    privateMsgOption = msg
                    message.roomId = msg.roomId
                    
                    if let fromUser = msg.fromUser {
                        message.userId = fromUser.id
                    }
                    
                    if !isCurrentChat(message: message) {
                        //刷新未读消息数(私聊) :会显示在页面上的消息,
                        if ["\(CRMessageType.Text.rawValue)","\(CRMessageType.Image.rawValue)","\(CRMessageType.RedPacket.rawValue)","\(CRMessageType.ShareBet.rawValue)","\(CRMessageType.ShareGainsLosses.rawValue)"].contains(message.msgType) {
                            
                            drawer?.subviewsBGView.updateUnreadNum(msg: message)
                        }
                    }
                    
                    if let user = passiveUser {
                        infoDic["passiveUserId"] = user.id
                        infoDic["userType"] = user.userType
                        infoDic["nickName"] = user.nickName
                        infoDic["account"] = user.account
                    }
                }
            }
            
            //文本消息
            if ["\(CRMessageType.Text.rawValue)"].contains(message.msgType) {
                let textMessageItem = CRTextMessageItem()
                textMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                textMessageItem.ownerType = .Others
                textMessageItem.messageType = .Text
                
                textMessageItem.msgResult?.userEntity = (message.msgResult?.userEntity ?? nil)
                textMessageItem.msgResult?.userEntity?.userDetailEntity = (message.msgResult?.userEntity?.userDetailEntity ?? nil)
                
                textMessageItem.nickName = message.nickName
                textMessageItem.nativeAccount = message.nativeAccount
                
                if isPrivateMsg {
                    if isPrivateChat {
                        if let privateMsg = privateMsgOption {
                            CRChatRoomLogic.formatWithPrivateMessage(privateMsg: privateMsg, realMsg: textMessageItem)
                        }
                    }
                }else {
                    if let messageEntityItem = CRMessageEntity.deserialize(from: decodeString) {
                        textMessageItem.text = messageEntityItem.record
                        textMessageItem.msgUUID = messageEntityItem.msgUUID
                        textMessageItem.msgId = messageEntityItem.msgId
                        textMessageItem.source = messageEntityItem.source
                    }
                    
                    textMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                    textMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                    textMessageItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
                    textMessageItem.userId = message.userId
                    textMessageItem.account = message.account
                    textMessageItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                }
                
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " +  (textMessageItem.text ?? "")
                //                    localNotification.alertLaunchImage
                
                textMessageItem.sentTime  = message.sentTime
                
                localMessage = textMessageItem
                
            }else if message.msgType == "\(CRMessageType.Image.rawValue)"{
                //图片消息
                //调用读图片的接口
                let imageMessageItem = CRImageMessage()
                imageMessageItem.userId = message.userId
                imageMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                imageMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                imageMessageItem.ownerType = .Others
                imageMessageItem.messageType = .Image
                imageMessageItem.nickName = message.nickName
                imageMessageItem.account = message.account
                imageMessageItem.nativeAccount = message.nativeAccount
                imageMessageItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
                imageMessageItem.sentTime  = message.sentTime
                imageMessageItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                imageMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                
                if isPrivateMsg {
                    if isPrivateChat {
                        if let privateMsg = privateMsgOption {
                            CRChatRoomLogic.formatWithPrivateMessage(privateMsg: privateMsg, realMsg: imageMessageItem)
                        }
                    }
                }else {
                    guard let imageResource = CRImageResource.deserialize(from: decodeString) else {
                        return
                    }
                    
                    let imageUrlCode = imageResource.fileString.isEmpty ? imageResource.record : imageResource.fileString
                    //                    imageMessageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imageUrlCode
                    
                    imageMessageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imageUrlCode
                    
                    imageMessageItem.msgUUID = imageResource.msgUUID
                    imageMessageItem.msgId = imageResource.msgId
                }
                
                //把图片添加到图片浏览器中
                imageUrls.append(imageMessageItem.imageUrl ?? "")
                
                let imageV = UIImageView()
                imageV.kf.setImage(with: URL(string: imageMessageItem.imageUrl ?? ""), placeholder: nil, options: nil, progressBlock: { (curProgress, totalProgress) in
                    
                }) { (image, error, cache, url) in
                    //
                    imageMessageItem.imageSize = (image?.size) ?? CGSize.zero
                    print("imageSize = \(imageMessageItem.imageSize)")
                    imageMessageItem.kMessageFrame = nil
                    imageMessageItem.sendState = .Success
                }
                
                localMessage = imageMessageItem
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " + "图片消息"
            }else if message.msgType == "\(CRMessageType.RedPacket.rawValue)" {
                //接收消息类型是红包
                let redPacketItem = CRRedPacketMessage()
                redPacketItem.userId = message.userId
                redPacketItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                redPacketItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                redPacketItem.ownerType = .Others
                redPacketItem.messageType = .RedPacket
                
                redPacketItem.nickName = message.nickName
                redPacketItem.account = message.account
                redPacketItem.nativeAccount = message.nativeAccount
                redPacketItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
                
                let decodeString = message.msgStr
                if let redPacketContentItem = CRRedPacketContentItem.deserialize(from: decodeString) {
                    redPacketItem.redPacketContentItem = redPacketContentItem
                    redPacketItem.msgUUID = redPacketContentItem.msgUUID
                }
                redPacketItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                redPacketItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                redPacketItem.sentTime = message.sentTime
                
                if isPrivateMsg {
                    if let privateMsg = privateMsgOption {
                        CRChatRoomLogic.formatWithPrivateMessage(privateMsg: privateMsg, realMsg: redPacketItem)
                        message.userId = redPacketItem.userId
                    }
                }
                
                localMessage = redPacketItem
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " + "红包消息"
            }else if message.msgType == "\(CRMessageType.ShareBet.rawValue)" {
                let shareBetMessageItem = CRShareBetMessageItem()
                shareBetMessageItem.userId = message.userId
                shareBetMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                shareBetMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                shareBetMessageItem.ownerType = .Others
                shareBetMessageItem.messageType = .ShareBet
                shareBetMessageItem.nickName = message.nickName
                shareBetMessageItem.account = message.account
                shareBetMessageItem.nativeAccount = message.nativeAccount
                shareBetMessageItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
                
                guard let betModel = CRShareBetModel.deserialize(from: decodeString), let betInfos = betModel.betInfos else {
                    return
                }
                
                var height:CGFloat = 0
                if betInfos.count == 1 {
                    height = CRShareBetAllView.singleH
                }else if betInfos.count >= 2 {
                    height = CGFloat(2) *  CRShareBetAllView.singleH + CRShareBetAllView.followAllH
                }
                
                let messageFrame  = CRMessageFrame()
                messageFrame.contentSize = CGSize.init(width: 300, height: height)
                messageFrame.height = Float(height) + Float(50)
                shareBetMessageItem.messageFrame = messageFrame
                shareBetMessageItem.shareBetModel = betModel
                shareBetMessageItem.winRate = betModel.winOrLost?.winPer ?? 0
                
                shareBetMessageItem.msgUUID = betModel.msgUUID
                shareBetMessageItem.msgId = betModel.msgId
                shareBetMessageItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                shareBetMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                
                if isPrivateMsg {
                    if isPrivateChat {
                        if let privateMsg = privateMsgOption {
                            CRChatRoomLogic.formatWithPrivateMessage(privateMsg: privateMsg, realMsg: shareBetMessageItem)
                        }
                    }
                }
                
                localMessage = shareBetMessageItem
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " +  "注单消息"
            }else if message.msgType == "\(CRMessageType.PlanMsg.rawValue)" {
                let planMsgItem = CRPlanMsgItem()
                planMsgItem.userId = message.userId
                planMsgItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                planMsgItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                planMsgItem.ownerType = .Others
                planMsgItem.messageType = .PlanMsg
                planMsgItem.msgResult?.userEntity?.nickName = "计划消息"
                planMsgItem.planItem = CRPlanMessage.deserialize(from: message.msgStr)
                planMsgItem.sentTime = message.time
                
                planMsgItem.avatar = planMsgItem.planItem?.avatar ?? ""
                planMsgItem.nickName = planMsgItem.planItem?.nickName ?? ""
                
                planMsgItem.msgUUID = planMsgItem.planItem?.msgUUID ?? ""
                planMsgItem.msgId = planMsgItem.planItem?.msgId ?? ""
                
                localMessage = planMsgItem
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " +  "计划消息"
            }else if ["\(CRMessageType.ShareGainsLosses.rawValue)"].contains(message.msgType) { //"4" 私聊时的 分享输赢
                //今日分享
                
                guard let msgStrItem =  CRMessage.deserialize(from: message.msgStr) else{
                    return
                }
                guard let shareGainsLossesItem =  CRShareGainsLossesItem.deserialize(from: msgStrItem.record) else{return}
                shareGainsLossesItem.userId = message.userId
                shareGainsLossesItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                shareGainsLossesItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                shareGainsLossesItem.ownerType = .Others
                shareGainsLossesItem.messageType = .ShareGainsLosses
                shareGainsLossesItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
                shareGainsLossesItem.sentTime = message.sentTime
                shareGainsLossesItem.msgUUID = msgStrItem.msgUUID
                shareGainsLossesItem.msgId = msgStrItem.msgId
                shareGainsLossesItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                shareGainsLossesItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                shareGainsLossesItem.nickName = message.nickName
                shareGainsLossesItem.account = message.account
                shareGainsLossesItem.nativeAccount = message.nativeAccount
                
                if isPrivateMsg {
                    if isPrivateChat {
                        if let privateMsg = privateMsgOption {
                            CRChatRoomLogic.formatWithPrivateMessage(privateMsg: privateMsg, realMsg: shareGainsLossesItem)
                        }
                    }
                }
                
                localMessage = shareGainsLossesItem
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " +  "今日盈亏消息"
            }else if message.msgType == "\(CRMessageType.robotMsg.rawValue)" {
                let robotMsgItem = CRRobotMsgItem()
                robotMsgItem.userId = message.userId
                robotMsgItem.robotMessage = CRRobotMessage.deserialize(from: message.msgStr)
                
                let contents = CRChatRoomLogic.deleteHtmlTagsWhenPTag(contents: robotMsgItem.robotMessage?.record ?? "")
                robotMsgItem.robotMessage?.record = contents
                var showMsg = "富文本消息"
                
                if contents.contains("<p>") || contents.contains("<img") || contents.contains("<p") || contents.contains("</p") {
                    robotMsgItem.msgUUID = message.msgUUID
                    robotMsgItem.msgId = message.msgId
                    robotMsgItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                    robotMsgItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                    robotMsgItem.ownerType = .Others
                    robotMsgItem.messageType = .robotMsg
                    
                    robotMsgItem.nickName = message.nickName
                    robotMsgItem.account = message.account
                    robotMsgItem.nativeAccount = message.nativeAccount
                    
                    robotMsgItem.sentTime = message.sentTime
                    robotMsgItem.avatar = robotMsgItem.robotMessage?.fromUser?.robotImage
                    
                    robotMsgItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                    robotMsgItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                    
                    localMessage = robotMsgItem
                }else {
                    let textMessageItem = CRTextMessageItem()
                    textMessageItem.msgType = "\(CRMessageType.Text.rawValue)"
                    textMessageItem.msgUUID = message.msgUUID
                    textMessageItem.msgId = message.msgId
                    textMessageItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                    textMessageItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                    textMessageItem.ownerType = .Others
                    textMessageItem.messageType = .Text
                    
                    //用户图像
                    textMessageItem.avatar = robotMsgItem.robotMessage?.fromUser?.robotImage
                    textMessageItem.levelIcon =  message.msgResult?.userEntity?.levelIcon ?? ""
                    textMessageItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                    
                    textMessageItem.nickName = message.nickName
                    textMessageItem.account = message.account
                    textMessageItem.nativeAccount = message.nativeAccount
                    textMessageItem.text = robotMsgItem.robotMessage?.record
                    showMsg = robotMsgItem.robotMessage?.record ?? ""
                    textMessageItem.sentTime  = message.sentTime
                    localMessage = textMessageItem
                }
                
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " +  showMsg
            }else if message.msgType == "\(CRMessageType.Voice.rawValue)" {
                //音频消息
                let decodeString = message.msgStr
                let voiceMsgItem = CRVoiceMessageItem()
                voiceMsgItem.userId = message.userId
                voiceMsgItem.isPlanUser = message.msgResult?.userEntity?.planUser ?? "0"
                voiceMsgItem.userType = message.msgResult?.userEntity?.userType ?? "1"
                guard let msgItem = CRAudioMsgStrItem.deserialize(from: decodeString) else{return}
                guard let audioMsgItem = msgItem.audioMsg else{return}
                voiceMsgItem.durationTime = String.init(format:"%ld",audioMsgItem.duration)
                voiceMsgItem.ownerType = .Others
                voiceMsgItem.messageType = .Voice
                voiceMsgItem.sentTime = message.time
                //                    voiceMsgItem.path = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE + "?fileId=" + "\(audioMsgItem.record)" + "&contentType=audio/amr"
                //下载该音频文件
                downloadFileFromServer(fileCode: audioMsgItem.record, voiceMsgItem: voiceMsgItem)
                
                //音频消息详情
                voiceMsgItem.nickName = message.nickName
                voiceMsgItem.account = message.account
                voiceMsgItem.nativeAccount = message.nativeAccount
                
                voiceMsgItem.sentTime = message.time
                voiceMsgItem.avatar = message.msgResult?.userEntity?.userDetailEntity?.avatarCode
                voiceMsgItem.msgUUID = msgItem.msgUUID
                voiceMsgItem.msgId = msgItem.msgId
                voiceMsgItem.levelIcon = message.msgResult?.userEntity?.levelIcon ?? ""
                voiceMsgItem.levelName = message.msgResult?.userEntity?.levelName ?? ""
                
                localMessage = voiceMsgItem
                let showName = getNickNameWith(msg: message)
                localNotification.alertBody = showName + ": " +  "语音消息"
            }else if message.msgType == "\(CRMessageType.appealBand.rawValue)" {
                print("")
            }
            
            let isUserSelf = message.userId == CRDefaults.getUserID() //是否是当前用户
            
            if !isUserSelf {
                if let msg = localMessage {
                    if isCurrentChat(message: message) {
                        msg.privatePermission = message.privatePermission
                        addToShowMessage(message: msg)
                        
                        if isPrivateChat && isPrivateMsg { //是私聊消息，并且在私聊页面 则设置消息为已读
                            sendMsgRequestPrivateChat(type: "3", model: passiveUser)
                        }
                    }
                }
            }else { // 是用户自己发的消息则需要赋值 msgId到本地消息
                let msgItems = contentView.messageArea.msgItems
                let msg = CRChatRoomLogic.getMsgFromMessageItemsWith(msgUUID: localMessage?.msgUUID ?? "", msgItems: msgItems)
                msg?.msgId = localMessage?.msgId ?? ""
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                    self.updateMsgOnCell(msgUUID: msg?.msgUUID ?? "", sendStatus: .Success)
                }
            }
            
            //收到的消息所属房间，是当前房间，推送通知
            if  CRDefaults.getChatroomMessageNoti() && !isUserSelf && isCurrentChat(message: message) {
                let roomName = isPrivateChat ? getNickNameWith(msg: message) : self.roomName
                localNotification.alertTitle = "房间: " + roomName
                
                localNotification.userInfo = infoDic
                
                UIApplication.shared.presentLocalNotificationNow(localNotification)
            }
        }
    }
    
    //是否是当前会话
    func isCurrentChat(message:CRMessage?) -> Bool {
        guard let message = message else {return false}
        var roomId = ""
        
        if isPrivateChat {
            roomId = chat_privateRoomId
        }else {
            roomId = chat_roomId
        }
        
        return message.roomId == roomId  || (message.roomId == (roomId + "_" + chat_stationId))
    }
    
    //MARK: 添加消息到数据源 并显示
    func addToShowMessage(message:CRMessage){
        message.sendState = .progress
        contentView.messageArea.addMessage(message: message)
        DispatchQueue.main.async{
            if message.ownerType == .TypeSelf {
                self.scrollTableViewToBottom(animated: true)
            }
        }
    }
    
    //MARK: socket发送消息
    func sendSysMessage(msgType:String,code:String = "",parameters:[String:Any]) {
        let parAndFuncs = CRChatRoomLogic.getMsgFuncDes(code: code, msgType: msgType,parameters:parameters) //对消息功能的描述，和参数的处理
        let funcs = parAndFuncs.0
        var parameters = parAndFuncs.1
        
        if isSocketConnect {
            
            if !funcs.isEmpty {
                HUDView.label.text = funcs
            }
            
            let jsonString = jsonStringFrom(dictionary: parameters)
            guard let msgJson = jsonString else {return}
            
            var encryKey = newAESKey
            if [CR_LOGIN_S,CR_USER_JOIN_ROOM_S].contains(msgType) {
                encryKey = aesKey
            }
            
            if encryKey.isEmpty {
                return
            }
            
            var encryMsg = ""
            
            if [CR_USER_JOIN_GROUP_S].contains(msgType) {
                encryMsg = Encryption.Endcode_AES_ECB(key: aesIV, iv: aesKey, strToEncode: msgJson)
            }else {
                encryMsg = Encryption.Endcode_AES_ECB(key: encryKey, strToEncode: msgJson)
            }
            
            let timeOut:Double = 15 //超时
            
            socket?.emitWithAck(msgType, encryMsg).timingOut(after: timeOut, callback: {[weak self] (data) in
                guard let weakSelf = self else {return}
                
                if code == "\(sendMessageType.image.rawValue)"{
                    //音频也是这个方法
                    guard let imgMessageItem = CRImageResource.deserialize(from: jsonString) else{return}
                    if imgMessageItem.record.isEmpty{return}
                    
//                    let imageUrl =  CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imgMessageItem.record
                    
                    let imageUrl =  CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imgMessageItem.record
                    
                    weakSelf.imageUrls.append(imageUrl)
                }
                
                if let datas = data as? [String] {
                    if datas.count > 0 {
                        if datas[0] == "NO ACK" {
                            if !funcs.isEmpty {
                                // 失败状态
                                let msgUUId = parameters["msgUUID"] as? String
                                
//                                let msgItems = weakSelf.contentView.messageArea.msgItems
//                                let  originalMsg = CRChatRoomLogic.getMsgFromMessageItemsWith(msgUUID: msgUUId,msgItems:msgItems)
//                                originalMsg?.sendState = .Fail
                                weakSelf.updateMsgOnCell(msgUUID: msgUUId ?? "", sendStatus: .Fail)
                                
                                weakSelf.HUDView.hide(animated: true)
                                
                                if !(code == CRUrl.shareInstance().LOGIN_AUTHORITY && isSocketConnect) {
                                    showToast(view: weakSelf.view, txt: "\(funcs)超时")
                                }else if code == CRUrl.shareInstance().LOGIN_AUTHORITY && isSocketConnect {
                                    //重连聊天室
                                    weakSelf.sendSysMessage(msgType: CR_LOGIN_S, code:CRUrl.shareInstance().LOGIN_AUTHORITY,parameters: parameters)
                                }
                            }
                        }
                    }
                }
            })
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                socketManager?.reconnect()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    self.sendSysMessage(msgType: msgType, parameters: parameters)
                }
            }
        }
    }
    
    //MARK: 处理历史记录返回的数据
    private func handleHistoryRecord(historys:[CRMessage],isPrivateChat:Bool) {
        self.isPrivateChat = isPrivateChat
        contentView.roomsBottomFucView.isPrivateChat = isPrivateChat
        contentView.messageArea.isPrivateChat = isPrivateChat
        contentView.inputAreaView.layoutVoicceButton(show: !isPrivateChat)
        contentView.roomsBottomFucView.updateItemModels(isPricateChat: isPrivateChat)
        contentView.hasCloseWinnerInfo = isPrivateChat
        setupDrawer(andShow: false)
        
        //        contentView.roomsBottomFucView.configWith(width: CRConstants.screenWidth)
        
        if isPrivateChat {
            pullLotterReward(isPrivateChat: true) //隐藏开奖结果
            sendMsgSystemNoticeRequest() //获取系统公告
        }
        
        if historyStart == 0 { //切换房间的时候，即使拉取聊天记录失败，也要清除上次房间的数据
            //清空之前的数据源
            contentView.messageArea.msgItems.removeAll()
            imageUrls.removeAll()
        }
        
        var firstDateString = ""
        var localMsgs: [CRMessage] = [] //历史记录
        
        for (index ,messageItem) in historys.enumerated() {
            //因私聊历史记录结构与 群聊的不同，所以这里需要处理为统一的数据
            if isPrivateChat {
                if let fromUser = messageItem.fromUser {
                    messageItem.levelName = fromUser.levelName
                    messageItem.userType = fromUser.userType
                    messageItem.avatar = fromUser.avatar
                    messageItem.userId = fromUser.id
                    messageItem.account = fromUser.account
                    messageItem.levelIcon = fromUser.levelIcon
                }
                
                if messageItem.msgType == "6" {
                    messageItem.msgType = "\(CRMessageType.RedPacket.rawValue)"
                }
            }
            
            if index == 0{
                //记录第一条消息的日期
                firstDateString = messageItem.time
                messageItem.isShowTime = true
            }else{
                if Date.isSameDay(firstDateString: firstDateString, secondDateString: messageItem.time){
                    messageItem.isShowTime = false
                }else{
                    firstDateString = messageItem.time
                    messageItem.isShowTime = true
                }
                
            }
            
            if messageItem.sender == CRDefaults.getUserID(){
                messageItem.ownerType = .TypeSelf
            }else{
                messageItem.ownerType = .Others
            }
            
            //去除年的显示
            //            if messageItem.time.length > 5{
            //                messageItem.time = messageItem.time.subString(start: 10)
            //            }
            
            if messageItem.msgType == "\(CRMessageType.Text.rawValue)"{
                //文本
                let msgTextItem = CRTextMessageItem()
                msgTextItem.msgUUID = messageItem.msgUUID
                msgTextItem.msgId = messageItem.msgId
                msgTextItem.privatePermission = messageItem.privatePermission
                msgTextItem.stopTalkType = messageItem.stopTalkType
                msgTextItem.isPlanUser = messageItem.isPlanUser
                msgTextItem.userType = messageItem.userType
                msgTextItem.text = messageItem.context
                msgTextItem.userId = messageItem.sender
                //                msgTextItem.time = messageItem
                msgTextItem.messageType = .Text
                msgTextItem.isShowTime = messageItem.isShowTime
                
                if isPrivateChat {
                    msgTextItem.text = messageItem.record
                    msgTextItem.userId = messageItem.userId
                    msgTextItem.msgUUID = messageItem.userMessageId
                    msgTextItem.msgId = messageItem.userMessageId
                }
                
                msgTextItem.ownerType = msgTextItem.userId == CRDefaults.getUserID() ? .TypeSelf:.Others
                
                msgTextItem.avatar = messageItem.avatar
                if messageItem.time.length > 5{
                    msgTextItem.sentTime = messageItem.time.subString(start: 10)
                }
                msgTextItem.levelIcon = messageItem.levelIcon
                msgTextItem.levelName = messageItem.levelName
                msgTextItem.time = messageItem.time
                msgTextItem.nickName = messageItem.nickName
                msgTextItem.account = messageItem.account
                msgTextItem.nativeAccount = messageItem.nativeAccount
                
                localMsgs.append(msgTextItem)
                
            }else if messageItem.msgType == "\(CRMessageType.Image.rawValue)" {
                //图片
                let msgImgItem = CRImageMessage()
                msgImgItem.msgUUID = messageItem.msgId
                msgImgItem.msgId = messageItem.msgId
                msgImgItem.privatePermission = messageItem.privatePermission
                msgImgItem.userId = messageItem.sender
                msgImgItem.stopTalkType = messageItem.stopTalkType
                msgImgItem.isPlanUser = messageItem.isPlanUser
                msgImgItem.userType = messageItem.userType
                msgImgItem.messageType = .Image
                msgImgItem.isShowTime = messageItem.isShowTime
                msgImgItem.time = messageItem.time
                
                if isPrivateChat {
                    msgImgItem.userId = messageItem.userId
                    msgImgItem.msgUUID = messageItem.userMessageId
                    msgImgItem.msgId = messageItem.userMessageId
                }
                
                msgImgItem.ownerType = msgImgItem.userId == CRDefaults.getUserID() ? .TypeSelf:.Others
                //发送者和接收者区分
                
                var imageCode = ""
                if isPrivateChat {
                    imageCode = messageItem.record
                }else {
                    imageCode = messageItem.url ?? ""
                }
                
                if imageCode.contains("/chatFile/read_file/") {
                    imageCode = imageCode.replacingOccurrences(of: "/chatFile/read_file/", with: "")
                }
                
                //                msgImgItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + imageCode
                
                msgImgItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + imageCode
                
                msgImgItem.avatar = messageItem.avatar
                msgImgItem.levelIcon = messageItem.levelIcon
                msgImgItem.levelName = messageItem.levelName
                
                msgImgItem.nickName = messageItem.nickName
                msgImgItem.account = messageItem.account
                msgImgItem.nativeAccount = messageItem.nativeAccount
                //把历史记录的图片加入图片浏览器数据源
                imageUrls.append(msgImgItem.imageUrl!)
                msgImgItem.sentTime = messageItem.time
                localMsgs.append(msgImgItem)
            }else if messageItem.msgType == "3"{
                //红包
                let redPacketItem = CRRedPacketMessage()
                redPacketItem.msgUUID = messageItem.msgId
                redPacketItem.msgId = messageItem.msgId
                redPacketItem.privatePermission = messageItem.privatePermission
                redPacketItem.userId = messageItem.sender
                redPacketItem.stopTalkType = messageItem.stopTalkType
                redPacketItem.isPlanUser = messageItem.isPlanUser
                redPacketItem.userType = messageItem.userType
                redPacketItem.messageType = .RedPacket
                //                redPacketItem.sendState = .Success
                redPacketItem.isShowTime = messageItem.isShowTime
                redPacketItem.time = messageItem.time
                //发送者和接收者区分
                if isPrivateChat {
                    redPacketItem.userId = messageItem.userId
                    redPacketItem.msgUUID = messageItem.userMessageId
                    redPacketItem.msgId = messageItem.userMessageId
                }
                
                redPacketItem.ownerType = redPacketItem.userId == CRDefaults.getUserID() ? .TypeSelf:.Others
                
                redPacketItem.avatar = messageItem.avatar
                //金额
                //                                redPacketItem.amount  = messag。eItem.money
                redPacketItem.remark = messageItem.remark
                redPacketItem.payId = messageItem.payId
                redPacketItem.levelIcon = messageItem.levelIcon
                redPacketItem.levelName = messageItem.levelName
                //用户昵称
                redPacketItem.nickName = messageItem.nickName
                redPacketItem.account = messageItem.account
                redPacketItem.nativeAccount = messageItem.nativeAccount
                redPacketItem.sentTime = messageItem.time
                //                                redPacketItem.payId = messageItem.payId
                localMsgs.append(redPacketItem)
                
            }else if ["5","8"].contains(messageItem.msgType) {
                //分享注单
                //"\(CRMessageType.ShareBet.hashValue)"
                let shareMessageItem = CRShareBetMessageItem()
                shareMessageItem.msgUUID = messageItem.msgId
                shareMessageItem.msgId = messageItem.msgId
                shareMessageItem.privatePermission = messageItem.privatePermission
                shareMessageItem.userId = messageItem.sender
                shareMessageItem.stopTalkType = messageItem.stopTalkType
                shareMessageItem.isPlanUser = messageItem.isPlanUser
                shareMessageItem.userType = messageItem.userType
                shareMessageItem.messageType = .ShareBet
                shareMessageItem.sentTime = messageItem.time
                shareMessageItem.avatar = messageItem.avatar
                shareMessageItem.levelIcon = messageItem.levelIcon
                shareMessageItem.levelName = messageItem.levelName
                shareMessageItem.time = messageItem.time
                shareMessageItem.isShowTime = messageItem.isShowTime
                shareMessageItem.nickName = messageItem.nickName
                shareMessageItem.account = messageItem.account
                shareMessageItem.nativeAccount = messageItem.nativeAccount
                
                if isPrivateChat {
                    shareMessageItem.userId = messageItem.userId
                    shareMessageItem.msgUUID = messageItem.userMessageId
                    shareMessageItem.msgId = messageItem.userMessageId
                }
                
                shareMessageItem.ownerType = shareMessageItem.userId == CRDefaults.getUserID() ? .TypeSelf:.Others
                
                var jsonString = ""
                var shareBetModel = CRShareBetModel()
                
                //                if messageItem.msgType == "5" {
                //                    jsonString = messageItem.context
                //
                //                    //暂时没有一单中，有多种注单消息的，因为多注单都被拆分成一个注单发出去的，所以这里可以取数组中最后一个数据（因为数组中最多有一个元素）
                //                    if let betInfos = [CRShareBetInfo].deserialize(from: jsonString),let betInfo = betInfos.first {
                //                        shareBetModel.betInfo = betInfo
                //
                //                        let shareOrder = CRShareOrder()
                //                        shareOrder.betMoney = betInfo?.betMoney ?? ""
                //                        shareOrder.orderId = betInfo?.orderId ?? ""
                //                        shareBetModel.orders = [shareOrder]
                //                    }
                //                }else if messageItem.msgType == "8" {
                jsonString = messageItem.nativeContent
                
                guard let betModel = CRShareBetModel.deserialize(from: jsonString), let betInfos = betModel.betInfos else {
                    //过滤错误信息
                    continue
                }
                
                var height:CGFloat = 0
                if betInfos.count == 1 {
                    height = CRShareBetAllView.singleH
                }else if betInfos.count >= 2 {
                    height = CGFloat(2) *  CRShareBetAllView.singleH + CRShareBetAllView.followAllH
                }
                
                let messageFrame  = CRMessageFrame()
                messageFrame.contentSize = CGSize.init(width: 300, height: height)
                messageFrame.height = Float(height) + Float(50)
                
                shareMessageItem.messageFrame = messageFrame
                shareBetModel = betModel
                //                }
                
                shareMessageItem.shareBetModel = shareBetModel
                //胜率
                shareMessageItem.winRate = shareBetModel.winOrLost?.winPer ?? 0
                localMsgs.append(shareMessageItem)
                
            }else if messageItem.msgType == "6" { //计划消息
                let planMsgItem = CRPlanMsgItem()
                planMsgItem.msgUUID = messageItem.msgId
                planMsgItem.msgId = messageItem.msgId
                planMsgItem.privatePermission = messageItem.privatePermission
                planMsgItem.userId = messageItem.sender
                planMsgItem.stopTalkType = messageItem.stopTalkType
                planMsgItem.isPlanUser = messageItem.isPlanUser
                planMsgItem.userType = messageItem.userType
                planMsgItem.ownerType = .Others
                planMsgItem.time = messageItem.time
                planMsgItem.isShowTime = messageItem.isShowTime
                planMsgItem.messageType = .PlanMsg
                planMsgItem.msgResult?.userEntity?.nickName = "计划消息"
                planMsgItem.text = messageItem.text ?? ""
                planMsgItem.lotteryName = messageItem.lotteryName
                
                if isPrivateChat {
                    planMsgItem.userId = messageItem.userId
                    planMsgItem.msgUUID = messageItem.userMessageId
                    planMsgItem.msgId = messageItem.userMessageId
                }
                
                let planItem = CRPlanMessage()
                planItem.text = planMsgItem.text ?? ""
                planItem.lotteryName = planMsgItem.lotteryName
                planMsgItem.planItem = planItem
                //                planMsgItem.planItem = CRPlanMessage.deserialize(from: jsonString)
                
                planMsgItem.sentTime = messageItem.time
                
                planMsgItem.msgUUID = planMsgItem.planItem?.msgUUID ?? ""
                planMsgItem.avatar = messageItem.avatar ?? ""
                planMsgItem.nickName = messageItem.nickName.isEmpty ? "计划消息" : messageItem.nickName
                
                localMsgs.append(planMsgItem)
            }else if messageItem.msgType == "7"{
                //机器人消息
                let robotItem = CRRobotMsgItem()
                robotItem.msgUUID = messageItem.msgId
                robotItem.msgId = messageItem.msgId
                robotItem.stopTalkType = messageItem.stopTalkType
                robotItem.isPlanUser = messageItem.isPlanUser
                robotItem.userType = messageItem.userType
                //                robotItem.robotMessage?.record = messageItem.context
                robotItem.text = messageItem.text
                robotItem.userId = messageItem.sender
                
                let robotMessageItem = CRRobotMessage()
                robotMessageItem.fromUser?.robotImage = messageItem.avatar ?? ""
                robotMessageItem.record = messageItem.context
                robotItem.robotMessage = robotMessageItem
                
                let contents = CRChatRoomLogic.deleteHtmlTagsWhenPTag(contents: robotMessageItem.record) + ""
                robotMessageItem.record = contents
                
                if robotMessageItem.record.contains("<p>") || contents.contains("<img") || contents.contains("<p") || contents.contains("</p") {
                    robotItem.time = messageItem.time
                    robotItem.sentTime = messageItem.time
                    robotItem.isShowTime = messageItem.isShowTime
                    robotItem.ownerType = .Others
                    robotItem.messageType = .robotMsg
                    
                    robotItem.avatar = messageItem.avatar
                    robotItem.nickName = messageItem.nickName
                    robotItem.nativeAccount = messageItem.nativeAccount
                    robotItem.account = messageItem.account
                    robotItem.levelIcon = messageItem.levelIcon
                    robotItem.levelName = messageItem.levelName
                    
                    localMsgs.append(robotItem)
                }else {
                    let msgTextItem = CRTextMessageItem()
                    msgTextItem.msgUUID = messageItem.msgId
                    msgTextItem.msgId = messageItem.msgId
                    msgTextItem.stopTalkType = messageItem.stopTalkType
                    msgTextItem.isPlanUser = messageItem.isPlanUser
                    msgTextItem.userType = messageItem.userType
                    
                    msgTextItem.messageType = .Text
                    msgTextItem.isShowTime = messageItem.isShowTime
                    msgTextItem.ownerType = .Others
                    msgTextItem.text = contents
                    msgTextItem.avatar = messageItem.avatar
                    msgTextItem.userId = messageItem.sender
                    if messageItem.time.length > 5{
                        msgTextItem.sentTime = messageItem.time.subString(start: 10)
                    }
                    msgTextItem.levelIcon = messageItem.levelIcon
                    msgTextItem.levelName = messageItem.levelName
                    msgTextItem.time = messageItem.time
                    msgTextItem.nickName = messageItem.nickName
                    msgTextItem.account = messageItem.account
                    msgTextItem.nativeAccount = messageItem.nativeAccount
                    
                    localMsgs.append(msgTextItem)
                    //////////////////////////
                    
                    //                    let textMessageItem = CRTextMessageItem()
                    //                    textMessageItem.msgUUID = messageItem.msgId
                    //                    msgTextItem.stopTalkType = messageItem.stopTalkType
                    //                    textMessageItem.msgType = "\(CRMessageType.Text.rawValue)"
                    //                    textMessageItem.isPlanUser = messageItem.isPlanUser
                    //                    textMessageItem.userType = messageItem.userType
                    //                    textMessageItem.messageType = .Text
                    //                    textMessageItem.ownerType = .Others
                    //                    textMessageItem.userId = messageItem.userId
                    //
                    //                    //用户图像
                    //                    textMessageItem.avatar = messageItem.avatar
                    //                    textMessageItem.levelIcon = messageItem.levelIcon
                    //                    textMessageItem.levelName = messageItem.levelName
                    //
                    //                    textMessageItem.nickName = messageItem.nickName
                    //                    textMessageItem.account = messageItem.account
                    //                    textMessageItem.nativeAccount = messageItem.nativeAccount
                    //                    textMessageItem.text = robotMessageItem.record
                    //                    textMessageItem.sentTime  = messageItem.sentTime
                    //                    localMsgs.append(robotItem)
                }
                
            }else if messageItem.msgType == "9" || messageItem.msgType == "4" { // 分享今日输赢,9 群聊；4 私聊
                //今日分享
                var contents = ""
                
                if isPrivateChat {
                    contents = messageItem.record
                }else {
                    contents = messageItem.context
                }
                
                guard let shareTodayGainsLossesItem = CRShareGainsLossesItem.deserialize(from: contents) else{
                    continue
                }
                
                shareTodayGainsLossesItem.msgUUID = messageItem.msgId
                shareTodayGainsLossesItem.msgId = messageItem.msgId
                shareTodayGainsLossesItem.privatePermission = messageItem.privatePermission
                shareTodayGainsLossesItem.userId = messageItem.sender
                shareTodayGainsLossesItem.stopTalkType = messageItem.stopTalkType
                shareTodayGainsLossesItem.isPlanUser = messageItem.isPlanUser
                shareTodayGainsLossesItem.userType = messageItem.userType
                shareTodayGainsLossesItem.messageType = .ShareGainsLosses
                shareTodayGainsLossesItem.isShowTime = messageItem.isShowTime
                shareTodayGainsLossesItem.userId = messageItem.sender
                shareTodayGainsLossesItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
                shareTodayGainsLossesItem.avatar = messageItem.avatar
                shareTodayGainsLossesItem.levelIcon = messageItem.levelIcon
                shareTodayGainsLossesItem.levelName = messageItem.levelName
                shareTodayGainsLossesItem.sentTime = messageItem.time
                shareTodayGainsLossesItem.time = messageItem.time
                shareTodayGainsLossesItem.nickName = messageItem.nickName
                shareTodayGainsLossesItem.account = messageItem.account
                shareTodayGainsLossesItem.nativeAccount = messageItem.nativeAccount
                
                if isPrivateChat {
                    shareTodayGainsLossesItem.userId = messageItem.userId
                    shareTodayGainsLossesItem.msgUUID = messageItem.userMessageId
                    shareTodayGainsLossesItem.msgId = messageItem.userMessageId
                }
                
                shareTodayGainsLossesItem.ownerType = shareTodayGainsLossesItem.userId == CRDefaults.getUserID() ? .TypeSelf:.Others
                
                localMsgs.append(shareTodayGainsLossesItem)
            }else if messageItem.msgType == "11"{
                //音频消息
                let voiceMessageItem = CRVoiceMessageItem()
                voiceMessageItem.msgUUID = messageItem.msgId
                voiceMessageItem.msgId = messageItem.msgId
                voiceMessageItem.privatePermission = messageItem.privatePermission
                voiceMessageItem.userId = messageItem.sender
                voiceMessageItem.stopTalkType = messageItem.stopTalkType
                voiceMessageItem.isPlanUser = messageItem.isPlanUser
                voiceMessageItem.userType = messageItem.userType
                voiceMessageItem.messageType = .Voice
                //在语音历史消息中后台返回的字段remark代表语音时长
                voiceMessageItem.durationTime = messageItem.remark
                voiceMessageItem.time = messageItem.time
                voiceMessageItem.isShowTime = messageItem.isShowTime
                
                //发送者和接收者区分
                if isPrivateChat {
                    voiceMessageItem.userId = messageItem.userId
                    voiceMessageItem.msgUUID = messageItem.userMessageId
                    voiceMessageItem.msgId = messageItem.userMessageId
                }
                
                voiceMessageItem.ownerType = voiceMessageItem.userId == CRDefaults.getUserID() ? .TypeSelf:.Others
                
                voiceMessageItem.avatar = messageItem.avatar
                voiceMessageItem.levelIcon = messageItem.levelIcon
                voiceMessageItem.levelName = messageItem.levelName
                voiceMessageItem.msgUUID = messageItem.msgUUID
                voiceMessageItem.nickName = messageItem.nickName
                voiceMessageItem.account = messageItem.account
                voiceMessageItem.nativeAccount = messageItem.nativeAccount
                voiceMessageItem.sentTime = messageItem.time
                let fillCodeText = messageItem.context
                if fillCodeText.contains("/"){
                    let array = fillCodeText.components(separatedBy: "/")
                    voiceMessageItem.fileCode = array.last!
                }
                
                localMsgs.append(voiceMessageItem)
                //下载历史记录的语音消息
                downloadFileFromServer(fileCode: voiceMessageItem.fileCode, voiceMsgItem: voiceMessageItem)
            }
        }
        
        contentView.messageArea.msgItems.insert(contentsOf: localMsgs, at: 0)
        
        contentView.messageArea.msgTable.reloadData()
        
        if historyStart == 0 {
            scrollTableViewToBottom(animated: false)
        }
        
        //需要判断 historyStart 自增条件
        
        //        contentView.messageArea.msgTable.mj_header?.endRefreshing()
    }
    
    //MARK: "user_r" 消息回调Response处理
    private func receiveUserMsg(msgType:String = "",datas:[Any],handler:@escaping (_ success:Bool,_ mzsg:String, _ model:Any?,_ type:String?,_ msgUUID:String?,_ privateType:String?) -> ()) {
        
        HUDView.hide(animated: true)
        
        if let jsonAndCodeTuples = CRChatRoomLogic.formatDecodedStringArray(msgType:msgType,datas: datas) {
            
            let code = jsonAndCodeTuples.type
            let jsonArray = jsonAndCodeTuples.jsonArray
            var response = ""
            if jsonArray.count > 0 {
                response = jsonArray[0]
            }
            
            if let baseWraper = CRMessage.deserialize(from: response) {
                if !baseWraper.success {
                    showToast(view: self.view, txt: baseWraper.msg.isEmpty ? "请求异常" : baseWraper.msg)
                }
            }
            
            if msgType == CR_USER_JOIN_GROUP_R { //私聊切换房间

                class PrivateJoinGroup: HandyJSON {
                    var success = false
                    var type:String? //不要赋值空字符串 ""，作为默认值，""代表退出私聊
                    var roomId = ""
                    required init() {}
                }
                
                guard let model = PrivateJoinGroup.deserialize(from: response) else {return}
                if let type = model.type {
                    if type == "join" {
                        chat_privateRoomId = model.roomId
                        chat_passiveUserId = passiveUser?.id ?? ""
                        
                        //更改聊天室title
                        if let user = passiveUser {
                            let nickName = user.nickName
                            let account = user.account
                            let name = nickName.isEmpty ? account : nickName
                            
                            let canSwitchRoom = getChatRoomSystemConfigFromJson()?.source?.switch_room_show != "0"
                            navigationBar.setTitleButtonCanClick(can: canSwitchRoom, title: name)
                        }
                        
                        //拉取历史记录
                        isPersonBandSpeak = "0"
                        isRoomBandSpeak = "0"
                        sendMsgPrivateChatHistory(roomId: chat_privateRoomId, type: "2")
                        sendMsgRequestPrivateChat(type: "3", model: passiveUser)
                    }else if type == "" {
                        
                    }
                }
                
            }else if msgType == CR_USER_JOIN_ROOM_R { //群聊换房间
                
                class CRChangeRoomModel: HandyJSON {
                    var success = false
                    var roomId = ""
                    var msg = ""
                    var code = ""
                    var status = ""
                    var msgUUID = ""
                    required init() {}
                }
                
                let funcDes = "切换房间"
                guard let wrapper = CRChangeRoomModel.deserialize(from: response) else {
                    handler(false,"\(funcDes)失败",nil, "", "", nil)
                    return
                }
                
                if wrapper.success  {
                    handler(true,"",wrapper.roomId,code,wrapper.msgUUID, nil)
                }else {
                    handler(false,wrapper.msg,nil,code,wrapper.msgUUID, nil)
                }
                
            }else {
                if code == "\(sendMessageType.privateChatInit.rawValue)" { //私聊初始化相关
                    self.handlePrivateChatInitalHandleWith(code: code, response: response) { (success, msg, model, code, msgUUid) in
                        handler(success,msg,model,code, msgUUid, nil)
                    }
                }else if code == "\(sendMessageType.longDragon.rawValue)" {
                    //  response就是获取到的 长龙数据代码
                    self.handleLongDragonWith(code: code, response: response) { (success, msg, model, code, msgUUid) in
                        return
                    }
                }
                else if ["\(sendMessageType.privateChatSend.rawValue)","\(sendMessageType.privateHistory.rawValue)"].contains(code) { //私聊发送消息相关
                    self.handlePrivateChatSendHandleWith(code: code, response: response) { (success, msg, model, code, msgUUid,privateType) in
                        
                        handler(success,msg,model,code, msgUUid, privateType)
                    }
                    
                }else if code == CRUrl.shareInstance().CODE_IMAGECOLLECT { // 新增图片收藏列表数据回调
                    let funcDes = "收藏图片"
                    guard let wrapper = CRImageCollectListModel.deserialize(from: response) else {
                        handler(false,"获取\(funcDes)失败",nil,code,"", nil)
                        return
                    }
                    if wrapper.success == false {
                        showErrorHUD(errStr: wrapper.msg.isEmpty ? "请求失败" : wrapper.msg)
                        return
                    }
                    if wrapper.source?.option == "2" {
                        showErrorHUD(errStr: "收藏图片成功")
                        return
                    }
                    self.imageCollectModelHandle?(wrapper)
                }
                    /// 新增拉取快速发言列表
                else if code == CRUrl.shareInstance().CODE_SPEAKQUICKLY_LIST {
                    let funcDes = "获取快速发言列表数据"
                    guard let warpper = CRSpeakQuicklyListModel.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code,"", nil)
                        return
                    }
                    if warpper.source.isEmpty {
                        showErrorHUD(errStr: "暂无快速发言数据")
                        return
                    }
                    showSpeakQuicklyList(list: warpper.source)
                }
                    /// 新增彩票计划列表
                else if code == CRUrl.shareInstance().CODE_GAME_LIST {
                    let funcDes = "获取彩票计划列表数据"
                    guard let wrapper = CRLotterySchemeModel.deserialize(from: response) else {
                        self.lotterySchemeModelHandle?(CRLotterySchemeModel())
                        handler(false,"\(funcDes)失败",nil,code,"", nil)
                        return
                    }
                    self.lotterySchemeModelHandle?(wrapper)
                }else if code == CRUrl.shareInstance().CHAT_ROOM_LIST {
                    let funcDes = "获取聊天室列表"
                    guard let wrapper = CRRoomListWrapper.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code,"", nil)
                        return
                    }
                    
                    if wrapper.success  {
                        handler(true,"",wrapper.source,code,wrapper.msgUUID, nil)
                    }else {
                        handler(false,wrapper.msg,nil,code,wrapper.msgUUID, nil)
                    }
                }else if code == CRUrl.shareInstance().LOGIN_AUTHORITY {
                    
                    let funcDes = "聊天室登录"
                    guard let wrapper = CRAuthWrapper.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code, "", nil)
                        return
                    }
                    
                    if wrapper.success  {
                        handler(true,"",wrapper.source,code, nil, nil)
                    }else {
                        handler(false,wrapper.msg,nil,code, nil, nil)
                    }
                }else if code == CRUrl.shareInstance().JOIN_CHAT_ROOM {
                    let funcDes = "进入房间"
                    
                    guard let wrapper = CRRoomInfoWrapper.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code, nil, nil)
                        return
                    }
                    if wrapper.success  {
                        handler(true,"",wrapper.source,code, nil, nil)
                        
                        
                    }else {
                        handler(false,wrapper.msg,nil,code, nil, nil)
                    }
                }else if code == CRUrl.shareInstance().CODE_ROOM_CONFIG {
                    let funcDes = "拉取房间配置"
                    guard let wrapper = CRSystemConfigurationItem.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code, nil, nil)
                        return
                    }
                    if wrapper.success  {
                        handler(true,"",response,code, nil, nil)
                    }else {
                        handler(false,wrapper.msg,nil,code, nil, nil)
                    }
                }else if code == CRUrl.shareInstance().SEND_MSG_TXT {
                    guard let jsonObjctItem = CRMessage.deserialize(from: response) else{
                        handler(false,"发送消息失败",nil,code, "", nil)
                        return
                    }
                    
                    setMsgId(message: jsonObjctItem,response:response)
                    
                    if  jsonObjctItem.success {
                        handler(true, "发送消息成功", jsonObjctItem, code, jsonObjctItem.msgUUID, nil)
                    }else{
                        handler(false,jsonObjctItem.msg,jsonObjctItem,code, jsonObjctItem.msgUUID, nil)
                    }
                }else if code == CRUrl.shareInstance().CODE_SHARE_BET_MSG {
                    //                    let funcDes = "分享注单"
                    print("\(response)")
                    guard let shareOrderItem = CRShareBetMessageItem.deserialize(from: response) else{
                        handler(false,"发送分享注单失败",nil,code, nil, nil)
                        return
                    }
                    
                    setMsgId(message: shareOrderItem,response:response)
                    
                    if shareOrderItem.success{
                        handler(true,"发送分享注单成功",shareOrderItem,code, shareOrderItem.msgUUID, nil)
                    }else{
                        handler(false,shareOrderItem.msg,shareOrderItem,code, shareOrderItem.msgUUID, nil)
                    }
                    
                }else if code == "\(sendMessageType.redPacket.rawValue)" {
                    
                    guard let jsonObjctItem = CRRedPacketDataItem.deserialize(from: response) else{
                        handler(false,"发送红包失败",nil,code, nil, nil)
                        return
                    }
                    
//                    setMsgId(message: jsonObjctItem,response:response)
                    
                    if jsonObjctItem.success{
                        handler(true, "发送红包成功", jsonObjctItem,code, jsonObjctItem.msgUUID, nil)
                    }else{
                        handler(false,jsonObjctItem.msg,jsonObjctItem,code, jsonObjctItem.msgUUID, nil)
                    }
                }else if code == "\(sendMessageType.image.rawValue)" {
                    guard let msg = CRImageMessage.deserialize(from: response) else {
                        handler(false,"发送图片失败",nil,code, nil, nil)
                        return
                    }
                    
                    setMsgId(message: msg,response:response)
                    
                    if msg.success {
                        handler(true,"发送图片成功",msg,code, msg.msgUUID, nil)
                    }else {
                        handler(false,msg.msg,msg,code,msg.msgUUID, nil)
                    }
                }else if code == "\(sendMessageType.systemNotice.rawValue)" { //系统公告
                    guard let jsonObjcItem = CRSystemNoticeItem.deserialize(from: response) else{
                        handler(false,"获取系统公告失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取系统公告成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.contrabandLanguage.rawValue)" { //违禁词
                    guard let jsonObjcItem = CRProhibitLanguageItem.deserialize(from: response) else{
                        handler(false,"获取违禁词失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success{
                        
                        self.prohibitLanguageArray = jsonObjcItem.source ?? []
                        
                        handler(true,"获取违禁词成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.followOrder.rawValue)" { //跟单
                    guard let jsonObjctItem = CRFollowBetWrapper.deserialize(from: response) else{
                        handler(false,"跟单失败",nil,code, nil, nil)
                        return
                    }
                    
                    guard let modelWraper = jsonObjctItem.source else {
                        handler(false,"\(jsonObjctItem.msg)",nil,code, nil, nil)
                        return
                    }
                    if modelWraper.success {
                        handler(true,"跟单成功",modelWraper,code, nil, nil)
                    }else {
                        handler(false,modelWraper.msg,nil,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.pullLotteryTicket.rawValue)" { //拉取开奖结果
                    guard let jsonObjcItem = CRLotterDataItem.deserialize(from: response) else{
                        handler(false,"获取彩种失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取彩种成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.historyRecord.rawValue)" { //获取历史记录
                    guard let jsonObjcItem = CRHistoryRecordItem.deserialize(from: response) else{
                        handler(false,"获取聊天记录失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取聊天记录成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.historyBetShare.rawValue)" { //投注历史
                    guard let wrapper = CRBetHistoryWraper.deserialize(from: response) else {
                        handler(false,"获取历史注单消息失败",nil,code, nil, nil)
                        return
                    }
                    if wrapper.success{
                        handler(true,"获取历史注单消息成功",wrapper.source?.msg,code, nil, nil)
                    }else{
                        handler(false,"获取历史注单消息失败",wrapper.source?.msg,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.tutorPlan.rawValue)" {
                    guard let jsonObjcItem = CRHistoryRecordItem.deserialize(from: response) else{
                        handler(false,"获取导师计划失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取导师计划成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.userInfo.rawValue)" { //个人信息
                    guard let jsonObjcItem = CRUserInfoCenterItem.deserialize(from: response) else{
                        handler(false,"获取个人信息失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success {
                        handler(true,"获取个人信息成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if ["\(sendMessageType.userIcon.rawValue)","\(sendMessageType.submitUserInfo.rawValue)"].contains(code) { //默认图片列表 //提交修改的用户个人信息
                    guard let jsonObjcItem = CRSystemIconItem.deserialize(from: response) else{
                        handler(false,"获取系统默认用户图像失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success{
                        handler(true,"获取系统默认用户图像成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if ["\(sendMessageType.receiveRedPacket.rawValue)","\(sendMessageType.receiveRedPacketNew.rawValue)"].contains(code) {
                    guard let jsonObjctItem = CRReceiveRedPacketItem.deserialize(from: response) else{
                        handler(false,"发送抢红包消息失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjctItem.success{
                        handler(true,"发送抢红包消息成功",jsonObjctItem,code, nil, nil)
                    }else{
                        if jsonObjctItem.status == "se34" {
                            
                            class LocalReceiveRedPacket: HandyJSON {
                                var payId = ""
                                required init() {}
                            }
                            
                            if let localReceiveRedPacket = LocalReceiveRedPacket.deserialize(from: response) {
                                let model = CRReceiveRedPacketItem()
                                let source = CRReceiveRedPacketSourceItem()
                                source.payId = localReceiveRedPacket.payId
                                model.source = source
                                handler(true,jsonObjctItem.msg,model,code, nil, nil)
                            }
                        }else {
                            handler(false,jsonObjctItem.msg,jsonObjctItem,code, nil, nil)
                        }
                    }
                }else if code == "\(sendMessageType.receiveDetail.rawValue)" {
                    guard let jsonObjcItem = CRReceiveDetailItem.deserialize(from: response) else{
                        handler(false,"获取领取红包详情失败",nil,code, nil, nil)
                        return
                    }
                    if jsonObjcItem.success {
                        handler(true,"获取领取红包详情成功",jsonObjcItem,code, nil, nil)
                    }else{
                        handler(false,jsonObjcItem.msg,jsonObjcItem,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.shareToday.rawValue)" {
                    print(response)
                }else if ["\(sendMessageType.roomOnLineList.rawValue)","\(sendMessageType.managerList.rawValue)"].contains(code) {
                    
                    var funcDes = ""
                    
                    if code == "\(sendMessageType.roomOnLineList.rawValue)" {
                        funcDes = "获取在线用户"
                    }else if code == "\(sendMessageType.managerList.rawValue)" {
                        funcDes = "获取管理员列表"
                    }
                    
                    guard let wraper = CROnLineListWraper.deserialize(from: response) else {
                        handler(false,"\(funcDes)失败",nil,code, nil, nil)
                        return
                    }
                    
                    if wraper.success {
                        handler(true,"",wraper,code, nil, nil)
                    }else {
                        handler(false,wraper.msg,wraper,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.currentBalance.rawValue)" {
                    guard let wraper = CRBalanceWraper.deserialize(from: response) else {
                        handler(false,"获取余额失败",nil,code, nil, nil)
                        return
                    }
                    
                    if wraper.success {
                        handler(true,"",wraper.source,code, nil, nil)
                    }else {
                        handler(false,wraper.msg,wraper,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.winPrizeList.rawValue)"{
                    guard let wraper = CRWinPrizeListItem.deserialize(from: response) else{
                        handler(false,"获取榜单失败",nil,code, nil, nil)
                        return
                    }
                    
                    if wraper.success {
                        handler(true,"",wraper.source,code, nil, nil)
                    }else {
                        handler(false,wraper.msg,wraper,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.signIn.rawValue)" {
                    
                    guard let wraper = CRSignInWraper.deserialize(from: response) else{
                        handler(false,"签到失败",nil,code, nil, nil)
                        return
                    }
                    
                    if wraper.success {
                        guard let model = wraper.source else {
                            handler(false,"签到失败",nil,code, nil, nil)
                            return
                        }
                        
                        if model.success {
                            handler(true,"",model,code, nil, nil)
                        }else {
                            handler(false,model.erroMsg,model,code, nil, nil)
                        }
                    }else {
                        handler(false,wraper.msg,nil,code, nil, nil)
                    }
                }else if code == "\(sendMessageType.prohibit.rawValue)" { //禁言,取消禁言
                    guard let wraper = CRPorhibitModelWraper.deserialize(from: response) else {return}
                    if !wraper.success {
                        showToast(view: self.view, txt: wraper.msg.isEmpty ? "禁言处理失败" : wraper.msg)
                    }else if wraper.source?.speakingClose == "1" {
                        showToast(view: self.view, txt: "禁言成功")
                    }else if wraper.source?.speakingClose == "0" {
                        showToast(view: self.view, txt: "取消禁言成功")
                    }else {
                        showToast(view: self.view, txt: "禁言处理失败")
                    }
                }else if code == "\(sendMessageType.prohibitAll.rawValue)" { //禁言房间,取消房间禁言
                    guard let wraper = CRPorhibitModelWraper.deserialize(from: response) else {return}
                    if !wraper.success {
                        showToast(view: self.view, txt: wraper.msg.isEmpty ? "禁言处理失败" : wraper.msg)
                    }else if wraper.source?.isBanSpeak == "1" {
                        showToast(view: self.view, txt: "全体禁言成功")
                    }else if wraper.source?.isBanSpeak == "0" {
                        showToast(view: self.view, txt: "解除全体禁言成功")
                    }else {
                        showToast(view: self.view, txt: "全体禁言处理失败")
                    }
                }else if code == "\(sendMessageType.retract.rawValue)" { // 撤回
                    guard let wraper = CRBaseModel.deserialize(from: response) else {return}
                    if !wraper.success {
                        showToast(view: self.view, txt: wraper.msg.isEmpty ? "删除消息失败" : wraper.msg)
                    }
                }else if code == "\(sendMessageType.toolPermisson.rawValue)" { //获取工具箱权限
                    guard let wraper = CRToolPermissonWraper.deserialize(from: response) else {return}
                    if !wraper.success {
                        showToast(view: self.view, txt: wraper.msg.isEmpty ? "获取工具箱权限失败" : wraper.msg)
                    }else {
                        guard let source = wraper.source,let toolpermissson = source.toolPermission else {
                            return
                        }
                        
                        if contentView.messageArea.toolPermission.count == 0 {
                            let tools = formatToolPermission(toolPermission: toolpermissson)
                            contentView.messageArea.toolPermission = tools
                        }
                    }
                }else if code == "\(sendMessageType.appealBand.rawValue)" { //申述
                    let errorMsg = "申述请求失败"
                    if let wraper = CRBaseModel.deserialize(from: response) {
                        if wraper.success {
                            showToast(view: self.view, txt: "已提交")
                        }else {
                            showToast(view: view, txt: wraper.msg.isEmpty ? errorMsg : wraper.msg)
                        }
                    }else {
                        showToast(view: view, txt: errorMsg)
                    }
                }else if code == "\(sendMessageType.updatePersonnalAvatar.rawValue)" {
                    let msg = "修改头像"
                    
                    class InFuncModel:CRBaseModel {
                        var source:InFuncSource?
                    }
                        
                    class InFuncSource:HandyJSON {
                        var avatar = ""
                        required init() {}
                    }
                    
                    if let wraper = InFuncModel.deserialize(from: response) {
                        if wraper.success {
                            showToast(view: self.view, txt: "\(msg)成功")
                            
                            guard let topVC = self.navigationController?.topViewController as? CRUserProfileInfoCtrl else {
                                return
                            }
                            
                            topVC.modifyAvatar = wraper.source?.avatar ?? ""
                            sendMsgSubmitInfo()
                            
                        }else {
                            showToast(view: view, txt: wraper.msg.isEmpty ? "\(msg)失败" : wraper.msg)
                        }
                    }else {
                        showToast(view: view, txt: "\(msg)失败")
                    }
                }
            }
        }
    }
    
    //MARK: 格式化全乡工具箱数据
    func formatToolPermission(toolPermission:[CRToolPermission]) -> [String] {
        var tools = [String]()
        for model in toolPermission {
            tools.append(model.toolCode)
        }
        
        return tools
    }
    
    //MARK: "user_r" 消息回调Model 处理
    func handleReceiveUserMsgWith(data:[Any]) {
        
        receiveUserMsg(datas: data, handler: {[weak self] (success, msg, model,type,msgUUID,privateType) in
            
            guard let weakSelf = self else {return}
            
            var messageItem:CRMessage?
            if type == sendMessageType.text.rawValue ||
                type == sendMessageType.image.rawValue ||
                type == sendMessageType.redPacket.rawValue ||
                type == sendMessageType.shareOrder.rawValue {
                
                let msgItems = weakSelf.contentView.messageArea.msgItems
                messageItem = CRChatRoomLogic.getMsgFromMessageItemsWith(msgUUID: msgUUID, msgItems:msgItems)
            }
           
            if !success {
//                messageItem?.sendState = .Fail
                weakSelf.updateMsgOnCell(msgUUID: msgUUID ?? "", sendStatus: .Fail)
                
                if type == sendMessageType.text.rawValue {
                    weakSelf.scrollTableViewToBottom(animated: false)
                }else if type == sendMessageType.image.rawValue ||
                    type == sendMessageType.shareOrder.rawValue ||
                    type == sendMessageType.redPacket.rawValue {
                    weakSelf.contentView.messageArea.msgTable.reloadData()
                    weakSelf.scrollTableViewToBottom(animated: false)
                }else if type == sendMessageType.submitUserInfo.rawValue{
                    showToast(view: (weakSelf.navigationController?.topViewController?.view)!, txt: msg)
                }
                showToast(view: weakSelf.view, txt: msg)
                return
            }else{
                
                if type == sendMessageType.text.rawValue ||
                    type == sendMessageType.image.rawValue ||
                    type == sendMessageType.redPacket.rawValue ||
                    type == sendMessageType.shareOrder.rawValue{
                    messageItem?.sendState = .Success
                    weakSelf.playSendMessageSuccessVoice()
                    //                            weakSelf.contentView.messageArea.msgTable.reloadData()
                }
                
            }
            if type == CRUrl.shareInstance().CHAT_ROOM_LIST { //获取房间列表
                guard let m = model as? CRRoomListModels else {return}
                
                weakSelf.handleGetRoomList(rooms: m)
            }else if type == CRUrl.shareInstance().JOIN_CHAT_ROOM { //进入房间
                guard let infoModel = model as? CRRoomInfoModel else {return}
                let winrateItem = infoModel.winOrLost
                //投注命中率
                weakSelf.userWinRate = winrateItem?.winPer ?? 0
                //投注命中率
                CRDefaults.setBetWinRate(rate: winrateItem?.winPer ?? 0)
                
                //socket发送换房间消息
                weakSelf.sendMsgChangeRoom(roomInfo: infoModel)
            }else if type == CRUrl.shareInstance().CODE_ROOM_CONFIG { //拉取房间配置
                guard let jsonString = model as? String else {return}
                //name_new_members_default_photo
                CRPreference.saveChatRoomConfig(value: jsonString as AnyObject)
                weakSelf.configWithChatConfig() //获取到开关后进行的配置
                
                weakSelf.sendMsgGetUserAvatarList(userId: CRDefaults.getUserID())
            }else if type == CRUrl.shareInstance().SEND_MSG_TXT { //发送文本
//                guard let textMsgItem = model as? CRMessage else{return}
                
//                let msgItems = weakSelf.contentView.messageArea.msgItems
//                guard let originalMsg = CRChatRoomLogic.getMsgFromMessageItemsWith(msgUUID: textMsgItem.msgUUID, msgItems:msgItems) as? CRTextMessageItem else {
//                    return
//                }
//                originalMsg.sendState = .Success
                
            }else if type == CRUrl.shareInstance().CODE_SHARE_BET_MSG { //分享注单
                
                print("")
            }else if type == "\(sendMessageType.image.rawValue)" { //发送图片
//                guard let imgMsgItem = model as? CRImageMessage else{return}
//                
//                let msgItems = weakSelf.contentView.messageArea.msgItems
//                guard let originalMsg = CRChatRoomLogic.getMsgFromMessageItemsWith(msgUUID: imgMsgItem.msgUUID, msgItems:msgItems) as? CRImageMessage else{return}
//                originalMsg.sendState = .Success
                
            }else if type == "\(sendMessageType.systemNotice.rawValue)" { //系统公告 stringByDecodingHTMLEntities
                if let noticeItem = model as? CRSystemNoticeItem{
                    let sourceString = noticeItem.source?.toJSONString()?.htmlDecoded()
                    if let sourceArray = [CRSystemNoticesourceItem].deserialize(from: sourceString){
                        if sourceArray.count != 0{
                            for item in sourceArray{
                                if let noticeSourceItem = item {
                                    weakSelf.contentView.announceView.configWith(title:noticeSourceItem.body)
                                }
                            }
                        }else{
                            weakSelf.contentView.announceView.configWith(title: "")
                        }
                        
                    }else{
                        weakSelf.contentView.announceView.configWith(title: "")
                    }
                }else {
                    showToast(view: weakSelf.view, txt: "获取系统失败")
                }
                
            }else if type == "\(sendMessageType.contrabandLanguage.rawValue)" { //拉取违禁词
                guard let languageItem = model as? CRProhibitLanguageItem, let languageSourceArray = languageItem.source else {
                    return
                }
                
                weakSelf.prohibitLanguageArray = languageSourceArray
            }else if type == "\(sendMessageType.followOrder.rawValue)" {
                showToast(view: weakSelf.view, txt: success ? "跟单成功" : "\(msg)")
            }else if type == "\(sendMessageType.pullLotteryTicket.rawValue)" { //拉取开奖结果
                guard let lotterItem = model as? CRLotterDataItem else {
                    showToast(view: weakSelf.view, txt: "拉取开奖结果失败")
                    return
                }
                DispatchQueue.main.async {
                    weakSelf.contentView.lotteryBanner.lotterData =  lotterItem.source?.lotteryData
                }
                
            }else if type == "\(sendMessageType.historyRecord.rawValue)" { //聊天记录
                var messages = [CRMessage]()
                
                if let historyItems = model as? CRHistoryRecordItem {
                    messages = historyItems.source ?? []
                }
                
                weakSelf.handleHistoryRecord(historys:messages, isPrivateChat:false)
            }else if type == "\(sendMessageType.tutorPlan.rawValue)" { //导师计划
                guard let historyItems = model as? CRHistoryRecordItem else {
                    return
                }
                
                if let topControlelr = weakSelf.navigationController?.topViewController as? CRMasterPlanController {
                    topControlelr.updateMsgs(msgs: historyItems)
                }
                
            }else if type == "\(sendMessageType.historyBetShare.rawValue)" { //投注历史
                guard let historyModels = model as? [CRBetHistoryMsgModel] else {
                    return
                }
                
                weakSelf.lotteryHistoryController?.historyModels = historyModels
            }else if type == "\(sendMessageType.userInfo.rawValue)" {
                guard let userInfo = model as? CRUserInfoCenterItem,let sourceItem = userInfo.source else {
                    return
                }
                
                let finalUserInfo = userInfo.copy()
                
                //保存上次的输赢信息，个人信息结果
                if let _ = sourceItem.winLost {
                    weakSelf.userInfoCenter = userInfo.copy()
                }else {
                    finalUserInfo.source!.winLost = weakSelf.userInfoCenter?.source!.winLost
                }
                
                if let topControlelr = weakSelf.navigationController?.topViewController as? CRUserProfileInfoCtrl {
                    topControlelr.updateUserInfo(userItem: finalUserInfo)
                }else {
                    guard let drawerHeader = weakSelf.drawer?.subviewsBGView.header else {return}
                    drawerHeader.updateUserInfo(userItem: finalUserInfo)
                }
                
            }else if type == "\(sendMessageType.userIcon.rawValue)" {
                guard let userIcon = model as? CRSystemIconItem else {
                    return
                }
                
                weakSelf.userIconLists = (userIcon.source?.items) ?? []
                
                //设置个人信息页面
                if let topControlelr = weakSelf.navigationController?.topViewController as? CRUserProfileInfoCtrl {
                    topControlelr.chooseUserIconHandle(userListsItem: userIcon)
                }
                
                //打开抽屉时
                if let drawer = weakSelf.drawer {
                    drawer.subviewsBGView.userIconLists = weakSelf.userIconLists + [String]()
                }
                
            }else if type == "\(sendMessageType.submitUserInfo.rawValue)" { //提交个人信息修改
                guard let userIcon = model as? CRSystemIconItem else {
                    return
                }
                
                if let topControlelr = weakSelf.navigationController?.topViewController as? CRUserProfileInfoCtrl {
                    topControlelr.submitInfoHandle(userListsItem: userIcon)
                }
                
            }else if type == "\(sendMessageType.redPacket.rawValue)" { //红包
                
                guard let redPacgetItem = model as? CRRedPacketDataItem,let redPacketDataItem = redPacgetItem.source else {
                    return
                }
                // 获取当前msgUUID对应的模型
                let msgItems = weakSelf.contentView.messageArea.msgItems
                guard let originalMsg = CRChatRoomLogic.getMsgFromMessageItemsWith(msgUUID: redPacketDataItem.msgUUID, msgItems:msgItems) as? CRRedPacketMessage else {
                    return
                }
                
                originalMsg.payId = redPacketDataItem.payId
                originalMsg.remark = redPacketDataItem.remark
                print("payId = \(originalMsg.payId)")
                print()
            }else if  ["\(sendMessageType.receiveRedPacket.rawValue)","\(sendMessageType.receiveRedPacketNew.rawValue)"].contains(type) {
                // 处理抢红包 返回结果，更新红包状态
                guard let m = model as? CRReceiveRedPacketItem else {
                    return
                }
                
                guard let source = m.source else {
                    return
                }
                if success {
                    if let receiverRedpacketItem = model as? CRReceiveRedPacketItem{
                        if  receiverRedpacketItem.source?.pickData == nil{
                            //红包已被领取
                            let msgItems  = weakSelf.contentView.messageArea.msgItems
                            
                            guard let message = CRChatRoomLogic.getRedpacketWith(payId: source.payId, msgItems:msgItems) else {
                                return
                            }
                            message.status = (m.source?.status)!
                            weakSelf.redPacketDetailView.redPacketStatus = message.status
                            //MARK：房间配置
                            let chatRoomConfig = getChatRoomSystemConfigFromJson()
                            if chatRoomConfig?.source?.switch_red_info == "1"{
                                //显示详情
                                weakSelf.redPacketDetailView.isShowDetail = true
                            }else{
                                weakSelf.redPacketDetailView.isShowDetail = false
                            }
                            weakSelf.sendMsgReceiverDataRequest(message: message)
                        }else{
                            let msgItems  = weakSelf.contentView.messageArea.msgItems
                            
                            guard let message = CRChatRoomLogic.getRedpacketWith(payId: source.pickData?.payId ?? "", msgItems:msgItems) else {
                                return
                            }
                            message.status = (m.source?.status)!
                            weakSelf.redPacketDetailView.redPacketStatus = message.status
                            //MARK：房间配置
                            let chatRoomConfig = getChatRoomSystemConfigFromJson()
                            if chatRoomConfig?.source?.switch_red_info == "1"{
                                //显示详情
                                weakSelf.redPacketDetailView.isShowDetail = true
                            }else{
                                weakSelf.redPacketDetailView.isShowDetail = false
                            }
                            weakSelf.sendMsgReceiverDataRequest(message: message)
                        }
                    }
                    //红包详情
                    
                }else {
                    if let receiverRedpacketItem = model as? CRReceiveRedPacketItem{
                        //红包过期
                        var payId = source.pickData?.payId
                        if payId?.count == 0{
                            payId = source.payId
                        }
                        
                        let msgItems  = weakSelf.contentView.messageArea.msgItems
                        guard let message = CRChatRoomLogic.getRedpacketWith(payId: source.pickData?.payId ?? "",msgItems: msgItems) else {
                            return
                        }
                        message.status = receiverRedpacketItem.status
                        weakSelf.redPacketDetailView.redPacketStatus = message.status
                        //MARK：房间配置
                        let chatRoomConfig = getChatRoomSystemConfigFromJson()
                        if chatRoomConfig?.source?.switch_red_info == "1"{
                            //显示详情
                            weakSelf.redPacketDetailView.isShowDetail = true
                        }else{
                            weakSelf.redPacketDetailView.isShowDetail = false
                        }
                    }
                    
                    showToast(view: weakSelf.view, txt: msg)
                }
            }else if type == "\(sendMessageType.receiveDetail.rawValue)" {
                //领取红包详情
                guard let receiveDetailItem = model as? CRReceiveDetailItem else {
                    return
                }
                let receiveDetailString = receiveDetailItem.source?.parent?.toJSONString()
                //领取的红包的当前用户信息
                let receiveDetailCustomString = receiveDetailItem.source?.c?.toJSONString()
                let receiveDetailCustomItem = CRReceiveDetailCustom.deserialize(from: receiveDetailCustomString)
                
                let totalPeopleDetails = receiveDetailItem.source?.other
                if let redPacketParentItem = CRReceiveDetailParent.deserialize(from: receiveDetailString){
                    
                    weakSelf.redPacketDetailView.receiveRedPacketDetailItem = redPacketParentItem
                    weakSelf.redPacketDetailView.receiveRedPacketDetailCustomItem = receiveDetailCustomItem
                    weakSelf.redPacketDetailView.totalPeopleDetails = totalPeopleDetails
                    //MARK：房间配置
                    let chatRoomConfig = getChatRoomSystemConfigFromJson()
                    if chatRoomConfig?.source?.switch_red_info == "1"{
                        //显示详情
                        weakSelf.redPacketDetailView.isShowDetail = true
                    }else{
                        weakSelf.redPacketDetailView.isShowDetail = false
                    }
                    if !weakSelf.redPacketDetailView.isShow{
                        UIApplication.shared.keyWindow?.addSubview(weakSelf.redPacketDetailView)
                    }
                    weakSelf.redPacketDetailView.isShow = true
                }
            }else if type == "\(sendMessageType.roomOnLineList.rawValue)" { //在线用户
                guard let wraper = model as? CROnLineListWraper,let list = wraper.source else {
                    return
                }
                
                weakSelf.drawer?.subviewsBGView.userIconLists = weakSelf.userIconLists
                weakSelf.drawer?.subviewsBGView.updateInfo(list: list, type: .DrawerSectionOnlineUser)
                
            }else if type == "\(sendMessageType.updatePersonnalAvatar.rawValue)" { //修改用户头像
                print("")
            }else if type == "\(sendMessageType.managerList.rawValue)" { //管理员列表
                guard let wraper = model as? CROnLineListWraper,let list = wraper.source else {
                    return
                }
                weakSelf.drawer?.subviewsBGView.userIconLists = weakSelf.userIconLists
                weakSelf.drawer?.subviewsBGView.updateInfo(list: list,type: .DrawerSectionManager)
            }else if type == "\(sendMessageType.privateChatInit.rawValue)" { //私聊列表，和初始化私聊
                weakSelf.handlePrivateChatInitWith(model: model)
            }else if ["\(sendMessageType.privateChatSend.rawValue)","\(sendMessageType.privateHistory.rawValue)"].contains(type ?? "") { //私聊发送消息相关
                weakSelf.handlePrivateChatSend(model:model,privateType: privateType)
            }else if type == "\(sendMessageType.currentBalance.rawValue)" { //用户余额
                guard let balanceModel = model as? CRBalanceModel else {
                    return
                }
                
                weakSelf.drawer?.subviewsBGView.header.updateBalance(value: balanceModel.money)
            }else if type == "\(sendMessageType.winPrizeList.rawValue)"{
                
                guard let prizeSource = model as? CRWinningSource,let winPrizeLists = prizeSource.winningList else{
                    return
                }
                //获取系统默认图像前十
                if winPrizeLists.count == 0{
                    showToast(view: weakSelf.view, txt: "暂无排行榜数据")
                    return
                }
                weakSelf.sendMsgGetUserAvatarList(userId: winPrizeLists[0].id)
                
                var type = "" //2中奖榜单，4 盈利榜单
                for (index,item) in winPrizeLists.enumerated(){
                    if index == 0 {
                        type = item.prizeType
                    }
                    
                    if weakSelf.userIconLists.count > winPrizeLists.count{
                        item.imageIconUrl = weakSelf.userIconLists[index]
                    }
                }
                
                if type == "2" {
                    weakSelf.winPrizeLists?.removeAll()
                    weakSelf.winPrizeLists = winPrizeLists
                }else if type == "4" {
                    weakSelf.profitPrizeLists?.removeAll()
                    weakSelf.profitPrizeLists = winPrizeLists
                }
                
            }else if type == "\(sendMessageType.signIn.rawValue)" {
                guard let signModel = model as? CRSignModel else {
                    return
                }
                
                weakSelf.handleSignin(model: signModel)
            }else if type == "\(sendMessageType.prohibit.rawValue)" { //取消禁言，禁言
                
            }else if type == "\(sendMessageType.retract.rawValue)" { //撤回
                
            }else if type == "\(sendMessageType.appealBand.rawValue)" { //禁言申述
                
            }
        })
    }
    
    
    //MARK: 收到消息后设置其 msgUUID，msgId
    func setMsgId(message:CRMessage,response:String) {
        class InMessage: HandyJSON {
            var source: SourceData?
            required init() {}
        }
        
        class SourceData: HandyJSON {
            var msgId = ""
            var userMessageId = ""
            var msgUUID = ""
            required init() {}
        }
        
        if let data = InMessage.deserialize(from: response),let source = data.source {
            message.msgUUID = source.msgUUID
            message.msgId = source.msgId
        }
    }
    
    //MARK: 根据消息，在cell上更新视图
    func updateMsgOnCell(msgUUID:String,sendStatus:CRMessageSendState) {
        if !msgUUID.isEmpty {
            guard let visibleIndexes = contentView.messageArea.msgTable.indexPathsForVisibleRows else {return}
            
            for indexPath in visibleIndexes.reversed() {
                let index = indexPath.row
                let msg = contentView.messageArea.msgItems[index]
                if msg.msgUUID == msgUUID {
                    msg.sendState = sendStatus
                    if let cell = contentView.messageArea.msgTable.cellForRow(at: indexPath) as? CRBaseChatCell {
                        cell.updateMsgStatus(message: msg)
                    }
                    
                    break
                }
            }
        }
    }
    
    //中奖榜单弹窗
    func showWinAndProfitListPop() {
        var winOn = false
        var profit = false
        
        let status = switcnWinnerAndProfitListStatus()
        if status == 1 {
            winOn = true
            profit = true
        }else if status == 2 {
            winOn = true
            profit = false
        }else if status == 3 {
            winOn = false
            profit = true
        }else if status == 0 { 
            winOn = false
            profit = false
        }
        
        winPrizeView = nil
        winPrizeView =  CRWinPrizeListView.init(frame: UIScreen.main.bounds, winningShow: winOn, profitShow: profit)
        
        if let view = winPrizeView {
            UIApplication.shared.keyWindow?.addSubview(view)
        }
    }

}


//MARK: - 发送消息 && 发送会显示在 页面上的消息
extension CRChatController {
    //MARK: 发送分享注单消息
    func sendShareBetMessage(model:CRShareBetModel) {
        let message = CRShareBetMessageItem()
        message.shareBetModel = model
        message.ownerType = .TypeSelf
        message.messageType = .ShareBet
        message.avatar = self.userIconUrl
        message.levelIcon = self.userLevelIcon
        message.levelName = self.userLevelName
        message.msgUUID = model.msgUUID
        
        if let betInfos = model.betInfos {
            var height:CGFloat = 0
            if betInfos.count == 1 {
                height = CRShareBetAllView.singleH
            }else if betInfos.count >= 2 {
                height = CGFloat(2) *  CRShareBetAllView.singleH + CRShareBetAllView.followAllH
            }
            
            let messageFrame  = CRMessageFrame()
            messageFrame.contentSize = CGSize.init(width: 300, height: height)
            messageFrame.height = Float(height) + Float(50)
            
            message.messageFrame = messageFrame
        }
        
        //会员投注命中率
        message.winRate = userWinRate
        sendMessage(message: message)
    }
    
    //MARK: 发送图片消息
    func sendImageMessage(image:UIImage){
        let imageDate = (image.pngData() != nil) ? image.pngData() : image.jpegData(compressionQuality: 0.5)
        let imageName = String.init(format:"%f.jpg",Date().timeIntervalSince1970)
        let imgPath = FileManager.pathUserChatImage(imageName: imageName)
        FileManager.default.createFile(atPath: imgPath, contents: imageDate, attributes: nil)
        let message = CRImageMessage()
        
        message.ownerType = .TypeSelf
        //图片名字
        message.imagePath = imageName;
        //图片迟寸
        message.imageSize = image.size;
        message.msgUUID = CRCommon.generateMsgID()
        //发送过程中状态
        message.sendState = .progress
        //发送消息
        sendMessage(message: message)
        //上传图片到服务器
        uploadImageToServerRequest(messageItem: message)
    }
    
    //收藏图片发送图片消息  GIF  此处应拿到图片类型 GIF类型走网络图片 非GIF 走本地静态图片
    func sendGifImageMessage(imageUrl:String,fileCode:String = ""){
        let message = CRImageMessage()
        message.msgUUID = CRCommon.generateMsgID()
        message.ownerType = .TypeSelf
        //图片在服务器上的地址
        message.imageUrl = imageUrl
        message.messageType = .Image
        //发送过程中状态
        message.sendState = .progress
        //发送消息
        sendMessage(message: message)
        
        let sendImageItem = CRUploadImageItem()
        sendImageItem.msgUUID = message.msgUUID
        sendImageItem.fileCode = fileCode
        sendImageItem.fileString = ""
        //推送发送图片消息
        sendMsgImageMessage(message: sendImageItem, imageName: "", type: 2)
    }
    
    //MARK: 聊天类 发送消息总入口, 将消息显示到页面上
    func sendMessage(message:CRMessage){
        message.ownerType = .TypeSelf
        message.roomId = self.msgModel.roomId
        //当前房间
        message.stationId = self.msgModel.stationId
        //会员icon
        message.avatar = userIconUrl
        //会员昵称
        message.nickName = userNickname
        message.account = userAccount
        message.nativeAccount = userNativeAccount
        //会员等级图标
        message.levelIcon = userLevelIcon
        //会员等级名称
        message.levelName = userLevelName
        
        message.userId = CRDefaults.getUserID()
        
        //当前发送时间
        let currentTime = String().getCurrentTimeString()
        
        message.sentTime = currentTime
        
        addToShowMessage(message: message)
    }
    
    //MARK:分享今日盈亏
    func shareGainsAndLossesForToday(gainsLossesItem:CRUserInfoCenterSourceWinLostItem){
        let msgUUID = CRCommon.generateMsgID()
        
        var code = ""
        var roomId = ""
        
        if isPrivateChat {
            code = "\(sendMessageType.privateChatSend.rawValue)"
            roomId = chat_privateRoomId
        }else {
            code = "\(sendMessageType.text.rawValue)"
            roomId = chat_roomId
        }
        
        var record:[String:AnyObject] = [:]
        record["yingli"] = gainsLossesItem.allWinAmount as AnyObject
        record["touzhu"] = gainsLossesItem.allBetAmount as AnyObject
        record["yingkui"] = gainsLossesItem.yingkuiAmount as AnyObject
        
        var parmaters = ["code": code,
                         "msgUUID": msgUUID,
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "speakType": 2,
                         "type":9,
                         "roomId":roomId,
                         "source":"app",
                         "stationId":chat_stationId,
                         "record":record,
                         ] as [String : Any]
        
        if isPrivateChat {
            parmaters["passiveUserId"] = chat_passiveUserId
            parmaters["stationId"] = chat_stationId
            parmaters["msgType"] = "4" //1 文本， 2 图片，4 分享今日输赢，5 分享注单
            parmaters["type"] = 1
        }
        
        let todayGainsLossesItem = CRShareGainsLossesItem()
        todayGainsLossesItem.msgUUID = msgUUID
        todayGainsLossesItem.yingkui = gainsLossesItem.yingkuiAmount
        todayGainsLossesItem.yingli = gainsLossesItem.allWinAmount
        todayGainsLossesItem.touzhu = gainsLossesItem.allBetAmount
        todayGainsLossesItem.messageType = .ShareGainsLosses
        todayGainsLossesItem.ownerType = .TypeSelf
        
        sendMessage(message: todayGainsLossesItem)
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: parmaters)
    }
    
    //MARK: 发送文本消息 发送消息的类型 1.快捷发言 2.自由发言 3.发送表情 4.投注消息 5.@其它用户的消息 6.快捷图片
    func sendTxtMsgRequest(text: String,speakType:String = "2") {
        let msgUUID = CRCommon.generateMsgID()
        
        let messageItem = CRTextMessageItem()
        messageItem.text = text
        messageItem.messageType = .Text
        messageItem.speakType = "2"
        messageItem.avatar  = self.userIconUrl
        messageItem.userId = CRDefaults.getUserID()
        messageItem.msgUUID = msgUUID
        
        var paramters = [String: String]()
        var code = ""
        var roomId = ""
        
        if isPrivateChat {
            code = "\(sendMessageType.privateChatSend.rawValue)"
            roomId = chat_privateRoomId
        }else {
            code = CRUrl.shareInstance().SEND_MSG_TXT
            roomId = chat_roomId
        }
        
        paramters = ["stauts":"",
                     "msgUUID":msgUUID,
                     "roomId":roomId,
                     "record":messageItem.text ?? "",
                     "type":"1",
                     "speakType":messageItem.speakType,
                     "ATaccount":messageItem.userId,
                     "option1":"",
                     "channelId":"ios",
                     "userId":CRDefaults.getUserID(),
                     "source":"app",
                     "code":code,
                     //1代表当前帐号是当前所在的代理房间的主人 0 不是
            "agentRoomHost":agentRoomHost]
        
        if isPrivateChat {
            paramters["passiveUserId"] = chat_passiveUserId
            paramters["stationId"] = chat_stationId
            paramters["msgType"] = "1" //1 文本， 2 图片，4 分享今日输赢，5 分享注单
            paramters["roomId"] = roomId
            paramters["type"] = "1"
        }
        
        sendMessage(message: messageItem)
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: paramters)
    }
    
    //MARK:发红包
    func sendMsgRedPackageMessageRequest(message:CRMessage) {
        guard let redPacgetItem = message as? CRRedPacketMessage else {
            showToast(view: self.view, txt: "发送红包失败")
            return
        }
        
        let msgUUID = CRCommon.generateMsgID()
        
        var parameter = ["roomId":chat_roomId,
                         "agentRoomHost":agentRoomHost,
                         "amount":redPacgetItem.amount,
                         "count":redPacgetItem.count,
                         "remark":redPacgetItem.remark,
                         "stationId":chat_stationId,
                         "packetId":"",
                         "msgUUID":msgUUID,
                         "code":redPacgetItem.code,
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app"] as [String : Any]
        
        if isPrivateChat {
            parameter["passiveUserId"] = chat_passiveUserId
            parameter["roomId"] = chat_privateRoomId
        }
        
        redPacgetItem.msgUUID = msgUUID
        redPacgetItem.messageType = .RedPacket
        redPacgetItem.msgType = "\(CRMessageType.RedPacket.rawValue)"
        
        sendMessage(message: redPacgetItem)
        sendSysMessage(msgType: CR_USER_S, code: "\(sendMessageType.redPacket.rawValue)", parameters: parameter)
    }
    
    //MARK: 发送图片,语音
    //发送图片，语音
    func sendMsgImageMessage(message:CRUploadImageItem,imageName:String,type:Int,duration:Int = 0) {
        
        let msgUUID = message.msgUUID
        
        chat_userId = CRDefaults.getUserID()
        
        let stationId = self.msgModel.stationId
        var roomId = ""
        var code = ""
        
        if isPrivateChat {
            code = "\(sendMessageType.privateChatSend.rawValue)"
            roomId = chat_privateRoomId
        }else {
            code = "\(sendMessageType.image.rawValue)"
            roomId = chat_roomId
        }
        
        //type图片消息类型 2--图片 视频--10 语音--11
        var paramters:[String:Any] = [:]
        if type == 2 {
            paramters = ["stauts":"",
                         "roomId":chat_roomId,
                         "agentRoomHost":agentRoomHost,
                         "type":type,
                         "remark":"",
                         "option1":"",
                         "speakType":"6",
                         "channelId":"ios",
                         "duration":duration,
                         "userId":chat_userId,
                         "source":"app",
                         "code": code,
                         "record":message.fileCode,
                         "stationId":stationId,
                         "fileString":message.fileString,
                         "key":"",
                         "msgUUID":msgUUID,
                         "name":imageName,
                         "fileType":"image/jpeg",
                         "picMsg":["code": "\(sendMessageType.image.rawValue)",
                            "roomId":chat_roomId,
                            "userId":chat_userId,
                            "size":"",
                            "duration":duration,
                            "latocalPh":"",
                            "thumbPath":"",
                            "source":"app",
                            "type":"2",
                            "msgUUID":message.msgUUID]] as [String : Any]
        }else if type == 11{
            paramters = ["stauts":"",
                         "roomId":chat_roomId,
                         "agentRoomHost":agentRoomHost,
                         "type":type,
                         "remark":"",
                         "option1":"",
                         "speakType":"6",
                         "channelId":"ios",
                         "duration":duration,
                         "userId":chat_userId,
                         "source":"app",
                         "code": code,
                         "record":message.fileCode,
                         "stationId":stationId,
                         "fileString":message.fileString,
                         "key":"",
                         "msgUUID":msgUUID,
                         "name":imageName,
                         "fileType":"audio/wav",
                         "audioMsg":["code": "\(sendMessageType.image.rawValue)",
                            "roomId":chat_roomId,
                            "userId":chat_userId,
                            "size":"",
                            "duration":duration,
                            "latocalPh":"",
                            "thumbPath":"",
                            "source":"app",
                            "type":"2",
                            "msgUUID":msgUUID]] as [String : Any]
        }
        
        if isPrivateChat {
            paramters["passiveUserId"] = chat_passiveUserId
            paramters["stationId"] = chat_stationId
            paramters["msgType"] = "2" //1 文本， 2 图片，4 分享今日输赢，5 分享注单
            paramters["roomId"] = roomId
            paramters["type"] = "1"
        }
        
        sendSysMessage(msgType: CR_USER_S, code: code, parameters: paramters)
    }
}

extension CRChatController {
    //MARK:从服务器下载文件
    func downloadFileFromServer(fileCode:String,voiceMsgItem:CRVoiceMessageItem){
        
        let downString = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE + "?fileId=" + "\(fileCode)" + "&contentType=audio/amr"
        //        let downString = "http://testfile.yibochat.com/" + CRUrl.shareInstance().URL_READ_FILE + "?fileId=" + "\(fileCode)"
        
        //下载路径地址
        let downLoadString = String(format:"%@",downString)
        print(downLoadString)
        
        let destination: DownloadRequest.DownloadFileDestination = {  _, _ in
            //后台下载AMR
            var amrFile = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            amrFile.appendPathComponent("downLoadAmr.amr")
            return (amrFile, [.removePreviousFile])
        }
        Alamofire.download(downString, to: destination).response { response in
            print(response.request)
            print(response.response)
            print(response.destinationURL)
            
            var  destinationPath = response.destinationURL?.path ?? ""
            if (destinationPath.contains("file:///")){
                destinationPath =  destinationPath.subString(start: 7)
            }
            
            if FileManager.default.fileExists(atPath: destinationPath){
                //音频文件名称
                let fileName = String(format:"%@.wav",fileCode)
                //                //下载录音文件的地址转码后的地
                let path = FileManager.pathUserDonwloadVoice(voiceName: fileName)
                //
                //音频名称
                voiceMsgItem.recFileName = fileName
                print("voiceUUUID:\(voiceMsgItem.msgUUID),path:\(voiceMsgItem.path)")
                //已经存在该音频文件
                //                if  FileManager.directoryIsExists(path: path){
                //                    return
                //                }
                
                //                PublicConversionModel.convertAMRtoWAV(destinationPath, savePath: path)
                
                TLAudioRecorder.convertAMRtoWAV(destinationPath, savePath: path)
                //音频文件路t
                voiceMsgItem.path = path
                
                try? FileManager.default.moveItem(atPath: destinationPath, toPath: path)
                
                
            }
        }
    }
}

//MARK: YJ不同于YB
extension CRChatController {
    
    ///格式化分享注单的消息model--投注页面
    func formatBetShareMsgModelYJ(models:[OrderDataInfo],shareOrders:[CRShareOrder]) {
        let shareBetModel = CRShareBetModel()
        
        shareBetModel.roomId = self.msgModel.roomId + ""
        shareBetModel.stationId = chat_stationId + ""
        shareBetModel.code = CRUrl.shareInstance().CODE_SHARE_BET_MSG
        shareBetModel.source = "app"
        shareBetModel.userId = CRDefaults.getUserID()
        shareBetModel.speakType = "4"
        shareBetModel.userType = "\(userType)"
        
        var betInfos = [CRShareBetInfo]()
        
        for model in models {
            let shareBetInfo = CRShareBetInfo()
            shareBetInfo.lottery_amount = "\(model.money)"
            shareBetInfo.lottery_content = model.numbers
            
            shareBetInfo.lottery_play =  model.subPlayName + (!(model.oddsName.isEmpty || model.oddsName == model.subPlayName) ? "-\(model.oddsName)" : "")
            
            shareBetInfo.lottery_qihao = self.currentQihao + ""
            shareBetInfo.lottery_type = self.cpName + ""
            shareBetInfo.lottery_zhushu = "\(model.zhushu)"
            shareBetInfo.lotCode = self.cpBianHao + ""
            shareBetInfo.version = self.cpVersion + ""
            shareBetInfo.ago = self.lotteryData.ago + 0
            shareBetInfo.lotIcon = self.lotteryData.lotteryIcon
            shareBetInfo.icon = self.lotteryData.lotteryIcon
            shareBetInfo.model = model.mode + 0
            
            betInfos.append(shareBetInfo)
        }
        
        shareBetModel.betInfos = betInfos
        shareBetModel.orders = shareOrders
        shareBetModel.multiBet = "1"
        
        self.shareBetModel = shareBetModel
    }
    
    ///格式化分享注单的消息model--历史注单页面
    func sendMsgFormatAllBetShareMsgModelInHistoryYJ(models:[CRBetHistoryMsgModel]) {
        let shareBetModel = CRShareBetModel()
        
        var orders = [CRShareOrder]() //金额，注单id
        var betInfos = [CRShareBetInfo]() //用于显示注单信息
        
        for model in models {
            let shareBetInfo = CRShareBetInfo()
            shareBetInfo.lottery_amount = "\(model.buyMoney)"
            shareBetInfo.lottery_content = model.haoMa
            shareBetInfo.lottery_play = model.playName
            shareBetInfo.lottery_qihao = model.qiHao
            shareBetInfo.lottery_type = model.playType
            shareBetInfo.lottery_zhushu = "\(model.buyZhuShu)"
            shareBetInfo.lotCode = model.lotCode
            shareBetInfo.version = "\(model.lotVersion)"
            shareBetInfo.lotIcon = model.icon
            shareBetInfo.icon = model.icon
            shareBetInfo.model = model.model + 0
            betInfos.append(shareBetInfo)
            
            let shareOrder = CRShareOrder()
            shareOrder.orderId = model.orderId
            shareOrder.betMoney = "\(model.buyMoney)"
            orders.append(shareOrder)
        }
        
        shareBetModel.roomId = self.msgModel.roomId + ""
        shareBetModel.stationId = chat_stationId + ""
        shareBetModel.code = CRUrl.shareInstance().CODE_SHARE_BET_MSG
        shareBetModel.source = "app"
        shareBetModel.userId = CRDefaults.getUserID()
        shareBetModel.speakType = "4"
        shareBetModel.userType = "\(userType)"
        shareBetModel.orders = orders
        shareBetModel.multiBet = "1"
        shareBetModel.betInfos = betInfos
        
        self.shareBetModel = shareBetModel
    }
}
