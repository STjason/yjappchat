 //
//  CRUserProfileInfoCtrl.swift
//  gameplay
//
//  Created by Gallen on 2019/10/3.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRUserProfileInfoCtrl: UIViewController {
    
    var updateAvatarListHandler:(() -> Void)? //获取系统默认图片列表
    var updateUserInfoHandler:(() -> Void)? //获取昵称和头像
    var updateEditInfoHandler:(() -> Void)? //提交用户修改的个人信息
    var updateBetInfoHandler:(() -> Void)? //刷新用户今天投注输赢数据
    var shareProfitlossHandler:(() -> Void)? //分享今日盈亏
    var editAvatarWithLocalHandler:(() -> Void)? //本地图片上传，修改头像
    /**分享今日输赢*/
    var shareGainsAndLossesForTodayHandler:((_ gainsLossesItem : CRUserInfoCenterSourceWinLostItem)-> Void)?
    
    lazy var systemUserIconListView:CRSystemUserIconView = {
        let view = CRSystemUserIconView.init(frame: UIScreen.main.bounds)
        return view
    }()
    //用户信息回调
//    var userInfo:(()->())?
    
    var reloadWinLostCount = 0 //获取 R7038有时 winLost字段为空，这时要再次获取。改字段表示第几次重新获取
    
    var stationId:String = ""
    //用户昵称
    var nickName:String = ""
    //用户修改图像
    var avatar:String = ""
    //修改后的昵称
    var modifyNickName:String = ""
    //修改后的icon
    var modifyAvatar:String = ""
    
    var roomId:String = ""
    //
    var gainsLossesItem:CRUserInfoCenterSourceWinLostItem?
    
    var systemUserIcons:[String] = []
    //用户图像选择列表开始是隐藏的
    var isHideCollectionView:Bool = true
    /**是否修改昵称*/
    var isModifyNickname:Bool = false
    var editedLocalAvatar = false//是否选择本地图片，来更改头像
    
    var userInfoArray:[CRUserInfoItem] = []
    /**修改用户资料提交的回调*/
    var commitUserInfoClourse:((_ userInfoItem:CRUserInfoCenterSource)->())?
    
//    var titleNames:[String] = ["今日中奖:",
//                               "今日盈亏:",
//                               "今日总投注:",
//                               "累计充值:",
//                               "今日充值:",
//                               "打码量:",
//                               "消息发送提示音",
//                               "消息接收提示音",
//                               "进房通知",
//                               "消息推送通知"]
    var titleNames:[String] = ["消息发送提示音",
                               "消息接收提示音",
                               "进房通知",
                               "消息推送通知"]
    var titleNamesOn:[String] = []
    
    lazy var userIconImageV:UIImageView = {
        let userIcon = UIImageView()
        userIcon.layer.cornerRadius = 60 * 0.5
        userIcon.layer.masksToBounds = true
        userIcon.isUserInteractionEnabled = true
        userIcon.kf.setImage(with: URL(string: avatar), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
      
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseUserIcon))
        userIcon.addGestureRecognizer(tap)
        return userIcon
    }()
    
    lazy var localAvarButton: UIButton = {
        let view = UIButton()
        view.setTitle("选择本地头像", for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        view.setTitleColor(UIColor.white, for: .normal)
        view.layer.cornerRadius = 3
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 0.5
        view.addTarget(self, action: #selector(tapLocalAvatarButton), for: .touchUpInside)
        view.isHidden = !switchLocalAvar()
        return view
    }()
    
    lazy var userProfileBackView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 200))
        view.backgroundColor = UIColor.colorWithHexString("#545c64")
        return view
    }()
    
    lazy var userInfoTableView:UITableView = {
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - CGFloat(KNavHeight)))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        return tableView
    }()
    //用户昵称
    lazy var systemDefalutNickNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = UIColor.white
//        label.textColor = UIColor.colorWithHexString("#666")
        label.text = nickName
        return label
    }()
    //修改昵称
    lazy var modifyNickNameButton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ali_modify_icon"), for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(modifyNickNameClick), for: .touchUpInside)
        return button
    }()
    //修改昵称的输入框
    lazy var modifyNickTextField:UITextField = {
        let textField = UITextField()
        textField.layer.borderWidth = 0.5
        textField.isHidden = true
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.tintColor = UIColor.white
        textField.textColor = UIColor.white
        textField.textAlignment = .center
        textField.delegate = self
        textField.addTarget(self, action: #selector(modifyNickName(textField:)), for: UIControl.Event.editingChanged)
        let leftView = UIView(frame: CGRect(x: 10, y: 0, width: 10, height: 10))
        textField.leftView = leftView
        textField.leftViewMode = UITextField.ViewMode.always
        return textField
    }()
    //提交修改
    lazy var commitButton:UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.backgroundColor = UIColor.colorWithRGB(r: 119, g: 212, b: 114, alpha: 1.0)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.setTitle("确认修改", for: .normal)
        button.addTarget(self, action: #selector(commitClick), for: .touchUpInside)
        button.addTarget(self, action: #selector(buttonBackGroundHighlighted), for: .touchDown)
        return button
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "个人设置"
        //设置UI
        setupUI()
        //根据开关整理数据源
        setupData()
    }
    func addLayoutContranst(){
        
        userInfoTableView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(0)
            make.left.right.bottom.equalTo(view)
        }
        //用户图像
        userIconImageV.snp.makeConstraints { (make) in
            make.top.equalTo(userProfileBackView).offset(40)
            make.centerX.equalTo(userProfileBackView.snp.centerX)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
        // 用户昵称
        systemDefalutNickNameLabel.snp.makeConstraints { (make) in

            make.top.equalTo(userIconImageV.snp.bottom).offset(8)
            make.centerX.equalTo(userIconImageV.snp.centerX)
        }
        
        localAvarButton.snp.makeConstraints { (make) in
            make.top.equalTo(systemDefalutNickNameLabel.snp.bottom).offset(20)
            make.centerX.equalTo(systemDefalutNickNameLabel.snp.centerX)
            make.height.equalTo(30)
            make.width.equalTo(120)
        }
        
        modifyNickNameButton.snp.makeConstraints { (make) in
            make.left.equalTo(systemDefalutNickNameLabel.snp.right).offset(5)
            make.centerY.equalTo(systemDefalutNickNameLabel.snp.centerY)
        }
        modifyNickTextField.snp.makeConstraints { (make) in
            make.top.equalTo(userIconImageV.snp.bottom).offset(8)
            make.centerX.equalTo(userIconImageV.snp.centerX)
            make.width.equalTo(150)
            make.height.equalTo(30)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let navBackgroudImage = UIImage(named: "chatNavBar")
        
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroudImage?.getNavImageWithImage(image: navBackgroudImage!), for: .default)
        
        if let view = getSemicycleButton() {
            view.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
    }
    
    func updateUserInfo(userItem:CRUserInfoCenterItem?) {
        if  let userInfoItem =  userItem {
            guard let sourceItem = userInfoItem.source else{
                showToast(view: self.view, txt: "刷新失败，请重试")
                return
            }
            
            //当前用户的图像
            let userIconString = sourceItem.avatar
            //user's icon
            var userNickname = sourceItem.account
            if sourceItem.nickName != "" {
                userNickname = sourceItem.nickName
            }
            
            userIconImageV.kf.setImage(with: URL(string: userIconString), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
            //user's nickName
            systemDefalutNickNameLabel.text = userNickname
            guard  let userInfoDetailItem = sourceItem.winLost else{
                reloadWinLostCount = reloadWinLostCount + 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.updateUserInfoHandler?()
                    }
                return
            }
            
            
            gainsLossesItem = userInfoDetailItem
            userInfoArray.removeAll()

            for on in 0..<titleNamesOn.count{
                let userItem = CRUserInfoItem()
                userItem.titleName = titleNamesOn[on]
                if userItem.titleName == "今日中奖:"{
                    //今日总盈亏 净盈利 = 中奖金额 + 投注反水 - 投注金额
                    //今日中奖
                    userItem.money = userInfoDetailItem.allWinAmount
                    userItem.isSwitchOn = false
                }else if  userItem.titleName == "今日盈亏:"{
                    //今日盈亏
                    userItem.money = userInfoDetailItem.yingkuiAmount
                    userItem.isSwitchOn = false
                    
                }else if  userItem.titleName == "今日总投注:"{
                    //今日总投注
                    userItem.money = userInfoDetailItem.allBetAmount
                    userItem.isSwitchOn = false
                }else if userItem.titleName == "累计充值:"{
                    userItem.money = userInfoDetailItem.sumDepost
                }else if userItem.titleName == "今日充值:"{
                    userItem.money = userInfoDetailItem.sumDepostToday
                }else if userItem.titleName == "打码量:"{
                    userItem.money = userInfoDetailItem.betNum
                }else {
                    userItem.isSwitchOn = true
                    userItem.money = 0
                }
                userInfoArray.append(userItem)
            }
            
        }
        
        userInfoTableView.reloadData()
    }
    
    //获取当前用户图像和昵称
    
 /**
    func getUserIconAndnickNameDataFromServer(loadTextStr:String){
        let parmaters = ["code":"R7038",
                         "msgUUID":CRCommon.generateMsgID(),
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app",
                         "roomId":roomId]
        
//        let data = try? JSONSerialization.data(withJSONObject: parmaters, options: .prettyPrinted)
//        let strJson = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//        print(strJson)
        
        CRNetManager.shareInstance().sendMessage(paramters: parmaters, method: .post, requestType: .userInfo) {[weak self] (isSuccess, jsonString, userItem) in
            if isSuccess{
                if let weakSelf = self{
                    weakSelf.showDialog(view: weakSelf.view, loadText: loadTextStr)
                    
                    if  let userInfoItem =  userItem as? CRUserInfoCenterItem{
                    
                        guard let sourceItem = userInfoItem.source else{
                            showToast(view: weakSelf.view, txt: "刷新失败，请重试")
                            return
                        }
                       
                        //当前用户的图像
                        let userIconString = sourceItem.avatar
                        //user's icon
                        var userNickname = sourceItem.account
                        if sourceItem.nickName != "" {
                           userNickname = sourceItem.nickName
                        }
                        weakSelf.userIconImageV.kf.setImage(with: URL(string: userIconString), placeholder: UIImage(named: "chatroom_default"), options: nil, progressBlock: nil, completionHandler: nil)
                        //user's nickName
                        weakSelf.systemDefalutNickNameLabel.text = userNickname
                        guard  let userInfoDetailItem = sourceItem.winLost else{
                            return
                        }
                        
                         weakSelf.userInfoArray.removeAll()
                        for on in 0..<weakSelf.titleNamesOn.count{
                            let userItem = CRUserInfoItem()
                            userItem.titleName = weakSelf.titleNamesOn[on]
                            if userItem.titleName == "今日中奖:"{
                                //今日总盈亏 净盈利 = 中奖金额 + 投注反水 - 投注金额
                                //今日中奖
                                userItem.money = userInfoDetailItem.allWinAmount
                                userItem.isSwitchOn = false
                            }else if  userItem.titleName == "今日盈亏:"{
                                //今日盈亏
                                userItem.money = userInfoDetailItem.yingkuiAmount
                                userItem.isSwitchOn = false
                                
                            }else if  userItem.titleName == "今日总投注:"{
                                //今日总投注
                                userItem.money = userInfoDetailItem.allBetAmount
                                userItem.isSwitchOn = false
                            }else if userItem.titleName == "累计充值:"{
                                userItem.money = userInfoDetailItem.sumDepost
                            }else if userItem.titleName == "今日充值:"{
                                userItem.money = userInfoDetailItem.sumDepostToday
                            }else if userItem.titleName == "打码量:"{
                                userItem.money = userInfoDetailItem.betNum
                            }else {
                                userItem.isSwitchOn = true
                                userItem.money = 0
                            }
                            weakSelf.userInfoArray.append(userItem)
                        }
                        
                    }
                    weakSelf.userInfoTableView.reloadData()
                }
            }
        }
    }
*/
 
 }

//MARK:设置UI
extension CRUserProfileInfoCtrl{
    func setupUI(){
        view.backgroundColor = UIColor.white
        view.addSubview(userInfoTableView)
        userProfileBackView.addSubview(userIconImageV)
        userProfileBackView.addSubview(systemDefalutNickNameLabel)
        userProfileBackView.addSubview(localAvarButton)
        userProfileBackView.addSubview(modifyNickTextField)
        userProfileBackView.addSubview(modifyNickNameButton)
        userInfoTableView.tableHeaderView = userProfileBackView
        //添加约束
        addLayoutContranst()
        userInfoTableView.register(CRUserInfoDetailCell.self, forCellReuseIdentifier: "CRUserInfoDetailCell")
        if let chatroomConfig = getChatRoomSystemConfigFromJson(){
            //
            if  chatroomConfig.source?.switch_user_name_show == "1"{
                modifyNickNameButton.isHidden = false
            }else{
                modifyNickNameButton.isHidden = true
            }
        }else{
            modifyNickNameButton.isHidden = false
        }
    }
    //MARK:设置数据源
    func setupData(){
        let config = getChatRoomSystemConfigFromJson()
        guard  let source = config?.source else {
            return
        }
        for (index,value) in titleNames.enumerated() {
            //
            if value == "今日中奖:"{
                titleNamesOn.append(value)
            }else if value == "今日盈亏:"{
                if  source.switch_yingkui_show ==  "1"{
                    //显示今日盈亏
                    titleNamesOn.append(value)
                }
            }else if value == "今日总投注:"{
                titleNamesOn.append(value)
            }else if value == "累计充值:"{
                titleNamesOn.append(value)
            }else if value == "今日充值:"{
                titleNamesOn.append(value)
            }else if value == "打码量:"{
                if source.switch_bet_num_show == "1"{
                    titleNamesOn.append(value)
                }
            }else if value == "消息发送提示音"{
//                if source.switch_room_voice == "1"{
                    titleNamesOn.append(value)
//                }
            }else if value == "消息接收提示音"{
//                if source.switch_msg_receive_notify == "1"{
                    titleNamesOn.append(value)
//                }
            }else if value == "进房通知"{
//                if source.switch_room_tips_show == "1"{
//
//                }
                titleNamesOn.append(value)
            }else if value == "消息推送通知"{
//                if source.switch_new_msg_notification == "1"{
//
//                }
                titleNamesOn.append(value)
            }
        }
        
//         getUserIconAndnickNameDataFromServer(loadTextStr: "正在加载...")
        updateUserInfoHandler?()
    }
}
 //MARK:- 事件响应
extension CRUserProfileInfoCtrl{
    @objc func tapLocalAvatarButton() {
        tapOneTapGestureRecognizerClick()
    }
    
    /// 分享今日盈亏
    @objc private func shareProfitloss() {
        shareProfitlossHandler!()
    }
    
    //系统默认图像列表
    @objc private func chooseUserIcon(){
        updateAvatarListHandler?()
    }
    //MARK:选择图片
    func chooseUserIconHandle(userListsItem:CRSystemIconItem?) {
        guard let userIconList = userListsItem else{return}
        if let userIcons = userIconList.source?.items{
            systemUserIconListView.systemUserIcons = userIcons
            systemUserIconListView.userIconUrl = {[weak self] (userIconUrl:String) in
                guard let weakSelf = self else {return}
                
                weakSelf.systemUserIconListView.removeFromSuperview()
                
                if userIconUrl.isEmpty{
                    weakSelf.tapOneTapGestureRecognizerClick()
                }else {
                    weakSelf.userIconImageV.kf.setImage(with: URL(string: userIconUrl), placeholder: UIImage(named: "member_page_default_header"), options: nil, progressBlock: nil, completionHandler: nil)
                    weakSelf.modifyAvatar = userIconUrl
                    weakSelf.editedLocalAvatar = false
                }
            }
            
            UIApplication.shared.keyWindow?.addSubview(systemUserIconListView)
        }
    }
    
    //MARK:提交用户修改的个人信息
    @objc private func commitClick(){
        if editedLocalAvatar {
            editAvatarWithLocalHandler?()
            return
        }
        
        if self.modifyAvatar.isEmpty && modifyNickName.isEmpty{
            //如果昵称和用户图像都没有修改
            navigationController?.popViewController(animated: true)
            return
        }
        
        updateEditInfoHandler?()
 }
    
    ///提交修改用户信息的返回数据处理
    func submitInfoHandle(userListsItem:CRSystemIconItem?) {
        guard let userInfoItem = userListsItem else{return}
        if userInfoItem.success{
            let userItem = CRUserInfoCenterSource()
            userItem.avatar = modifyAvatar
            userItem.nickName = modifyNickName
            commitUserInfoClourse?(userItem)
            navigationController?.popViewController(animated: true)
        }else{
            showToast(view: self.view, txt: userInfoItem.msg)
        }
    }
    
    //按钮高亮状态
    @objc private func buttonBackGroundHighlighted(){
        self.commitButton.backgroundColor = UIColor.green
    }
    //MARK:刷新用户今天投注输赢数据
    @objc private func refreshData(loadTextStr:String){
        let parmaters = ["code":"R7036",
                         "msgUUID":CRCommon.generateMsgID(),
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app",
                         "roomId":roomId]
    
        CRNetManager.shareInstance().sendMessage(paramters: parmaters, method: .post, requestType: .userTodayWinLos) { (isSuccess, jsonString, userWinLostItem) in
            if isSuccess{
                
            }
        }
    }
    //修改昵称
    @objc private func modifyNickNameClick(){
        //
        isModifyNickname = !isModifyNickname
        systemDefalutNickNameLabel.isHidden = isModifyNickname ? true : false
        modifyNickTextField.isHidden = isModifyNickname ? false : true
        self.modifyNickName = ""
        //编辑状态
        if isModifyNickname {
            modifyNickTextField.text = nickName
            modifyNickNameButton.snp.remakeConstraints { (make) in
                make.left.equalTo(modifyNickTextField.snp.right)
                make.centerY.equalTo(modifyNickTextField.snp.centerY)
            }
        }else{
            modifyNickNameButton.snp.remakeConstraints { (make) in
                make.left.equalTo(systemDefalutNickNameLabel.snp.right)
                make.centerY.equalTo(systemDefalutNickNameLabel.snp.centerY)
            }
        }
    }
    @objc private func modifyNickName(textField:UITextField){
        //修改后
        self.modifyNickName = textField.text ?? ""
//        nickName = textField.text ?? ""
    }
}
 extension CRUserProfileInfoCtrl:UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //
//        self.nickName = textField.text ?? ""
    }
    //限制用户修改昵称的字数
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else{
            return true
        }
        let textLength = text.length + string.length - range.length
        return textLength <= 8
    }
 }
//MARK: --UITableViewDelegate
extension CRUserProfileInfoCtrl:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userInfoArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CRUserInfoDetailCell")  as? CRUserInfoDetailCell
        if indexPath.row < self.userInfoArray.count {
             let userItem = self.userInfoArray[indexPath.row]
            cell?.userInfoItem = userItem
            cell?.userInfoDetailCellDelegate = self
            
            return cell!
        }
       return UITableViewCell()
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 100))
        footView.backgroundColor = UIColor.white
        footView.addSubview(commitButton)

        commitButton.snp.makeConstraints { (make) in
            make.left.equalTo(footView).offset(20)
//            make.top.equalTo(footView).offset(50)
            make.centerX.equalTo(footView.snp.centerX)
            make.centerY.equalTo(footView.snp.centerY)
            make.height.equalTo(40)
        }

        return footView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
}
 //MARK：- CRUserInfoDetailCellDelegate
 extension CRUserProfileInfoCtrl:CRUserInfoDetailCellDelegate{
    //今日输赢分享
    func shareGainsAndLossesForToday() {
        shareGainsAndLossesForTodayHandler?(gainsLossesItem!)
        self.navigationController?.popViewController(animated: true)
    }
    //刷新最新数据
    func userInfoDetailCellRefreshData() {
//        getUserIconAndnickNameDataFromServer(loadTextStr: "正在刷新数据...")
        updateUserInfoHandler?()
    }
    //开关
    func switchFunctionStartAndClose(on: Bool,cell:CRUserInfoDetailCell) {
        let indexPath = userInfoTableView.indexPath(for: cell) ?? IndexPath.init(row: 0, section: 0)
        if cell.titleNameLabel.text == "消息推送通知" {
            CRDefaults.setChatroomMessageNoti(value: on)
        }else if cell.titleNameLabel.text == "消息发送提示音"{
            CRDefaults.setChatroomSendMessageVoice(value: on)
        }else if cell.titleNameLabel.text == "消息接收提示音"{
            CRDefaults.setChatroomRecerveMessageVoice(value: on)
        }else if cell.titleNameLabel.text == "进房通知"{
            CRDefaults.setChatroomEnterRoomNoti(value: on)
        }
    }
    
 }
 
 extension CRUserProfileInfoCtrl: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
     //手势 拍照
     func tapOneTapGestureRecognizerClick(){
         let sexActionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
         
         weak var weakSelf = self
         
         let sexNanAction = UIAlertAction(title: "从相册中选择", style: UIAlertAction.Style.default){ (action:UIAlertAction)in
             self.initPhotoPicker()
             //填写需要的响应方法
         }
         let sexNvAction = UIAlertAction(title: "拍照", style: UIAlertAction.Style.default){ (action:UIAlertAction)in
             self.initCameraPicker()
             //填写需要的响应方法
         }
         let sexSaceAction = UIAlertAction(title: "取消", style: UIAlertAction.Style.cancel){ (action:UIAlertAction)in
             //填写需要的响应方法
         }
         sexActionSheet.addAction(sexNanAction)
         sexActionSheet.addAction(sexNvAction)
         sexActionSheet.addAction(sexSaceAction)
         
         self.present(sexActionSheet, animated: true, completion: nil)
     }
     
     //MARK: - 相机
     
     //从相册中选择
     func initPhotoPicker(){
         let photoPicker =  UIImagePickerController()
         photoPicker.delegate = self
         photoPicker.allowsEditing = true
         photoPicker.sourceType = .photoLibrary
         //在需要的地方present出来
         self.present(photoPicker, animated: true, completion: nil)
     }
     
     //拍照
     func initCameraPicker(){
         
         if UIImagePickerController.isSourceTypeAvailable(.camera){
             let  cameraPicker = UIImagePickerController()
             cameraPicker.delegate = self
             cameraPicker.allowsEditing = true
             cameraPicker.sourceType = .camera
             //在需要的地方present出来
             self.present(cameraPicker, animated: true, completion: nil)
         } else {
             print("不支持拍照")
         }
     }
     
     func base64ToNormalString(base64Msg:String) -> String {
         guard let baseData = Data(base64Encoded: base64Msg, options: Data.Base64DecodingOptions(rawValue: NSData.Base64DecodingOptions.RawValue(0))) else {
             return ""
         }
         
         return String.init(data:baseData, encoding: .utf8) ?? ""
     }
     
     //MARK: 选择图片上传
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         var image:UIImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as! UIImage
         image = image.scaleImage(scaleSize: 50 / image.size.width)
        userIconImageV.image = image
         if picker.sourceType == .camera {
             UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
         }
         
         guard let imageDate = (image.pngData() != nil) ? image.pngData() : image.jpegData(compressionQuality: 0.5) else {
             showToast(view: self.view, txt: "图片格式不支持")
             return
         }
        
         self.editedLocalAvatar = true
         let imageName = String.init(format:"%f.png",Date().timeIntervalSince1970)
         let imgPath = FileManager.pathUserChatImage(imageName: imageName)
         FileManager.default.createFile(atPath: imgPath, contents: imageDate, attributes: nil)
         
         let imageFileString = imageDate.base64EncodedString()
         
         // fileString: 图片base64值
         // fileType:图片类型(gif、jpeg、png、jpg)
         // type:对应图片类型的角标(0、1、2、3)
         let type = contentTypeForImageData(data: imageDate as NSData)
         let parmaters = [
             "fileType":".\(type.fileType)",
             "fileString":imageFileString,
             "type":type.type
         ]
        
        editAvatarWithLocalHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            
            CRNetManager.shareInstance().uploadAvatarFile(paramters: parmaters as CRNetManager.Parameters, method: .post, imageData: imageDate, imagePath: imageName, handler: {(isSuccess, json, fileCode) in
                               
               DispatchQueue.main.asyncAfter(deadline: .now()) {
                   if isSuccess{
                       weakSelf.navigationController?.children.forEach{ UIViewController in
                           if UIViewController.isKind(of: CRChatController.self){
                               let VC: CRChatController = UIViewController as! CRChatController
                               VC.sendMsgUpdateUserAvatarList(fileString: fileCode ?? "" )
                           }
                       }
                   }else {
                      showToast(view: weakSelf.view, txt: json)
                   }
               }
            }) { (progress) in
            }
        }
        
        self.dismiss(animated: true, completion: nil)
     }
     
     @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
         
         if error != nil {
             print("保存失败")
         } else {
             print("保存成功")
         }
     }
     
     //通过图片Data数据第一个字节 来获取图片扩展名
     func contentTypeForImageData(data: NSData) -> (fileType: String, type: String){
         var c: UInt8 = 0
         data.getBytes(&c, length: 1)
         switch (c) {
             case 0xFF:
                 return ("jpeg", "1")
             case 0x89:
                 return ("png", "2")
             case 0x47:
                 return ("gif", "0")
         default:
             return ("jpg", "3")
         }
     }
 }
