//
//  CRBaseController.swift
//  Chatroom
//
//  Created by admin on 2019/7/10.
//  Copyright © 2019年 yun. All rights reserved.
//

import UIKit
import YogaKit
//重置聊天窗口
let NOTI_CHAT_VIEW_RESET  =  "NT_ResetChatView"
class CRBaseController: BaseController{

    var moreKeyboard:CRMoreKeyboard{
        get{
            return CRMoreKeyboard.shareMoreKeyboard
        }
    }
    var emojiKeyboard:CREmojiKeyboard{
        get{
            return CREmojiKeyboard.emojiKeyboard
        }
    }
    
    lazy var navigationBar:CRNavigationBar = {
        let size = self.view.frame.size
        let view = CRNavigationBar.init(frame: CGRect.init(x: 0, y: 0, width: Int(size.width), height: KNavHeight))
        
        view.leftView.configureLayout(block: { (layout) in
            layout.width = YGValue(CGFloat(100))
        })

        view.titleView.configureLayout(block: { (layout) in
            layout.width = YGValue(CGFloat(200))
        })

        view.rightView.configureLayout(block: { (layout) in
            layout.width = YGValue(CGFloat(100))
        })
        
        view.leftTapHandler = {[weak self] () in
            if let weakSelf = self {
                weakSelf.leftBarItemTapAction()
            }
        }
        
        view.titleTapHandler = {[weak self] () in
            if let weakSelf = self {
                weakSelf.titleBarItemTapAction()
            }
        }
        
        view.rightTapHander = {[weak self] () in
            if let weakSelf = self{
                weakSelf.rightBarItemTapAction()
            }
        }
        
        return view
    }()
    /**emoji展示View*/
    lazy var emojiDisplayView:CREmojiDisplayView = {
        let emojiView = CREmojiDisplayView(frame: CGRect.zero)
        return emojiView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        self.view.addSubview(self.navigationBar)
    }
    
    //MARK: - 子类覆盖
    func leftBarItemTapAction() {
        
    }
    
    func titleBarItemTapAction() {
        
    }
    
    func rightBarItemTapAction(){
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func setChatMoreKeyboardData(moreKeyboardData:[Any]){
        
        moreKeyboard.setChatMoreKeyboardData(chatMoreKeyboardData: moreKeyboardData)
    }
    /**设置emoji键盘数据*/
    func setChatEmojiKeyboard(emojiKeyboardData:[Any]){
        
        CREmojiKeyboard.emojiKeyboard.emojiGroupData = emojiKeyboardData
    }
    
    @objc func resetChatVC(){
//        NSString *chatViewBGImage;
//        if (self.partner) {
//            chatViewBGImage = [[NSUserDefaults standardUserDefaults] objectForKey:[@"CHAT_BG_" stringByAppendingString:[self.partner chat_userID]]];
//        }
//        if (chatViewBGImage == nil) {
//            chatViewBGImage = [[NSUserDefaults standardUserDefaults] objectForKey:@"CHAT_BG_ALL"];
//            if (chatViewBGImage == nil) {
//                [self.view setBackgroundColor:[UIColor colorGrayCharcoalBG]];
//            }
//            else {
//                NSString *imagePath = [NSFileManager pathUserChatBackgroundImage:chatViewBGImage];
//                UIImage *image = [UIImage imageNamed:imagePath];
//                [self.view setBackgroundColor:[UIColor colorWithPatternImage:image]];
//            }
//        }
//        else {
//            NSString *imagePath = [NSFileManager pathUserChatBackgroundImage:chatViewBGImage];
//            UIImage *image = [UIImage imageNamed:imagePath];
//            [self.view setBackgroundColor:[UIColor colorWithPatternImage:image]];
//        }
    }
    
    lazy var recorderIndicateView:CRRecorderIndicatorView = {
        let view = CRRecorderIndicatorView()
        return view
    }()
    
}

