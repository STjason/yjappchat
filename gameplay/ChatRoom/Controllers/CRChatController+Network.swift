//
//  CRChatController+Network.swift
//  gameplay
//
//  Created by admin on 2019/11/1.
//  Copyright © 2019年 yibo. All rights reserved.
//  HTTP网络请求

import Foundation
import PKHUD

//MARK: - 网络请求
extension CRChatController {
    
    ///获取当前期号离结束投注倒计时
     func netGetCountDownByCpcode() {
        var p = [String:Any]()
        
        let numSelectView = contentView.betArea.numSelectArea.numSelectView
        let cpBianHao = numSelectView.cpBianHao
        let cpVersion = numSelectView.cpVersion
        p["lotCode"] = cpBianHao
        p["version"] = cpVersion
        
        CRNetManager.shareInstance().netGetCountDownByCpcode(paramters: p) {[weak self] (success, msg, code,model) in
            guard let weakSelf = self else {
                return
            }
            
            if !success {
                showToast(view: weakSelf.view, txt: msg)
            }
            
            if let model = model {
                weakSelf.updateCurrenQihaoCountDown(countDown: model, shouldReStartDisableTimer: true, successHandler: { (dealDuration) in
                    if YiboPreference.getAbleBet() == "on" {
                        weakSelf.countDownValue = String.init(format:"%@", dealDuration)
                    }
                })
            }else {
                //彩票停售时弹框显示
                guard let code = code else {
                    return
                }
                if code == 1300{
                    weakSelf.showStopSaleWindow()
                    return
                }
                
                if code == 0{
                    loginWhenSessionInvalid(controller: weakSelf)
                }
            }
        }
    }
    
    ///获取当前期号离结束投注倒计时_跟单
    func netGetCountDownShareByCpcode() {
        guard let betInfo = betInfo else {
            return
        }
        
        var p = [String:Any]()
        p["lotCode"] = betInfo.lotCode
        p["version"] = betInfo.version
        
        CRNetManager.shareInstance().netGetCountDownByCpcode(paramters: p) {[weak self] (success, msg, code,model) in
            guard let weakSelf = self else {
                return
            }
            
            if !success {
                showToast(view: weakSelf.view, txt: msg)
            }
            
            if let model = model {
                weakSelf.updateCurrenQihaoCountShareDown(countDown: model, shouldReStartDisableTimer: true, successHandler: { (dealDuration) in
                    if YiboPreference.getAbleBetShare() == "on" {
                        weakSelf.countDownShareValue = String.init(format:"%@", dealDuration)
                    }
                })
            }else {
                //彩票停售时弹框显示
                guard let code = code else {
                    return
                }
                if code == 1300{
                    weakSelf.showStopSaleWindow()
                    return
                }
                
                if code == 0{
                    loginWhenSessionInvalid(controller: weakSelf)
                }
            }
            
        }
    }
    
    ///获取账户信息
    func netAccountWeb() {
        let p = [String:Any]()
        CRNetManager.shareInstance().netAccountWeb(paramters: p) {[weak self] (success, msg, model) in
            guard let weakSelf = self else {
                return
            }
            
            if success {
                if let model = model {
                    weakSelf.updateAccount(memInfo: model)
                }
            }else {
                showToast(view: weakSelf.view, txt: msg)
            }
        }
    }
    
    /**
     * 创建查询开奖结果倒计时
     * @param duration
     */
    func createLastResultTimer() -> Void {
        lastKaiJianResultTimer?.invalidate()
        lastKaiJianResultTimer = nil
        lastKaiJianResultTimer = Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(lastResultTickDown), userInfo: nil, repeats: true)
        if let timer = lastKaiJianResultTimer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    @objc func lastResultTickDown() -> Void {
        if let previousPeriod = Int64(previousResults),let currentPeriod = Int64(currentQihao) {
            if currentPeriod - previousPeriod == 1 {
                lastKaiJianResultTimer?.invalidate()
                lastKaiJianResultTimer = nil
                return
            }
        }
        
        netLastOpenResult()
    }
    
    ///获取开奖结果
    func netLastOpenResult() {
        var p = [String:Any]()
        p["lotCode"] = self.cpBianHao
        p["pageSize"] = 1
        CRNetManager.shareInstance().netLastOpenResult(paramters: p) {[weak self] (success, msg, models) in
            guard let weakSelf = self else {
                return
            }
            
            if !success {
                showToast(view: weakSelf.view, txt: msg)
                return
            }
            
            if let models = models {
                
                var lastOpenResultChanged = true
                if models.count > 0 && weakSelf.recentResults.count > 0 {
                    if models[0].qiHao == weakSelf.recentResults[0].qiHao {
                        lastOpenResultChanged = false
                    }
                }
                
                if lastOpenResultChanged {
                    weakSelf.recentResults = models
                }
                
                if models.count > 0 {
                    let firstResult = models[0]
                    let qihaoStr = trimQihao(currentQihao: firstResult.qiHao)
                    
                    if qihaoStr != weakSelf.previousResults {
                        weakSelf.lastKaiJianResultTimer?.invalidate()
                        weakSelf.lastKaiJianResultTimer = nil
                        weakSelf.netAccountWeb()
                        weakSelf.previousResults = qihaoStr
                    }
                }
            }
        }
    }
    
    ///获取冷热遗漏数据,根据彩种lotCode（如老重庆时时彩的lotCode为：LCQSSC），所以在点击彩种的时候就可以获取
    func netColdHoltCodeRank() {
        var p = [String:Any]()
        p["lotCode"] = lotteryData.code ?? ""
        CRNetManager.shareInstance().netColdHoltCodeRank(paramters: p) {[weak self] (success, msg, models) in
            
            guard let weakSelf = self else {
                return
            }
            
            if !success {
                showToast(view: weakSelf.view, txt: msg)
                return
            }
            
            let codeRankBlock = {
                let firstRuleName = weakSelf.playRules[0].name
                weakSelf.contentView.betArea.numSelectArea.topView.updateRule(name: firstRuleName)
                
                weakSelf.selectPlay = weakSelf.playRules[0] //改变当前玩法的时候刷新 号码选择视图
            }
            
            if let models = models {
                //项目之前刷新视图太多次，现改为获取到遗漏数据后再刷新 号码选择视图
                weakSelf.codeRank = models
                codeRankBlock()
                
            }else {
                codeRankBlock()
            }
        }
    }
    
    ///获取彩种对应玩法数据
    func netAllPlayRules() {
        var p = [String:Any]()
        
        p["lotType"] = self.lotteryData.lotType
        p["lotCode"] = self.lotteryData.code ?? ""
        p["lotVersion"] = self.lotteryData.lotVersion
        CRNetManager.shareInstance().netAllPlayRules(paramters: p) {[weak self] (success, msg, model) in
            
            guard let weakSelf = self else {
                return
            }
            
            if !success {
                showToast(view: weakSelf.view, txt: (msg ?? ""))
                return
            }
            
            //奖获取到的玩法平铺成一级数组列表，展现出来的玩法是所有叶子结点的玩法数据
            if let weakSelf = self, let lottery = model {
                weakSelf.lotteryData = lottery
                weakSelf.updateLocalConstants(lotData: lottery)
                
                let rules:[BcLotteryPlay] = PlayTool.leaf_play_rules(lottery: lottery)
                if !rules.isEmpty{
                    weakSelf.playRules.removeAll()
//                    weakSelf.playRules = rules
                    
                    if let rules = handlePlayRules(czCode: lottery.czCode ?? "", rulesP: rules) {
                        weakSelf.playRules = rules
                    }
                    
                    if weakSelf.playRules.count > 0 {
                        weakSelf.netColdHoltCodeRank()
                    }
                }
            }
        }
    }
    
    /// 获取所有彩种信息
    func netAllLotteries() {
        CRNetManager.shareInstance().netAllLotteries(paramters: [:]) {[weak self] (success, msg, models) in
            guard let weakSelf = self else {
                return
            }
            
            if !success {
                showToast(view: weakSelf.view, txt: (msg ?? ""))
                return
            }
            
            if let weakSelf = self,let models = models {
                var lotterys = models
                lotterys = lotterys.sorted {(model_1,model_2) -> Bool in
                    return model_1.sortNo > model_2.sortNo
                }
                let config = getSystemConfigFromJson()
                let defaultType = config?.content.switch_xfwf=="off" ? 0 : 1

                weakSelf.allLotteries = lotterys
                //选中的官方或信用
                //获取当前用户所选中的官方或信用玩法
                let type = weakSelf.contentView.betArea.lotteryView.officalCreditSeg.selectedSegmentIndex 
//                weakSelf.numSelectView.selectedPlayRulesModel.clearModel()
                
                var datas = [LotteryData]()
                
                let officalDatas = weakSelf.lotteryLogic.prepare_data_for_subpage(type: 0, lotteryDatas: models)
                
                let creditDatas = weakSelf.lotteryLogic.prepare_data_for_subpage(type: 1, lotteryDatas: models)
                
                weakSelf.contentView.betArea.lotteryView.officalCreditSeg.isHidden = (officalDatas.count == 0 || creditDatas.count == 0)
                
                if officalDatas.count == 0 {
                    datas = creditDatas
                }
                
                //判断官方和信用的数据下标是否和Segment的下标相反
                let realType = defaultType==0 ? type : (type==0 ? 1 : 0)
                if !(officalDatas.count == 0 && creditDatas.count == 0) {
                    datas = weakSelf.lotteryLogic.prepare_data_for_subpage(type: realType, lotteryDatas: models)
                    if datas.count == 0{
                        datas = weakSelf.lotteryLogic.prepare_data_for_subpage(type: (realType==0 ? 1 : 0), lotteryDatas: models)
                    }
                }
                
                datas = datas.sorted {(model_1,model_2) -> Bool in
                    return model_1.sortNo > model_2.sortNo
                }
                
                weakSelf.lotteryModels = datas
            }
        }
    }
    
    //一获取到开关，立刻执行的操作
    func configWithChatConfig() {
        let switch_room_show = getChatRoomSystemConfigFromJson()?.source?.switch_room_show != "0"
        let chatroomSendBetting = CRDefaults.getChatroomSendBetting()
        var removeArray = [Int]()
        if !switch_room_show && !chatroomSendBetting {
            removeArray = [0,1]
        }else if !switch_room_show {
//            removeArray = [0]
        }else if !chatroomSendBetting {
            removeArray = [1]
        }
        if getChatRoomSystemConfigFromJson()?.source?.switch_plan_list_show == "0" {
            //彩票计划开关 关闭状态 就把tabbar 彩票计划去掉
            removeArray.append(2)
        }
        //将会员等级的背景颜色写到本地
        ////不同等级聊天展示的聊天背景颜色不同
        let native_name_backGround_color_info = getChatRoomSystemConfigFromJson()?.source?.native_name_backGround_color_info ?? []
        var levelColor:[String:AnyObject] = [:]
        if !native_name_backGround_color_info.isEmpty{
            for backGroud_colors in native_name_backGround_color_info{
                var backgroudDict:[String:AnyObject] = [:]
//                let backgrounds = ("backgroudColor",backGroud_colors.color)
                backgroudDict["backgroudColor"] = backGroud_colors.color as AnyObject
                levelColor[backGroud_colors.levelName] = backgroudDict as AnyObject
            }
            CRDefaults.setLevelColor(colorDict: levelColor)
        }
        
        
        //不同等级聊天展示的文字颜色不同
        var levelTitleColor:[String:AnyObject] = [:]
        let native_name_level_title_color_info = getChatRoomSystemConfigFromJson()?.source?.native_name_level_title_color_info ?? []
        if !native_name_level_title_color_info.isEmpty {
            for titleColor in native_name_level_title_color_info{
                var titleDict:[String:AnyObject] = [:]
                titleDict["titleColor"] = titleColor.color as AnyObject
                levelTitleColor[titleColor.levelName] = titleDict as AnyObject
            }
            CRDefaults.setLevelTitleColor(colorDict: levelTitleColor)
        }
        
        //历史记录拉取条目
        let history_count = getChatRoomSystemConfigFromJson()?.source?.switch_history_perpage_count ?? ""
        if !history_count.isEmpty {
            let countArray = history_count.components(separatedBy: ":")
            if countArray.count > 0 {
                if let count = Int(countArray[0]) {
                    if count >= 0 {
                        historyCountPerPage = count
                    }
                }
            }
        }
        
        auto_share_bet_arrive_amount = getChatRoomSystemConfigFromJson()?.source?.name_auto_share_bet_arrive_amount ?? ""
        
        getMsgHistoryRecordData()
        
        self.contentView.tabbar.configTabBar(removeItems: removeArray)
        contentView.messageArea.setupRemoter()
        contentView.messageArea.remoteView.delegate = self //遥控器代理设置
    }
 
    //MARK: - 消息的http发送方式
    //MARK: 分享注单
    ///格式化分享注单的消息model--历史注单页面
    func formatBetShareMsgModelInHistory(model:CRBetHistoryMsgModel){
        let HUD = getHUD(text: "注单分享中...")
        
        let shareBetModel = CRShareBetModel()
        let shareBetInfo = CRShareBetInfo()
        
        let shareOrder = CRShareOrder()
        shareOrder.orderId = model.orderId
        shareOrder.betMoney = "\(model.buyMoney)"
        
        shareBetInfo.lottery_amount = "\(model.buyMoney)"
        shareBetInfo.lottery_content = model.haoMa
        shareBetInfo.lottery_play = model.playName
        shareBetInfo.lottery_qihao = model.qiHao
        shareBetInfo.lottery_type = model.playType
        shareBetInfo.lottery_zhushu = "\(model.buyZhuShu)"
        shareBetInfo.lotCode = model.lotCode
        shareBetInfo.version = "\(model.lotVersion)"
        shareBetInfo.model = model.model + 0
        //shareBetInfo.ago = self.lotteryData.ago + 0
        shareBetModel.betInfo = shareBetInfo
        
        shareBetModel.roomId = self.msgModel.roomId + ""
        shareBetModel.stationId = self.msgModel.stationId + ""
        shareBetModel.code = CRUrl.shareInstance().CODE_SHARE_BET_MSG
        shareBetModel.msgUUID = CRCommon.generateMsgID()
        shareBetModel.source = "app"
        shareBetModel.userId = CRDefaults.getUserID()
        shareBetModel.orders = [shareOrder]
        shareBetModel.multiBet = "1"
        shareBetModel.betInfos = [shareBetInfo]
        
        self.sendShareBetMessage(model: shareBetModel)
        
        if let paramters = shareBetModel.toJSON() {
            CRNetManager.shareInstance().sendMessage(paramters: paramters, method: .post, requestType: .shareOrder) {[weak self] (success, msg, model) in
                
                HUD.hide(animated: true)
                
                guard let weakSelf = self else {
                    return
                }
                
                if !success {
                    showToast(view: weakSelf.view, txt: msg)
                }
            }
        }
    }
    
    //发送分享注单消息接口调用
    func sendShareBetMessage() {
        let HUD = getHUD(text: "注单分享中...")
        
        for (index,shareBetModel) in self.shareBetModels.enumerated() {
            if index < self.shareBetModels.count {
                self.sendShareBetMessage(model: shareBetModel)
                
                if let paramters = shareBetModel.toJSON() {
                    CRNetManager.shareInstance().sendMessage(paramters: paramters, method: .post, requestType: .shareOrder) {[weak self] (success, msg, model) in
                        
                        guard let weakSelf = self else {
                            return
                        }
                        
                        if index + 1 == weakSelf.shareBetModels.count {
                            HUD.hide(animated: true)
                            weakSelf.shareBetModels.removeAll()
                            weakSelf.playSendMessageSuccessVoice()
                        }
                        
                        if success {
                            
                        }else {
                            HUD.hide(animated: true)
                            showToast(view: weakSelf.view, txt: msg)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - 彩票类消息相关
    
    //发送消息请求
    //SEND_MSG = R7001发送文本消息
    //SEND_IMAGE = R7024 发送图片消息
    
    //SHARE_BET = J 分享投注
    //FOLLOW_BET = R7010 跟单
    //RED_PACKAGE = R7009 发红包
    //roomId source:app type:用户类型 普通用户
    //speakType:1快捷发言 2,自由发言 3,发送表情 4,投注消息 5 @其它用户消息 6 快捷图片
    //ATaccount:传被@的用户userId
    func sendMessageRequest(messageItem : CRMessage){
        //yibochat.com/app/nativeTestApi
        
        let textMsgItem = messageItem as? CRTextMessageItem
        let paramters = ["stauts":"",
                         "msgUUID":CRCommon.generateMsgID(),
                         "roomId":chat_roomId,
                         "record":textMsgItem?.text,
                         "type":"2",
                         "speakType":messageItem.speakType,
                         "ATaccount":messageItem.userId,
                         "option1":"",
                         "channelId":"ios",
                         "userId":CRDefaults.getUserID(),
                         "source":"app",
                         "code":"R7001",
                         //1代表当前帐号是当前所在的代理房间的主人 0 不是
            "agentRoomHost":agentRoomHost]
        CRNetManager.shareInstance().sendMessage(paramters: paramters as Any as! CRNetManager.Parameters, method: .post, requestType: .text) {[weak self] (isSuccess, msg, textMessagItem) in
            guard let weakSelf = self else {
                return
            }
            if isSuccess {
                messageItem.sendState = .Success
                weakSelf.playSendMessageSuccessVoice()
                weakSelf.contentView.messageArea.msgTable.reloadData()
            }else {
                messageItem.sendState = .Fail
                showToast(view: weakSelf.view, txt: msg)
                weakSelf.contentView.messageArea.msgTable.reloadData()
            }
//            weakSelf.contentView.messageArea.msgTable.reloadData()
            
            weakSelf.scrollTableViewToBottom(animated:true)
        }
    }
    
    //MAERK:上传图片到服务器
    func uploadImageToServerRequest(messageItem:CRImageMessage){
        
        chat_userId = CRDefaults.getUserID()
        
        let imgPath = FileManager.pathUserChatImage(imageName: (messageItem.imagePath) )
        
        guard let uploadImage = UIImage.init(contentsOfFile: imgPath) else {
            return
        }
        let imageName = messageItem.imagePath
        let image = uploadImage.resizeImage(originalImg: uploadImage)
        let imagesData = image.compressImageOnlength(maxLength: 1024 * 1024,image:image)
        
        let parmaters = ["fileType":"image/jpeg",
                         "name":imageName,
                         "fileString":imgPath,
                         "userId":chat_userId
        ]
        DispatchQueue.global().async {
            CRNetManager.shareInstance().uploadFile(paramters: parmaters, method: .post, imageData: imagesData!, imagePath: imgPath, handler: {[weak self] (isSuccess, json, uploadImageItem) in
                
                guard let weakSelf = self else {
                    return
                }
                if isSuccess{
                    
                    weakSelf.fileCode = (uploadImageItem?.fileCode)!
//                    messageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?contentType=image/jpeg&fileId=" + (uploadImageItem?.fileCode)!
                    messageItem.imageUrl = CRUrl.shareInstance().CHAT_FILE_BASE_URL + "\(CRUrl.shareInstance().URL_WEB_READ_FILE)/" + (uploadImageItem?.fileCode)!
                    
                    messageItem.sendState = .Success
                    weakSelf.contentView.messageArea.msgTable.reloadData()
                    weakSelf.scrollTableViewToBottom(animated:false)
//                    weakSelf.sendImageMessageRequset(message: uploadImageItem!,imageName:imageName)
                    
                    guard let item = uploadImageItem else {
                        showToast(view: weakSelf.view, txt: "上传图片失败")
                        return
                    }
                    item.msgUUID = messageItem.msgUUID
                    weakSelf.sendMsgImageMessage(message: item, imageName: imageName, type: 2)
                }else {
                    showToast(view: weakSelf.view, txt: json)
                    messageItem.sendState = .Fail
                    weakSelf.contentView.messageArea.msgTable.reloadData()
                    weakSelf.scrollTableViewToBottom(animated:false)
                }
                
            }) { (progress) in
                //当前上传图片的进度条
                if Int(progress) == 1{
                    messageItem.sendState = .Success
                    self.contentView.messageArea.msgTable.reloadData()
                    self.scrollTableViewToBottom(animated:false)
                }
                
                messageItem.progress = progress
            }
        }
        
        self.contentView.messageArea.msgTable.reloadData()
        self.scrollTableViewToBottom(animated:true)
    }
    
    /**上传音频到服务器*/
    func uploadVoiceFileToServer(voiceItem:CRMessage,filePath:String){
        chat_userId = CRDefaults.getUserID()
        let fileName = (voiceItem as? CRVoiceMessageItem)?.recFileName ?? ""
        let voiceDuartion = voiceItem.content["time"] as? String ?? ""
        let duartion = Float(voiceDuartion) ?? 1
        
        let fileData = NSData(contentsOfFile: filePath)
        let parmaters = ["fileType":"audio/wav",
                         "name":fileName,
                         "fileString":filePath,
                         "userId":chat_userId
        ]
        CRNetManager.shareInstance().uploadFile(paramters: parmaters, method: .post, fileData: fileData! as Data, filePath: filePath, handler: {[weak self](isSuccess, msg, voiceMessageItem) in
            if isSuccess{
                guard let weakSelf = self else{return}
                guard let audioItem = voiceMessageItem else{return}
                //上传音频文件返回的
                if audioItem.success{
                    let fileCode = audioItem.fileCode
                    let fileItem = CRUploadImageItem()
                    fileItem.fileCode = fileCode
                    fileItem.fileString = audioItem.fileString
                    fileItem.success = audioItem.success
                    fileItem.msgUUID = voiceItem.msgUUID
                    weakSelf.sendMsgImageMessage(message: fileItem, imageName: fileName, type: 11, duration: Int(duartion))
                }
            }else{
                voiceItem.sendState = .Fail
            }
        }) { (progress) in
            if progress >= 1{
                voiceItem.sendState = .Success
                self.contentView.messageArea.msgTable.reloadData()
            }
        }
    }
    //发送图片
//    func sendImageMessageRequset(message:CRUploadImageItem,imageName:String,type:Int){
//
//        chat_userId = CRDefaults.getUserID()
//        let stationId = self.msgModel.stationId
//        var fileType:String = ""
//        if type == 2{
//            fileType = "image/jpeg"
//        }else if type == 11{
//            fileType = "audio/amr"
//        }
//        //type图片消息类型 2--图片 视频--10 语音--11
//        let paramters = ["stauts":"",
//                         "roomId":chat_roomId,
//                         "agentRoomHost":agentRoomHost,
//                         "type":type,
//                         "remark":"",
//                         "option1":"",
//                         "speakType":"2",
//                         "channelId":"ios",
//                         "userId":chat_userId,
//                         "source":"app",
//                         "code":"R7024",
//                         "record":message.fileCode,
//                         "stationId":stationId,
//                         "fileString":message.fileString,
//                         "key":"",
//                         "msgUUID":CRCommon.generateMsgID(),
//                         "name":imageName,
//                         "fileType":fileType,
//                         "picMsg":["code":"R7024",
//                                   "roomId":chat_roomId,
//                                   "userId":CRCommon.generateMsgID(),
//                                   "size":"",
//                                   "latocalPh":"",
//                                   "thumbPath":"",
//                                   "source":"app",
//                                   "type":type,
//                                   "msgUUID":CRCommon.generateMsgID()]] as [String : Any]
//        CRNetManager.shareInstance().sendMessage(paramters: paramters as Any as! CRNetManager.Parameters, method: .post, requestType: .image) {[weak self] (isSuccess, json, messageItem) in
//
//            guard let weakSelf = self else  {
//                return
//            }
//            if isSuccess{
//                if type == 2{
//                    //成功
//                    //发送成功图片加到图片浏览器数据源中
//                    let imageUrl =  CRUrl.shareInstance().CHAT_FILE_BASE_URL + CRUrl.shareInstance().URL_READ_FILE  + "?fileId=" + message.fileCode
//                    weakSelf.playSendMessageSuccessVoice()
//                    weakSelf.imageUrls.append(imageUrl)
//
//                }
//              print(json)
//            }else {
//                showToast(view: weakSelf.view, txt: json)
//            }
//        }
//    }
    
    //MARK:发红包
//
//    func sendRedPackageMessageRequest(message:CRMessage){
//        // amount 总金额 count
//        guard let redPacgetItem = message as? CRRedPacketMessage else {return}
//        let stationId = self.msgModel.stationId
//        let paramters = ["roomId":chat_roomId,
//                         "agentRoomHost":agentRoomHost,
//                         "amount":redPacgetItem.amount,
//                         "count":redPacgetItem.count,
//                         "remark":redPacgetItem.remark,
//                         "stationId":stationId,
//                         "packetId":"",
//                         "msgUUID":CRCommon.generateMsgID(),
//                         "code":redPacgetItem.code,
//                         "channelId":"ios",
//                         "userId":CRDefaults.getUserID(),
//                         "source":"app"] as [String : Any]
//
//        CRNetManager.shareInstance().sendMessage(paramters: paramters, method: .post, requestType: .redPacket) {[weak self] (isSuccess, jsonString, messageItem) in
//
//            guard let weakSelf = self else {
//                return
//            }
//            if isSuccess{
//                if let redMessageItem = messageItem as? CRRedPacketDataItem{
//                    if  let redPacketDataItem = redMessageItem.source{
//                        redPacgetItem.payId = redPacketDataItem.payId
//                        redPacgetItem.remark = redPacketDataItem.remark
//                        redPacgetItem.sendState = .Success
//                        weakSelf.sendRedPacketMessage(redPacketItem: redPacgetItem)
//                        weakSelf.playSendMessageSuccessVoice()
//                    }
//                    //                    if let redPacketDataItem = CRRedPacketMessageItem.deserialize(from: sourceString){
//                    //成功
//                    //
//                    //                    }
//                }
//            }else {
//                redPacgetItem.sendState = .Fail
//                showToast(view: weakSelf.view, txt: jsonString)
//            }
//            weakSelf.contentView.messageArea.msgTable.reloadData()
//            weakSelf.scrollTableViewToBottom(animated:true)
//        }
//    }
//
    //MARK:抢红包
//
//    func receiveRedPacketMessageRequest(message:CRRedPacketMessage){
//        //        var agentRoomHost1:Int = 0
//        //        if !agentUserCode.isEmpty{
//        //            agentRoomHost1 = 1
//        //        }
//
//        let parmaters = ["payId":message.payId,
//                         "roomId":chat_roomId,
//                         //代理房间 1或0
//            "agentRoomHost":agentRoomHost,
//            //简易版
//            "openSimpleChatRoom":"",
//            "msgUUID":CRCommon.generateMsgID(),
//            "code":"R7027",
//            "channelId":"ios",
//            "userId":CRDefaults.getUserID(),
//            "source":"app"] as [String : Any]
//        CRNetManager.shareInstance().sendMessage(paramters: parmaters, method: .post, requestType: .receiveRedPacket) {[weak self] (isSuccess, jsonString, messageItem) in
//            guard let weakSelf = self else {
//                return
//            }
//
//            if isSuccess{
//                if let receiverRedpacketItem = messageItem as? CRReceiveRedPacketItem{
//                    if  receiverRedpacketItem.source?.pickData == nil{
//                        //红包已被领取
//                        message.status = (receiverRedpacketItem.source?.status)!
//                    }
//                }
//            }else {
//                if let receiverRedpacketItem = messageItem as? CRReceiveRedPacketItem{
//                    //红包过期
//                    message.status = receiverRedpacketItem.status
//
//                }
//                showToast(view: weakSelf.view, txt: jsonString)
//            }
//
//            weakSelf.receiverDataRequest(message: message)
//        }
//    }
//
    //MARK:--领红包的人的详情
//
//    func receiverDataRequest(message:CRRedPacketMessage){
//        let parmaters = ["payId":message.payId,
//                         "roomId":chat_roomId,
//                         "msgUUID":CRCommon.generateMsgID(),
//                         "code":"R7032",
//                         "channelId":"ios",
//                         "userId":CRDefaults.getUserID(),
//                         "source":"app"]
//
//        CRNetManager.shareInstance().sendMessage(paramters: parmaters, method: .post, requestType: .receiveDetail) { [weak self](isSuccess, json, redPackMessageItem) in
//            guard let weakSelf = self else {
//                return
//            }
//
//            if isSuccess{
//                if let receiveDetailItem = redPackMessageItem as? CRReceiveDetailItem{
//                    let receiveDetailString = receiveDetailItem.source?.parent?.toJSONString()
//                    //领取的红包的当前用户信息
//                    let receiveDetailCustomString = receiveDetailItem.source?.c?.toJSONString()
//                    let receiveDetailCustomItem = CRReceiveDetailCustom.deserialize(from: receiveDetailCustomString)
//                    receiveDetailCustomItem?.status = message.status
//                    let totalPeopleDetails = receiveDetailItem.source?.other
//                    if  let redPacketParentItem = CRReceiveDetailParent.deserialize(from: receiveDetailString){
//
//                        weakSelf.redPacketDetailView.receiveRedPacketDetailItem = redPacketParentItem
//                        weakSelf.redPacketDetailView.receiveRedPacketDetailCustomItem = receiveDetailCustomItem
//                        weakSelf.redPacketDetailView.totalPeopleDetails = totalPeopleDetails
//                        //MARK：房间配置
//                        let chatRoomConfig = getChatRoomSystemConfigFromJson()
//                        if chatRoomConfig?.source?.switch_red_info == "1"{
//                            //显示详情
//                            weakSelf.redPacketDetailView.isShowDetail = true
//                        }else{
//                            weakSelf.redPacketDetailView.isShowDetail = false
//                        }
//                        if !weakSelf.redPacketDetailView.isShow{
//                            UIApplication.shared.keyWindow?.addSubview(weakSelf.redPacketDetailView)
//                        }
//                        weakSelf.redPacketDetailView.isShow = true
//                    }
//                }
//            }else {
//                showToast(view: weakSelf.view, txt: json)
//            }
//        }
//    }
//
    func hideGrayBGView() {
        self.removeaddLotteryHistoryView()
        self.contentView.floatView.removeFromSuperview()
        self.contentView.roomsTopView.hideView()
    }
    /////////////
}
