//
//  CRPictureBrowser.swift
//  gameplay
//
//  Created by Gallen on 2019/9/27.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class CRPictureBrowser: UIViewController {
    /**图片路径*/
    var imagePath:String?
    /**网络图片*/
    var imageUrl:String?
    
    var scrollView:UIScrollView?
    //滚动到的位置
    var indexPath:IndexPath?
    //图片数据源
    var images:[String] = []
    
    lazy var pictrueCollectionView : UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: PhotoBrowserCollectionViewLayout())
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.bounces = true
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    lazy var imgCountTitleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = UIColor.white
        return label
    }()
    
    init(urls:[String],indexPath:IndexPath) {
        self.indexPath = indexPath
        self.images = urls
        super.init(nibName: nil, bundle: nil)
    }
//
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func loadView() {
//        super.loadView()
//
//        view.frame.size.width += 20
//
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    deinit {
        //释放控制器
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if  images.count > 0{
            pictrueCollectionView.scrollToItem(at: self.indexPath!, at: .centeredHorizontally, animated: false)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let view = getSemicycleButton() {
            view.isHidden = true
        }
    }
}

//MARK:--设置UI
extension CRPictureBrowser{
    
    func setupUI(){
        view.backgroundColor = UIColor.black
        view.addSubview(pictrueCollectionView)
        view.addSubview(imgCountTitleLabel)
        
        //点击背景关闭
        view.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(done))
        view.addGestureRecognizer(gesture)
        
        pictrueCollectionView.snp.makeConstraints { (make) in
            make.right.left.equalTo(view)
            make.bottom.equalTo(-KTabBarHeight)
            make.top.equalTo(KNavHeight)
        }
        imgCountTitleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(KNavHeight - Int(kStatusHeight))
            make.centerX.equalTo(view.snp.centerX)
        }
        pictrueCollectionView.register(CRPictrueBrowerCell.self, forCellWithReuseIdentifier: "CRPictrueBrowerCell")
        pictrueCollectionView.reloadData()
//        imgCountTitleLabel.text =  String(format:"%ld/%ld",(self.indexPath?.row)! + 1,(images?.count ?? 0))
    }
    
}

//MARK: -- UICollectionViewDelegateFlowLayout
extension CRPictureBrowser:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //
        return images.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        let pictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CRPictrueBrowerCell", for: indexPath) as? CRPictrueBrowerCell
        pictureCell?.imageUrl = self.images[indexPath.row]
        pictureCell?.delegate = self
        return pictureCell!
    }
    //collectionViewCell的大小
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: kScreenHeight - CGFloat(KNavHeight) - CGFloat(49))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
//MARK:--点击事件
extension CRPictureBrowser{
    @objc func done(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - 自定义UICollectionViewFlowLayout()
class PhotoBrowserCollectionViewLayout: UICollectionViewFlowLayout {
    
    override func prepare() {
        super.prepare()
        itemSize = (collectionView?.frame.size)!
        minimumLineSpacing = 0
        minimumInteritemSpacing = 0
        scrollDirection = .horizontal
        
        collectionView?.isPagingEnabled = true
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.showsHorizontalScrollIndicator = false
    }
}
extension CRPictureBrowser:PhotoBrowserViewCellDelegate{
    func imageViewClick() {
        self.dismiss(animated: true, completion: nil)
    }
}



