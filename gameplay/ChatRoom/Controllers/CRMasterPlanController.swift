//
//  CRMasterPlanController.swift
//  gameplay
//
//  Created by admin on 2019/12/21.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit
import SnapKit
import MBProgressHUD

class CRMasterPlanController: BaseController {
    var getTutorPlanHandler:(() -> Void)? //拉取导师计划消息
    var msgItems = [CRMessage]() //历史消息
    
    lazy var HUDView:MBProgressHUD = {
        let hud = getHUD()
        return hud
    }()
    
    lazy var tableView:UITableView = {
        let view = UITableView.init(frame: .zero, style: .plain)
        view.separatorStyle = .none
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.register(CRTextMessageCell.self, forCellReuseIdentifier: "CRTextMessageCell")
        
        view.tableFooterView = UIView.init()
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        sestupData()
    }

    func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.title = "导师计划"
        
        self.view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets.zero)
        }
    }
    
    func sestupData() {
        HUDView = getHUD(text: "加载中")
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            self.HUDView.hide(animated: true)
        }
        
        getTutorPlanHandler?()
    }
    
    func updateMsgs(msgs:CRHistoryRecordItem) {
        handleHistoryRecord(historyItems: msgs)
        
        HUDView.hide(animated: true)
        tableView.tableViewDisplayWithLabel(datasCount: msgItems.count,tableView: tableView,message:"暂无导师计划",textColor:UIColor.darkGray,fontSize:20)
        
        tableView.reloadData()
        
        if msgItems.count > 0 {
            tableView.scrollToRow(at: IndexPath.init(row: msgItems.count - 1, section: 0), at: UITableView.ScrollPosition.middle, animated: true)
        }
    }
    
    private func handleHistoryRecord(historyItems:CRHistoryRecordItem) {
        let historys = historyItems.source ?? []
        var firstDateString = ""
        for (index ,messageItem) in historys.enumerated() {
            if index == 0{
                //记录第一条消息的日期
                firstDateString = messageItem.time
                messageItem.isShowTime = true
            }else{
                if Date.isSameDay(firstDateString: firstDateString, secondDateString: messageItem.time){
                    messageItem.isShowTime = false
                }else{
                    firstDateString = messageItem.time
                    messageItem.isShowTime = true
                }
                
            }
            if messageItem.sender == CRDefaults.getUserID(){
                messageItem.ownerType = .TypeSelf
            }else{
                messageItem.ownerType = .Others
            }
            
            if messageItem.msgType == "\(CRMessageType.Text.rawValue)"{
                //文本
                let msgTextItem = CRTextMessageItem()
                msgTextItem.isPlanUser = messageItem.isPlanUser
                msgTextItem.userType = messageItem.userType
                //                msgTextItem.time = messageItem
                msgTextItem.messageType = .Text
                msgTextItem.isShowTime = messageItem.isShowTime
                msgTextItem.ownerType = messageItem.sender == CRDefaults.getUserID() ? .TypeSelf:.Others
                msgTextItem.text = messageItem.context
                msgTextItem.avatar = messageItem.avatar
                msgTextItem.userId = messageItem.sender
                if messageItem.time.length > 5{
                    msgTextItem.sentTime = messageItem.time.subString(start: 10)
                }
                msgTextItem.levelIcon = messageItem.levelIcon
                msgTextItem.levelName = messageItem.levelName
                msgTextItem.time = messageItem.time
                let userNickname = messageItem.nickName
                if userNickname.isEmpty && msgTextItem.ownerType == .Others{
                    msgTextItem.nickName = messageItem.nativeAccount
                }else{
                    if userNickname.isEmpty{
                        msgTextItem.nickName = messageItem.account
                    }else{
                        msgTextItem.nickName = userNickname
                    }
                }
                
                msgItems.append(msgTextItem)
            }
        }
    }
    
    //MARK: 初始化 MBProgressHUD
    func getHUD(text:String = "") -> MBProgressHUD {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = text
        hud.label.textColor = UIColor.white
        hud.bezelView.color = UIColor.black
        hud.label.layoutMargins = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        hud.backgroundView.style = .solidColor
        hud.isUserInteractionEnabled = false
//        hud.activityIndicatorColor = UIColor.white
        hud.graceTime = 0.5
        
        return hud
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension CRMasterPlanController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = msgItems[indexPath.row]
        //显示时间高20 间距5
        return CGFloat(message.messageFrame?.height ?? 0) + 20 + 5
    }
}

extension CRMasterPlanController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = msgItems[indexPath.row]
        
        //文本
        if message.messageType == .Text {
            guard let textMsgCell =  tableView.dequeueReusableCell(withIdentifier: "CRTextMessageCell",for:indexPath) as? CRTextMessageCell else{
                fatalError("dequeueReusableCell CRTextMessageCell failure")
            }
            textMsgCell.setMessage(message: message)
//            textMsgCell.messageDelegate = self
            return textMsgCell
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgItems.count
    }
    
}









