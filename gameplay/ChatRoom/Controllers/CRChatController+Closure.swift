//
//  CRChatController+Closure.swift
//  gameplay
//
//  Created by admin on 2019/11/1.
//  Copyright © 2019年 yibo. All rights reserved.
// closure,处理视图闭包中的事件

import Foundation
import PKHUD

extension CRChatController {

    func setupChatViewClosure() {
        
        let view = self.contentView
        
        //禁言申述
        contentView.inputAreaView.appealHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            weakSelf.appealBandAlert()
        }
        
        //信用版底部反水条初始化数据
        numSelectArea.numSelectView.update_honest_seekbarHandler = {[weak self] (maxOddResult) in
            guard let weakSelf = self else {return}
            weakSelf.numSelectArea.confirmBetArea.peilvBottom.creditbottomTopSlider.setupLogic(odd: maxOddResult)
        }
        
        //tabbar选择
        view.tabClickHandler = {[weak self] (index) in
            if let weakSelf = self {
                weakSelf.tapTabBarWith(index:index)
            }
        }
        //加号中更多功能选择
        view.roomsBottomFucView.selectRoomHandler = {[weak self](name) in
            if let weakSelf = self {
                weakSelf.tapMoreFucItemWith(name: name)
            }
        }
        
        //选择房间
        view.roomsTopView.selectRoomHandler = {[weak self] (index,isMore) in
            if let weakSelf = self {
                
                weakSelf.hideGrayBGView()
                
                if isMore {
                    //进入更多房间列表
                    let vc = CRMoreRoomController()
                    vc.userType = weakSelf.userType
                    vc.roomsList = weakSelf.roomList
                    //MARK:点击房间列表
                    //MARK:确认选择房间
                    vc.insideRoomHandler = { (roomModel) in
                        //切换房间重新初始化socket
//                        weakSelf.netInsideChatRoom(model: roomModel)
                        weakSelf.sendMsgInsideChatRoom(model: roomModel)
                    }
                    
                    weakSelf.navigationController?.pushViewController(vc, animated: true)
                }else {
                    //
                    weakSelf.contentView.roomsTopView.show()
                    weakSelf.contentView.showGrayBackgroundView(show: false)
                    
                    let model = weakSelf.roomList[index]
//                    weakSelf.netInsideChatRoom(model:model,roomIndex: index)
                    weakSelf.sendMsgInsideChatRoom(model: model)
                }
            }
        }
        //会话列表滚动
        view.messageArea.scrollViewWillBeginDraggingHandler = {[weak self] () in
            guard let weakSelf = self else{return}
            weakSelf.contentView.inputAreaView.inputField.endEditing(true)
            weakSelf.contentView.showOrHideMoreFuncView(show: false)
            weakSelf.contentView.showOrHideBetArea()
        }
        
        //跟单消息发送,betModel:元角分模式 1 元，10角，100分
        view.messageArea.followBetHandler = {[weak self] (message,betModel,inIndex) in
            guard let weakSelf = self else {
                return
            }
            weakSelf.followLogic(message:message,betModel:betModel,inIndex: inIndex)
        }
        
        //跟单弹窗显示，回调赋值相关属性
        view.messageArea.showFollowBetHandler = {[weak self] (betInfo) in
            guard let weakSelf = self else {return}
            weakSelf.betInfo = betInfo
            weakSelf.agoShare = betInfo?.ago ?? 0
            weakSelf.getCountDownShareByCpCodeAction()
        }
        
        view.messageArea.followBetPop.followBetKeyboardStatusHandler = {[weak self] (show) in
            guard let weakSelf = self else {return}
            weakSelf.curStatus = .KeyboardFollowField
        }
                
        //切换官方信用,switch_xfwf为off时，则官方为0,信用为1；为on时则相反
        view.betArea.lotteryView.officalOrCreditHandler = {[weak self] (index) in
            guard let weakSelf = self else {return}
            if let models = weakSelf.allLotteries {
                let config = getSystemConfigFromJson()
                let switchState = config?.content.switch_xfwf
                
                var type = index
                if switchState == "on" {
                    if index == 0 {
                        type = 1
                    }else {
                        type = 0
                    }
                }
                
                //type == 0 则官方，type == 1 则信用
                var lotterys = weakSelf.lotteryLogic.prepare_data_for_subpage(type: type, lotteryDatas: models)
                lotterys = lotterys.sorted {(model_1,model_2) -> Bool in
                    return model_1.sortNo > model_2.sortNo
                }
                
                weakSelf.lotteryModels = lotterys
            }
        }
        
        //点击某个彩种
        view.betArea.lotteryView.lotterySelectHandler = {[weak self] (model) in
            guard let weakSelf = self else {return}
            
            weakSelf.clearBetInfo()
            weakSelf.lotteryData = model //替换当前选择的彩种model: LotteryData
            weakSelf.contentView.betArea.lotterySegment?.show(show: true)
            weakSelf.contentView.betArea.showNumSelectArea()
            weakSelf.netAllPlayRules()
            //            weakSelf.netLastOpenResult()
        }
        
        //点击某个玩法
        numSelectArea.betRuleView.ruleSelectHandler = {[weak self] (model,index) in
            guard let weakSelf = self else {return}
            
            weakSelf.numSelectArea.showNumSelectView()
            weakSelf.numSelectArea.topView.updateShowRuleView(isShowRuleView: 1)
            weakSelf.numSelectArea.numSelectView.selectedPlayRulesModel.currentIndex = index
            
            let numSelectView = weakSelf.numSelectArea.numSelectView
            handlePeilvListDatasSuperSet(peilvListDatas: numSelectView.peilvListDatas, peilvListDatasSuperSet: numSelectView.peilvListDatasSuperSet)
            weakSelf.selected_rule_position = index
            weakSelf.selectPlay = model
        }
        
        //点击当前玩法以切换 玩法视图和号码选择视图
        numSelectArea.topView.tapRuleNameHandler = {[weak self] (showViewIndex) in
            guard let weakSelf = self else {return}
            if showViewIndex == 0 {
                weakSelf.numSelectArea.showRulesView()
            }else if showViewIndex == 1 {
                weakSelf.numSelectArea.showNumSelectView()
            }
        }
        
        //0:玩法选择view，1:号码选择view，2:注单确认view
        numSelectArea.topView.showViewIndexChangeHandler = {[weak self] (index) in
            guard let weakSelf = self else {return}
            //            注单确认页的底部按钮标题切换
            weakSelf.numSelectArea.bottomView.updateButtons(isConfirmView: index == 2)
        }
        
        //玩法cell选择状态
        numSelectArea.betRuleView.showCellHandler = {[weak self] (index,isSelected,cell) in
            guard let weakSelf = self else {return}
            let hadSelected = weakSelf.numSelectArea.numSelectView.selectedPlayRulesModel.getCountOfThisPlay(Index: index) > 0
            
            cell.configUI(isSelected: isSelected, hadSelected: hadSelected)
        }
        
        //号码选择页面，取消下注点击
        numSelectArea.bottomView.cancelHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            
            /// 0:玩法选择view，1:号码选择view，2:注单确认view
            let viewIndex = weakSelf.numSelectArea.topView.getShowViewIndex()
            
            if viewIndex == 1 || viewIndex == 0 {
                weakSelf.clearBetInfo(viewIndex:viewIndex)
            }else if viewIndex == 2 {
                //返回号码选择页面
                weakSelf.numSelectArea.topView.updateShowRuleView(isShowRuleView: 1)
                weakSelf.numSelectArea.showNumSelectView()
            }
            view.endEditing(true)
            UIView.animate(withDuration: weakSelf.mKeyBoardAnimateDuration) {
                weakSelf.contentView.betArea.transform = CGAffineTransform.identity
            }
        }
        
        //确定投注点击
        numSelectArea.bottomView.confirmHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            let viewIndex = weakSelf.numSelectArea.topView.getShowViewIndex()
            
            if viewIndex == 1 { //号码选择页面，确定点击事件
                weakSelf.confirmctionInNumSelectArea()
            }else if viewIndex == 2 { //注单确认页面，投注点击事件
                weakSelf.confirmBetInConfirmView()
            }
            view.endEditing(true)
            UIView.animate(withDuration: weakSelf.mKeyBoardAnimateDuration) {
                weakSelf.contentView.betArea.transform = CGAffineTransform.identity
            }
        }
        
        numSelectArea.numSelectView.gotoBetNewOfficalHandler = {[weak self] (_ data:[OrderDataInfo],_ lotCode:String,_ lotName:String,_ subPlayCode:String,_ subPlayName:String,
            _ cpTypeCode:String,_ cpVersion:String,_ officail_odds:[PeilvWebResult],_ icon:String,_ meminfo:Meminfo?,_ officalBonus:Double?) in
            
            guard let weakSlef = self else {return}
            
            openBetOrderPage(controller: weakSlef, data: data,lotCode: lotCode,lotName: lotName,subPlayCode: subPlayCode,subPlayName: subPlayName,cpTypeCode: cpTypeCode,cpVersion:cpVersion,officail_odds:officail_odds,icon: icon,meminfo:meminfo,officalBonus:officalBonus)
        }
        
        numSelectArea.numSelectView.gotoBetNewPeilvHandler = {[weak self] (datas, peilvs,lhcLogic, subPlayName, subPlayCode, cpTypeCode, cpBianHao, current_rate,cpName, cpVersion,lotteryICON) in
            
            guard let weakSlef = self else {return}
            openPeilvBetOrderPage(controller: weakSlef, order: datas, peilvs: peilvs, lhcLogic: lhcLogic, subPlayName: subPlayName, subPlayCode: subPlayCode, cpTypeCode: cpTypeCode, cpBianHao: cpBianHao, current_rate: current_rate, cpName: cpName,cpversion: cpVersion,icon:lotteryICON)
        }
        
        //彩种注数，金额变化
        numSelectArea.numSelectView.betInfoHandler = {[weak self] (zhuShu,money,info) in
            guard let weakSelf = self else {return}
            weakSelf.selectZhuShu = zhuShu
            weakSelf.selectMoney = money
            weakSelf.numSelectArea.bottomView.updateInfoLabel(info: info)
        }
        
        //投注页是否显示快捷 一般按钮
        numSelectArea.numSelectView.shownormalFastButtons = {[weak self] (show) in
            guard let weakSelf = self else {return}
            weakSelf.numSelectArea.topView.shownormalFastButtons(show: show)
        }
        
        //官方版确认投注bottomView
        let officalBottom = numSelectArea.confirmBetArea.officalBottom
        //反水加减button的点击
        officalBottom.onKickbackRaiseSubtractHandler = {[weak self] (tag) in
            
            guard let weakSelf = self else {return}
            let officalBottom = weakSelf.numSelectArea.confirmBetArea.officalBottom
            
            var value = officalBottom.oddSlider.value
            var stepProgress:Float = 0
            if officalBottom.oddSlider.maxRakeback > 0{
                stepProgress = Float(weakSelf.fixRateStep) / officalBottom.oddSlider.maxRakeback
            }
            if weakSelf.fixRateStep > officalBottom.oddSlider.maxRakeback  || weakSelf.fixRateStep == 0{
                var value:Float = 0
                if tag == 201{
                    value = officalBottom.oddSlider.value + 0.01
                }else{
                    value = officalBottom.oddSlider.value - 0.01
                }
                officalBottom.oddSlider.setValue(value, animated: true)
                officalBottom.oddSlider.changeSeekbar(currentProgress: officalBottom.oddSlider.value,reload:true)
                return
            }
            if tag == 200{
                value = value - stepProgress
            }else{
                value = value + stepProgress
            }
            officalBottom.oddSlider.changeSeekbarStepFixRate(fixRate: weakSelf.fixRateStep, currentProgress: value,add: tag == 201,reload: true)
            
        }
        
        //点击官方版本 元角分模式btn
        officalBottom.show_mode_switch_dialogHandler = {[weak self] (button) in
            guard let weakSelf = self else {return}
            
            weakSelf.show_mode_switch_dialog(sender: button, handler: { (title) in
                button.setTitle(title, for: .normal)
                if title == "元" {
                    weakSelf.selectMode = YUAN_MODE
                }else if title == "角" {
                    weakSelf.selectMode = JIAO_MODE
                }else if title == "分" {
                    weakSelf.selectMode = FEN_MODE
                }
            })
        }
        //官方版本输入框事件
        officalBottom.officialInputMoneyHander = {[weak self] (keyboardHeight,animateDuration) in
            
            guard let weakSelf = self else {return}
            //弹出键盘
            weakSelf.mKeyBoardAnimateDuration = animateDuration
            UIView.animate(withDuration: animateDuration, animations: {
                weakSelf.contentView.betArea.transform = CGAffineTransform(translationX: 0,y: -keyboardHeight)
            })
        }
        //官方输入框收键盘
        officalBottom.officiaHiddenKeyboardHander = {[weak self] (duartion) in
            guard let weakSelf = self else {return}
            
            UIView.animate(withDuration: duartion, animations: {
                weakSelf.contentView.betArea.transform = CGAffineTransform.identity
            })
        }
        
        //官方倍数 加
        officalBottom.plusMultipleActionHandler = {[weak self] (beishu_money_input) in
            guard let weakSelf = self else {return}
            
            if isEmptyString(str: beishu_money_input.text!) {
                beishu_money_input.text = "1"
            }else {
                let nowNumP = Int(beishu_money_input.text!)
                if let nowNum = nowNumP {
                    var nowChangeNum = nowNum
                    nowChangeNum += 1
                    beishu_money_input.text = "\(nowChangeNum)"
                    
                    weakSelf.selectedBeishu = nowChangeNum
                    weakSelf.numSelectArea.numSelectView.selectedBeishu = nowChangeNum
                    weakSelf.numSelectArea.numSelectView.updateBottomUI()
                }
            }
        }
        
        officalBottom.officalInputMoneyStringHandler = { [weak self](moneyString) in
            guard let weakSelf = self else {return}
            
            if moneyString == "0"{
                weakSelf.selectedBeishu = 1
            }else{
                weakSelf.selectedBeishu = Int(moneyString) ?? 1
            }
        }
        
        //官方倍数 减
        officalBottom.minusMultipleActionHandler = {[weak self] (beishu_money_input) in
            guard let weakSelf = self else {return}
            
            if isEmptyString(str: beishu_money_input.text!) {
                beishu_money_input.text = "1"
            }else {
                let nowNumP = Int(beishu_money_input.text!)
                if let nowNum = nowNumP {
                    var nowChangeNum = nowNum
                    if nowChangeNum > 1 {
                        nowChangeNum -= 1
                        beishu_money_input.text = "\(nowChangeNum)"
                        
                        weakSelf.selectedBeishu = nowChangeNum
                        weakSelf.numSelectArea.numSelectView.selectedBeishu = nowChangeNum
                        weakSelf.numSelectArea.numSelectView.updateBottomUI()
                    }
                }
            }
        }
        
        
        //信用版确认投注bottomView
        let peilvBottom = numSelectArea.confirmBetArea.peilvBottom
        //加减号改变赔率slider
        peilvBottom.onKickbackRaiseSubtractHandler = {[weak self] (tag) in
            guard let weakSelf = self else {return}
            
            let peilvBottom = weakSelf.numSelectArea.confirmBetArea.peilvBottom
            
            var value = peilvBottom.creditbottomTopSlider.value
            var stepProgress:Float = 0
            if peilvBottom.creditbottomTopSlider.maxRakeback > 0{
                stepProgress = Float(weakSelf.fixRateStep) / peilvBottom.creditbottomTopSlider.maxRakeback
            }
            if weakSelf.fixRateStep > peilvBottom.creditbottomTopSlider.maxRakeback || weakSelf.fixRateStep == 0{
                if tag == 101{
                    value = peilvBottom.creditbottomTopSlider.value + 0.01
                }else{
                    value = peilvBottom.creditbottomTopSlider.value - 0.01
                }
                peilvBottom.creditbottomTopSlider.setValue(value, animated: true)
                peilvBottom.creditbottomTopSlider.changeSeekbar(currentProgress: peilvBottom.creditbottomTopSlider.value,reload:true)
                return
            }
            if tag == 100{
                value = value - stepProgress
            }else{
                value = value + stepProgress
            }
            peilvBottom.creditbottomTopSlider.changeSeekbarStepFixRate(fixRate: weakSelf.fixRateStep, currentProgress: value,add: tag == 101,reload: true)
        }
        
        //MARK: 点击灰置背景，隐藏聊天室历史投注页
        view.hideGrayBackgroundViewHandler = {[weak self]() in
            if let weakSelf = self {
                weakSelf.hideGrayBGView()
            }
        }
        
        //快速金额选择
        peilvBottom.onFastMoneyClickHandler = {[weak self](moneys,sender) in
            guard let weakSelf = self else {return}
            
            let alert = UIAlertController.init(title: "快选金额", message: nil, preferredStyle: .actionSheet)
            for m in moneys{
                let action = UIAlertAction.init(title: m, style: .default, handler: {(action:UIAlertAction) in
                    weakSelf.selectMoneyFromDialog(money: m)
                })
                alert.addAction(action)
            }
            let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            //ipad使用，不加ipad上会崩溃
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender
                popoverController.sourceRect = sender.bounds
            }
            weakSelf.present(alert,animated: true,completion: nil)
        }
        
        peilvBottom.peilvMoneyHandler = {[weak self] (money) in
            guard let weakSelf = self else {return}
            weakSelf.selectMoneyFromDialog(money: money ?? "")
        }
        
        //点击号码球，播放音乐
        numSelectArea.numSelectView.playClickBallSoundHandler = {[weak self] in
            guard let weakSelf = self else {return}
            weakSelf.playVolume()
        }
        
        contentView.confirmCloseWinnerInfoHandler = {[weak self] (winnerView) in
            guard let weakSelf = self else {return}
            let alert = UIAlertController.init(title: "关闭中奖提示将不再提示中奖，是否关闭？", message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
            let confirmAction = UIAlertAction.init(title: "确定", style: .default, handler: { (_) in
                weakSelf.contentView.hasCloseWinnerInfo = true
                winnerView.isHidden = true
            })
            
            alert.addAction(cancelAction)
            alert.addAction(confirmAction)
            weakSelf.present(alert, animated: true, completion: nil)
        }
        
        contentView.messageArea.coverShareTapHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            weakSelf.dismissKeyboard()
        }
        contentView.messageArea.voiceTapHandler = {[weak self] (message,voiceImage) in
            guard let weakSelf = self else {return}
            weakSelf.recordVoiceImage = voiceImage
            if (TLAudioPlayer.shared().player != nil) {
                if TLAudioPlayer.shared().player.isPlaying {
                    //如果正在播话音频 就停下来
                    voiceImage.startPlayingAnimation()
                    TLAudioPlayer.shared().stopPlayingAudio()
                }else {
                    if let path = message.path {
                        TLAudioPlayer.shared().playAudio(atPath: path, complete: { (finished) in
                            //停止动画播放
                            voiceImage.stopPlayingAnimation()
                            print("播放完毕")
                        })
                    }
                }
            }else{
                if let path = message.path {
                    TLAudioPlayer.shared().playAudio(atPath: path, complete: { (finished) in
                        //停止动画播放
                        voiceImage.stopPlayingAnimation()
                        print("播放完毕")
                    })
                }
            }
        }
        
        numSelectArea.confirmBetArea.officalBottom.oddSlider.delegate = self
        numSelectArea.confirmBetArea.peilvBottom.creditbottomTopSlider.delegate = self
        
        // 新增语音15s限制
//        self.contentView.inputAreaView.speakerBar
        
        
        /// 新增快速发言
        self.contentView.roomsBottomFucView.clickSpeakQuicklyHandle = {[weak self]() in
            if let weakSelf = self {
                weakSelf.getSpeakQuicklyListData()
            }
        }
        /// 新增跳转图片收藏列表
        self.contentView.roomsBottomFucView.clickImageCollectListHandle = {[weak self]() in
            if let weakSelf = self {
                let imageCollectVc = CRImageCollectViewController()
                imageCollectVc.networkVc = weakSelf
                weakSelf.navigationController?.pushViewController(imageCollectVc, animated: true)
            }
        }
        
        contentView.messageArea.mergeFollowBethandler = {[weak self] (message) in
            guard let weakSelf = self else {return}
            
            let alert = UIAlertController.init(title: "合并跟单", message: nil, preferredStyle: .alert)
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "请输入单注 金额/倍数"
                textField.keyboardType = .numberPad
            })
            
            let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: {[weak self] (action) in
                guard let weakSelf = self else {return}
                weakSelf.clearBetInfo()
            })
            
            let confirmAction = UIAlertAction.init(title: "确定", style: .default, handler: {[weak self] (action) in
                guard let weakSelf = self else {return}
                
                if let message  = message as? CRShareBetMessageItem {
                    if let text = alert.textFields?.first?.text, let intValue = Int(text) {
                        weakSelf.followMergeLogic(message: message, moneyOrMulti: "\(intValue)")
                    }else {
                        showToast(view: weakSelf.view, txt: "跟单信息有误")
                    }
                }else {
                    showToast(view: weakSelf.view, txt: "跟单信息有误")
                }
            })
            
            alert.addAction(cancelAction)
            alert.addAction(confirmAction)
            
            weakSelf.present(alert, animated: true, completion: {() in
                
            })
        }
        
        //拉取更多历史消息
        self.contentView.messageArea.loadMoreMsgsHandler = {[weak self] () in
            guard let weakSelf = self else {return}
            weakSelf.getMsgHistoryRecordData(start: "\(weakSelf.historyStart)")
        }
    }

    func setupDrawerClosure(drawer:CRDrawer?) {
        drawer?.closeHandler = {() in
            
        }
        
        drawer?.subviewsBGView.header.requestUserInfoHandler = {[weak self] () in //获取用户avatar和nickName
            guard let weakSelf = self else {return}
            weakSelf.sendMsgUserIconAndnickNameData()
            weakSelf.sendMsgRequestUserBalance(userId: CRDefaults.getUserID()) //获取账户余额
        }
        
        drawer?.subviewsBGView.requestOnLineListHandler = {[weak self] () in //获取管理员列表，在线用户,私聊列表
            guard let weakSelf = self else {return}
            weakSelf.sendMsgRequestOnLineList()
            weakSelf.sendMsgRequestManagerList()
            
//            if privatePermission {
            weakSelf.sendMsgRequestPrivateChat(type: "2",model: nil) //拉取私聊列表
//            }
        }
        
        drawer?.subviewsBGView.clickPrivateChatHandler = {[weak self] (model, sectionType) in //点击管理员列表，在线用户列表
            guard let weakSelf = self else {return}
            weakSelf.sendMsgRequestPrivateChat(type: "1",model: model) //初始化私聊
            drawer?.closeAction() //关闭抽屉
        }
        
        drawer?.subviewsBGView.insidePrivateChatHandler = {[weak self] (model) in //点击私聊用户
            guard let weakSelf = self else {return}
            weakSelf.sendMsgInsidePrivateChatGroup(type:"1",model: model) //初始化私聊
            drawer?.closeAction() //关闭抽屉
        }
        
        drawer?.subviewsBGView.updateUnreadMsgNum = {[weak self] (unReadPrivateMsgNum) in
            guard let weakSelf = self else {return}
            weakSelf.navigationBar.newMsgTipIcon.isHidden = unReadPrivateMsgNum == 0
        }
    }

}











