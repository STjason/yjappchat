//
//  QueryResultsCell.swift
//  gameplay
//
//  Created by Lenny's Macbook Air on 2018/6/1.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class QueryResultsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private let label_LotteryName = UILabel()
    private let label_Status = UILabel()
    private let label_Haoma = UILabel()
    private let label_BetsType = UILabel()
    private let label_betMoney = UILabel()
    private let label_Date = UILabel()
    private let label_winningAmountTip = UILabel()
    private let label_Issue = UILabel()
    
    
    private let label_playName = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupNoPictureAlphaBgView(view: self)
        
        contentView.addSubview(label_LotteryName)
        label_LotteryName.whc_Top(14).whc_Left(24).whc_WidthAuto().whc_Height(24)
        label_LotteryName.font = UIFont.systemFont(ofSize: 14)
        label_LotteryName.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_LotteryName.text = "二分彩"
        
        contentView.addSubview(label_Status)
        label_Status.whc_CenterYEqual(label_LotteryName).whc_Right(10).whc_WidthAuto().whc_Height(24)
        label_Status.font = UIFont.systemFont(ofSize: 14)
        label_Status.textColor = UIColor.cc_51()
        label_Status.text = "未开奖"
        
        contentView.addSubview(label_Issue)
        label_Issue.whc_Top(14, toView:label_LotteryName).whc_LeftEqual(label_LotteryName).whc_WidthAuto().whc_Height(10)
        label_Issue.font = UIFont.systemFont(ofSize: 12)
        label_Issue.text = "期号"
        
        contentView.addSubview(label_Haoma)
        label_Haoma.whc_Top(4, toView: label_Issue).whc_LeftEqual(label_Issue).whc_Height(30).whc_Right(20)
        label_Haoma.font = UIFont.systemFont(ofSize: 12)
        label_Haoma.numberOfLines = 0
        label_Haoma.text = "投注号码："
        
        contentView.addSubview(label_betMoney)
        label_betMoney.whc_Top(4, toView: label_Haoma).whc_LeftEqual(label_Haoma).whc_WidthAuto().whc_Height(10)
        label_betMoney.font = UIFont.systemFont(ofSize: 12)
        label_betMoney.text = "下注金额："
        
        contentView.addSubview(label_winningAmountTip)
        label_winningAmountTip.whc_Top(6, toView: label_betMoney).whc_LeftEqual(label_LotteryName).whc_Right(50).whc_Height(10).whc_Bottom(15)
        label_winningAmountTip.font = UIFont.systemFont(ofSize: 12)
        label_winningAmountTip.text = "派奖金额："
        
        contentView.addSubview(label_BetsType)
        label_BetsType.isHidden = true
        label_BetsType.whc_Top(5, toView: label_Status).whc_RightEqual(label_Status).whc_Width(44).whc_Height(24)
        label_BetsType.font = UIFont.systemFont(ofSize: 12)
        label_BetsType.layer.cornerRadius = 3
        label_BetsType.clipsToBounds = true
        
        contentView.addSubview(label_playName)
        label_playName.whc_Bottom(15).whc_RightEqual(label_BetsType).whc_WidthAuto().whc_Height(10)
        label_playName.font = UIFont.systemFont(ofSize: 12)
        
        contentView.addSubview(label_Date)
        label_Date.whc_Bottom(30).whc_RightEqual(label_BetsType).whc_WidthAuto().whc_Height(10)
        label_Date.font = UIFont.systemFont(ofSize: 12)
        
        
        accessoryType = .disclosureIndicator
        selectionStyle = .none
        whc_AddBottomLine(0.5, color: UIColor.cc_224())
        
        label_Date.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_LotteryName.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_Issue.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_Haoma.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_betMoney.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_BetsType.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_betMoney.theme_textColor = "FrostedGlass.normalDarkTextColor"
        label_winningAmountTip.theme_textColor = "FrostedGlass.normalDarkTextColor"
        
        label_playName.theme_textColor = "FrostedGlass.normalDarkTextColor"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIssue(value: String) {
        label_Issue.text = String.init(format: "期号：%@", !isEmptyString(str: value) ? value : "0")
    }
    
    func setLotteryName(value: String) {
        label_LotteryName.text = !isEmptyString(str: value) ? value : "暂无名称"
    }
    func setBetNumbers(value: String){
        label_Haoma.text = !isEmptyString(str: value) ? String.init(format: "投注号码：%@", value) : "暂无号码"
    }
    func setStatus(value: String) {
        label_Status.text = statusToStringChar(status: value)
        if value == "1" || value == "3"{
            label_Status.textColor = UIColor.black
        }else if value == "2" {
             label_Status.textColor = UIColor.red
        }
    }
    func setBalance(value: String) {
        label_betMoney.text = String.init(format: "下注金额：%@元", !isEmptyString(str: value) ? value : "0")
    }

    func setBetsType(value: String) {
        label_BetsType.text = value
    }
    
    func setPaiJiangBalance(value: String) {
        label_betMoney.text = String.init(format: "派奖金额：%@元", !isEmptyString(str: value) ? value : "0")
    }
    func setDate(value: Double) {
        
        label_Date.text = String.init().getStringForTimeStamp(time: TimeInterval(value))
    }
    func setDateStr(date: String) {
        label_Date.text = date
    }
    func setPlayName(value: String) {
        label_playName.text = "玩法:\(value)"
    }
    
    /** 购彩查询的派奖金额 */
    func setWinningAmount(value: String) {
        var money:String = ""
        if !isEmptyString(str: value){
            money = String.init(format: "%.2f", Float(value)!)
        }
        label_winningAmountTip.text = String.init(format: "派奖金额：%@元",!isEmptyString(str: money) ? money : "0")
    }
    
    //状态标识转换为对应字符串
    //// 1未开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6和局
    func statusToStringChar(status:String) -> String {
        switch status {
        case "1":
            return "未开奖"
        case "2":
            return "已中奖"
        case "3":
            return "未中奖"
        case "4":
            return "已撤单"
        case "5":
            return "派奖回滚成功"
        case "6":
            return "和局"
        default:
            return "未开奖"
        }
    }
    
}
