//
//  QueryResultsBasicController.swift
//  gameplay
//
//  Created by Lenny's Macbook Air on 2018/6/1.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import MJRefresh

class QueryResultsBasicController: LennyBasicViewController {
    var supController = UIViewController()
    //条件过滤变量
    var filterUsername:String = ""
    var filterLotCode:String = ""
    var filterStartTime:String = ""
    var filterEndTime:String = ""
    var includeSwitch = "0"
    var goucaiDetailView: GoucaiDetailView =  GoucaiDetailView()
    var selectedView: GoucaiDetailView = GoucaiDetailView()
    var isSelect: Bool = false
    
    var dataRows = [AllLotteryQueryModelRows]()
    var betMoney = ""
    var winMoney = ""
    var profitMoney = ""
    let refreshHeader = MJRefreshNormalHeader()
    let refreshFooter = MJRefreshBackNormalFooter()
    
    var cancelOrderHandler:(() -> Void)?
    
    //MARK: - 刷新
    private func setupRefreshView() {
        refreshHeader.setRefreshingTarget(self, refreshingAction: #selector(headerRefresh))
        refreshFooter.setRefreshingTarget(self, refreshingAction: #selector(footerRefresh))
        self.mainTableView.mj_header = refreshHeader
        self.mainTableView.mj_footer = refreshFooter
    }
    
    /** 由子类重写 */
    @objc  func headerRefresh() {}
    /** 由子类重写 */
    @objc  func footerRefresh() {}
    
    func endRefresh() {
        self.mainTableView.mj_header?.endRefreshing()
        self.mainTableView.mj_footer?.endRefreshing()
    }
    
    func noMoreDataStatusRefresh(noMoreData: Bool = true) {
        if noMoreData {
            self.mainTableView.mj_footer?.endRefreshingWithNoMoreData()
        }else {
            self.mainTableView.mj_footer?.resetNoMoreData()
        }
    }

    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setViews()
        setupRefreshView()
    }

    override var hasNavigationBar: Bool {
        return false
    }
    
    private let mainTableView = UITableView.init(frame: .zero, style: .grouped)
    private func setViews() {
        
        contentView.addSubview(mainTableView)
        setViewBackgroundColorTransparent(view: mainTableView)
//        mainTableView.isSkeletonable = true
        mainTableView.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.estimatedRowHeight = 80
        mainTableView.tableFooterView = UIView()
        mainTableView.rowHeight = UITableView.automaticDimension
        mainTableView.separatorStyle = .none
    }
    
    private func getHeaderView() -> QueryResultsHeader? {
        if let view = Bundle.main.loadNibNamed("QueryResultsHeader", owner: self, options: nil)?.last as? QueryResultsHeader {
            
            view.configWith(betMoney: betMoney, winMoney: winMoney, profit: profitMoney)
            return view
        }
        
        return nil
    }
    
    override func setStaticViewsAndContent() {
        super.setStaticViewsAndContent()
//        loadNetRequestWithViewSkeleton(animate: true)
    }
    
    override func refreshTableViewData() {
        mainTableView.reloadData()
    }
    
    
}
extension QueryResultsBasicController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if (isSelect == false) {
            isSelect = true
            //在延时方法中将isSelect更改为false
            self.showGoucaiDetailView(indexPath: indexPath)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.isSelect = false
            }
        }
        
    }
}

extension QueryResultsBasicController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getHeaderView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 68 //58
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableViewDisplayWithMessage(datasCount: self.dataRows.count,tableView: tableView)
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if let rows = LennyModel.allLotteryQueryModel?.content?.rows {
//            return rows.count
//        }
//        return 0
        
        return self.dataRows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = QueryResultsCell.init(style: .default, reuseIdentifier: "cell")
    
        let rows = self.dataRows[indexPath.row]
        cell.setIssue(value:rows.qiHao!)
        cell.setLotteryName(value: rows.lotName!)
        cell.setBalance(value: rows.buyMoney!)
        cell.setBetNumbers(value: rows.haoMa!)
        cell.setWinningAmount(value: rows.winMoney ?? "")
        cell.setBetsType(value: rows.terminalBetType!)
        cell.setStatus(value: rows.status!)
        cell.setDateStr(date: rows.createTime!)
        cell.setPlayName(value: rows.playName!)
        return cell
    }
    
    //MARK: - 购彩详情列表
    private func showGoucaiDetailView(indexPath: IndexPath){

        let rows = self.dataRows[indexPath.row]
        let dataSource = formatGoucaiDetailDatas(model: rows)
        
        selectedView = GoucaiDetailView()
        
//        switchDetailBetoOrderVersion(status: <#T##String#>)
//        if judgeStatusRevokeOrderToShow(status: rows.status ?? "") {
//             selectedView = GoucaiDetailView(dataSource: dataSource, viewTitle: "购彩详情", bottomHeight: 35)
//        }else {
//            selectedView = GoucaiDetailView(dataSource: dataSource, viewTitle: "购彩详情", bottomHeight: 0)
//        }
        
        let version = switchDetailBetoOrderVersion(status: rows.status ?? "")
        if  version != "v4" {
            selectedView = GoucaiDetailView(dataSource: dataSource, viewTitle: "购彩详情", bottomHeight: 35,version:version)
        }else {
            selectedView = GoucaiDetailView(dataSource: dataSource, viewTitle: "购彩详情", bottomHeight: 0)
        }
        
        self.view.window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width - 100).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            self.selectedView.transform = CGAffineTransform.identity
        }) { (_) in
        }
        
        setupBottomButton(indexPath: indexPath,version:version)
    }
    
    private func setupBottomButton(indexPath:IndexPath,version:String) {
        
        let withdrawalButton = UIButton()
        withdrawalButton.tag = 100 + indexPath.row
        withdrawalButton.addTarget(self, action: #selector(QueryResultsBasicController.withdrawAction(button:)), for: .touchUpInside)
        withdrawalButton.setTitleColor(UIColor.white, for: .normal)
        withdrawalButton.theme_backgroundColor = "Global.themeColor"
        withdrawalButton.setTitle("撤 单", for: .normal)
        withdrawalButton.titleLabel?.font = UIFont.systemFont(ofSize: 17.0)
        selectedView.bringSubviewToFront( withdrawalButton)
        selectedView.addSubview(withdrawalButton)
        
        let goonBetButton = UIButton()
        goonBetButton.tag = 1000 + indexPath.row
        goonBetButton.addTarget(self, action: #selector(QueryResultsBasicController.goonBetAction(button:)), for: .touchUpInside)
        goonBetButton.setTitleColor(UIColor.white, for: .normal)
        goonBetButton.theme_backgroundColor = "Global.themeColor"
        goonBetButton.setTitle("继续下注", for: .normal)
        goonBetButton.titleLabel?.font = UIFont.systemFont(ofSize: 17.0)
        selectedView.bringSubviewToFront( goonBetButton)
        selectedView.addSubview(goonBetButton)
        
        goucaiDetailView = selectedView
        
        if version == "v1" {            
            withdrawalButton.snp.makeConstraints { (make) in
                make.left.equalTo(0)
                make.bottom.equalTo(0)
                make.height.equalTo(35)
            }
            
            goonBetButton.snp.makeConstraints { (make) in
                make.left.equalTo(withdrawalButton.snp.right)
                make.width.equalTo(withdrawalButton)
                make.right.equalTo(0)
                make.bottom.equalTo(0)
                make.height.equalTo(35)
            }
            
        }else if version == "v2" {
            withdrawalButton.snp.makeConstraints { (make) in
                make.leading.trailing.equalTo(0)
                make.bottom.equalTo(0)
                make.height.equalTo(35)
            }
            goonBetButton.snp.makeConstraints { (maker) in
                maker.width.height.equalTo(0)
            }
        }else if version == "v3" {
            goonBetButton.snp.makeConstraints { (make) in
                make.leading.trailing.equalTo(0)
                make.bottom.equalTo(0)
                make.height.equalTo(35)
            }
            withdrawalButton.snp.makeConstraints { (maker) in
                maker.width.height.equalTo(0)
            }
        }
        

    }
    
    func withDrawalWebMethod(index: Int) {
        
        goucaiDetailView.dismissGroupDetailView()
        
        let rows = self.dataRows[index]
        var order = ""
        if let o = rows.orderId{
            order = o
        }
        
        var lotCode = ""
        if let code = rows.lotCode{
            lotCode = code
        }
        
        let isTest = YiboPreference.getAccountMode() == GUEST_TYPE
        
        let parameters = ["isTest":isTest,"lotCode":lotCode,"orderIds":order] as [String : Any]
        print("parame === ",parameters)
        self.request(frontDialog: true, method: .post,url: LOTTERY_CANCEL_ORDER_URL,params:parameters,
                     callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "提交失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        
                        if let result = CancelOrderWraper.deserialize(from: resultJson){
                            if result.success{
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                                if result.content{
                                    showToast(view: self.view, txt: "撤单成功")
//                                    self.loadNetRequestWithViewSkeleton(animate: true)
                                    self.cancelOrderHandler?()
                                }else{
                                    showToast(view: self.view, txt: "撤单失败")
                                }
                            }else{
                                if !isEmptyString(str: result.msg){
                                    showToast(view: self.view, txt: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "撤单失败"))
                                }
                                if result.code == 0  || result.code == -1{
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
        })
    }
    
    
    //MARK: - 点击事件
    @objc func withdrawAction(button:UIButton) {
        
        goucaiDetailView.dismissGroupDetailView()
        
        let message = "确定要撤单吗？"
        let alertController = UIAlertController(title: "温馨提示",
                                                message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "好的", style: .default, handler: {
            action in
            self.withDrawalWebMethod(index: button.tag - 100)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func goonBetAction(button:UIButton) {
        
        goucaiDetailView.dismissGroupDetailView()
        
        self.goonBetRealAction(index: button.tag - 1000)
    }
    
    private func goonBetRealAction(index:Int) {
        let rows = self.dataRows[index]
        
        let lotVersion = rows.lotVersion ?? ""
        if  lotVersion == "1" {
            
            let orderDataInfo = formatOrderData(rows: rows,lotVersion:1)
            openBetOrderPage(controller: supController, data: [orderDataInfo], lotCode: rows.lotCode ?? "", lotName: rows.lotName ?? "", subPlayCode: rows.oddsCode ?? "", subPlayName: rows.playName ?? "", cpTypeCode: rows.lotType ?? "", cpVersion: rows.lotVersion ?? "", officail_odds: [PeilvWebResult()], meminfo: nil,fromHistory:true, officalBonus: nil)
            print("")
            
        }else if lotVersion == "2" {
            let orderDataInfo = formatOrderData(rows: rows,lotVersion:2)
            
            let logic = LHCLogic2()
            
            var current_rate:Float = 0.0
            if let p = Float(rows.kickback ?? "0") {
                current_rate = p
            }
            
            openPeilvBetOrderPage(controller: supController, order: [orderDataInfo], peilvs: [BcLotteryPlay()], lhcLogic: logic, subPlayName: rows.playName ?? "", subPlayCode: "", cpTypeCode: rows.lotType ?? "", cpBianHao: rows.lotCode ?? "", current_rate: current_rate, cpName: rows.lotName ?? "",fromHistory:true)
        }
    }
    
    
    /// 格式化投注数据
    ///
    /// - Parameters:
    ///   - lotVersion: 1 官方，3 信用
    private func formatOrderData(rows:AllLotteryQueryModelRows,lotVersion:Int) -> OrderDataInfo {
        let orderDataInfo = OrderDataInfo()
        orderDataInfo.subPlayName = rows.playName ?? ""
        orderDataInfo.numbers = rows.haoMa ?? ""
        orderDataInfo.user = rows.username ?? ""
        orderDataInfo.lotcode = rows.lotCode ?? ""
        orderDataInfo.lottype = rows.lotType ?? ""
        orderDataInfo.cpCode = rows.oddsCode ?? ""
        orderDataInfo.oddsCode = rows.playCode ?? ""
        
        if let mode = Int(rows.model ?? "0") {
            orderDataInfo.mode = mode
        }
        
        if let zhushu = Int(rows.buyZhuShu ?? "0") {
            orderDataInfo.zhushu = zhushu
        }
        
        if let zhushu = Int( rows.multiple ?? "" ) {
            orderDataInfo.beishu = zhushu
        }
        
        if let rate = Double(rows.kickback ?? "") {
            orderDataInfo.rate = rate
        }
        
        if let money = Double(rows.buyMoney ?? "0") {
            orderDataInfo.money = money
        }
        
//        orderDataInfo.oddsName = rows.playName ?? ""
        
        return orderDataInfo
    }
    
    private func formatGoucaiDetailDatas(model: AllLotteryQueryModelRows) -> [String] {
        
        var array = [String]()
        array.append(String.init(format: "订单号: %@", model.orderId!))
        //单价 = 购买金额 / 倍数/购买注数
        if !isEmptyString(str: model.buyMoney!) && !isEmptyString(str: model.multiple!) &&
            !isEmptyString(str: model.buyZhuShu!){
            let single = Float(model.buyMoney!)!/Float(model.multiple!)!/Float(model.buyZhuShu!)!
            array.append(String.init(format: "单注金额: %.2f元", single))
        }
        array.append(String.init(format: "下注时间: %@", String.init().getStringForTimeStamp(time: TimeInterval(model.createTimeLong))))
        array.append(String.init(format: "投注注数: %@", model.buyZhuShu!))
        array.append(String.init(format: "彩种: %@", model.lotName!))
        if let haoma = model.openHaoMa{
            array.append(String.init(format: "开奖号码: %@", haoma))
        }
        if model.lotVersion == VERSION_1{
            array.append(String.init(format: "倍数: %@倍", model.multiple!))
            var modelStr = "元"
            if model.model! == "1"{
                modelStr = "元"
            }else if model.model! == "10"{
                modelStr = "角"
            }else if model.model! == "100"{
                modelStr = "分"
            }
            
            var jjfFinal = "单注奖金: "
            
            if let jj = model.buyOdds{
//                let mode:Float = Float(model.model!)!
//                let jjf:Float = Float(jj)!/mode
                
                if let mode:Float = Float(model.model!)
                {
                    
                    if let jjfP:Float = Float(jj)
                    {
                        let jjf = Float(jjfP / mode)
                        
                        
                        jjfFinal = String.init(format: "单注奖金: %@元", String.init(format: "%.3f", jjf))
                    }
                }
                
//                array.append(String.init(format: "单注奖金: %@", String.init(format: "%.3f", jjf)))
            }
            
            array.append(jjfFinal)
            array.append(String.init(format: "倍彩模式: %@", modelStr))
        }
        array.append(String.init(format: "期号: %@", model.qiHao!))
        array.append(String.init(format: "投注总额: %@元", model.buyMoney!))
        array.append(String.init(format: "玩法: %@", model.playName!))
//        array.append(String.init(format: "奖金:%@元", model.winMoney!))
        array.append(String.init(format: "投注号码: %@", model.haoMa!))
        
        let str = convertRatebackStatus(model: model)
        let rateback = "返水: \(str)(\(model.kickback!)%)"
        array.append(rateback)
        
        if model.lotVersion == VERSION_2 {
            let peilvStr = "赔率: \(model.buyOdds ?? "")"
            array.append(peilvStr)
        }
        
        array.append(String.init(format: "状态: %@", statusToStringChar(status: model.status!)))
        if let zjje = model.winMoney{
//            array.append(String.init(format: "中奖金额:%@元", !isEmptyString(str: zjje) ? zjje:"0"))
//            var myvalue:Double = 0.0
//            if zjje != ""{
//                myvalue = Double(zjje)!
//            }
            array.append("中奖金额: \(zjje)")
        }
//        if !isEmptyString(str: model.buyMoney!) && !isEmptyString(str: model.multiple!){
//            let win = Float((!isEmptyString(str: model.winMoney!) ? model.winMoney! : "0"))!
//            let buy = Float((!isEmptyString(str: model.buyMoney!) ? model.buyMoney! : "0"))!
//            array.append(String.init(format: "盈亏:%.2f元", (win-buy)))
//        }
        
        
        return array
    }
    
    //状态标识转换为对应字符串
    //// 1未开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6和局
    private func statusToStringChar(status:String) -> String {
        switch status {
        case "1":
            return "未开奖"
        case "2":
            return "已中奖"
        case "3":
            return "未中奖"
        case "4":
            return "已撤单"
        case "5":
            return "派奖回滚成功"
        case "6":
            return "和局"
        default:
            return "未开奖"
        }
    }
    
}
