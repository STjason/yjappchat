//
//  QueryResultsAllController.swift
//  gameplay
//
//  Created by Lenny's Macbook Air on 2018/6/1.
//  Copyright © 2018年 yibo. All rights reserved.
// 全部彩票注单

import UIKit

class QueryResultsAllController: QueryResultsBasicController {
    

    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setViewBackgroundColorTransparent(view: self.view)
        
        loadNetRequest(isLoadMoreData: false)
        
        self.cancelOrderHandler = {() ->Void in
            self.loadNetRequest(isLoadMoreData: false)
        }
    }
    
    func loadNetRequest(isLoadMoreData: Bool,pageSize: Int = defaultCountOfRequestPagesize) {
        
        var pageNumber = 1
        if isLoadMoreData {
            if self.dataRows.count % pageSize == 0 {
                pageNumber = self.dataRows.count / pageSize + 1
            }else {
                super.noMoreDataStatusRefresh()
                return
            }
        }
        
        LennyNetworkRequest.obtainLotteryQuery(lotteryCode: self.filterLotCode, startTime: self.filterStartTime, endTime: self.filterEndTime, qihao: "", version: "", order: "", zuihaoOrder: "", status: "", queryType: "1", playCode: "", include: includeSwitch, username: self.filterUsername,pageSize: "\(pageSize)",pageNumber: "\(pageNumber)") { (model) in
            
            super.endRefresh()
            
            if model?.code == 0 && !(model?.success)!{
                loginWhenSessionInvalid(controller: self)
                return
            }
            
            guard let modelP = model else {return}
            guard let contentP = modelP.content else {return}
            guard let rowsP = contentP.rows else {return}
            
            if let aggsData = contentP.aggsData {
                self.betMoney = "\(aggsData.bettingMoneyCount)"
                self.winMoney = "\(aggsData.winMoneyCount)"
                let mstr = self.calculateProfitMoney(originalProfit:(aggsData.winMoneyCount - aggsData.bettingMoneyCount))
                self.profitMoney = String.init(format:"%.2f",Float(mstr)!)
            }
            
            
            if !isLoadMoreData {
                self.dataRows.removeAll()
            }
            self.dataRows += rowsP
            self.noMoreDataStatusRefresh(noMoreData: rowsP.count % pageSize != 0 || rowsP.count == 0)
            
            self.view.hideSkeleton()
            self.refreshTableViewData()
        }
    }
    
    private func calculateProfitMoney(originalProfit:Double) -> String {
        
        var fixValue:Double = 0
        for (_,model) in self.dataRows.enumerated() {
            if model.status == "6" {
                if let value = Double(model.buyMoney ?? "0") {
                    fixValue += value
                }
            }
        }
        
        
        return "\(originalProfit + fixValue)"
    }
    
    @objc override func headerRefresh() {
        self.loadNetRequest(isLoadMoreData: false)
    }
    
    @objc override func footerRefresh() {
        self.loadNetRequest(isLoadMoreData: true)
    }
    
}
