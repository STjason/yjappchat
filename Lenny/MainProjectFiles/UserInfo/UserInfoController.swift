//
//  UserInfoController.swift
//  SinglePages
//
//  Created by Lenny's Macbook Air on 2018/5/4.
//  Copyright © 2018年 Lenny. All rights reserved.
//

import UIKit

class UserInfoController: LennyBasicViewController {

    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        setupthemeBgView(view: self.view, alpha: 0)
        setViews()
        loadNetRequestWithViewSkeleton(animate: true)
        if let config = getSystemConfigFromJson(){
            if config.content != nil{
                modifyInfoAfterFirstModify = config.content.modify_person_info_after_first_modify_switch == "on"
            }
        }
    }

    private var mainTableView: UITableView!
    var model:UserInfoModel!
    var modifyInfoAfterFirstModify = false//个人资料填写后不能再修改
    let titles = ["用户昵称: ","真实姓名: ","用户手机: ","用户邮箱: ","用户QQ: ","用户微信: "]
    
    private func setViews() {
        self.title = "用户资料"
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
//        self.navigationController?.navigationBar.barTintColor = UIColor.ccolor(with: 236, g: 40, b: 40)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        mainTableView = UITableView()
        setViewBackgroundColorTransparent(view: mainTableView)
        contentView.addSubview(mainTableView)
        mainTableView.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
        mainTableView.delegate = self
        mainTableView.dataSource = self
//        mainTableView.tableFooterView = UIView()
        mainTableView.separatorStyle = .none
        mainTableView.allowsSelection = false
    }
    
    
    private func showAlertController(index: Int, hander:String) {
        var title = ""
        var typeContents = ""
        let res = hander.suffix(2)
        if index == 1 {title = "真实姓名" + res;typeContents = "realname"}
        else if index == 2 {title = "手机号" + res;typeContents = "phone"}
        else if index == 3 {title = "邮箱" + res;typeContents = "email"}
        else if index == 4 {title = "QQ" + res;typeContents = "QQ"}
        else if index == 5 {title = "微信" + res;typeContents = "wechat"}
        
        let alert = UIAlertController.init(title: title, message: nil, preferredStyle: .alert)
        let confirmAction = UIAlertAction.init(title: "确定", style: .default) { _ in
            
            let originalContents = alert.textFields![0].text!
            switch index {
            case 1:
                if !matchChineseName(message: originalContents) {
                    showToast(view: self.view, txt: "真实姓名格式不对")
                    return
                }
            case 2:
                if !matchPhone(message: originalContents) {
                    showToast(view: self.view, txt: "手机号格式不对")
                    return
                }
            case 3:
                if !matchEmail(message: originalContents) {
                    showToast(view: self.view, txt: "邮箱格式不对")
                    return
                }
            case 4:
                if !matchQQ(message: originalContents) {
                    showToast(view: self.view, txt: "QQ格式不对")
                    return
                }
            case 5:
                if !matchWeiChat(message: originalContents) {
                    showToast(view: self.view, txt: "微信格式不对")
                    return
                }
            default:
                print("error")
            }
            
            if index == 1 {
                if let originalContents = alert.textFields![0].text {
                    let params = ["realName": originalContents]
                    self.commitAction(index: index,params: params)
                }
                return
            }

            if let originalContents = alert.textFields![0].text {
                var newContents = ""
                if (alert.textFields?.count)! > 1 {
                    if let temp = alert.textFields![1].text {
                        newContents = temp
                    }
                }
                
            if hander == "去设置" {
//                let params = ["newInfo": newContents,"oldInfo": originalContents,"type": typeContents]
                let params = ["newInfo": originalContents,"oldInfo": "","type": typeContents]
                self.commitAction(index: index,params: params)
                return
            }

            let params = ["newInfo": newContents,"oldInfo": originalContents,"type": typeContents]
            self.commitAction(index: index,params: params)
            }
        }
        
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel) { (_) in}
        
        if hander == "去设置" {
            alert.addTextField { (originalContents) in
                originalContents.clearButtonMode = .always
                if index == 1 {originalContents.placeholder = "请输入真实姓名"}
                else if index == 2 {originalContents.placeholder = "请输入手机号";originalContents.keyboardType = .numberPad}
                else if index == 3 {originalContents.placeholder = "请输入邮箱"}
                else if index == 4 {originalContents.placeholder = "请输入QQ号"}
                else if index == 5 {originalContents.placeholder = "请输入微信号"}
            }

        }else if hander == "去修改" {
            alert.addTextField { (originalContents) in
                originalContents.clearButtonMode = .always
                if index == 1 {originalContents.placeholder = "请输入真实姓名"}
                else if index == 2 {originalContents.placeholder = "请输入原手机号";originalContents.keyboardType = .numberPad}
                else if index == 3 {originalContents.placeholder = "请输入原邮箱"}
                else if index == 4 {originalContents.placeholder = "请输入原QQ号"}
                else if index == 5 {originalContents.placeholder = "请输入原微信号"}
            }
            alert.addTextField { (newContents) in
                if index == 1 {newContents.placeholder = "请输入真实姓名"}
                else if index == 2 {newContents.placeholder = "请输入新手机号";newContents.keyboardType = .numberPad}
                else if index == 3 {newContents.placeholder = "请输入新邮箱"}
                else if index == 4 {newContents.placeholder = "请输入新QQ号"}
                else if index == 5 {newContents.placeholder = "请输入新微信号"}
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        self.present(alert, animated: true) {}
    }
    
    private func commitAction(index: Int,params: Dictionary<String, Any>) {
        
        if index == 1 {
            request(frontDialog: true,method: .post, loadTextStr:"设置中...",url:API_UPDATEUSERREALNAME,params:params,
                    callback: {(resultJson:String,resultStatus:Bool)->Void in
                        if !resultStatus {
                            if resultJson.isEmpty {
                                showToast(view: self.view, txt: convertString(string: "获取数据失败"))
                            }else{
                                showToast(view: self.view, txt: resultJson)
                            }
                            return
                        }
                        if let result = LoginOutWrapper.deserialize(from: resultJson){
                            if result.success{
                                if !isEmptyString(str: result.accessToken){
                                    YiboPreference.setToken(value: result.accessToken as AnyObject)
                                }
                                showToast(view: self.view, txt: "设置成功")
//                                self.navigationController?.popViewController(animated: true)
                                self.loadNetRequestWithViewSkeleton(animate: true)
                            }else{
                                if !isEmptyString(str: result.msg){
                                    showToast(view: self.view, txt: result.msg)
                                }else{
                                    showToast(view: self.view, txt: convertString(string: "获取数据失败"))
                                }
                                if result.code == 0  || result.code == -1{
                                    loginWhenSessionInvalid(controller: self)
                                }
                            }
                        }
            })
            
            return
        }
        
        request(frontDialog: true,method: .post, loadTextStr:"修改中...",url:API_UpdateUSER_INFO,params:params,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if resultJson.isEmpty {
                            showToast(view: self.view, txt: convertString(string: "获取数据失败"))
                        }else{
                            showToast(view: self.view, txt: resultJson)
                        }
                        return
                    }
                    if let result = LoginOutWrapper.deserialize(from: resultJson){
                        if result.success{
                            if !isEmptyString(str: result.accessToken){
                                YiboPreference.setToken(value: result.accessToken as AnyObject)
                            }
                            showToast(view: self.view, txt: "操作成功")
//                            self.navigationController?.popViewController(animated: true)
                            self.loadNetRequestWithViewSkeleton(animate: true)
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "获取数据失败"))
                            }
                            if result.code == 0  || result.code == -1{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    override func loadNetRequestWithViewSkeleton(animate: Bool) {
        super.loadNetRequestWithViewSkeleton(animate: true)
        LennyNetworkRequest.obtainUserinfo(){
            (model) in
            if model?.code == 0 && !(model?.success)!{
                loginWhenSessionInvalid(controller: self)
                return
            }
            self.model = model
            self.view.hideSkeleton()
            self.refreshTableViewData()
        }
    }
    
    override func refreshTableViewData() {
        mainTableView.reloadData()
    }
}

extension UserInfoController: UITableViewDelegate {
    
}

extension UserInfoController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let config = getSystemConfigFromJson()
        {
            if config.content != nil
            {
                if config.content.switch_communication == "off"
                {
                    let title = titles[indexPath.row]
                    if title == "用户手机: " || title == "用户邮箱: " || title == "用户QQ: " || title == "用户微信: " {
                        return 0
                    }
                }
            }
        }
        
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc private func editContents(button: UIButton) {
        let tag = button.tag - 20000
        switch tag {
        case 0:
            print("不能更改")
        case 1,2,3,4,5:
            showAlertController(index: tag, hander: (button.titleLabel?.text)!)
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if let cellP = cell {
            setViewBackgroundColorTransparent(view: cellP)
        }
        
//        if cell == nil {
            guard let model = self.model else {return UITableViewCell()}
            guard let content = model.content else {return UITableViewCell()}
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
            setupNoPictureAlphaBgView(view: cell)
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 12)
            cell?.textLabel?.textColor = UIColor.ccolor(with: 51, g: 51, b: 51)
            
            cell?.contentView.whc_AddBottomLine(0.5, color: UIColor.ccolor(with: 224, g: 224, b: 224))
            let textField = UITextField()
            cell?.contentView.addSubview(textField)
            textField.whc_CenterY(0).whc_Left(120).whc_Right(20).whc_Top(5).whc_Bottom(5)
            textField.borderStyle = .none
            textField.tag = indexPath.row + 10000
            textField.isUserInteractionEnabled = false
            
            let editButton = UIButton.init()
            editButton.isHidden = false
            cell?.contentView.addSubview(editButton)
            editButton.setBackgroundImage(UIImage(named: "fast_money_normal_bg"), for: .normal)
//            editButton.layer.borderWidth = 1
//            editButton.layer.borderColor = UIColor.gray.cgColor
            editButton.setTitleColor(.black, for: .normal)
            editButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            editButton.setTitle("修改", for: .normal)
            editButton.tag = indexPath.row + 20000
            editButton.addTarget(self, action: #selector(self.editContents), for: .touchUpInside)
            editButton.whc_CenterY(0).whc_Right(10).whc_Top(8).whc_Bottom(8).whc_Width(45)
        
        cell?.textLabel?.text = titles[indexPath.row]
        
            switch indexPath.row {
            case 0:
                editButton.isHidden = true
                cell?.imageView?.image = UIImage.init(named: "usericon")
                textField.backgroundColor = .clear
                textField.text = (content.username)!
                break
            case 1:
                cell?.imageView?.image = UIImage.init(named: "realname")
                textField.text = (content.realName)!
                if content.realName == nil || content.realName == "" {
                    editButton.isHidden = false
                    editButton.setTitle("去设置", for: .normal)
                }else{
                    editButton.isHidden = true
                }
                break
            case 2:
                cell?.imageView?.image = UIImage.init(named: "userphone")
                textField.text = (content.phone)!
                if content.phone == nil || content.phone == "" {
                    editButton.setTitle("去设置", for: .normal)
                }else {
                    if modifyInfoAfterFirstModify{
                        editButton.isHidden = true
                    }else{
                        editButton.isHidden = false
                    }
                    editButton.setTitle("去修改", for: .normal)
                }
                break
            case 3:
                cell?.imageView?.image = UIImage.init(named: "email")
                textField.text = (content.email)!
                if content.email == nil || content.email == "" {
                    editButton.setTitle("去设置", for: .normal)
                }else {
                    if modifyInfoAfterFirstModify{
                        editButton.isHidden = true
                    }else{
                        editButton.isHidden = false
                    }
                    editButton.setTitle("去修改", for: .normal)
                }
                break
            case 4:
                cell?.imageView?.image = UIImage.init(named: "QQ")
                textField.text = (content.qq)!
                if content.qq == nil || content.qq == "" {
                    editButton.setTitle("去设置", for: .normal)
                }else {
                    if modifyInfoAfterFirstModify{
                        editButton.isHidden = true
                    }else{
                        editButton.isHidden = false
                    }
                    editButton.setTitle("去修改", for: .normal)
                }
                break
            case 5:
                cell?.imageView?.image = UIImage.init(named: "wechat")
                textField.text = (content.wechat)!
                if content.wechat == nil || content.wechat == "" {
                    editButton.setTitle("去设置", for: .normal)
                }else {
                    if modifyInfoAfterFirstModify{
                        editButton.isHidden = true
                    }else{
                        editButton.isHidden = false
                    }
                    editButton.setTitle("去修改", for: .normal)
                }
            default:
                break
            }
            
            
//        }
        return cell!
    }
}
