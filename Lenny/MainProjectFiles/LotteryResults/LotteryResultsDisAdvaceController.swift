//
//  LotteryResultsDisAdvaceController.swift
//  gameplay
//
//  Created by admin on 2018/10/30.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LotteryResultsDisAdvaceController: LotteryResultsDisBaseController {
    
    //MARK: -lazy
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout.init()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = UIColor.clear
        
        let nib = UINib.init(nibName: "LotteryResultsAdvaceCell", bundle: nil)
        collection.register(nib, forCellWithReuseIdentifier: "lotteryResultsAdvaceCell")
        
        collection.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        
        return collection
    }()
    
    //MARK: -life cycle
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(themeChanged), name: Notification.Name(rawValue: ThemeUpdateNotification), object: nil)
    
        setupCollection()
    }
    
    //MARK: -UI
    private func setupCollection()
    {
        self.view.addSubview(collectionView)
        
    }
    
    override func loadLotteryResults(pageNumber: Int, code:String) {
        
        LennyNetworkRequest.obtainLotteryResults(pageNumber: pageNumber, gameCode: code) { [weak self](model) in
            DispatchQueue.main.async {
                if model == nil{
                    return
                }
                if model?.content == nil{
                    return
                }
                if model?.content?.history == nil{
                    return
                }
                if model?.content?.history?.list == nil{
                    return
                }
                self?.analysis(list: (model!.content?.history?.list)!)
            }
        }
    }
    
    // 底部刷新
    @objc fileprivate func footerRefresh(){
        pageNum += 1
        loadLotteryResults(pageNumber: pageNum, code: code)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if shouldLoadMoreData
        {
            shouldLoadMoreData = false
            if (collectionView.contentOffset.y + collectionView.height) > (collectionView.contentSize.height - 10)
            {
                footerRefresh()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.shouldLoadMoreData = true
            }
        }
        
    }
    
    //解析请求数据结果 AllLotteryResultsModel
    func analysis(list:[AllLotteryResultsList]) {
        if list.count == 0 {
//            showToast(view: self.view, txt: "没有更多数据")
        }else {
            self.dataArr += list
            self.collectionView.reloadData()
        }
        
    }
    
    
    //MARK: -事件
    @objc private func themeChanged() {
        refreshCollectionView()
    }
    
    private func refreshCollectionView()
    {
        self.collectionView.reloadData()
    }
    
    //MARK: -layout
    override func viewDidLayoutSubviews() {
        var titleHeight:CGFloat = 90
        if lotType == "6" || lotType == "66" || lotType == "9" || lotType == "7"
        {
            titleHeight = 50
        }
        
        if !isAttachInTabBar{
            collectionView.frame  = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 64 - titleHeight)
        }else {
            collectionView.frame  = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 64 - titleHeight - 49)
        }
    }
    
    private func addHeaderContent() -> UIView {
        let view = Bundle.main.loadNibNamed("LotResAdvanceHeader", owner: self, options: nil)?.last as! LotResAdvanceHeader
        view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 44)
        view.themeBgView.theme_alpha = "FrostedGlass.ColorImageCoverAlphHigerForGlass"
        view.themeBgView.theme_backgroundColor = "FrostedGlass.subColorImageCoverColorNormalGray"
        for index in 0..<5
        {
            let width = LottResultsDisLogic.getWidthWithIndex(index: index, lotType: lotType)
            view.configWidthWithIndex(index: index, width: width)
            
        }
        return view
    }
}

extension LotteryResultsDisAdvaceController:UICollectionViewDelegate
{
    
}


extension LotteryResultsDisAdvaceController:UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lotteryResultsAdvaceCell", for: indexPath) as? LotteryResultsAdvaceCell else {fatalError("dequeueReusableCell lotteryResultsAdvaceCell failure")}
        
        //定位crash dataArr为空
        if !(self.dataArr.count > 0)
        {
            return cell
        }
        
        let item = self.dataArr[indexPath.section]
        
        if indexPath.row == 0 //期号
        {
            cell.configWithTitle(title: trimQihao(currentQihao: item.period!,length: 4),indexPath: indexPath)
        }else if indexPath.row == 1 //开奖号码
        {
            let width = LottResultsDisLogic.getWidthWithIndex(index: indexPath.row, lotType: lotType)
            cell.configBalls(balls: item.results(), indexPath: indexPath,width:width,cpVersion: lotType,year:item.year)
        }else if indexPath.row == 2 //和值
        {
            let title = "\(LottResultsDisLogic.sumOfItems(lotType: lotType,item: item))"
            cell.configWithTitle(title: title ,indexPath: indexPath)
        }else if indexPath.row == 3 //大小
        {   
            cell.configCircleWithTitle(title: LottResultsDisLogic.getBigOrSmall(lotType: lotType, item: item), indexPath: indexPath)
        }else if indexPath.row == 4 //单双
        {
            cell.configCircleWithTitle(title: LottResultsDisLogic.getParity(lotType: lotType,item: item),indexPath: indexPath)
        }
        
        cell.configTheme(indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader{
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
            //添加头部视图
            header.addSubview(self.addHeaderContent())
            return header
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return section == 0 ? CGSize.init(width: screenWidth, height: 44) : CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return getDatasCount()
    }
    
    private func getDatasCount() -> Int
    {
        if LennyModel.allLotteryResultsModel?.content?.history?.list == nil
        {
            return 0
        }
        
        return self.dataArr.count
    }
}

extension LotteryResultsDisAdvaceController:UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: LottResultsDisLogic.getWidthWithIndex(index: indexPath.row,lotType: lotType), height: 50)
    }
}
