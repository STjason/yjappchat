//
//  LottResultsDisLogic.swift
//  gameplay
//
//  Created by admin on 2018/11/1.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LottResultsDisLogic: NSObject
{
    //MARK:  -数据逻辑
    //是否是 豹子
    static func isLeopard(values:[String]) -> Bool {
        if let sys = getSystemConfigFromJson()
        {
            if sys.content.k3_baozi_daXiaoDanShuang == "off" {
                var allValueEqual = false
                for (index,value) in values.enumerated()
                {
                    if index > 0
                    {
                        allValueEqual = value == values[index - 1]
                        if !allValueEqual
                        {
                            break
                        }
                    }
                }
                return allValueEqual
            }
        }
       return false
    }
    
    //MARK: 大小
    static func getBigOrSmall(lotType:String,item:AllLotteryResultsList) -> String
    {
        let values = item.results()
        let total = sumOfItems(lotType:lotType,item: item)
        let maxTotal = 9 * values.count//时时彩，快三，低频彩，PC蛋蛋最大号码为9
        
        if lotType == "4" {
            var diceResult = ""
            
            if isLeopard(values: values)
            {
                return "豹" //豹子
            }else
            {
                diceResult = total <= 10 ? "小" : "大"
                return diceResult
            }
        } else if lotType == "7"
        {
            var diceResult = ""
            var pcdd_daXiaoDanShuang_1314 = false
            if let sys = getSystemConfigFromJson()
            {
                if sys.content != nil
                {
                    pcdd_daXiaoDanShuang_1314 = sys.content.pcdd_daXiaoDanShuang_1314 == "on"
                }
            }
            
            if pcdd_daXiaoDanShuang_1314 && (total == 13 || total == 14)
            {
                diceResult = "和"
            }else
            {
                diceResult = total >= 14 ? "大" : "小"
            }
            
            return diceResult
        }
        else if lotType == "5"
        {
            return fiveInElevenBigOrSmall(lotType: lotType, item: item)
        }else if lotType == "3"
        {
            return configSumSaiCheDoubleSmall(values: item.results())
        }else {
            return (total > maxTotal/2 ? "大" : "小")
        }
        
    }
    
    // 11选5的 大小
    private static func fiveInElevenBigOrSmall(lotType:String,item:AllLotteryResultsList) -> String
    {
        let total = sumOfItems(lotType:lotType,item: item)
        
        if lotType == "5"
        {
//            var switch_on = false
//            if let config = getSystemConfigFromJson()
//            {
//                if config.content != nil
//                {
//                    switch_on = config.content.syxw_zongHe_dxds_29_30 == "on"
//                }
//            }
            
            if total >= 31
            {
                return "大"
            }
//            else if total == 30
//            {
//                return "-"
//            }
            else
            {
               return "小"
            }
        }else
        {
            return ""
        }
    }
    
    //11选5 单双
    private static func fiveInElevenParity(lotType:String,item:AllLotteryResultsList) -> String
    {
//        var switch_on = false
//        if let config = getSystemConfigFromJson()
//        {
//            if config.content != nil
//            {
//                switch_on = config.content.syxw_zongHe_dxds_29_30 == "on"
//            }
//        }

        let total = sumOfItems(lotType:lotType,item: item)
//        if total == 29 {return "-"}
        
        return total % 2 == 0 ? "双" : "单"
    }
    
    // 11选5 奇偶
    private static func fiveInElevenDoubleOrSingle(lotType:String,item:AllLotteryResultsList) -> String
    {
        
        let valuesP =  item.results()
        var values = [String]()
        for (_,value) in valuesP.enumerated()
        {
            if value != "11"
            {
                values.append(value)
            }
        }
        
        if values.count == 5
        {
            values.remove(at: 4)
        }
        
        
        var isEven = 0
        for index in 0..<values.count
        {
            if let value = Int(values[index])
            {
                if value % 2 == 0
                {
                    isEven += 1
                }
                
                if index == (values.count - 1)
                {
                    if isEven >= 3
                    {
                        return "奇偶"
                    }else
                    {
                        let isOdd = (index + 1) - isEven
                        if isOdd >= 3
                        {
                            return "奇偶"
                        }
                    }
                    
                    if isEven == 2
                    {
                        return "和"
                    }
                }
            }else
            {
                return ""
            }
        }
        
        return ""
    }

    //MARK:  号码的和
    static func sumOfItems(lotType:String,item:AllLotteryResultsList) -> Int
    {
        let values =  item.results()
        
        
        var total: Int = 0
        var index = 0
        for value in values {
            total += Int(value)!
            
            if lotType == "3" && index == 1
            {
                return total
            }
            
            index += 1
        }
        
        return total
    }
    
    
    /** 设置type == 3的大小单双的值 */
    static func configSumSaiCheDoubleSmall(values: [String]) -> String {
        
        if let firstValue = Int(values[0])
        {
            if let secondValue = Int(values[1])
            {
                let sum = firstValue + secondValue
                
                var bjsc_guanyahe_11 = false
                if let sys = getSystemConfigFromJson()
                {
                    if sys.content != nil
                    {
                        bjsc_guanyahe_11 = sys.content.bjsc_guanyahe_11 == "on"
                    }
                }
                
                if bjsc_guanyahe_11
                {
                    return sum > 11 ? "大" : "小"
                }else
                {
                    if sum == 11
                    {
                        return "和"
                    }else
                    {
                        return sum > 11 ? "大" : "小"
                    }
                }
            }
        }
        
        return ""
    }
    
    
    
    //MARK: 单双
    static func getParity(lotType: String,item:AllLotteryResultsList) -> String
    {
        if lotType == "5"
        {
            return fiveInElevenParity(lotType: lotType, item: item)
        }else if lotType == "4" {
            let values = item.results()
            if isLeopard(values: values)
            {
                return "豹" //豹子
            }else{
                let total = sumOfItems(lotType:lotType,item: item)
                return total % 2 == 0 ? "双" : "单"
            }
        }else{
            let total = sumOfItems(lotType:lotType,item: item)
            return total % 2 == 0 ? "双" : "单"
        }
        
    }
    
    //MARK:  每个item对应的宽度
    static func getWidthWithIndex(index:Int,lotType: String) -> CGFloat
    {
        //1.均分
        if lotType == "6" || lotType == "66" || lotType == "9" || lotType == "5"
        {
            if index == 0
            {
                return 60
            }else if index == 1
            {
                return screenWidth - 60
            }else
            {
                return 0
            }
        }else if lotType == "4" || lotType == "7" || lotType == "8"
        {
            if index == 0
            {
                return screenWidth * 0.2
            }else if index == 1
            {
                return screenWidth - screenWidth * 0.2 * 4 + 60
            }else
            {
                return (screenWidth * 0.2 - 20)
            }
        }else if lotType == "1" || lotType == "2"
        {
            if index == 0
            {
                return 60
            }else if index == 1
            {
                return screenWidth - 60 - 40 * 3
            }else
            {
                return 40
            }
        }else if lotType == "3"
        {
            let firstWidth:CGFloat = 45
            if index == 0
            {
                return firstWidth
            }else if index == 1
            {
                return screenWidth - firstWidth - 35 * 3
            }else
            {
                return 35
            }
        }else
        {
            return 0
        }
        
    }
}
