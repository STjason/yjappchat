//
//  LotteryResultsDisBaseController.swift
//  gameplay
//
//  Created by admin on 2018/10/30.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LotteryResultsDisBaseController: LennyBasicViewController {

    var isAttachInTabBar = true
    
    var code: String! = "CQSSC" {
        didSet {
            dataArr.removeAll()
            loadLotteryResults(pageNumber: 1, code: code)
        }
    }
    
    var lotType: String = "1"
    var dataArr:[AllLotteryResultsList] = [AllLotteryResultsList]()//保存数据
    
    var shouldLoadMoreData = true
    private var lotteryTypes: [String]!
    private var selectedIndex: Int = 0
    var pageNum:Int = 1  //页数
    
    override var hasNavigationBar: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loadLotteryResults(pageNumber: Int, code:String) { }
}
