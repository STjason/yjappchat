//
//  LotResAdvanceHeader.swift
//  gameplay
//
//  Created by admin on 2018/10/31.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit

class LotResAdvanceHeader: UIView {

    
    @IBOutlet weak var themeBgView: UIView!
    @IBOutlet weak var firstConstraintW: NSLayoutConstraint!
    
    @IBOutlet weak var secConstraintW: NSLayoutConstraint!
    
    @IBOutlet weak var fifthConstraintW: NSLayoutConstraint!
    @IBOutlet weak var fourthConstraintW: NSLayoutConstraint!
    @IBOutlet weak var trirdConstraintW: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
    }
    
    func configWidthWithIndex(index:Int,width:CGFloat)
    {
        switch index {
        case 0:
            firstConstraintW.constant = width
        case 1:
            secConstraintW.constant = width
        case 2:
            trirdConstraintW.constant = width
        case 3:
            fourthConstraintW.constant = width
        case 4:
            fifthConstraintW.constant = width
        default:
            print("index 不存在")
        }
    }
}
