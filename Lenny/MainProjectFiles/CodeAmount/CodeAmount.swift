//
//  CodeAmount.swift
//  gameplay
//
//  Created by ken on 2019/4/25.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import MJRefresh
class CodeAmount: LennyBasicViewController {
    var filterStartTime = ""
    var filterTyep = 0 //默认-1 全部
    var filterEndTime = ""
    var model:CodeAmountModel!
    var arrayType = ["全部","彩票", "真人","电子游艺","体育","六合彩","沙巴体育投注","人工增加","人工扣减","注册赠送","红包","反水","存款","系统接口存款","棋牌","活动中奖","等级晋级","余额生金收益"]
    var arrayIndex = [0,1,2,3,4,5,6,10,11,12,13,21,22,23,24,25,26,27]
    var dataRows = [CodeAmountRowsModel]()
    let refreshHeader = MJRefreshNormalHeader()
    let refreshFooter = MJRefreshBackNormalFooter()

    //MARK: - 刷新
    private func endRefresh() {
        self.mainTableView.mj_footer?.endRefreshing()
        self.mainTableView.mj_header?.endRefreshing()
    }
    
    private func noMoreDataStatusRefresh(noMoreData: Bool = true) {
        if noMoreData {
            self.mainTableView.mj_footer?.endRefreshingWithNoMoreData()
        }else {
            self.mainTableView.mj_footer?.resetNoMoreData()
        }
    }
    
    private func setupRefreshView() {
        refreshHeader.setRefreshingTarget(self, refreshingAction: #selector(headerRefresh))
        refreshFooter.setRefreshingTarget(self, refreshingAction: #selector(footerRefresh))
        self.mainTableView.mj_header = refreshHeader
        self.mainTableView.mj_footer = refreshFooter
    }
    
    @objc fileprivate func headerRefresh() {
        loadNetRequest(isLoadMoreData:false)
    }
    
    @objc fileprivate func footerRefresh() {
        loadNetRequest(isLoadMoreData:false)
    }
    
    override func viewDidLoad() {
        self.shouldFrosted = false
        super.viewDidLoad()
        
        setupthemeBgView(view: self.view, alpha: 0)
        
        initDate()
        setViews()
        
        setupRefreshView()
        loadNetRequest(isLoadMoreData:false)
    }
    
    private let mainTableView = UITableView()
    private func setViews() {
        
        
        self.title = "打码量变动记录"
        
        let button = UIButton(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        button.setTitle("筛选", for: .normal)
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(rightBarButtonItemAction(button:)), for: .touchUpInside)
        button.isSelected = false
        button.theme_setTitleColor("Global.barTextColor", forState: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        setViewBackgroundColorTransparent(view: mainTableView)
        contentView.addSubview(mainTableView)
        mainTableView.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
        mainTableView.tableHeaderView = tableHeaderView()
        mainTableView.tableFooterView = UIView()
        mainTableView.rowHeight = UITableView.automaticDimension
        mainTableView.estimatedRowHeight = 56
    }
    
    @objc private func rightBarButtonItemAction(button: UIButton) {
        
        if button.isSelected == false {
            if contentView.viewWithTag(101) != nil { return }
            let filterView = CodeAmountView(height: 150,controller:self,array:arrayType,arrayI:arrayIndex)
            
            
            filterView.initializeDate(start: self.filterStartTime, end: self.filterEndTime,username:"") //时间init 加上全部？默认
            
            filterView.didClickCancleButton = {
                self.rightBarButtonItemAction(button: button)
            }
            filterView.didClickConfirmButton = {(username:String,startTime:String,endTime:String,include:Bool,type:Int)->Void in
                self.rightBarButtonItemAction(button: button)
                self.filterStartTime = startTime
                self.filterEndTime = endTime
                self.filterTyep = type
                self.loadNetRequest(isLoadMoreData: false)
            }
            filterView.tag = 101
            contentView.addSubview(filterView)
            let offsetY: CGFloat = 10
            filterView.frame = CGRect.init(x: 0, y: -180 - 10, width: MainScreen.width, height: 180)
            contentView.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: {
                filterView.frame = CGRect.init(x: 0, y: offsetY, width: MainScreen.width, height: 180)
                self.mainTableView.frame = CGRect.init(x: 0, y: 180 + offsetY, width: MainScreen.width, height: self.contentView.height)
            }) { ( _) in
                button.isSelected = true
            }
        }else {
            button.isSelected = false
            let filterView = contentView.viewWithTag(101)
            UIView.animate(withDuration: 0.5, animations: {
                filterView?.alpha = 0
                filterView?.frame = CGRect.init(x: 0, y: -180, width: MainScreen.width, height: 180)
                self.mainTableView.frame = CGRect.init(x: 0, y: 0, width: MainScreen.width, height: self.contentView.height)
            }) { ( _) in
                filterView?.removeFromSuperview()
                button.isSelected = false
            }
        }
    }
    
    private func tableHeaderView() -> UIView {
 
        let totalHeader = UIView(frame: CGRect.init(x: 0, y: 0, width: MainScreen.width, height: 33))
        
        let arrayText:[String] = ["变动类型", "变动前", "变动","变动后","详情"]
        
        for index in 0...4 {
            let label = UILabel()
            totalHeader.addSubview(label)
            label.font = UIFont.systemFont(ofSize: 12)
            label.textAlignment = NSTextAlignment.center
            label.textColor = UIColor.red
            label.text = arrayText[index]
        
            label.snp.makeConstraints { (make) in
                make.left.equalTo(0+(CGFloat.init(index)*MainScreen.width/5))
                make.centerY.equalTo(totalHeader)
                make.height.equalTo(20)
                make.width.equalTo(MainScreen.width/5)
            }
        }
        
        return totalHeader
    }
    
    func initDate(){
        self.filterStartTime = getTomorrowNowTime(beforeDays: 30)
        self.filterEndTime = getTomorrowNowTime()
    }
    
    func loadNetRequest(isLoadMoreData: Bool,pageSize: Int = defaultCountOfRequestPagesize) {
        
        var pageNumber = 1
        if isLoadMoreData {
            if self.dataRows.count % pageSize == 0 {
                pageNumber = self.dataRows.count / pageSize + 1
            }else {
                noMoreDataStatusRefresh()
                return
            }
        }
        
        LennyNetworkRequest.damaChangelist(
            startTime: self.filterStartTime, endTime: self.filterEndTime,
            type:self.filterTyep, pageNumber: pageNumber, pageSize: pageSize){
                (model) in
                
                self.endRefresh()

                if model!.code == 0 && !(model?.success)!{
                    loginWhenSessionInvalid(controller: self)
                    return
                }
                
                self.model = model
                
                if !isLoadMoreData {
                    self.dataRows.removeAll()
                }
                
                guard let rowsP = model?.content?.rows else {return}
                
                self.dataRows += rowsP
//                self.noMoreDataStatusRefresh(noMoreData: rowsP.count % pageSize != 0 || rowsP.count == 0)

                self.view.hideSkeleton()
                self.refreshTableViewData()
        }
    }
    
    override func refreshTableViewData() {
        mainTableView.reloadData()
    }
    
 
}

extension CodeAmount: UITableViewDelegate {
    
}

extension CodeAmount: UITableViewDataSource {
    
    @objc func onItemClick(datasoure:[String]){
        if datasoure.isEmpty{
            return
        }
        let dialog = DetailListDialog(dataSource: datasoure, viewTitle: "打码量详细")
        dialog.selectedIndex = 0
        self.view.window?.addSubview(dialog)
        dialog.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(dialog.kHeight)
        dialog.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            dialog.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    private func formSource(row:Int) -> [String]{
       
        let data = self.dataRows[row]

        var sources = [String]()
        //投注返点
        let tzfd = String.init(format: "提款所需:%2.f",data.drawNeed)
        //代理返点
        let dlfd = String.init(format: "变动前提款所需:%2.f",data.beforeDrawNeed)
        //返奖总额
        let cpzj = String.init(format: "变动后提款所需:%2.f",data.afterDrawNeed)
        //活动金额
        let hdje = String.init(format: "时间:%@",data.createDatetime!)
        //tikuan
        let withdraw = String.init(format: "备注:%@",data.remark!)
        
        sources.append(tzfd)
        sources.append(dlfd)
        sources.append(cpzj)
        sources.append(hdje)
        sources.append(withdraw)
        return sources

    }
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if getSwitch_lottery() {
            onItemClick(datasoure: formSource(row: indexPath.row))
        }
    }
    
    func typeStr(type:Int)->String{
        switch type {
        case 1:
            return arrayType[1]
        case 2:
            return arrayType[2]
        case 3:
            return arrayType[3]
        case 4:
            return arrayType[4]
        case 5:
            return arrayType[5]
        case 6:
            return arrayType[6]
        case 10:
            return arrayType[7]
        case 11:
            return arrayType[8]
        case 12:
            return arrayType[9]
        case 13:
            return arrayType[10]
        case 21:
            return arrayType[11]
        case 22:
            return arrayType[12]
        case 23:
            return arrayType[13]
        case 24:
            return arrayType[14]
        case 25:
            return arrayType[15]
        case 26:
            return arrayType[16]
        case 27:
            return arrayType[17]
        default:
            return "其他"
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataRows.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        let type = typeStr(type:self.dataRows[indexPath.row].type)
        let beforeNum = String.init(format:"%2.f",self.dataRows[indexPath.row].beforeNum)
        let betNum = String.init(format:"%2.f",self.dataRows[indexPath.row].betNum)
        let afterNum = String.init(format:"%2.f",self.dataRows[indexPath.row].afterNum)
        let arrayText:[String] = [type,beforeNum,betNum,afterNum,"详情"]
        
        for index in 0...3 {
            let label = UILabel()
            cell.addSubview(label)
            label.font = UIFont.systemFont(ofSize: 12)
            label.textAlignment = NSTextAlignment.center
            label.textColor = UIColor.black
            label.text = arrayText[index]
            
            label.snp.makeConstraints { (make) in
                make.left.equalTo(0+(CGFloat.init(index)*MainScreen.width/5))
                make.centerY.equalTo(cell)
                make.height.equalTo(20)
                make.width.equalTo(MainScreen.width/5)
            }
        }
        
        let viewRight = UIView.init()
        cell.addSubview(viewRight)
        viewRight.snp.makeConstraints { (make) in
            make.right.equalTo(0)
            make.width.equalTo(MainScreen.width/5)
            make.top.equalTo(0)
            make.height.equalTo(45) //每行未定义
        }
        
//      右箭头
        let imageRight = UIImageView.init(image: UIImage.init(named: "右箭头"))
        viewRight.addSubview(imageRight)
        imageRight.snp.makeConstraints { (make) in
            make.center.equalTo(viewRight)
            make.height.width.equalTo(15)
        }
        
        return cell
    }

}
