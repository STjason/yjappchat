//
//  CodeAmountView.swift
//  gameplay
//
//  Created by ken on 2019/4/25.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit

class CodeAmountView: UIView {

    var arrayType = [String]()
    var arrayIndex = [Int]()
    
    private let button_dateEnd = UIButton() //结束时间
    private let button_dateStart = UIButton() //开始时间
    
    private let button_Cancel = UIButton()
    private let button_Confirm = UIButton()

    private let button_Selecter = UIButton() //类型选择
    
    var controller:UIViewController!
    
    lazy var  start_Timer:CustomDatePicker = {
        let datePick = CustomDatePicker()
        datePick.tag = 101
        return datePick
    }()//开始时间选择器02
    
    lazy var end_Timer:CustomDatePicker = {
        let datePick = CustomDatePicker()
        datePick.tag = 102
        return datePick
    }()//结束时间选择器02
    
    
    private var startTime:String = ""
    private var endTime:String = ""
   
    private var accountTypes = 0
   
    
    func lableFactory(lable:UILabel,text:String){
        lable.font = UIFont.systemFont(ofSize: 12)
        lable.textColor = UIColor.cc_51()
        lable.text = text
    }
    
    func buttonFactory(button:UIButton){
        button.setTitleColor(UIColor.cc_136(), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.cc_224().cgColor
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
    }
    
    convenience init(height: CGFloat,controller:UIViewController,array:[String],arrayI:[Int]) {
        self.init()
        
        arrayType = array
        arrayIndex = arrayI
        
        self.theme_backgroundColor = "FrostedGlass.filterViewColor"
        self.controller = controller
 
        let labal_Type = UILabel()
        addSubview(labal_Type)
        lableFactory(lable: labal_Type, text: "类型选择:")
        
        let labal_time = UILabel()
        addSubview(labal_time)
        lableFactory(lable: labal_time, text: "日期:")
        
        let labal_line = UILabel()
        addSubview(labal_line)
        lableFactory(lable: labal_line, text: "--")
        
        addSubview(button_Selecter)
        button_Selecter.setTitle("全部", for: .normal)
        button_Selecter.setTitleColor(UIColor.black, for: .normal)
        button_Selecter.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button_Selecter.setImage(UIImage(named: "pulldown"), for: .normal)
        button_Selecter.imageEdgeInsets = UIEdgeInsets(top: 0, left: 200 - 30, bottom: 0, right: 0)
        button_Selecter.titleEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 120)
        button_Selecter.setTitleShadowColor(UIColor.ccolor(with: 136, g: 136, b: 136), for: .highlighted)
        button_Selecter.addTarget(self, action: #selector(button_SelectorClickHandle), for: .touchUpInside)
        button_Selecter.whc_AddBottomLine(0.5, color: UIColor.ccolor(with: 224, g: 224, b: 224))
        button_Selecter.layer.borderColor = UIColor.cc_224().cgColor
        button_Selecter.layer.borderWidth = 1.0
        button_Selecter.layer.cornerRadius = 3
        button_Selecter.clipsToBounds = true

        
     
        addSubview(button_dateStart)
        button_dateStart.setTitle(self.startTime, for: .normal)
        buttonFactory(button: button_dateStart)
//        button_dateStart.isHidden = false
        button_dateStart.addTarget(self, action: #selector(button_StartTimeClickHandle), for: .touchUpInside)
        
    
        
        addSubview(button_dateEnd)
        button_dateEnd.setTitle(self.endTime, for: .normal)
        buttonFactory(button: button_dateEnd)
        button_dateEnd.addTarget(self, action: #selector(button_EndTimeClickHandle), for: .touchUpInside)
    
        
        addSubview(button_Confirm)
        addSubview(button_Cancel)
        
        button_Confirm.setTitle("确认", for: .normal)
        button_Confirm.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button_Confirm.backgroundColor = UIColor.mainColor()
        button_Confirm.setTitleColor(UIColor.white, for: .normal)
        button_Confirm.layer.cornerRadius = 3
        button_Confirm.clipsToBounds = true
        button_Confirm.addTarget(self, action: #selector(button_ConfirmClickHandle), for: .touchUpInside)
        
        button_Cancel.setTitle("取消", for: .normal)
        button_Cancel.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button_Cancel.backgroundColor = UIColor.white
        button_Cancel.setTitleColor(UIColor.cc_136(), for: .normal)
        button_Cancel.layer.cornerRadius = 3
        button_Cancel.clipsToBounds = true
        button_Cancel.layer.borderWidth = 0.5
        button_Cancel.layer.borderColor = UIColor.lightGray.cgColor
        button_Cancel.addTarget(self, action: #selector(button_CancelClickHandle), for: .touchUpInside)

        
        button_Selecter.snp.makeConstraints { (make) in
            make.right.equalTo(-20)
            make.top.equalTo(20)
            make.width.equalTo(220)
            make.height.equalTo(30)
        }
        
        labal_Type.snp.makeConstraints { (make) in
            make.right.equalTo(button_Selecter.snp.left).offset(-10)
            make.centerY.equalTo(button_Selecter)
        }
        
        
        button_dateEnd.snp.makeConstraints { (make) in
            make.right.equalTo(button_Selecter)
            make.top.equalTo(button_Selecter.snp.bottom).offset(5)
            make.width.equalTo((220-10)/2)
            make.height.equalTo(30)
        }
        
        button_dateStart.snp.makeConstraints { (make) in
            make.right.equalTo(button_dateEnd.snp.left).offset(-10)
            make.top.equalTo(button_Selecter.snp.bottom).offset(5)
            make.width.equalTo((220-10)/2)
            make.height.equalTo(30)
        }
        
        labal_line.snp.makeConstraints { (make) in
            make.left.equalTo(button_dateStart.snp.right)
            make.right.equalTo(button_dateEnd.snp.left)
            make.centerY.equalTo(button_dateStart)
        }
    
        labal_time.snp.makeConstraints { (make) in
            make.right.equalTo(button_dateStart.snp.left).offset(-10)
            make.centerY.equalTo(button_dateStart)
        }
        

        button_Cancel.snp.makeConstraints { (make) in
            make.right.equalTo(button_dateEnd.snp.right)
            make.top.equalTo(button_dateStart.snp.bottom).offset(5)
            make.width.equalTo(50)
            make.height.equalTo(30)
        }
        
        button_Confirm.snp.makeConstraints { (make) in
            make.right.equalTo(button_Cancel.snp.left).offset(-5)
            make.top.equalTo(button_dateStart.snp.bottom).offset(5)
            make.width.equalTo(50)
            make.height.equalTo(30)
        }
        
        
    }
    
  
    
    @objc private func button_SelectorClickHandle() {

        let selectedView = CodeAmoutSelectView(dataSource:self.arrayType,dataIndex:self.arrayIndex,viewTitle: "类型选择")
        selectedView.selectedIndex = selectIndex
        selectedView.didSelected = { [weak self] (index, content) in //selectedView
            
            self?.button_Selecter.setTitle(content, for: .normal)
            self?.selectIndex = index
            self?.accountTypes = index
        }
        window?.addSubview(selectedView)
        selectedView.whc_Center(0, y: 0).whc_Width(MainScreen.width*0.75).whc_Height(selectedView.kHeight)
        selectedView.transform =  CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {
            selectedView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func initializeDate(start:String,end:String,username:String){
        self.startTime = start
        self.endTime = end
        button_dateStart.setTitle(self.startTime, for: .normal)
        button_dateEnd.setTitle(self.endTime, for: .normal)
    }
    
    //MARK:打开时间选择
    
    func openDatePick(tag:Int)  {
        
        if tag == 101{
            self.start_Timer.canButtonReturnB = {
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
            }
            self.start_Timer.sucessReturnB = { returnValue in
                self.startTime = returnValue
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
                let task = delay(0.5){
                    self.button_dateStart.titleLabel?.lineBreakMode = .byTruncatingHead
                    self.button_dateStart.setTitle(returnValue, for: .normal)
                }
                //                cancel(task)
            }
            self.gototargetView(_targetView:self.start_Timer)
        }else if tag == 102{
            self.end_Timer.canButtonReturnB = {
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
            }
            self.end_Timer.sucessReturnB = { returnValue in
                self.endTime = returnValue
                self.controller.view.ttDismissPopupViewControllerWithanimationType(TTFramePopupViewSlideBottomTop)
                let task = delay(0.5){
                    self.button_dateEnd.titleLabel?.lineBreakMode = .byTruncatingHead
                    self.button_dateEnd.setTitle(returnValue, for: .normal)
                }
                //                cancel(task)
            }
            self.gototargetView(_targetView:self.end_Timer)
        }
    }
    
    @objc private func button_StartTimeClickHandle() {
        openDatePick(tag: 101)
    }
    
    @objc private func button_EndTimeClickHandle() {
        openDatePick(tag: 102)
    }
    
    //MARK:打开底部弹出view
    
    func gototargetView(_targetView:UIView)  {
        controller.view.ttPresentFramePopupView(_targetView, animationType: TTFramePopupViewSlideBottomTop) {
            
        }
        _targetView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(controller.view)
            make.height.equalTo(250)
        }
    }
    
    //过滤view中点击确定时的响应方法
    @objc private func button_ConfirmClickHandle() {
        didClickConfirmButton?("",self.startTime,self.endTime,false,self.accountTypes)
    }
    @objc private func button_CancelClickHandle() {
        didClickCancleButton?()
    }
    
    var didClickCancleButton: ( () -> Void)?
    var didClickConfirmButton: ( (String,String,String,Bool,Int) -> Void)?
    var changeTypes: [AccountTypeBean]!//所有账变类型信息
    
    var selectIndex: Int = 0
    var selectLotCode: String = ""
    
    var resultModel:AccountTypesModel!
  

}
