//
//  AccoutChangeDetailCell.swift
//  gameplay
//
//  Created by Gallen on 2019/11/5.
//  Copyright © 2019年 yibo. All rights reserved.
//

import UIKit

class AccoutChangeDetailCell: UITableViewCell {
    //账变详情标题
    lazy var accoutChangeTitleLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    //账变详情内容
    lazy var accountContentLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
        
    }()
    
    lazy var underLineView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.colorWithHexString("#eaeaea")
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = false
        self.backgroundColor = UIColor.colorWithRGB(r: 230, g: 230, b: 230, alpha: 1)
        contentView.addSubview(accoutChangeTitleLabel)
        
        accoutChangeTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(20)
            make.height.greaterThanOrEqualTo(40)
            make.right.equalTo(self).offset(-20)
            make.top.equalTo(5)
            make.bottom.equalTo(-5)
        }
//        contentView.addSubview(accountContentLabel)
//        contentView.addSubview(underLineView)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
