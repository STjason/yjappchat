//
//  ModifyPwdResultModel.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/6.
//  Copyright © 2018年 yibo. All rights reserved.
//
/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-06 10:50:00
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import UIKit
import HandyJSON
//修改密码model结构体

class ModifyPwdResultModel :HandyJSON{
    required init() {}
   
    var success: Bool = false
    var content: Bool = false
    var accessToken: String?
    var msg: String = ""
}

