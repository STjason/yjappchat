

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-20 14:51:16
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON

class AccountChangeRowModel :HandyJSON {
    required init() {}
    
    var remark: String?
    var typeCn: String?
    var createDatetime: String?
    var afterMoney: Float = 0.0
    var username: String?
    var type: Int = 0
    var add: Bool = false
    var money: Float = 0.0
}

class AggsData :HandyJSON {
    required init() {}
    
    var totalSMoney: Float = 0.0
    var totalZMoney: Float = 0.0
}

class AccountChangePageModel :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [AccountChangeRowModel]?
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
    var aggsData: AggsData?
}

class AccountChangeContentModel :HandyJSON{
    required init() {}
    
    var subTotal: Int = 0
    var subTotalZMoney: Int = 0
    var subTotalSMoney: Float = 0.0
    var totalSMoney: Float = 0.0
    var total: Int = 0
    var page: AccountChangePageModel?
    var totalZMoney: Float = 0.0
}

class AccountChangeModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: AccountChangeContentModel?
    var code:Int?
}
