//
//  GeRenBaoBiaoModel.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/19.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-19 16:27:25
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */


class ReportModeRows :HandyJSON{
    required init() {}
    
    var lotteryWinAmount: CGFloat = 0
    var profitAndLoss: CGFloat = 0
    var lotteryRebateAmount: CGFloat = 0
    var statDate: String?
    var lotteryBetAmount: CGFloat = 0
    var depositAmount: CGFloat = 0
    var proxyRebateAmount: CGFloat = 0.0
    var activeAwardAmount: CGFloat = 0
    var withdrawAmount: CGFloat = 0
}

class PageDataModel :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [ReportModeRows]?
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
}

class ContentModel :HandyJSON {
    required init() {}
    
    var proxyRebateAmountCount: CGFloat = 0.0
    var profitAndLossTotal: CGFloat = 0.0
    var lotteryBetAmountCount: CGFloat = 0.0
    var lotteryWinAmountCount: CGFloat = 0.0
    var depositAmountCount: Int = 0
    var lotteryRebateAmountCount: CGFloat = 0.0
    var withdrawAmountCount: Int = 0
    var page: PageDataModel?
    var activeAwardAmountCount: Int = 0
}

class GeRenBaoBiaoModel :HandyJSON{
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: ContentModel?
    var code: Int? 
}


