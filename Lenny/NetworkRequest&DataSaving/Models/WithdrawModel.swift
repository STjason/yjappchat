//
//  WithdrawModel.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/20.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON


/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-20 00:29:19
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */



class WithdrawRows :HandyJSON {
    required init() {}
    
    var cardNo: String?
    var bankName: String?
    var orderId: String?
    var createDatetime: String?
    var remark: String?
    var drawMoney: Float = 0
    var status: Int = 0
    var lockFlag = ""
}

class WithdrawContent :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [WithdrawRows]?
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
}

class WithdrawModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: WithdrawContent?
    var code:Int?  
}

