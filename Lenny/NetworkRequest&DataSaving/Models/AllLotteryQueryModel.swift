//
//  AllLotteryQueryModel.swift
//  gameplay
//
//  Created by Lenny's Macbook Air on 2018/6/3.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class AllLotteryQueryModelRows :HandyJSON {
    
    required init() {}
    
    var orderId: String?
    var buyZhuShu: String?
    var lotVersion: String?
    var status: String?
    var username: String?
    var lotType: String?
    var rollBackStatus: String?
    var multiple: String?
    var buyMoney: String?
    var winMoney: String?
    var winOdds: String?
    var zhuiHao: String?
    var zhuiOrderId: String?
    var buyOdds: String?
    var playCode: String?
    var lotCode: String?
    var haoMa: String?
    var id: String?
    var openHaoMa: String?
    var playName: String?
    var lotName: String?
    var closedTime: String?
    var model: String?
    var proxyRollback: String?
    var kickback: String?
    var sellingTime: String?
    var stationId: String?
    var createTime: String?
    var terminalBetType: String?
    var cheat: String?
    var qiHao: String?
    var oddsCode: String?
    var accountId: String?
    var createTimeLong:Double = 0.0
}

class AllLotteryQueryAggs :HandyJSON {
    required init() {}
    
    var bettingMoneyCount = 0.0
    var kickbackCount = 0.0
    var winMoneyCount = 0.0
}

class AllLotteryQueryModelContent :HandyJSON {
    required init() {}
    
    var aggsData:AllLotteryQueryAggs?
    var hasPre: String?
    var pageSize: String?
    var start: String?
    var hasNext: String?
    var totalPageCount: String?
    var rows: [AllLotteryQueryModelRows]?
    var nextPage: String?
    var currentPageNo: String?
    var total: String?
    var prePage: String?
}

class AllLotteryQueryModel :HandyJSON {
    required init() {}
    
    var accessToken: String?
    var content: AllLotteryQueryModelContent?
    var success: Bool = false
    var code: Int?
//    var msg: String?    
}

