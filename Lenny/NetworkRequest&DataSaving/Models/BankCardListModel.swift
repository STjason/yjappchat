

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-20 19:26:25
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON

class BankCardListContent :HandyJSON{
    required init() {}
    
    var cardNo: String = ""
    var bankName: String = ""
    var realName: String?
    var bankCode: String?
    var id: Int = 0
    var status: Int = 0
    var cardNoSc: String = ""
    var bankAddress: String?
    var createTime: String?
    var remarks: String = ""
    var type: String?
}

class BankCardListModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: [BankCardListContent]?
    var code:Int?  
}
