

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-24 16:40:26
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON



class ReadMessageModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var code:Int?
}

