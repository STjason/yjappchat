//
//  sss.swift
//  gameplay
//
//  Created by Lenny's Macbook Air on 2018/5/22.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class AllLotteryTypesSubData: HandyJSON {
    required init() {}
    
    var czCode: String?
    var status: String?
    var moduleCode: String?
    var groupCode: String?
    var code: String?
    var lotType: String?
    /** 1：官方，2：信用 */
    var lotVersion: String?
    var groupName: String?
    var duration: String?
    var ballonNums: String?
    var name: String?
}

class AllLotteryTypesContent :HandyJSON {
    required init() {}
    
    var ballonNums: String?
    var czCode: String?
    var lotVersion: String?
    var code: String?
    var subData: [AllLotteryTypesSubData]?
    var groupCode: String?
    var groupName: String?
    var moduleCode: String?
    var lotType: String?
    var duration: String?
    var status: String?
    var name: String?    
}

class AllLotteryTypesModel :HandyJSON {
    required init() {}
    
    var accessToken: String?
    var content: [AllLotteryTypesContent]?
    var success: String?
}

extension AllLotteryTypesModel {
    /// 获取去重后的小彩种带Code
    
    static var lotterysIndex : Int = 0
    
    func obtainAllLotteryWithIndex() -> [AllLotteryTypesSubData] {
        var lotterys = [AllLotteryTypesSubData]()
        for item: AllLotteryTypesContent in content! {
            for it: AllLotteryTypesSubData in item.subData! {
                if lotterys.count == 0 { lotterys.append(it) }
                var add: Bool = true
                for lot in lotterys {
                    if lot.code == it.code {
                        add = false
                        continue
                    }
                }
                if add {
                    lotterys.append(it)
                }
            }
        }
        
                for lot in lotterys {   
                    if lot.code == DefaultLotteryNumber() {
                        let th = lotterys[0]
                        lotterys[0] = lot
                        lotterys[AllLotteryTypesModel.lotterysIndex] = th
                    } //这里会闪退？
                    AllLotteryTypesModel.lotterysIndex += 1
                }
        
        AllLotteryTypesModel.lotterysIndex = 0
        return lotterys
    }
    
}
