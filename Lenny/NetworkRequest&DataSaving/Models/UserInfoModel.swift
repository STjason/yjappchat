

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-20 17:35:36
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON

class UserInfoContent :HandyJSON {
    required init() {}
    
    var phone: String?
    var accountId: Int = 0
    var qq: String?
    var realName: String?
    var score: Int = 0
    var wechat: String?
    var email: String?
    var username: String?
    var money: Int = 0
    var stationId: Int = 0
}

class UserInfoModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: UserInfoContent?
    var code:Int? 
}
