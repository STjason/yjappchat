

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-24 16:40:26
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON



/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-24 16:43:30
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */



class ReceiveMessageModelContentRow :HandyJSON {
    required init() {}
    
    var status: Int = 0//已读，未读标识 1-未读 2-已读
    var sendAccount: String?
    var createTime: String?
    var id: Int = 0
    var sendType: Int = 0
    var title: String?
    var levelGroup: Int = 0
    var message: String?
    var receiveMessageId: Int64 = 0
    var stationId: Int = 0
    var receiveType: Int = 0
    /**是否弹窗 1不弹 2有弹窗*/
    var popStatus:Int = 0
}

class ReceiveMessageModelContent :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [ReceiveMessageModelContentRow]?
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
}

class ReceiveMessageModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: ReceiveMessageModelContent?
    var code:Int?
}

