//
//  ChargeWidthDrawModel.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/20.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON


/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-20 00:04:57
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */



class Rows :HandyJSON {
    required init() {}
    
    var opDesc: String?
    var bankWay: Int = 0
    var orderId: String?
    var createDatetime: String?
    var status: Int = 0
    var depositType: Int = 0
    var payName: String?
    var bankAddress: String?
    var money: Float = 0
    var lockFlag = ""
}

class Content :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [Rows]?
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
}

class ChargeModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: Content?
    var code:Int?
}

