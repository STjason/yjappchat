//
//  AccountTypesModel.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/6/20.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON


/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-20 14:19:12
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */



class PayType :HandyJSON {
    required init() {}
    
    var name: String?
    var type: Int = 0
}

class IncomeType :HandyJSON {
    required init() {}
    
    var name: String?
    var type: Int = 0
}

class AccountTypesContentModel :HandyJSON {
    required init() {}
    
    var payType: [PayType]?
    var incomeType: [IncomeType]?
}

class AccountTypesModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: AccountTypesContentModel?
    var code:Int? 
}

