

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-24 16:40:26
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON



/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-06-24 16:43:30
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */



class SendMessageModelContentRow :HandyJSON {
    required init() {}
    
    var status: Int = 0
    var createTime: String?
    var id: Int = 0
    var message: String?
    var receiveAccount:String?
    //接收类型 1 个人 2群发 3层级 4上级 5 下级
    var receiveType:String?
    //发送人
    var sendAccount: String?
    var sendId:String?
    //发送方类型 1.租户后台 2，会员
    var sendType:String?;
    var stationId: Int = 0
    var title: String?
}

class SendMessageModelContent :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [SendMessageModelContentRow]?
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
}

class SendMessageModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: SendMessageModelContent?
    var code:Int?
    var msg:String? 
}

