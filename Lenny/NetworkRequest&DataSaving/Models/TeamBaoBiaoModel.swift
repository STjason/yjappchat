

/**
 * Copyright 2018 WHC_DataModelFactory
 * Auto-generated: 2018-07-17 15:16:55
 *
 * @author netyouli (whc)
 * @website http://wuhaichao.com
 * @github https://github.com/netyouli
 */

import HandyJSON

class TeamBaoBiaoModelTotal :HandyJSON {
    required init() {}
    
    var depositGiftAmount: Int = 0
    var registerGiftAmount: Int = 0
    var withdrawAmount: Int = 0
    var lotteryWinAmount: CGFloat = 0.0
    var activeAwardAmount: Int = 0
    var depositArtificial: Int = 0
    var withdrawArtificial: Int = 0
    var profitAndLossTotal: CGFloat = 0.0
    var proxyRebateAmount: Int = 0
    var lotteryBetAmount: Int = 0
    var depositAmount: Int = 0
    var lotteryRebateAmount: CGFloat = 0.0
}

class TeamBaoBiaoModelRows :HandyJSON {
    required init() {}
    
    var depositGiftAmount: Int = 0
    var profitAndLoss: CGFloat = 0.0
    var registerGiftAmount: Int = 0
    var withdrawAmount: Int = 0
    var lotteryWinAmount: CGFloat = 0.0
    var activeAwardAmount: Int = 0
    var accountType: Int = 0
    var depositArtificial: Int = 0
    var accountId: Int = 0
    var withdrawArtificial: Int = 0
    var username: String?
    var proxyRebateAmount: Int = 0
    var lotteryBetAmount: Int = 0
    var depositAmount: Int = 0
    var lotteryRebateAmount: CGFloat = 0.0
}

class CodeAmountRowsModel :HandyJSON {
    required init() {}
    
    var afterDrawNeed: CGFloat = 0.0
    var afterNum: CGFloat = 0.0
    var beforeDrawNeed: CGFloat = 0.0
    var beforeNum: CGFloat = 0.0
    var betNum: CGFloat = 0.0
    var createDatetime:String?
    var drawNeed: CGFloat = 0.0
    var remark:String?
    var type:Int = 0
    var username:String?
}

class TeamBaoBiaoModelPage :HandyJSON {
    required init() {}
    
    var hasPre: Bool = false
    var pageSize: Int = 0
    var start: Int = 0
    var hasNext: Bool = false
    var totalPageCount: Int = 0
    var rows: [TeamBaoBiaoModelRows]? // CodeAmountRowsModel TeamBaoBiaoModelRows
    var nextPage: Int = 0
    var currentPageNo: Int = 0
    var total: Int = 0
    var prePage: Int = 0
}

class TeamBaoBiaoModelContent :HandyJSON {
    required init() {}
    
    var total: TeamBaoBiaoModelTotal?
    var page: TeamBaoBiaoModelPage?
}

class CodeAmountModelContent :HandyJSON {
    required init() {}
    var rows : [CodeAmountRowsModel]?  // [CodeAmountRowsModel]?
}

class TeamBaoBiaoModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: TeamBaoBiaoModelContent?
    var code = 0
}


class CodeAmountModel :HandyJSON {
    required init() {}
    
    var success: Bool = false
    var accessToken: String?
    var content: CodeAmountModelContent?
    var code = 0
}
