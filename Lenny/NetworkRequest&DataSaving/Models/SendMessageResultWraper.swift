//
//  SendMessageResultWraper.swift
//  gameplay
//
//  Created by yibo-johnson on 2018/7/18.
//  Copyright © 2018年 yibo. All rights reserved.
//

import HandyJSON
//发送站内信结果
class SendMessageResultWraper :HandyJSON {
    required init() {}
    
    var msg: String?
    var success: Bool = false
    var code: Int = 0
}

