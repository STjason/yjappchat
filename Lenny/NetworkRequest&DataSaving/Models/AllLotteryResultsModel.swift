//
//  AllLotteryResultsModel.swift
//  gameplay
//
//  Created by Lenny's Macbook Air on 2018/5/23.
//  Copyright © 2018年 yibo. All rights reserved.
//

import UIKit
import HandyJSON

class AllLotteryResultsList :HandyJSON {
    required init() {}
    
    var result: String?
    var id: String?
    var period: String?
    var date: String?
    var year: String = ""
}

extension AllLotteryResultsList {
    
    func results() -> [String] {
        return (result?.components(separatedBy: ","))!
    }
}

class AllLotteryResultsHistory :HandyJSON {
    required init() {}
    
    var total: String?
    var list: [AllLotteryResultsList]?
}

class AllLotteryResultsCodeNums :HandyJSON{
    required init() {}
    
    var Code: String?
    var MC: String?
    var AC: String?
}

class AllLotteryResultsCodeRank :HandyJSON{
    required init() {}
    
    var Local: String?
    var CodeNums: [AllLotteryResultsCodeNums]?
}

private var kLastArray = "kLastArray"

extension AllLotteryResultsCodeRank {
    
    
    func obtainDatasBy(pageIndex: Int, rowIndex: Int, codeRankArr:[AllLotteryResultsCodeNums],dataArr:[AllLotteryResultsList]) -> [String] {
        var array_Return = [String]()
        
        if rowIndex == 0 {
            for codenum: AllLotteryResultsCodeNums in codeRankArr { //CodeNums
                array_Return.append(codenum.AC!)
            }
            LennyModel.lastArray = array_Return
            return array_Return
        }
        for i in 0 ..< LennyModel.lastArray!.count {
            var s = Int(LennyModel.lastArray![i])!
            s += 1
            array_Return.append(String(s))
            //            let item = LennyModel.allLotteryResultsModel?.content?.history?.list![19 - rowIndex + 1]
            let item = dataArr[rowIndex]
            let winningNumber = Int(item.results()[pageIndex])!
            if i == winningNumber - 1 {
                array_Return[i] = "1"
            }
        }
        LennyModel.lastArray = array_Return
        return array_Return
    }
    
    func obtainDatas11x5By(pageIndex: Int, rowIndex: Int) -> [String] {
        
        var array_Return = [String]()
        
        if rowIndex == 0 {
            for codenum: AllLotteryResultsCodeNums in CodeNums! {
                array_Return.append(codenum.AC!)
            }
            LennyModel.lastArray = array_Return
            return array_Return
        }
        for i in 0 ..< LennyModel.lastArray!.count {
            var s = Int(LennyModel.lastArray![i])!
            s += 1
            array_Return.append(String(s))
            let item = LennyModel.allLotteryResultsModel?.content?.history?.list![19 - rowIndex + 1]
            let winningNumber = Int(item!.results()[pageIndex])!
            if i == winningNumber - 1 {
                array_Return[i] = "1"
            }
        }
        LennyModel.lastArray = array_Return
        return array_Return
    }
    
    func obtainDatas11X5By(pageIndex: Int, rowIndex: Int, codeRankArr:[AllLotteryResultsCodeNums],dataArr:[AllLotteryResultsList]) -> [String] {
        var array_Return = [String]()
        
        if rowIndex == 0 {
            for codenum: AllLotteryResultsCodeNums in codeRankArr { //CodeNums
                array_Return.append(codenum.AC!)
            }
            LennyModel.lastArray = array_Return
            return array_Return
        }
        for i in 0 ..< LennyModel.lastArray!.count {
            var s = Int(LennyModel.lastArray![i])!
            s += 1
            array_Return.append(String(s))
            //            let item = LennyModel.allLotteryResultsModel?.content?.history?.list![19 - rowIndex + 1]
            let item = dataArr[rowIndex]
            let winningNumber = Int(item.results()[pageIndex])!
            if i == winningNumber - 1 {
                array_Return[i] = "1"
            }
        }
        LennyModel.lastArray = array_Return
        return array_Return
    }
}

class AllLotteryResultsContent :HandyJSON {
    required init() {}
    
    var history: AllLotteryResultsHistory?
    var codeRank: [AllLotteryResultsCodeRank]?
}

class AllLotteryResultsModel :HandyJSON {
    required init() {}
    
    var accessToken: String?
    var content: AllLotteryResultsContent?
    var success: String?
    var lotteryType: String?
}

