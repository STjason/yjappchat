//
//  PasswordModifyController.swift
//  SinglePages
//
//  Created by Lenny's Macbook Air on 2018/5/4.
//  Copyright © 2018年 Lenny. All rights reserved.
//

import UIKit

class PasswordModifyController: LennyBasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        checkAccountFromWeb()
        
        self.title = "密码修改"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "返回", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackClick))
    }
    
    
    
    fileprivate var mainPageView: WHC_PageView!
    
//    fileprivate let vcs = [ModifyLoginPasswordController(), SetWithdrawPasswordController()]
    fileprivate var vcs = [LennyBasicViewController]()
    
    private func setViews(hasWithdrawPwd:Bool) {
        
        vcs = [ModifyLoginPasswordController()]
        let vcP = SetWithdrawPasswordController()
        vcP.hasPwd = hasWithdrawPwd ? true : false
        vcs.append(vcP)
        
        mainPageView = WHC_PageView()
        contentView.addSubview(mainPageView)
        if /*glt_iphoneX*/UIScreen.main.bounds.height >= 812.0  {
            mainPageView.whc_AutoSize(left: 0, top: 24, right: 0, bottom: 0)
        }else {
            mainPageView.whc_AutoSize(left: 0, top: 0, right: 0, bottom: 0)
        }
        mainPageView.delegate = self
        let layoutParameter = WHC_PageViewLayoutParam()
        layoutParameter.titles = ["修改登录密码", hasWithdrawPwd ? "修改提款密码" : "设置提款密码"]
        layoutParameter.selectedTextColor = UIColor.ccolor(with: 51, g: 51, b: 51)
        layoutParameter.selectedFont = UIFont.systemFont(ofSize: 16)
        layoutParameter.normalTextColor = UIColor.ccolor(with: 136, g: 136, b: 136)
        layoutParameter.normalFont = UIFont.systemFont(ofSize: 16)
        layoutParameter.cursorColor = UIColor.ccolor(with: 236, g: 40, b: 40)
        layoutParameter.canChangeFont = true
        layoutParameter.canChangeTextColor = true
        layoutParameter.cursorHeight = 2
        mainPageView.layoutIfNeeded()
        mainPageView.layoutParam = layoutParameter
        
    }
    
    func checkAccountFromWeb() -> Void {
        request(frontDialog: true,method: .get,loadTextStr: "正在检查帐号...",url:CHECK_PICK_MONEY_URL,
                callback: {(resultJson:String,resultStatus:Bool)->Void in
                    if !resultStatus {
                        if !isEmptyString(str: resultJson){
                            showToast(view: self.view, txt: resultJson)
                        }else{
                            showToast(view: self.view, txt: convertString(string: "检查提款帐户安全失败"))
                        }
                        return
                    }
                    if let result = CheckPickAccountWraper.deserialize(from: resultJson){
                        if result.success{
                            YiboPreference.setToken(value: result.accessToken as AnyObject)
                            
                            if let content = result.content{
                                // 判断是否有提款密码
                                self.setupByBankRequest(content:content)
                            }
                            
                        }else{
                            if !isEmptyString(str: result.msg){
                                showToast(view: self.view, txt: result.msg)
                            }else{
                                showToast(view: self.view, txt: convertString(string: "检查提款帐户安全失败"))
                            }
                            if result.code == 0 || result.code == -1{
                                loginWhenSessionInvalid(controller: self)
                            }
                        }
                    }
        })
    }
    
    private func setupByBankRequest(content: PickBankAccount){
        
        //银行帐户等信息检查通过后获取提款配置信息
        if let pwd = content.receiptPwd
        {
            if !isEmptyString(str: pwd)
            {
                setViews(hasWithdrawPwd: true)
                return
            }
        }
        
        setViews(hasWithdrawPwd: false)
    }
}

extension PasswordModifyController: WHC_PageViewDelegate {
    
    func whcPageViewStartLoadingViews() -> [UIView]! {
        return [vcs.first!.view, vcs.last!.view]
    }
    func whcPageView(_ pageView: WHC_PageView, willUpdateView view: UIView, index: Int) {
        
    }
}
