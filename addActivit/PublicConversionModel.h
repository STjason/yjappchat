//
//  PublicConversionModel.h
//  gameplay
//
//  Created by JK on 2019/9/4.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// 用于处理swift 因为语言限制无法处理的事件
@interface PublicConversionModel : NSObject

/**
 屏幕适配

 @param x x为 需要适配的数值
 @return 回执适配后的数值
 */
+ (CGFloat) getCurrentScreenAdaptation:(id)x;


/**
 封装CGRect 规避 Int、CGFlot、Double问题
 
 @param x x坐标
 @param y y坐标
 @param w 宽度
 @param h 高度
 @return 回执CGRect
 */
+ (CGRect) getCurrentRectX:(id)x Y:(id)y W:(id)w H:(id)h;

+(BOOL)convertAMRtoWAV:(NSString *)filePath savePath:(NSString *)savePath;

@end
