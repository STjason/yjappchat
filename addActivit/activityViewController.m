//
//  activityViewController.m
//  gameplay
//
//  Created by JK on 2019/9/10.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "activityViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "PublicAlertView.h"
#import "gameplay-Swift.h"
#import "UIColor+Category.h"
#import <WebKit/WebKit.h>
#import "ActivityHeaderView.h"
#import "ActivityCell.h"
#import "ActivityFooterView.h"


// 导航栏高度
#define SCREEN_NAV  ([[UIApplication sharedApplication] statusBarFrame].size.height+44)
// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

#define kStatusHeight             [[UIApplication sharedApplication] statusBarFrame].size.height

@interface activityViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

/** 业务类 */
@property (nonatomic, strong) ActivityProtocol *activityProtocol;

/** 返回 按钮 */
@property (nonatomic, strong) UIButton *popBtn;

/** 头部试图 */
@property (nonatomic, strong) ActivityHeaderView *heaerView;

/** 底部试图 */
@property (nonatomic, strong) ActivityFooterView *footerView;

/** 奖品列表 */
@property (nonatomic, strong) UICollectionView *prizesCollectionView;

/** content数据 */
@property (nonatomic, strong) ActivityContentModel *contentModel;

/** 奖品数据源 */
@property (nonatomic, strong) NSArray *awardListData;

/** id */
@property (nonatomic, strong) NSString *aId;

/** 底部试图高度 */
@property (nonatomic, assign) CGFloat bottomHeight;

/** 当前是否处于摇奖状态 */
@property (nonatomic, assign) BOOL isLotteryStatus;

@end

@implementation activityViewController

- (void)dealloc
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"activityResource.bundle/MidAutumnFestival_bg"]];
    
    [self.view addSubview:self.prizesCollectionView];
    [self.view addSubview:self.popBtn];
    
    __weak typeof(self) weakSelf = self;
    // 请求活动数据
    [self.activityProtocol requestActivityDataWithBlockModel:^(ActivityContentModel * model)
    {
        weakSelf.awardListData = model.awardList;
        weakSelf.aId = model.aId;
        [weakSelf.heaerView getBeforTime:model.beginDatetime currentTime:model.createDatetime endTime:model.endDatetime luckyDrawNum:model.playNum];
        weakSelf.heaerView.championListData = model.forgeryDataList;
        
        [weakSelf.footerView getStatementContent:model.activeRemark rulesContent:model.activeRole];
        
        [weakSelf.prizesCollectionView reloadData];
    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.heaerView removeDelegate];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _awardListData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"activityCell" forIndexPath:indexPath];
    
    [cell getAwardData:[_awardListData objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake((SCREEN_WIDTH - kCurrentScreen(30)*6)/3 -1, ((SCREEN_WIDTH - kCurrentScreen(30)*6)/3 -1)*2);
    return size;
}

//设置sectionHeader | sectionFoot
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    __weak typeof(self) weakSelf = self;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        self.heaerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"activityHeaderView" forIndexPath:indexPath];
        
        self.heaerView.blockLuckDraw = ^{
            [weakSelf requestLuckDraw];
        };
        self.heaerView.blockLotteryStatus = ^(BOOL status) {
            weakSelf.isLotteryStatus = status;
        };
        
        [self.heaerView.myPrizeBtn addTarget:self action:@selector(requestPrizeList) forControlEvents:UIControlEventTouchUpInside];
        
        return self.heaerView;
    }else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        self.footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"activityFooterView" forIndexPath:indexPath];
        
        self.footerView.blockCurrentViewHeight = ^(CGFloat currentViewHeight) {
            weakSelf.bottomHeight = currentViewHeight;
        };
        
        return self.footerView;
    }
    return nil;
}

// 请求我的奖品列表数据
- (void) requestPrizeList
{
    __weak typeof(self) weakSelf = self;
    [self.activityProtocol requestActivityPrizeListDataWithBlockResultList:^(NSArray * resultList) {
        weakSelf.heaerView.myPrizeData = resultList;
    }];
}

/// 请求抽奖结果
- (void) requestLuckDraw
{
    __weak typeof(self) weakSelf = self;
    [self.activityProtocol requestActivityPlayWithId:_aId blockResult:^(NSString * result) {
        result = [result stringByReplacingOccurrencesOfString:@"[" withString:@""];
        result = [result stringByReplacingOccurrencesOfString:@"]" withString:@""];
        weakSelf.heaerView.result = [result componentsSeparatedByString:@","];
    }];
}

/// 返回事件
- (void) popPreviousPage
{
    if (!_isLotteryStatus) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (ActivityProtocol *)activityProtocol
{
    if (!_activityProtocol) {
        _activityProtocol = [[ActivityProtocol alloc] init];
        _activityProtocol.activityVc = self;
    }
    return _activityProtocol;
}

- (UIButton *)popBtn
{
    if (!_popBtn) {
        _popBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _popBtn.frame = CGRectMake(kCurrentScreen(20), kStatusHeight, kCurrentScreen(200), 44);
        [_popBtn setTitle:@"返 回" forState:UIControlStateNormal];
        [_popBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _popBtn.titleLabel.font = [UIFont systemFontOfSize:kCurrentScreen(50)];
        
        [_popBtn addTarget:self action:@selector(popPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    }
    return _popBtn;
}

- (UICollectionView *)prizesCollectionView
{
    if (!_prizesCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = kCurrentScreen(30);
        layout.minimumInteritemSpacing = kCurrentScreen(30);
        layout.sectionInset = UIEdgeInsetsMake(kCurrentScreen(30), kCurrentScreen(30), kCurrentScreen(30), kCurrentScreen(30));
        layout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH, kCurrentScreen(2000));
        layout.footerReferenceSize = CGSizeMake(SCREEN_WIDTH, kCurrentScreen(700));
        
        _prizesCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        _prizesCollectionView.backgroundColor = [UIColor clearColor];
        _prizesCollectionView.delegate = self;
        _prizesCollectionView.dataSource = self;
        _prizesCollectionView.showsVerticalScrollIndicator = NO;
        _prizesCollectionView.showsHorizontalScrollIndicator = NO;
        
        [_prizesCollectionView registerClass:[ActivityHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"activityHeaderView"];
        
        [_prizesCollectionView registerClass:[ActivityCell class] forCellWithReuseIdentifier:@"activityCell"];
        [_prizesCollectionView registerClass:[ActivityFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"activityFooterView"];
    }
    return _prizesCollectionView;
}



@end
