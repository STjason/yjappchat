//
//  ActivityChampionListCell.h
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityChampionListCell : UITableViewCell


/**
 接收列表数据
 */
- (void) getChampionListData:(id)data row:(NSInteger)row;

@end
