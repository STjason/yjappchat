//
//  ActivityFooterView.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityFooterView.h"


// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

@interface ActivityFooterView ()

/** 活动声明 */
@property (nonatomic, strong) UILabel *activityTitleLabel;

/** 声明内容 */
@property (nonatomic, strong) UILabel *statementContentLabel;

/** 活动规则 */
@property (nonatomic, strong) UILabel *rulesTitleLabel;

/** 规则内容 */
@property (nonatomic, strong) UILabel *rulesContentLabel;

@end


@implementation ActivityFooterView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.activityTitleLabel];
        [self addSubview:self.statementContentLabel];
        [self addSubview:self.rulesTitleLabel];
        [self addSubview:self.rulesContentLabel];
        
    }
    return self;
}

- (void)getStatementContent:(NSString *)statementContent rulesContent:(NSString *)rulesContent
{
    self.statementContentLabel.text = statementContent;
    self.rulesContentLabel.text = rulesContent;
    
    CGSize statementSize = [self.statementContentLabel.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, MAXFLOAT) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15.]} context:nil].size;
    self.statementContentLabel.frame = CGRectMake(5, CGRectGetMaxY(self.activityTitleLabel.frame), SCREEN_WIDTH - 10, statementSize.height);
    self.rulesTitleLabel.frame = CGRectMake(5, CGRectGetMaxY(self.statementContentLabel.frame), SCREEN_WIDTH - 10, kCurrentScreen(60));
    CGSize rulesSize = [self.rulesContentLabel.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, MAXFLOAT) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15.]} context:nil].size;
    self.rulesContentLabel.frame = CGRectMake(5, CGRectGetMaxY(self.rulesTitleLabel.frame), SCREEN_WIDTH - 10, rulesSize.height);
    
    _currentViewHeight = CGRectGetMaxY(self.rulesContentLabel.frame) + 10;
    
    if (_blockCurrentViewHeight) {
        _blockCurrentViewHeight(_currentViewHeight);
    }
}

- (UILabel *)activityTitleLabel
{
    if (!_activityTitleLabel) {
        _activityTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH - 10, kCurrentScreen(60))];
        _activityTitleLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
        _activityTitleLabel.textColor = [UIColor yellowColor];
        _activityTitleLabel.textAlignment = NSTextAlignmentCenter;
        _activityTitleLabel.text = @"活动声明";
        
    }
    return _activityTitleLabel;
}

- (UILabel *)statementContentLabel
{
    if (!_statementContentLabel) {
        _statementContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.activityTitleLabel.frame), SCREEN_WIDTH - 10, kCurrentScreen(60))];
        _statementContentLabel.font = [UIFont systemFontOfSize:kCurrentScreen(45)];
        _statementContentLabel.textColor = [UIColor whiteColor];
        _statementContentLabel.textAlignment = NSTextAlignmentCenter;
        _statementContentLabel.numberOfLines = 0;
    }
    return _statementContentLabel;
}

- (UILabel *)rulesTitleLabel
{
    if (!_rulesTitleLabel) {
        _rulesTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.statementContentLabel.frame), SCREEN_WIDTH - 10, kCurrentScreen(60))];
        _rulesTitleLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
        _rulesTitleLabel.textColor = [UIColor yellowColor];
        _rulesTitleLabel.textAlignment = NSTextAlignmentCenter;
        _rulesTitleLabel.text = @"活动规则";
    }
    return _rulesTitleLabel;
}

- (UILabel *)rulesContentLabel
{
    if (!_rulesContentLabel) {
        _rulesContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.rulesTitleLabel.frame), SCREEN_WIDTH - 10, kCurrentScreen(60))];
        _rulesContentLabel.font = [UIFont systemFontOfSize:kCurrentScreen(45)];
        _rulesContentLabel.textColor = [UIColor whiteColor];
        _rulesContentLabel.textAlignment = NSTextAlignmentCenter;
        _rulesContentLabel.numberOfLines = 0;
        
    }
    return _rulesContentLabel;
}

@end
