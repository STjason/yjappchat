//
//  UIColor+Category.h
//  Lottery
//
//  Created by IOS on 2017/8/30.
//  Copyright © 2017年 IOS. All rights reserved.
//  *************************************************
//  注意：为了颜色能够高效统一管理，可在此类增加颜色以提高项目维护性
//  *************************************************

#import <UIKit/UIKit.h>

@interface UIColor (Category)

/** 淡灰色 */
+ (UIColor *)hc_lightgreyColor;

/** hc_mainColor */
+ (UIColor *)hc_mainColor;

/** 字体red */
+ (UIColor *)fontColor;

/** 按钮orange */
+ (UIColor *)btnOrangeColor;

/** 常用线条颜色 */
+ (UIColor *)lineColor;

/** 导航栏颜色 */
+ (UIColor *)navColor;

/** view背景颜色 */
+ (UIColor *)viewBgColor;

/** view白色背景色 */
+ (UIColor *)viewWhiteColor;

/** 常用黑色文字颜色2(浅黑) */
+ (UIColor *)label2Color;

/** 常用黑色文字颜色3(纯白) */
+ (UIColor *)label3Color;

/** 按钮黄色字体 */
+ (UIColor *)labelOrangColor;

/** 常用输入框背景 */
+ (UIColor *)textFieldBgColor;

/** 常用边距颜色 */
+ (UIColor *)viewBorderColor;

+ (UIColor *)colorWithHexString:(NSString *)color;

//从十六进制字符串获取颜色，
//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;

/** 随机色(可用于控件构建测试) */
+ (UIColor *)randomColor;

@end
