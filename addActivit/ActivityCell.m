//
//  ActivityCell.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityCell.h"
#import "gameplay-Swift.h"


//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

@interface ActivityCell ()

/** 奖品图片 */
@property (nonatomic, strong) UIImageView *awardImageView;

/** 奖品描述 */
@property (nonatomic, strong) UILabel *discriptionLabel;

/** 奖品类型 */
@property (nonatomic, strong) UILabel *typeLabel;

@end


@implementation ActivityCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.awardImageView];
        [self addSubview:self.discriptionLabel];
        [self addSubview:self.typeLabel];
    }
    return self;
}

- (void)getAwardData:(id)data
{
    ActivityAwardListModel *model = data;
    
    NSString *imageStr;
    if ([model.awardType isEqualToString:@"2"]) {
        imageStr = @"activityResource.bundle/MidAutumnFestival_xianjin";
    }else if ([model.awardType isEqualToString:@"3"]){
        imageStr = @"activityResource.bundle/MidAutumnFestival_liwu";
    }else imageStr = @"activityResource.bundle/MidAutumnFestival_jifen";
    
    self.awardImageView.image = [UIImage imageNamed:imageStr];
    self.discriptionLabel.text = model.awardName;
    self.typeLabel.text = model.codeStr;
}

- (UIImageView *)awardImageView
{
    if (!_awardImageView) {
        CGFloat x = kCurrentScreen(30);
        CGFloat y = kCurrentScreen(30);
        CGFloat w = self.frame.size.width - kCurrentScreen(60);
        CGFloat h = w;
        _awardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, w, h)];
        
    }
    return _awardImageView;
}

- (UILabel *)discriptionLabel
{
    if (!_discriptionLabel) {
        CGFloat x = kCurrentScreen(30);
        CGFloat y = CGRectGetMaxY(self.awardImageView.frame) + kCurrentScreen(20);
        CGFloat w = self.frame.size.width - kCurrentScreen(60);
        CGFloat h = kCurrentScreen(50);
        _discriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, w, h)];
        _discriptionLabel.font = [UIFont systemFontOfSize:kCurrentScreen(35)];
        _discriptionLabel.textColor = [UIColor lightGrayColor];
        _discriptionLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _discriptionLabel;
}

- (UILabel *)typeLabel
{
    if (!_typeLabel) {
        CGFloat x = kCurrentScreen(30);
        CGFloat y = CGRectGetMaxY(self.discriptionLabel.frame) + kCurrentScreen(20);
        CGFloat w = self.frame.size.width - kCurrentScreen(60);
        CGFloat h = kCurrentScreen(50);
        _typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, w, h)];
        _typeLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(50)];
        _typeLabel.textColor = [UIColor blackColor];
        _typeLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    return _typeLabel;
}

@end
