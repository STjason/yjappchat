//
//  ActivityFooterView.h
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef void(^BlockCurrentViewHeight)(CGFloat currentViewHeight);
@interface ActivityFooterView : UICollectionReusableView

/** 当前视图高度 */
@property (nonatomic, assign) CGFloat currentViewHeight;


/** 回调 */
@property (strong, nonatomic) BlockCurrentViewHeight blockCurrentViewHeight;

- (void) getStatementContent:(NSString *)statementContent rulesContent:(NSString *)rulesContent;



@end
