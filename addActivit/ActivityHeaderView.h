//
//  ActivityHeaderView.h
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^BlockLuckDraw)(void);
typedef void(^BlockLotteryStatus)(BOOL status);
@interface ActivityHeaderView : UICollectionReusableView
{
    UIImageView *background;
    UIImageView *image1,*image2,*image3,*image4,*image5,*image6;
    UIImageView *dong1,*dong2,*dong3,*dong4,*dong5,*dong6;
    int index;
    UILabel *countDown_drawNumLabel;
    CAAnimationGroup *animGroup, *animGroup2, *animGroup3, *animGroup4, *animGroup5, *animGroup6;
    CAKeyframeAnimation *animation, *animation2, *animation3, *animation4, *animation5, *animation6;
    UIImageView *dong11, *dong12, *dong13, *dong14, *dong15, *dong16;
}



/** 是否试玩 */
@property (nonatomic, assign) BOOL isDemo;

/** 试玩 按钮 */
@property (nonatomic, strong) UIButton *demoBtn;

/** 抽奖 按钮 */
@property (nonatomic, strong) UIButton *luckDrawBtn;

/** 活动细则 按钮 */
@property (nonatomic, strong) UIButton *activityRulesBtn;

/** 我的奖品 按钮 */
@property (nonatomic, strong) UIButton *myPrizeBtn;

/** 状元榜 按钮 */
@property (nonatomic, strong) UIButton *championListBtn;

/** 开奖结果 */
@property (nonatomic, strong) NSArray *result;

/** 我的奖品列表数据 */
@property (nonatomic, strong) NSArray *myPrizeData;

/** 状元榜数据 */
@property (nonatomic, strong) NSArray *championListData;

/** block 回调抽奖事件 */
@property (nonatomic, strong) BlockLuckDraw blockLuckDraw;

/** block 回调当前是否处于抽奖状态 */
@property (nonatomic, strong) BlockLotteryStatus blockLotteryStatus;

/** 提取时间信息 */
- (void) getBeforTime:(NSString *)bTime currentTime:(NSString *)cTime endTime:(NSString *)eTime luckyDrawNum:(NSString *)num;

/** 移除动画 规避内存泄漏 */
- (void) removeDelegate;

@end
