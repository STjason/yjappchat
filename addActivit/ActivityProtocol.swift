//
//  ActivityProtocol.swift
//  gameplay
//
//  Created by JK on 2019/9/11.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

    public var loadingDialog:MBProgressHUD?

func kRequest(isContent: Bool = true, frontDialog:Bool,method:HTTPMethod = .get, loadTextStr:String="正在加载中...",url:String,params:Parameters=[:],
                   callback:@escaping (_ jsonDic: AnyObject)->()) -> Void
    {
        if !NetwordUtil.isNetworkValid()
        {
            showErrorHUD(errStr: "网络连接不可用，请检测")
            return
        }
        /// 开始加载 判断是否需要懒加载提示
        let beforeClosure:beforeRequestClosure = {(showDialog:Bool,showText:String)->Void in
            if frontDialog
            {
                loadingDialog = showLoadingHUD(loadingStr: loadTextStr)
            }
        }
        /// 加载完成 处理回执数据 关闭懒加载提示
        let afterClosure:afterRequestClosure = {(returnStatus:Bool,resultJson:String)->Void in
            if loadingDialog != nil
            {
                loadingDialog?.hide(animated: true)
            }
            if returnStatus == true
            {
                if isContent == true {
                    let jsonData = getDictionaryFromJSONString(jsonString: resultJson)
                    let status = jsonData.value(forKey: "success") as? Bool ?? false
                    if status == true
                    {
                        let content = jsonData.value(forKey: "content")
                        callback(content as AnyObject)
                    }
                    else
                    {
                        showErrorHUD(errStr: jsonData["msg"] as? String ?? "加载失败!");
                    }
                }else {
                    callback(resultJson as AnyObject) 
                }
            }
            else
            {
                showErrorHUD(errStr: "请求超时！请检查网络！");
            }
        }
        
        if url == URL_HEADER_DER {
            requestData(curl: url , cm: method, parameters:params,before: beforeClosure, after: afterClosure)
            return
        }else if url.contains("gateway?") { //支付
            requestData(curl:url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
            return
        }
        
        let baseUrl = BASE_URL + PORT
        requestData(curl: baseUrl + url, cm: method, parameters:params,before: beforeClosure, after: afterClosure)
    }

@objcMembers
class ActivityProtocol: NSObject {

    /// 当前控制器
    @objc weak var activityVc: activityViewController?
   
    /// 请求中秋活动数据
    @objc func requestActivityData(blockModel:@escaping (_ model: ActivityContentModel) -> ())  {
        
        kRequest(isContent: true, frontDialog: true, url: ACTIVITY_GET_BOBING) { (content) in
            let contentDic = content as? NSDictionary ?? NSDictionary()
            let model = ActivityContentModel().getCurrentJson(dic: contentDic)
            blockModel(model)
        }
    }
    
    /// 抽奖事件请求
    @objc func requestActivityPlay(id: String, blockResult:@escaping (_ result: String) -> ()) {
        
        kRequest(isContent: true, frontDialog: true, url: ACTIVITY_BOBING_PLAY) { (content) in
            let contentData = content as? String ?? ""
            blockResult(contentData)
        }
    }
    
    /// 中奖名单
    @objc func requestActivityPrizeListData(blockResultList:@escaping (_ result: NSArray) -> ()) {
        
        kRequest(isContent: true, frontDialog: true, url: ACTIVITY_BOBING_AWARDLIST) { (content) in
            let contentData = content as? NSArray ?? NSArray()
            blockResultList(contentData)
        }
    }
}
