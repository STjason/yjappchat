//
//  NSString+Extension.h
//  常用类的封装
//
//  Created by IOS on 2017/5/23.
//  Copyright © 2017年 IOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)
/** 是否是手机号*/
- (BOOL)hc_isPhoneNumber;

/** 字符串是否是2-16位字母和数字组合 */
+ (BOOL)hc_checkIsDigitalAndLetterRy:(NSString *)string;

/** 是否有数字*/
+ (BOOL)hc_hasDigital:(NSString *)string;

/** 是否有字母*/
+ (BOOL)hc_hasLetter:(NSString *)string;

/** 是否是正确的邮箱*/
- (BOOL)hc_isEmail;

#pragma 匹配密码
- (BOOL)hc_isPassWord;

/** 是否是资金密码*/
- (BOOL)hc_isMoneyPassWord;

/** 是否为微信号*/
- (BOOL)hc_isWeChat;

/** 是否为QQ号*/
- (BOOL)hc_isQQ;

/** MD5加密*/
+ (NSString *)hc_stringToMD5:(NSString *)str;

/** 对象方法保留四位小数*/
- (NSString *)hc_keepDotStr;

/** 保存三位小数*/
+ (NSString *)hc_keepDot:(NSString *)str;

/** 保存idx位小数*/
+ (NSString *)hc_keepDot:(NSString *)str idx:(NSInteger)idx;

+ (NSString *)hc_formaterDoubleString:(double)doublevalue;

/** 字典转JSON字符串*/
+ (NSString *)convertToJsonData:(NSDictionary *)dict;

/** A-B*/
+ (NSString *)A:(NSString *)a jianB:(NSString *)b;

/** A+B*/
+ (NSString *)A:(NSString *)a jiaB:(NSString *)b;

/** A除以B*/
+ (NSString *)A:(NSString *)a chuyiB:(NSString *)b;

/** A乘以B*/
+ (NSString *)A:(NSString *)a chengyiB:(NSString *)b;

/** 添加货币符号*/
- (NSString *)hc_currencyString;

/** 密码 6-13为字母数字组合*/
+ (BOOL)hc_checkPassword:(NSString *)password;

/** 匹配手机号*/
+ (BOOL)hc_checkTelNumber:(NSString *) telNumber;

/** 移除HTML标签*/
+ (NSString *)hc_stringRemoveHtml:(NSString *)htmlString;

/*获取 bundle version版本号 */
+ (NSString*)hc_getLocalAppVersion;

/* 获取BundleID */
+ (NSString*)hc_getBundleID;

/* 获取app的名字 */
+ (NSString*)hc_getAppName;

/** 获取设备型号*/
+ (NSString*)hc_deviceModelName;

/** 生成随机UUID*/
+ (NSString *)hc_UUIDString;

/** 获取当前时间*/
+ (NSString *)hc_currenTime;

/** 限制输入数字*/
+ (BOOL)hc_validateNumber:(NSString*)number;
@end
