//
//  ActivityMyPrizeView.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityMyPrizeView.h"
#import "ActivityMyPrizeCell.h"




// 导航栏高度
#define SCREEN_NAV  ([[UIApplication sharedApplication] statusBarFrame].size.height+44)
// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

@interface ActivityMyPrizeView ()<UITableViewDelegate, UITableViewDataSource>

/** 状元榜列表 */
@property (nonatomic, strong) UITableView *myPrizeListView;


@end

@implementation ActivityMyPrizeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.myPrizeListView];
    }
    return self;
}

- (void)setListData:(NSArray *)listData
{
    _listData = listData;
    [self.myPrizeListView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityMyPrizeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityChampionListCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell getPrizeModel:[_listData objectAtIndex:indexPath.row]];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kCurrentScreen(100))];
    
    UIView *bgView = [[UIView alloc] initWithFrame:view.bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = .4;
    [view addSubview:bgView];
    
    UILabel *rankLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kCurrentScreen(160), kCurrentScreen(100))];
    rankLabel.text = @"博到";
    rankLabel.textColor = [UIColor yellowColor];
    rankLabel.textAlignment = NSTextAlignmentCenter;
    rankLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rankLabel.frame), 0, kCurrentScreen(400), kCurrentScreen(100))];
    nameLabel.text = @"奖品";
    nameLabel.textColor = [UIColor yellowColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    UILabel *officalLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(nameLabel.frame), 0, kCurrentScreen(160), kCurrentScreen(100))];
    officalLabel.text = @"时间";
    officalLabel.textColor = [UIColor yellowColor];
    officalLabel.textAlignment = NSTextAlignmentCenter;
    officalLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    UILabel *numLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(officalLabel.frame), 0, kCurrentScreen(160), kCurrentScreen(100))];
    numLabel.text = @"状态";
    numLabel.textColor = [UIColor yellowColor];
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    [view addSubview:rankLabel];
    [view addSubview:nameLabel];
    [view addSubview:officalLabel];
    [view addSubview:numLabel];
    
    return view;
}

- (UITableView *)myPrizeListView
{
    if (!_myPrizeListView) {
        _myPrizeListView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _myPrizeListView.delegate = self;
        _myPrizeListView.dataSource = self;
        _myPrizeListView.backgroundColor = [UIColor clearColor];
        _myPrizeListView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_myPrizeListView registerClass:[ActivityMyPrizeCell class] forCellReuseIdentifier:@"activityChampionListCell"];
    }
    return _myPrizeListView;
}

@end
