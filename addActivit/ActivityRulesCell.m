//
//  ActivityRulesCell.m
//  gameplay
//
//  Created by JK on 2019/9/11.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityRulesCell.h"

@interface ActivityRulesCell ()

/** 描述 Label */
@property (nonatomic, strong) UILabel *discriptionLabel;

/** 图片展示 */
@property (nonatomic, strong) UIImageView *displayImageView;

@end

@implementation ActivityRulesCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.discriptionLabel];
        [self addSubview:self.displayImageView];
    }
    return self;
}

- (void)getCurrentData:(NSDictionary *)dic
{
    self.displayImageView.image = [UIImage imageNamed:[dic valueForKey:@"image"]];
    self.discriptionLabel.text = [dic valueForKey:@"title"];
    CGSize labelSize = [self.discriptionLabel.text boundingRectWithSize:CGSizeMake(280, MAXFLOAT)
                                                   options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15.]} context:nil].size;
    self.discriptionLabel.frame = (CGRect){{10, CGRectGetMaxY(self.displayImageView.frame) + 5}, labelSize};
    
    _currentCellHeight = CGRectGetMaxY(self.discriptionLabel.frame);
}

- (UILabel *)discriptionLabel
{
    if (!_discriptionLabel) {
        _discriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.displayImageView.frame) + 5, 280, 30)];
        _discriptionLabel.textColor = [UIColor whiteColor];
        _discriptionLabel.font = [UIFont boldSystemFontOfSize:15.];
        _discriptionLabel.numberOfLines = 0;
    }
    return _discriptionLabel;
}

- (UIImageView *)displayImageView
{
    if (!_displayImageView) {
        _displayImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 180, 25)];
    }
    return _displayImageView;
}

@end
