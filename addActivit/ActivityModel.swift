//
//  ActivityModel.swift
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
@objcMembers
class ActivityModel: NSObject {

}
@objcMembers
class ActivityContentModel: NSObject {
    
    /**  */
    @objc var activeRemark : String?
    /**  */
    @objc var activeRole : String?
    /**  */
    @objc var beginDatetime : String?
    /**  */
    @objc var countType : String?
    /**  */
    @objc var createDatetime : String?
    /**  */
    @objc var endDatetime : String?
    /**  */
    @objc var aId : String?
    /**  */
    @objc var imgPath : String?
    /**  */
    @objc var joinType : String?
    /**  */
    @objc var levelIds : String?
    /**  */
    @objc var playConfig : String?
    /**  */
    @objc var playNum : String?
    /**  */
    @objc var playVal : String?
    /**  */
    @objc var stationId : String?
    /**  */
    @objc var status : String?
    /**  */
    @objc var title : String?
    /**  */
    @objc var forgeryDataList : [ActivityChampionListModel]?
    /**  */
    @objc var awardList : [ActivityAwardListModel]?
    
    func getCurrentJson(dic: NSDictionary) -> ActivityContentModel
    {
        let model = ActivityContentModel()
        
        model.activeRemark      = String(format: "%@", dic.value(forKey: "activeRemark") as! CVarArg)
        model.activeRole        = String(format: "%@", dic.value(forKey: "activeRole") as! CVarArg)
        model.beginDatetime     = String(format: "%@", dic.value(forKey: "beginDatetime") as! CVarArg)
        model.countType         = String(format: "%@", dic.value(forKey: "countType") as! CVarArg)
        model.createDatetime    = String(format: "%@", dic.value(forKey: "createDatetime") as! CVarArg)
        model.endDatetime       = String(format: "%@", dic.value(forKey: "endDatetime") as! CVarArg)
        model.aId               = String(format: "%@", dic.value(forKey: "id") as! CVarArg)
        model.imgPath           = String(format: "%@", dic.value(forKey: "imgPath") as! CVarArg)
        model.joinType          = String(format: "%@", dic.value(forKey: "joinType") as! CVarArg)
        model.levelIds          = String(format: "%@", dic.value(forKey: "levelIds") as! CVarArg)
        model.playConfig        = String(format: "%@", dic.value(forKey: "playConfig") as! CVarArg)
        model.playNum           = String(format: "%@", dic.value(forKey: "playNum") as! CVarArg)
        model.playVal           = String(format: "%@", dic.value(forKey: "playVal") as! CVarArg)
        model.stationId         = String(format: "%@", dic.value(forKey: "stationId") as! CVarArg)
        model.status            = String(format: "%@", dic.value(forKey: "status") as! CVarArg)
        model.title             = String(format: "%@", dic.value(forKey: "title") as! CVarArg)
        
        model.forgeryDataList = []
        let forgeryDataList = dic.value(forKey: "forgeryDataList") as! NSArray
        for obj in forgeryDataList {
            let aModel = ActivityChampionListModel().getCurrentJson(dic: obj as! NSDictionary)
            model.forgeryDataList?.append(aModel)
        }
        
        model.awardList = []
        let awardList = dic.value(forKey: "awardList") as! NSArray
        for obj in awardList {
            let aModel = ActivityAwardListModel().getCurrentJson(dic: obj as! NSDictionary)
            model.awardList?.append(aModel)
        }
        
        return model
    }
    
}
@objcMembers
class ActivityAwardListModel: NSObject {
    
    /** 奖品id */
    @objc var activeId : String?
    /** 奖品名称 */
    @objc var awardName : String?
    /** 奖品类型 */
    @objc var awardType : String?
    /**  */
    @objc var awardValue : String?
    /**  */
    @objc var chance : String?
    /**  */
    @objc var code : String?
    /**  */
    @objc var codeStr : String?
    /**  */
    @objc var id : String?
    /** 图片地址 */
    @objc var productImg : String?
    /**  */
    @objc var productRemark : String?
    
    func getCurrentJson(dic: NSDictionary) -> ActivityAwardListModel
    {
        let model = ActivityAwardListModel()
        
        model.activeId          = String(format: "%@", dic.value(forKey: "activeId") as! CVarArg)
        model.awardName         = String(format: "%@", dic.value(forKey: "awardName") as! CVarArg)
        model.awardType         = String(format: "%@", dic.value(forKey: "awardType") as! CVarArg)
        
        if dic.value(forKey: "awardValue") != nil {
            model.awardValue        = String(format: "%@", dic.value(forKey: "awardValue") as! CVarArg)
        }
        
        model.chance            = String(format: "%@", dic.value(forKey: "chance") as! CVarArg)
        model.code              = String(format: "%@", dic.value(forKey: "code") as! CVarArg)
        model.codeStr           = String(format: "%@", dic.value(forKey: "codeStr") as! CVarArg)
        model.id                = String(format: "%@", dic.value(forKey: "id") as! CVarArg)
        model.productImg        = String(format: "%@", dic.value(forKey: "productImg") as! CVarArg)
        model.productRemark     = String(format: "%@", dic.value(forKey: "productRemark") as! CVarArg)
        
        return model;
    }
    
}
@objcMembers
class ActivityChampionListModel: NSObject {
    
    /**  */
    @objc var id : String?
    /**  */
    @objc var itemName : String?
    /**  */
    @objc var stationId : String?
    /**  */
    @objc var type : String?
    /**  */
    @objc var username : String?
    /**  */
    @objc var winMoney : String?
    /**  */
    @objc var winTime : String?
    
    func getCurrentJson(dic: NSDictionary) -> ActivityChampionListModel
    {
        let model = ActivityChampionListModel()
        
        model.id            = String(format: "%@", dic.value(forKey: "id") as! CVarArg)
        model.itemName      = String(format: "%@", dic.value(forKey: "itemName") as! CVarArg)
        model.stationId     = String(format: "%@", dic.value(forKey: "stationId") as! CVarArg)
        model.type          = String(format: "%@", dic.value(forKey: "type") as! CVarArg)
        model.username      = String(format: "%@", dic.value(forKey: "username") as! CVarArg)
        model.winMoney      = String(format: "%@", dic.value(forKey: "winMoney") as! CVarArg)
        model.winTime       = String(format: "%@", dic.value(forKey: "winTime") as! CVarArg)
        
        return model
    }
}
@objcMembers
class ActivityMyPrizeModel: NSObject {
    
    /**  */
    @objc var account : String?
    /**  */
    @objc var accountId : String?
    /**  */
    @objc var activeId : String?
    /**  */
    @objc var awardType : String?
    /**  */
    @objc var awardValue : String?
    /**  */
    @objc var createDatetime : String?
    /**  */
    @objc var dataVersion : String?
    /**  */
    @objc var haoMa : String?
    /**  */
    @objc var id : String?
    /**  */
    @objc var productName : String?
    /**  */
    @objc var stationId : String?
    /**  */
    @objc var status : String?
    
    func getCurrentJson(dic: NSDictionary) -> ActivityMyPrizeModel
    {
        let model = ActivityMyPrizeModel()
        
        model.account               = String(format: "%@", dic.value(forKey: "account") as! CVarArg)
        model.accountId             = String(format: "%@", dic.value(forKey: "accountId") as! CVarArg)
        model.activeId              = String(format: "%@", dic.value(forKey: "activeId") as! CVarArg)
        model.awardType             = String(format: "%@", dic.value(forKey: "awardType") as! CVarArg)
        model.awardValue            = String(format: "%@", dic.value(forKey: "awardValue") as! CVarArg)
        model.createDatetime        = String(format: "%@", dic.value(forKey: "createDatetime") as! CVarArg)
        model.haoMa                 = String(format: "%@", dic.value(forKey: "dataVersion") as! CVarArg)
        model.id                    = String(format: "%@", dic.value(forKey: "activeId") as! CVarArg)
        model.productName           = String(format: "%@", dic.value(forKey: "awardType") as! CVarArg)
        model.stationId             = String(format: "%@", dic.value(forKey: "awardValue") as! CVarArg)
        model.status                = String(format: "%@", dic.value(forKey: "createDatetime") as! CVarArg)
        
        return model
    }
    
    
}
