//
//  PublicConversionModel.m
//  gameplay
//
//  Created by JK on 2019/9/4.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "PublicConversionModel.h"
#import "amr_wav_converter.h"


//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

@implementation PublicConversionModel


+ (CGFloat) getCurrentScreenAdaptation:(id)x
{
    return kCurrentScreen([x floatValue]);
}

+ (CGRect)getCurrentRectX:(id)x Y:(id)y W:(id)w H:(id)h
{
    return CGRectMake([x floatValue], [y floatValue], [w floatValue], [h floatValue]);
}

//转换为wav格式并生成文件到savePath(showSize是否在控制台打印转换后的文件大小)
+ (BOOL)convertAMRtoWAV:(NSString *)filePath savePath:(NSString *)savePath
{
    NSString *wavPlayerFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"WAVtemporaryPlayer.wav"];
    NSString *amrPlayerFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"AMRtemporaryPlayer.amr"];
    
    NSData *amrData = [NSData dataWithContentsOfFile:filePath];
    
    //amr的data写入文件
    [amrData writeToFile:amrPlayerFilePath atomically:YES];
    //将AMR文件转码成WAVE文件
    amr_file_to_wave_file([amrPlayerFilePath cStringUsingEncoding:NSUTF8StringEncoding],
                          [wavPlayerFilePath cStringUsingEncoding:NSUTF8StringEncoding]);
    
    //得到转码后wav的data
    NSData *wavData = [NSData dataWithContentsOfFile:wavPlayerFilePath];
    
    BOOL isSuccess = [wavData writeToFile:savePath atomically:YES];
    if (isSuccess) {
        NSLog(@"TLAudioRecorder转换为wav格式成功,大小为%@",[self fileSizeAtPath:savePath]);
    }
    return isSuccess;
}

+ (NSString *)fileSizeAtPath:(NSString*)filePath{
    unsigned long long size=0;
    
    NSFileManager* manager =[NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        size=[[manager attributesOfItemAtPath:filePath error:nil] fileSize];
        
        if (size >=pow(10,9)) {
            // size >= 1GB
            return [NSString stringWithFormat:@"%.2fGB",size/pow(10,9)];
        } else if (size>=pow(10,6)) {
            // 1GB > size >= 1MB
            return [NSString stringWithFormat:@"%.2fMB",size/pow(10,6)];
        } else if (size >=pow(10,3)) {
            // 1MB > size >= 1KB
            return [NSString stringWithFormat:@"%.2fKB",size/pow(10,3)];
        } else {
            // 1KB > size
            return [NSString stringWithFormat:@"%zdB",size];
        }
    }
    return @"0";
}

@end
