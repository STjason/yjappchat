//
//  ActivityRulesView.m
//  gameplay
//
//  Created by JK on 2019/9/11.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityRulesView.h"
#import "ActivityRulesCell.h"

@interface ActivityRulesView ()<UITableViewDelegate, UITableViewDataSource>

@ property(nonatomic, strong) UITableView *activityTabView;

@property (nonatomic, strong) NSArray *listData;

@end

@implementation ActivityRulesView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.activityTabView];
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityRulesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityRulesCell"];
    
    [cell getCurrentData:[self.listData objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityRulesCell *cell = (ActivityRulesCell *)[self tableView:self.activityTabView cellForRowAtIndexPath:indexPath];
    return cell.currentCellHeight;
}

- (UITableView *)activityTabView
{
    if (!_activityTabView) {
        _activityTabView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _activityTabView.delegate = self;
        _activityTabView.dataSource = self;
        _activityTabView.backgroundColor = [UIColor clearColor];
        _activityTabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_activityTabView registerClass:[ActivityRulesCell class] forCellReuseIdentifier:@"activityRulesCell"];
    }
    return _activityTabView;
}

- (NSArray *)listData
{
    if (!_listData) {
        _listData = @[@{@"title" : @"一秀: 六粒骰子中有一粒为4的。", @"image" : @"activityResource.bundle/MidAutumnFestival_yixiu"},
                      @{@"title" : @"二举: 六粒骰子中有两粒为4的。", @"image" : @"activityResource.bundle/MidAutumnFestival_erju"},
                      @{@"title" : @"三红: 六粒骰子中有三粒为4的。", @"image" : @"activityResource.bundle/MidAutumnFestival_sanhong"},
                      @{@"title" : @"四进: 六粒骰子中有四粒为一样的。", @"image" : @"activityResource.bundle/MidAutumnFestival_sijin"},
                      @{@"title" : @"五红: 有五个点数是4的,称作五红", @"image" : @"activityResource.bundle/MidAutumnFestival_wuhong"},
                      @{@"title" : @"六杯红: 全部的点数都为4,称作六杯红", @"image" : @"activityResource.bundle/MidAutumnFestival_liubeihong"},
                      @{@"title" : @"状元: 有四粒骰子为4既是状元。", @"image" : @"activityResource.bundle/MidAutumnFestival_zhuangyuan"},
                      @{@"title" : @"对称: 六粒骰子为1、2、3、4、5、6", @"image" : @"activityResource.bundle/MidAutumnFestival_duicheng"},
                      @{@"title" : @"五子登科: 有五个点数是一样的。", @"image" : @"activityResource.bundle/MidAutumnFestival_wuzidk"},
                      @{@"title" : @"满地榜: 六粒骰子的点数是一样的,称作满地榜。", @"image" : @"activityResource.bundle/MidAutumnFestival_mandibang"},
                      @{@"title" : @"状元插金花: 有四个点数是四,两个是1为最高点数,称作状元插金花。", @"image" : @"activityResource.bundle/MidAutumnFestival_zhuangyuancjh"}
                      ];
    }
    return _listData;
}

@end
