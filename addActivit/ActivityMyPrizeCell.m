//
//  ActivityMyPrizeCell.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityMyPrizeCell.h"
#import "gameplay-Swift.h"

@interface ActivityMyPrizeCell ()

/** 排行 */
@property (nonatomic, strong) UILabel *resultLabel;

/** 昵称 */
@property (nonatomic, strong) UILabel *nickNameLabel;

/** 官衔 */
@property (nonatomic, strong) UILabel *officialRankLabel;

/** 次数 */
@property (nonatomic, strong) UILabel *numLabel;

/** 数据 */
@property (nonatomic, strong) ActivityMyPrizeModel *myPrizeModel;


@end

@implementation ActivityMyPrizeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.resultLabel];
        [self addSubview:self.nickNameLabel];
        [self addSubview:self.officialRankLabel];
        [self addSubview:self.numLabel];
        
    }
    return self;
}

- (void)getPrizeModel:(id)data
{
    self.resultLabel.text = [data valueForKey:@"haoMa"];
    self.nickNameLabel.text = [data valueForKey:@"account"];
    self.officialRankLabel.text = [data valueForKey:@"createDatetime"];
    
    NSString *status = [NSString stringWithFormat:@"%@", [data valueForKey:@"status"]];
    NSString *str;
    
    if ([status isEqualToString:@"1"]) {
        str = @"未兑换";
    }else if ([status isEqualToString:@"2"]){
        str = @"兑换成功";
    }else if ([status isEqualToString:@"3"]){
        str = @"兑换失败";
    }else{
        str = @"系统取消";
    }
    self.numLabel.text = str;
    
}

- (UILabel *)resultLabel
{
    if (!_resultLabel) {
        _resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 75, 50)];
        _resultLabel.font = [UIFont boldSystemFontOfSize:14.];
        _resultLabel.textColor= [UIColor whiteColor];
        _resultLabel.textAlignment = NSTextAlignmentCenter;
        _resultLabel.numberOfLines = 0;
    }
    return _resultLabel;
}

- (UILabel *)nickNameLabel
{
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.resultLabel.frame) + 5, 0, 75, 50)];
        _nickNameLabel.font = [UIFont boldSystemFontOfSize:14.];
        _nickNameLabel.textColor= [UIColor whiteColor];
        _nickNameLabel.textAlignment = NSTextAlignmentCenter;
        _nickNameLabel.numberOfLines = 0;
    }
    return _nickNameLabel;
}

- (UILabel *)officialRankLabel
{
    if (!_officialRankLabel) {
        _officialRankLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame), 0, 75, 50)];
        _officialRankLabel.font = [UIFont boldSystemFontOfSize:14.];
        _officialRankLabel.textColor = [UIColor whiteColor];
        _officialRankLabel.textAlignment = NSTextAlignmentCenter;
        _officialRankLabel.numberOfLines = 0;
    }
    return _officialRankLabel;
}

- (UILabel *)numLabel
{
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.officialRankLabel.frame), 0, 75, 50)];
        _numLabel.font = [UIFont boldSystemFontOfSize:14.];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.numberOfLines = 0;
    }
    return _numLabel;
}

@end
