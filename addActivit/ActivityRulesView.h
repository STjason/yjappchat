//
//  ActivityRulesView.h
//  gameplay
//
//  Created by JK on 2019/9/11.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityRulesView : UIView

@property (nonatomic, strong) NSArray *activityRulesData;

@end
