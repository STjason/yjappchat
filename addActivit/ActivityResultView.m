//
//  ActivityResultView.m
//  gameplay
//
//  Created by JK on 2019/9/13.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityResultView.h"
#import "UIColor+Category.h"


// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

@interface ActivityResultView ()

/** 图片试图 */
@property (nonatomic, strong) UIView *imgView;

/** 开奖结果 */
@property (nonatomic, strong) UILabel *resultLabel;

@end

@implementation ActivityResultView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.imgView];
        [self addSubview:self.resultLabel];
        [self addSubview:self.demoBtn];
        [self addSubview:self.luckDrawBtn];
        [self addSubview:self.closeBtn];
    }
    return self;
}

- (void)setResult:(NSString *)result
{
    _result = result;
    
    
    for (int i = 0; i < result.length; i++)
    {
        NSRange range = NSMakeRange(i, 1);
        NSString *num = [result substringWithRange:range];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(45*i, 0, 40, 40)];
        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%@@2x.png", num]];
        [self.imgView addSubview:imageView];
    }
    
    NSString *resultStatus;
    NSString *oneStr, *twoStr, *threeStr, *fourStr, *fiveStr, *sexStr;
    oneStr      = [result stringByReplacingOccurrencesOfString:@"1" withString:@""];
    twoStr      = [result stringByReplacingOccurrencesOfString:@"2" withString:@""];
    threeStr    = [result stringByReplacingOccurrencesOfString:@"3" withString:@""];
    fourStr     = [result stringByReplacingOccurrencesOfString:@"4" withString:@""];
    fiveStr     = [result stringByReplacingOccurrencesOfString:@"5" withString:@""];
    sexStr      = [result stringByReplacingOccurrencesOfString:@"6" withString:@""];
    
    if ([fourStr isEqualToString:@"11"])
    {
        resultStatus = @"状元插金花";
    }
    else if (!fourStr.length)
    {
        resultStatus = @"六杯红";
    }
    else if (!oneStr.length || !twoStr.length || !threeStr || !fiveStr.length || !sexStr.length)
    {
        resultStatus = @"满地榜";
    }
    else if (fourStr.length == 1)
    {
        resultStatus = @"五红";
    }
    else if (oneStr.length == 1 || twoStr.length == 1 || threeStr.length == 1 || fiveStr.length == 1 || sexStr.length == 1)
    {
        resultStatus = @"五子登科";
    }
    else if (fourStr.length == 2)
    {
        resultStatus = @"状元";
    }
    else if (fourStr.length == 3)
    {
        resultStatus = @"三红";
    }
    else if (fourStr.length == 4)
    {
        resultStatus = @"二举";
    }
    else if (fourStr.length == 5)
    {
        resultStatus = @"一秀";
    }else resultStatus = @"没有中奖";
    
    self.resultLabel.text = [NSString stringWithFormat:@"摇奖号码:%@(%@)", result, resultStatus];
}

- (UIView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIView alloc] initWithFrame:CGRectMake(15.5, 25, 265, 40)];
    }
    return _imgView;
}

- (UILabel *)resultLabel
{
    if (!_resultLabel) {
        _resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + 10, 300, 30)];
        _resultLabel.textColor = [UIColor blackColor];
        _resultLabel.font = [UIFont boldSystemFontOfSize:16.];
        _resultLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _resultLabel;
}

- (UIButton *)demoBtn
{
    if (!_demoBtn) {
        _demoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _demoBtn.frame = CGRectMake(30, CGRectGetMaxY(self.resultLabel.frame) + 10, 73.3, 30);
        [_demoBtn setTitle:@"试玩" forState:UIControlStateNormal];
        _demoBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
        _demoBtn.layer.shadowColor = [UIColor blackColor].CGColor;
        _demoBtn.layer.shadowOpacity = .5;
        _demoBtn.layer.shadowRadius = 5.;
        _demoBtn.layer.shadowOffset = CGSizeMake(5., 5.);
        _demoBtn.titleLabel.font = [UIFont systemFontOfSize:14.];
        [_demoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _demoBtn;
}

- (UIButton *)luckDrawBtn
{
    if (!_luckDrawBtn) {
        _luckDrawBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _luckDrawBtn.frame = CGRectMake(CGRectGetMaxX(self.demoBtn.frame) + 10, CGRectGetMaxY(self.resultLabel.frame) + 10, 73.3, 30);
        [_luckDrawBtn setTitle:@"抽奖" forState:UIControlStateNormal];
        _luckDrawBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
        _luckDrawBtn.layer.shadowColor = [UIColor blackColor].CGColor;
        _luckDrawBtn.layer.shadowOpacity = .5;
        _luckDrawBtn.layer.shadowRadius = 5.;
        _luckDrawBtn.layer.shadowOffset = CGSizeMake(5., 5.);
        _luckDrawBtn.titleLabel.font = [UIFont systemFontOfSize:14.];
        [_luckDrawBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _luckDrawBtn;
}

- (UIButton *)closeBtn
{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(CGRectGetMaxX(self.luckDrawBtn.frame) + 10, CGRectGetMaxY(self.resultLabel.frame) + 10, 73.3, 30);
        [_closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
        _closeBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
        _closeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
        _closeBtn.layer.shadowOpacity = .5;
        _closeBtn.layer.shadowRadius = 5.;
        _closeBtn.layer.shadowOffset = CGSizeMake(5., 5.);
        _closeBtn.titleLabel.font = [UIFont systemFontOfSize:14.];
        [_closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _closeBtn;
}

@end
