//
//  ActivityMyPrizeCell.h
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityMyPrizeCell : UITableViewCell


- (void) getPrizeModel:(id)data;

@end
