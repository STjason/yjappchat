//
//  ActivityHeaderView.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityHeaderView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Category.h"
#import "PublicAlertView.h"
#import "ActivityRulesView.h"
#import "ActivityChampionListView.h"
#import "ActivityMyPrizeView.h"
#import "ActivityResultView.h"


// 导航栏高度
#define SCREEN_NAV  ([[UIApplication sharedApplication] statusBarFrame].size.height+44)
// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

#define kStatusHeight             [[UIApplication sharedApplication] statusBarFrame].size.height

@interface ActivityHeaderView () <CAAnimationDelegate>

/** 当前倒计时时间 */
@property (nonatomic, assign) NSInteger surplusTime;

/** 当前活动状态 */
@property (nonatomic, strong) NSString *activityStatusStr;

/** 定时器 */
@property (nonatomic, strong) NSTimer *timer;

/** 抽奖次数 */
@property (nonatomic, strong) NSString *luckyDrawNum;

/** 抽奖 弹窗 */
@property (nonatomic, strong) PublicAlertView *resultAlertView;

@end

@implementation ActivityHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // 接口没通 默认试玩
        _isDemo = YES;
        
        [self createUi];
    }
    return self;
}

- (void)setResult:(NSArray *)result
{
    _result = result;
    _isDemo = NO;
    [self clickBobing];
}

- (void)setMyPrizeData:(NSArray *)myPrizeData
{
    _myPrizeData = myPrizeData;
    
    [self showMyPrize];
}

- (void)getBeforTime:(NSString *)bTime currentTime:(NSString *)cTime endTime:(NSString *)eTime luckyDrawNum:(NSString *)num
{
    
    // 开始时间
    NSInteger b = bTime.integerValue;
    // 当前时间
    NSInteger c = cTime.integerValue;
    // 结束时间
    NSInteger e = eTime.integerValue;
    
    if (b > c)
    {
        _activityStatusStr = @"距离活动开始";
        _surplusTime = (c - b)/1000;
        
        [self createTimer];
    }
    else if (c < e)
    {
        _activityStatusStr = @"距离活动结束";
        _surplusTime = (e - c)/1000;
        
        [self createTimer];
    }
    else
    {
        _activityStatusStr = @"活动已结束";
    }
    
    _luckyDrawNum = [NSString stringWithFormat:@"剩余抽奖次数:%@次", num];
}

- (void) createTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshCurrentTime) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void) refreshCurrentTime
{
    _surplusTime--;
    countDown_drawNumLabel.text = [NSString stringWithFormat:@"%@%@\n%@", _activityStatusStr, [self calculationOfTheRemainingTime:_surplusTime], _luckyDrawNum];
    
    CGSize labelSize = [countDown_drawNumLabel.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH, MAXFLOAT)
                                                                options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:15.]} context:nil].size;
    countDown_drawNumLabel.frame = CGRectMake(0, CGRectGetMaxY(background.frame) + kCurrentScreen(100), SCREEN_WIDTH, labelSize.height);
    
}

/// 计算剩余时间
- (NSString *) calculationOfTheRemainingTime:(NSInteger)remainingTime
{
    if(remainingTime <= 0) {
        return @"00:00:00";
    }
    /// 天
    NSInteger d = remainingTime/86400;
    /// 小时
    NSInteger h = (remainingTime/3600)%24;
    /// 分
    NSInteger m = (remainingTime/60)%60;
    /// 秒
    NSInteger s = remainingTime%60;
    
    NSString *dStr;
    if (d < 10) {
        dStr = [NSString stringWithFormat:@"0%ld", d];
    }else dStr = [NSString stringWithFormat:@"%ld", d];
    NSString *hStr;
    if (h < 10) {
        hStr = [NSString stringWithFormat:@"0%ld", h];
    }else hStr = [NSString stringWithFormat:@"%ld", h];
    
    NSString *mStr;
    if (m < 10) {
        mStr = [NSString stringWithFormat:@"0%ld", m];
    }else mStr = [NSString stringWithFormat:@"%ld", m];
    
    NSString *sStr;
    if (s < 10) {
        sStr = [NSString stringWithFormat:@"0%ld", s];
    }else sStr = [NSString stringWithFormat:@"%ld", s];
    
    return [NSString stringWithFormat:@"%@天%@:%@:%@", dStr, hStr, mStr, sStr];
}

- (void) createUi
{
    //添加大碗背景
    background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/MidAutumnFestival_bowl.png"]];
    background.frame = CGRectMake((SCREEN_WIDTH - 320.0)/2., SCREEN_NAV, 320.0, 320.0);
    [self addSubview:background];
    //逐个添加骰子
    UIImageView *_image1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/activity_1@2x.png"]];
    _image1.frame = CGRectMake(kCurrentScreen(150), kCurrentScreen(200), kCurrentScreen(150), kCurrentScreen(150));
    image1 = _image1;
    [background addSubview:_image1];
    UIImageView *_image2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/activity_2@2x.png"]];
    _image2.frame = CGRectMake(kCurrentScreen(350), kCurrentScreen(200), kCurrentScreen(150), kCurrentScreen(150));
    image2 = _image2;
    [background addSubview:_image2];
    UIImageView *_image3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/activity_3@2x.png"]];
    _image3.frame = CGRectMake(kCurrentScreen(550), kCurrentScreen(200), kCurrentScreen(150), kCurrentScreen(150));
    image3 = _image3;
    [background addSubview:_image3];
    UIImageView *_image4 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/activity_4@2x.png"]];
    _image4.frame = CGRectMake(kCurrentScreen(150), kCurrentScreen(400), kCurrentScreen(150), kCurrentScreen(150));
    image4 = _image4;
    [background addSubview:_image4];
    UIImageView *_image5 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/activity_5@2x.png"]];
    _image5.frame = CGRectMake(kCurrentScreen(350), kCurrentScreen(400), kCurrentScreen(150), kCurrentScreen(150));
    image5 = _image5;
    [background addSubview:_image5];
    UIImageView *_image6 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activityResource.bundle/activity_6@2x.png"]];
    _image6.frame = CGRectMake(kCurrentScreen(550), kCurrentScreen(400), kCurrentScreen(150), kCurrentScreen(150));
    image6 = _image6;
    [background addSubview:_image6];
    
    countDown_drawNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(background.frame) + kCurrentScreen(100), SCREEN_WIDTH, .1)];
    countDown_drawNumLabel.textColor = [UIColor whiteColor];
    countDown_drawNumLabel.numberOfLines = 0;
    countDown_drawNumLabel.font = [UIFont systemFontOfSize:kCurrentScreen(45)];
    countDown_drawNumLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:countDown_drawNumLabel];
    
    _demoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _demoBtn.frame = CGRectMake(kCurrentScreen(305), kCurrentScreen(300) + CGRectGetMaxY(background.frame), kCurrentScreen(200), kCurrentScreen(100));
    [_demoBtn setTitle:@"试 玩" forState:UIControlStateNormal];
    [_demoBtn addTarget:self action:@selector(clickBobing) forControlEvents:UIControlEventTouchUpInside];
    [_demoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _demoBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
    _demoBtn.layer.cornerRadius = kCurrentScreen(10);
    _demoBtn.titleLabel.font = [UIFont systemFontOfSize:kCurrentScreen(45)];
    [self addSubview:_demoBtn];
    
    _luckDrawBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _luckDrawBtn.frame = CGRectMake(CGRectGetMaxX(_demoBtn.frame) + kCurrentScreen(45), kCurrentScreen(300) + CGRectGetMaxY(background.frame), kCurrentScreen(285), kCurrentScreen(100));
    [_luckDrawBtn setTitle:@"点击抽奖" forState:UIControlStateNormal];
    [_luckDrawBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _luckDrawBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
    _luckDrawBtn.layer.cornerRadius = kCurrentScreen(10);
    _luckDrawBtn.titleLabel.font = [UIFont systemFontOfSize:kCurrentScreen(45)];
    [_luckDrawBtn addTarget:self action:@selector(clickLuckDraw) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_luckDrawBtn];
    
    _activityRulesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _activityRulesBtn.frame = CGRectMake(kCurrentScreen(85), CGRectGetMaxY(_demoBtn.frame) + kCurrentScreen(60), kCurrentScreen(275), kCurrentScreen(135));
    [_activityRulesBtn setTitle:@"活动细则" forState:UIControlStateNormal];
    [_activityRulesBtn addTarget:self action:@selector(showActivityRules) forControlEvents:UIControlEventTouchUpInside];
    [_activityRulesBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _activityRulesBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
    _activityRulesBtn.layer.cornerRadius = kCurrentScreen(15);
    _activityRulesBtn.titleLabel.font = [UIFont systemFontOfSize:kCurrentScreen(55)];
    [self addSubview:_activityRulesBtn];
    
    _myPrizeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _myPrizeBtn.frame = CGRectMake(CGRectGetMaxX(_activityRulesBtn.frame) + kCurrentScreen(90), CGRectGetMaxY(_demoBtn.frame) + kCurrentScreen(60), kCurrentScreen(275), kCurrentScreen(135));
    [_myPrizeBtn setTitle:@"我的奖品" forState:UIControlStateNormal];
    [_myPrizeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _myPrizeBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
    _myPrizeBtn.layer.cornerRadius = kCurrentScreen(15);
    _myPrizeBtn.titleLabel.font = [UIFont systemFontOfSize:kCurrentScreen(55)];
    [self addSubview:_myPrizeBtn];
    
    _championListBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _championListBtn.frame = CGRectMake(CGRectGetMaxX(_myPrizeBtn.frame) + kCurrentScreen(90), CGRectGetMaxY(_demoBtn.frame) + kCurrentScreen(60), kCurrentScreen(275), kCurrentScreen(135));
    [_championListBtn setTitle:@"状元榜" forState:UIControlStateNormal];
    [_championListBtn addTarget:self action:@selector(showChampionList) forControlEvents:UIControlEventTouchUpInside];
    [_championListBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _championListBtn.backgroundColor = [UIColor colorWithHexString:@"FFCC33"];
    _championListBtn.layer.cornerRadius = kCurrentScreen(15);
    _championListBtn.titleLabel.font = [UIFont systemFontOfSize:kCurrentScreen(55)];
    [self addSubview:_championListBtn];
    
}

- (void) clickBobing {
    
    _demoBtn.userInteractionEnabled = NO;
    _luckDrawBtn.userInteractionEnabled = NO;
    
    [_resultAlertView dismiss];
    
    index = 0;
    //隐藏初始位置的骰子
    image1.hidden = YES;
    image2.hidden = YES;
    image3.hidden = YES;
    image4.hidden = YES;
    image5.hidden = YES;
    image6.hidden = YES;
    dong1.hidden = YES;
    dong2.hidden = YES;
    dong3.hidden = YES;
    dong4.hidden = YES;
    dong5.hidden = YES;
    dong6.hidden = YES;
    //******************旋转动画的初始化******************
    //转动骰子的载入
    NSArray *myImages = [NSArray arrayWithObjects:
                         [UIImage imageNamed:@"activityResource.bundle/dong1@2x.png"],
                         [UIImage imageNamed:@"activityResource.bundle/dong2@2x.png"],
                         [UIImage imageNamed:@"activityResource.bundle/dong3@2x.png"],nil];
    //骰子1的转动图片切换
    dong11 = [[UIImageView alloc] initWithFrame:CGRectMake(kCurrentScreen(150), kCurrentScreen(225), kCurrentScreen(150), kCurrentScreen(150))];
    dong11.animationImages = myImages;
    dong11.animationDuration = 0.5;
    [dong11 startAnimating];
    [self addSubview:dong11];
    dong1 = dong11;
    //骰子2的转动图片切换
    dong12 = [[UIImageView alloc] initWithFrame:CGRectMake(kCurrentScreen(245), kCurrentScreen(235), kCurrentScreen(150), kCurrentScreen(150))];
    dong12.animationImages = myImages;
    dong12.animationDuration = 0.5;
    [dong12 startAnimating];
    [background addSubview:dong12];
    dong2 = dong12;
    //骰子3的转动图片切换
    dong13 = [[UIImageView alloc] initWithFrame:CGRectMake(kCurrentScreen(345), kCurrentScreen(245), kCurrentScreen(150), kCurrentScreen(150))];
    dong13.animationImages = myImages;
    dong13.animationDuration = 0.5;
    [dong13 startAnimating];
    [background addSubview:dong13];
    dong3 = dong13;
    //骰子4的转动图片切换
    dong14 = [[UIImageView alloc] initWithFrame:CGRectMake(kCurrentScreen(170), kCurrentScreen(290), kCurrentScreen(150), kCurrentScreen(150))];
    dong14.animationImages = myImages;
    dong14.animationDuration = 0.5;
    [dong14 startAnimating];
    [background addSubview:dong14];
    dong4 = dong14;
    //骰子5的转动图片切换
    dong15 = [[UIImageView alloc] initWithFrame:CGRectMake(kCurrentScreen(280), kCurrentScreen(360), kCurrentScreen(150), kCurrentScreen(150))];
    dong15.animationImages = myImages;
    dong15.animationDuration = 0.5;
    [dong15 startAnimating];
    [background addSubview:dong15];
    dong5 = dong15;
    //骰子6的转动图片切换
    dong16 = [[UIImageView alloc] initWithFrame:CGRectMake(kCurrentScreen(400), kCurrentScreen(360), kCurrentScreen(150), kCurrentScreen(150))];
    dong16.animationImages = myImages;
    dong16.animationDuration = 0.5;
    [dong16 startAnimating];
    [background addSubview:dong16];
    dong6 = dong16;
    
    //******************旋转动画******************
    //设置动画
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    [spin setToValue:[NSNumber numberWithFloat:M_PI * 16.0]];
    [spin setDuration:4];
    //******************位置变化******************
    
    //骰子1的位置变化
    CGPoint p1 = CGPointMake(kCurrentScreen(570), kCurrentScreen(330));
    CGPoint p2 = CGPointMake(kCurrentScreen(330), kCurrentScreen(200));
    CGPoint p3 = CGPointMake(kCurrentScreen(280), kCurrentScreen(320));
    CGPoint p4 = CGPointMake(kCurrentScreen(380), kCurrentScreen(400));
    NSArray *keypoint = [[NSArray alloc] initWithObjects:[NSValue valueWithCGPoint:p1],[NSValue valueWithCGPoint:p2],[NSValue valueWithCGPoint:p3],[NSValue valueWithCGPoint:p4], nil];
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation setValues:keypoint];
    [animation setDuration:4.0];
    animation.delegate = self;
    [dong11.layer setPosition:CGPointMake(kCurrentScreen(380), kCurrentScreen(400))];
    //骰子2的位置变化
    CGPoint p21 = CGPointMake(kCurrentScreen(270), kCurrentScreen(230));
    CGPoint p22 = CGPointMake(kCurrentScreen(320), kCurrentScreen(440));
    CGPoint p23 = CGPointMake(kCurrentScreen(170), kCurrentScreen(380));
    CGPoint p24 = CGPointMake(kCurrentScreen(410), kCurrentScreen(200));
    NSArray *keypoint2 = [[NSArray alloc] initWithObjects:[NSValue valueWithCGPoint:p21],[NSValue valueWithCGPoint:p22],[NSValue valueWithCGPoint:p23],[NSValue valueWithCGPoint:p24], nil];
    CAKeyframeAnimation *animation2 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation2 setValues:keypoint2];
    [animation2 setDuration:4.0];
    animation2.delegate = self;
    [dong12.layer setPosition:CGPointMake(kCurrentScreen(410), kCurrentScreen(200))];
    //骰子3的位置变化
    CGPoint p31 = CGPointMake(kCurrentScreen(290), kCurrentScreen(230));
    CGPoint p32 = CGPointMake(kCurrentScreen(350), kCurrentScreen(190));
    CGPoint p33 = CGPointMake(kCurrentScreen(280), kCurrentScreen(440));
    CGPoint p34 = CGPointMake(kCurrentScreen(600), kCurrentScreen(260));
    NSArray *keypoint3 = [[NSArray alloc] initWithObjects:[NSValue valueWithCGPoint:p31],[NSValue valueWithCGPoint:p32],[NSValue valueWithCGPoint:p33],[NSValue valueWithCGPoint:p34], nil];
    CAKeyframeAnimation *animation3 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation3 setValues:keypoint3];
    [animation3 setDuration:4.0];
    animation3.delegate = self;
    [dong13.layer setPosition:CGPointMake(kCurrentScreen(600), kCurrentScreen(260))];
    //骰子4的位置变化
    CGPoint p41 = CGPointMake(kCurrentScreen(160),  kCurrentScreen(380));
    CGPoint p42 = CGPointMake(kCurrentScreen(440), kCurrentScreen(320));
    CGPoint p43 = CGPointMake(kCurrentScreen(150),  kCurrentScreen(230));
    CGPoint p44 = CGPointMake(kCurrentScreen(460), kCurrentScreen(350));
    NSArray *keypoint4 = [[NSArray alloc] initWithObjects:[NSValue valueWithCGPoint:p41],[NSValue valueWithCGPoint:p42],[NSValue valueWithCGPoint:p43],[NSValue valueWithCGPoint:p44], nil];
    CAKeyframeAnimation *animation4 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation4 setValues:keypoint4];
    [animation4 setDuration:4.0];
    animation4.delegate = self;
    [dong14.layer setPosition:CGPointMake(kCurrentScreen(460), kCurrentScreen(350))];
    //骰子5的位置变化
    CGPoint p51 = CGPointMake(kCurrentScreen(280), kCurrentScreen(360));
    CGPoint p52 = CGPointMake(kCurrentScreen(450), kCurrentScreen(360));
    CGPoint p53 = CGPointMake(kCurrentScreen(390), kCurrentScreen(230));
    CGPoint p54 = CGPointMake(kCurrentScreen(480), kCurrentScreen(540));
    NSArray *keypoint5 = [[NSArray alloc] initWithObjects:[NSValue valueWithCGPoint:p51],[NSValue valueWithCGPoint:p52],[NSValue valueWithCGPoint:p53],[NSValue valueWithCGPoint:p54], nil];
    CAKeyframeAnimation *animation5 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation5 setValues:keypoint5];
    [animation5 setDuration:4.0];
    animation5.delegate = self;
    [dong15.layer setPosition:CGPointMake(kCurrentScreen(480), kCurrentScreen(540))];
    //骰子6的位置变化
    CGPoint p61 = CGPointMake(kCurrentScreen(400), kCurrentScreen(360));
    CGPoint p62 = CGPointMake(kCurrentScreen(180), kCurrentScreen(380));
    CGPoint p63 = CGPointMake(kCurrentScreen(140), kCurrentScreen(280));
    CGPoint p64 = CGPointMake(kCurrentScreen(660), kCurrentScreen(570));
    NSArray *keypoint6 = [[NSArray alloc] initWithObjects:[NSValue valueWithCGPoint:p61],[NSValue valueWithCGPoint:p62],[NSValue valueWithCGPoint:p63],[NSValue valueWithCGPoint:p64], nil];
    CAKeyframeAnimation *animation6 = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation6 setValues:keypoint6];
    [animation6 setDuration:4.0];
    animation6.delegate = self;
    [dong16.layer setPosition:CGPointMake(kCurrentScreen(660), kCurrentScreen(570))];
    
    //******************动画组合******************
    //骰子1的动画组合
    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
    animGroup.animations = [NSArray arrayWithObjects: animation, spin,nil];
    animGroup.duration = 4;
    animGroup.delegate = self;
    
    [[dong11 layer] addAnimation:animGroup forKey:@"position"];
    //骰子2的动画组合
    CAAnimationGroup *animGroup2 = [CAAnimationGroup animation];
    animGroup2.animations = [NSArray arrayWithObjects: animation2, spin,nil];
    animGroup2.duration = 4;
    animGroup2.delegate = self;
    [[dong12 layer] addAnimation:animGroup2 forKey:@"position"];
    //骰子3的动画组合
    CAAnimationGroup *animGroup3 = [CAAnimationGroup animation];
    animGroup3.animations = [NSArray arrayWithObjects: animation3, spin,nil];
    animGroup3.duration = 4;
    animGroup2.delegate = self;
    [[dong13 layer] addAnimation:animGroup3 forKey:@"position"];
    //骰子4的动画组合
    CAAnimationGroup *animGroup4 = [CAAnimationGroup animation];
    animGroup4.animations = [NSArray arrayWithObjects: animation4, spin,nil];
    animGroup4.duration = 4;
    animGroup4.delegate = self;
    [[dong14 layer] addAnimation:animGroup4 forKey:@"position"];
    //骰子5的动画组合
    CAAnimationGroup *animGroup5 = [CAAnimationGroup animation];
    animGroup5.animations = [NSArray arrayWithObjects: animation5, spin,nil];
    animGroup5.duration = 4;
    animation5.delegate = self;
    [[dong15 layer] addAnimation:animGroup5 forKey:@"position"];
    //骰子6的动画组合
    CAAnimationGroup *animGroup6 = [CAAnimationGroup animation];
    animGroup6.animations = [NSArray arrayWithObjects: animation6, spin,nil];
    animGroup6.duration = 4;
    animGroup6.delegate = self;
    [[dong16 layer] addAnimation:animGroup6 forKey:@"position"];
}

- (void)animationDidStart:(CAAnimation *)anim
{
    if (_blockLotteryStatus) {
        _blockLotteryStatus(YES);
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (index) {
        return;
    }else index++;
    
    //停止骰子自身的转动
    [dong1 stopAnimating];
    [dong2 stopAnimating];
    [dong3 stopAnimating];
    [dong4 stopAnimating];
    [dong5 stopAnimating];
    [dong6 stopAnimating];
    
    //*************产生随机数，真正博饼**************
    srand((unsigned)time(0));  //不加这句每次产生的随机数不变
    
    int result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0;
    
    if (_isDemo) {
        //骰子1的结果
        result1 = (rand() % 5) +1 ;  //产生1～6的数
        //骰子2的结果
        result2 = (rand() % 5) +1 ;  //产生1～6的数
        //骰子3的结果
        result3 = (rand() % 5) +1 ;  //产生1～6的数
        //骰子4的结果
        result4 = (rand() % 5) +1 ;  //产生1～6的数
        //骰子5的结果
        result5 = (rand() % 5) +1 ;  //产生1～6的数
        //骰子6的结果
        result6 = (rand() % 5) +1 ;  //产生1～6的数
    }else {
        result1 = [[_result objectAtIndex:0] intValue];
        result2 = [[_result objectAtIndex:1] intValue];
        result3 = [[_result objectAtIndex:2] intValue];
        result4 = [[_result objectAtIndex:3] intValue];
        result5 = [[_result objectAtIndex:4] intValue];
        result6 = [[_result objectAtIndex:5] intValue];
    }
    dong1.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%d@2x.png", result1]];
    dong2.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%d@2x.png", result2]];
    dong3.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%d@2x.png", result3]];
    dong4.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%d@2x.png", result4]];
    dong5.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%d@2x.png", result5]];
    dong6.image = [UIImage imageNamed:[NSString stringWithFormat:@"activityResource.bundle/activity_%d@2x.png", result6]];
    
    [self showResult:[NSString stringWithFormat:@"%d%d%d%d%d%d", result1, result2, result3, result4, result5, result6]];
    
    _demoBtn.userInteractionEnabled = YES;
    _luckDrawBtn.userInteractionEnabled = YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_blockLotteryStatus) {
            _blockLotteryStatus(NO);
        }
    });
}

- (void) showResult:(NSString *)num
{
    NSString *title = @"抽奖结果";
    if (_isDemo) {
        title = @"试玩结果";
    }
    
    ActivityResultView *resultView = [[ActivityResultView alloc] initWithFrame:CGRectMake(0, 0, 300, 125)];
    resultView.result = num;
    
    [resultView.demoBtn addTarget:self action:@selector(clickBobing) forControlEvents:UIControlEventTouchUpInside];
    [resultView.luckDrawBtn addTarget:self action:@selector(clickLuckDraw) forControlEvents:UIControlEventTouchUpInside];
    [resultView.closeBtn addTarget:self action:@selector(closeResultAlertView) forControlEvents:UIControlEventTouchUpInside];
    
    _resultAlertView = [[PublicAlertView alloc] initWithTitle:title message:nil customView:resultView delegate:nil cancelButtonTitle:nil otherButtonTitles:nil type:0];
    [_resultAlertView show];
}

/// 关闭抽奖结果弹窗
- (void) closeResultAlertView
{
    [_resultAlertView dismiss];
}

/// 抽奖事件
- (void) clickLuckDraw
{
    [_resultAlertView dismiss];
    if (_blockLuckDraw) {
        _blockLuckDraw();
    }
}

/// 展示活动细则
- (void) showActivityRules {
    
    ActivityRulesView *rulesView = [[ActivityRulesView alloc] initWithFrame:CGRectMake(0, 0, 300, 450)];
    
    PublicAlertView *alertView = [[PublicAlertView alloc] initWithTitle:@"活动细则" message:nil customView:rulesView delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确 定" type:1];
    [alertView show];
}

/// 展示我的奖品
- (void) showMyPrize {
    
    ActivityMyPrizeView *myPrizeView = [[ActivityMyPrizeView alloc] initWithFrame:CGRectMake(0, 0, 300, 450)];
    myPrizeView.listData = _myPrizeData;
    PublicAlertView *alertView = [[PublicAlertView alloc] initWithTitle:@"我的奖品" message:nil customView:myPrizeView delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确 定" type:1];
    [alertView show];
}

/// 展示状元榜
- (void) showChampionList {
    
    ActivityChampionListView *championListView = [[ActivityChampionListView alloc] initWithFrame:CGRectMake(0, 0, 300, 450)];
    championListView.listData = _championListData;
    
    PublicAlertView *alertView = [[PublicAlertView alloc] initWithTitle:@"状元榜" message:nil customView:championListView delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确 定" type:1];
    [alertView show];
}

- (void) removeDelegate
{
    [_timer invalidate];
    _timer = nil;
}

@end
