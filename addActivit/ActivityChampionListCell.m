//
//  ActivityChampionListCell.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityChampionListCell.h"
#import "gameplay-Swift.h"

@interface ActivityChampionListCell ()

/** 排行 */
@property (nonatomic, strong) UILabel *rankIcon;

/** 昵称 */
@property (nonatomic, strong) UILabel *nickNameLabel;

/** 官衔 */
@property (nonatomic, strong) UILabel *officialRankLabel;

/** 次数 */
@property (nonatomic, strong) UILabel *numLabel;

/** 数据 */
@property (nonatomic, strong) ActivityChampionListModel *championListModel;

@end

@implementation ActivityChampionListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.rankIcon];
        [self addSubview:self.nickNameLabel];
        [self addSubview:self.officialRankLabel];
        [self addSubview:self.numLabel];
    }
    return self;
}

- (void)getChampionListData:(id)data row:(NSInteger)row
{
    
    self.rankIcon.text = [NSString stringWithFormat:@"%ld", row + 1];
    self.nickNameLabel.text = [data valueForKey:@"username"];
    self.officialRankLabel.text = [data valueForKey:@"itemName"];
    self.numLabel.text = [data valueForKey:@"winMoney"];
}

- (UILabel *)rankIcon
{
    if (!_rankIcon) {
        _rankIcon = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        _rankIcon.font = [UIFont boldSystemFontOfSize:14.];
        _rankIcon.textColor= [UIColor whiteColor];
        _rankIcon.textAlignment = NSTextAlignmentCenter;
    }
    return _rankIcon;
}

- (UILabel *)nickNameLabel
{
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.rankIcon.frame) + 5, 0, 100, 50)];
        _nickNameLabel.font = [UIFont boldSystemFontOfSize:14.];
        _nickNameLabel.textColor= [UIColor whiteColor];
        _nickNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nickNameLabel;
}

- (UILabel *)officialRankLabel
{
    if (!_officialRankLabel) {
        _officialRankLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame), 0, 75, 50)];
        _officialRankLabel.font = [UIFont boldSystemFontOfSize:14.];
        _officialRankLabel.textColor = [UIColor whiteColor];
        _officialRankLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _officialRankLabel;
}

- (UILabel *)numLabel
{
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.officialRankLabel.frame), 0, 75, 50)];
        _numLabel.font = [UIFont boldSystemFontOfSize:14.];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _numLabel;
}


@end
