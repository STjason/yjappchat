//
//  ActivityRulesCell.h
//  gameplay
//
//  Created by JK on 2019/9/11.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityRulesCell : UITableViewCell

/**
 分配规则数据

 @param dic 规则数据
 */
- (void) getCurrentData:(NSDictionary *)dic;

/**
 因为涉及换行，动态计算cell高度
 */
@property (nonatomic, assign) CGFloat currentCellHeight;

@end
