//
//  NSString+Extension.m
//  常用类的封装
//
//  Created by IOS on 2017/5/23.
//  Copyright © 2017年 IOS. All rights reserved.
//

#import "NSString+Category.h"
#import <sys/utsname.h>
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (Category)

#pragma mark 正则表达式
//是否是手机号
- (BOOL)hc_isPhoneNumber
{
    return [[self isRegular:@"^\\d{9,16}$"] evaluateWithObject:self];
}
/**验证该字符串是否是2-16位字母和数字组合*/
+ (BOOL)hc_checkIsDigitalAndLetterRy:(NSString *)string{
    if (string.length < 2 || string.length > 16){
        return NO;
    }
    NSString *regex = @"^[A-Za-z0-9]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([predicate evaluateWithObject:string]) {
        if ([self hc_hasDigital:string] && [self hc_hasLetter:string]) {
            return YES;
        }else{
            return NO;
        }
    }else{
        return NO;
    }
}

/**
 *  是否有数字
 *
 *  @param string 字符串
 *
 *  @return YES 有数字 ，NO 没有数字
 */
+ (BOOL)hc_hasDigital:(NSString *)string
{
    for(int i = 0; i < string.length ; i++) {
        unichar a = [string characterAtIndex:i];
        if ((a >= '0' && a <= '9' )) {
            return YES;
        }
    }
    return NO;
}
/**
 *  是否有字母
 *
 *  @param string 字符串
 *
 *  @return YES 有字母 ，NO 没有字母
 */
+ (BOOL)hc_hasLetter:(NSString *)string
{
    for(int i = 0; i < string.length ; i++){
        unichar a = [string characterAtIndex:i];
        if ((a >= 'A' && a <= 'Z' ) || (a >= 'a' && a <= 'z')) {
            return YES;
        }
    }
    return NO;
}
//判断是否是微信
- (BOOL)hc_isWeChat{
    NSPredicate *predicate = [self isRegular:@"^[A-Za-z0-9]{3,13}$"];
    return [predicate evaluateWithObject:self];
}

- (BOOL)hc_isQQ{
    NSPredicate *predicate = [self isRegular:@"^[1-9][0-9]{5,12}$"];
    return [predicate evaluateWithObject:self];
}

- (BOOL)hc_isPassWord{
    if(self.length >13 || self.length <6) {
        return NO;
    }
    return YES;
}

- (NSString *)hc_keepDotStr {
   return [NSString hc_keepDot:self idx:3];
}

// 是否是正确的邮箱
- (BOOL)hc_isEmail{
   NSPredicate *predicate = [self isRegular:@"^[a-zA-Z\\d]+[\\w\\d.-]*[a-zA-Z\\d]+@([a-zA-Z\\d-]+\\.)+[a-zA-Z]{2,6}$"];
    return [predicate evaluateWithObject:self];
}

- (NSPredicate *)isRegular:(NSString *)string{
    NSString *regular = string;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regular];
    return predicate;
}

- (BOOL)hc_isMoneyPassWord{
    return [[self isRegular:@"^[a-zA-Z0-9]{4,6}$"] evaluateWithObject:self];
}

+ (NSString *)hc_stringToMD5:(NSString *)str {
    const char *fooData = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(fooData, (CC_LONG)strlen(fooData), result);
    NSMutableString *saveResult = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [saveResult appendFormat:@"%02x", result[i]];
    }
    return saveResult;
}

/**格式化浮点数*/
+(NSString *)hc_formaterDoubleString:(double)doublevalue{
    NSString *doubleStr = [NSString stringWithFormat:@"%.2f",doublevalue];
    NSRange pointRange = [doubleStr rangeOfString:@"."];
    if (pointRange.length > 0) {
        //包含小数点
        if ([[doubleStr substringWithRange:NSMakeRange(pointRange.location+2, 1)] isEqualToString:@"0"]) {
            //最后一位为0
            if ([[doubleStr substringWithRange:NSMakeRange(pointRange.location+1, 1)] isEqualToString:@"0"]) {
                //小数点后一位为0
                doubleStr = [NSString stringWithFormat:@"%.f",doublevalue];
            }
            else{
                doubleStr = [NSString stringWithFormat:@"%.1f",doublevalue];
            }
        }
    }
    else{
        //整数
        doubleStr = [NSString stringWithFormat:@"%.f",doublevalue];
    }
    return doubleStr;
}

+ (NSString *)convertToJsonData:(NSDictionary *)dict {
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格、{、}
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    //去掉字符串中的转义字符
   
   
//    NSMutableString *responseString = [NSMutableString stringWithString:mutStr];
//    NSString *character = nil;
//    for (int i = 0; i < responseString.length; i++) {
//        character = [responseString substringWithRange:NSMakeRange(i, 1)];
//        if ([character isEqualToString:@"\\"])
//            [responseString deleteCharactersInRange:NSMakeRange(i, 1)];
//    }

    return mutStr;
}


+ (NSString *)A:(NSString *)a jianB:(NSString *)b{
    if(a==nil || a.floatValue==0.00){
        a = @"0";
    }
    if(b.floatValue==0.00 || a==nil) {
        b = @"0";
    }
    NSDecimalNumber *num1 = [NSDecimalNumber decimalNumberWithString:a];
    NSDecimalNumber *num2 = [NSDecimalNumber decimalNumberWithString:b];
    NSDecimalNumber *resultNum = [num1 decimalNumberBySubtracting:num2];
    return [resultNum stringValue];
}

+ (NSString *)A:(NSString *)a jiaB:(NSString *)b{
    if(a==nil || a.floatValue==0.00){
        a = @"0";
    }
    if(b.floatValue==0.00 || a==nil) {
        b = @"0";
    }
    NSDecimalNumber *num1 = [NSDecimalNumber decimalNumberWithString:a];
    NSDecimalNumber *num2 = [NSDecimalNumber decimalNumberWithString:b];
    NSDecimalNumber *resultNum = [num1 decimalNumberByAdding:num2];
    return [resultNum stringValue];
}

+ (NSString *)A:(NSString *)a chuyiB:(NSString *)b{
    if(a==nil || a.doubleValue==0.00){
        return @"0";
    }
    if(b==nil || b.doubleValue==0.00) {
        return @"0";
    }
    NSDecimalNumber *num1 = [NSDecimalNumber decimalNumberWithString:a];
    NSDecimalNumber *num2 = [NSDecimalNumber decimalNumberWithString:b];
    NSDecimalNumber *resultNum = [num1 decimalNumberByDividingBy:num2];
    
    NSRange range = [[resultNum stringValue] rangeOfString:@"."];
    if([resultNum stringValue].length >= range.location+11) {
        return [[resultNum stringValue] substringToIndex:range.location+11];
    }else{
        return [resultNum stringValue];
    }
}

+ (NSString *)A:(NSString *)a chengyiB:(NSString *)b{
    if(!a.length || a.doubleValue==0.00) {
        a = @"0";
    }
    if(!b.length) {
        b = @"0";
    }
    NSDecimalNumber *num1 = [NSDecimalNumber decimalNumberWithString:a];
    NSDecimalNumber *num2 = [NSDecimalNumber decimalNumberWithString:b];
    NSDecimalNumber *resultNum = [num1 decimalNumberByMultiplyingBy:num2];
    return [resultNum stringValue];
}

- (NSString *)hc_currencyString
{
   NSLocale *cnLocal = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
   NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
   [numFormatter setLocale:cnLocal];
   [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
   NSString *priceStr = [numFormatter stringFromNumber:[NSNumber numberWithDouble:[[self hc_keepDotStr] doubleValue]]];
   return [NSString hc_keepDot:priceStr idx:4];
}

+ (BOOL)hc_checkPassword:(NSString *)password
{
   NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,13}";
   NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
   BOOL isMatch = [pred evaluateWithObject:password];
   return isMatch;
}

+ (BOOL)hc_checkTelNumber:(NSString *)telNumber
{
   NSString *pattern = @"^1+[3578]+\\d{9}";
   NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
   BOOL isMatch = [pred evaluateWithObject:telNumber];
   return isMatch;
}


+ (NSString *)hc_stringRemoveHtml:(NSString *)htmlString{
   NSRegularExpression *regularExpretion = [NSRegularExpression regularExpressionWithPattern:@"<[^>]*>|\n|&[a-zA-Z;]{1,6}" options:NSRegularExpressionCaseInsensitive error:nil];
   NSString *htmlStr = [regularExpretion stringByReplacingMatchesInString:htmlString options:NSMatchingReportProgress range:NSMakeRange(0, htmlString.length) withTemplate:@""];
   htmlStr = [htmlStr stringByReplacingOccurrencesOfString:@"   " withString:@""];
   return htmlStr;
}

/*获取 bundle version版本号 */
+(NSString*) hc_getLocalAppVersion {
   return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
   
}

/* 获取BundleID */
+(NSString*) hc_getBundleID {
   return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

/* 获取app的名字 */
+(NSString*) hc_getAppName {
   NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
   NSMutableString *mutableAppName = [NSMutableString stringWithString:appName];
   return mutableAppName;
}

/** 获取设备名*/
+ (NSString*)hc_deviceModelName
{
   struct utsname systemInfo;
   uname(&systemInfo);
   NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
   
   if ([deviceModel isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
   if ([deviceModel isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
   if ([deviceModel isEqualToString:@"iPhone3,3"])    return @"iPhone 4";
   if ([deviceModel isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
   if ([deviceModel isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
   if ([deviceModel isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
   if ([deviceModel isEqualToString:@"iPhone5,3"])    return @"iPhone 5C";
   if ([deviceModel isEqualToString:@"iPhone5,4"])    return @"iPhone 5C";
   if ([deviceModel isEqualToString:@"iPhone6,1"])    return @"iPhone 5S";
   if ([deviceModel isEqualToString:@"iPhone6,2"])    return @"iPhone 5S";
   if ([deviceModel isEqualToString:@"iPhone7,1"])    return @"iPhone 6Plus";
   if ([deviceModel isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
   if ([deviceModel isEqualToString:@"iPhone8,1"])    return @"iPhone 6S";
   if ([deviceModel isEqualToString:@"iPhone8,2"])    return @"iPhone 6SPlus";
   if ([deviceModel isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
   if ([deviceModel isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
   if ([deviceModel isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
   if ([deviceModel isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
   if ([deviceModel isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
   if ([deviceModel isEqualToString:@"iPhone10,1"])   return @"iPhone 8";
   if ([deviceModel isEqualToString:@"iPhone10,4"])   return @"iPhone 8";
   if ([deviceModel isEqualToString:@"iPhone10,2"])   return @"iPhone 8Plus";
   if ([deviceModel isEqualToString:@"iPhone10,5"])   return @"iPhone 8Plus";
   if ([deviceModel isEqualToString:@"iPhone10,3"])   return @"iPhone X";
   if ([deviceModel isEqualToString:@"iPhone10,6"])   return @"iPhone X";
   if ([deviceModel isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
   if ([deviceModel isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
   if ([deviceModel isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
   if ([deviceModel isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
   if ([deviceModel isEqualToString:@"iPod5,1"])      return @"iPod Touch (5 Gen)";
   if ([deviceModel isEqualToString:@"iPad1,1"])      return @"iPad";
   if ([deviceModel isEqualToString:@"iPad1,2"])      return @"iPad 3G";
   if ([deviceModel isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
   if ([deviceModel isEqualToString:@"iPad2,2"])      return @"iPad 2";
   if ([deviceModel isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
   if ([deviceModel isEqualToString:@"iPad2,4"])      return @"iPad 2";
   if ([deviceModel isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
   if ([deviceModel isEqualToString:@"iPad2,6"])      return @"iPad Mini";
   if ([deviceModel isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
   if ([deviceModel isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
   if ([deviceModel isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
   if ([deviceModel isEqualToString:@"iPad3,3"])      return @"iPad 3";
   if ([deviceModel isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
   if ([deviceModel isEqualToString:@"iPad3,5"])      return @"iPad 4";
   if ([deviceModel isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
   if ([deviceModel isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
   if ([deviceModel isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
   if ([deviceModel isEqualToString:@"iPad4,4"])      return @"iPad Mini 2 (WiFi)";
   if ([deviceModel isEqualToString:@"iPad4,5"])      return @"iPad Mini 2 (Cellular)";
   if ([deviceModel isEqualToString:@"iPad4,6"])      return @"iPad Mini 2";
   if ([deviceModel isEqualToString:@"iPad4,7"])      return @"iPad Mini 3";
   if ([deviceModel isEqualToString:@"iPad4,8"])      return @"iPad Mini 3";
   if ([deviceModel isEqualToString:@"iPad4,9"])      return @"iPad Mini 3";
   if ([deviceModel isEqualToString:@"iPad5,1"])      return @"iPad Mini 4 (WiFi)";
   if ([deviceModel isEqualToString:@"iPad5,2"])      return @"iPad Mini 4 (LTE)";
   if ([deviceModel isEqualToString:@"iPad5,3"])      return @"iPad Air 2";
   if ([deviceModel isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
   if ([deviceModel isEqualToString:@"iPad6,3"])      return @"iPad Pro 9.7";
   if ([deviceModel isEqualToString:@"iPad6,4"])      return @"iPad Pro 9.7";
   if ([deviceModel isEqualToString:@"iPad6,7"])      return @"iPad Pro 12.9";
   if ([deviceModel isEqualToString:@"iPad6,8"])      return @"iPad Pro 12.9";
   
   if ([deviceModel isEqualToString:@"AppleTV2,1"])      return @"Apple TV 2";
   if ([deviceModel isEqualToString:@"AppleTV3,1"])      return @"Apple TV 3";
   if ([deviceModel isEqualToString:@"AppleTV3,2"])      return @"Apple TV 3";
   if ([deviceModel isEqualToString:@"AppleTV5,3"])      return @"Apple TV 4";
   
   if ([deviceModel isEqualToString:@"i386"])         return @"Simulator";
   if ([deviceModel isEqualToString:@"x86_64"])       return @"Simulator";
   
   return deviceModel;
}


+ (NSString *)hc_UUIDString
{
   NSString *result;
   
   CFUUIDRef UUID;
   
   CFStringRef uuidStr;
   
   UUID = CFUUIDCreate(NULL);
   
   uuidStr = CFUUIDCreateString(NULL, UUID);
   
   result  = [NSString stringWithFormat:@"%@", uuidStr];
   
   CFRelease(uuidStr);
   
   CFRelease(UUID);
   
   return result;
}

+ (NSString *)hc_keepDot:(NSString *)str idx:(NSInteger)idx{
   NSRange range = [str rangeOfString:@"."];
   
   if(range.location && [str containsString:@"."]){
      NSString *numberStr = [str substringFromIndex:range.location+1];
      if(numberStr.length>idx){
         numberStr = [numberStr substringToIndex:idx];
         numberStr = [NSString stringWithFormat:@"%zd.%@",str.integerValue,numberStr];
         //            return numberStr;
      }else
      {
         numberStr = str;
      }
      for (NSInteger i = numberStr.length; i > 0; i--) {
         if ([numberStr containsString:@"."]) {
            NSString *numStr = [numberStr substringWithRange:NSMakeRange(i - 1, 1)];
            if ([numStr isEqualToString:@"0"] || [numStr isEqualToString:@"."]) {
               numberStr = [numberStr stringByReplacingCharactersInRange:NSMakeRange(i - 1, 1) withString:@""];
            } else return numberStr;
         }else return numberStr;
      }
   }
   return str;
}

+ (NSString *)hc_keepDot:(NSString *)str{
   return [NSString hc_keepDot:str idx:3];
}

+ (NSString *)hc_currenTime {
   NSDate *currentDate = [NSDate dateWithTimeIntervalSinceNow:8*60*60];
   //日期解析器
   NSDateFormatter *dateFormatter = [NSDateFormatter new];
   [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:+8]];
   dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
   
   return  [dateFormatter stringFromDate:currentDate];
}

/** 限制输入数字*/
+ (BOOL)hc_validateNumber:(NSString*)number {
   BOOL res = YES;
   NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
   int i = 0;
   while (i < number.length) {
      NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
      NSRange range = [string rangeOfCharacterFromSet:tmpSet];
      if (range.length == 0) {
         res = NO;
         break;
      }
      i++;
   }
   return res;
}



@end
