//
//  ActivityResultView.h
//  gameplay
//
//  Created by JK on 2019/9/13.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityResultView : UIView

/** 摇号结果 */
@property (nonatomic, strong) NSString *result;

/** 试玩 按钮 */
@property (nonatomic, strong) UIButton *demoBtn;

/** 抽奖 按钮 */
@property (nonatomic, strong) UIButton *luckDrawBtn;

/** 关闭 按钮 */
@property (nonatomic, strong) UIButton *closeBtn;

@end
