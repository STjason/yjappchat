//
//  ActivityChampionListView.m
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import "ActivityChampionListView.h"
#import "ActivityChampionListCell.h"


// 导航栏高度
#define SCREEN_NAV  ([[UIApplication sharedApplication] statusBarFrame].size.height+44)
// 屏幕宽度
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
//plus 完美适配  公式
#define kCurrentScreen(x)         ([UIScreen mainScreen].bounds.size.width/1242)*x

@interface ActivityChampionListView ()<UITableViewDelegate, UITableViewDataSource>

/** 状元榜列表 */
@property (nonatomic, strong) UITableView *championListView;


@end

@implementation ActivityChampionListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.championListView];
    }
    return self;
}

- (void)setListData:(NSArray *)listData
{
    _listData = listData;
    [self.championListView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityChampionListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityChampionListCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell getChampionListData:[_listData objectAtIndex:indexPath.row] row:indexPath.row];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kCurrentScreen(100))];
    
    UIView *bgView = [[UIView alloc] initWithFrame:view.bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = .4;
    [view addSubview:bgView];
    
    UILabel *rankLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kCurrentScreen(160), kCurrentScreen(100))];
    rankLabel.text = @"排行";
    rankLabel.textColor = [UIColor yellowColor];
    rankLabel.textAlignment = NSTextAlignmentCenter;
    rankLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rankLabel.frame), 0, kCurrentScreen(400), kCurrentScreen(100))];
    nameLabel.text = @"昵称";
    nameLabel.textColor = [UIColor yellowColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    UILabel *officalLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(nameLabel.frame), 0, kCurrentScreen(160), kCurrentScreen(100))];
    officalLabel.text = @"官衔";
    officalLabel.textColor = [UIColor yellowColor];
    officalLabel.textAlignment = NSTextAlignmentCenter;
    officalLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    UILabel *numLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(officalLabel.frame), 0, kCurrentScreen(160), kCurrentScreen(100))];
    numLabel.text = @"次数";
    numLabel.textColor = [UIColor yellowColor];
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.font = [UIFont boldSystemFontOfSize:kCurrentScreen(45)];
    
    [view addSubview:rankLabel];
    [view addSubview:nameLabel];
    [view addSubview:officalLabel];
    [view addSubview:numLabel];
    
    return view;
}

- (UITableView *)championListView
{
    if (!_championListView) {
        _championListView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _championListView.delegate = self;
        _championListView.dataSource = self;
        _championListView.backgroundColor = [UIColor clearColor];
        _championListView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_championListView registerClass:[ActivityChampionListCell class] forCellReuseIdentifier:@"activityChampionListCell"];
    }
    return _championListView;
}

@end
