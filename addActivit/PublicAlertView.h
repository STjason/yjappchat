//
//  PublicAlertView.h
//  ANG
//
//  Created by czj on 15/7/16.
//  Copyright (c) 2015年 aniuge. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ANGBasicBlock)(void);

@protocol PublicAlertViewDelegate;

@interface PublicAlertView : UIView
{
@private
    UILabel   *_titleLabel;
    UILabel   *_bodyTextLabel;
    UITextView *_bodyTextView;
    UIView    *_customView;
    UIView    *_contentView;
    UIView    *_backgroundView;
    BOOL    _visible;
    BOOL    _dimBackground;
    UIInterfaceOrientation _orientation;
    ANGBasicBlock    _cancelBlock;
    ANGBasicBlock    _confirmBlock;
}

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *bodyTextLabel;
/*!
 是否正在显示
 */
@property (nonatomic, readonly, getter=isVisible) BOOL visible;

/*!
 背景是否有渐变背景, 默认YES
 */
@property (nonatomic, assign) BOOL dimBackground;       //是否渐变背景，默认YES

@property (nonatomic, strong) UIView *contentView;       //是否渐变背景，默认YES

/*!
 背景视图，覆盖全屏的，默认nil
 */
@property (nonatomic, strong) UIView *backgroundView;   //背景view, 可无

/*!
 在点击确认后,是否需要dismiss, 默认YES
 */
@property (nonatomic, assign) BOOL shouldDismissAfterConfirm;

/*!
 文本对齐方式
 */
@property (nonatomic, assign) NSTextAlignment contentAlignment;


@property (nonatomic, weak) id<PublicAlertViewDelegate> delegate;

- (void)setCancelBlock:(ANGBasicBlock)block;

- (void)setConfirmBlock:(ANGBasicBlock)block;

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
         customView:(UIView *)customView
           delegate:(id <PublicAlertViewDelegate>)delegate
  cancelButtonTitle:(NSString *)cancelButtonTitle
  otherButtonTitles:(NSString *)otherButtonTitle
               type:(NSInteger)type;

- (void)show;

- (void)dismiss;

+ (void)dismissMaskWindow;

+ (PublicAlertView *)getStackTopAlertView;

+ (void)dismissAllAlerts;

@end

@protocol PublicAlertViewDelegate <NSObject>

@optional

- (void)alertView:(PublicAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;

- (void)alertView:(PublicAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;

- (void)didRotationToInterfaceOrientation:(BOOL)Landscape view:(UIView*)view alertView:(PublicAlertView *)aletView;

@end
