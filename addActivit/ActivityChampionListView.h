//
//  ActivityChampionListView.h
//  gameplay
//
//  Created by JK on 2019/9/12.
//  Copyright © 2019 yibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityChampionListView : UIView

/** 列表数据 */
@property (nonatomic, strong) NSArray *listData;

@end
