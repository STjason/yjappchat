//
//  PublicConfigPlistModel.swift
//  gameplay
//
//  Created by JK on 2019/9/3.
//  Copyright © 2019 yibo. All rights reserved.
//

import UIKit
import MBProgressHUD


    /** 判断是否是iPhoneX */
    let kIsPhone_X           = (UIScreen.main.bounds.height >= 812.0)
    /** info Plist */
    let kInfoDictionary      = Bundle.main.infoDictionary
    /** App 名称 */
    let kAppDisplayName      = Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
    /** Bundle Identifier */
    let kBundleIdentifier    = Bundle.main.bundleIdentifier!
    /** App 版本号 */
    let kAppVersion          = Bundle.main.infoDictionary! ["CFBundleShortVersionString"] as! String
    /** Bulid 版本号 */
    let kBuildVersion        = Bundle.main.infoDictionary! ["CFBundleVersion"] as! String
    /** iOS 版本 */
    let kIOSVersion          = UIDevice.current.systemVersion
    /** 设备 udid */
    let kIdentifierNumber    = UIDevice.current.identifierForVendor
    /** 设备名称 */
    let kSystemName          = UIDevice.current.systemName
    /** 设备型号 */
    let kDeviceModel         = UIDevice.current.model
    /** 设备区域化型号 */
    let kLocalizedModel       = UIDevice.current.localizedModel
    /** 状态栏高度 */
    let kStatusHeight        = UIApplication.shared.statusBarFrame.height
//    /** 屏幕宽 */
//    let kScreenWidth         = UIScreen.main.bounds.width
//    /** 屏幕高 */
//    let kScreenHeight        = UIScreen.main.bounds.height

    ///手势锁 回执状态
    enum GestureLockBlockStatus {
        /// 设置成功
        case setSuccess
        /// 设置失败
        case setFail
        /// 修改成功
        case modifySuccess
        /// 修改失败
        case modifyFail
        /// 验证成功
        case checkSuccess
        /// 验证失败
        case checkFail
        /// 验证次数超过4次需踢出登录
        case checkLoginOut
    }

/// 每日加奖、周周转运活动领取记录公共页面状态
enum ActivityStatus {
    /// 每日加奖
    case DailyBonusStatus
    /// 周周转运
    case WeeklyTransportStatus
}


    /// 封装CGRect 规避 Int、CGFlot、Double问题
    ///
    /// - Parameters:
    ///   - x: x坐标
    ///   - y: y坐标
    ///   - width: 宽
    ///   - height: 高
    /// - Returns: CGRect
    func kCGRect(x: Any, y: Any, width: Any, height: Any) -> CGRect {
        return PublicConversionModel.getCurrentRectX(x, y: y, w: width, h: height)
    }

    /// 屏幕适配
    ///
    /// - Parameter x: x为 需要适配的数值
    /// - Returns: 回执适配后的数值
    func kCurrentScreen(x: Any) -> CGFloat {
        return PublicConversionModel.getCurrentScreenAdaptation(x)
    }

    // MARK: 字典转字符串
    func dictValueString(_ dic:[String : Any]) -> String?{
        let data = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let str = String(data: data!, encoding: String.Encoding.utf8)
        return str
    }

    // MARK: 字符串转字典 Dictionary
    func stringValueDict(_ str: String) -> [String : Any]?{
        let data = str.data(using: String.Encoding.utf8)
        if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
            return dict
        }
        return nil
    }

    // MARK: 字符串转字典 NSDictionary
    func getDictionaryFromJSONString(jsonString:String) ->NSDictionary{
        
        let jsonData:Data = jsonString.data(using: .utf8)!
        
        let dict = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        if dict != nil {
            return dict as! NSDictionary
        }
        return NSDictionary()
    }


    /// 提前配置总数据源
    ///
    /// - Returns: 回执数据 字典类型
    func configData() -> NSDictionary {
        
        let path = Bundle.main.path(forResource: String(format: "%@Config", kBundleIdentifier), ofType: "plist")
        let dataDict = NSDictionary(contentsOfFile: path!)
        
        return dataDict!
    }

    /// 判断字符串是否为空
    ///
    /// - Parameter value: 是AnyObject类型是因为有可能所传的值不是String类型
    /// - Returns: 回执判断结果  true为空 false为非空
    func kStrIsEmpty(value: AnyObject?) -> Bool {
        //首先判断是否为nil
        if (nil == value) {
            //对象是nil，直接认为是空串
            return true
        }else{
            //然后是否可以转化为String
            if let myValue  = value as? String{
                //然后对String做判断
                return myValue == "" || myValue == "(null)" || myValue == "<null>" || myValue == "null" || 0 == myValue.count
            }else{
                //字符串都不是，直接认为是空串
                return false
            }
        }
    }
    /// 加载错误提示
    func showErrorHUD(errStr: String)
    {
        var errHud:MBProgressHUD!
        let appDelegate = UIApplication.shared.delegate
        let window = appDelegate?.window;
        
        errHud = MBProgressHUD.showAdded(to:(window as? UIView)!, animated: false)
        
        errHud.detailsLabel.text = errStr
        errHud.isSquare = false
        errHud.mode = .text
        errHud.label.textColor = UIColor.white
        errHud.detailsLabel.textColor = UIColor.white
        errHud.bezelView.color = UIColor.black
        errHud.hide(animated: true, afterDelay: 2)
    }
    /// 网络懒加载提示
    func showLoadingHUD(loadingStr: String) -> MBProgressHUD
    {
        var loadingHud:MBProgressHUD!
        let appDelegate = UIApplication.shared.delegate
        let window = appDelegate?.window;
        
        loadingHud = MBProgressHUD.showAdded(to:(window as? UIView)!, animated: true)
        
        loadingHud.label.text = loadingStr
        loadingHud.label.textColor = UIColor.white
        loadingHud.bezelView.color = UIColor.black
        loadingHud.label.layoutMargins = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        loadingHud.backgroundView.style = .solidColor
        loadingHud.contentColor = .white
        loadingHud.isUserInteractionEnabled = false
        loadingHud.hide(animated: true, afterDelay: 2)
        
        return loadingHud
    }


    func setQRCodeToImageView(_ imageView: UIImageView?, _ url: String?) {
        if imageView == nil || url == nil {
            return
        }
        
        // 创建二维码滤镜
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        // 恢复滤镜默认设置
        filter?.setDefaults()
        
        // 设置滤镜输入数据
        let data = url!.data(using: String.Encoding.utf8)
        filter?.setValue(data, forKey: "inputMessage")
        
        // 设置二维码的纠错率
        filter?.setValue("M", forKey: "inputCorrectionLevel")
        
        // 从二维码滤镜里面, 获取结果图片
        var image = filter?.outputImage
        
        // 生成一个高清图片
        let transform = CGAffineTransform.init(scaleX: 20, y: 20)
        image = image?.transformed(by: transform)
        
        // 图片处理
        var resultImage = UIImage(ciImage: image!)
        
        // 设置二维码中心显示的小图标
        let center = UIImage(named: "AppIcon.png")
        resultImage = getClearImage(sourceImage: resultImage, center: center!)
        
        // 显示图片
        imageView?.image = resultImage
    }
    
    // 使图片放大也可以清晰
    func getClearImage(sourceImage: UIImage, center: UIImage) -> UIImage {
        
        let size = sourceImage.size
        // 开启图形上下文
        UIGraphicsBeginImageContext(size)
        
        // 绘制大图片
        sourceImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        // 绘制二维码中心小图片
        let width: CGFloat = 80
        let height: CGFloat = 80
        let x: CGFloat = (size.width - width) * 0.5
        let y: CGFloat = (size.height - height) * 0.5
        center.draw(in: CGRect(x: x, y: y, width: width, height: height))
        
        // 取出结果图片
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // 关闭上下文
        UIGraphicsEndImageContext()
        
        return resultImage!
    }

    /** json 字符串数组*/
    func getArrayFromJSONString(jsonString:String) ->NSArray{
        
        let jsonData:Data = jsonString.data(using: .utf8)!
        
        let array = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        if array != nil &&  (jsonString.contains("[") && jsonString.contains("]")){
            return array as! NSArray
        }
        return NSArray()
    }
    /** 从试图层遍历获取控制器 */
func kViewControler(view:UIView) -> UIViewController
    {
        // 1.获取当前视图的下一响应者
        var responder = view.next;
        let b = true
        // 2.判断当前对象是否是视图控制器
        while (b)
        {
            if (responder?.isKind(of: UIViewController.self))!
            {
                return responder as! UIViewController
            }
            else
            {
                responder = responder?.next;
            }
        }
    }

extension Array {
    // 去重
    func filterDuplicates<E: Equatable>(_ filter: (Element) -> E) -> [Element] {
        var result = [Element]()
        for value in self {
            let key = filter(value)
            if !result.map({filter($0)}).contains(key) {
                result.append(value)
            }
        }
        return result
    }
}
/// belowCode获取URL中所有参赛
extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
}
/// 获取url中指定参赛
func getQueryStringParameter(url: String, param: String) -> String? {
    guard let url = URLComponents(string: url) else { return nil }
    return url.queryItems?.first(where: { $0.name == param })?.value
}

class PublicConfigPlistModel: NSObject {
    
    

}
